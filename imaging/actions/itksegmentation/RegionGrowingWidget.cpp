/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <QFileDialog>
#include <QList>
#include <QVector3D>
#include <PickedPixelMap.h>
#include <Application.h>

#include "RegionGrowingWidget.h"
using namespace camitk;

RegionGrowingWidget::RegionGrowingWidget(Action* action)
    : QWidget() {
    this->myAction = action;
    ui.setupUi(this);
    pickingW = new MultiPickingWidget(this);
    ui.seedPointGroupBox->layout()->addWidget(pickingW);

    QObject::connect(ui.applyButton, SIGNAL(clicked()), myAction, SLOT(apply()));
}

bool RegionGrowingWidget::isSmoothingChecked() {
    return ui.smoothingGroupBox->isChecked();
}

int RegionGrowingWidget::getNumberOfIterations() {
    return ui.numberOfIterationsSpinBox->value();
}

double RegionGrowingWidget::getTimeStep() {
    return ui.timeStepDoubleSpinBox->value();
}

int RegionGrowingWidget::getLowThreshold() {
    return ui.lowThresholdSpinBox->value();
}

int RegionGrowingWidget::getHighThreshold() {
    return ui.highThresholdSpinBox->value();
}

QList<QVector3D>* RegionGrowingWidget::getSeedPoints(ImageComponent* image) {
    return pickingW->getPickedPixelMap(image)->getPixelIndexList();
}


void RegionGrowingWidget::updateComponent(ImageComponent* image) {
    ui.componentName->setText(image->getName());
    pickingW->updateComponent(image);
}

