/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// CamiTK incldues
#include "ManualThreshold.h"
#include <Application.h>
#include <ItkProgressObserver.h>
#include <Property.h>

// Itk includes
#include <itkImageToVTKImageFilter.h>
#include <itkVTKImageToImageFilter.h>
#include <itkBinaryThresholdImageFilter.h>

using namespace camitk;


// --------------- constructor -------------------
ManualThreshold::ManualThreshold(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Manual Threshold Filter");
    setDescription("<br/>Sets all the pixels / voxels scalar value according to the following rules:\
                   <ul> \
                   <li>low threshold &lt; scalar value &lt; high threshold &gt; white (255) </li> \
                   <li>scalar value &lt; low threshold OR scalar value &gt; high threshold &gt; black (0)</li> \
                   </ul>");
    setComponent("ImageComponent");

    // Setting classification family and tags
    this->setFamily("ITK Segmentation");
    this->addTag("Threshold");
    this->addTag("Manal");
    this->addTag("Classification");

    // Setting parameters default values
    Property* lowThresholdProperty = new Property(tr("Low threshold"), 128, tr("Voxels which have a scalar value below this threshold will considered as black (0). Above it AND below high threshold, they will be considered as white (255)."), "");
    lowThresholdProperty->setAttribute("minimum", 0);
    lowThresholdProperty->setAttribute("maximum", 255);
    lowThresholdProperty->setAttribute("singleStep", 1);
    addParameter(lowThresholdProperty);

    Property* highThresholdProperty = new Property(tr("High threshold"), 255, tr("Voxels which have a scalar value above this threshold will considered as black (0). Below it AND above low threshold, they will be considered as white (255)."), "");
    highThresholdProperty->setAttribute("minimum", 0);
    highThresholdProperty->setAttribute("maximum", 255);
    highThresholdProperty->setAttribute("singleStep", 1);
    addParameter(highThresholdProperty);
}

// --------------- destructor -------------------
ManualThreshold::~ManualThreshold() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- apply -------------------
Action::ApplyStatus ManualThreshold::apply() {
    foreach (Component* comp, getTargets()) {
        ImageComponent* input = dynamic_cast<ImageComponent*>(comp);
        process(input);
    }
    return SUCCESS;
}

// --------------- process -------------------
void ManualThreshold::process(ImageComponent* comp) {
    // Get the parameters
    this->lowThreshold = property("Low threshold").toInt();
    this->highThreshold = property("High threshold").toInt();

    // ITK filter implementation using templates
    vtkSmartPointer<vtkImageData> inputImage = comp->getImageData();
    vtkSmartPointer<vtkImageData> outputImage = implementProcess(inputImage);

    ImageComponent* outputComp = new ImageComponent(outputImage, comp->getName() + "_threshold");

    // consider frame policy on new image created
    Action::applyTargetPosition(comp, outputComp);

    Application::refresh();

}

#include "ManualThreshold.impl"

// ITK filter implementation
template <class InputPixelType, class OutputPixelType, const int dim>
vtkSmartPointer<vtkImageData> ManualThreshold::itkProcess(vtkSmartPointer<vtkImageData> img) {
    vtkSmartPointer<vtkImageData> result = vtkSmartPointer<vtkImageData>::New();

    // --------------------- Filters declaration and creation ----------------------
    // Define ITK input and output image types with respect to the instanciation
    //    types of the tamplate.
    typedef itk::Image< InputPixelType,  dim > InputImageType;
    typedef itk::Image< OutputPixelType, dim > OutputImageType;

    // Convert the image from CamiTK in VTK format to ITK format to use ITK filters.
    typedef itk::VTKImageToImageFilter<InputImageType> vtkToItkFilterType;
    typename vtkToItkFilterType::Pointer vtkToItkFilter = vtkToItkFilterType::New();

    typedef itk::BinaryThresholdImageFilter <InputImageType, OutputImageType> FilterType;
    typename FilterType::Pointer filter = FilterType::New();

    // In the same way, once the image is filtered, we need to convert it again to
    // VTK format to give it to CamiTK.
    typedef itk::ImageToVTKImageFilter<OutputImageType> itkToVtkFilterType;
    typename itkToVtkFilterType::Pointer itkToVtkFilter = itkToVtkFilterType::New();
// ------------------------- WRITE YOUR CODE HERE ----------------------------------

    // To update CamiTK progress bar while filtering, add an ITK observer to the filters.
    ItkProgressObserver::Pointer observer = ItkProgressObserver::New();
    // ITK observers generally give values between 0 and 1, and CamiTK progress bar
    //    wants values between 0 and 100...
    observer->SetCoef(100.0);

    // --------------------- Plug filters and parameters ---------------------------
    // From VTK to ITK
    vtkToItkFilter->SetInput(img);
    // For the filter itself
    filter->SetInput(vtkToItkFilter->GetOutput());
    filter->AddObserver(itk::ProgressEvent(), observer);
    filter->SetLowerThreshold(lowThreshold);
    filter->SetUpperThreshold(highThreshold);

    // From ITK to VTK
    // Change the following line to put your filter instead of vtkToItkFilter
    // For example: itkToVtkFilter->SetInput(filter->GetOutput());
    itkToVtkFilter->SetInput(filter->GetOutput());

    // --------------------- Actually execute all filters parts --------------------
    itkToVtkFilter->Update();

    // --------------------- Create and return a copy (the filters will be deleted)--
    vtkSmartPointer<vtkImageData> resultImage = itkToVtkFilter->GetOutput();
    int extent[6];
    resultImage->GetExtent(extent);
    result->SetExtent(extent);
    result->DeepCopy(resultImage);

    // Set CamiTK progress bar back to zero (the processing filter is over)
    observer->Reset();

    return result;
}

