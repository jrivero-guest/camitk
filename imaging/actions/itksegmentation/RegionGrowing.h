/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef REGIONGROWING_H
#define REGIONGROWING_H

#include <Action.h>
#include <ImageComponent.h>

/**
 *
 * @ingroup group_cepimaging_actions_itksegmentation
 *
 * @brief
 * Segment using a region growing algorithm the @ref camitk::ImageComponent "ImageComponent"
 *
 **/
class RegionGrowing : public camitk::Action {

public:
    /// Default Constructor
    RegionGrowing(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~RegionGrowing();

    /// Returns the widget that allows one to modify the action parameters
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
      * !!! This method works only if theWidget has been instantiated
      * (i.e. if getWidget has been called at least once, this is the case when there is a GUI)
      * if not, please use the apply method with the parameters...
      */
    virtual ApplyStatus apply();

    /// Same method to be called manually with the parameters
    virtual ApplyStatus apply(int lowThreshold, int highThreshold, QList<QVector3D>* seedPoints, bool filterBefore = false, int nbIterations = 5, double timeStep = 0.120);


private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent* comp);

    vtkSmartPointer<vtkImageData> implementProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcess(vtkSmartPointer<vtkImageData> img);


protected:
    /// The widget will be filled with parameters
    QWidget* theWidget;

    /** Parameters @{ */
    /// Should a smoothing filter be applied before region growing ?
    bool filterBefore;
    /// number of iterations of this smoothing filter
    int nbIterations;
    /// time step of this smoothing filter
    double timeStep;
    /// low threshold for region growing
    int lowThreshold;
    /// high threshold for region growing
    int highThreshold;
    /// list of seed points
    QList<QVector3D>* seedPoints;
    /// @}
};
#endif // REGIONGROWING_H
