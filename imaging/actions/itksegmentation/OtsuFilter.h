/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef OTSUFILTER_H
#define OTSUFILTER_H

#include <QObject>
#include <Action.h>
#include <ImageComponent.h>
#include <ActionWidget.h>

// CamiTK forward declaration
namespace camitk {
class Property;
}

/**
 *
 * @ingroup group_cepimaging_actions_itksegmentation
 *
 * @brief
 * Segment using the otsu filter the @ref camitk::ImageComponent "ImageComponent"
 *
 **/
class OtsuFilter : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    OtsuFilter(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~OtsuFilter();

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();

private:
    /// camitk::Property that displayed the computed threshold after applying the action.
    /// @note The camitk::Property is readonly
    camitk::Property* computedThresholdProperty;

    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent* comp);

    vtkSmartPointer<vtkImageData> implementProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcess(vtkSmartPointer<vtkImageData> img);


protected:
    int insideValue;
    int outsideValue;
    double usedThreshold;


};
#endif // OTSUFILTER_H
