/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef REGIONGROWINGWIDGET_H
#define REGIONGROWINGWIDGET_H

#include <QWidget>

#include <ImageComponent.h>
#include <MultiPickingWidget.h>
#include <Action.h>

#include "ui_RegionGrowingWidget.h"

/**
 *
 * @ingroup group_cepimaging_actions_itksegmentation
 *
 * @brief
 * @ref RegionGrowing "RegionGrowing" action widget.
 *
 **/
class RegionGrowingWidget : public QWidget {
    Q_OBJECT

public:
    /// Default construtor
    RegionGrowingWidget(camitk::Action* action);

    /// destructor
    ~RegionGrowingWidget() override = default;

    /// Update the widget with the correct PickedPixelMap (ImageComponent + Qlist of the selected points)
    void updateComponent(camitk::ImageComponent* image);

    /** Accessors to all parameters
     * @{
     */
    /// Do the user want smoothing ?
    bool isSmoothingChecked();
    /// number of iterations for smoothing
    int getNumberOfIterations();
    /// time step for smoothing
    double getTimeStep();
    /// low threshold for region growing
    int getLowThreshold();
    /// high threshold for region growing
    int getHighThreshold();
    /// List of seed points
    QList<QVector3D>* getSeedPoints(camitk::ImageComponent* image);
    /** @} */

protected:
    Ui::RegionGrowingWidget ui;

    /// Multiple picking widget
    MultiPickingWidget* pickingW;

    /// Connected Action
    camitk::Action* myAction;

};

#endif // REGIONGROWINGWIDGET_H
