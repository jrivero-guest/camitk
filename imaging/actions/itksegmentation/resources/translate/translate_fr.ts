<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ManualThreshold</name>
    <message>
        <location filename="../../ManualThreshold.cpp" line="62"/>
        <source>Low threshold</source>
        <translation>Seuil bas</translation>
    </message>
    <message>
        <location filename="../../ManualThreshold.cpp" line="62"/>
        <source>Voxels which have a scalar value below this threshold will considered as black (0). Above it AND below high threshold, they will be considered as white (255).</source>
        <translation>Les voxels qui ont une valeur scalaire sous ce seuil seront considérés comme noires (0). Au dessus de celui-ci et en dessous du seuil haut, ils seront considérés comme blanc (255). </translation>
    </message>
    <message>
        <location filename="../../ManualThreshold.cpp" line="68"/>
        <source>High threshold</source>
        <translation>Seuil haut</translation>
    </message>
    <message>
        <location filename="../../ManualThreshold.cpp" line="68"/>
        <source>Voxels which have a scalar value above this threshold will considered as black (0). Below it AND above low threshold, they will be considered as white (255).</source>
        <translation>Les voxels qui ont une valeur scalaire au dessus de ce seuil seront considérés comme noires (0). Au dessous de celui-ci et au dessus du seuilbas, ils seront considérés comme blanc (255).</translation>
    </message>
</context>
<context>
    <name>OtsuFilter</name>
    <message>
        <location filename="../../OtsuFilter.cpp" line="62"/>
        <source>Computed threshold:</source>
        <translation>Seuil calculé:</translation>
    </message>
    <message>
        <location filename="../../OtsuFilter.cpp" line="62"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../../OtsuFilter.cpp" line="62"/>
        <source>The computed threshold found by the Otsu filter. 
This property is read only and only updated after applying the action on a component.</source>
        <translation>Le seuil calculé trouvé par le filtre Otsu.
La propriété est read only et only updated aprés l&apos;application de l&apos;action sur un component.</translation>
    </message>
    <message>
        <location filename="../../OtsuFilter.cpp" line="67"/>
        <source>Inside value</source>
        <translation>Valeur intérieure</translation>
    </message>
    <message>
        <location filename="../../OtsuFilter.cpp" line="67"/>
        <source>The scalar type value assigned to voxels which have their scalar type BELOW the threshold value.</source>
        <translation>La valeur du type scalaire assignée aux voxels qui ont leur type scalaire en dessous de la valeur de seuil.</translation>
    </message>
    <message>
        <location filename="../../OtsuFilter.cpp" line="73"/>
        <source>Outside value</source>
        <translation>Valeur extérieure</translation>
    </message>
    <message>
        <location filename="../../OtsuFilter.cpp" line="73"/>
        <source>The scalar type value assigned to voxels which have their scalar type ABOVE the threshold value.</source>
        <translation>La valeur du type scalaire assignée aux voxels qui ont leur type scalaire au-dessus de la valeur de seuil. </translation>
    </message>
</context>
<context>
    <name>RegionGrowingWidget</name>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="14"/>
        <source>Image Volume</source>
        <translation>Volume de l&apos;image</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="20"/>
        <source>Image Component</source>
        <translation>Component Image</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="26"/>
        <source>Original Image Name: </source>
        <translation>Nom original de l&apos;image:</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="33"/>
        <source>None</source>
        <translation>Rien</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="48"/>
        <source>Seed Points</source>
        <translation>Points ensemencés</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="62"/>
        <source>Smoothing</source>
        <translation>Lissage</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="71"/>
        <source>Number of iterations</source>
        <translation>Nombre d&apos;itérations</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="85"/>
        <source>Time Step</source>
        <translation>Durée de l&apos;étape</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="118"/>
        <source>Threshold Values </source>
        <translation>Valeurs de seuil</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="124"/>
        <source>Low Threshold</source>
        <translation>Seuil bas</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="141"/>
        <source>High Threshold</source>
        <translation>Seuil haut</translation>
    </message>
    <message>
        <location filename="../../RegionGrowingWidget.ui" line="189"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
</context>
</TS>
