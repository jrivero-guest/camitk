/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef MORPHOLOGICALOPERATORS_H
#define MORPHOLOGICALOPERATORS_H

#include <QObject>
#include <Action.h>
#include <ImageComponent.h>
#include <ActionWidget.h>

#include "ITKFilterAPI.h"

#include <itkImage.h>

/**
 *
 * @ingroup group_cepimaging_actions_itkfilters
 *
 * @brief
 * Perform some morphological operators such as opening, closing ...
 *
 **/
class ITKFILTER_API MorphologicalOperators : public camitk::Action {
    Q_OBJECT

public:
    /// Define the possible types of morphological operations
    enum MorphoType {GREY_LEVEL, BINARY};

    Q_ENUMS(MorphoType)

    /// Define the possible morphological operations
    enum MorphoOperation {EROSION, DILATION, OPENING, CLOSING};

    Q_ENUMS(MorphoOperation)

    /// Default Constructor
    MorphologicalOperators(camitk::ActionExtension*);

    /// Get the morphological type of operation
    MorphoType getMorphoType() const ;

    /// Get the morphological operation selected
    MorphoOperation getMorphoOperation() const ;

    /// Default Destructor
    virtual ~MorphologicalOperators();

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();


private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent*);

private:
    vtkSmartPointer<vtkImageData> implementProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> binaryErosionFilter(vtkSmartPointer<vtkImageData> img);
    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> binaryDilationFilter(vtkSmartPointer<vtkImageData> img);
    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> binaryOpeningFilter(vtkSmartPointer<vtkImageData> img);
    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> binaryClosureFilter(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> greyLevelErosionFilter(vtkSmartPointer<vtkImageData> img);
    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> greyLevelDilationFilter(vtkSmartPointer<vtkImageData> img);
    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> greyLevelOpeningFilter(vtkSmartPointer<vtkImageData> img);
    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> greyLevelClosureFilter(vtkSmartPointer<vtkImageData> img);



protected:
    /// Morphological operators properties
    MorphoType typeOfOperation;
    MorphoOperation operation;
    int structuringElementSize;

    QString suffix;

};

Q_DECLARE_METATYPE(MorphologicalOperators::MorphoType)
Q_DECLARE_METATYPE(MorphologicalOperators::MorphoOperation)
#endif // MORPHOLOGICALOPERATORS_H
