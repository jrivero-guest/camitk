/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ANISOTROPICDIFFUSION_H
#define ANISOTROPICDIFFUSION_H

#include <QObject>
#include <Action.h>
#include <ImageComponent.h>
#include <ActionWidget.h>

#include "ITKFilterAPI.h"

/**
 *
 * @ingroup group_cepimaging_actions_itkfilters
 *
 * @brief
 * Perform a anisotropic diffusion on the @ref camitk::ImageComponent "ImageComponent"
 *
 **/
class ITKFILTER_API AnisotropicDiffusion : public camitk::Action {
    Q_OBJECT

public:
    /// Define the possible implementations of Gaussian filtering
    enum AnisoDiffType {GRADIENT, CURVATURE};

    // prefers Q_ENUM to Q_ENUMS if possible (Qt >= 5.5)
    Q_ENUMS(AnisoDiffType)

    /// Default Constructor
    AnisotropicDiffusion(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~AnisotropicDiffusion();

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();

    /**
     * @brief Get the diffusion type to be used by the algorithm.
     * @return The diffusion type as an enum.
     * @note The diffusion type uses camitk::Property with its ENUM.
     */
    AnisoDiffType getDiffusionType();


private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent*);

    vtkSmartPointer<vtkImageData> implementProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcessGradientAnisotropicDiffusion(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcessCurvatureAnisotropicDiffusion(vtkSmartPointer<vtkImageData> img);

protected:
    bool keepOrgVoxelType;
    int numberOfIterations;
    double conductance;
};

Q_DECLARE_METATYPE(AnisotropicDiffusion::AnisoDiffType)

#endif // ANISOTROPICDIFFUSION_H
