/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef CONNECTEDCOMPONENTS_H
#define CONNECTEDCOMPONENTS_H

#include <QObject>
#include <Action.h>
#include <ImageComponent.h>
#include <ActionWidget.h>

#include "ITKFilterAPI.h"

/**
 *
 * @ingroup group_cepimaging_actions_itkfilters
 *
 * @brief
 * Perform a connected component analysis on the @ref camitk::ImageComponent "ImageComponent"
 *
 **/
class ITKFILTER_API ConnectedComponents : public camitk::Action {
    Q_OBJECT

    // declaration of parameters
    Q_PROPERTY(int nbConnectedComponents READ getNbConnectedComponents)


public:
    /// Default Constructor
    ConnectedComponents(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~ConnectedComponents() = default;

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();

    int getNbConnectedComponents() const ;
    void setNbConnectedComponents(const int nbConnectedComponents);

private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent*);

    vtkSmartPointer<vtkImageData> implementProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcess(vtkSmartPointer<vtkImageData> img);

    int nbConnectedComponents;


};
#endif // CONNECTEDCOMPONENTS_H
