/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef LAPLACIANSHARPENING_H
#define LAPLACIANSHARPENING_H

#include <QObject>
#include <Action.h>
#include <ImageComponent.h>
#include <ActionWidget.h>

#include "ITKFilterAPI.h"

/**
 *
 * @ingroup group_cepimaging_actions_itkfilters
 *
 * @brief
 * Perform a laplacian sharpening filter on the @ref camitk::ImageComponent "ImageComponent"
 *
 **/
class ITKFILTER_API LaplacianSharpening : public camitk::Action {


public:
    /// Default Constructor
    LaplacianSharpening(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~LaplacianSharpening() = default;

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();

private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent*);

private:
    vtkSmartPointer<vtkImageData> implementProcess(vtkSmartPointer<vtkImageData> img);

    template <class InputPixelType, class OutputPixelType, const int dim>
    vtkSmartPointer<vtkImageData> itkProcess(vtkSmartPointer<vtkImageData> img);

};
#endif // LAPLACIANSHARPENING_H
