/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ITK_IMAGE_COMPONENT_H
#define ITK_IMAGE_COMPONENT_H

#include <vtkImageData.h>
#include <vtkSmartPointer.h>
#include <ImageComponent.h>
/**
 * @ingroup group_cepimaging_components_itkimage
 *
 * @brief
 * This class manage Itk images, i.e images that can be loaded in CamiTK by using an ITK importer.
 *
 * @note This @ref camitk::ComponentExtension "ComponentExtension" requires the ITK library in order to work correctly.
 */
class ItkImageComponent : public camitk::ImageComponent  {
    Q_OBJECT
public:
    /** default constructor: give it the name of the file containing the data.
     *  This method may throw an AbortException if a problem occurs.
     */
    ItkImageComponent(const QString&);

    /// needed for deleting
    virtual ~ItkImageComponent() = default;

    virtual void createComponent(const QString&);

protected:
    vtkSmartPointer<vtkImageData> readVolume(const QString& filename);

};

#endif // ITK_IMAGE_COMPONENT_H
