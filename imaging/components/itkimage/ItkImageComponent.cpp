/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ItkImageComponent.h"

#include <ImageComponent.h>


using namespace camitk;

//-- Qt
#include <QFileInfo>

//-- Vtk
#include <vtkImageData.h>

//-- Itk
#include <itkImageFileReader.h>
#include <itkOrientImageFilter.h>
#include <itkImageToVTKImageFilter.h>

// include Log.h last in order to solve MSVC wingdi.h macro redefinition of ERROR
#include <Log.h>

// -------------------- constructor --------------------
ItkImageComponent::ItkImageComponent(const QString& fileName)
    : ImageComponent(fileName) {
    createComponent(fileName);
}

// -------------------- createComponent --------------------
void ItkImageComponent::createComponent(const QString& filename) {
    if (!filename.isEmpty()) {
        vtkSmartPointer<vtkImageData> img = readVolume(filename);
        if (img) {
            setImageData(img, false);
            // use file basename as default name
            setName(QFileInfo(filename).baseName());
        }
        else {
            throw (AbortException("Could not open file\n"));
        }
    }
    else {
        throw (AbortException("No filename found\n"));
    }

}

// -------------------- readVolume --------------------
vtkSmartPointer<vtkImageData> ItkImageComponent::readVolume(const QString& filename) {
    // Let itk ImageIO factory do all the job...
    // see itk users guide  chapter 7 "Rading and Writing Images"
    typedef short                               PixelType;
    const   unsigned int                    Dimension = 3;
    typedef itk::Image< PixelType, Dimension >  ImageType;
    typedef itk::ImageFileReader< ImageType >  ReaderType;
    typedef itk::ImageToVTKImageFilter< ImageType > ConnectorType;
    //  typedef itk::OrientImageFilter<ImageType,ImageType> OrientType;

    // To copy data
    int dims[3];
    int extent[6];
    double origin[3];
    double spacing[3];

    vtkSmartPointer<vtkImageData> oldPointer = NULL;
    vtkSmartPointer<vtkImageData> newImage = NULL;

    ReaderType::Pointer reader = ReaderType::New();
    // To convert itkImages to vtkImages
    ConnectorType::Pointer connector = ConnectorType::New();

    reader->SetFileName(filename.toStdString().c_str());
    connector->SetInput(reader->GetOutput());
    try {
        connector->Update();

        // Copy the output image because the filters will be removed
        // and we want to keep the image for other vtk pipelines
        oldPointer = connector->GetOutput();
        newImage = vtkSmartPointer<vtkImageData>::New();

        oldPointer->GetDimensions(dims);
        oldPointer->GetExtent(extent);
        oldPointer->GetOrigin(origin);
        oldPointer->GetSpacing(spacing);

        newImage->SetDimensions(dims);
        newImage->SetExtent(extent);
        newImage->SetOrigin(origin);
        newImage->SetSpacing(spacing);

        newImage->DeepCopy(oldPointer);

        // set the original slices. Other orientation are build when/if required only.
    }
    catch (itk::ExceptionObject& err) {
        std::stringstream buffer;
        err.Print(buffer);
        CAMITK_ERROR(tr("ITK exception caught while reading the volume:\n%1\n%2").arg(err.what(), buffer.str().c_str()))
        return NULL;
    }

    oldPointer = NULL;

    return newImage;
}


