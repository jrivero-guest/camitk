/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK include
#include "DicomComponentExtension.h"
#include "DicomParser.h"
#include "DicomDialog.h"
#include "DicomDialogEntry.h"
#include "DicomSerie.h"
#include <Application.h>

// include generated components headers
#include "DicomComponent.h"

// Qt stuff
#include <QFileInfo>

// GDCM includes
#include <gdcmScanner.h>
#include <gdcmIPPSorter.h>

using namespace camitk;

// --------------- GetFileExtensions -------------------
QStringList DicomComponentExtension::getFileExtensions() const {
    QStringList ext;
    ext << "[directory]";

    return ext;
}

// --------------- Open -------------------
Component* DicomComponentExtension::open(const QString& path) {

    serieParsed = DicomParser::parseDirectory(path);

    // create a Dialog entry for each series parsed
    foreach (DicomSerie* dicomSerie, serieParsed) {
        DicomDialogEntry* entry = new DicomDialogEntry();
        entry->setSelected(false);
        entry->setAcquisitionDate(dicomSerie->getAcquisitionDate());
        entry->setAcquisitionTime(dicomSerie->getAcquisitionTime());
        entry->setStudyName(dicomSerie->getStudyName());
        entry->setSerieName(dicomSerie->getSerieName());
        entry->setSerieDescription(dicomSerie->getSerieDescription());
        entry->setPatientName(dicomSerie->getPatientName());
        serieDialogEntries.append(entry);
    }

    // Prompt the user which series he wishes to open
    QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));
    dialog = new DicomDialog(serieDialogEntries);
    serieDialogEntries = dialog->getSelectedDicomDialogEntries();
    QApplication::restoreOverrideCursor();

    // Open each selected Dicom as a component
    if (!serieDialogEntries.isEmpty()) {
        foreach (DicomDialogEntry* entry, serieDialogEntries) {
            // Find the corresponding Dicom Series in the List
            foreach (DicomSerie* serie, serieParsed) {
                if ((serie->getStudyName() == entry->getStudyName()) &&
                        (serie->getSerieName() == entry->getSerieName())) {
                    lastOpenedComponent = new DicomComponent(serie);
                    break;
                }
                else {
                    delete serie;
                }
            }

        }
    }
    serieDialogEntries.clear();


    return lastOpenedComponent;
}

// --------------- Save --------------------
bool DicomComponentExtension::save(Component* component) const {
    // depending on the components managed by DicomComponentExtension, use
    // component->getPointSet() (for a MeshComponent derived class)
    // or component->getImageData() (for a ImageComponent derived class)
    // and save the data in the managed format in the file component->getFileName()

    return false;
}

// --------------- hasDataDirectory -------------------
bool DicomComponentExtension::hasDataDirectory() const {
    return true;
}

