/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef DICOMDIALOGENTRY_H
#define DICOMDIALOGENTRY_H

// Qt includes
#include <QString>
#include <QDate>

/**
 * @ingroup group_cepimaging_components_dicom
 *
 * @brief This class represents a line in the Dialog box of the DICOM series the user is prompted to open.
 *
 * @note This class is part of the view (MVP design pattern) of the DICOM COMPONENT
 *
 */
class DicomDialogEntry {

public:

    DicomDialogEntry();

    /// Default Destructor
    virtual ~DicomDialogEntry() = default;

    ///@name Getters
    ///@{
    bool isSelected();
    QDate getAcquisitionDate();
    QTime getAcquisitionTime() const;
    QString getStudyName();
    QString getSerieName();
    QString getSerieDescription();
    QString getPatientName();
    ///@}

    ///@name Setters
    ///@{
    void setSelected(bool value);
    void setAcquisitionDate(QDate date);
    void setAcquisitionTime(QTime time);
    void setStudyName(QString name);
    void setSerieDescription(QString name);
    void setSerieName(QString name);
    void setPatientName(QString name);
    ///@}

    /// Get the number of item to display for the corresponding DicomTableWidgetItem class
    /// This is used to set number of Column in the Dialog
    static int numberOfItems() {
        return 5;
    }


private:

    /// Is this SERIES selected for opening?
    bool selected;

    /// Acquisition date of the SERIES.
    QDate acquisitionDate;

    /// Acquisition time of the SERIES.
    QTime acquisitionTime;

    /// Name of the STUDY this SERIES belongs to.
    QString studyName;

    /// Name of this SERIES.
    QString serieName;

    /// Description of this SERIES.
    QString serieDescription;

    /// Name of the patient.
    QString patientName;


};

#endif // DICOMDIALOGENTRY_H

