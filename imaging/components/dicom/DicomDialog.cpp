/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- CamiTK includes
#include <Log.h>
#include <AbortException.h>

#include "DicomDialog.h"
#include "DicomDialogEntry.h"

//--------------- Constructor ---------------------------------
DicomDialog::DicomDialog(QList<DicomDialogEntry*> elements): QDialog() {

    dicomDialogEntries = elements;
    ui.setupUi(this);

    if (elements.isEmpty()) {
        throw camitk::AbortException(tr("No DICOM image files found").toStdString());
    }

    // set table size
    ui.tableWidget->clearContents();
    ui.tableWidget->setRowCount(elements.size());

    int row = 0;
    int column = 0;
    foreach (DicomDialogEntry* item, dicomDialogEntries) {
        column = 0;
        QTableWidgetItem* selectionItem = new QTableWidgetItem("");
        selectionItem->setCheckState(Qt::Unchecked);
        ui.tableWidget->setItem(row, column++, selectionItem);
        ui.tableWidget->setItem(row, column++, new QTableWidgetItem(item->getAcquisitionDate().toString()));
        ui.tableWidget->setItem(row, column++, new QTableWidgetItem(item->getAcquisitionTime().toString(Qt::SystemLocaleShortDate)));
        ui.tableWidget->setItem(row, column++, new QTableWidgetItem(item->getPatientName()));
        ui.tableWidget->setItem(row, column++, new QTableWidgetItem(item->getSerieDescription()));
        ui.tableWidget->setItem(row, column++, new QTableWidgetItem(item->getStudyName()));
        ui.tableWidget->setItem(row, column++, new QTableWidgetItem(item->getSerieName()));
        row++;
    }

    ui.tableWidget->resizeColumnsToContents();

    exec();
}

//--------------- getSelectedDicomDialogEntries ---------------------------------
QList<DicomDialogEntry*> DicomDialog::getSelectedDicomDialogEntries() {

    QList<DicomDialogEntry*> selectedElements;

    int nbOfElements = ui.tableWidget->rowCount();
    for (int row = 0; row < nbOfElements; row++) {
        if (ui.tableWidget->item(row, 0)->checkState()) {
            selectedElements.append(dicomDialogEntries.at(row));
        }
    }

    return selectedElements;
}










