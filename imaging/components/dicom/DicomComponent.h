/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef DICOMCOMPONENT_H
#define DICOMCOMPONENT_H

#include <QObject>

#include <ImageComponent.h>

class DicomSerie;
class vtkGDCMImageReader;

/**
 * @ingroup group_cepimaging_components_dicom
 *
 * @brief
 * DICOM Component represents a volumic image of a DICOM SERIES with its corresponding tags information (as CamiTK properties).
 * This class is part of the MODEL (in the MVP pattern) of the DICOM COMPONENT.
 *
 * @note This component uses GDCM >= 2.2 library.
 */
class DicomComponent : public camitk::ImageComponent {

    Q_OBJECT

public:
    /**
     * Create a new Component that handles DICOM images
     * This method may throw an AbortException if a problem occurs.
     * @param dSerie to the associated DicomSerie
     **/
    DicomComponent(DicomSerie* dSerie);

    /**
     * @brief Get the DicomSerie associated to this component.
     */
    DicomSerie* getSerie() {
        return serie;
    }

    /// Default Destructor
    virtual ~DicomComponent();

private:
    /**
     * @brief The image reader using GDCM that allows to read DICOM image as vtkImageData
     *
     * @note This class allows to make the bridge between GDCM and CamiTK.
     */
    vtkSmartPointer<vtkGDCMImageReader> imageReader;

    /**
     * @brief Update the LUT of the image by reading the good information from the DICOM headers.
     *
     * @note This method would have to be removed as soons as the CamiTK LUT is updated and working.
     */
    void updateLUT();

    /**
     * @brief Read the direct cosines angles from the input image
     * @param fileNames the SERIE slices
     * @return the corresponding ImageOrientation, rounded modulo 90 degrees
     */
    camitk::ImageOrientationHelper::PossibleImageOrientations readDirectCosinesAngle(const std::vector< std::string >& fileNames) const;

    /**
     * @brief Round the cosine input to the closest value between -1, 0 or 1
     * @param value the input cosine value
     * @return the rounded cosine value -1, 0 or 1
     */
    double roundCosine(const double& value) const;

    /**
     * @brief The associated DicomSerie to this component.
     */
    DicomSerie* serie;
};

#endif // DICOMCOMPONENT_H

