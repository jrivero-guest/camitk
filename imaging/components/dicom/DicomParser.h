/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef DICOMPARSER_H
#define DICOMPARSER_H

// CamiTK includes
#include <DicomSerie.h>

// Qt includes
#include <QMap>
#include <QList>
#include <QString>
#include <QCoreApplication>

// C++ includes
#include <cstdlib>
#include <vector>


/**
 * @ingroup group_cepimaging_components_dicom
 *
 * @brief DicomParser allows one to parse a directory of DICOM files looking for studies and series
 *
 * @note This class is part of the model (in the MVP pattern) of the DICOM COMPONENT
 *
 */
class DicomParser {

    Q_DECLARE_TR_FUNCTIONS(DicomParser)

public:

    DicomParser() = default;

    /// Default Destructor
    virtual ~DicomParser() = default;

    /**
     * @brief Parse the input directory for DICOM series.
     * @param directory The input directory to parse DICOM image files.
     * @return A list of DicomSerie, high level CamiTK object which store information of a SERIES
     */
    static QList<DicomSerie*> parseDirectory(const QString& directory);

    /**
     * @brief Retrieve the DICOM image file Z spacing attribute (commonly known as spacing between slices)
     * @param serieFileNames the input DICOM image images to retrieve the Z spacing attribute from.
     * @return the Z spacing value.
     */
    static double getZSpacing(const std::vector<std::string>& serieFileNames);

private:
    /**
     * @brief Convert a simple std list of string into a QList of QString.
     * @param inputList std list of string to convert.
     * @return QList of QString version of the input std list.
     */
    static QList<QString> stdListOfStringToQt(const std::vector< std::string >& inputList);

    /**
     * @brief Convert a QList of QString into a std list of string.
     * @param inputList QList of QString to convert.
     * @return std list of string version of the input Qt list.
     */
    static std::vector< std::string > qtListOfStringToStd(const QList<QString>& inputList);

    /**
     * @brief Retrieve the SERIES acquisition date.
     * @param serieFileNames The filenames belonging to a single series.
     * @return the acquisition date of this SERIES.
     */
    static QDate getAcquisitionDate(const std::vector<std::string>& serieFileNames);

    /**
     * @brief Retrieve the SERIES acquisition time.
     * @param serieFileNames The filenames belonging to a single series.
     * @return the acquisition time of this SERIES.
     */
    static QTime getAcquisitionTime(const std::vector<std::string>& serieFileNames);

    /**
     * @brief Retrieve the patient name information from the input files.
     * @param serieFileNames The filenames belonging to a single series.
     * @return the patient name.
     */
    static QString getPatientName(const std::vector<std::string>& serieFileNames);

    /**
     * @brief Retrieve the series name.
     * @param serieFileNames The filenames belonging to a single series.
     * @return the series name.
     */
    static QString getSerieName(const std::vector<std::string>& serieFileNames);

    /**
     * @brief Retrieve the series description.
     * @param serieFileNames The filenames belonging to a single series.
     * @return the series description.
     */
    static QString getSerieDescription(const std::vector<std::string>& serieFileNames);

    /**
     * @brief Retrieve the study name.
     * @param serieFileNames The filenames belonging to a single series.
     * @return the study name.
     */
    static QString getStudyName(const std::vector<std::string>& serieFileNames);

};

#endif // DICOMPARSER_H

