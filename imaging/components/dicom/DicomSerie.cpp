/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// CamiTK includes
#include "DicomSerie.h"

// --------------- Constructor -------------------
DicomSerie::DicomSerie() {
    acquisitionDate = QDate::currentDate();
    studyName = "NOT FOUND";
    serieName = "NOT FOUND";
    serieDescription = "NOT FOUND";
    patientName = "NOT FOUND";
}

// --------------- Getters -------------------
QDate DicomSerie::getAcquisitionDate() const {
    return acquisitionDate;
}
QTime DicomSerie::getAcquisitionTime() const {
    return acquisitionTime;
}
QString DicomSerie::getStudyName() const {
    return studyName;
}
QString DicomSerie::getSerieName() const {
    return serieName;
}
QString DicomSerie::getSerieDescription() const {
    return serieDescription;
}
QString DicomSerie::getPatientName() const {
    return patientName;
}
QList<QString> DicomSerie::getFileNames() const {
    return fileNames;
}
std::vector<std::string> DicomSerie::getStdFileNames() const {
    return stdFileNames;
}

// --------------- Setters -------------------
void DicomSerie::setAcquisitionDate(QDate date) {
    acquisitionDate = date;
}
void DicomSerie::setAcquisitionTime(QTime time) {
    acquisitionTime = time;
}
void DicomSerie::setStudyName(QString name) {
    studyName = name;
}
void DicomSerie::setSerieName(QString name) {
    serieName = name;
}
void DicomSerie::setSerieDescription(QString name) {
    serieDescription = name;
}
void DicomSerie::setPatientName(QString name) {
    patientName = name;
}
void DicomSerie::setFileNames(QList<QString> inputFileNames) {
    fileNames = inputFileNames;
}
void DicomSerie::setStdFileNames(std::vector<std::string> inputFileNames) {
    stdFileNames = inputFileNames;
}













