camitk_extension(COMPONENT_EXTENSION
                 NEEDS_GDCM
                 CEP_NAME CEP_IMAGING
                 DESCRIPTION "This allows one to read a single DICOM image file (.dcm) using the library GDCM 2.2.x"
                 INSTALL_ALL_HEADERS
                 ENABLE_AUTO_TEST
)

