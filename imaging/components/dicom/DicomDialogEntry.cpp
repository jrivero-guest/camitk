/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// CamiTK includes
#include "DicomDialogEntry.h"


// --------------- Constructor -------------------
DicomDialogEntry::DicomDialogEntry() {
    acquisitionDate = QDate::currentDate();
    studyName = "NOT FOUND";
    serieName = "NOT FOUND";
    patientName = "NOT FOUND";
}

// --------------- Getters -------------------
bool DicomDialogEntry::isSelected() {
    return selected;
}
QDate DicomDialogEntry::getAcquisitionDate() {
    return acquisitionDate;
}
QTime DicomDialogEntry::getAcquisitionTime() const {
    return acquisitionTime;
}
QString DicomDialogEntry::getStudyName() {
    return studyName;
}
QString DicomDialogEntry::getSerieName() {
    return serieName;
}
QString DicomDialogEntry::getSerieDescription() {
    return serieDescription;
}
QString DicomDialogEntry::getPatientName() {
    return patientName;
}
///@}

// --------------- Setters -------------------
void DicomDialogEntry::setSelected(bool value) {
    selected = value;
}
void DicomDialogEntry::setAcquisitionDate(QDate date) {
    acquisitionDate = date;
}
void DicomDialogEntry::setAcquisitionTime(QTime time) {
    acquisitionTime = time;
}
void DicomDialogEntry::setStudyName(QString name) {
    studyName = name;
}
void DicomDialogEntry::setSerieName(QString name) {
    serieName = name;
}
void DicomDialogEntry::setSerieDescription(QString name) {
    serieDescription = name;
}
void DicomDialogEntry::setPatientName(QString name) {
    patientName = name;
}
