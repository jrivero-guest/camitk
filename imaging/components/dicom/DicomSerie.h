/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef DICOMSERIE_H
#define DICOMSERIE_H

// Qt includes
#include <QString>
#include <QDate>

// C++ includes
#include <vector>


/**
 * @ingroup group_cepimaging_components_dicom
 *
 * @brief DicomSerie is a CamiTK high level class to represent a DICOM SERIES
 * This class encapsulates GDCM objects to handle a DICOM SERIES
 *
 * @note This class is part of the model (in the MVP pattern) of the DICOM COMPONENT
 * @note This class does NOT represent a CamiTK Component.
 *
 * @see DicomComponent
 *
 */
class DicomSerie {

public:

    DicomSerie();

    /// Default Destructor
    virtual ~DicomSerie() = default;

    ///@name Getters
    ///@{
    QDate getAcquisitionDate() const;
    QTime getAcquisitionTime() const;
    QString getStudyName() const;
    QString getSerieName() const;
    QString getSerieDescription() const;
    QString getPatientName() const;
    QList<QString> getFileNames() const;
    std::vector<std::string> getStdFileNames() const;
    ///@}

    ///@name Setters
    ///@{
    void setAcquisitionDate(QDate date);
    void setAcquisitionTime(QTime time);
    void setStudyName(QString name);
    void setSerieName(QString name);
    void setSerieDescription(QString name);
    void setPatientName(QString name);
    void setFileNames(QList<QString> inputFileNames);
    void setStdFileNames(std::vector<std::string> inputFileNames);
    ///@}


private:

    /// Acquisition date of the SERIES.
    QDate acquisitionDate;

    /// Acquisition time of the SERIES.
    QTime acquisitionTime;

    /// Name of the STUDY this SERIES belongs to.
    QString studyName;

    /// Name of this SERIES.
    QString serieName;

    /// Description of this SERIES (sometime more readable than the SERIES name itself).
    QString serieDescription;

    /// Name of the patient.
    QString patientName;

    /// The image file names associated to this SERIES
    QList<QString> fileNames;

    /// The image file names associated to this SERIES (std list, for GDCM API)
    std::vector<std::string> stdFileNames;
};

#endif // DICOMSERIE_H

