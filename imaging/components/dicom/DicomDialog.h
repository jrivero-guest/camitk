/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef DICOMDIALOG_H
#define DICOMDIALOG_H

// -- CamiTK includes
#include "ui_DicomDialog.h"

// Camitk forward declaration
class DicomDialogEntry;

// -- QT includes
#include <QDialog>
#include <QList>

/**
 * @ingroup group_cepimaging_components_dicom
 *
 * @brief
 * Dialog class to select the DICOM series to open, using a GUI.
 *
 **/
class DicomDialog : public QDialog {
    Q_OBJECT

public:
    /**Default Constructor*/
    DicomDialog(QList<DicomDialogEntry*>);
    /**Destructor*/
    ~DicomDialog() override = default;

    /// Get the list of DicomDialogEntry selected for opening
    QList<DicomDialogEntry*> getSelectedDicomDialogEntries();

private:
    // Qt model designed by QtDesigner (with qt4, nor more inheritance from the ui)
    Ui::Dialog ui;

    /// Elements to display for selection
    QList<DicomDialogEntry*> dicomDialogEntries;

} ;
#endif // DICOMDIALOG_H


