#!/bin/bash
# ---------------------- exampleComponents.xml ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/exampleComponents.xml
cat <<EOF > exampleComponents.xml
<?xml version="1.0" encoding="UTF-8"?>
<cep
    xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
    xmlns='http://camitk.imag.fr/cepcoreschema'
    xsi:schemaLocation='http://camitk.imag.fr/cepcoreschema ../Cep.xsd'>
    <name>Example Of Tinman With Components</name>
    <contact>
        <email>Celine.Fouard@imag.fr</email>
    </contact>
    <description>Tinman Test file containing several kinds of Component Extensions.</description>
    <copyright><![CDATA[/* ****************************************************************************
$USERDEF_LICENCE_BEGIN$
CamiTK - Computer Assisted Medical Intervention ToolKit
(c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
        
Visit http://camitk.imag.fr for more information
        
This file is part of CamiTK.
        
$CEP_NAME$ is under the following licence:
A specific private licence, just to test Tinman licence managing
        
$USERDEF_LICENCE_END$
*************************************************************************** */
    ]]></copyright>
    
    <componentExtensions>
        <componentExtension>
            <name>Example with no file suffix</name>
            <description>
                This component tests the ability of Tinman to create and make 
                compilable and executable component extensions which do not
                handle any file suffix (components should then be created from actions).
            </description>
            <components>
                <component>
                    <name>my own cube</name>
                    <description>A Mesh Cube</description>
                    <representation>Mesh</representation>
                    <properties>
                        <parameter name="size"  type="int" defaultValue="5" description="An integer."/>
                        <parameter name="color" type="QColor" description="A QColor."/>
                    </properties>
                </component>
            </components>
        </componentExtension>
        <componentExtension>
            <name>Example with one file suffix</name>
            <description>Simple Mixed component extension handling .example files</description>
            <components>
                <component>
                    <name>Tinman Example File</name>
                    <description>
                        Simple component handling .example files with no representation.
                    </description>
                    <representation>None</representation>
                    <fileSuffix>example</fileSuffix>
                </component>
            </components>
        </componentExtension>

        <componentExtension>
            <name>Example with several file suffixes</name>
            <description>Simple Mixed component extension handling .example files</description>
            <components>
                <component>
                    <name>Tinman A File</name>
                    <description>
                        Simple component handling .a files with an image representation.
                    </description>
                    <representation>Image</representation>
                    <properties>
                        <parameter name="aa"  type="int" defaultValue="2" description="An integer."/>
                    </properties>
                    <fileSuffix>a</fileSuffix>
                </component>
                <component>
                    <name>Tinman B File</name>
                    <description>
                        Simple component handling .b files with a Mesh representation.
                    </description>
                    <representation>Image</representation>
                    <properties>
                        <parameter name="bb"  type="bool" description="A boolean."/>
                    </properties>
                    <fileSuffix>b</fileSuffix>
                </component>
                <component>
                    <name>Tinman C File</name>
                    <description>
                        Simple component handling .c files with no representation.
                    </description>
                    <representation>None</representation>
                    <properties>
                        <parameter name="cc"  type="double" defaultValue="3.8" description="A double."/>
                    </properties>
                    <fileSuffix>c</fileSuffix>
                </component>
            </components>
        </componentExtension>
    </componentExtensions>
</cep>
EOF


# ---------------------- actionsExamplesLicence.xml ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/actionsExamplesLicence.xml
cat <<EOF > actionsExamplesLicence.xml
<?xml version="1.0" encoding="UTF-8"?>
<cep    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://camitk.imag.fr/cepcoreschema"
        xsi:schemaLocation="http://camitk.imag.fr/cepcoreschema ../cep.xsd">
    <name>CEP created by Wizard</name>
    <contact>
        <email>Celine.Fouard@imag.fr</email> 
    </contact>
    <description>Example of empty CEP</description>
    <copyright><![CDATA[/*****************************************************************************
$USERDEF_LICENCE_BEGIN$
CamiTK - Computer Assisted Medical Intervention ToolKit
(c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
        
Visit http://camitk.imag.fr for more information
        
This file is part of CamiTK.
        
$CEP_NAME$ is under the following licence:
A specific private licence
$USERDEF_LICENCE_END$
****************************************************************************/
    ]]></copyright>
    <actionExtensions>
        <actionExtension>
            <name>Wizard created image actions</name>
            <description>This action extension proposes several actions on image components</description>
            <!-- <directoryName>imagewizardactions</directoryName> -->
            <actions>
                <action>
                    <name>Example of Image Action</name>
                    <description>Example of simple action on image component.</description>
                    <component>ImageComponent</component>
                    <classification>
                        <family>CreatedByWizard</family>
                        <tag>Image</tag>
                    </classification>
                    <parameters>
                        <parameter name="alpha" type="int" description="An integer."/>
                        <parameter name="beta"  type="bool" description="A boolean."/>
                    </parameters>
                </action>
                <action>
                    <name>Example of Image ITK Filter</name>
                    <description>Example of an itk image action.</description>
                    <component>ImageComponent</component>
                    <classification>
                        <family>CreatedByWizard</family>
                        <tag>Image</tag>
                        <tag>itk</tag>
                        <itkFilter outputType="Same as Input"/>
                    </classification>
                    <parameters>
                        <parameter name="gamma" type="QDate" description="A QDate."/>
                        <parameter name="phi"  type="double" description="A double."/>
                    </parameters>
                </action>
            </actions>
            <dependencies>
                <dependency type="library" name="itk"/>
            </dependencies>
        </actionExtension>
        <actionExtension>
            <name>Wizard created mesh actions</name>
            <description>This action extension proposes an action on mesh components</description>
            <actions>
                <action>
                    <name>Example of Mesh Action</name>
                    <description>Example of action on a mesh.</description>
                    <component>MeshComponent</component>
                    <classification>
                        <family>CreatedByWizard</family>
                        <tag>Mesh</tag>
                        <tag>Example</tag>
                    </classification>
                    <parameters>
                        <parameter name="A long parameter name" type="QColor" description="A QColor."/>
                    </parameters>
                </action>
            </actions>
        </actionExtension>
    </actionExtensions>
    
</cep>
EOF


# ---------------------- actionsExamplesNoLicence.xml ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/actionsExamplesNoLicence.xml
cat <<EOF > actionsExamplesNoLicence.xml
<?xml version="1.0" encoding="UTF-8"?>
<cep xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://camitk.imag.fr/cepcoreschema"
    xsi:schemaLocation="http://camitk.imag.fr/cepcoreschema ../cep.xsd">
    <name>CEP created by Wizard</name>
    <contact>
        <email>Celine.Fouard@imag.fr</email> 
    </contact>
    <description>Example of empty CEP</description>
    <actionExtensions>
        <actionExtension>
            <name>Wizard created image actions</name>
            <description>This action extension proposes several actions on image components</description>
            <!-- <directoryName>imagewizardactions</directoryName> -->
            <actions>
                <action>
                    <name>Example of Image Action</name>
                    <description>Example of simple action on image component.</description>
                    <component>ImageComponent</component>
                    <classification>
                        <family>CreatedByWizard</family>
                        <tag>Image</tag>
                    </classification>
                    <parameters>
                        <parameter name="alpha" type="int" description="An integer."/>
                        <parameter name="beta"  type="bool" description="A boolean parameter."/>
                    </parameters>
                </action>
                <action>
                    <name>Example of Image ITK Filter</name>
                    <description>Example of an itk image action.</description>
                    <component>ImageComponent</component>
                    <classification>
                        <family>CreatedByWizard</family>
                        <tag>Image</tag>
                        <tag>itk</tag>
                        <itkFilter outputType="Same as Input"/>
                    </classification>
                    <parameters>
                        <parameter name="gamma" type="QDate" description="Using class QDate as parameter type"/>
                        <parameter name="phi"  type="double" description="Phi is &lt;i>another parameter&lt;/i>" />
                    </parameters>
                </action>
            </actions>
            <dependencies>
                <dependency type="library" name="itk"/>
            </dependencies>
        </actionExtension>
        <actionExtension>
            <name>Wizard created mesh actions</name>
            <description>This action extension proposes an action on mesh components</description>
            <actions>
                <action>
                    <name>Example of Mesh Action</name>
                    <description>Example of action on a mesh.</description>
                    <component>MeshComponent</component>
                    <classification>
                        <family>CreatedByWizard</family>
                        <tag>Mesh</tag>
                        <tag>Example</tag>
                    </classification>
                    <parameters>
                        <parameter name="A long parameter name" type="QColor" description="This is a very long description&lt;br/>(for a very long parameter name)"/>
                    </parameters>
                </action>
            </actions>
        </actionExtension>
    </actionExtensions>
    
</cep>
EOF


# ---------------------- actionAndComponent.xml ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/actionAndComponent.xml
cat <<EOF > actionAndComponent.xml
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<cep xmlns="http://camitk.imag.fr/cepcoreschema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://camitk.imag.fr/cepcoreschema Cep.xsd">
  <name>Yet another example of CEP</name>
  <contact>
    <email>Emmanuel.Promayon@imag.fr</email>
  </contact>
  <description>This CEP contains a new type of component and an action that can do something with it.
This test the link between an action and a component defined in the same CEP.</description>
  <actionExtensions>
    <actionExtension>
      <name>Another Example Of An Action Extension</name>
      <description>This extension description is really useless.</description>
      <actions>
        <action>
          <name>DoSomethingOnSpecific</name>
          <description>This description is void.</description>
          <component>SpecificMeshBasedComponent</component>
          <classification>
            <family>Testing CepGenerator</family>
          </classification>
          <parameters>
            <parameter defaultValue="100.0" description="Testing double" editable="true" name="testDouble" type="double" unit=""/>
          </parameters>
        </action>
        <action>
          <name>OnlyOneVec3D</name>
          <description>Check compilation of an action with only a QVector3D parameter (check bug#189).</description>
          <component></component>
          <classification>
            <family>Testing CepGenerator</family>
          </classification>
          <parameters>
            <parameter defaultValue="QVector3D(0.0, 0.0, 0.0)" description="test 3D vector" editable="true" name="testVector3D" type="QVector3D" unit=""/>
          </parameters>
        </action>        
      </actions>
      <dependencies>
        <dependency name="specificcomponent" type="component"/>
      </dependencies>
    </actionExtension>
  </actionExtensions>
  <componentExtensions>
    <componentExtension>
      <name>Specific component</name>
      <description>This extension description a new / specific component</description>
      <components>
        <component>
          <name>SpecificMeshBasedComponent</name>
          <description>My description is useless...</description>
          <representation>Mesh</representation>
          <properties>
            <parameter defaultValue="false" description="Testing bools" editable="true" name="testBool" type="bool" unit=""/>
            <parameter defaultValue="QColor(255, 255, 255, 255)" description="Testing color" editable="true" name="testColor" type="QColor" unit=""/>
            <parameter defaultValue="QVector3D(0.0, 0.0, 0.0)" description="test 3D vector" editable="true" name="testVector3D" type="QVector3D" unit=""/>
          </properties>
          <fileSuffix>specific</fileSuffix>
        </component>
      </components>
    </componentExtension>
  </componentExtensions>
</cep>
EOF

# ---------------------- empty.xml ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/empty.xml
cat <<EOF > empty.xml
<?xml version="1.0" encoding="UTF-8"?>
<cep xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://camitk.imag.fr/cepcoreschema"
        xsi:schemaLocation="http://camitk.imag.fr/cepcoreschema ../cep.xsd">
    <name>empty</name>
    <contact>
        <email>Celine.Fouard@imag.fr</email> 
        <email>Emmanuel.Promayon@imag.fr</email>
    </contact>
    <description>Example of empty CEP</description>    
</cep>
EOF

# ---------------------- completeTest1.xml ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/complete-test-1.xml
cat <<EOF > completeTest1.xml
<?xml version="1.0" encoding="utf-8"?>
<cep xmlns="http://camitk.imag.fr/cepcoreschema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://camitk.imag.fr/cepcoreschema Cep.xsd">
  <name>Complete test CEP 1</name>
  <contact>
    <email>timc-camitk@univ-grenoble-alpes.fr</email>
  </contact>
  <description>This CEP is trying to achieve a complete test of all possible cepcoreschema element</description>
  <readme>This CEP is trying to achieve a complete test of all possible cepcoreschema element. Probably some specific cases are not included, but it is our best shot</readme>
  <copyright><![CDATA[/*****************************************************************************
$USERDEF_LICENCE_BEGIN$
CamiTK - Computer Assisted Medical Intervention ToolKit
(c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
        
Visit http://camitk.imag.fr for more information
        
This file is part of CamiTK.
        
$CEP_NAME$ is under the following licence:
This is the complete licence, it covers everything, everywhere, for everyone at everytime.
Please consider thatthis is just an example...
$USERDEF_LICENCE_END$
****************************************************************************/
    ]]></copyright>
  <actionExtensions>
    <actionExtension>
      <name>Test Action Extension 1</name>
      <description>Test the first action extension</description>
      <actions>
        <action>
          <name>Test Action 1</name>
          <description>Test a first action, for image component, with a lot of parameters</description>
          <component>ImageComponent</component>
          <classification>
            <family>Complete Test Actions #1</family>
            <tag>Test</tag>
            <tag>Action #1</tag>
            <tag>ImageComponent</tag>
            <itkFilter outputType="Same as Input" />
          </classification>
          <parameters>
            <parameter name="p1" type="int" description="This is p1" unit="unit 1" defaultValue="42" editable="true" />
            <parameter name="p2" type="bool" description="This is p2" unit="unit 2" defaultValue="true" editable="true" />
            <parameter name="p3" type="double" description="This is p3" unit="unit 3" defaultValue="42.42" editable="true" />
            <parameter name="p4" type="QString" description="This is p4" unit="unit 4" defaultValue="Default value" editable="true" />
            <parameter name="p5" type="QDate" description="This is p5" unit="unit 5" defaultValue="QDate(1952,3,11)" editable="true" />
            <parameter name="p6" type="QColor" description="This is p6" unit="unit 6" defaultValue='QColor("gold")' editable="true" />
            <parameter name="p7" type="QPoint" description="This is p7" unit="unit 7" defaultValue="QPoint(42,84)" editable="true" />
            <parameter name="p8" type="QPointF" description="This is p8" unit="unit 8" defaultValue="QPointF(42.42,84.84)" editable="true" />
            <parameter name="p9" type="QVector3D" description="This is p9" unit="unit 9" defaultValue="QVariant().value&lt;QVector3D>()" editable="true" />
            <parameter name="p10" type="QVector4D" description="This is p10" unit="unit 10" defaultValue="QVariant().value&lt;QVector4D>()" editable="true" />
            <parameter name="p11" type="QTime" description="This is p11" unit="unit 11" defaultValue="QTime(23,58)" editable="true" />
          </parameters>
          <properties modal="false" embedded="true" delayed="false" />
        </action>
        <action>
          <name>Test Action 2</name>
          <description>Test a second action, with no tag, no component, and with just one non editable parameters</description>
          <component></component>
          <classification>
            <family>Complete Test Actions #1</family>
            <!-- no tag... -->
          </classification>
          <parameters>
              <parameter name="No Tag Action" type="bool" description="A &lt;b>small&lt;/b> description with &lt;i>hyper text&lt;i>" unit="€" defaultValue="false" editable="false"/>
          </parameters>
          <properties modal="true" embedded="false" delayed="false" />
        </action>
        <action>
          <name>Test Action 3</name>
          <description>Test a third action, with non existing \"Mesh Component\", and with lots of parameters and defautl editable</description>
          <component></component>
          <classification>
            <family>Complete Test Actions #1</family>
            <tag>Mesh Component</tag>
          </classification>
          <parameters>
            <parameter name="p1" type="int" description="This is p1" unit="unit 1" defaultValue="42"/>
            <parameter name="p2" type="bool" description="This is p2" unit="unit 2" defaultValue="true"/>
            <parameter name="p3" type="double" description="This is p3" unit="unit 3" defaultValue="42.42"/>
            <parameter name="p4" type="QString" description="This is p4" unit="unit 4" defaultValue="Default value"/>
            <parameter name="p5" type="QDate" description="This is p5" unit="unit 5" defaultValue="QDate(1952,3,11)"/>
            <parameter name="p6" type="QColor" description="This is p6" unit="unit 6" defaultValue='QColor("gold")'/>
            <parameter name="p7" type="QPoint" description="This is p7" unit="unit 7" defaultValue="QPoint(42,84)"/>
            <parameter name="p8" type="QPointF" description="This is p8" unit="unit 8" defaultValue="QPointF(42.42,84.84)"/>
            <parameter name="p9" type="QVector3D" description="This is p9" unit="unit 9" defaultValue="QVariant().value&lt;QVector3D>()"/>
            <parameter name="p10" type="QVector4D" description="This is p10" unit="unit 10" defaultValue="QVariant().value&lt;QVector4D>()"/>
            <parameter name="p11" type="QTime" description="This is p11" unit="unit 11" defaultValue="QTime(23,58)" editable="true" />
          </parameters>
          <properties modal="true" embedded="false" delayed="false" />
        </action>
      </actions>
      <dependencies>
        <dependency type="library" name="itk"/>
        <dependency type="cepLibrary" name="testlib"/>
      </dependencies>
    </actionExtension>
    <actionExtension>
      <name>Test Action Extension 2</name>
      <description>Test the second action extension</description>
      <actions>
        <action>
          <name>Test Action 4</name>
          <description>This action use the Test Component 1</description>
          <component>TestComponent1</component>
          <classification>
            <family>Complete Test Actions #2</family>
            <tag>Test</tag>
            <tag>Action #2</tag>
            <tag>Test Component 1</tag>
          </classification>
          <parameters>
            <parameter name="message" type="QString" description="This is a message parameter" unit="none" defaultValue="hello!" editable="true" />
          </parameters>
          <properties modal="false" embedded="true" delayed="false" />
        </action>
      </actions>
      <dependencies>
        <dependency type="component" name="testcomponentextension1"/>
      </dependencies>
    </actionExtension>
  </actionExtensions>
  <applications>
    <application>
      <name>testapp</name>
      <description>Test application</description>
      <directoryName>testappdir</directoryName>
    </application>
  </applications>
  <componentExtensions>
    <componentExtension>
      <name>Test Component Extension 1</name>
      <description>Test the first component extension</description>
      <components>
        <component>
          <name>Test Component 1</name>
          <description>Test a first action with an image representation</description>
          <representation>Image</representation>
          <properties>
            <parameter name="p1" type="int" description="This is p1" unit="unit 1" defaultValue="42" editable="true" />
            <parameter name="p2" type="bool" description="This is p2" unit="unit 2" defaultValue="true" editable="true" />
            <parameter name="p3" type="double" description="This is p3" unit="unit 3" defaultValue="42.42" editable="true" />
            <parameter name="p4" type="QString" description="This is p4" unit="unit 4" defaultValue="Default value" editable="true" />
            <parameter name="p5" type="QDate" description="This is p5" unit="unit 5" defaultValue="QDate(1952,3,11)" editable="true" />
            <parameter name="p6" type="QColor" description="This is p6" unit="unit 6" defaultValue='QColor("gold")' editable="true" />
            <parameter name="p7" type="QPoint" description="This is p7" unit="unit 7" defaultValue="QPoint(42,84)" editable="true" />
            <parameter name="p8" type="QPointF" description="This is p8" unit="unit 8" defaultValue="QPointF(42.42,84.84)" editable="true" />
            <parameter name="p9" type="QVector3D" description="This is p9" unit="unit 9" defaultValue="QVariant().value&lt;QVector3D>()" editable="true" />
            <parameter name="p10" type="QVector4D" description="This is p10" unit="unit 10" defaultValue="QVariant().value&lt;QVector4D>()" editable="true" />
            <parameter name="p11" type="QTime" description="This is p11" unit="unit 11" defaultValue="QTime(23,58)" editable="true" />
          </properties>
          <fileSuffix>test</fileSuffix>
        </component>
      </components>
      <dependencies>
        <dependency type="cepLibrary" name="testlib"/>
      </dependencies>
    </componentExtension>
  </componentExtensions>
  <libraries>
      <library static="false">
      <name>testlib</name>
      <description>A simple test lib. This lib will not compile unless some minimal C++ code is present in the generated source. Copy TestLib.h and TestLib.cpp to the library directory before configuration time.</description>
      <dependencies>
        <dependency type="library" name="XSD"/>
        <dependency type="library" name="ITK"/>
        <dependency type="cepLibrary" name="qtpropertybrowser"/>
      </dependencies>
    </library>
  </libraries>
</cep>
EOF
