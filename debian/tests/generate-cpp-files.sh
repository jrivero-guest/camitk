#!/bin/bash

# ---------------------- TestLib.h ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/TestLib.h
cat <<EOF > TestLib.h
#ifndef TESTLIB_H
#define TESTLIB_H

// A simple test library
class TestLib {

public:
    static void test();
};

#endif // TESTLIB_H
EOF

# ---------------------- TestLib.cpp ----------------------
# Example taken from distributed source ./sdk/libraries/cepcoreschema/testdata/TestLib.cpp
cat <<EOF > TestLib.cpp
#include "TestLib.h"

#include <iostream>

// --------------- test -------------------
void TestLib::test() {
    std::cout << "Test output" << std::endl;
}
EOF
