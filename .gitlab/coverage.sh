#!/bin/bash

echo "Job $CI_JOB_NAME"

if ! grep -q TRIGGER_STAGE_COVERAGE "${PROJECT_LOG_DIR}/trigger-stage.txt"; then
    echo "Job skipped as /coverage flag not in commit message and CAMITK_CI_STAGE < $STAGE_CONFIGURE"; 
    exit 1;
fi

echo "===== coverage ====="

# coverage for some reason seems to always exit with non-zero code. Force exit 0
xvfb-run --auto-servernum --server-args="-screen 0 1024x768x24" \
    ctest   -VV \
            -DCTEST_SITE="$CDASH_SITE" \
            -DCI_MODE="$CAMITK_CI_MODE" \
            -DCI_ID="P $CI_PIPELINE_ID - J $CI_BUILD_ID" \
            -DCI_BRANCH="$CI_COMMIT_REF_NAME" \
            -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
            -DCTEST_SOURCE_DIRECTORY="$PROJECT_SOURCE_DIR" \
            -DCTEST_BINARY_DIRECTORY="$PROJECT_BUILD_DIR" \
            -S $PROJECT_SOURCE_DIR/sdk/cmake/ctest/ci-coverage.cmake > >(tee ${PROJECT_LOG_DIR}/coverage.log | grep --line-buffered -e "   produced s") 2>&1 && echo "[OK] $?" || echo "[FAIL] $?"

# move file to a the log directory (so that it can be uploaded as an artifact)
echo "Copy html coverage report to ${PROJECT_LOG_DIR}"
cp -pR ${PROJECT_BUILD_DIR}/camitk-ce-test-coverage ${PROJECT_LOG_DIR}

echo "Coverage report"
grep "  lines" ${PROJECT_LOG_DIR}/coverage.log
grep "  functions" ${PROJECT_LOG_DIR}/coverage.log
