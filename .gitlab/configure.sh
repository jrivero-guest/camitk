#!/bin/bash
# Uncomment next line to print each bash command before it is executed
#set -x

echo "Job $CI_JOB_NAME"

if ! grep -q TRIGGER_STAGE_CONFIGURE "${PROJECT_LOG_DIR}/trigger-stage.txt"; then
    echo "Job skipped as /configure flag not in commit message and CAMITK_CI_STAGE < $STAGE_CONFIGURE"; 
    exit 1;
fi

echo "===== configure ====="

# Clean build directory
# note: cannot build outside the source tree otherwise artifacts cannot be collected
if [ "$CAMITK_CI_MODE" == "Nightly" ]; then 
    rm -rf ${PROJECT_BUILD_DIR}; 
    echo "Nightly Build, start from scratch"; 
fi

if [ "$OS" == "win7" ]; then
    # there is no xvfb on windows
    ctest   -VV \
            -DCTEST_SITE="$CDASH_SITE" \
            -DCI_MODE="$CAMITK_CI_MODE" \
            -DCI_ID="P $CI_PIPELINE_ID - J $CI_BUILD_ID" \
            -DCI_BRANCH="$CI_COMMIT_REF_NAME" \
            -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
            -DCTEST_SOURCE_DIRECTORY="$PROJECT_SOURCE_DIR" \
            -DCTEST_BINARY_DIRECTORY="$PROJECT_BUILD_DIR" \
            -S $PROJECT_SOURCE_DIR/sdk/cmake/ctest/ci-configure.cmake > >(tee ${PROJECT_LOG_DIR}/configure.log) 2>&1
else
    # on Linux, xvfb is required to run the tests
    xvfb-run --auto-servernum --server-args="-screen 0 1024x768x24" \
        ctest   -VV \
            -DCTEST_SITE="$CDASH_SITE" \
            -DCI_MODE="$CAMITK_CI_MODE" \
            -DCI_ID="P $CI_PIPELINE_ID - J $CI_BUILD_ID" \
            -DCI_BRANCH="$CI_COMMIT_REF_NAME" \
            -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
            -DCTEST_SOURCE_DIRECTORY="$PROJECT_SOURCE_DIR" \
            -DCTEST_BINARY_DIRECTORY="$PROJECT_BUILD_DIR" \
            -S $PROJECT_SOURCE_DIR/sdk/cmake/ctest/ci-configure.cmake > >(tee ${PROJECT_LOG_DIR}/configure.log) 2>&1

fi
