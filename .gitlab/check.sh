#!/bin/bash

echo "Job $CI_JOB_NAME"

if [ "$TRIGGER_STAGE_CHECK" == "false" ]; then 
    echo "Job skipped as /check flag not in commit message and CAMITK_CI_STAGE < $STAGE_CHECK"; 
    exit 1; 
fi

pwd
printenv
