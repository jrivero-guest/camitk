// The following content describes the index.html page of the API documentation.
// It uses CMake variable (surrounded by the '@' pattern), replaced by their content using the CONFIGURE_FILE CMake command
// This file is configured in camitk/cmake/modules/CamiTKApiDoc.cmake into the CamiTKVersion.h file.

/** @mainpage CamiTK API Documentation
 *
 * @section introduction Introduction
 * Welcome to the Computer Assisted Medical Intervention Tool kit (CamiTK) API Documentation.
 * CamiTK is an open-source framework that helps researchers and clinicians to easily and rapidly collaborate in order to prototype CAMI applications, that feature medical images, surgical navigation and biomechanical simulations. CamiTK uses famous tools such as Qt, ITK and VTK.
 * This API documentation would introduce you to the architecture of the framework
 *
 *
 *
 * @subsection design-pattern Design Pattern
 * CamiTK core architecture follows the Model-View-Presenter (MVP) design-pattern. It helps dividing the logic in two parts : domain logic (data structures and algorithms) and application logic (the Model, GUI aspects to present and interact with the domain logic).@section architecture Core Architecture
 * CamiTK follows the Components Based Software Engineering (CBSE) principles. Each new functionality to the framework is a new module represented as a dynamic library (.dll on windows, .so on Linux systems). These principles guarantee CamiTK to be higly modular and easier to maintain.
 * CamiTK is provided with its core and basic modules (to handle 2D and 3D images using VTK for instance). You can easily add new modules to the framework or create new camitk-application.
 *
 * \image html camitk_service_layer_1.png "The service layer" width=10cm
 *
 * To do so, 'viewers' (application logic modules) are used to present and interact with the domain logic : the 'components' (the data logic, the data structures) and the 'actions' (the processing logic, the algorithms to be used on these components).
 * In order to easily glue together all this modules CamiTK uses a Service Layer Pattern. This service layer can be seen as a contract from the viewers consumers of services provided by the 'components' and 'actions'. The viewers interact with the components without knowing exactly which kind of data it represents.
 *
 * @subsection components Components
 *
 * \image html camitk_service_layer_2.png "The four interfaces implemented by the Component class" width=10cm
 *
 * CamiTK components describe data structure and therefore are used to represent the data logic. The @ref camitk::Component "Component" class implements four interfaces, which gather the different possibilities of a component in order to provide the service layer. See the @ref camitk::Component "Component" class to have more details.
 *
 * @subsection actions Actions
 * The @ref camitk::Action "Action" class generalizes the notion of component processing. An action applies a given algorithm to one or several listed components.
 * Generally viewers trigger actions on the currently selected components.
 *
 * @subsection viewers Viewers
 * The @ref camitk::Viewer "Viewer" class handles all the presentation and interaction logic (the View and Presenter part of the MVP). The CamiTK viewers are using either Qt widget or VTK renderer but are not limited to that. Viewers manage the user interactions and map them into calls to the interface methods of the components.
 *
 * @section getting-started Getting started
 *
 * @subsection how-to-extend-camitk How to add a new action or component ?
 * To add a new action or component to CamiTK, you will need to create a new dynamic link library that extends the framework with your desired data structure and / or algorithms. You may want to use the CamiTK wizard to automatically generate a code skeleton for your extension. Please check the wiki documentation about <a href="https://forge.imag.fr/plugins/mediawiki/wiki/camitk/index.php/Create_your_own_custom_action_for_CamiTK">adding a new component / action</a>.
 *
 * @subsection how-to-create-new-application How to create a new application ?
 * You may inherits the @ref camitk::MainWindow "MainWindow" class. The MainWindow class is a kind of super presenter : it instanciates and organizes all the viewers in an application GUI. Check the wiki tutorial : <a href="https://forge.imag.fr/plugins/mediawiki/wiki/camitk/index.php/Create_your_own_application_for_CamiTK">create a new applicaiton</a>.
 *
 * @subsection tutorials Tutorials
 * Many tutorials are proposed in the <a href="https://forge.imag.fr/plugins/mediawiki/wiki/camitk/index.php/Main_Page">CamiTK wiki</a> (you may want to edit it and share your knowledge).
 *
 * @section HomePage Home Page
 * The home page of the project can be found at : <a href="http://camitk.imag.fr">camitk.imag.fr</a>
 *
 * @section copyright (c) Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 */



