// Here are the related pages of the documention
/**
 * @page home-page CamiTK website
 * The home page of the project can be found at : <a href="http://camitk.imag.fr">camitk.imag.fr</a>
 *
 * @page wiki CamiTK wiki
 * Installation instructions, many tips and tutorials are proposed in the <a href="https://forge.imag.fr/plugins/mediawiki/wiki/camitk/index.php/Main_Page">CamiTK wiki</a> (you may want to edit it and share your knowledge).
 *
 * \namespace CMake
 *
 *  \brief A pseudo-namespace used to group CMake macros and functions.
 *
 *  This is not a regular C++ namespace. It is just used to group the CMake
 *  macros and function of the CamiTK build system.
 *
 * @page CamiTKBuildSystem The CamiTK Build System (based on CMake Macros)
 *
 **/