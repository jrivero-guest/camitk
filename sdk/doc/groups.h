// Doxygen SDK groups creation
/**
 * @defgroup group_sdk CamiTK SDK
 * The CamiTK SDK, required by any other CamiTK Extension Projects (CEP).
 * The CamiTK SDK features the @ref camitk::Core and several extensions (actions, components, applications, viewers and libraries).
 *
 *
 *
 *
 * @defgroup group_sdk_actions Actions
 * @ingroup group_sdk
 * The actions of the CamiTK SDK.
 * The @ref camitk::Action "Action" class generalizes the notion of component processing. An action applies a given algorithm to one or several listed components.
 * Generally viewers trigger actions on the currently selected components.
 *
 * @defgroup group_sdk_actions_application Application
 * @ingroup group_sdk_actions
 * The basic actions of the CamiTK SDK.
 * The @ref camitk::Action application features actions used at aplication level (these actions are mandatory for any CamiTK appliction to work)
 * \image html actions/action_application.png "Basic handle your component through the contextual menu." width=10cm
 *
 * @defgroup group_sdk_actions_image Image
 * @ingroup group_sdk_actions
 * The basic image actions of the CamiTK SDK.
 * The @ref camitk::Action image actions features bacic actions working on volumic image (@ref camitk::ImageComponent)
 *
 * @defgroup group_sdk_actions_ImageAcquisition ImageAcquisition
 * @ingroup group_sdk_actions
 * The generics actions of the CamiTK SDK to perform image acquisition.
 * The @ref camitk::Action ImageAcquisition features actions through interface system to perform any imaging acquisition process while manipulating ImageComponent object.
 *
 * @defgroup group_sdk_actions_image_arbitraryslice Arbitrary Slice
 * @ingroup group_sdk_actions_image
 * A basic action allowing you to select the arbitray slice on your @ref camitk::ImageComponent.
 * This action displays a simple widget allowing you to select the desired angle of your slice.
 * \image html actions/arbitray_slice.png "The arbitray slice widget" width=10cm
 *
 * @defgroup group_sdk_actions_image_cropvolume Crop Volume
 * @ingroup group_sdk_actions_image
 * A basic action allowing you to crop a volume within a @ref camitk::ImageComponent.
 * Helps you to crop a volume to keep only an interesting subsample.
 * You only need to select 2 or 6 points defining a parallelepiped to crop within the volumic image.
 * This action has a widget allowing the user to select those points.
 * \image html actions/crop_volume.png "The crop volume widget" width=10cm
 *
 * @defgroup group_sdk_actions_image_lut Look Up Table (LUT)
 * @ingroup group_sdk_actions_image
 * The look up table of a volumic image.
 *
 * This action allows you to display and manipulate the look up table (lut) of a @ref camitk::ImageComponent
 * The look up table aims at display the repartition of voxel values thanks to an histogram. The look up table features several options available through a widget.
 * \image html actions/lut.png "The Look Up Table widget" width=10cm
 *
 * @defgroup group_sdk_actions_image_multipicking Multipicking
 * @ingroup group_sdk_actions_image
 * The multipicking action
 *
 * This action allows allows the user to select several voxel coordinates in a @ref camitk::ImageComponent
 * The voxels are listed within a widget.
 * \image html actions/multipicking.png "The multipicking action widget" width=10cm
 *
 * @defgroup group_sdk_actions_image_reconstruction Reconstruction
 * @ingroup group_sdk_actions_image
 * Mesh reconstruction from a volumic image using Marching Cube algorithm.
 *
 * The reconstruction actions allows the user to compute a 3D @ref camitk::MeshComponent from a @ref camitk::ImageComponent
 * This action use the famous Marching Cube algorithm to compute the surfacic mesh from the volumic image.
 * \image html actions/reconstruction.png "The reconstruction action widget" width=10cm
 *
 * @defgroup group_sdk_actions_image_showin3d Show in 3D
 * @ingroup group_sdk_actions_image
 * Display or not, the selected slices of the volumic image within the 3D viewer.
 * This @ref camitk::Action aims at displaying :
 * - the selected slice (axial, coronal, sagittal and arbitrary) of the selected @ref camitk::ImageComponent
 * - the whole image itself in the 3D viewer.
 * @note
 * The arbitrary slice is hidden by default, you should use the corresponding action in order to display it.
 * Then use the @ref SetAnglesAction "SetAngleAction" to select the desired angles.
 * \image html actions/show_in_3D.png "Launch the desired action using the contextual menu" width=10cm
 *
 * @defgroup group_sdk_actions_image_volumerendering Volume rendering
 * @ingroup group_sdk_actions_image
 * Compute a 3D represention of the volumic image.
 * This @ref camitk::Action uses ray casting to compute a 3D reprensentation of the volumic image.
 * Typically, this action take a gray-level @ref camitk::ImageComponent as an input to produce a 3D colored image.
 * \image html actions/volume_rendering_1.png "The image computed is displayed in the 3D viewer." width=5cm
 *
 * To color the produced image, a transfer function is used to compute the corresponding (gray-level) => (R,G,B,alpha) values for each voxel of the image.
 * This transfer function parameters can be adjusted thanks to the action widget.
 *
 * \image html actions/volume_rendering_2.png "The transfer function adjustement." width=10cm
 *
 *
 * @defgroup group_sdk_actions_mesh Mesh
 * @ingroup group_sdk_actions
 * Some basic mesh actions of the CamiTK SDK.
 *
 *
 * @defgroup group_sdk_actions_mesh_basicmesh Basic mesh
 * @ingroup group_sdk_actions_mesh
 * Provides basic actions on @ref camitk::MeshComponent.
 * The @ref camitk::Action mesh actions features bacic actions working on mesh image (@ref camitk::MeshComponent)
 * \image html actions/basic_mesh_menu.png "The basic mesh actions contextual menu." width=10cm
 *
 * Several widgets are provided for each action, please take a look at each class to have a look of each widget.
 *
 * @defgroup group_sdk_actions_mesh_basictopology Basic topology
 * @ingroup group_sdk_actions_mesh
 * Basic topology actions allows the user to simply create spheres and grid of different dimensions.
 *
 * @note The component created in the 3D mesh (i.e. a @ref camitk::MeshComponent).
 * Also, as boths featured actions do not take as an input a component (they both create one, a @ref camitk::MeshComponent)
 * You should not select any component in order to throw this action.
 *
 * Use, as shown above, the Action menu bar accessible from the application to select both actions:
 * \image html actions/basic_topology_menu.png "The Actions menu bar showing available actions." width=10cm
 *
 * @note Both actions have a widget.
 * Please, take a look at the corresponding classes for more information.
 *
 * @defgroup group_sdk_actions_mesh_meshprocessing Mesh Processing
 * @ingroup group_sdk_actions_mesh
 * Features some nice algorithms for mesh processing.
 *
 * @note The input of each action is a @ref camitk::MeshComponent.
 *
 * Use, as shown above, the action contextual menu in order to select the desired mesh processing action.
 * \image html actions/basic_mesh_processing_menu.png "The mesh processing contextual menu." width=10cm
 *
 * @note Actions have a widget in order to select the input parameters (if needed).
 * Please, take a look at the corresponding classes for more information.
 *
 * @defgroup group_sdk_applications Applications
 * @ingroup group_sdk
 * The applications of the CamiTK SDK.
 * CamiTK applications are Qt applications. When featuring end user graphical interfaces, they are composed of several @ref camitk::Viewer.
 * You may inherits the @ref camitk::MainWindow "MainWindow" class. The MainWindow class is a kind of super presenter : it instanciates and organizes all the viewers in an application GUI. Check the wiki tutorial : <a href="https://forge.imag.fr/plugins/mediawiki/wiki/camitk/index.php/Create_your_own_application_for_CamiTK">create a new application</a>.
 *
 *
 * @defgroup group_sdk_application_asm Action State Machine (ASM)
 * @ingroup group_sdk_applications
 * The Action State Machine (ASM) is a simple pipeline of @ref camitk::Action interpreter, thanks to simple interface.
 *
 * Through a simple graphical user interface, the user will apply, step by step a bunch of actions described in a pipeline.
 * The description of the pipeline is done within a XML file, which is taken as an input of the application.
 * The user will have 2 main buttons "previous" and "next" to navigate from a step to another.
 *
 * @note Each time the user goes next, the current action is applied.
 * Each time the user goes previous, the current state component are deleted, and the application presents the previous action.
 *
 * @note This application was firstly targeting clinicians end users, which explains the simple ergonomy.
 *
 * \image html applications/asm.png "Smart application !" width=10cm
 *
 *
 * @defgroup group_sdk_application_cepgenerator CEP Generator
 * @ingroup group_sdk_applications
 * The CEP generator aims at generating a CEP with its content from a describing XML file.
 *
 * @note This application is used by the Wizard application to generate the CEP.
 * This application has no graphical user interface (GUI).
 *
 *
 * @defgroup group_sdk_application_config Config
 * @ingroup group_sdk_applications
 * The config application is used to retrieve information about a CamiTK installation and is used at the configuration step (CMake) of another CEP to indicate where the CamiTK installation is located.
 *
 * @note This application is used at configuration step of any CEP. Therefore, this application must be in the current PATH.
 * This application is located in the bin subfolder of the installation directory of CamiTK.
 *
 *
 * @defgroup group_sdk_application_imp Imp
 * @ingroup group_sdk_applications
 * Imp is the most famous, used CamiTK application.
 *
 * Imp is a graphical users interface (GUI) which allows one to manipulate @ref camitk::Action, @ref camitk::Component and @ref camitk::Viewer and adjust lots of parameters.
 *
 * \image html applications/imp.png "The Imp application !" width=1024
 *
 * @note Imp is mainly focusing users desiring testing their algorithms on data.
 * For prototyping a ready to test pipeline please see @ref ActionStateMachine
 *
 * @note This application is used at configuration step of any CEP. Therefore, this application must be in the current PATH.
 * This application is located in the bin subfolder of the installation directory of CamiTK.
 *
 *
 * @defgroup group_sdk_application_testactions Test actions
 * @ingroup group_sdk_applications
 * Camitk-testactions is able to execute a bench of unit tests on CamiTK actions.
 *
 * @code
 * SYNOPSIS
       camitk-testactions [OPTIONS]...

   OPTIONS
       --help|-h
              Print usage information and exit.


       --version|-v
              Print CamiTK version message


       --action|-a file
              filename of the action extension to process


       --input|-i file
              input file on which the tested action will process -  give  full
              path - file is not modified

 * @endcode
 * @note This application has no graphical user interface (GUI).
 *
 *
 * @defgroup group_sdk_application_testcomponents Test components
 * @ingroup group_sdk_applications
 * Camitk-testcomponents is able to execute a bench of unit tests on CamiTK components.
 *
 * @code
 * SYNOPSIS
       camitk-testcomponents [OPTIONS]...

   OPTIONS
       --help|-h
              Print usage information and exit.


       --version|-v
              Print CamiTK version message


       --input|-i file
              test file to open


       --output-dir|-o path
              directory where the output file will be saved

 * @endcode
 * @note This application has no graphical user interface (GUI).
 *
 *
 * @defgroup group_sdk_application_wizard Wizard
 * @ingroup group_sdk_applications
 * Wizard is the application which will allows you to generate the "glue" to create your own extensions.
 * The Wizard application ask you questions in order to generate the skeleton of your extensions. The extensions are generated inside an existing CEP. It also allows you to create a new CEP.
 *
 * \image html applications/wizard.png "The Wizard application !" width=1024
 *
 *
 *
 * @defgroup group_sdk_components Components
 * @ingroup group_sdk
 * The components of the CamiTK SDK.
 * CamiTK components describe data structure and therefore are used to represent the data logic. The @ref camitk::Component "Component" class implements four interfaces, which gather the different possibilities of a component in order to provide the service layer. See the @ref camitk::Component "Component" class to have more details.
 *
 *
 * @defgroup group_sdk_components_msh MSH
 * @ingroup group_sdk_components
 * The component MSH allows users to handle .msh files.
 * MSH file format describes 3D meshes.
 *
 *
 * @defgroup group_sdk_components_obj Obj
 * @ingroup group_sdk_components
 * The component Obj allows users to handle .obj files.
 * Obj file format describes 3D meshes.
 *
 *
 * @defgroup group_sdk_components_off Off
 * @ingroup group_sdk_components
 * The component Off allows users to handle .off files.
 * Off file format describes 3D meshes.
 *
 *
 * @defgroup group_sdk_components_vrml Vrml
 * @ingroup group_sdk_components
 * The component Vrml allows users to handle .vrml files.
 * Vrml file format describes 3D meshes.
 *
 *
 * @defgroup group_sdk_components_vtkimage Vtkimage
 * @ingroup group_sdk_components
 * Vtkimage components allows one to handle several @ref camitk::ImageComponent supported by VTK
 *
 *
 * @defgroup group_sdk_components_vtkmesh Vtkmesh
 * @ingroup group_sdk_components
 * Vtkmesh components allows one to handle several @ref camitk::MeshComponent supported by VTK
 *
 * @defgroup group_sdk_components_stl STLmesh
 * @ingroup group_sdk_components
 * STL component is based on @ref camitk::MeshComponent to include STL file format in CamiTK
 *
 * @defgroup group_sdk_viewers Viewers
 * @ingroup group_sdk
 * The viewers of the CamiTK SDK.
 * The @ref camitk::Viewer "Viewer" class handles all the presentation and interaction logic (the View and Presenter part of the MVP). The CamiTK viewers are using either Qt widget or VTK renderer but are not limited to that. Viewers manage the user interactions and map them into calls to the interface methods of the components.
 *
 *
 * @defgroup group_sdk_libraries Libraries
 * @ingroup group_sdk
 * The libraries of the CamiTK SDK.
 * Libraries folder features static and dynamic libraries which are non CamiTK dependent. Typically, when you bring your own code / outside libraries, you put them in this part of the CEP.
 * Libraries are thus used by other CamiTK extension, such as @ref camitk::Action and @ref camitk::Component.
 *
 *
 * @defgroup group_sdk_libraries_cepcoreschema CEP coreschema
 * @ingroup group_sdk_libraries
 * The Cep coreschema library aims describing the classes for @ref CepGenerator library.
 * The Wizard application generates an XML that describes the CEP of the user.
 * The XMl file follows rules described in the corresponding XML schema (XSD) files of this library.
 * @note CEP Coreschema classes description are in XSD (XML schema) description.
 *
 *
 * @defgroup group_sdk_libraries_cepgenerator CEP Generator
 * @ingroup group_sdk_libraries
 * The Cep generator library aims at generating a CEP from a XML description.
 * The Wizard application generate an XML file which describes the CEP generated by the user. Then, the application calls this library to generate the CEP (directories structures with file) corresponding to the XML description.
 *
 *
 * @defgroup group_sdk_libraries_core Core
 * @ingroup group_sdk_libraries
 * The CamiTK Core library represents the base of CamiTK.
 *
 * CamiTK core allows extensions to communicate and works together, for this it's the base of CamiTK and any extension using CamiTK requires the CamiTK Core library.
 *
 * \image html libraries/CamiTKCore.png "The CamiTK 'roulette', the core and some extensions" width=768
 *
 * @section architecture Core Architecture
 * CamiTK core architecture follows the Model-View-Presenter (MVP) design-pattern. It helps dividing the logic in two parts : domain logic (data structures and algorithms) and application logic (the Model, GUI aspects to present and interact with the domain logic).
 *
 * CamiTK follows the Components Based Software Engineering (CBSE) principles. Each new functionality to the framework is a new module represented as a dynamic library (.dll on windows, .so on Linux systems). These principles guarantee CamiTK to be higly modular and easier to maintain.
 * CamiTK is provided with its core and basic modules (to handle 2D and 3D images using VTK for instance). You can easily add new modules to the framework or create new camitk-application.
 *
 * \image html camitk_service_layer_1.png "The service layer" width=10cm
 *
 * To do so, 'viewers' (application logic modules) are used to present and interact with the domain logic : the 'components' (the data logic, the data structures) and the 'actions' (the processing logic, the algorithms to be used on these components).
 * In order to easily glue together all this modules CamiTK uses a Service Layer Pattern. This service layer can be seen as a contract from the viewers consumers of services provided by the 'components' and 'actions'. The viewers interact with the components without knowing exactly which kind of data it represents.
 *
 * @see group::group_sdk_libraries_core_viewer
 *
 *
 * @defgroup group_sdk_libraries_core_action Action
 * @ingroup group_sdk_libraries_core
 * The CamiTK Core Action structure.
 * The @ref camitk::Action "Action" class generalizes the notion of component processing. An action applies a given algorithm to one or several listed components.
 * Generally viewers trigger actions on the currently selected components.
 * Actions may work on [0 ... N] components and may have [0 ... N] input parameters.
 *
 *
 * @defgroup group_sdk_libraries_core_application Application
 * @ingroup group_sdk_libraries_core
 * The CamiTK Core Application structure.
 * The @ref camitk::Application "Application" class handle the concept of CamiTK application. When the user wants to create a CamiTK (which 99% statiscally not needed, as the IMP application should be sufficient), it will instanciate this class.
 * The @ref camitk::Application is based on the Qt QApplication class with some CamiTK features.
 * A CamiTK application will have a @ref camitk::MainWindow which will agregate @ref camitk::Viewer in a graphical user interface.
 * @note CamiTK application may also be created without graphical user interface.
 *
 *
 * @defgroup group_sdk_libraries_core_component Component
 * @ingroup group_sdk_libraries_core
 * The CamiTK Core Component structure.
 * Components are used to handle date whithin CamiTK. They may be use to handle some 2D, 3D images or devices such as robots, kinects ...
 * @section Architecture
 * CamiTK components describe data structure and therefore are used to represent the data logic.
 * \image html camitk_service_layer_2.png "The four interfaces implemented by the Component class" width=10cm
 * The @ref camitk::Component "Component" class implements four interfaces, which gather the different possibilities of a component in order to provide the service layer. See the @ref camitk::Component "Component" class to have more details.
 *
 *
 * @defgroup group_sdk_libraries_core_component_image Image
 * @ingroup group_sdk_libraries_core_component
 * The CamiTK Core Image Component structure.
 * Image component handle volumic image, typically acquired by a scanner (CT, MRI ...).
 * Image components can be displayed in all the viewer of the @ref camitk::MedicalImageViewer "MedicalImageViewer".
 *
 *
 * @defgroup group_sdk_libraries_core_component_mesh Mesh
 * @ingroup group_sdk_libraries_core_component
 * The CamiTK Core Mesh Component structure.
 * Mesh component handle 3D mesh structures (such as 3D clouds etc.).
 *
 *
 *
 * @defgroup group_sdk_libraries_core_utils Utils
 * @ingroup group_sdk_libraries_core
 * Some useful tools of CamiTK.
 * Utils contains useful tools provided with the CamiTK architecture, such as the @ref camik::Log "Log", the nice @ref camitk::Property "Property" handling and so more.
 * @note
 * Please directly take a look at the corresponding classes for more information.
 *
 *
 * @defgroup group_sdk_libraries_core_viewer Viewer
 * @ingroup group_sdk_libraries_core
 * The CamiTK Core Viewer structure.
 * The @ref camitk::Viewer "Viewer" class handles all the presentation and interaction logic (the View and Presenter part of the MVP). The CamiTK viewers are using either Qt widget or VTK renderer but are not limited to that. Viewers manage the user interactions and map them into calls to the interface methods of the components.
 *
 *
 * @defgroup group_sdk_libraries_coreschema Coreschema
 * @ingroup group_sdk_libraries
 * The Coreschema library aims describing the classes of the @ref camitk::Core architecture.
 * @note Coreschema classes description are presented trough an XML schema description (XSD files).
 *
 *
 * @defgroup group_sdk_libraries_qtpropertybrowser QtPropertyBrowser
 * @ingroup group_sdk_libraries
 * The QtPropertyBrowser library allows one to manage the Qt property
 *
 *
 * @defgroup group_sdk_cmake CMake macros
 * @ingroup group_sdk
 * The CMake macros used to configure this project.
 * These macros are CamiTK projects specific (the ones in the camitk subfolders), others are CMake macros that you can use in any CMake projects.
 *
 * @defgroup group_sdk_cmake_camitk camitk
 * @ingroup group_sdk_cmake
 * The CMake macros especially defined to configure CMake projects using CamiTK.
 *
 * @defgroup group_sdk_cmake_camitk_cdash cdash
 * @ingroup group_sdk_cmake_camitk
 * CamiTK CMake macros used for continuous integration purpose.
 *
 * @defgroup group_sdk_cmake_camitk_cdash_projects projects
 * @ingroup group_sdk_cmake_camitk_cdash
 * CamiTK CMake macros for generating the listing of all the CamiTK project as a build/SubProjects.cmake and build/Project.xml files.
 * These files are used by CDash and continuous integration scripts to exactly know the CMake projects to work with and their cross-dependencies.
 *
 * @defgroup group_sdk_cmake_camitk_packaging packaging
 * @ingroup group_sdk_cmake_camitk
 * CamiTK CMake macros used to create package of the CamiTK project (a custom CEP or the CamiTK project itself).
 *
 * @defgroup group_sdk_cmake_camitk_test test
 * @ingroup group_sdk_cmake_camitk
 * CamiTK CMake macros used for testing.
 *
 * @defgroup group_sdk_cmake_camitk_test_level level
  * @ingroup group_sdk_cmake_camitk_test
 * CamiTK CMake macros used generating the mediawiki tab containing the list of all functionnal tests, classified by CamiTK extensions with their given LEVEL.
 * @see https://forge.imag.fr/plugins/mediawiki/wiki/camitk/index.php/Testing_level_in_CamiTK
 *
 **/
