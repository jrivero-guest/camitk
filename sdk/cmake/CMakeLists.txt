#
# cmake module CMakeList.txt
#
set(CAMITK_CMAKE_PATH "share/${CAMITK_SHORT_VERSION_STRING}/cmake")

# install CamiTK configuration file
install(FILES ${CMAKE_BINARY_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cmake/CamiTKConfig.cmake
        DESTINATION ${CAMITK_CMAKE_PATH}
        )
        
# install the generic cmake files
file(GLOB CAMITK_CMAKE_FILES "modules/*.cmake")
install(FILES ${CAMITK_CMAKE_FILES}
        DESTINATION ${CAMITK_CMAKE_PATH}
        )

# install the macros 
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/modules/macros
        DESTINATION ${CAMITK_CMAKE_PATH}
        )
        
# install launcher-templates
file(GLOB CAMITK_CMAKE_FILES "modules/launcher-templates/*.in")
install(FILES ${CAMITK_CMAKE_FILES}
        DESTINATION ${CAMITK_CMAKE_PATH}/launcher-templates
        )
