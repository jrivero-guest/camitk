# Configuration file for CamiTK
#
# Do not modify this file, it has been generated automatically by CamiTK SDK
#
# CAMITK_DIR should be known before this file is read by CMake
#
# see:
# http://www.itk.org/Wiki/CMake/Tutorials/How_to_create_a_ProjectConfig.cmake_file
# http://www.cmake.org/Wiki/CMake/Tutorials/Packaging

#-- CamiTK current version
set(CAMITK_VERSION_MAJOR "@CAMITK_VERSION_MAJOR@")
set(CAMITK_VERSION_MINOR "@CAMITK_VERSION_MINOR@")
# CamiTK short version name we are looking for
set(CAMITK_SHORT_VERSION_STRING "camitk-${CAMITK_VERSION_MAJOR}.${CAMITK_VERSION_MINOR}")

#-- Cmake policy 
# Error on non-existent dependency in add_dependencies => old behavior (no error)
# It is useful to have add_dependencies(anyextension camitkcore) for SDK/modeling/imaging dev
# It shoud not generate an error or a warning for external CEP trying to compile
# see cmake --help-policy CMP0046
cmake_policy(SET CMP0046 OLD)

#-- Check dependencies: Qt and VTK
# List of Qt5 modules that are required in CamiTK
set(CAMITK_QT_COMPONENTS Core Gui Xml XmlPatterns Widgets Help UiTools OpenGL OpenGLExtensions Test)
# Find Qt5
find_package(Qt5 COMPONENTS ${CAMITK_QT_COMPONENTS} REQUIRED)
if (Qt5_FOUND)
    message(STATUS "CamiTK SDK : found Qt ${Qt5_VERSION}.")
    set(QT_INCLUDE_DIRS ${Qt5Widgets_INCLUDE_DIRS} ${Qt5Core_INCLUDE_DIRS} ${Qt5Gui_INCLUDE_DIRS} ${Qt5Xml_INCLUDE_DIRS} ${Qt5XmlPatterns_INCLUDE_DIRS} ${Qt5Declarative_INCLUDE_DIRS} ${Qt5Help_INCLUDE_DIRS} ${Qt5UiTools_INCLUDE_DIRS} ${Qt5OpenGL_INCLUDE_DIRS} ${Qt5OpenGLExtensions_INCLUDE_DIRS})
    # force Qt to print more debug information
    add_definitions(-DQT_MESSAGELOGCONTEXT)
else()
    message(SEND_ERROR "CamiTK SDK : Failed to find Qt 5.x. This is needed by the CamiTK SDK.")
endif()

# Find VTK
find_package(VTK REQUIRED)
if(NOT VTK_FOUND)
    message(SEND_ERROR "Failed to find Vtk 6.0.0 or greater. This is needed by CamiTK.")
endif()
if("${VTK_VERSION}" VERSION_LESS 6.0)
    message(FATAL_ERROR "Found VTK ${VTK_VERSION} version but at least 6.0.0 is required. Please update your version of VTK.")
endif()
message(STATUS "Found suitable version of VTK : ${VTK_VERSION} (required is at least 6.0.0)")
set(CAMITK_VTK_VERSION ${VTK_VERSION_MAJOR}.${VTK_VERSION_MINOR})
include(${VTK_USE_FILE}) # This automatically set the include dirs (remain the link_directories and target_link_libraries)
set(VTK_LIBRARY_DIR ${VTK_DIR}/../..)

#-- CAMITK_DIR variable
set(CAMITK_BIN_DIR         ${CAMITK_DIR}/bin                                           )
set(CAMITK_PRIVATE_LIB_DIR ${CAMITK_DIR}/lib/${CAMITK_SHORT_VERSION_STRING}            )
set(CAMITK_PUBLIC_LIB_DIR  ${CAMITK_DIR}/lib                                           )
set(CAMITK_INCLUDE_DIR     ${CAMITK_DIR}/include/${CAMITK_SHORT_VERSION_STRING}        )
set(CAMITK_TESTDATA_DIR    ${CAMITK_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/testdata )
set(CAMITK_CMAKE_DIR       ${CAMITK_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cmake    )

#-- User Config directory variable
# see http://qt-project.org/doc/qt-4.8/qsettings.html#platform-specific-notes
if(WIN32)
    # %APPDATA%\MySoft\Star Runner.ini
    set(CAMITK_USER_BASE_DIR_WINDOWS $ENV{APPDATA})
    file(TO_CMAKE_PATH "${CAMITK_USER_BASE_DIR_WINDOWS}" CAMITK_USER_BASE_DIR)
else()
    # (UNIX OR APPLE)
    # $HOME/.config/MySoft/Star Runner.ini 
    set(CAMITK_USER_BASE_DIR "$ENV{HOME}/.config")
endif()
set(CAMITK_USER_DIR "${CAMITK_USER_BASE_DIR}/CamiTK")
set(CAMITK_USER_BIN_DIR          ${CAMITK_USER_DIR}/bin                                           )
set(CAMITK_USER_PRIVATE_LIB_DIR  ${CAMITK_USER_DIR}/lib/${CAMITK_SHORT_VERSION_STRING}            )
set(CAMITK_USER_PUBLIC_LIB_DIR   ${CAMITK_USER_DIR}/lib                                           )
set(CAMITK_USER_INCLUDE_DIR      ${CAMITK_USER_DIR}/include/${CAMITK_SHORT_VERSION_STRING}        )
set(CAMITK_USER_TESTDATA_DIR     ${CAMITK_USER_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/testdata )
set(CAMITK_USER_CMAKE_DIR        ${CAMITK_USER_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cmake    )

#-- Current build dir
set(CAMITK_BUILD_DIR ${CMAKE_BINARY_DIR})
set(CAMITK_BUILD_BIN_DIR         ${CAMITK_BUILD_DIR}/bin                                           )
set(CAMITK_BUILD_PRIVATE_LIB_DIR ${CAMITK_BUILD_DIR}/lib/${CAMITK_SHORT_VERSION_STRING}            )
set(CAMITK_BUILD_PUBLIC_LIB_DIR  ${CAMITK_BUILD_DIR}/lib/                                          )
set(CAMITK_BUILD_INCLUDE_DIR     ${CAMITK_BUILD_DIR}/include/${CAMITK_SHORT_VERSION_STRING}        )
set(CAMITK_BUILD_TESTDATA_DIR    ${CAMITK_BUILD_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/testdata )
set(CAMITK_BUILD_CMAKE_DIR       ${CAMITK_BUILD_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cmake    )

# no need to see this
mark_as_advanced ( CMAKE_RUNTIME_OUTPUT_DIRECTORY CMAKE_LIBRARY_OUTPUT_DIRECTORY CMAKE_ARCHIVE_OUTPUT_DIRECTORY)

#
# Convenience variables
#


# name of the camitk core lib
set(CAMITK_CORE_LIB_NAME camitkcore)

if(PACKAGING_NSIS)
    set(CAMITK_CORE_TARGET_LIB_NAME library_${CAMITK_CORE_LIB_NAME})
else()
    set(CAMITK_CORE_TARGET_LIB_NAME library-${CAMITK_CORE_LIB_NAME})
endif()
    
# All the link directories
set(CAMITK_LINK_DIRECTORIES
    # private and public libs (including bin dir for windows)
    ${CAMITK_PRIVATE_LIB_DIR}
    ${CAMITK_PUBLIC_LIB_DIR}
    ${CAMITK_BIN_DIR}
    ${CAMITK_USER_PRIVATE_LIB_DIR}
    ${CAMITK_USER_PUBLIC_LIB_DIR}
    ${CAMITK_USER_BIN_DIR}
    ${CAMITK_BUILD_PRIVATE_LIB_DIR}
    ${CAMITK_BUILD_PUBLIC_LIB_DIR}
    ${CAMITK_BUILD_BIN_DIR}
    # CEP extensions dependencies
    ${CAMITK_PRIVATE_LIB_DIR}/components
    ${CAMITK_PRIVATE_LIB_DIR}/actions
    ${CAMITK_USER_PRIVATE_LIB_DIR}/components
    ${CAMITK_USER_PRIVATE_LIB_DIR}/actions
    ${CAMITK_BUILD_PRIVATE_LIB_DIR}/components
    ${CAMITK_BUILD_PRIVATE_LIB_DIR}/actions    
)

# List of directories where the compiler should look for CamiTK core headers as well
# as all the tools headers and dependencies headers (Qt, Vtk)
set(CAMITK_INCLUDE_DIRECTORIES
    ${CAMITK_INCLUDE_DIR}
    ${CAMITK_INCLUDE_DIR}/libraries
    ${CAMITK_INCLUDE_DIR}/libraries/qtpropertybrowser
    ${CAMITK_INCLUDE_DIR}/libraries/${CAMITK_CORE_LIB_NAME}
    ${CAMITK_USER_INCLUDE_DIR}
    ${CAMITK_BUILD_INCLUDE_DIR}
    ${QT_INCLUDE_DIRS}
    ${VTK_INCLUDE_DIRS}
    ${CAMITK_BUILD_INCLUDE_DIR}/libraries  
)
# TODO: remove ${CAMITK_BUILD_INCLUDE_DIR}/libraries => find a better way to organize dependencies

# postfix for MSVC debug version
set(CAMITK_DEBUG_POSTFIX "-debug")

# basic list of all lib needed for linking with CamiTK core
if(MSVC)
    # name of the camitk core lib
    set(CAMITK_CORE_LIBRARIES optimized ${CAMITK_CORE_LIB_NAME}
                              debug ${CAMITK_CORE_LIB_NAME}${CAMITK_DEBUG_POSTFIX}
    )
    
    # Construct list of VTK libraries for linking
    # CAMITK_VTK_LIBRARIES = VTK input libraries at linkage
    foreach(VTK_LIBRARY ${VTK_LIBRARIES})
        # Clear some errors on VTK configuration
        # rename correctly verdict -> vtkverdict library
        if(${VTK_LIBRARY} STREQUAL "verdict")
            set(VTK_LIBRARY "vtkverdict")
        endif()
        set(CAMITK_VTK_LIBRARIES ${CAMITK_VTK_LIBRARIES} debug ${VTK_LIBRARY_DIR}/${VTK_LIBRARY}-${CAMITK_VTK_VERSION}${CAMITK_DEBUG_POSTFIX}.lib optimized ${VTK_LIBRARY_DIR}/${VTK_LIBRARY}-${CAMITK_VTK_VERSION}.lib)
    endforeach()
    # add missing vtkIOExport, vtkImagingStencil, vtkIOImport, vtkImagingMath library
    set(CAMITK_VTK_LIBRARIES ${CAMITK_VTK_LIBRARIES} debug ${VTK_LIBRARY_DIR}/vtkIOExport-${CAMITK_VTK_VERSION}${CAMITK_DEBUG_POSTFIX}.lib optimized ${VTK_LIBRARY_DIR}/vtkIOExport-${CAMITK_VTK_VERSION}.lib)
    set(CAMITK_VTK_LIBRARIES ${CAMITK_VTK_LIBRARIES} debug ${VTK_LIBRARY_DIR}/vtkImagingStencil-${CAMITK_VTK_VERSION}${CAMITK_DEBUG_POSTFIX}.lib optimized ${VTK_LIBRARY_DIR}/vtkImagingStencil-${CAMITK_VTK_VERSION}.lib)
    set(CAMITK_VTK_LIBRARIES ${CAMITK_VTK_LIBRARIES} debug ${VTK_LIBRARY_DIR}/vtkIOImport-${CAMITK_VTK_VERSION}${CAMITK_DEBUG_POSTFIX}.lib optimized ${VTK_LIBRARY_DIR}/vtkIOImport-${CAMITK_VTK_VERSION}.lib)
    set(CAMITK_VTK_LIBRARIES ${CAMITK_VTK_LIBRARIES} debug ${VTK_LIBRARY_DIR}/vtkImagingMath-${CAMITK_VTK_VERSION}${CAMITK_DEBUG_POSTFIX}.lib optimized ${VTK_LIBRARY_DIR}/vtkImagingMath-${CAMITK_VTK_VERSION}.lib)
        
    # list of all core dependencies
    set(CAMITK_LIBRARIES
            ${CAMITK_VTK_LIBRARIES}
            debug qtpropertybrowser${CAMITK_DEBUG_POSTFIX}.lib optimized qtpropertybrowser
    )
else()
    # Name of the core library to link against
    set(CAMITK_CORE_LIBRARIES ${CAMITK_CORE_LIB_NAME})

    # list of all core dependencies
    set(CAMITK_LIBRARIES
        ${VTK_LIBRARIES} # no need to add more libraries in the list, CMake & VTK handle it correclty on Linux & Apple
        # QVTK
        qtpropertybrowser
    )
endif()

mark_as_advanced (CAMITK_CORE_LIB CAMITK_LINK_DIRECTORIES CAMITK_INCLUDE_DIRECTORIES CAMITK_LIBRARIES)

# additional unix system resources
if (UNIX)
    # only possible on unix
    if(DEFINED ENV{SOURCE_DATE_EPOCH})
        # to enable reproducible builds, see debian bug #794740
        execute_process(COMMAND "date" "-u" "+%Y-%m-%d" "--date=@$ENV{SOURCE_DATE_EPOCH}"
                        OUTPUT_VARIABLE CURRENT_DATE
                        OUTPUT_STRIP_TRAILING_WHITESPACE
        )
    else()
        execute_process(COMMAND "date" "+%Y-%m-%d"
                        OUTPUT_VARIABLE CURRENT_DATE
                        OUTPUT_STRIP_TRAILING_WHITESPACE
        )
    endif()
    
    # Application man pages (always in section 1, this is an application...)
    set(CAMITK_APPLICATION_MAN_INSTALL_DIR "share/man/man1") 
    # Desktop file always in share/applications (freedesktop standard)
    set(CAMITK_APPLICATION_DESKTOP_INSTALL_DIR "share/applications")
    # icons file always in pixmaps (should be xpm or svg)
    set(CAMITK_APPLICATION_PIXMAP_INSTALL_DIR "share/pixmaps")
endif()
            
# rpath settings for linux
# This settings allow running the exe from the build tree and running them from its install location
# without doing anything in particular.
# see http://www.itk.org/Wiki/CMake_RPATH_handling
# use, i.e. don't skip the full RPATH for the build tree
set(CMAKE_SKIP_BUILD_RPATH FALSE)

# when building, don't use the install RPATH already
# (but later on when installing)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# the RPATH to be used when installing, but only if it's not a system directory
# TODO : write a specific macro camitk_install to use for installing library
# This macro should not only do the usual install job, but also should add the
# specific install path in the CMAKE_INSTALL_RPATH variables
# On windows: write a message at the end of the installation process with the value
# of CMAKE_INSTALL_RPATH
# On Windows: build a specific .bat for each application?
# Even better, try this: cumulate the CMAKE_INSTALL_RPATH in a file containing the list
# of all needed dir in the path. This file is read at run-time to add the path using
# Application::instance()->addLibraryPath(path#i.canonicalPath());
list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
if("${isSystemDir}" STREQUAL "-1")
    set(CMAKE_INSTALL_RPATH  "${CMAKE_INSTALL_PREFIX}/lib"
                            "${CMAKE_INSTALL_PREFIX}/lib/${CAMITK_SHORT_VERSION_STRING}"
                            "${CMAKE_INSTALL_PREFIX}/lib/${CAMITK_SHORT_VERSION_STRING}/components"
                            "${CMAKE_INSTALL_PREFIX}/lib/${CAMITK_SHORT_VERSION_STRING}/actions")
endif()

#-- update module path
# for SDK look also directly in source dir
if(CAMITK_COMMUNITY_EDITION_BUILD)
    set(CMAKE_MODULE_PATH   ${CMAKE_MODULE_PATH}
                            ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules
                            ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules/macros
                            ${CMAKE_CURRENT_SOURCE_DIR}/../sdk/cmake/modules
                            ${CMAKE_CURRENT_SOURCE_DIR}/../sdk/cmake/modules/macros
    )
endif()
set(CMAKE_MODULE_PATH   ${CMAKE_MODULE_PATH}
                        ${CAMITK_CMAKE_DIR}
                        ${CAMITK_CMAKE_DIR}/modules
                        ${CAMITK_CMAKE_DIR}/modules/macros
                        ${CAMITK_CMAKE_DIR}/macros
                        ${CAMITK_USER_CMAKE_DIR}
                        ${CAMITK_USER_CMAKE_DIR}/modules
                        ${CAMITK_BUILD_CMAKE_DIR}
                        ${CAMITK_BUILD_CMAKE_DIR}/modules
)


# include useful modules
include(CamiTKMacros)

# include CamiTK log configuration
include(CamiTKLog)

# TODO check this
# 
# # By default, validate all projects are build in the build/bin directory (no subfolders).
# # Note that, subfolders for actions / components are added later.
# set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CAMITK_BIN_DIR})
# set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CAMITK_BIN_DIR})
# set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CAMITK_BIN_DIR})
# 
# # Remove the Debug, Release subfolders in build/bin, natively created with Visual Studio
# if ( ${CMAKE_GENERATOR} MATCHES "Visual Studio") # MSVC compiler
#         foreach( OUTPUTCONFIG ${CMAKE_CONFIGURATION_TYPES} )
#                 string( TOUPPER ${OUTPUTCONFIG} OUTPUTCONFIG )
#                 set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CAMITK_BIN_DIR} CACHE TYPE STRING)
#                 set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CAMITK_BIN_DIR} CACHE TYPE STRING)
#                 set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CAMITK_BIN_DIR} CACHE TYPE STRING)
#         endforeach( OUTPUTCONFIG CMAKE_CONFIGURATION_TYPES )
# endif(${CMAKE_GENERATOR} MATCHES "Visual Studio")
# 
# # Create the Xml data for Project.xml description, fyi store in the global variable CAMITK_SUBPROJECTS
# camitk_init_manifest_data()
