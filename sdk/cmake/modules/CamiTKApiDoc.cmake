#------------------------------------------
# To generate CamiTK the api documentation
#-------------------------------------------
set(APIDOC_SDK FALSE CACHE BOOL "Generate API documentation for CamiTK Community Edition")
if(APIDOC_SDK)
    include(FindDoxygen)
    if (DOXYGEN)
        # ---------------------------------
        # Cmake modules needed
        # ---------------------------------
        find_package(HTMLHelp)

        if (HTML_HELP_COMPILER)
        set(DOXYGEN_HTMLHELP YES)
        else (HTML_HELP_COMPILER)
        set(DOXYGEN_HTMLHELP NO)
        endif (HTML_HELP_COMPILER)

        if (DOT)
        set(HAVE_DOT YES)
        else (DOT)
        set(HAVE_DOT NO)
        endif (DOT)

        set(DOXYGEN_LANGUAGE "English" CACHE STRING "Language used by doxygen")
        mark_as_advanced(DOXYGEN_LANGUAGE)

        # define where to put the camitk-ce-api-doc at build time
        set(CAMITK_APIDOC_DIR ${CMAKE_BINARY_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/apidoc)

        set(DOXYGEN_OUTPUT_DIR ${CAMITK_APIDOC_DIR})

        message(STATUS "Generating build target 'camitk-ce-api-doc' for CamiTK SDK documentation in ${CAMITK_APIDOC_DIR}")

        # which directories to use
        set(DOXYGEN_SUBDIRS)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/sdk/doc)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/imaging/doc)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_SOURCE_DIR}/modeling/doc)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/libraries/core)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/libraries/cepgenerator)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/libraries/qtpropertybrowser)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/../modeling/libraries)        
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/actions)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/../modeling/actions)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/../imaging/actions)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/applications)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/components)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/../modeling/components)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_CURRENT_SOURCE_DIR}/../imaging/components)
        set(DOXYGEN_SUBDIRS ${DOXYGEN_SUBDIRS} ${CMAKE_BINARY_DIR}/doxygencmake)

        string(REGEX REPLACE ";" " " DOXYGEN_INPUT_LIST "${DOXYGEN_SUBDIRS}")

        set(PROJECT_LOGO ${CMAKE_CURRENT_SOURCE_DIR}/libraries/core/resources/camitk-small.png)

        file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/doxygencmake)
        # read all cmake macro and generate corresponding header files converting #! to doxygen comment
        file(GLOB_RECURSE CMAKE_MACROS ${CMAKE_SOURCE_DIR}/sdk/cmake/modules/macros/*.cmake)
        foreach(CMAKE_MACRO ${CMAKE_MACROS})
            # message(STATUS "Generating doxygen documentation for CMake macro ${CMAKE_MACRO}")
            # read to a string buffer
            file(READ ${CMAKE_MACRO} cmakeMacroText)
            # replace on each line the specific macro doxygen code #! by C++ header doxygecn code ///
            string(REGEX REPLACE "#!" "///" doxygenDoc "${cmakeMacroText}")
            # replace the string "macro(name_of_camitk_macro..." by "name_of_camitk_macro(...)" in order to have an nice doxygen output
            string(REGEX REPLACE "macro\\(([a-z_]*)" "\\1(){" doxygenDocWithName "${doxygenDoc}")
            # name of the file
            get_filename_component(doxygenDocFilename ${CMAKE_MACRO} NAME_WE)
            file(WRITE ${CMAKE_BINARY_DIR}/doxygencmake/${doxygenDocFilename}.h ${doxygenDocWithName})
        endforeach()
  
        # Configure doxyfile, doxygen project configuration file.
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules/doxygen.conf.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)

        # Configure CamiTKVersion.h.in to generate index.html page for API documentation
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/libraries/core/CamiTKVersion.h.in ${CMAKE_CURRENT_BINARY_DIR}/core/CamiTKVersion.h @ONLY)

        add_custom_target(camitk-ce-api-doc
                          COMMAND ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile
                          COMMENT "Generating CamiTK SDK API documentation using Doxygen"
                          VERBATIM
        )

        # create directory if it does not exist
        if (NOT EXISTS ${CAMITK_APIDOC_DIR})
            message(STATUS "Creating api-doc directory: ${CAMITK_APIDOC_DIR}")
            file(MAKE_DIRECTORY ${CAMITK_APIDOC_DIR})
        endif()

        # doc installation
        install(DIRECTORY ${CAMITK_APIDOC_DIR}
                DESTINATION share/${CAMITK_SHORT_VERSION_STRING}
                COMPONENT camitk-ce-api-doc
        )
    else()
        message(WARNING "Doxygen not found - CamiTK SDK API documentation and reference manual will not be created")
    endif()
endif()
