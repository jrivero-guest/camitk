# Locate code synthesis xsd and define include paths, binary and determine installed version
# xsd can be found at http://codesynthesis.com/products/xsd/
# Originally written by Frederic Heem, frederic.heem _at_ telsey.it

# This module defines
# XSD_INCLUDE_DIR    where to find elements.hxx, etc.
# XSD_EXECUTABLE     where is the xsd compiler
# XSD_FOUND          if false, don't try to use xsd
# XSD_VERSION        the xsd version that was found
# XSD_CXX_STD_FLAG   force to C++11 if and only if xsd version is greater or equals to 4.0.0, empty string otherwise

find_path(XSD_INCLUDE_DIR 
          NAMES "xsd/cxx/parser/elements.hxx"
          PATHS "[HKEY_CURRENT_USER\\software\\xsd\\include]"
                "[HKEY_CURRENT_USER]\\xsd\\include]"
                $ENV{XSDDIR}/include
                $ENV{XERCESC_ROOT_DIR}/include
                $ENV{XERCESC_DIR}/include
                ${XERCESC_INCLUDE_DIR}
                ${XERCESC_INCLUDE_DIR}/include
                ${XERCESC_INCLUDE_DIR}/..
                /usr/local/include
                /usr/include
)

# check if the plateform can compile the xsd automatically
find_program(XSD_EXECUTABLE
             NAMES xsdcxx xsd
             PATHS "[HKEY_CURRENT_USER\\xsd\\bin"
                   $ENV{XSDDIR}/bin 
                   /usr/local/bin
                   /usr/bin
                   ${CMAKE_FIND_ROOT_PATH}/bin 
)

# if the include and the program are found then it is installed and found
if(XSD_INCLUDE_DIR)
    if(XSD_EXECUTABLE)
       set(XSD_FOUND TRUE)
    endif()
endif()

if(XSD_FOUND)
    # find out more information about XSD
    unset(XSD_VERSION_TEXT)
    execute_process(COMMAND ${XSD_EXECUTABLE} --version
                    OUTPUT_VARIABLE XSD_VERSION_TEXT
                    ERROR_VARIABLE  XSD_VERSION_TEXT
                    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    # extract xsd version number
    string(REGEX REPLACE "^.*compiler (.*)[\\r\\n\\t ]*Copyright.*" "\\1" XSD_VERSION "${XSD_VERSION_TEXT}")

    # force c++11 version if possible
    if(${XSD_VERSION} VERSION_LESS "4.0.0")
        set(XSD_CXX_STD_FLAG "")
    else()
        set(XSD_CXX_STD_FLAG "--std;c++11") # use ";" so that it will be escaped as " " in the xsd command line arg
    endif()
    
    if(NOT XSD_FIND_QUIETLY)
        message(STATUS "Found XSD executable: ${XSD_EXECUTABLE} version ${XSD_VERSION}")
    endif()
else()
    if(XSD_FIND_REQUIRED)
        message(FATAL_ERROR "Cannot find xsdcxx command: please install XSD / check xsd binary is in your sytem PATH.")
    endif()
endif()

mark_as_advanced(
  XSD_INCLUDE_DIR
  XSD_EXECUTABLE
  XSD_VERSION
  XSD_CXX_STD_FLAG
) 
