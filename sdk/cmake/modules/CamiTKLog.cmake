#-----------------------------------
# CamiTK Log configuration
#-----------------------------------

# --- Log in file or not
# Option to remove completely the log message
option(CAMITK_DISABLE_LOG "Completely Disable Log Messages (faster but without any log)" OFF)

# Follow the option policy
if(CAMITK_DISABLE_LOG)
    add_definitions(-DCAMITK_DISABLE_LOG)
endif()
