#!
#! @ingroup group_sdk_cmake_camitk
#!
#! macro camitk_translate allows to perform the translation of any project (action, component, application and libraries) in several languages.
#!
#! usage:
#! \code
#! camitk_translate(
#!     [EXTRA_LANGUAGE lang1 lang2...]
#!     [USE_FLAGS]
#! )
#! \endcode
#!
#! \param EXTRA_LANGUAGE optional, allows the translation of this additional language
#! \param USE_FLAGS   optional, use the flag directory to associate a flag per language
macro(camitk_translate)

    if(NOT CAMITK_DISABLE_TRANSLATION) #To allow enabling or not translation, directly from CMake configure command

        set(options USE_FLAGS)
        set(oneValueArgs "")
        set(multiValueArgs EXTRA_LANGUAGE)
        cmake_parse_arguments(CAMITK_TRANSLATION "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

        #insert here your new language _xx
        set(CAMITK_TRANSLATIONS _en_fr)

        if(CAMITK_TRANSLATION_EXTRA_LANGUAGE)
            set(CAMITK_TRANSLATIONS ${CAMITK_TRANSLATIONS}_${CAMITK_TRANSLATION_EXTRA_LANGUAGE})
        endif()

        #create translate.pro file
        file (GLOB HEADER_FILES *.h *.hpp)
        file (GLOB CPP_FILES *.cpp)
        file (GLOB UI_FILES *.ui)
        set (FILES_TO_TRANSLATE ${FILES_TO_TRANSLATE} ${CPP_FILES} ${UI_FILES} ${HEADER_FILES} CACHE INTERNAL "Files to translate")

        file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro "\nHEADERS = ")
        foreach(F ${HEADER_FILES})
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro ${F})
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro " ")
        endforeach()

        file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro "\nSOURCES = ")
        foreach(F ${CPP_FILES})
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro ${F})
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro " ")
        endforeach()

        file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro "\nFORMS = ")
        foreach(F ${UI_FILES})
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro ${F})
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro " ")
        endforeach()

        file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro "\nTRANSLATIONS = ")

        string(TOLOWER ${CAMITK_TRANSLATIONS} CAMITK_TRANSLATIONS)
        string(LENGTH ${CAMITK_TRANSLATIONS} longueur)
        string(FIND ${CAMITK_TRANSLATIONS} "_" index)

        while(index GREATER -1)
            string(SUBSTRING  ${CAMITK_TRANSLATIONS} ${index} 3 language)

            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro ${CMAKE_CURRENT_BINARY_DIR}/translate${language}.ts)
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro " ")

            string(LENGTH ${CAMITK_TRANSLATIONS} longueur)
            math(EXPR index "${index} + 1 ")
            math(EXPR longueur "${longueur} - ${index}")
            string(SUBSTRING ${CAMITK_TRANSLATIONS} ${index} ${longueur} CAMITK_TRANSLATIONS)

            string(LENGTH ${CAMITK_TRANSLATIONS} longueur)
            string(FIND ${CAMITK_TRANSLATIONS} "_" index)

        endwhile(index GREATER -1)

        #update translate_xx.ts files and create translate_xx.qm files
        set(WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate )
        find_path(QT_BINARY_DIR NAMES lupdate)
        execute_process (COMMAND ${QT_BINARY_DIR}/lupdate -silent ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro)
        find_path(QT_BINARY_DIR NAMES lrelease)
        execute_process (COMMAND ${QT_BINARY_DIR}/lrelease -silent ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro)

        # remove the translate.pro file
        file(REMOVE ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/translate.pro)


        # create the translate_extension_name.qrc file
        # Remember that the .qrc filename must be unique
        # Also, when having loaded all .qrc files within the application, you have to point on
        # each .qm file separately (i.e. use a specific prefix on each .qrc file)
        get_directory_name(${CMAKE_CURRENT_SOURCE_DIR} EXTENSION_NAME)

        #create the translate.qrc file
        set(TRANSLATE_QRC_FILE ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate_${EXTENSION_NAME}.qrc)
        file(WRITE ${TRANSLATE_QRC_FILE} "<!DOCTYPE RCC>\n")
        file(APPEND ${TRANSLATE_QRC_FILE} "<RCC version=\"1.0\">\n")
        file(APPEND ${TRANSLATE_QRC_FILE} "\t<qresource prefix=\"/translate_${EXTENSION_NAME}\">\n")

        file (GLOB QM_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate ${CMAKE_CURRENT_SOURCE_DIR}/resources/translate/*.qm)
        foreach(F ${QM_FILES})
            file(APPEND ${TRANSLATE_QRC_FILE} "\t\t<file>translate/${F}</file>\n")
        endforeach(F ${QM_FILES})

        file(APPEND ${TRANSLATE_QRC_FILE} "\t</qresource>\n")
        file(APPEND ${TRANSLATE_QRC_FILE} "</RCC>\n")

        #create the flags.qrc file
        if(CAMITK_TRANSLATION_USE_FLAGS)
            file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/resources/flags.qrc "<!DOCTYPE RCC>\n")
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/flags.qrc "<RCC version=\"1.0\">\n")
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/flags.qrc "\t<qresource prefix=\"/flags\">\n")

            file(GLOB PNG_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/resources ${CMAKE_CURRENT_SOURCE_DIR}/resources/flags/16/*.png)

            foreach(F ${PNG_FILES})
                file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/flags.qrc "\t\t<file>${F}</file>\n")
            endforeach(F ${PNG_FILES})

            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/flags.qrc "\t</qresource>\n")
            file(APPEND ${CMAKE_CURRENT_SOURCE_DIR}/resources/flags.qrc "</RCC>\n")
        endif()
    endif()
endmacro()
