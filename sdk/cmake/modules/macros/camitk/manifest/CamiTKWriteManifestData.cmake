#!
#! @ingroup group_sdk_cmake_camitk_cdash_projects
#!
#! macro camitk_write_manifest_data validates the xml_subprojects variable by ending its xml tag
#! i.e. By adding the closing </Project> xml node, this will close the xml data
#! Then create the '${CMAKE_BINARY_DIR}/Project.xml file
#! Use this file to send to the DashBoard to get the CEP and its subprojects definition
#!
#! Usage:
#! \code
#! camitk_write_manifest_data()
#! \endcode
#!
#! \param CEP_SET (optional)   should be added only if this is a CEP set validation
macro(camitk_write_manifest_data)

    set(options CEP_SET)
    set(oneValueArgs "")
    set(multiValueArgs "")
    cmake_parse_arguments(ADD_SUB_PROJECT_VALIDATE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    # if this a standalone CEP, then write the cdash xml report
    if(NOT CAMITK_EXTENSION_PROJECT_SET OR ADD_SUB_PROJECT_VALIDATE_CEP_SET)

        # Add the CDash dependencies in the correct order
        set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_CORE_LIBRARY_SUBPROJECTS})
        set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_CEP_LIBRARY_SUBPROJECTS})
        set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_APPLICATION_ACTION_SUBPROJECTS})

#         # Add camitk communityedition CEP specific subprojects
#         if(${CAMITK_EXTENSION_PROJECT_SET_NAME} MATCHES "camitk-communityedition")
#             # Add application-testcomponents
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n  <SubProject name=\"application-testcomponents\"> ")
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n    <Dependency name=\"library-camitkcore \"/>")
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n    <Dependency name=\"action-application \"/>")
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "  </SubProject>")
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_COMPONENT_SUBPROJECTS})
#             # Add application-testactions
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n  <SubProject name=\"application-testactions\"> ")
#             list(REMOVE_DUPLICATES CAMITK_COMPONENT_TARGETS) # avoid duplicates
#             foreach(DEPENDENCY ${CAMITK_COMPONENT_TARGETS})
#                 set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n    <Dependency name=\"${DEPENDENCY}\"/>")
#             endforeach()
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "  </SubProject>")
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_ACTION_SUBPROJECTS})
#             set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_APPLICATION_SUBPROJECTS})
# 
#             # Add packaging source, packaging and api_doc target (COMMENTED > let's be frank, those are not compiled anymore on VMs)
# #            set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n  <SubProject name=\"package-source\">\n  </SubProject>")
# #            set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n  <SubProject name=\"package\">\n  </SubProject>")
# #            set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n  <SubProject name=\"camitk-ce-api-doc\">\n  </SubProject>")
#         else() # Other external CEP
            set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_COMPONENT_SUBPROJECTS})
            set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_ACTION_SUBPROJECTS})
            set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} ${CAMITK_APPLICATION_SUBPROJECTS})
#         endif()

        # Write down the Project.xml file
        set(CAMITK_SUBPROJECTS ${CAMITK_SUBPROJECTS} "\n</Project>")
        set(CAMITK_SUBPROJECTS_FILENAME "${CMAKE_BINARY_DIR}/Project.xml")
        file(WRITE ${CAMITK_SUBPROJECTS_FILENAME} ${CAMITK_SUBPROJECTS})
        message(STATUS "Generated ${CAMITK_SUBPROJECTS_FILENAME} for project description")

        # Write down subprojects.cmake file
        list(REMOVE_DUPLICATES CAMITK_TARGETS)
        set(CAMITK_SUBPROJECTS_FILENAME "${CMAKE_BINARY_DIR}/Subprojects.cmake")
        file(WRITE ${CAMITK_SUBPROJECTS_FILENAME} "set(CAMITK_TARGETS ${CAMITK_TARGETS})")
        message(STATUS "Generated ${CAMITK_SUBPROJECTS_FILENAME} for subprojects listing")
    endif()
endmacro()
