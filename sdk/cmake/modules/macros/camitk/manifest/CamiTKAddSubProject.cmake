#!
#! @ingroup group_sdk_cmake_camitk_cdash_projects
#!
#! macro camitk_register_subproject adds a subproject definition for the CDash report
#! 
#! Subprojects in CDash shows configure/build/test information by subprojects.
#! In CamiTK there are 6 types of subprojects:
#! - the corelib (only present in the SDK)
#! - the cep library (one subproject per library of the CEP)
#! - the application action (only present in the SDK for application actions)
#! - the component (one subproject per component extension of the CEP)
#! - the action (one subproject per action extension of the CEP)
#! - the application (one subproject per application of the CEP)
#!
#! Note: application actions are special actions grouped into a specific action extension.
#! This unique action extension is mandatory for any CamiTK application to run properly
#! (they include basic actions such as "save", "open"...)
#! 
#! macro camitk_register_subproject is called to add one subproject and define:
#! - the target 
#! - and its dependencies (if any)
#!
#! This macro should be called once for each corelib, cep library component, action, and application
#! of the CEP.
#! This macro is called automatically by the camitk corresponding CMake macros
#!
#! This macro adds an XML fragment that will be understood by CDash in the
#! corresponding global cache variables:
#! - CAMITK_CORE_LIBRARY_SUBPROJECTS is the XML fragment describing the CamiTK Core library (only found in the SDK)
#! - CAMITK_CEP_LIBRARY_SUBPROJECTS is the XML fragment describing the library created by the CEP
#! - CAMITK_APPLICATION_ACTION_SUBPROJECTS is the XML fragment describing the application actions (only found in the SDK)
#! - CAMITK_COMPONENT_SUBPROJECTS is the XML fragment describing the component extensions created by the CEP
#! - CAMITK_ACTION_SUBPROJECTS is the XML fragment describing the action extensions created by the CEP
#! - CAMITK_APPLICATION_SUBPROJECTS is the XML Fragment describing applications created by the CEP
#!
#! Usage:
#! \code
#! camitk_register_subproject(CORELIB|CEP_LIBRARY|COMPONENT|ACTION|APPLICATION targetName
#!                             [DEPENDENCIES dep1 dep2 dep3]
#! )
#! \endcode
#!
#! \param TARGET_TYPE name              the library / exe program targeted for CMake compilation
#! \param DEPENDENCIES (optional)       the library dependencies to build the target
macro(camitk_register_subproject)

    set(options "")
    set(oneValueArgs ACTION COMPONENT CEP_LIBRARY APPLICATION CORELIB)
    set(multiValueArgs DEPENDENCIES)
    cmake_parse_arguments(ADD_SUB_PROJECT "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    
    #
    # 1. get the subproject name and add it to the corresponding target list
    #
    
    # CORE LIBRARIES
    if(ADD_SUB_PROJECT_CORELIB)
        set(SUB_PROJECT_NAME ${ADD_SUB_PROJECT_CORELIB})
    endif()
    
    # LIBRARY
    if(ADD_SUB_PROJECT_CEP_LIBRARY)
        set(SUB_PROJECT_NAME ${ADD_SUB_PROJECT_CEP_LIBRARY})
        # Add the project to the corresponding list
        set(CAMITK_CEP_LIBRARY_TARGETS ${CAMITK_CEP_LIBRARY_TARGETS} ${SUB_PROJECT_NAME} CACHE STRING "List of library targets" FORCE)
    endif()
    
    # COMPONENTS
    if(ADD_SUB_PROJECT_COMPONENT)
        set(SUB_PROJECT_NAME ${ADD_SUB_PROJECT_COMPONENT})
        # Add the project to the corresponding list
        set(CAMITK_COMPONENT_TARGETS ${CAMITK_COMPONENT_TARGETS} ${SUB_PROJECT_NAME} CACHE STRING "List of component extension targets" FORCE)
    endif()
    
    # ACTIONS
    if(ADD_SUB_PROJECT_ACTION)
        set(SUB_PROJECT_NAME ${ADD_SUB_PROJECT_ACTION})
        # Add the project to the corresponding list
        set(CAMITK_ACTION_TARGETS ${CAMITK_ACTION_TARGETS} ${SUB_PROJECT_NAME} CACHE STRING "List of action extension targets" FORCE)
    endif()
    
    # APPLICATION
    if(ADD_SUB_PROJECT_APPLICATION)
        set(SUB_PROJECT_NAME ${ADD_SUB_PROJECT_APPLICATION})
        # Add the project to the corresponding list
        set(CAMITK_APPLICATION_TARGETS ${CAMITK_APPLICATION_TARGETS} ${SUB_PROJECT_NAME} CACHE STRING "List of application targets" FORCE)
    endif()
    
    #
    # 2. Create the XML fragment (including name and dependency list
    #
    set(SUBPROJECT_XML_FRAGMENT "\n  <SubProject name=\"${SUB_PROJECT_NAME}\">")
    if(SUB_PROJECT_NAME STREQUAL "action-application")
        # Particular case of action-applications: only depends on CamiTK core library
        set(SUBPROJECT_XML_FRAGMENT ${SUBPROJECT_XML_FRAGMENT} "<Dependency name=\"library-camitkcore\"/>")
    else()
        # concatenate all dependencies
        foreach(DEPENDENCY ${ADD_SUB_PROJECT_DEPENDENCIES})
                set(SUBPROJECT_XML_FRAGMENT ${SUBPROJECT_XML_FRAGMENT} "<Dependency name=\"${DEPENDENCY}\"/>")
        endforeach()        
    endif()
    set(SUBPROJECT_XML_FRAGMENT ${SUBPROJECT_XML_FRAGMENT} "</SubProject>")
    
    #
    # 3. add the project to the list of the CEP/CEP set subprojects
    #
    set(CAMITK_TARGETS ${CAMITK_TARGETS} ${SUB_PROJECT_NAME} CACHE INTERNAL "")

    #
    # 4. add the xml fragment for the corresponding subproject XML fragment
    #

    # CORE LIBRARIES
    if(ADD_SUB_PROJECT_CORELIB)
        set(CAMITK_CORE_LIBRARY_SUBPROJECTS ${CAMITK_CORE_LIBRARY_SUBPROJECTS} ${SUBPROJECT_XML_FRAGMENT} CACHE INTERNAL "XML description of Core library subprojects")
    endif()
    
    # LIBRARY
    if(ADD_SUB_PROJECT_CEP_LIBRARY)
        set(CAMITK_CEP_LIBRARY_SUBPROJECTS ${CAMITK_CEP_LIBRARY_SUBPROJECTS} ${SUBPROJECT_XML_FRAGMENT} CACHE INTERNAL "XML description of library subprojects")
    endif()
    
    # COMPONENTS
    if(ADD_SUB_PROJECT_COMPONENT)
        set(CAMITK_COMPONENT_SUBPROJECTS ${CAMITK_COMPONENT_SUBPROJECTS} ${SUBPROJECT_XML_FRAGMENT} CACHE INTERNAL "XML description of component extension subprojects")
    endif()
    
    # ACTIONS
    if(ADD_SUB_PROJECT_ACTION)
        # Particular case of action-applications
        if(SUB_PROJECT_NAME STREQUAL "action-application")
            set(CAMITK_APPLICATION_ACTION_SUBPROJECTS ${CAMITK_APPLICATION_ACTION_SUBPROJECTS} ${SUBPROJECT_XML_FRAGMENT} CACHE INTERNAL "XML description of application action extension subprojects")
        else()
            set(CAMITK_ACTION_SUBPROJECTS ${CAMITK_ACTION_SUBPROJECTS} ${SUBPROJECT_XML_FRAGMENT} CACHE INTERNAL "XML description of action extension subprojects")
        endif()
    endif()
    
    # APPLICATION
    if(ADD_SUB_PROJECT_APPLICATION)
        # Add the xml fragment for the current subproject
        set(CAMITK_APPLICATION_SUBPROJECTS ${CAMITK_APPLICATION_SUBPROJECTS} ${SUBPROJECT_XML_FRAGMENT} CACHE INTERNAL "XML description of application subprojects")
    endif()
    
endmacro()
