#!
#! @ingroup group_sdk_cmake_camitk_packaging
#!
#! CamiTK cep packaging allows you to build a package from a CEP
#! If README and COPYRIGHT files exists at top source dir, then
#! they will be included in the source package
#!
#! Usage:
#! \code
#! camitk_cep_packaging(DESCRIPTION "CEP package description"
#!                      [NAME packageName]
#!                      [VENDOR vendorName]
#!                      [CONTACT contactName]
#!                      [LICENSE "This is the license text..."]
#! )
#! \endcode
#!
#  \param DESCRIPTION                   Some comments on what is this CEP package about
#! \param NAME (optional)               Specify the package name (default is project name)
#! \param VENDOR (optional)             Specify the vendor name (default is TIMC-IMAG)
#! \param CONTACT (optional)            Specify the contact name (default is link to CamiTK website)
#! \param LICENSE (optional)            Specify the license test
#!
#! When configuring CMake, do not forget to set the CMAKE_INSTALL_PREFIX correctly (e.g., /usr on Linux)
#! It will always use the CamiTK install directory as install prefix (install where the core is already)
#!
#! To generate deb package add this to the cmake configuration line: -DCPACK_BINARY_DEB:BOOL=TRUE
#! To generate rpm package add this to the cmake configuration line: -DCPACK_BINARY_RPM:BOOL=TRUE
macro(camitk_cep_packaging)

#  CEP Packaging only works for stand-alone CEP
if (NOT CAMITK_EXTENSION_PROJECT_SET)
    
set(options "")
set(oneValueArgs NAME VENDOR CONTACT DESCRIPTION LICENSE)
set(multiValueArgs "")
cmake_parse_arguments(CAMITK_CEP_PACKAGING "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  

# Clean list of targets by removing duplicates
# Those may appear if configuring more than once (i.e. by manually clicking more than once on the CMake configure 
# button before generating
list(REMOVE_DUPLICATES CAMITK_ACTION_TARGETS)
list(REMOVE_DUPLICATES CAMITK_COMPONENT_TARGETS)
list(REMOVE_DUPLICATES CAMITK_CEP_LIBRARY_TARGETS)
list(REMOVE_DUPLICATES CAMITK_APPLICATION_TARGETS)

# package named
if(CAMITK_CEP_PACKAGING_NAME)
    set(PACKAGING_NAME ${CAMITK_CEP_PACKAGING_NAME})
else()
    # package name is project name
    set(PACKAGING_NAME ${CMAKE_PROJECT_NAME})
endif()

# simply name so that the custom target name or file does not contains any space and are all lowercase
string(REGEX REPLACE " " "" ESCAPED_PACKAGING_NAME "${PACKAGING_NAME}")
string(TOLOWER "${ESCAPED_PACKAGING_NAME}" CPACK_PACKAGE_NAME)

# set version number in case cep does not give any values
if(CAMITK_CEP_PACKAGING_VERSION_MAJOR)
    set(CPACK_PACKAGE_VERSION_MAJOR ${CAMITK_CEP_PACKAGING_VERSION_MAJOR})
else()
    set(CPACK_PACKAGE_VERSION_MAJOR ${CAMITK_VERSION_MAJOR})
endif()
if(CAMITK_CEP_PACKAGING_VERSION_MINOR)
    set(CPACK_PACKAGE_VERSION_MINOR ${CAMITK_CEP_PACKAGING_VERSION_MINOR})
else()
    set(CPACK_PACKAGE_VERSION_MINOR ${CAMITK_VERSION_MINOR})
endif()
if(CAMITK_CEP_PACKAGING_VERSION_PATCH)
    set(CPACK_PACKAGE_VERSION_PATCH ${CAMITK_CEP_PACKAGING_VERSION_PATCH})
else()
    set(CPACK_PACKAGE_VERSION_PATCH ${CAMITK_VERSION_PATCH})
endif()

# contact information
if(CAMITK_CEP_PACKAGING_VENDOR)
    set(CPACK_PACKAGE_VENDOR ${CAMITK_CEP_PACKAGING_VENDOR})
else()
    set(CPACK_PACKAGE_VENDOR "Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525")
endif()
if (CAMITK_CEP_PACKAGING_CONTACT)
    set(CPACK_PACKAGE_CONTACT ${CAMITK_CEP_PACKAGING_CONTACT})
else()
    set(CPACK_PACKAGE_CONTACT "http://camitk.imag.fr")
endif()

# resource files
if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/README")
    set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README")
endif()
if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/COPYRIGHT")
    set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/COPYRIGHT")
endif()

# applications / executable
set(EXECUTABLES_TO_PACKAGE)
foreach(APPLICATION_TARGET_NAME ${CAMITK_APPLICATION_TARGETS})
        # name and label are the same
        set(EXECUTABLES_TO_PACKAGE ${EXECUTABLES_TO_PACKAGE} ${APPLICATION_TARGET_NAME} ${APPLICATION_TARGET_NAME})
endforeach()
set(CPACK_PACKAGE_EXECUTABLES ${EXECUTABLES_TO_PACKAGE})

# package description
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY ${CAMITK_CEP_PACKAGING_DESCRIPTION})

# Files to ignore during generation of the package
set(CPACK_SOURCE_IGNORE_FILES
"~$"
"/CVS/"
"tags"
"/\\\\.svn/"
"/\\\\.git/"
"/\\\\.kdev4/"
"/kdev4$/"
"/build/"
"\\\\.kdevses$"
"\\\\.kdev4$"
"\\\\.kdevelop\\\\.pcs$"
".DS_Store"
${CPACK_SOURCE_IGNORE_FILES}
)

set(CPACK_SOURCE_GENERATOR "TGZ" "ZIP" )

# ----------------------------------
# Unix packages (Ubuntu and Debian)
# ----------------------------------
if (UNIX AND NOT WIN32)
        message(STATUS "Packaging ${CPACK_PACKAGE_NAME} for UNIX systems")
        # Try to find architecture
        execute_process(COMMAND uname -m OUTPUT_VARIABLE CPACK_PACKAGE_ARCHITECTURE)
        string(STRIP "${CPACK_PACKAGE_ARCHITECTURE}" CPACK_PACKAGE_ARCHITECTURE)
        # Try to find distro name and distro-specific arch
        execute_process(COMMAND lsb_release -is OUTPUT_VARIABLE LSB_ID)
        execute_process(COMMAND lsb_release -rs OUTPUT_VARIABLE LSB_RELEASE)
        string(STRIP "${LSB_ID}" LSB_ID)
        string(STRIP "${LSB_RELEASE}" LSB_RELEASE)
        set(LSB_DISTRIB "${LSB_ID}${LSB_RELEASE}")
        if(NOT LSB_DISTRIB)
                set(LSB_DISTRIB "unix")
        endif(NOT LSB_DISTRIB)
        message(STATUS "Packager architecture : ${LSB_DISTRIB}")

        #Find DPKG
        find_program(DPKG
                NAMES dpkg-deb
                PATHS "/usr/bin" #Add paths here
        )
        if ( DPKG )
                get_filename_component(DPKG_PATH ${DPKG} ABSOLUTE)
                message(STATUS "Packaging deb using ${DPKG_PATH}: OK")
                set(DPKG_FOUND "YES")
        else()
                set(DPKG_FOUND "NO")
        endif()

        # For Debian-based distribs we want to create DEB packages.
        if("${LSB_DISTRIB}" MATCHES "Ubuntu|Debian")

                # We need to alter the architecture names as per distro rules
                if("${CPACK_PACKAGE_ARCHITECTURE}" MATCHES "i[3-6]86")
                        set(CPACK_PACKAGE_ARCHITECTURE i386)
                endif()
                if("${CPACK_PACKAGE_ARCHITECTURE}" MATCHES "x86_64")
                        set(CPACK_PACKAGE_ARCHITECTURE amd64)
                endif()
                # Set the dependencies based on the distrib version

                # Installation path
                # Set the install location to "/usr"
                # set(CAMITK_INSTALL_ROOT "/usr") # Set the CamiTK install root path in your CMake command line / GUI.
                set(CPACK_PACKAGING_INSTALL_PREFIX "/usr")
                set(CPACK_PACKAGE_DEFAULT_LOCATION "/usr")
                set(CPACK_SET_DESTDIR TRUE)

                # Dependencies depending on the Operating System.
                # Ubuntu11.10
                if("${LSB_DISTRIB}" MATCHES "Ubuntu11.10")
                        set(CPACK_DEBIAN_PACKAGE_DEPENDS "libqt4-dev, libinsighttoolkit3.20, libxml2, libvtk5.6-qt4, libxerces-c3.1 ")
                endif()
                # For Ubuntu12.04
                if("${LSB_DISTRIB}" MATCHES "Ubuntu12.04")
                        set(LSB_DISTRIB "Ubuntu12.xx")
                        set(CPACK_DEBIAN_PACKAGE_DEPENDS "libqt4-dev, libinsighttoolkit3.20, libxml2, libvtk5.8-qt4, libxerces-c3.1 ")
                endif()
                if("${LSB_DISTRIB}" MATCHES "Ubuntu12.04.1")
                    set(LSB_DISTRIB "Ubuntu12.xx")
                    set(CPACK_DEBIAN_PACKAGE_DEPENDS "libqt4-dev, libinsighttoolkit3.20, libxml2, libvtk5.8-qt4, libxerces-c3.1 ")
                endif()
                # For Ubuntu12.10
                if("${LSB_DISTRIB}" MATCHES "Ubuntu12.10")
                    set(LSB_DISTRIB "Ubuntu12.xx")
                    set(CPACK_DEBIAN_PACKAGE_DEPENDS "libqt4-dev, libinsighttoolkit3.20, libxml2, libvtk5.8-qt4, libxerces-c3.1 ")
                endif()
                
                # TODO Debian dependencies (see CamiTKPackaging.cmake)

                if(NOT CPACK_DEBIAN_PACKAGE_DEPENDS)
                        message(STATUS "Packaging for ${LSB_DISTRIB} not supported yet.\nPlease set deps in CamiTKCEPPackaging.cmake before packaging.")
                endif()

        endif()
endif()



if(WIN32 AND NOT UNIX)
        # -----------------------
        # NSIS Windows installer
        # -----------------------

        # Update package name for windows release
        set(LSB_DISTRIB "win32")
        set(CPACK_PACKAGE_ARCHITECTURE i386)
        string(TOLOWER "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}-${CAMITK_VERSION_NICKNAME}-${LSB_DISTRIB}" CPACK_PACKAGE_FILE_NAME)

        # Try to find Makensis on Linux distribution
        find_program(MAKENSIS
          NAMES makensis
          PATHS "/usr/bin"
        )

        if(MAKENSIS)
            set(ITK_ROOT_DIR "C:/dev/ITK/3.20.1")

            # MinGW compiler
            if ("${CMAKE_GENERATOR}" MATCHES "MinGW Makefiles")
                # NSIS Windows installer
                message(STATUS "Packaging nsis using ${MAKENSIS} with MinGW compiler")
                # Qt dependencies
                set(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS
                  ##TODO use GetPrerequisites.cmake (i.e. find a better way to list dependencies)
                  ##get_prerequisites() is not well enough documented to justify the time investement - EP May 2011
         ${QT_BINARY_DIR}/QtCore4.dll
         ${QT_BINARY_DIR}/QtGui4.dll
         ${QT_BINARY_DIR}/QtSvg4.dll
         ${QT_BINARY_DIR}/QtXml4.dll
         ${QT_BINARY_DIR}/QtNetwork4.dll
         ${QT_BINARY_DIR}/QtSql4.dll
         ${QT_BINARY_DIR}/QtWebKit4.dll
         # VTK dependencies
         ${VTK_LIBRARY_DIRS}/libQVTK.dll
         ${VTK_LIBRARY_DIRS}/libvtkCommon.dll
         ${VTK_LIBRARY_DIRS}/libvtksys.dll
         ${VTK_LIBRARY_DIRS}/libvtkFiltering.dll
         ${VTK_LIBRARY_DIRS}/libvtkGraphics.dll
         ${VTK_LIBRARY_DIRS}/libvtkHybrid.dll
         ${VTK_LIBRARY_DIRS}/libvtkIO.dll
         ${VTK_LIBRARY_DIRS}/libvtkImaging.dll
         ${VTK_LIBRARY_DIRS}/libvtkRendering.dll
         ${VTK_LIBRARY_DIRS}/libvtkVolumeRendering.dll
         ${VTK_LIBRARY_DIRS}/libvtkverdict.dll
         ${VTK_LIBRARY_DIRS}/libvtkDICOMParser.dll
         ${VTK_LIBRARY_DIRS}/libvtkNetCDF.dll
         ${VTK_LIBRARY_DIRS}/libvtkNetCDF_cxx.dll
         ${VTK_LIBRARY_DIRS}/libvtkexpat.dll
         ${VTK_LIBRARY_DIRS}/libvtkjpeg.dll
         ${VTK_LIBRARY_DIRS}/libvtkmetaio.dll
         ${VTK_LIBRARY_DIRS}/libvtkpng.dll
         ${VTK_LIBRARY_DIRS}/libvtktiff.dll
         ${VTK_LIBRARY_DIRS}/libvtkzlib.dll
         ${VTK_LIBRARY_DIRS}/libvtkInfovis.dll
         ${VTK_LIBRARY_DIRS}/libvtkfreetype.dll
         ${VTK_LIBRARY_DIRS}/libvtkftgl.dll
         ${VTK_LIBRARY_DIRS}/libvtkalglib.dll
         ${VTK_LIBRARY_DIRS}/libvtklibxml2.dll
         ${VTK_LIBRARY_DIRS}/libvtkViews.dll
         ${VTK_LIBRARY_DIRS}/libvtkexoIIc.dll
         ${VTK_LIBRARY_DIRS}/libvtkWidgets.dll
         ${VTK_LIBRARY_DIRS}/vtkhdf5.dll
         ${VTK_LIBRARY_DIRS}/vtkhdf5_hl.dll
         ${VTK_LIBRARY_DIRS}/libLSDyna.dll
         # Xerces-c dependency
         # ${XERCESC_ROOT_DIR}/bin/xerces-c_3_1.dll
         # Libxml2 dependencies
         # ${LIBXML2_INCLUDE_DIR}/../bin/iconv.dll
         C:/dev/libxml2/2.8/bin/libxml2-2.dll
         # ${LIBXML2_INCLUDE_DIR}/../bin/zlib1.dll
         # ITK dependency
         ${ITK_ROOT_DIR}/bin/libITKAlgorithms.dll
         ${ITK_ROOT_DIR}/bin/libITKCommon.dll
         ${ITK_ROOT_DIR}/bin/libITKBasicFilters.dll
         ${ITK_ROOT_DIR}/lib/InsightToolkit/libitksys.dll
         ${ITK_ROOT_DIR}/bin/libitkvnl.dll
         ${ITK_ROOT_DIR}/bin/libitkvnl_algo.dll
         ${ITK_ROOT_DIR}/bin/libITKIO.dll
         ${ITK_ROOT_DIR}/bin/libitkv3p_lsqr.dll
         ${ITK_ROOT_DIR}/bin/libitkv3p_netlib.dll
         ${ITK_ROOT_DIR}/bin/libITKDICOMParser.dll
         ${ITK_ROOT_DIR}/bin/libITKEXPAT.dll
         ${ITK_ROOT_DIR}/bin/libitkgdcm.dll
         ${ITK_ROOT_DIR}/bin/libitkjpeg8.dll
         ${ITK_ROOT_DIR}/bin/libITKMetaIO.dll
         ${ITK_ROOT_DIR}/bin/libITKniftiio.dll
         ${ITK_ROOT_DIR}/bin/libITKNrrdIO.dll
         ${ITK_ROOT_DIR}/bin/libitkpng.dll
         ${ITK_ROOT_DIR}/bin/libitktiff.dll
         ${ITK_ROOT_DIR}/bin/libitkzlib.dll
         ${ITK_ROOT_DIR}/bin/libitkjpeg12.dll
         ${ITK_ROOT_DIR}/bin/libitkjpeg16.dll
         ${ITK_ROOT_DIR}/bin/libitkopenjpeg.dll
         ${ITK_ROOT_DIR}/bin/libITKznz.dll
         # MinGW Dependencies
         C:/dev/MinGW/bin/libgcc_s_dw2-1.dll
         C:/dev/MinGW/bin/libstdc++-6.dll
         C:/dev/MinGW/bin/mingwm10.dll
         )

      endif()

        # MSVC compiler
    if (MSVC)
      
        message(STATUS "Packaging nsis using ${MAKENSIS} with MSVC compiler")

        set(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS 
        ##TODO use GetPrerequisites.cmake (i.e. find a better way to list dependencies)
        ##get_prerequisites() is not well enough documented to justify the time investement - EP May 2011
        # Qt dependencies
        ${QT_BINARY_DIR}/QtCore4.dll 
        ${QT_BINARY_DIR}/QtGui4.dll
        ${QT_BINARY_DIR}/QtSvg4.dll
        ${QT_BINARY_DIR}/QtXml4.dll
        ${QT_BINARY_DIR}/QtNetwork4.dll
        ${QT_BINARY_DIR}/QtSql4.dll
        ${QT_BINARY_DIR}/QtWebKit4.dll
        # VTK dependencies
        ${VTK_DIR}/../../bin/QVTK.dll 
        ${VTK_DIR}/../../bin/vtkCommon.dll 
        ${VTK_DIR}/../../bin/vtksys.dll 
        ${VTK_DIR}/../../bin/vtkFiltering.dll 
        ${VTK_DIR}/../../bin/vtkGraphics.dll 
        ${VTK_DIR}/../../bin/vtkHybrid.dll 
        ${VTK_DIR}/../../bin/vtkIO.dll 
        ${VTK_DIR}/../../bin/vtkImaging.dll
        ${VTK_DIR}/../../bin/vtkRendering.dll 
        ${VTK_DIR}/../../bin/vtkVolumeRendering.dll
        ${VTK_DIR}/../../bin/vtkverdict.dll 
        ${VTK_DIR}/../../bin/vtkDICOMParser.dll 
        ${VTK_DIR}/../../bin/vtkNetCDF.dll 
        ${VTK_DIR}/../../bin/vtkNetCDF_cxx.dll
        ${VTK_DIR}/../../bin/vtkexpat.dll 
        ${VTK_DIR}/../../bin/vtkjpeg.dll 
        ${VTK_DIR}/../../bin/vtkmetaio.dll 
        ${VTK_DIR}/../../bin/vtkpng.dll 
        ${VTK_DIR}/../../bin/vtktiff.dll 
        ${VTK_DIR}/../../bin/vtkzlib.dll 
        ${VTK_DIR}/../../bin/vtkInfovis.dll 
        ${VTK_DIR}/../../bin/vtkfreetype.dll 
        ${VTK_DIR}/../../bin/vtkftgl.dll
        ${VTK_DIR}/../../bin/vtkalglib.dll 
        ${VTK_DIR}/../../bin/vtklibxml2.dll 
        ${VTK_DIR}/../../bin/vtkViews.dll 
        ${VTK_DIR}/../../bin/vtkexoIIc.dll
        ${VTK_DIR}/../../bin/vtkWidgets.dll  
        ${VTK_DIR}/../../bin/vtkhdf5.dll 
        ${VTK_DIR}/../../bin/vtkhdf5_hl.dll 
        ${VTK_DIR}/../../bin/LSDyna.dll
        # Xerces-c dependency
        ${XERCESC_ROOT_DIR}/bin/xerces-c_3_1.dll 
        # Libxml2 dependencies
        ${LIBXML2_INCLUDE_DIR}/../bin/iconv.dll
        ${LIBXML2_INCLUDE_DIR}/../bin/libxml2.dll
        ${LIBXML2_INCLUDE_DIR}/../bin/zlib1.dll
        # ITK dependency 
        ${ITK_DIR}/../../bin/ITKCommon.dll
        )
    endif()
      
    # Copy those dependencies DLLs if packaging on Windows.
    if (PACKAGING_NSIS)
        include(InstallRequiredSystemLibraries)
    endif()

    # Application icon
    set(CAMITK_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}\\\\core\\\\resources\\\\appIcon.ico") #set here the current icon for CamiTK apps
    set(CPACK_PACKAGE_ICON ${CAMITK_PACKAGE_ICON}) #icon for the top bar NSIS installer
    set(CPACK_NSIS_MUI_ICON ${CAMITK_PACKAGE_ICON}) #icon for the generated install program (the .exe to run to install CamiTK).
    set(CPACK_NSIS_MUI_UNIICON ${CAMITK_PACKAGE_ICON}) #icon for the generated uninstall program (the .exe to run to uninstall CamiTK).


            # Installer name
            set(CPACK_NSIS_DISPLAY_NAME "${CPACK_PACKAGE_NAME} ${CAMITK_VERSION_MAJOR}.${CAMITK_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
            set(CPACK_NSIS_INSTALLED_ICON_NAME ${CPACK_PACKAGE_NAME})

            set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CPACK_PACKAGE_NAME})
            set(CPACK_NSIS_HELP_LINK "http:\\\\\\\\camitk.imag.fr")
            set(CPACK_NSIS_URL_INFO_ABOUT "http:\\\\\\\\camitk.imag.fr")
            set(CPACK_NSIS_CONTACT ${CPACK_PACKAGE_CONTACT})
            include(InstallRequiredSystemLibraries)
        else()
            message(STATUS "Can not find makensis: nsis packaging is not possible")
        endif()

endif()

# ---------------
# Mac App Bundle
# ---------------
if(APPLE)
    # TODO test this!
    set(CPACK_GENERATOR "PackageMaker")
    # Libraries are bundled directly
    set(CPACK_COMPONENT_LIBRARIES_HIDDEN TRUE)
    # Bundle Properties
    set(MACOSX_BUNDLE_BUNDLE_NAME ${CPACK_PACKAGE_NAME})
    set(MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION})
    set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION})
    set(MACOSX_BUNDLE_LONG_VERSION_STRING "Version ${PROJECT_VERSION}")
endif()

# and here we go...
include(CPack)

# ----------------------------
# CamiTK's own package (.cep)
# ----------------------------
# TODO install a CEP in CamiTK
# TODO CamiTK Market Place

string(TOLOWER "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}-${LSB_DISTRIB}_${CPACK_PACKAGE_ARCHITECTURE}" CPACK_PACKAGE_FILE_NAME)

set(CEP_PACKAGE_FILENAME "${CPACK_PACKAGE_FILE_NAME}.cep")

set(CEP_PACKAGING_DIR "${CMAKE_BINARY_DIR}/CamiTKPackaging")

add_custom_target(camitk-${CPACK_PACKAGE_NAME}-package
    # configure for packaging
    COMMAND ${CMAKE_COMMAND} -DPACKAGING_NSIS:BOOL=ON ${CMAKE_BINARY_DIR}
    # build first
    COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR}
    # clean CEP package structure
    COMMAND ${CMAKE_COMMAND} -E remove_directory "${CEP_PACKAGING_DIR}"
    COMMAND ${CMAKE_COMMAND} -E remove -f ${CMAKE_BINARY_DIR}/${CEP_PACKAGE_FILENAME}
    COMMAND ${CMAKE_COMMAND} -E make_directory "${CEP_PACKAGING_DIR}"
    COMMAND ${CMAKE_COMMAND} -E make_directory "${CEP_PACKAGING_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cep"    
    # copy all cep files
    COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_BINARY_DIR}/lib" "${CEP_PACKAGING_DIR}/lib"
    COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_BINARY_DIR}/bin" "${CEP_PACKAGING_DIR}/bin"
    COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_BINARY_DIR}/include" "${CEP_PACKAGING_DIR}/include"
    COMMAND ${CMAKE_COMMAND} -E copy_directory "${CMAKE_BINARY_DIR}/share" "${CEP_PACKAGING_DIR}/share"
    # copy cep manifest
    COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_BINARY_DIR}/CEPPackageManifest.xml ${CEP_PACKAGING_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cep/${CPACK_PACKAGE_NAME}.xml
    # compress
    COMMAND ${CMAKE_COMMAND} -E tar cvj "${CMAKE_BINARY_DIR}/${CEP_PACKAGE_FILENAME}" "${CEP_PACKAGING_DIR}" 
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    # configure back for normal build
    COMMAND ${CMAKE_COMMAND} -DPACKAGING_NSIS:BOOL=OFF ${CMAKE_BINARY_DIR}
    COMMENT "Building cep package for ${CPACK_PACKAGE_NAME}"
)

#-- Build Manifest
# get the time stamp
execute_process(COMMAND "${CAMITK_DIR}/bin/camitk-config" "--time-stamp"
                RESULT_VARIABLE CAMITK_CONFIG_TIMESTAMP_RETURN_CODE
                OUTPUT_VARIABLE CAMITK_CONFIG_TIMESTAMP_OUTPUT
                ERROR_VARIABLE CAMITK_CONFIG_TIMESTAMP_OUTPUT_ERROR
                OUTPUT_STRIP_TRAILING_WHITESPACE
                ERROR_STRIP_TRAILING_WHITESPACE
)

# Generate XML
set(CEP_PACKAGE_MANIFEST_XML "<?xml version=\"1.0\"?>\n<cep>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <!-- CamiTK Extension Project Manifest File -->")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <name>${CPACK_PACKAGE_NAME}</name>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <contact>${CAMITK_CEP_PACKAGING_CONTACT}</contact>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <description>${CAMITK_CEP_PACKAGING_DESCRIPTION}</description>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <license>${CAMITK_CEP_PACKAGING_LICENSE}</license>")
if (NOT CAMITK_CONFIG_TIMESTAMP_RETURN_CODE)
    set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <timeStamp>${CAMITK_CONFIG_TIMESTAMP_OUTPUT}</timeStamp>")
endif()
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <actions>")
foreach(TARGET_NAME_ITERATOR ${CAMITK_ACTION_TARGETS})
     string(REPLACE "action-" "" TARGET_NAME ${TARGET_NAME_ITERATOR})
     set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "    <action>${TARGET_NAME}</action>")
endforeach()
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  </actions>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <components>")
foreach(TARGET_NAME_ITERATOR ${CAMITK_COMPONENT_TARGETS})
     string(REPLACE "component-" "" TARGET_NAME ${TARGET_NAME_ITERATOR})
     set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "    <component>${TARGET_NAME}</component>")
endforeach()
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  </components>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <libraries>")
foreach(TARGET_NAME_ITERATOR ${CAMITK_CEP_LIBRARY_TARGETS})
     set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "    <library>${TARGET_NAME_ITERATOR}</library>")
endforeach()
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  </libraries>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  <applications>")
foreach(TARGET_NAME_ITERATOR ${CAMITK_APPLICATION_TARGETS})
     string(REPLACE "application-" "" TARGET_NAME ${TARGET_NAME_ITERATOR})
     set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "    <application>${TARGET_NAME}</application>")
endforeach()
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "  </applications>")
set(CEP_PACKAGE_MANIFEST_XML ${CEP_PACKAGE_MANIFEST_XML} "</cep>")
# replace all ";" by \n
string (REGEX REPLACE "([^\\]|^);" "\\1\n" CEP_PACKAGE_MANIFEST_XML_TMP "${CEP_PACKAGE_MANIFEST_XML}")
string (REGEX REPLACE "[\\](.)" "\\1" CEP_PACKAGE_MANIFEST_XML_TEXT "${CEP_PACKAGE_MANIFEST_XML_TMP}") #fixes escaping
# write manifest
file(WRITE ${CMAKE_BINARY_DIR}/CEPPackageManifest.xml ${CEP_PACKAGE_MANIFEST_XML_TEXT})
message(STATUS "Generated CEP manifest in CEPPackageManifest.xml")

        
endif() # Standalone CEP
endmacro()
