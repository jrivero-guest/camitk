#!@ingroup group_sdk_cmake_camitk_packaging
#! 
#! ==================== Source Tar ball ====================
#!
#! To make a source tar ball, use the custom target "camitk-ce-package-source"
#! - make camitk-ce-package-source
#! (it generates the correct CamiTKVersion.h)
#! WARNING it is VERY important to use "make camitk-ce-package-source" and NOT just "make package_source"
#!
#! ==================== Windows User Package ====================
#!
#! To make an installer (setup.exe) for the user package:
#! - configure cmake with -DPACKAGING_NSIS:BOOL=TRUE (on windows AND unix)
#! - use the custom target camitk-package, e.g "make camitk-package"
#! 
#! On Windows platforms
#! CMake configure command (from the build directory) :
#! cmake -DCMAKE_GENERATOR="Visual Studio 12 Win64" -DPACKAGING_NSIS=TRUE -DCEP_IMAGING=TRUE -DCEP_MODELING=TRUE -DAPPLICATION_CEPGENERATOR=FALSE -DAPPLICATION_TESTACTIONS=FALSE -DAPPLICATION_TESTCOMPONENTS=FALSE -DAPPLICATION_WIZARD=FALSE ../src
#! Launch the camitkcommunityedition.sln visual studio solution file. 
#! Build all in Release (do it again if some .h files missing errors occured)
#! Build camitk-package target.
#!
#! On windows for the NSIS packager, to generate different components that can be opt out at install time, see
#! http://www.itk.org/Wiki/CMake:Component_Install_With_CPack
#!
#! More generally see http://www.itk.org/Wiki/CMake:CPackConfiguration
#!
#! ==================== Linux User Package ====================
#!
#! On UNIX platforms
#! cmake -DCMAKE_SKIP_RPATH:BOOL=ON -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=OFF -DCMAKE_BUILD_TYPE:STRING=Release -DCEP_IMAGING:BOOL=TRUE -DCEP_MODELING:BOOL=TRUE -DPACKAGING_NSIS:BOOL=TRUE -DCMAKE_INSTALL_PREFIX=/usr
#!
#! On debian/ubuntu, you need to have package "dpkg" (to build DEB packages) and package "rpm" (to build RPM packages) installed:
#! apt-get install dpkg rpm
#!
#! To check the content of a deb package:
#! dpkg --contents package.deb
#! To install the content of a deb in a subdirectory instead of system-wide:
#! dpkg --vextract package.deb subdirectory
#!
#! There is a bug deb package (cmake 2.8.9 on Debian Wheezy): permission for directories are not rights
#! Here is how to fix it (inspired from https://github.com/paralect/robomongo/blob/master/install/linux/fixup_deb.sh.in )
#! set PACKAGE_FILENAME="camitk-3.3.0-debian-7.x-wheezy_amd64.deb"
#! cd /tmp
#! mkdir camitk-deb
#! dpkg-deb -x $PACKAGE_FILENAME camitk-deb
#! dpkg-deb --control $PACKAGE_FILENAME camitk-deb/DEBIAN
#! rm $PACKAGE_FILENAME
#! find camitk-deb -type d -print0 |xargs -0 chmod 755
#! find camitk-deb -name "*.1" -print0 |xargs -0 chmod 0644
#! find fix_up_deb/usr/bin -name "camitk-*" -print0 |xargs -0 chmod 0755 
#! fakeroot dpkg -b camitk-deb $PACKAGE_FILENAME
#! rm -rf camitk-deb

macro(camitk_communityedition_packaging)
 
#! package name
string(TOLOWER ${CAMITK_PROJECT_NAME} CPACK_PACKAGE_NAME)
set(CPACK_PACKAGE_VERSION_MAJOR ${CAMITK_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${CAMITK_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${CAMITK_VERSION_PATCH})
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
string(TOLOWER "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}-${CAMITK_VERSION_NICKNAME}-${LSB_DISTRIB}_${CPACK_PACKAGE_ARCHITECTURE}" CPACK_PACKAGE_FILE_NAME)

#! contact information
set(CPACK_PACKAGE_VENDOR "Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525")
set(CPACK_PACKAGE_CONTACT "http://camitk.imag.fr")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Emmanuel Promayon <Emmanuel.Promayon@univ-grenoble-alpes.fr>")

#! resource files
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/COPYRIGHT")

#! executable
set(CPACK_PACKAGE_EXECUTABLES "camitk-imp" "camitk-imp" "camitk-actionstatemachine" "camitk-actionstatemachine")

#! CamiTK package description
set(CPACK_PACKAGE_DESCRIPTION "CamiTK stands for Computer Assisted Medical Intervention Tool Kit and aims at providing an easy way to protype medical applications")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "CamiTK stands for Computer Assisted Medical Intervention Tool Kit and aims at providing an easy way to protype medical applications. CamiTK functionnalities are extendable thanks to plugins. This package includes the IMP application with main plugins that allow users to interact with medical images (2D, 3D), process them and simulate biomechanichal deformations. It also includes the actionstatemachine application that allows user to easily run a pipelines of algorithms on images.")

#! Files to ignore during generation of the package
set(CPACK_SOURCE_IGNORE_FILES
"~$"
"/CVS/"
"tags"
"/\\\\.svn/"
"/\\\\.git/"
"/kdev4$/"
"/build/"
"\\\\.kdevses$"
"\\\\.kdev4$"
"\\\\.tmp$"
"\\\\.tmp.save$"
"\\\\.kdevelop\\\\.pcs$"
".DS_Store"
${CPACK_SOURCE_IGNORE_FILES}
)

set(CPACK_SOURCE_GENERATOR "TGZ" "ZIP" )

#! Detect processor bits range (32bits / 64bits)
#! not the whole architecture (ARM ...).
if(CMAKE_SIZEOF_VOID_P EQUAL 8) 
    set(WIN64 TRUE) 
else() 
    set(WIN64 FALSE) 
endif() 


#! ----------------------------------
#! Unix packages (Ubuntu and Debian)
#! ----------------------------------
if (PACKAGING_NSIS AND UNIX AND NOT WIN32)

    #! Note : even if not packaging for Unix with NSIS, the CMake variable 
    #! PACKAGING_NSIS is used here to indicate we are packaging (i.e. building
    #! the camitk-package target).
    
    message(STATUS "Packaging CamiTK for UNIX systems")
    #! Try to find architecture
    execute_process(COMMAND uname -m OUTPUT_VARIABLE CPACK_PACKAGE_ARCHITECTURE)
    string(STRIP "${CPACK_PACKAGE_ARCHITECTURE}" CPACK_PACKAGE_ARCHITECTURE)
    #! Try to find distro name and distro-specific arch
    execute_process(COMMAND lsb_release -is OUTPUT_VARIABLE LSB_ID)
    execute_process(COMMAND lsb_release -rs OUTPUT_VARIABLE LSB_RELEASE)
    string(STRIP "${LSB_ID}" LSB_ID)
    string(STRIP "${LSB_RELEASE}" LSB_RELEASE)
    set(LSB_DISTRIB "${LSB_ID}${LSB_RELEASE}")
    if(NOT LSB_DISTRIB)
        set(LSB_DISTRIB "unix")
    endif(NOT LSB_DISTRIB)
    message(STATUS "Packager architecture : ${LSB_DISTRIB}")

    #! debian section
    set(CPACK_DEBIAN_PACKAGE_SECTION "science")
    
    #!Find DPKG
    find_program(DPKG
        NAMES dpkg-deb
        PATHS "/usr/bin" #!Add paths here
    )
    if ( DPKG )
        get_filename_component(DPKG_PATH ${DPKG} ABSOLUTE)
        message(STATUS "Packaging deb using ${DPKG_PATH}: OK")
        set(DPKG_FOUND "YES")
    else() 
        set(DPKG_FOUND "NO")
    endif()

    #! Packaging requires ITK
    find_package(ITK REQUIRED)
    include(${ITK_USE_FILE})

    #! set the package generator
    set(CPACK_GENERATOR "DEB" "RPM")

    #! For Debian-based distribs we want to create DEB packages.
    if ("${LSB_DISTRIB}" MATCHES "Ubuntu|Debian")

        #! We need to alter the architecture names as per distro rules
        if("${CPACK_PACKAGE_ARCHITECTURE}" MATCHES "i[3-6]86")
            set(CPACK_PACKAGE_ARCHITECTURE i386)
        endif()
        if("${CPACK_PACKAGE_ARCHITECTURE}" MATCHES "x86_64")
            set(CPACK_PACKAGE_ARCHITECTURE amd64)
        endif()
        #! Set the dependencies based on the distrib version

        #! Installation path
        #! Set the install location to "/usr"
        #! PLEASE, set the CamiTK install root path in your CMake command line / GUI (and not directly here)      
        set(CPACK_SET_DESTDIR TRUE)
        set(CPACK_PACKAGING_INSTALL_PREFIX "/usr")
        set(CPACK_PACKAGE_DEFAULT_LOCATION "/usr")
        

        #! Dependencies depending on the Operating System.
        #! Ubuntu12.xx 
        #! WARNING Beware to the syntax of the Debian package dependencies, else it won't manage to install.
        if("${LSB_DISTRIB}" MATCHES "Ubuntu12.04" 
            OR "${LSB_DISTRIB}" MATCHES "Ubuntu12.04.1"
            OR "${LSB_DISTRIB}" MATCHES "Ubuntu12.10"
            OR "${LSB_DISTRIB}" MATCHES "Ubuntu13.04"
            OR "${LSB_DISTRIB}" MATCHES "Ubuntu13.10"
        )
            #! All these ubuntu version dependencies are the same, only one package is needed for all
            set(LSB_DISTRIB "Ubuntu12.04to13.10")
            set(CPACK_DEBIAN_PACKAGE_DEPENDS "cmake, libvtk5-dev (>= 5.8), libqt4-dev, libvtk5-qt4-dev (>= 5.8), xsdcxx, libinsighttoolkit3-dev (>= 3.20), libxerces-c-dev, libxml2-dev, libgdcm2-dev, libfftw3-dev, libqtwebkit-dev | libqt4-dev (<< 4.7), libvtkgdcm2-dev,doxygen, graphviz")
        endif()

        #! Debian dependencies
        if ("${LSB_DISTRIB}" MATCHES "Debian7.*") #! Debian Wheezy
            set(LSB_DISTRIB "Debian-7.x-Wheezy")
            set(CPACK_DEBIAN_PACKAGE_DEPENDS "libqt4-dev (>= 4:4.8), libqtwebkit4 (>= 2), libqtwebkit-dev (>= 2), libvtk5.8 (>= 5.8.0), libvtk5-dev (>= 5.8.0), libvtk5.8-qt4 (>= 5.8.0), libvtk5-qt4-dev (>= 5.8.0), libinsighttoolkit3-dev (>= 3.20.1), vtk-doc (>= 5.8.0), tcl-vtk (>= 5.8.0), libinsighttoolkit3.20 (>= 3.20.1), libtiff4-dev (>= 3.9), libopenjpeg-dev (>= 1.3), libxml2 (>= 2.7.8), libxml2-dev (>= 2.7.8), xsdcxx (>= 3.3.0), libxerces-c-dev (>= 3.1.1), libgdcm2.2 (>= 2), libgdcm2-dev (>= 2), libvtkgdcm2.2 (>= 2), libvtkgdcm2-dev (>= 2)")
        endif()

        if(NOT CPACK_DEBIAN_PACKAGE_DEPENDS)
            message(STATUS "Packaging for ${LSB_DISTRIB} not supported yet.\n")
        endif()

    endif()

    #! package name
    string(TOLOWER "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}-${LSB_DISTRIB}_${CPACK_PACKAGE_ARCHITECTURE}" CPACK_PACKAGE_FILE_NAME)

endif()


#! -----------------------
#! Packaging for Windows
#! -----------------------
if(WIN32 AND MSVC AND PACKAGING_NSIS)

    #! NSIS must be installed on the computer
    find_program(MAKENSIS makensis.exe)
    if(MAKENSIS)
        message(STATUS "NSIS Packaging using ${MAKENSIS}")
    else()
        message(FATAL_ERROR "NSIS executable (makensis.exe) is not installed or not in the system path. Please fix this problem for packaging.")
    endif()

    #! Update package name for windows release
    if(WIN64) #!64 bits installer
        set(CPACK_PACKAGE_ARCHITECTURE amd64)
    else() #!32 bits installer
        set(CPACK_PACKAGE_ARCHITECTURE i386)
    endif()
    string(TOLOWER "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}-${CAMITK_VERSION_NICKNAME}_${CPACK_PACKAGE_ARCHITECTURE}" CPACK_PACKAGE_FILE_NAME)
    
    # get the root installation dir of Qt5
    # Qt5_DIR
    string(REGEX REPLACE "(.*)/lib/.*" "\\1" QT5_ROOT_DIR "${Qt5_DIR}")
    # DEBUG
    # message(STATUS "==========***************************================ QT5_ROOT_DIR = ${QT5_ROOT_DIR}")
    find_file(QT5_PLATFORM_FILE
              qwindows.dll
              PATH_SUFFIXES plugins/platforms
              PATHS ${Qt5_DIR}
              )
    # DEBUG
    # message(STATUS "==========***************************================ QT5_PLATFORM_FILE = ${QT5_PLATFORM_FILE}")
    
    if(NOT QT5_PLATFORM_FILE)
        message(WARNING "Cannot find qwindows.dll: running target 'camitk-package' will result in a buggy NSIS user package")
    else()
        # Instead of putting the same value twice into different variable it would be a good idea to use this mecanism
        # to fill all libraries variables below (as seen in CamiTK meeting)
        set(CPACK_QT5_FILE_TO_INSTALL ${QT5_PLATFORM_FILE})
    endif()
    
    message(STATUS "Packaging CamiTK for Windows: ${CPACK_PACKAGE_FILE_NAME}")
    
    if(WIN64) #! 64bits installer
        #! Qt libraries
        set(QT_PACKAGE_LIBRARIES  
            c:/dev/qt5.6/5.6/msvc2015_64/bin/QT5CORE.DLL
            c:/dev/qt5.6/5.6/msvc2015_64/bin/QT5GUI.DLL
            c:/dev/qt5.6/5.6/msvc2015_64/bin/QT5OPENGL.DLL
            c:/dev/qt5.6/5.6/msvc2015_64/bin/QT5WIDGETS.DLL
            c:/dev/qt5.6/5.6/msvc2015_64/bin/QT5XML.DLL
        )
        
        set(QT_MANDATORY_WIN_RT_DLL
            ${QT5_PLATFORM_FILE}
        )
        
        #! VTK libraries
        set(VTK_PACKAGE_LIBRARIES
            c:/dev/vtk/6.3/bin/VTKALGLIB-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKCOMMONCOLOR-6.3.DLL            
            c:/dev/vtk/6.3/bin/VTKCOMMONCOMPUTATIONALGEOMETRY-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKCOMMONDATAMODEL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKCOMMONEXECUTIONMODEL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKCOMMONMATH-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKCOMMONMISC-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKCOMMONSYSTEM-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKCOMMONTRANSFORMS-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKDICOMPARSER-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSCORE-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSEXTRACTION-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSGENERAL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSGEOMETRY-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSHYBRID-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSMODELING-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSSOURCES-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFILTERSSTATISTICS-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFREETYPE-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKFTGL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKGL2PS-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKGUISUPPORTQT-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKGUISUPPORTQTOPENGL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKIMAGINGCOLOR-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKIMAGINGFOURIER-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKIMAGINGGENERAL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKIMAGINGHYBRID-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKINTERACTIONWIDGETS-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKIOCORE-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKIOEXPORT-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKJPEG-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKMETAIO-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKPNG-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKRENDERINGANNOTATION-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKRENDERINGCONTEXT2D-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKRENDERINGCONTEXTOPENGL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKRENDERINGGL2PS-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKRENDERINGLABEL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKRENDERINGVOLUME-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKRENDERINGVOLUMEOPENGL-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKTIFF-6.3.DLL
            c:/dev/vtk/6.3/bin/VTKZLIB-6.3.DLL
            c:/dev/vtk/6.3/bin/vtkFiltersVerdict-6.3.dll
            c:/dev/vtk/6.3/bin/vtkImagingMath-6.3.dll
            c:/dev/vtk/6.3/bin/vtkImagingStencil-6.3.dll
            c:/dev/vtk/6.3/bin/vtkIOGeometry-6.3.dll
            c:/dev/vtk/6.3/bin/vtkIOImport-6.3.dll
            c:/dev/vtk/6.3/bin/vtkIOLegacy-6.3.dll
            c:/dev/vtk/6.3/bin/vtkverdict-6.3.dll   
        )
       
        #!ITK library
        set(ITK_PACKAGE_LIBRARIES
            C:/dev/ITK/4.9/bin/ITKCommon-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOBioRad-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOBMP-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOGDCM-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOGE-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOGIPL-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOHDF5-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOImageBase-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOIPL-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOJPEG-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOLSM-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOMeta-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOMRC-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIONIFTI-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIONRRD-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOPNG-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOStimulate-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOTIFF-4.9.dll
            C:/dev/ITK/4.9/bin/ITKIOVTK-4.9.dll
        )
         
        #! GDCM libraries
        set(GDCM_PACKAGE_LIBRARIES
            c:/dev/gdcm/2.6.3/bin/VTKCOMMONCORE-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKIMAGINGCORE-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKIMAGINGSOURCES-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKINTERACTIONSTYLE-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKIOIMAGE-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKRENDERINGCORE-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKRENDERINGFREETYPE-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKRENDERINGOPENGL-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/VTKSYS-6.3.DLL
            c:/dev/gdcm/2.6.3/bin/gdcmcharls.dll
            c:/dev/gdcm/2.6.3/bin/gdcmCommon.dll
            c:/dev/gdcm/2.6.3/bin/gdcmDICT.dll
            c:/dev/gdcm/2.6.3/bin/gdcmDSED.dll
            c:/dev/gdcm/2.6.3/bin/gdcmexpat.dll
            c:/dev/gdcm/2.6.3/bin/gdcmgetopt.dll
            c:/dev/gdcm/2.6.3/bin/gdcmIOD.dll
            c:/dev/gdcm/2.6.3/bin/gdcmjpeg8.dll
            c:/dev/gdcm/2.6.3/bin/gdcmjpeg12.dll
            c:/dev/gdcm/2.6.3/bin/gdcmjpeg16.dll
            c:/dev/gdcm/2.6.3/bin/gdcmMSFF.dll
            c:/dev/gdcm/2.6.3/bin/gdcmopenjpeg.dll
            c:/dev/gdcm/2.6.3/bin/gdcmjpeg16.dll
            c:/dev/gdcm/2.6.3/bin/gdcmzlib.dll
            c:/dev/gdcm/2.6.3/bin/vtkgdcm.dll
        )
        
        set(XERCES_C_PACKAGE_LIBRARIES
            C:/dev/xerces-c/3.1.1/bin/xerces-c_3_1.dll
        )
            
    else() #! 32bits installer
                
        #! Qt libraries
        set(QT_PACKAGE_LIRABRIES 
            C:/dev_x86/Qt/4.8.5/bin/QtCore4.dll 
            C:/dev_x86/Qt/4.8.5/bin/QtGui4.dll
            C:/dev_x86/Qt/4.8.5/bin/QtSvg4.dll
            C:/dev_x86/Qt/4.8.5/bin/QtXml4.dll
            C:/dev_x86/Qt/4.8.5/bin/QtNetwork4.dll
            C:/dev_x86/Qt/4.8.5/bin/QtSql4.dll
            C:/dev_x86/Qt/4.8.5/bin/QtWebKit4.dll
        )
        
        #! VTK libraries
        #! set(VTK_DIR ${VTK_DIR}/../../bin)
        set(VTK_PACKAGE_LIBRARIES
            C:/dev_x86/VTK/5.10.1/bin/QVTK.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkCommon.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtksys.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkFiltering.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkGraphics.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkHybrid.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkIO.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkImaging.dll
            C:/dev_x86/VTK/5.10.1/bin/vtkRendering.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkVolumeRendering.dll
            C:/dev_x86/VTK/5.10.1/bin/vtkverdict.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkDICOMParser.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkNetCDF.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkNetCDF_cxx.dll
            C:/dev_x86/VTK/5.10.1/bin/vtkexpat.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkjpeg.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkmetaio.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkpng.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtktiff.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkzlib.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkInfovis.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkfreetype.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkftgl.dll
            C:/dev_x86/VTK/5.10.1/bin/vtkalglib.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtklibxml2.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkViews.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkexoIIc.dll
            C:/dev_x86/VTK/5.10.1/bin/vtkWidgets.dll
            C:/dev_x86/VTK/5.10.1/bin/vtkhdf5.dll 
            C:/dev_x86/VTK/5.10.1/bin/vtkhdf5_hl.dll 
            C:/dev_x86/VTK/5.10.1/bin/LSDyna.dll   
        )

        #!ITK Library
        set(ITK_PACKAGE_LIBRARIES
                C:/dev_x86/ITK/4.3.2/bin/ITKCommon-4.3.dll
            )
         
        #! GDCM libraries
        set(GDCM_PACKAGE_LIBRARIES
            C:/dev_x86/gdcm/2.2.5/bin/vtkgdcm.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmcommon.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmcharls.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmdict.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmdsed.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmexpat.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmiod.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmjpeg12.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmjpeg16.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmjpeg8.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmmsff.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmopenjpeg.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmzlib.dll
            C:/dev_x86/gdcm/2.2.5/bin/gdcmmsff.dll
        )
        
        set(LIBXML2_PACKAGE_LIBRARIES
            C:/dev_x86/libxml2/2.7.8/bin/iconv.dll
            C:/dev_x86/libxml2/2.7.8/bin/libxml2.dll
            C:/dev_x86/libxml2/2.7.8/bin/zlib1.dll
        )
        
        set(XERCES_C_PACKAGE_LIBRARIES
            C:/dev_x86/xerces-c/3.1.1/bin/xerces-c_3_1.dll
        )
    endif() #! end compiler specific settings
    
    #! Additional libraries to install with the installer
    set(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS
        ${QT_PACKAGE_LIBRARIES}
        ${QT_MANDATORY_WIN_RT_DLL}
        ${VTK_PACKAGE_LIBRARIES}
        ${ITK_PACKAGE_LIBRARIES}
        ${GDCM_PACKAGE_LIBRARIES}
        ${LIBXML2_PACKAGE_LIBRARIES}
        ${XERCES_C_PACKAGE_LIBRARIES}
    )
    
    #! By default, do not warn when built on machines using only VS Express:
    if(NOT DEFINED CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS_NO_WARNINGS)
      SET(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS_NO_WARNINGS ON)
    endif()
    
    #! Tell CMake to copy CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS content into the package
    include(InstallRequiredSystemLibraries)
           
    #! Application icon
    set(CAMITK_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}\\\\sdk\\\\libraries\\\\core\\\\resources\\\\appIcon.ico") #!set here the current icon for CamiTK
    set(CPACK_PACKAGE_ICON ${CAMITK_PACKAGE_ICON}) #!icon for the top bar NSIS installer
    set(CPACK_NSIS_MUI_ICON ${CAMITK_PACKAGE_ICON}) #!icon for the generated install program (the .exe to run to install CamiTK).
    set(CPACK_NSIS_MUI_UNIICON ${CAMITK_PACKAGE_ICON}) #!icon for the generated uninstall program (the .exe to run to uninstall CamiTK).

    #! Create desktop icons, by forcing CPack to build an internal command, as 'set(CPACK_NSIS_MODIFY_PATH ON)' option setted in the additional page doesn't work.
    set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS "CreateShortCut \\\"$DESKTOP\\\\${CAMITK_PROJECT_NAME}-imp.lnk\\\" \\\"$INSTDIR\\\\bin\\\\camitk-imp.exe\\\"")
        
    #! Tell CPack to remove the icons after uninstalling
    set(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS "Delete \\\"$DESKTOP\\\\${CAMITK_PROJECT_NAME}-imp.lnk\\\"")
        
    #! Installer name
    set(CPACK_NSIS_DISPLAY_NAME "CamiTK ${CAMITK_VERSION_MAJOR}.${CAMITK_VERSION_MINOR}.${CAMITK_VERSION_PATCH}")
    set(CPACK_NSIS_INSTALLED_ICON_NAME ${CAMITK_PROJECT_NAME})

    set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CAMITK_PROJECT_NAME})
    set(CPACK_NSIS_HELP_LINK "http:\\\\\\\\camitk.imag.fr")
    set(CPACK_NSIS_URL_INFO_ABOUT "http:\\\\\\\\camitk.imag.fr")
    set(CPACK_NSIS_CONTACT ${CPACK_PACKAGE_CONTACT})
      
endif()

#! ---------------
#! Mac App Bundle
#! ---------------
if(PACKAGING_NSIS AND APPLE)
    #! TODO test this!
    set(CPACK_GENERATOR "PackageMaker")
    #! Libraries are bundled directly
    set(CPACK_COMPONENT_LIBRARIES_HIDDEN TRUE)
    #! Bundle Properties
    set(MACOSX_BUNDLE_BUNDLE_NAME ${CPACK_PACKAGE_NAME})
    set(MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION})
    set(MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION})
    set(MACOSX_BUNDLE_LONG_VERSION_STRING "Version ${PROJECT_VERSION}")
endif()



#! Set up CPack configuration
include(CPack)

#! Create the groups that will contains the installer components
cpack_add_component_group(SDK
                          DISPLAY_NAME "CamiTK SDK"
                          DESCRIPTION "The CamiTK SDK features the core and basics extensions. CamiTK installation requires at least the SDK"
                          BOLD_TITLE
                          )
cpack_add_component_group(CEP_IMAGING
                          DISPLAY_NAME "CEP Imaging"
                          DESCRIPTION "The CamiTK Extension Project IMAGING contains image processing extensions. Those extensions are optional."
                          )
cpack_add_component_group(CEP_MODELING
                          DISPLAY_NAME "CEP Modeling"
                          DESCRIPTION "The CamiTK Extension Project MODELING contains biomechanical extensions. Those extensions are optional."
                          )

#! ----------------------------------
#! CamiTK specific packaging targets
#! ----------------------------------

#! create specific target for package source
#! When packaging, copy the CamiTKPackageVersion.h file to the source directory
#! Don't forget to remove it immediatly after packaging.
add_custom_target(camitk-ce-package-source
    COMMAND ${CMAKE_COMMAND}
        -E copy ${CMAKE_CURRENT_BINARY_DIR}/CamiTKPackageVersion.h
                ${CMAKE_CURRENT_SOURCE_DIR}/sdk/libraries/core/CamiTKVersion.h
    COMMAND ${CMAKE_COMMAND}
        --build ${CMAKE_BINARY_DIR}
        --target package_source
    COMMAND ${CMAKE_COMMAND}
        -E remove -f ${CMAKE_CURRENT_SOURCE_DIR}/sdk/libraries/core/CamiTKVersion.h
)

#! This target create the installer / linux package.
#! On windows, you have configured with PACKAGING_NSIS=TRUE => all project names have their "-" replaced by "_" character
#! NSIS doesn't support "-" character.
add_custom_target(camitk-ce-NSIS-Windows-package
    COMMAND ${CMAKE_COMMAND}
        -E copy ${CMAKE_CURRENT_BINARY_DIR}/sdk/libraries/core/CamiTKVersion.h
                ${CMAKE_CURRENT_BINARY_DIR}/sdk/libraries/core/CamiTKVersion.h-backup
    COMMAND ${CMAKE_COMMAND}
        -E copy ${CMAKE_CURRENT_BINARY_DIR}/CamiTKPackageVersion.h
                ${CMAKE_CURRENT_BINARY_DIR}/sdk/libraries/core/CamiTKVersion.h
    COMMAND ${CMAKE_COMMAND}
        --build ${CMAKE_BINARY_DIR}
        --target PACKAGE --config Release 
    COMMAND ${CMAKE_COMMAND}
        -E copy ${CMAKE_CURRENT_BINARY_DIR}/sdk/libraries/core/CamiTKVersion.h-backup
                ${CMAKE_CURRENT_BINARY_DIR}/sdk/libraries/core/CamiTKVersion.h
)

endmacro()
