#------------------------------------------
# Manage CamiTK Test coverage
#-------------------------------------------

# When using gcc, enable test coverage
# using https://github.com/bilke/cmake-modules/blob/master/CodeCoverage.cmake
# requires lcov to be installed on the machine
if(CMAKE_COMPILER_IS_GNUCXX)
    
    option(CAMITK_TEST_COVERAGE "Check this option to generate the 'camitk-ce-test-coverage' target that will create a coverage analysis report in ${PROJECT_BINARY_DIR}/camitk-ce-test-coverage" OFF)    
    
    if (CAMITK_TEST_COVERAGE)
        # CodeCoverage is an additional CMake modules published on github by Lars Bilke.
        # it declares append_coverage_compiler_flags and setup_target_for_coverage macros
        include(CodeCoverage)
        
        # First append gxx flags, this has to be done before any C++ project configuration takes place
        append_coverage_compiler_flags()
        
        # Exclude the following files:
        # - generated source file (in build dir) 
        # - installed library headers from coverage analysis
        set(COVERAGE_EXCLUDES   '${PROJECT_BINARY_DIR}/*' 
                                '/usr/*')
        
        # Defines a specific target to run test coverage and reporting.
        # This will create a specific camitk-ce-test-coverage target 
        # that, when run, will launch all the test known by ctest in parallel 
        # and generate a report in ${PROJECT_BINARY_DIR}/camitk-ce-test-coverage
        # 
        # Note: you can run a quick test by bypassing some tests. 
        #       e.g., to run only one test every 50 tests "-I 1,,50" (ctest documenation: -I Start,End,Stride,test#,test#
        # Note: add "-V" or "-VV" for verbose, add "-R pattern" to run only specific test
        setup_target_for_coverage(NAME camitk-ce-test-coverage 
                                  EXECUTABLE ctest --parallel 5) 
    endif()
    
endif()
