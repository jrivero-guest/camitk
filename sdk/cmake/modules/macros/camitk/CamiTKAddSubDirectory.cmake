#!
#! @ingroup group_sdk_cmake_camitk
#! 
#! camitk_add_subdirectory is a utility macro that add a source subdirectory only if:
#! 1. it exists
#! 2. it contains a CMakeLists
#! This can avoid CMake warning (see cmake --help-policy CMP0014), but do the
#! opposite: it prints out an information message when it succeed including the directory.
#!
#! Usage:
#! \code
#! camitk_add_subdirectory(subdir)
#! \endcode
#!
#! \param subdir (required)          the name of the source subdirectory to include
#!
#! \ingroup group_sdk_cmake
#!
macro(camitk_add_subdirectory DirectoryName)
    if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${DirectoryName}" AND EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${DirectoryName}/CMakeLists.txt")
        set(SUBDIRECTORY_MESSAGE ${CMAKE_PROJECT_NAME})
        if(CEP_NAME AND NOT ${CEP_NAME} STREQUAL ${CMAKE_PROJECT_NAME})
            set(SUBDIRECTORY_MESSAGE "${SUBDIRECTORY_MESSAGE} ${CEP_NAME}")
        endif()
        add_subdirectory(${DirectoryName})
    endif()
endmacro()
