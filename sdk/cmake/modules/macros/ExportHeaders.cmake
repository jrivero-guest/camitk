#!
#! @ingroup group_sdk_cmake
#!
#! export_headers is a macro that install header files at build and install time
#!
#! Duplicate headers installation:
#! - one is used at compiled time and puts everything in
#!   ${CMAKE_BINARY_DIR}/include/${CAMITK_SHORT_VERSION_STRING}/${GroupName}/${ComponentName}/${SubdirName}
#! - the other one is used at installation time and puts everything in
#!   ${CMAKE_INSTALL_PREFIX}/include/${CAMITK_SHORT_VERSION_STRING}/${GroupName}/${ComponentName}/${SubdirName}
#!
#! Usage:
#! \code
#! export_headers(HeaderFile1.h HeaderFile2.h ...
#!                COMPONENT ComponentName
#!                [GROUP GroupName]
#!                [SUBDIRECTORY SubdirName]
#! )
#! \endcode
#!
#! \param HeaderFileX.h (required)   A list of header files to install
#! \param COMPONENT                  name of the component to use. This is also the include subdirectory name
#!                                   used for copying the file
#! \param GROUP (optional)           the name of the group this install should be using group will be
#!                                   prepend to the component name.
#! \param SUBDIRECTORY (optional)    subdirectory to use in ${CAMITK_INCLUDE_DIR}/include/${ComponentName}
#!
#! Example invocation:
#!
#! \code
#!
#! #--------------
#! # installation
#! #--------------
#! export_headers(${MYPROJECT_HEADERS}
#!                COMPONENT ${MYPROJECT_NAME}
#! )
#!
#! \endcode
macro(export_headers)

  set(options "")
  set(oneValueArgs COMPONENT SUBDIRECTORY GROUP)
  set(multiValueArgs "")
  cmake_parse_arguments(EXPORT_HEADER "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  # special case for components: remove prefix for the destination directory
  string(REGEX REPLACE "^component-|^component_|^action-|^action_|^library_|^library-" "" EXPORT_HEADER_DESTINATION "${EXPORT_HEADER_COMPONENT}")

  # define where to install files
  set(EXPORT_HEADER_INCLUDE_DIR ${CMAKE_BINARY_DIR}/include/${CAMITK_SHORT_VERSION_STRING})
  set(EXPORT_HEADER_INSTALL_ROOT ${CMAKE_INSTALL_PREFIX})

  # Check group
  if(EXPORT_HEADER_GROUP)
    # check directory
    if (NOT EXISTS "${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_GROUP}")
      add_custom_command(TARGET ${EXPORT_HEADER_COMPONENT}
                        POST_BUILD
                        COMMAND ${CMAKE_COMMAND} -E make_directory ${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_GROUP}
                        COMMENT "Creating build-time group include dir ${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_GROUP}"
                        VERBATIM
      )
    endif()
    set(EXPORT_HEADER_DESTINATION ${EXPORT_HEADER_GROUP}/${EXPORT_HEADER_DESTINATION})
  endif()

  # check that the directory exists otherwise create it
  if (NOT EXISTS "${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_DESTINATION}")
    add_custom_command(TARGET ${EXPORT_HEADER_COMPONENT}
                       POST_BUILD
                       COMMAND ${CMAKE_COMMAND} -E make_directory ${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_DESTINATION}
                       COMMENT "Creating build-time include dir ${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_DESTINATION}"
                       VERBATIM
    )
  endif()

  # check the SUBDIRECTORY parameter
  if(EXPORT_HEADER_SUBDIRECTORY)
    set(EXPORT_HEADER_DESTINATION ${EXPORT_HEADER_DESTINATION}/${EXPORT_HEADER_SUBDIRECTORY})
    # create subdirectory if it does not exists
    if (NOT EXISTS "${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_DESTINATION}")
      add_custom_command(TARGET ${EXPORT_HEADER_COMPONENT}
                         POST_BUILD
                         COMMAND ${CMAKE_COMMAND} -E make_directory ${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_DESTINATION}
                         COMMENT "Creating build-time include subdir ${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_DESTINATION}"
                         VERBATIM
      )
    endif()
  endif()


  # at build time, copy the files to build directory include files when the target is built
  foreach(HEADER ${EXPORT_HEADER_UNPARSED_ARGUMENTS})
    # check if file name is relative or not
    set(FILE_TO_COPY ${HEADER})

    # copy after build, only if there was a change
    add_custom_command(TARGET ${EXPORT_HEADER_COMPONENT}
                       POST_BUILD
                       COMMAND ${CMAKE_COMMAND} -E copy_if_different ${FILE_TO_COPY} ${EXPORT_HEADER_INCLUDE_DIR}/${EXPORT_HEADER_DESTINATION}/
                       COMMENT "Installing build-time header ${HEADER}"
                       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                       VERBATIM
    )
  endforeach()

  # during installation, copy the files to install directory
  install(FILES ${EXPORT_HEADER_UNPARSED_ARGUMENTS}
          # DESTINATION ${CMAKE_INSTALL_PREFIX}/include/${CAMITK_SHORT_VERSION_STRING}/${EXPORT_HEADER_DESTINATION}/
          DESTINATION include/${CAMITK_SHORT_VERSION_STRING}/${EXPORT_HEADER_DESTINATION}/
          COMPONENT ${EXPORT_HEADER_COMPONENT}
  )

endmacro()
