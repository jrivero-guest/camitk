#!
#! @ingroup group_sdk_cmake
#!
#! macro get_git_info get the last value of commit count.
#! Set CAMITK_GIT_COMMIT_COUNT variable with the last number of commits
#!
#! Usage:
#! \code
#! get_git_info(PATH_TO_SOURCE_DIR)
#! \endcode
#!
#! \inparam PATH_TO_SOURCE_DIR (required) input source directory path
#!            
macro(get_git_info PATH_TO_SOURCE_DIR)
    unset(CAMITK_GIT_COMMIT_COUNT)
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-list HEAD --count
                    WORKING_DIRECTORY ${PATH_TO_SOURCE_DIR}
                    OUTPUT_VARIABLE CAMITK_GIT_COMMIT_COUNT
                    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    
    unset(CAMITK_GIT_BRANCH)
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
                    WORKING_DIRECTORY ${PATH_TO_SOURCE_DIR}
                    OUTPUT_VARIABLE CAMITK_GIT_BRANCH
                    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    
    unset(CAMITK_GIT_ABBREVIATED_HASH)
    execute_process(COMMAND ${GIT_EXECUTABLE} log -1 --format=%h
                    WORKING_DIRECTORY ${PATH_TO_SOURCE_DIR}
                    OUTPUT_VARIABLE CAMITK_GIT_ABBREVIATED_HASH
                    OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    unset(CAMITK_GIT_HASH)
    execute_process(COMMAND ${GIT_EXECUTABLE} log -1 --format=%H
                    WORKING_DIRECTORY ${PATH_TO_SOURCE_DIR}
                    OUTPUT_VARIABLE CAMITK_GIT_HASH
                    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    
    unset(CAMITK_GIT_COMMITER_DATE)
    execute_process(COMMAND ${GIT_EXECUTABLE} log -1 --format=%cd
                    WORKING_DIRECTORY ${PATH_TO_SOURCE_DIR}
                    OUTPUT_VARIABLE CAMITK_GIT_COMMITER_DATE
                    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    
    unset(CAMITK_ORIGIN_DEVELOP_GIT_HASH)
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse origin/develop
                    WORKING_DIRECTORY ${PATH_TO_SOURCE_DIR}
                    OUTPUT_VARIABLE CAMITK_ORIGIN_DEVELOP_GIT_HASH
                    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    
    string(SUBSTRING ${CAMITK_ORIGIN_DEVELOP_GIT_HASH} 0 8 CAMITK_ORIGIN_DEVELOP_GIT_ABBREVIATED_HASH)
    
    if(NOT CAMITK_GIT_COMMIT_COUNT) 
        message(WARNING "Unable to find the commit number using git : ${GIT_EXECUTABLE}")
        set(CAMITK_GIT_COMMIT_COUNT "unknown")
        set(CAMITK_GIT_BRANCH "unknown")
        set(CAMITK_GIT_HASH "unknown")
        set(CAMITK_GIT_ABBREVIATED_HASH "unknown")
        set(CAMITK_ORIGIN_DEVELOP_GIT_HASH "unknown")
        set(CAMITK_ORIGIN_DEVELOP_GIT_ABBREVIATED_HASH "unknown")
    endif()

endmacro()
