# Driver instructions for the build step of the continuous integration
#
# 1. Define parameters
# CAMITK_SITE="[Gitlab Runner] debian stable"                  # the name of the current machine 
# CAMITK_CI_MODE="Experimental"                                # ctest mode (Nightly, Continuous, Experimental...)
# CAMITK_CI_BRANCH="branch-name"                               # git branch name (check directly with git if not defined)
# CAMITK_CI_ID="Pipeline #$CI_PIPELINE_ID Job #$CI_BUILD_ID"   # unique id 
# CAMITK_SOURCE_DIR=~/Dev/CamiTK/src/camitk                    # path to CamiTK code source directory
# CAMITK_BUILD_DIR=~/Dev/CamiTK/build/camitk-exp               # path to the intended build directory
# CAMITK_BUILD_SETTINGS="GCC-64bits-Debug"                     # compiler-arch-buildtype string
#
# 2. run the command
# ctest -VV \
#       -DCTEST_SITE="$CAMITK_SITE" \
#       -DCI_MODE="$CAMITK_CI_MODE" \
#       -DCI_ID="$CAMITK_CI_ID" \
#       -DCI_BRANCH=$CAMITK_CI_BRANCH \
#       -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
#       -DCTEST_SOURCE_DIRECTORY="$CAMITK_SOURCE_DIR" \
#       -DCTEST_BINARY_DIRECTORY="$CAMITK_BUILD_DIR" \
#       -S $CAMITK_SOURCE_DIR/sdk/cmake/ctest/ci-build.cmake > build.log 2>&1
#
# It will submit a new report in the "configure" section of the dashboard 
# identified as $SITE and $COMPILER_CONFIG.
#
# What this script does ?
# 1. INFORMATION STEP
#       Configure SITE and BUILD information to be correctly display on the dashboard
#       Loads information from the CTestConfig.cmake file.
# 2. BUILD STEP 
#       build each CAMITK_TARGET of CamiTK (without testing)

set(CI_STAGE "Build")

# ------------------------ STEP 1: information step ------------------------
include("${CTEST_SOURCE_DIRECTORY}/sdk/cmake/ctest/ci-setup.cmake")

# ------------------------ STEP 2: build ------------------------
message(STATUS "Step 2. Build all CAMITK_TARGETs...")

# Get the CAMITK_TARGETs listing (automatically created at configuration step)
include("${CTEST_BINARY_DIRECTORY}/Subprojects.cmake")

# Update CDash configuration to the server
# The Project.xml file is automatically generated at configure time. 
# If a new extension is added to CamiTK, CDash will automatically be updated according to it.
# To do this, send this file to the server
ctest_submit(FILES "${CTEST_BINARY_DIRECTORY}/Project.xml") 

# Build each CAMITK_TARGETs
foreach(CAMITK_TARGET ${CAMITK_TARGETS})
        # tag sub project
        set_property(GLOBAL PROPERTY SubProject ${CAMITK_TARGET})
        set_property(GLOBAL PROPERTY Label ${CAMITK_TARGET})
        
        # build each sub project
        message(STATUS "- Building ${CAMITK_TARGET}...")
        set(CTEST_BUILD_TARGET ${CAMITK_TARGET})
        ctest_build()
        ctest_submit(PARTS Build)
endforeach()

message(STATUS "Build done")
