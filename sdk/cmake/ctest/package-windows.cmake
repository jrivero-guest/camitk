# Ctest script for packaging nightly CamiTK
# To configure a Nightly packaging, create a cron job using this script "ctest -S this-script.cmake -V"
#
# What this script does ?
# * INFORMATION STEP
#       Configure SITE and BUILD information to be correctly display on dashboard
#       Loads information from the CTestConfig.cmake file.
# * UPDATE STEP
#       use GIT to udpdate source code to the latest revision BEFORE Nightly start time, which is defined in the CTestConfig.cmake file.
# * CONFIGURE STEP
#       configure the whole CamiTK project and create a new build directory 
# * BUILD STEP 
#       build each subproject of CamiTK
# For each step a report is sent to the dashboard. This allows any developer to be informed (mailing list) of any problem, even BEFORE the script ends !

# Need to be defined, for the build to run.
if(NOT DEFINED CTEST_SOURCE_DIRECTORY)
    message(FATAL_ERROR "Please provide the build directory of the continuous test with the CTEST_SOURCE_DIRECTORY argument")
endif()
if(NOT DEFINED CTEST_BINARY_DIRECTORY)
    message(FATAL_ERROR "Please provide the build directory of the continuous test with the CTEST_BINARY_DIRECTORY argument")
endif()

# Script configuration, depending of the build, computer running the script
# Update to feat each computer which runs this script

# Get VM compilation information given by ctest call command
if(CAMITK_CONTINUOUS_INTEGRATION)
    string(REGEX REPLACE "^(.*)-.*-.*" "\\1" COMPILER "${CAMITK_CONTINUOUS_INTEGRATION}")
    string(REGEX REPLACE "^.*-(.*)-.*" "\\1" ARCH "${CAMITK_CONTINUOUS_INTEGRATION}")
    string(REGEX REPLACE "^.*-.*-(.*)" "\\1" BUILDTYPE "${CAMITK_CONTINUOUS_INTEGRATION}")
else()
    message(FATAL_ERROR "CAMITK_CONTINUOUS_INTEGRATION value must be given as option of the ctest command calling this script.")
endif()

# Compose with those variables the CTest required ones.
site_name(CTEST_SITE)
set( CTEST_BUILD_NAME ${CAMITK_CONTINUOUS_INTEGRATION})
if(UNIX)
    set( CTEST_CMAKE_GENERATOR  "Unix Makefiles" )
elseif(WIN32)
        if(COMPILER MATCHES "MinGW" OR "MINGW")
            set( CTEST_CMAKE_GENERATOR  "MinGW Makefiles" )
        elseif(COMPILER MATCHES "MSVC2008")
            set( CTEST_CMAKE_GENERATOR "Visual Studio 9 2008" )
        elseif(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "32bits")
            set( CTEST_CMAKE_GENERATOR  "Visual Studio 10" )
            set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/VCExpress.exe") # Do not use MSBuild.exe as it won't work
        elseif(COMPILER MATCHES "MSVC2010" AND ARCH MATCHES "64bits")
            set( CTEST_CMAKE_GENERATOR  "Visual Studio 10 Win64" )
            set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 10.0/Common7/IDE/VCExpress.exe") # Do not use MSBuild.exe as it won't work
        elseif(COMPILER MATCHES "MSVC2012" AND ARCH MATCHES "32bits")
            set( CTEST_CMAKE_GENERATOR  "Visual Studio 11" )
            set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 11.0/Common7/IDE/WDExpress.exe") # Do not use MSBuild.exe as it won't work
        elseif(COMPILER MATCHES "MSVC2012" AND ARCH MATCHES "64bits")
            set( CTEST_CMAKE_GENERATOR  "Visual Studio 11 Win64" )
            set( CMAKE_MAKE_PROGRAM "C:/Program Files (x86)/Microsoft Visual Studio 11.0/Common7/IDE/WDExpress.exe") # Do not use MSBuild.exe as it won't work
        elseif(COMPILER MATCHES "MSVC2013" AND ARCH MATCHES "32bits")
            set( CTEST_CMAKE_GENERATOR  "Visual Studio 12" )
        elseif(COMPILER MATCHES "MSVC2013" AND ARCH MATCHES "64bits")
            set( CTEST_CMAKE_GENERATOR  "Visual Studio 12 Win64" )
        elseif(COMPILER MATCHES "MSVC2015" AND ARCH MATCHES "64bits")
            set( CTEST_CMAKE_GENERATOR  "Visual Studio 14 2015 Win64" )
        else()
            message(FATAL_ERROR "CTEST COMPILER ERROR : No proper compiler found, please check ctest command syntax.")
        endif()
endif()

if(BUILDTYPE)
    set( CTEST_BUILD_CONFIGURATION ${BUILDTYPE})
else()
    message(FATAL_ERROR "NO BUILD TYPE : Please provide a build type: Debug or Release")
endif()

# Update source code and send reports
set( CTEST_UPDATE_COMMAND "git")
find_program(CTEST_GIT_COMMAND git)
set(CTEST_UPDATE_COMMAND ${CTEST_GIT_COMMAND})

# CMake configuration (put here all the configure flags)
set( CTEST_CONFIGURE_COMMAND "${CMAKE_COMMAND} -G \"${CTEST_CMAKE_GENERATOR}\"")
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCMAKE_BUILD_TYPE:STRING=${CTEST_BUILD_CONFIGURATION}")
# Packaging installation location 
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCAMITK_INSTALL_ROOT:PATH=.")
# CPack packaging target
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCPACK_BINARY_NSIS:BOOL=TRUE")
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCPACK_BINARY_ZIP:BOOL=FALSE")
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCPACK_SOURCE_ZIP:BOOL=FALSE")
# CEP preferences
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCEP_IMAGING:BOOL=TRUE")
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCEP_MODELING:BOOL=TRUE")
#Not needed in package: set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCEP_TUTORIALS:BOOL=TRUE")
# Temporary, explicitly ask for GDCM dependency to be set
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCAMITK_DICOM_INCOMPLETE_SUPPORT=FALSE")
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCOMPONENT_DICOMDIRECTORY=TRUE")
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} -DCOMPONENT_DICOMIMAGE=TRUE")
# Source files
set( CTEST_CONFIGURE_COMMAND "${CTEST_CONFIGURE_COMMAND} ${CTEST_SOURCE_DIRECTORY}")

# to get CDash server configuration :
include("${CTEST_SOURCE_DIRECTORY}/CTestConfig.cmake")

# The type of build that this script will make 
# For this script we use Nightly build has it is designed to be run once a day
ctest_start(Nightly)

# Update source code
ctest_update(SOURCE ${CTEST_SOURCE_DIRECTORY} RETURN_VALUE count)
if(count GREATER 0)
    # We only show update when there is change (avoid the CDash u=1 bug with no errors report)
    ctest_submit(PARTS Update Notes)
endif()

# Configure whole project
ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
ctest_configure()
ctest_submit(PARTS Configure)

# to get subprojects listing (automatically created at confituration step)
include("${CTEST_BINARY_DIRECTORY}/Subprojects.cmake")

# Update CDash configuration to the server
# Project.xml file is automatically generated. If someone added an extension to CamiTK, CDash will automatically be updated according to it.
# To do this, send this file to the server
ctest_submit(FILES "${CTEST_BINARY_DIRECTORY}/Project.xml") 

# Build each subprojects
foreach(subproject ${CAMITK_TARGETS})
        # tag sub project
        set_property(GLOBAL PROPERTY SubProject ${subproject})
        set_property(GLOBAL PROPERTY Label ${subproject})

        # build each sub project
        set(CTEST_BUILD_TARGET ${subproject})
        ctest_build()
        ctest_submit(PARTS Build)

        # TODO : mais plus tard ...
        # runs only tests that have a LABELS property matching “${subproject}” (si c'est pas la classe)
        # ctest_test(BUILD “${CTEST_BINARY_DIRECTORY}” INCLUDE_LABEL “${subproject}”)
        # ctest_submit(PARTS Test)
endforeach() 

# Packaging
# tag the corresponding subproject to match corresponding CDash's one.
set_property(GLOBAL PROPERTY SubProject package)
set_property(GLOBAL PROPERTY Label package)
# Package CamiTK and send the report.
set(CTEST_BUILD_TARGET camitk_package)
ctest_build()
ctest_submit(PARTS Build)

