# Macro to write to file and to stdout
macro(ci_start_log)
    set(options "")
    set(oneValueArgs FILENAME)
    set(multiValueArgs "")
    cmake_parse_arguments(CI_START_LOG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if(NOT CI_START_LOG_FILENAME)
        message(FATAL_ERROR "ci_start_log macro requires FILENAME")
    else()
        set(CAMITK_CI_LOG_FILENAME ${CI_START_LOG_FILENAME})
    endif()
    
    file(WRITE ${CAMITK_CI_LOG_FILENAME} "Continuous integration started\n")
endmacro()

macro(ci_log)
    set(options CI_ERROR)
    set(oneValueArgs "")
    set(multiValueArgs "")
    cmake_parse_arguments(CI_LOG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
    
    file(APPEND ${CAMITK_CI_LOG_FILENAME} "${CI_LOG_UNPARSED_ARGUMENTS}\n")

    if (CI_LOG_CI_ERROR)
        file(APPEND ${CAMITK_CI_LOG_FILENAME} "Fatal error\n")
    endif()
    message(STATUS ${CI_LOG_UNPARSED_ARGUMENTS})
endmacro()
