# Driver instructions for the configure step of the continuous integration
#
# 1. Define parameters
# CAMITK_SITE="[Gitlab Runner] debian stable"                  # the name of the current machine 
# CAMITK_CI_MODE="Experimental"                                # ctest mode (Nightly, Continuous, Experimental...)
# CAMITK_CI_BRANCH="branch-name"                               # git branch name (check directly with git if not defined)
# CAMITK_CI_ID="Pipeline #$CI_PIPELINE_ID Job #$CI_BUILD_ID"   # unique id 
# CAMITK_SOURCE_DIR=~/Dev/CamiTK/src/camitk                    # path to CamiTK code source directory
# CAMITK_BUILD_DIR=~/Dev/CamiTK/build/camitk-exp               # path to the intended build directory
# CAMITK_BUILD_SETTINGS="GCC-64bits-Debug"                     # compiler-arch-buildtype string
#
# 2. run the command
# ctest -VV \
#       -DCTEST_SITE="$CAMITK_SITE" \
#       -DCI_MODE="$CAMITK_CI_MODE" \
#       -DCI_ID="$CAMITK_CI_ID" \
#       -DCI_BRANCH=$CAMITK_CI_BRANCH \
#       -DCI_BUILD_SETTINGS="$COMPILER_CONFIG" \
#       -DCTEST_SOURCE_DIRECTORY="$CAMITK_SOURCE_DIR" \
#       -DCTEST_BINARY_DIRECTORY="$CAMITK_BUILD_DIR" \
#       -S $CAMITK_SOURCE_DIR/sdk/cmake/ctest/ci-configure.cmake > configure.log 2>&1
#
# It will submit a new report in the "configure" section of the dashboard 
# identified as $SITE and $COMPILER_CONFIG.
#
# What this script does ?
# 1. INFORMATION STEP
#       Configure SITE and BUILD information to be correctly display on the dashboard
#       Loads information from the CTestConfig.cmake file.
# 2. CONFIGURE STEP
#       configure the whole CamiTK project and create a new build directory 

set(CI_STAGE "Configure")

# ------------------------ STEP 1: information step ------------------------
include("${CTEST_SOURCE_DIRECTORY}/sdk/cmake/ctest/ci-setup.cmake")

# ------------------------ STEP 2: configure step ------------------------
message(STATUS "Step 2. Configure the CamiTK project...")

# Empty local installation directory
if(WIN32)
    # %APPDATA%\MySoft\Star Runner.ini
    set(CAMITK_USER_BASE_DIR_WINDOWS $ENV{APPDATA})
    file(TO_CMAKE_PATH "${CAMITK_USER_BASE_DIR_WINDOWS}" CAMITK_USER_BASE_DIR)
else()
    # (UNIX OR APPLE)
    # $HOME/.config/MySoft/Star Runner.ini 
    set(CAMITK_USER_BASE_DIR "$ENV{HOME}/.config")
endif()
set(CAMITK_USER_DIR "${CAMITK_USER_BASE_DIR}/CamiTK")
file(REMOVE_RECURSE ${CAMITK_USER_DIR})

# Configure whole project
# Note: no need to ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY}) in gitlab-ci as the environment is always clean
ctest_configure()
ctest_submit(PARTS Configure)

message(STATUS "Configuration done")
