set( qpe_DIR ../../src )

include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
  ${QT_INCLUDES}
  ${qpe_DIR}
)

include( ${QT_USE_FILE} )
add_definitions( ${QT_DEFINITIONS} )

set(
  LIBS
  ${LIBS} 
  ${QT_LIBRARIES}
)

set(
  qtpropertyeditor_SRCS
  ${qpe_DIR}/qtpropertybrowser.cpp
  ${qpe_DIR}/qtpropertymanager.cpp
  ${qpe_DIR}/qteditorfactory.cpp
  ${qpe_DIR}/qtvariantproperty.cpp
  ${qpe_DIR}/qttreepropertybrowser.cpp
  ${qpe_DIR}/qtbuttonpropertybrowser.cpp
  ${qpe_DIR}/qtgroupboxpropertybrowser.cpp
  ${qpe_DIR}/qtpropertybrowserutils.cpp
)

qt4_generate_moc(
  ${qpe_DIR}/qtpropertybrowser.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtpropertybrowser.cpp
)
qt4_generate_moc(
  ${qpe_DIR}/qtpropertymanager.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtpropertymanager.cpp
)
qt4_generate_moc(
  ${qpe_DIR}/qteditorfactory.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qteditorfactory.cpp
)
qt4_generate_moc(
  ${qpe_DIR}/qtvariantproperty.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtvariantproperty.cpp
)
qt4_generate_moc(
  ${qpe_DIR}/qttreepropertybrowser.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qttreepropertybrowser.cpp
)
qt4_generate_moc(
  ${qpe_DIR}/qtbuttonpropertybrowser.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtbuttonpropertybrowser.cpp
)
qt4_generate_moc(
  ${qpe_DIR}/qtgroupboxpropertybrowser.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtgroupboxpropertybrowser.cpp
)
qt4_generate_moc(
  ${qpe_DIR}/qtpropertymanager.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/qtpropertymanager.moc
)
qt4_generate_moc(
  ${qpe_DIR}/qteditorfactory.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/qteditorfactory.moc
)
qt4_generate_moc(
  ${qpe_DIR}/qttreepropertybrowser.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/qttreepropertybrowser.moc
)

set(
  qtpropertyeditor_HEADERS_ONLY_MOC
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtpropertybrowser.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtpropertymanager.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qteditorfactory.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtvariantproperty.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qttreepropertybrowser.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtbuttonpropertybrowser.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtgroupboxpropertybrowser.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/qtpropertymanager.moc
  ${CMAKE_CURRENT_SOURCE_DIR}/qteditorfactory.moc
  ${CMAKE_CURRENT_SOURCE_DIR}/qttreepropertybrowser.moc
)

set_source_files_properties(
  ${qtpropertyeditor_HEADERS_ONLY_MOC}
  PROPERTIES
    HEADER_FILE_ONLY true
)

qt4_generate_moc(
  ${qpe_DIR}/qtpropertybrowserutils_p.h
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtpropertybrowserutils_p.cpp
)

set(
  qtpropertyeditor_HEADERS_MOC
  ${CMAKE_CURRENT_SOURCE_DIR}/moc_qtpropertybrowserutils_p.cpp
)

set(
  qtpropertyeditor_MOC
  ${qtpropertyeditor_HEADERS_MOC}
  ${qtpropertyeditor_HEADERS_ONLY_MOC}
)

qt4_add_resources(
  qtpropertyeditor_RESOURCES
  ${qpe_DIR}/qtpropertybrowser.qrc
)

set(
  decoration_SRCS
  main.cpp
)

qt4_generate_moc(
  main.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/main.moc
)

set(
  decoration_MOC
  ${CMAKE_CURRENT_SOURCE_DIR}/main.moc
)

set_source_files_properties(
  ${decoration_MOC}
  PROPERTIES
    HEADER_FILE_ONLY true
)

add_executable(
  decoration
  ${decoration_MOC}
  ${decoration_SRCS}
  ${qtpropertyeditor_MOC}
  ${qtpropertyeditor_SRCS}
  ${qtpropertyeditor_RESOURCES}
)

target_link_libraries( decoration ${LIBS} )
