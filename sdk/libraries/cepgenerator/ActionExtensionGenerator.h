/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONEXTENSIONGENERATOR_H
#define ACTIONEXTENSIONGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only


// local includes
#include "ExtensionGenerator.h"

namespace cepcoreschema {
class ActionExtension;
}

class ActionGenerator;

/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Generate an action extension.
 *
 **/
class ActionExtensionGenerator : public ExtensionGenerator {

public:
    ActionExtensionGenerator(QString xmlFileName, QString actionExtensionsDirectory, QString licence = "");
    ActionExtensionGenerator(cepcoreschema::ActionExtension& domActionExtension, QString actionExtensionsDirectory, QString licence = "");

    ~ActionExtensionGenerator();


protected:
    /// @{ Helpers methods
    void generateActionOrComponent(QString directory) override;
    void writeCFile(QString directory) override;
    void writeHFile(QString directory) override;

    void generateTestDataFiles(QString directory, QString testDataDirName) override;

    ///@}

private:
    /// Helper method for constructors
    void createFromDom(cepcoreschema::ActionExtension& dom);

    QVector<ActionGenerator*> actions;
};

#endif
