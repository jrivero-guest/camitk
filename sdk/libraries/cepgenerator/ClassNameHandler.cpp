/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ClassNameHandler.h"

#include <QStringList>

#include <iostream>

QString ClassNameHandler::getClassName(QString genericName) {
    std::cout << "inside getClassName" << std::endl;
    QString className = genericName;
    // Remove non alphanumeric characters
    std::cout << "Before rege Exp" << className.toStdString() << std::endl;
    // Use regular expression [^a-zA-Z0-9\s]
    className = className.remove(QRegExp("[^a-zA-Z\\d\\s]"));


    std::cout << "After rege Exp" << className.toStdString() << std::endl;
    // transform all to whitespace
    className = className.simplified();
    QStringList cppNameList = className.split(" "); // split words
    // lower case for the first letter of the first word
    QString firstWord = cppNameList.takeFirst();
    // only if there is more than one word... (otherwise it means the user has just enter a longNameWithPossiblySomeUpperCaseLetters)
    if (cppNameList.size() > 0) {
        className = firstWord.toLower();
    }

    // uppercase the first letter of each word and concatenate
    foreach (QString s, cppNameList) {
        className += s.left(1).toUpper() + s.mid(1).toLower();
    }
    className = className.left(1).toUpper() + className.mid(1);

    return className;

}

QString ClassNameHandler::getDirectoryName(QString genericName) {
    return getClassName(genericName).toLower();
}
