/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef COMPONENTEXTENSIONGENERATOR_H
#define COMPONENTEXTENSIONGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only


// includes from Qt
#include <QString>
#include <QVector>

// local includes
#include "ExtensionGenerator.h"

namespace cepcoreschema {
class ComponentExtension;
}

class ComponentGenerator;

/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Generate a component extension.
 *
 **/
class ComponentExtensionGenerator : public ExtensionGenerator {

public:
    ComponentExtensionGenerator(QString xmlFileName, QString componentExtensionsDirectory, QString licence = "");
    ComponentExtensionGenerator(cepcoreschema::ComponentExtension& domComponentExtension, QString componentExtensionsDirectory, QString licence = "");

    ~ComponentExtensionGenerator();


protected:
    /// @{ Helpers methods
    void generateActionOrComponent(QString directory) override;
    void writeCFile(QString directory) override;
    void writeHFile(QString directory) override;

    void generateTestDataFiles(QString directory, QString testDataDirName) override;

    ///@}

private:
    /// Helper method for constructors
    void createFromDom(cepcoreschema::ComponentExtension& dom);

    QString findComponentClass(QString suffix);

    QVector<ComponentGenerator*> components;

};

#endif
