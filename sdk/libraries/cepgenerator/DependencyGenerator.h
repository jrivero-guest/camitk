/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef DEPENDENCYGENERATOR_H
#define DEPENDENCYGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only


// includes from Qt
#include <QString>


namespace cepcoreschema {
class Dependency;
}

/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Create the dependencies, generate the "NEEDs_LIBRARY" in the cmake file.
 *
 **/
class DependencyGenerator {

public:
    enum DependencyTypeGenerator {
        EXTERNAL_LIB,
        CEP_LIB,
        NEEDED_COMPONENT,
        NEEDED_ACTION
    };

    static QString getExternalLibsString(QVector<DependencyGenerator* > dependencies);
    static QString getCepLibsString(QVector<DependencyGenerator* > dependencies);
    static QString getNeededComponentsString(QVector<DependencyGenerator* > dependencies);
    static QString getNeededActionsString(QVector<DependencyGenerator* > dependencies);


    DependencyGenerator(cepcoreschema::Dependency domDependency);

    DependencyTypeGenerator getType();
    QString getName();

private:
    QString name;
    DependencyTypeGenerator type;

};


#endif
