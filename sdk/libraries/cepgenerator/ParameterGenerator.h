/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef PARAMETERGENERATOR_H
#define PARAMETERGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only


// includes from Qt
#include <QString>
#include <QMap>

namespace cepcoreschema {
class Parameter;
}

/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Generate parameters
 *
 **/
class ParameterGenerator {
public:
    typedef enum  {
        INT,
        DOUBLE,
        BOOLEAN,
        QSTRING,
        QDATE,
        QTIME,
        QCOLOR,
        QPOINT,
        QPOINTF,
        QVECTOR3D,
        QVECTOR4D,
        UNKNOWN
    } ParameterTypeGenerator;

    ParameterGenerator(cepcoreschema::Parameter& domParameter);

    QString getDefaultValue() const;
    QString getName() const;
    /// create a name that can be used as C++ variable name (remove all white space)
    QString getCppName() const;
    QString getType() const;
    bool isEditable() const;
    bool needsAdditionalInclude() const;
    QString getAdditionalInclude() const;
    QString getQVariantConversion() const;
    QString getPropertyQVariant() const;
    QString getToString() const;
    QString getDescription() const;
    QString getUnit() const;

    static QString getTypeDefaultValue(QString typeName);

private:
    bool editable;
    QString name;
    QString defaultValue;
    ParameterTypeGenerator type;
    QString description;
    QString unit;

    static QMap<ParameterGenerator::ParameterTypeGenerator, QString>& getTypeNames();
    static QMap<ParameterGenerator::ParameterTypeGenerator, QString>& getDefaultValues();

    static void initNamesAndValues();
};

#endif
