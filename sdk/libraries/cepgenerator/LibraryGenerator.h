/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef LIBRARYGENERATOR_H
#define LIBRARYGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only


// local includes
#include "ExtensionGenerator.h"

// Qt includes
#include <QStringList>

namespace cepcoreschema {
class Library;
}

/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Generate the library
 *
 **/
class LibraryGenerator : public ExtensionGenerator {

public:
    LibraryGenerator(QString xmlFileName, QString actionExtensionsDirectory, QString licence = "");
    LibraryGenerator(cepcoreschema::Library& domLibrary, QString librariesDirectory, QString licence = "");

    ~LibraryGenerator();


protected:
    /// @{ Helpers methods
    void generateExtensionCMakeLists(QString directory) override;

    void generateActionOrComponent(QString directory) {};
    void writeCFile(QString directory) {};
    void writeHFile(QString directory) {};

    void generateTestDataFiles(QString directory, QString testDataDirName) {};

    ///@}

private:
    /// Helper method for constructors
    void createFromDom(cepcoreschema::Library& dom);

    bool isStatic;

};

#endif
