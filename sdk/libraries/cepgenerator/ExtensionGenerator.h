/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef EXTENSIONGENERATOR_H
#define EXTENSIONGENERATOR_H

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only


// includes from Qt
#include <QString>
#include <QVector>


class DependencyGenerator;
/**
 * @ingroup group_sdk_libraries_cepgenerator
 *
 * @brief
 * Common abstract class to generate extensions.
 *
 * @note
 * This class is abstract and thus cannot be instantiated.
 *
 **/
class ExtensionGenerator {

public:
    ExtensionGenerator(QString actionExtensionsDirectory, QString licence = "", QString extensionType = "NONE");

    ~ExtensionGenerator();

    virtual void generateExtension();

protected:
    virtual void setExtensionsDirectory(QString extensionsDirectory) ;

    /// @{ Helpers methods
    virtual void generateExtensionCMakeLists(QString directory);

    virtual void generateActionOrComponent(QString directory) = 0;

    virtual void writeCFile(QString directory) = 0;
    virtual void writeHFile(QString directory) = 0;

    virtual void generateTestDataFiles(QString directory, QString testDataDirName) = 0;

    ///@}


    /// Absolute path to the extensions directory
    QString extensionsDirectory;

    /// Extension type: Action or Component extension
    QString extensionType;

    /// Name of the extension
    QString name;

    QString description;

    QString licence;

    QVector<DependencyGenerator* > dependencyGenerators;
};

#endif
