@LICENCE@
// CamiTK includes
#include "@COMPONENTCLASSNAME@.h"
#include <Property.h>

@IF_NONE@
#include <QFileInfo>
@ENDIF_NONE@

@IF_MESH@
// Qt includes
#include <QColor>
#include <QPointF>

// Vtk includes
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkSmartPointer.h>
@ENDIF_MESH@
@ADDITIONAL_INCLUDES@

using namespace camitk;

// --------------- Constructor -------------------
@COMPONENTCLASSNAME@::@COMPONENTCLASSNAME@(const QString & file)
@IF_NONE@
:
@PARENTCLASSNAME@(file, QFileInfo(file).baseName())
@ELSEIF_NONE@
:
@PARENTCLASSNAME@(file)
@ENDIF_NONE@
{
    @IF_NONE@
    // Read the input file...

    @ENDIF_NONE@
    @IF_ADDPROPERTIES@
    // Set the properties with their default values
    @ENDIF_ADDPROPERTIES@
    @BEGIN_ADDPROPERTIES@
    addProperty(new Property(tr("@PROPERTY_NAME@"), @PROPERTY_VALUE@, tr("@PROPERTY_DESCRIPTION@"), "@PROPERTY_UNIT@"));
    @ELSE_ADDPROPERTIES@
    // If you want to add properties to your component, you can add them
    // using properties
    // addProperty(new Property(tr("Property name"), "Property value", tr("Property description"), "Property unit"));
    @END_ADDPROPERTIES@

    @IF_NONE@
    // Add sub-components using the following code lines
    // Create a component (can be a component of your Component extension:
    // Component * subComp = new .....
    // add it as sub-component:
    // addChild(subComp);
    @ENDIF_NONE@
    @IF_IMAGE@

    // Read the image from the given input file
    if (!file.isNull()) {
        // Here is an example of image creation without reading a file
        //  to show how to use vtkImageData and ImageComponents.
        // Replace this code with real image creation using the input file.
        // Do not hesitate to read vtk documentation or tutorials to see how vtkImageData works

        // Create a vtkImageData
        vtkSmartPointer<vtkImageData> image = NULL;
        image = vtkSmartPointer<vtkImageData>::New();
        image->SetDimensions(3,3,3);
        image->AllocateScalars(VTK_UNSIGNED_CHAR, 1);
        // Here, we arbitrarily fill the image data
        int* dims = image->GetDimensions();

        for (int z = 0; z < dims[2]; z++) {
            for (int y=0; y<dims[1]; y++) {
                for (int x=0; x<dims[0]; x++) {
                    double val = ((x==1) && (y==1) && (z==1))? 1.0: 0.0;
                    image->SetScalarComponentFromDouble(x,y,z,0,val);
                }
            }
        }

        // Make this image the image of your component
        // (this method is already implemented in ImageComponent)
        setImageData(image, false);
    }
    else
    { // there is no file name, so we cannot open an image...
        throw (AbortException("No file found"));
    }
    @ENDIF_IMAGE@
    @IF_MESH@

    // Read the mesh from the given input file
    if (!file.isNull()) {
        // Here is an example of mesh creation without reading a file
        //  to show how to use vtkPolyData and MeshComponents.
        // Replace this code with real mesh creation using the input file.
        // Do not hesitate to read vtk documentation or tutorials to see how vtkPolyData works

        // Create a vtkPointSet
        // In this example, we create a vtkPolyData, but any kind of vtkPointSet holds
        vtkSmartPointer<vtkPolyData> data = NULL;

        // Change the following lines to fill the point set with what you have read
        // Here, we arbitrarilly create a cube (exemple from vtk).
        int i;
        static float x[8][3]= {{0,0,0}, {1,0,0}, {1,1,0}, {0,1,0}, {0,0,1}, {1,0,1}, {1,1,1}, {0,1,1}};
        static vtkIdType pts[6][4]= {{0,1,2,3}, {4,5,6,7}, {0,1,5,4}, {1,2,6,5}, {2,3,7,6}, {3,0,4,7}};
        // We create the building blocks of polydata including data attributes.
        data = vtkSmartPointer<vtkPolyData>::New();
        vtkSmartPointer<vtkPoints>     points  = vtkSmartPointer<vtkPoints>::New();
        vtkSmartPointer<vtkCellArray>  polys   = vtkSmartPointer<vtkCellArray>::New();
        vtkSmartPointer<vtkFloatArray> scalars = vtkSmartPointer<vtkFloatArray>::New();

        // Load the point, cell, and data attributes.
        for (i=0; i<8; i++)
            points->InsertPoint(i,x[i]);

        for (i=0; i<6; i++)
            polys->InsertNextCell(4,pts[i]);

        for (i=0; i<8; i++)
            scalars->InsertTuple1(i,i);

        // We now assign the pieces to the vtkPolyData.
        data->SetPoints(points);
        data->SetPolys(polys);
        data->GetPointData()->SetScalars(scalars);

        // Make this point set the representation of your component
        // (this method is already implemented in MeshComponent)
        initRepresentation(data);

    }
    else
    { // there is no file name, so we cannot open an image...
        throw (AbortException("No file found"));
    }
    @ENDIF_MESH@

}

// --------------- destructor -------------------
@COMPONENTCLASSNAME@::~@COMPONENTCLASSNAME@() {

}
