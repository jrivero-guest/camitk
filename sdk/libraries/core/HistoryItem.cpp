/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "HistoryItem.h"

namespace camitk {

// --------------- Constructor -------------------
HistoryItem::HistoryItem(QString name) {
    this->name = name;
}

// --------------- Accessor -------------------
QString HistoryItem::getName() {
    return this->name;
}


// --------------- addProperty -------------------
void HistoryItem::addProperty(QByteArray name, QVariant value) {
    this->properties.insert(name, value);
}

// --------------- getInputComponents -------------------
QList<HistoryComponent> HistoryItem::getInputHistoryComponents() {
    return this->inputHistoryComponents;
}

// --------------- setInputComponents -------------------
void HistoryItem::setInputHistoryComponents(QList<HistoryComponent> inputHistoryComponents) {
    this->inputHistoryComponents = inputHistoryComponents;
}

// --------------- getOutputComponents -------------------
QList<HistoryComponent> HistoryItem::getOutputHistoryComponents() {
    return this->outputHistoryComponents;
}

// --------------- setOutputComponents -------------------
void HistoryItem::setOutputHistoryComponents(QList<HistoryComponent> outputHistoryComponents) {
    this->outputHistoryComponents = outputHistoryComponents;
}



}
