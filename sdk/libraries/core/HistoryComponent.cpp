/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK stuff
#include "HistoryComponent.h"
#include <ImageComponent.h>
#include <MeshComponent.h>

using namespace camitk;

// --------------- Constructor -------------------
HistoryComponent::HistoryComponent(Component* component) {
    this->name = component->getName();
    // Determine the type according to the component's representation
    // By default its type is OTHER
    this->type = HistoryComponent::OTHER;
    if (dynamic_cast<MeshComponent*>(component) != nullptr) {
        this->type = HistoryComponent::MESH_COMPONENT;
    }
    else if (dynamic_cast<ImageComponent*>(component) != nullptr) {
        this->type = HistoryComponent::IMAGE_COMPONENT;
    }
}

// --------------- getName -------------------
QString HistoryComponent::getName() const {
    return this->name;
}

// --------------- getType -------------------
HistoryComponent::Type HistoryComponent::getType() const {
    return this->type;
}


