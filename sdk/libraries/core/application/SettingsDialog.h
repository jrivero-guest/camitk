/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SETTINGS_DIALOG_H
#define SETTINGS_DIALOG_H

// -- Core stuff
#include "CamiTKAPI.h"
namespace Ui {
class ui_Settings;
}

// -- QT stuff
#include <QDialog>
#include <QMap>

namespace camitk {

// -- Core stuff classes
class ObjectController;

/**
  * @ingroup group_sdk_libraries_core_application
  *
  * @brief
  * This class controls the settings dialog user interface.
  *
  * to add a settings for a given class, either add a tab widgets (e.g. for plugin tabs)
  * or add a new ObjectController tab using editSettings(QObject*).
  *
  */
class CAMITK_API SettingsDialog : public QDialog {
    Q_OBJECT

public:

    /** default constructor
      */
    SettingsDialog();

    /// destructor
    ~SettingsDialog() override;

    /// add a new property editor in a tab for the new object (it automatically connect the object "saveSettings" slot if exists to the dialog apply button)
    void editSettings(QObject*);

private slots:
    /** naming convention is QMetaObject's auto-connection.
      * name of slots allows for automatic connection (and say good bye to all your connect(...)! )
      *
      * e.g. : on_colorButton_released() is a slot that will automatically be connected.
      * just using this naming convention means that the following line will automatically
      * be called:
      *
      * \code
      * connect(colorButton, SIGNAL(released()), this, SLOT(on_colorButton_released()));
      * \endcode
      *
      * Isn't it great?!
      *
      * \note it should be the only places in this application were the naming convention is "deviant"
      */
    virtual void on_objectList_itemSelectionChanged();

    virtual void on_resetConfigurationButton_released();

    virtual void on_addComponentExtensionButton_released();

    virtual void on_removeComponentExtensionButton_released();

    virtual void on_componentExtensionList_cellClicked(int, int);

    virtual void on_addActionExtensionButton_released();

    virtual void on_removeActionExtensionButton_released();

    virtual void on_actionExtensionList_cellClicked(int, int);

private:
    /// if the user says it is ok and validate the settings dialog, this update the settings
    void accept() override;

    /// update the component extension list
    void updateComponentExtensionList();

    /// update the action plugin list
    void updateActionExtensionList();

    /// the property editor
    ObjectController* objectController;

    /// the map to get an edited object from its name
    QMap<QString, QObject*> editedObjectMap;

    /// Qt ui designed in Qt Designer
    Ui::ui_Settings* myUI;

    /// the list of user component extension locations
    QStringList userComponents;

    /// the list of user action extension locations
    QStringList userActions;

    /// get the current value of the settings
    void readUserExtensions();

    /// write the current value of the settings
    void writeUserExtensions();
};
}

#endif
