/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CAMITKCORE_H
#define CAMITKCORE_H



// -- Core stuff
#include "CamiTKAPI.h"
#include "HistoryItem.h"

// -- Qt stuff
#include <QStack>



namespace camitk {
/**
 * @ingroup group_sdk_libraries_core
 *
 * @brief
 * Core class specifies the basic static information for the CamiTK API.
 *
 * Extensions can only be in three different places:
 * - CAMITK_DIR (where the libcamitkcore is)
 * - user config directory
 * - current wording directory
 *
 */
class CAMITK_API Core {
public:

    /// @name runtime directory information
    ///@{
    /** get all the component directories. It gets all the valid directories
     *  where components are installed, and insert them in this particular order:
     *  1. current working directory (build install tree)
     *  2. user config directory (user install tree)
     *  3. CamiTK SDK installation directory (global install tree)
     */
    static const QStringList getComponentDirectories();

    /** get all the action directories. It gets all the valid directories
     *  where components are installed, and insert them in this particular order:
     *  1. current working directory (build install tree)
     *  2. user config directory (user install tree)
     *  3. CamiTK SDK installation directory (global install tree)
     */
    static const QStringList getActionDirectories();

    /** Get a valid camitk test data directory name. It returns the first valid
     *  test data directory that is found. Checking is done in this particular order:
     *  1. current working directory (build install tree)
     *  2. user config directory (user install tree)
     *  3. CamiTK SDK installation directory (global install tree)
     *
     *  If none of this three directories is valid, return user's home directory.
     *
     *  A test data directory is valid if it exists and contains a least one file.
     *
     *  @return a null QString, checkable with isNull(), if no test data directory found
     */
    static const QString getTestDataDir();

    /** get all installation directories, suffixed by the given word.
     *  All the returned Strings are unique valid directories, sorted in this particular order:
     *  1. current working directory (build install tree)
     *  2. user config directory (user install tree)
     *  3. CamiTK SDK installation directory (global install tree)
     */
    static const QStringList getInstallDirectories(QString suffix);

    /// get more information about installation, etc...
    static const QString getConfig();

    /// get all important paths
    static const QString getPaths();

    /// get the CAMITK_DIR (where camitk-config is installed)
    static const QString getGlobalInstallDir();

    /// get the user config directory (this is the local installation directory)
    static const QString getUserInstallDir();

    /// get the current working directory (during dev, this should be the build directory, which is an installation directory)
    static const QString getCurrentWorkingDir();

    /// get a well formed bug report with all necessary information
    static const QString getBugReport();

    ///@}

    /// @name runtime build information
    ///@{
    /// get the type of build we are running, Debug or Release
    /// @return true if running in Debug, elsewhere false for Release build.
    static const bool isDebugBuild();
    ///@}


    /// @name miscallaneous utility method or variables
    ///@{
    /// complete version string
    static const char* version;
    /// short version string (all in lower case, without the patch number)
    static const char* shortVersion;
    /// version used for so name
    static const char* soVersion;
    /// debug postfix used on MSVC to distinguished between release and debug version
    static const char* debugPostfix;
    ///@}

};

}

#endif // CAMITKCORE_H

