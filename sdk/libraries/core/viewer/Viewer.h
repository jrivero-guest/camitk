/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VIEWER_H
#define VIEWER_H

// -- Core stuff
#include "CamiTKAPI.h"

// -- QT stuff
#include <QObject>

// -- QT stuff classes
class QToolBar;
class QMenu;

namespace camitk {
class Component;

/**
  * @ingroup group_sdk_libraries_core_viewer
  *
  * @brief
  * Viewer is an abstract viewer.
  *
  * A viewer can contains other viewers, example of viewer are: Explorer, PropertyExplorer, MedicalImageViewer.
  * A viewer can be added to MainWindow, MainWindow will include its menu in the "View" menu and its toolbar in
  * the application toolbar.
  *
  * Use QObject method getObjectName to get the viewer's name.
  */

class CAMITK_API Viewer : public QObject {
    Q_OBJECT

public:
    /// default constructor
    Viewer(QString name);

    /// default destructor
    ~Viewer() override = default;

    /// returns the number of Component that are displayed by this viewer
    virtual unsigned int numberOfViewedComponent() = 0;

    /// refresh the view (can be interesting to know which other viewer is calling this)
    virtual void refresh(Viewer* whoIsAsking = nullptr) = 0;

    /// get the viewer widget. @param parent the parent widget for the viewer widget
    virtual QWidget* getWidget(QWidget* parent = nullptr) = 0;

    /// get the viewer property object (returns NULL by default, i.e. there are no property to edit)
    virtual QObject* getPropertyObject() {
        return nullptr;
    };

    /// get the viewer menu (returns NULL by default, i.e. there are no default edit menu)
    virtual QMenu* getMenu() {
        return nullptr;
    };

    /// get the viewer toolbar (returns NULL by default, i.e. there are no default toolbar)
    virtual QToolBar* getToolBar() {
        return nullptr;
    };

    /// Update the whole tree of the representation of the Component.
    /// Is actually useful (and defined) in Explorer.
    virtual void refreshInterfaceNode(Component* comp) {};

signals:

    /// this signal is emitted when the current selection was changed by the viewer
    void selectionChanged();

protected:

    /** The selection has changed to the given ComponentList.
      * This method updates the Component::selection and emit the modified signal.
      * This method should be called by the inheriting class which can select Components (e.g.: Explorer).
     */
    void selectionChanged(ComponentList& compSet);

    /// the selection has changed to be just one comp
    void selectionChanged(Component* comp);

    /// clear the selection
    void clearSelection();

};

}

#endif // VIEWER_H
