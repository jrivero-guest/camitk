/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ActionViewer.h"

#include "Application.h"
#include "Component.h"
#include "Action.h"

#include <QFrame>
#include <QComboBox>
#include <QCompleter>
#include <QLineEdit>
#include <QDialog>
#include <QGridLayout>
// Deprecated. Use Application.h to handle logger interface
//#include "Log.h"

namespace camitk {
// ---------------- constructor ----------------
ActionViewer::ActionViewer() : Viewer("Action Viewer") {
    myWidget = nullptr;
    familyComboBox = nullptr;
    nameComboBox = nullptr;
    actionWidgetStack = nullptr;
    searchFramePanel = nullptr;
}

// ---------------- getInstance ----------------
ActionViewer* ActionViewer::getInstance() {
    //static instanciation, static method variable
    static ActionViewer* actionViewer = nullptr;
    if (!actionViewer) {
        actionViewer = new ActionViewer();
    }

    return actionViewer;
}

// -------------------- refresh --------------------
void ActionViewer::refresh(Viewer* whoIsAsking) {
    updateActionViewer(ViewerRefresh);
}

// -------------------- getWidget --------------------
QWidget* ActionViewer::getWidget(QWidget* parent) {
    // lazy instanciation
    if (!myWidget) {
        // if no parent then put this in a dialog
        if (!parent) {
            // put it in a dialog, this is the case for default MainWindow, who does not add the ActionViewer in a dock
            QDialog* myWidgetIsADialog = new QDialog(nullptr);
            myWidgetIsADialog->setWindowTitle(objectName());
            myWidget = myWidgetIsADialog;
        }
        else {
            myWidget = new QWidget();
        }

        auto* actionWidgetLayout = new QVBoxLayout();

        //-- build the search frame widget
        searchFramePanel = new QFrame();
        searchFramePanel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
        searchFramePanel->setLineWidth(3);

        auto* searchFrameLayout = new QGridLayout();
        searchFrameLayout->addWidget(new QLabel("Family:"), 0, 0);
        familyComboBox = new QComboBox();
        searchFrameLayout->addWidget(familyComboBox, 0, 1);
        searchFrameLayout->addWidget(new QLabel("Action:"), 1, 0);
        nameComboBox = new QComboBox();
        searchFrameLayout->addWidget(nameComboBox, 1, 1);
        searchFrameLayout->addWidget(new QLabel("Tag:"), 2, 0);
        tagLineEdit = new QLineEdit();
        searchFrameLayout->addWidget(tagLineEdit, 2, 1);

        // add everything to the research frame
        searchFramePanel->setLayout(searchFrameLayout);
        actionWidgetLayout->addWidget(searchFramePanel);
        // no search panel by default
        searchFramePanel->setVisible(false);

        //-- build the stackedWidget to the action widget layout
        actionWidgetStack = new QStackedWidget();
        actionWidgetStack->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
        actionWidgetStack->setLineWidth(3);
        // insert empty widget to fill the space by default
        emptyActionWidgetIndex = actionWidgetStack->addWidget(new QWidget());
        // init history insert new index in history for empty selection
        ComponentList emptySelection;
        widgetHistory.insert(emptySelection, NULL);
        actionWidgetLayout->addWidget(actionWidgetStack);

        // set the action widget layout
        myWidget->setLayout(actionWidgetLayout);
    }

    // limit the width
    myWidget->setMaximumWidth(450);
    return myWidget;
}

// -------------------- setSearchPanelVisible --------------------
void ActionViewer::setSearchPanelVisible(bool visibility) {
    if (searchFramePanel) {
        searchFramePanel->setVisible(visibility);

        if (visibility) {
            // Connect buttons
            QObject::connect(familyComboBox, SIGNAL(activated(int)), this, SLOT(changeFamily()));
            QObject::connect(nameComboBox, SIGNAL(activated(int)), this, SLOT(changeName()));
            QObject::connect(tagLineEdit, SIGNAL(editingFinished()), this, SLOT(changeTag()));
        }
        else {
            // disconnect buttons
            QObject::disconnect(familyComboBox, SIGNAL(activated(int)), this, SLOT(changeFamily()));
            QObject::disconnect(nameComboBox, SIGNAL(activated(int)), this, SLOT(changeName()));
            QObject::disconnect(tagLineEdit, SIGNAL(editingFinished()), this, SLOT(changeTag()));
        }
    }
}

// -------------------- changeActionNameComboBox --------------------
void ActionViewer::changeName() {
    action = Application::getAction(nameComboBox->currentText());
    updateActionViewer(ActionNameChanged);
}

// -------------------- changeTag --------------------
void ActionViewer::changeTag() {
    updateActionViewer(ActionTagChanged);
}

// -------------------- changeFamilyComboBox --------------------
void ActionViewer::changeFamily() {
    updateActionViewer(ActionFamilyChanged);
}

// ---------------- componentListLessThan ----------------
bool operator<(const ComponentList& l1, const ComponentList& l2) {
    // This method is needed by the QMap for history
    if (l1.size() < l2.size()) {
        return true;
    }
    else {
        int i = 0;

        while (i < l1.size() && l1.value(i) == l2.value(i)) {
            i++;
        }

        if (i == l1.size()) {
            return false;
        }
        else {
            return (l1.value(i) < l2.value(i));
        }
    }
}

// -------------------- updateActionViewer --------------------
void ActionViewer::updateActionViewer(UpdateReason reason) {
    familyComboBox->blockSignals(true);
    nameComboBox->blockSignals(true);
    tagLineEdit->blockSignals(true);

    ActionList possibleActions;

    switch (reason) {
        case ActionFamilyChanged:
            //-- family was changed, fill the name combo box with this family actions
            nameComboBox->clear();
            nameComboBox->addItem("-- Select Action --");

            if (Application::getSelectedComponents().size() > 0) {
                possibleActions = Application::getActions(Application::getSelectedComponents());
            }
            else
                // no selection => select only empty component actions
            {
                possibleActions = Application::getActions(nullptr);
            }

            foreach (Action* action, possibleActions) {
                if (action->getFamily() == familyComboBox->currentText() || familyComboBox->currentText() == QString("-- Select Family --")) {
                    nameComboBox->addItem(action->getName());
                }
            }
            nameComboBox->model()->sort(0);
            break;

        case ActionNameChanged:

            //-- action name whas changed -> trigger the corresponding action
            if (action) {
                familyComboBox->setCurrentIndex(familyComboBox->findText(action->getFamily()));
                action->trigger();
            }

            break;

        case ActionTagChanged: {
            //-- tag field was changed, fill the name combo box with the corresponding actions
            ActionList actionset = Application::getActions(Application::getSelectedComponents(), tagLineEdit->text());

            if (!actionset.isEmpty()) {
                if (actionset.size() == 1) {
                    action = *(actionset.begin());
                    nameComboBox->setCurrentIndex(nameComboBox->findText(action->getName()));
                    updateActionViewer(ActionNameChanged);
                }
                else {
                    nameComboBox->clear();
                    nameComboBox->addItem("-- Select Action --");
                    foreach (Action* action, actionset) {
                        nameComboBox->addItem(action->getName());
                    }
                }

                nameComboBox->model()->sort(0);
            }
        }
        break;

        case ViewerRefresh:
        default: {

            //-- selection was changed, update combo boxes and tag list, and then check history
            // copy selected component list (so that references are not used for comparison)
            ComponentList selected(Application::getSelectedComponents());
            // if selection did not change between two updates, no need to refresh the widget
            if (currentlySelected != selected) {
                currentlySelected = selected;
                //-- update the search panel
                familyComboBox->clear();
                nameComboBox->clear();
                QStringList wordList;
                familyComboBox->addItem("-- Select Family --");
                nameComboBox->addItem("-- Select Action --");
                if (selected.size() > 0) {
                    possibleActions = Application::getActions(selected.last());
                }
                else {   // no selection => select only empty component actions
                    possibleActions = Application::getActions(nullptr);
                }  // Complete family and action lists depending of the last selected component
                foreach (Action* action, possibleActions) {
                    if (familyComboBox->findText(action->getFamily()) == -1) {
                        familyComboBox->addItem(action->getFamily());
                    }
                    if (nameComboBox->findText(action->getName()) == -1) {
                        nameComboBox->addItem(action->getName());
                    }
                    foreach (QString tag, action->getTag()) {
                        if (!wordList.contains(tag)) {
                            wordList.append(tag);
                        }
                    }
                }
                nameComboBox->model()->sort(0);
                familyComboBox->model()->sort(0);
                auto* completer = new QCompleter(wordList, myWidget);
                completer->setCaseSensitivity(Qt::CaseInsensitive);
                tagLineEdit->setCompleter(completer);

                //-- check history
                // check if the same list of Component* was already used
                QMap<ComponentList, Action*>::const_iterator it = widgetHistory.find(selected);
                if (it != widgetHistory.end()) {
                    // show the previously used widget
                    if (it.value() == NULL) {
                        actionWidgetStack->setCurrentIndex(emptyActionWidgetIndex);
                    }
                    else {  // update search panel
                        familyComboBox->setCurrentIndex(familyComboBox->findText(it.value()->getFamily()));
                        nameComboBox->setCurrentIndex(nameComboBox->findText(it.value()->getName()));
                        it.value()->trigger();
                    }
                }
                else {
                    // insert new index in history (NULL action, with empty widget)
                    widgetHistory.insert(selected, NULL);
                    actionWidgetStack->setCurrentIndex(emptyActionWidgetIndex);
                }
            }
        }
    }

    familyComboBox->blockSignals(false);
    nameComboBox->blockSignals(false);
    tagLineEdit->blockSignals(false);
}

// -------------------- embedActionWidget --------------------
void ActionViewer::embedActionWidget(Action* action) {
    // make sure the action viewer is visible (even if in a dockwidget)
    if (getWidget()->parentWidget()) {
        getWidget()->parentWidget()->setVisible(true);
        getWidget()->setVisible(true);
    }

    //-- check history
    QWidget* actionWidget = action->getWidget();
    int actionWidgetIndex = actionWidgetStack->indexOf(actionWidget);

    if (actionWidgetIndex == -1 && actionWidget) {
        // add the widget (beware that actionWidgetStack then takes ownership of the widget!)
        actionWidgetIndex = actionWidgetStack->addWidget(actionWidget);
    }

    // insert new index in history
    ComponentList selected(Application::getSelectedComponents());
    widgetHistory.insert(selected, action);
    actionWidgetStack->setCurrentIndex(actionWidgetIndex);
    actionWidgetStack->update();
    // ignore size policy of widget so that it can be resized
    actionWidgetStack->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

    // update search panel
    familyComboBox->setCurrentIndex(familyComboBox->findText(action->getFamily()));
    updateActionViewer(ActionFamilyChanged); // to have only the actions of the same family
    nameComboBox->setCurrentIndex(nameComboBox->findText(action->getName()));
}

}
