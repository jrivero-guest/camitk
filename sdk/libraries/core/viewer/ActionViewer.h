/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ACTIONVIEWER_H
#define ACTIONVIEWER_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QStackedWidget>
#include <QCompleter>
#include <QLineEdit>

#include "Viewer.h"
#include "Action.h"
#include "Component.h"

namespace camitk {
/**
 * @ingroup group_sdk_libraries_core_viewer
 *
 * @brief
 * ActionViewer is the viewer used to manage the actions.
 *
 * By default all action widgets are displayed in this viewer widget (in the stackedWidget).
 * There is also a search panel to find an action to apply to the currently selected component.
 * The search panel is not not shown by default. Use setSearchPanelVisible(true) to show it.
 *
 * This viewer also manages a singleton (THE CamiTK action viewer). See getInstance() for more information.
 * You do not have to use it, but it is convienent (and sometimes preferable) to use this instance
 * instead of creating your own one.
 */
class CAMITK_API ActionViewer : public Viewer {
    Q_OBJECT

public:
    /** @name General
      */
    ///@{
    /// constructor
    ActionViewer();

    /// destructor
    ~ActionViewer() override = default;

    /// returns the unique instance of ActionViewer
    static ActionViewer* getInstance();
    //@}

    /// @name Inherited from Viewer
    //@{
    /// returns the number of Component that are displayed by this viewer
    unsigned int numberOfViewedComponent() {
        return 0;
    };

    /// refresh the view (can be interesting to know which other viewer is calling this)
    void refresh(Viewer* whoIsAsking = nullptr) override;

    /// get the viewer widget. @param parent the parent widget for the viewer widget
    QWidget* getWidget(QWidget* parent = nullptr) override;
    //@}

    /// @name Specific to the Action viewer
    //@{
    /// embed an action widget in the stacked widget
    void embedActionWidget(Action*);

    /// show/hide the search panel (hidden by default)
    void setSearchPanelVisible(bool);
    //@}

protected slots :
    /// Method used to change the action selected
    void changeName();

    /// Method used to change the action family selected
    void changeFamily();

    /// Method used to change the tag
    void changeTag();

private:
    /// Enum the different fields of the action viewwer
    enum UpdateReason {ActionFamilyChanged, ActionNameChanged, ActionTagChanged, ViewerRefresh};

    /// method used to update the viewer for a given update field
    void updateActionViewer(UpdateReason);

    /// Main action widget of the viewer
    QWidget* myWidget;

    /// Family combo box
    QComboBox* familyComboBox;

    /// Action name combo box
    QComboBox* nameComboBox;

    /// Current action
    Action* action;

    /// action tags line edit
    QLineEdit* tagLineEdit;

    /// actions stacked widget of the viewer
    QStackedWidget* actionWidgetStack;

    /// the search panel
    QFrame* searchFramePanel;

    /// index of the empty widget, used when no action is active or when no action has been used for the currently selected components
    int emptyActionWidgetIndex;

    /// this map stores the list of selected component and the corresponding stack index of their embedded action
    QMap<ComponentList, Action*> widgetHistory;

    /// Used to evaluate modification of the list while execution
    ComponentList currentlySelected;
};
}

#endif // ACTIONVIEWER_H
