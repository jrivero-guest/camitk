/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef FRAMEEXPLORER_H
#define FRAMEEXPLORER_H

// -- Core stuff
#include "Viewer.h"

// -- QT stuff
#include <QTreeWidget>
#include <QTreeWidgetItem>

namespace camitk {
// -- Core stuff classes
class InterfaceFrame;

/**
 * @ingroup group_sdk_libraries_core_viewer
 *
 * @brief
 * Explorer window, display the list of all data currently opened in the application.
 * All objects are displayed in a QListView widget and can be selected
 * (single/multiple selection is available).
 *
 * \image html libraries/explorer.png "The component explorer viewer."
 *
 * This viewer also manages a singleton (THE CamiTK explorer). See getInstance() for more information.
 * You do not have to use it, but it is convienent (and sometimes preferable) to use this instance
 * instead of creating your own one.
 */
class CAMITK_API FrameExplorer : public Viewer {
    Q_OBJECT

public:
    /** @name General
      */
    ///@{
    /** Construtor */
    FrameExplorer();

    /** Destructor */
    ~FrameExplorer() override = default;

    /// returns the unique instance of ActionViewer
    static FrameExplorer* getInstance();
    /// @}

    /** @name Inherited from Viewer
      */
    ///@{
    /// returns the number of Component that are displayed by this viewer
    unsigned int numberOfViewedComponent() override;

    /// refresh the explorer (can be interesting to know which other viewer is calling this)
    void refresh(Viewer* whoIsAsking = nullptr) override;

    /// get the explorer widget (QTreeWidget). @param parent the parent widget for the viewer widget
    QWidget* getWidget(QWidget* parent) override;

    /// get the explorer preference widget (widget where all preferences can be modified). @param parent the parent widget for the preference widget
    virtual QWidget* getPreferenceWidget(QWidget* parent);

public slots :

private slots :

    /// slot called whenever the selection changed in the explorer
    void selectionChanged();

private:

    /// @name QTreeWidget and QTreeWidgetItem management
    ///@{
    /// instanciate a new QTreeWidgetItem using names and properties from the InterfaceNode, and using parent
    QTreeWidgetItem* getNewItem(QTreeWidgetItem* parent, Component*);

    /// recursively add the Component in the tree explorer and return the QTreeWidgetItem of the InterfaceNode
    QTreeWidgetItem* add(QTreeWidgetItem*, Component*);

    /** Add the given Component to the explorer (at top level) and automatically create children Component items.
     * @param comp The Component to add in the tree view.
     */
    void add(Component* comp);

    /// clear the tree explorer
    void remove();


    /// the list view
    QTreeWidget* explorerTree;
    ///@}

};

}


#endif
