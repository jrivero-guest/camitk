/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef MEDICAL_IMAGE_VIEWER_H
#define MEDICAL_IMAGE_VIEWER_H

// -- Core stuff
#include "CamiTKAPI.h"
#include "Viewer.h"

// -- QT stuff
#include <QFrame>
#include <QGridLayout>
#include <QAction>
#include <QBoxLayout>

namespace camitk {
// -- Core stuff classes
class InteractiveViewer;

/**
  * @ingroup group_sdk_libraries_core_viewer
  *
  * @brief
  * The views manager.
  *
  * MedicalImageViewer is a viewer that can display from 1 to 4 InteractiveViewer that represents
  * the axial, coronal, sagittal and 3D view of the same medical image.
  * The views used the default InteractiveViewers singletons.
  *
  * This class follow the "singleton" design pattern, see getInstance().
  * Singleton is enforced/recommanded so that actions can directly use the show*Viewer() methods
  *
  * \image html libraries/medicalimageviewer.png "The medical image viewer"
  *
  */
class CAMITK_API MedicalImageViewer : public Viewer {
    Q_OBJECT

public:
    /// \enum LayoutVisibility describes the possible currently displayed InteractiveViewer
    enum LayoutVisibility {
        VIEWER_ALL,        ///< All InteractiveViewer are visible
        VIEWER_3D,         ///< Only the 3D InteractiveViewer are visible
        VIEWER_AXIAL,      ///< Only the axial InteractiveViewer are visible
        VIEWER_CORONAL,    ///< Only the coronal InteractiveViewer are visible
        VIEWER_SAGITTAL,   ///< Only the sagittal InteractiveViewer are visible
        VIEWER_ARBITRARY  ///< Only the arbitrary InteractiveViewer are visible
    };

    /// get singleton instance
    static MedicalImageViewer* getInstance();

    /** destructor */
    ~MedicalImageViewer() override;

    /** @name Viewer inherited
      */
    /// @{
    /// returns the number of Component that are displayed by this viewer
    unsigned int numberOfViewedComponent() override;

    /// refresh the view (can be interesting to know which other viewer is calling this)
    void refresh(Viewer* whoIsAsking = nullptr) override;

    /// get the viewer widget. @param parent the parent widget for the viewer widget
    QWidget* getWidget(QWidget* parent = nullptr) override;

    /// get the propertyObject (only the 3D Scene one)
    QObject* getPropertyObject() override;

    /// get the viewer menu
    QMenu* getMenu() override;

    /// get the viewer toolbar
    QToolBar* getToolBar() override;

    /** force toolbar visibility.
     *  The toolbar is normally updated automatically on layout update (see updateLayout()) and is visible
     *  if all the 3D scene is visible (which is the default visualization state).
     *  Calling this method with false will avoid this automatic update.
     *
     *  By default the toolbar is automatically update
     */
    virtual void setToolbarAutoVisibility(bool);
    /// @}

    /// called to change the layout, i.e. which viewer is visible
    void setVisibleViewer(LayoutVisibility);

public slots:
    /// called when an internal InteractiveViewers has emitted a selectionChanged signal
    void synchronizeSelection();

protected:

    /// Protected construtor (singleton)
    MedicalImageViewer();

private:
    /// set the current visibility of the different viewer
    void updateLayout();

    /// The main layout
    QGridLayout* frameLayout;

    /// the layout for the arbitrary/axial at the top left position of frameLayout
    QVBoxLayout* topLeftLayout;

    /// the main widget
    QFrame* frame;

    /// the QMenu for the MedicalImageViewer
    QMenu* viewerMenu;

    /// which viewer(s) is/are currently visible
    LayoutVisibility visibleLayout;

    /// contains all InteractiveViewer instance (access them by LayoutVisibility)
    QMap<LayoutVisibility, Viewer*> viewers;

    /// viewer visibility enum
    QList<LayoutVisibility> viewerVisibility;

    /// number of top-level component that are currently displayed
    unsigned int displayedTopLevelComponents;

    /// the default instance (singleton)
    static MedicalImageViewer* singleton;

    /// if true, the toolbar automatically updated
    bool autoUpdateToolbarVisibility;
};

}


#endif

