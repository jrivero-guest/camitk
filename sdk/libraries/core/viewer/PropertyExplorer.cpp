/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include "Application.h"
#include "PropertyExplorer.h"
#include "Component.h"

// -- QT stuff
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTabWidget>
#include <QSettings>

namespace camitk {

// ---------------- constructor ----------------
PropertyExplorer::PropertyExplorer() : Viewer("Property Explorer") {
    theController = nullptr;
    tabWidget = nullptr;
    revertButton = nullptr;
    applyButton = nullptr;
    currentComponent = nullptr;

    createProperties();
}

// ---------------- destructor ----------------
PropertyExplorer::~PropertyExplorer() {
    clear();
    if (propertyObject) {
        delete propertyObject;
    }
    if (viewModeProperty) {
        delete viewModeProperty;
    }
}

//----------------------- getInstance ------------------------
PropertyExplorer* PropertyExplorer::getInstance() {
    // static instanciation, static method variable
    static PropertyExplorer* propExplorer = nullptr;
    if (!propExplorer) {
        propExplorer = new PropertyExplorer();
    }

    return propExplorer;
}

// ---------------- numberOfViewedComponent ----------------
unsigned int PropertyExplorer::numberOfViewedComponent() {
    if (currentComponent) {
        return 1;
    }
    else {
        return 0;
    }
}

// ---------------- refresh ----------------
void PropertyExplorer::refresh(Viewer* whoIsAsking) {
    // refresh the explorer if there is one selected Component
    if (!Application::getSelectedComponents().isEmpty()) {
        // last selected is different from currently selected
        if (currentComponent != Application::getSelectedComponents().last()) {
            //-- clear
            clear();
            //-- update currentComponent
            currentComponent = Application::getSelectedComponents().last();
            currentComponent->setVisibility(this, true);
            //-- get the property widgets
            QWidget* customWidget;
            for (unsigned int i = 0; i < currentComponent->getNumberOfPropertyWidget(); i++) {
                customWidget = currentComponent->getPropertyWidgetAt(i);
                if (customWidget) {
                    tabWidget->addTab(customWidget, customWidget->objectName());
                }
            }
        }
        //--  update the property editor
        // first clear all (otherwise if a new dynamic properties was added, it won't show)
        theController->setObject(nullptr);
        // rebuild all GUI
        theController->setObject(currentComponent->getPropertyObject());

        //-- select the desired tab in the PropertyExplorer
        this->selectIndex(currentComponent->getIndexOfPropertyExplorerTab());
    }
    else {
        // clean everything only if there is no selection and cleaning was not already done
        if (currentComponent != nullptr) {
            // clear tabs
            clear();
            theController->setObject(nullptr);
        }
    }

    //-- update button states
    revertButton->setEnabled(currentComponent != nullptr && currentComponent->getPropertyObject() != nullptr);
    applyButton->setEnabled(currentComponent != nullptr && currentComponent->getPropertyObject() != nullptr);
}

// ---------------- getWidget ----------------
QWidget* PropertyExplorer::getWidget(QWidget* parent) {
    if (tabWidget == nullptr) {
        tabWidget = new QTabWidget(parent);
        tabWidget->setWindowTitle(tr("Property Tabs"));

        // create the property editor tab itself
        auto* propertyFrame = new QFrame;
        auto* propertyTabLayout = new QVBoxLayout;
        propertyTabLayout->setSpacing(2);
        propertyTabLayout->setMargin(2);

        // get user pref for view mode
        QSettings& settings = Application::getSettings();
        settings.beginGroup("PropertyExplorer");
        ObjectController::ViewMode mode = (ObjectController::ViewMode) propertyObject->property(viewModeProperty->getName().toStdString().c_str()).toInt();
        settings.endGroup();

        // first the property editor
        theController = new ObjectController(propertyFrame, mode);
        propertyTabLayout->addWidget(theController);

        // then the buttons
        auto* buttonFrame = new QFrame(parent);
        auto* buttonLayout = new QHBoxLayout;

        applyButton = new QPushButton("Apply");
        buttonLayout->addWidget(applyButton);
        applyButton->show();
        applyButton->setEnabled(false);

        revertButton = new QPushButton("Revert");
        buttonLayout->addWidget(revertButton);
        revertButton->show();
        revertButton->setEnabled(false);

        buttonFrame->setLayout(buttonLayout);
        propertyTabLayout->addWidget(buttonFrame);

        // connect the buttons
        QObject::connect(applyButton, SIGNAL(clicked()), theController, SLOT(apply()));
        QObject::connect(applyButton, SIGNAL(clicked()), this, SLOT(refreshAll()));
        QObject::connect(revertButton, SIGNAL(clicked()), theController, SLOT(revert()));

        propertyFrame->setLayout(propertyTabLayout);

        tabWidget->addTab(propertyFrame, tr("P&roperties"));

        // connect the tab widget index change signal
        QObject::connect(this->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(updateTabIndexToDisplay(int)));
    }

    return tabWidget;
}

// ---------------- getPropertyObject ----------------
QObject* PropertyExplorer::getPropertyObject() {
    return propertyObject;
}

// ---------------- clear ----------------
void PropertyExplorer::clear() {
    // page #0 is the property widget (not be counted)
    for (int i = tabWidget->count() - 1; i > 0; i--) {
        tabWidget->removeTab(i);
    }

    if (currentComponent && Application::isAlive(currentComponent)) {
        currentComponent->setVisibility(this, false);
    }
    currentComponent = nullptr;
}

// ---------------- selectWidget ----------------
void PropertyExplorer::selectWidget(QWidget* widget) {
    // get the last component tab widgets
    QWidget* customWidget;
    for (int i = 0; i < this->tabWidget->count(); i++) {
        customWidget = this->tabWidget->widget(i);
        if (customWidget && (customWidget == widget)) {
            this->tabWidget->setCurrentWidget(widget);
            return;
        }
    }
}

// ---------------- selectIndex ----------------
void PropertyExplorer::selectIndex(unsigned int index) {
    if ((index >= 0) && ((int)index < this->tabWidget->count())) {
        this->tabWidget->setCurrentIndex(index);
    }
}

// ---------------- updateTabIndexToDisplay ----------------
void PropertyExplorer::updateTabIndexToDisplay(int index) {
    if (!Application::getSelectedComponents().isEmpty()) {
        currentComponent = Application::getSelectedComponents().last();
        currentComponent->setIndexOfPropertyExplorerTab((unsigned int)index);
    }
}

// ---------------- eventFilter ----------------
bool PropertyExplorer::eventFilter(QObject* object, QEvent* event) {
    // watch propertyObject instance for dynamic property changes
    if (event->type() == QEvent::DynamicPropertyChange) {
        // objectcontroller view mode update
        ObjectController::ViewMode mode = (ObjectController::ViewMode) propertyObject->property(viewModeProperty->getName().toStdString().c_str()).toInt();
        theController->setViewMode(mode);
        QSettings& settings = Application::getSettings();
        settings.beginGroup("PropertyExplorer");
        settings.setValue("viewMode", mode);
        settings.endGroup();
        return true;

    }
    else {
        // pass the event on to the parent class
        return Viewer::eventFilter(object, event);
    }

}

// ---------------- createProperties ----------------
void PropertyExplorer::createProperties() {
    // viewer properties
    propertyObject = new PropertyObject("Property Explorer");

    // view mode property
    // get the value store in the application settings
    QSettings& settings = Application::getSettings();
    settings.beginGroup("PropertyExplorer");
    ObjectController::ViewMode mode = (ObjectController::ViewMode) settings.value("viewMode", ObjectController::BUTTON).toInt();
    settings.endGroup();

    viewModeProperty = new Property("View mode", mode, "The display type of the PropertyExplorer", "");
    viewModeProperty->setEnumTypeName("camitk::ObjectController::ViewMode");
    QStringList viewModeNames;
    viewModeNames << "Tree" << "Groupbox" << "Button";
    viewModeProperty->setAttribute("enumNames", viewModeNames);
    propertyObject->addProperty(viewModeProperty);

    // propertyObject is monitored by the PropertyExplorer instance (this) when its properties change
    propertyObject->installEventFilter(this);
}

// ---------------- createProperties ----------------
void PropertyExplorer::refreshAll() {
    Application::refresh();
}


}
