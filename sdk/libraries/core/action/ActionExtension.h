/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ACTION_EXTENSION_H
#define ACTION_EXTENSION_H

#include "CamiTKAPI.h"

// -- QT stuff
#include <QtPlugin>
#include <QPluginLoader>
#include <QObject>
#include <QTranslator>

namespace camitk {
class Action;

// a bit simplistic, but greatly simply syntax (and explanation)
// to be used in an ActionExtension register method
#define registerNewAction(X) registerAction(new X(this))

/**
 * @ingroup group_sdk_libraries_core_action
 *
 * @brief
 * This class describes what is a generic Action extension.
* To add a ActionExtension to CamiTK core, write a new class that inherits from this class.
*
* The following methods HAVE to be redefined in your subclass:
* - getName: return the name of your extension
* - getDescription: return a small description
* - init: a simple enough method, just call registerNewAction(MyAction) for any
*   MyAction class inheriting from camitk::Action
*
* @see BasicMeshExtension For an example of an extension registering more than one actions
*/
class CAMITK_API ActionExtension : public QObject {

protected :
    /// constructor
    ActionExtension() {};

public :
    /// destructor
    ~ActionExtension() override;

    /// returns the action extension name (to be overriden in your ActionExtension)
    virtual QString getName() = 0;

    /// returns the action extension small description (to be overriden in your ActionExtension)
    virtual QString getDescription() = 0;

    /// this method should just call registerNewAction(MyAction) for any MyAction class you need to register by this extension
    virtual void init() = 0;

    /// get the list of actions registered y this extension
    const ActionList& getActions();

    /// set the file path (once loaded as a dynamic library)
    void setLocation(const QString loc) {
        dynamicLibraryFileName = loc;
    }

    /// get the file path (location of the .dll/.so/.dylib) of this plugin
    QString getLocation() const {
        return dynamicLibraryFileName;
    }

    /// Load, for the selected langage (asked to the Application), the associated .qm file
    void initResources();

protected:
    /// register an action instance
    void registerAction(Action*);

    /// the list of actions
    ActionList actions;

private:
    /// the shared lib (.so, .dll or .dylib) used to instanciate the ComponentExtension subclass instance
    QString dynamicLibraryFileName;

    /// Provide internationalization support for text output.
    QTranslator* translator{nullptr};
};

}
// -------------------- declare the interface for QPluginLoader --------------------
Q_DECLARE_INTERFACE(camitk::ActionExtension, "TIMC-IMAG. Action Extension/2.1") //TODO use variable from CMake?


#endif //ACTION_EXTENSION_H
