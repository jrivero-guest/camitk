/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef HISTORYCOMPONENT_H
#define HISTORYCOMPONENT_H

// Qt stuffs
#include <QString>

// CamiTK stuffs
#include <Component.h>

namespace camitk {


/**
 * @ingroup group_sdk_libraries_core
 *
 * @brief
 * HistoryComponent class describes the component information (name, type) stored in the history
 *
 * In CamiTK, every Action are stored in a history, which can be saved as a XML file. \n
 * Each history entry is an instance of HistoryItem and provides information about the processed action :
 * - the name of the action processed.
 * - the properties (name, values).
 * - its input components, stored as HistoryComponent instances.
 * - its output components, stored as HistoryComponent instances.
 *
 * History items are mainly useful for getting back to a previous state by undoing an action or for scripting by storing in a file a pipeline of actions written in Python. \n
 *
 * The history is stored in the Application class and uses the "construct on first use" idiom/design-pattern (with singletons). \n
 * It therefore avoids the infamous "static initialization order fiasco",
 * see http://www.parashift.com/c++-faq/ctors.html
 *
 * @see HistoryItem
 */
class CAMITK_API HistoryComponent {
public:
    /** \enum Type
     * The different Type (of representation) of the associated Component.
     * Use getType() to get the information about a specific HistoryComponent.
     *
     * \note The type is highly linked to Component::Representation information :
     * * ImageComponent instances have subcomponents of representation SLICE.
     * * MeshComponent instances have a representation of type GEOMETRY.
     * * Other components have no representation (NO_REPRESENTATION), and are thus of type OTHER.
     *
     * \note
     * As for Component::Representation the HistoryComponent type cannot be NULL : if a Component has no representation, then he is of type OTHER
     */
    enum Type {
        IMAGE_COMPONENT, ///< this Component represents a volumic image and its subcomponents can be displayed as slices.
        MESH_COMPONENT, ///< this Component represent a 3D mesh, it has a 3D representation which can be displayed in the 3D viewer.
        OTHER      ///< this Component has no defined representation.
    };

private:
    /// @name Private properties
    /// @{

    /// The name of the Component.
    QString name;

    /// The component's Type.
    /// Type is a persistent information, stored in the XML.
    Type type;
    /// @}

public:
    /// @name Constructors
    ///@{

    /**
    * Construct a new History component from the input component.
    * @param component The Component associated to the HistoryComponent.
    */
    HistoryComponent(Component* component);
    ///@}

    /// @name Accessors
    ///@{

    /// Get the the name of the Component associated to this item.
    /// @return the name of the associated Component.
    QString getName() const;

    /// Get the Type of the Component associated to this item.
    /// @return the Type of the associated Component.
    Type getType() const;
    /// @}


};

}; // namespace camitk

#endif // HISTORYCOMPONENT_H
