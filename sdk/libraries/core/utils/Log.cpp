/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include "Log.h"
#include "InterfaceLogger.h"
#include "CamiTKLogger.h"

namespace camitk {
/// Global Logger manager
InterfaceLogger* Log::applicationLogger = nullptr;

// --------------------- setLogger ---------------------
void Log::setLogger(InterfaceLogger* logger) {
    // Ensures that the given logger really exists, otherwise, do not change anything...
    if (logger != nullptr) {
        // If there is already a logger, delete it so that it can finish everything properly
        // Check that it is not the existing logger otherwise there will no logger any more !
        if ((logger != applicationLogger) && (applicationLogger != nullptr)) {
            delete applicationLogger;
        }
        applicationLogger = logger;
    }

}

// --------------------- getLogger ---------------------
InterfaceLogger* Log::getLogger() {
    // lazy instancation but make sure that there is always a logger
    if (applicationLogger == nullptr) {
        applicationLogger = new CamiTKLogger();
    }
    return applicationLogger;
}

// --------------------- getLevelAsText ---------------------
QString Log::getLevelAsString(InterfaceLogger::LogLevel level) {
    QString levelString;
    switch (level) {
        case InterfaceLogger::NONE:
            levelString = "NONE";
            break;
        case InterfaceLogger::ERROR:
            levelString = "ERROR";
            break;
        case InterfaceLogger::WARNING:
            levelString = "WARNING";
            break;
        case InterfaceLogger::INFO:
            levelString = "INFO";
            break;
        case InterfaceLogger::TRACE:
            levelString = "TRACE";
            break;
        default:
            levelString = "UNKNOWN";
            break;
    }

    return levelString;
}

// --------------- getLevelFromString -------------------
InterfaceLogger::LogLevel Log::getLevelFromString(QString levelString) {
    InterfaceLogger::LogLevel logLevel;
    if (levelString.toUpper() == "NONE") {
        logLevel = InterfaceLogger::NONE;
    }
    else if (levelString.toUpper() == "TRACE") {
        logLevel = InterfaceLogger::TRACE;
    }
    else if (levelString.toUpper() == "INFO") {
        logLevel = InterfaceLogger::INFO;
    }
    else if (levelString.toUpper() == "WARNING") {
        logLevel = InterfaceLogger::WARNING;
    }
    else if (levelString.toUpper() == "ERROR") {
        logLevel = InterfaceLogger::ERROR;
    }
    else {
        logLevel = InterfaceLogger::NONE;
    }

    return logLevel;
}

}
