/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef OBJECTCONTROLLER_H
#define OBJECTCONTROLLER_H

// -- CamiTK includes
#include "CamiTKAPI.h"
#include <QtVariantProperty>

// -- QT stuff
#include <QMetaEnum>
#include <QPushButton>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QWidget>
#include <csignal>


namespace camitk {
class ObjectControllerPrivate;


/**
 *
 * @ingroup group_sdk_libraries_core_utils
 *
 * @brief
 * The object controller class.

  This is a special "magical" widget.
  It allows one to show the meta properties and dynamic property (see Qt Object Model / Property for lmore
  information on these).
  This class also automatically manage camitk::Property for components and actions.
  It is STRONGLY recommanded to use camitk::Property instead of Qt Meta Object property.

  \note
  In order to benefit from the camitk::Property decoration, the class that needs to be edited
  by the ObjectController have to declare a getProperty() method using the Q_INVOKABLE macro.
  E.g.:
  Q_INVOKABLE virtual Property* getProperty(QString name);

  A complete example that show the usability of object controller is available in applications
  "objectcontroller". The best way to learn how to use this Core facility is to look at
  this application and the given TestClass.
  The class PropComponent and PropAction in the tutorials demonstrates how to use camitk::Property
  instead of Qt Meta Object Property.

  An object controller is a sort of property browser where each line represents a property.
  The object controller automatically build a GUI to access and optionally changed
  the properties you defined.

  There could be many different types of properties, and as many as you want.

  In order to handle a list of specific properties for your class, you
  have three choices:
  - use camitk::Property (in Components or Actions): this is the recommanded way.
  camitk:Property overcomes the limitation of dynamic Qt Meta Object Properties.
  - write a new class inheriting from QObject and declare as many Q_PROPERTY as
  needed. When send to the ObjectController class the Q_PROPERTY list will be
  read and the GUI interfaces build.
  - generate dynamic properties on-the-fly

  The first choice is the best as you can specify description, have read only property,
  define min and max values and step for numeric properties as well as precision for
  float/double. A camitk::Property is necessarily dynamic.
  For component and action properties, this is the way to go.

  For a class that is neither a component or an action, you can still use the second
  and third method.
  The third method does even not need a new class. You have to create a new QObject
  instance, and add as many dynamic properties as you wish using QObject::setProperty
  method.
  You also have to install an event monitor in your class to be notified when a property
  has changed (see TestClass in "objectcontroller" application).

  Three different view mode are available to represent/interact with the property values:
     - TREE based on a list view
     - GROUPBOX same as TREE, but each group of properties are viewed in a group box
     - BUTTON same as GROUPBOX, but buttons allow the user to show/hide the sub properties

  The view mode can be changed at any time, even at run-time (see the "objectcontroller" application).

  How to use this class:
     - the constructor, to create the widget:
\code
    // default view mode is TREE:
    ObjectController *theController = new ObjectController(this);
    // Choose another initial view mode:
    ObjectController *theController = new ObjectController(this, ObjectController::BUTTON );
\endcode
     - to view/interact with the properties of a given QObject:
\code
    theController->setObject(myObject);
\endcode
     - to change the view mode:
\code
    theController->setViewMode(ObjectController::GROUPBOX);
\endcode

  \note
  And... that's it! This method is easier, but does not work well for Q_ENUMS (enumerator).
  If you have an enum/Q_ENUMS property, you have to use the second method below.

  The available property type are (see example in the "objectcontroller" application (tutorials),
  the tutorial class PropComponent and PropAction
  and (if you feel adventurous) also QtVariantProperty in tools/qpropertybrowser for a complete list of associated constraints):

    Property Type | Property Type Id
    ------------- | -------------
    int           | QVariant::Int
    double        | QVariant::Double
    bool          | QVariant::Bool
    QString       | QVariant::String
    QVector3D     | QVariant::QVector3D
    QColor        | QVariant::Color
    QDate         | QVariant::Date
    QTime         | QVariant::Time
    QChar         | QVariant::Char
    QDateTime     | QVariant::DateTime
    QPoint        | Variant::Point
    QPointF       | QVariant::PointF
    QKeySequence  | QVariant::KeySequence
    QLocale       | QVariant::Locale
    QSize         | QVariant::Size
    QSizeF        | QVariant::SizeF
    QRect         | QVariant::Rect
    QRectF        | QVariant::RectF
    QSizePolicy   | QVariant::SizePolicy
    QFont         | QVariant::Font
    QCursor       | QVariant::Cursor
    enum          | enumTypeId()
    flag          | flagTypeId()
    group         | groupTypeId()

  \note in Q_PROPERTY if you don't specify a WRITE method, then the property is set as read only.

*/

class CAMITK_API ObjectController : public QWidget {
    Q_OBJECT
    Q_ENUMS(ViewMode); // so that it can be used in property editor

public:
    // [CAMITK ADDED]
    /** \enum ViewMode The property browser can be shown in different view mode.
      * Use getViewMode() to get the information about the view mode.
      * \note the view mode can be NULL; the default value is TREE.
      */
    enum ViewMode {
        TREE, ///< The property browser can be shown like a QListView
        GROUPBOX, ///< It is like the TREE but with a delimitation to indicate the arborescence of the property type
        BUTTON ///< It is like the GROUPBOX but the buttons allow the user to control the comput display of arborescence
    };

    /// constructor of object controller with in parameters the parent widget and the view mode (default : TREE)
    ObjectController(QWidget* parent = nullptr, ViewMode viewMode = TREE);

    /// destructor of object controller
    ~ObjectController() override;

    /// mutatorMethod of the object at unroll in the property browser
    void setObject(QObject* object);

    /// accessorMethod of the object contains in the property browser
    QObject* object() const;

    /// if set, the properties are immediatly updated in the managed object (default false)
    void setAutoUpdateProperty(bool);

    // [CAMITK ADDED]
    /// mutator Method of the view mode
    void setViewMode(ViewMode viewMode);

    // [CAMITK ADDED]
    /// accessorMethod of the view mode
    ViewMode getViewMode() const;

    // [CAMITK ADDED]
    /// call this to check if a modification was done
    bool isModified();

public slots:
    // [CAMITK ADDED]
    /// slot to apply change made by the user in the property editor
    void apply();

    // [CAMITK ADDED]
    /// slot to revert the values of all properties (revert to values in memory)
    void revert();

private:
    /// private object controller wich contains pointer about object, browser and more
    ObjectControllerPrivate* d_ptr;
    Q_DECLARE_PRIVATE(ObjectController)
    Q_DISABLE_COPY(ObjectController)

    // [CAMITK CHANGED]
    Q_PRIVATE_SLOT(d_func(), void saveChange(QtProperty*, const QVariant&))
    Q_PRIVATE_SLOT(d_func(), void valueChanged(QtProperty*, const QVariant&))

    // [CAMITK ADDED]
    /// variable to choose the type of property browser
    ViewMode currentViewMode;

    // [CAMITK ADDED]
    /// initialize the view mode depending on currentViewMode
    void initViewMode();

    /// layout which will contains the propertybrowser and, in view mode GROUPBOX and BUTTON, the scroll
    QVBoxLayout* layout;

    /// scroll add to navigate in the property browser
    QScrollArea* scroll;

};

class ObjectControllerPrivate {
    ObjectController* q_ptr;
    Q_DECLARE_PUBLIC(ObjectController)
public:

    void addClassProperties(const QMetaObject* metaObject);
    void addDynamicProperties(QObject* edited);
    void updateClassProperties(const QMetaObject* metaObject, bool recursive);
    void updateDynamicProperties(const QObject* edited);
    void saveExpandedState();
    void restoreExpandedState();
    void valueChanged(QtProperty* property, const QVariant value);
    //CAMITK ADDED
    void saveChange(QtProperty* property, const QVariant& value);
    void applyChange();
    //
    int enumToInt(const QMetaEnum& metaEnum, int enumValue) const;
    int intToEnum(const QMetaEnum& metaEnum, int intValue) const;
    int flagToInt(const QMetaEnum& metaEnum, int flagValue) const;
    int intToFlag(const QMetaEnum& metaEnum, int intValue) const;
    bool isSubValue(int value, int subValue) const;
    bool isPowerOf2(int value) const;

    /// the currently edited object
    QObject*                  m_object;

    /* [CAMITK REMOVED]
        QMap<const QMetaObject *, QtProperty *> m_classToProperty;
        QMap<QtProperty *, const QMetaObject *> m_propertyToClass;
    */
    /// map from the class property to the index in the meta object
    QMap<QtProperty*, int>     m_classPropertyToIndex;
    /// Each meta object have a map to get the property by its index
    QMap<const QMetaObject*, QMap<int, QtVariantProperty*> > m_classToIndexToProperty;

    /// map from idx of the dynamic property to the property
    QMap<int, QtVariantProperty*>     m_indexToDynamicProperty;
    /// map from the dynamic property to the idx in the dynamic property
    QMap<QtProperty*, int>     m_dynamicPropertyToIndex;

    /// TODO doc
    QMap<QtProperty*, bool>    m_propertyToExpanded;

    /// TODO doc
    QList<QtProperty*>         m_topLevelProperties;

    /// when a property is modified by the user, the change is stored here (until applyChange is called)
    QMap<QtProperty*, QVariant>     saveChangeValue;

    /// the browser where all properties are shown to the user
    QtAbstractPropertyBrowser*    m_browser;

    /// the main property manager (does not include the read only property)
    QtVariantPropertyManager* m_manager;

    /// the property manager for the read only property
    QtVariantPropertyManager* m_readOnlyManager;

    // [CAMITK ADDED]
    /// build a QtVariantProperty from name, type, property...etc...
    QtVariantProperty* buildQtVariantProperty(QString name, QMetaType::Type type, QVariant value, bool isReadable, bool isWritable, bool isEnumType, bool isFlagType, bool isDesignable, QMetaEnum* metaEnum = NULL);

    /// map of all the QtProperty that are top-level groups
    QMap<QString, QtProperty*> groupProperties;
};

}

// to use this enum as a CamiTK property
Q_DECLARE_METATYPE(camitk::ObjectController::ViewMode)

#endif
