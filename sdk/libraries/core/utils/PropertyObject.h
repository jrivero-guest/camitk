/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef PROPERTYOBJECT_H
#define PROPERTYOBJECT_H

// Qt includes
#include <QObject>

// CamiTK includes
#include "CamiTKAPI.h"
#include "Property.h"

namespace camitk {

/**
 *  @ingroup group_sdk_libraries_core_utils
 *
 *  @brief
 *  This class describes a property object.
 *
 *  A property object is simply a QObject tagged with some CamiTK Properties
 *  The idea is to have an object which implements all the necessary methods to take advantages of the
 *  CamiTK Properties within the ObjectController
 *
 *  PropertyObject can be saved/loaded from settings.
 *  A typical use in this case is to
    \code
        PropertyObject = new PropertyObject(...);

        // set default value
        propertyObject->addProperty(...);
        propertyObject->addProperty(...);
        propertyObject->addProperty(...);

        // load values from settings
        loadFromSettings("mySection");

        ...

        /// at any time values can be saved to settings
        saveToSettings("mySection")
    \endcode

    See the Application class for an example of a property object load/save to settings.

 *  @see ObjectController, Property
 */
class CAMITK_API PropertyObject : public QObject {

    Q_OBJECT

public:
    /**
     * Default constructor
     * @param name The name of the PropertyObject instance. This one would be displayed in any SettingsDialog entries.
     */
    PropertyObject(QString name);

    /// Destructor
    ~PropertyObject() override;

    /**
     * Tag a new CamiTK property to this object.
     * If the property already exist, it will just change its value.
     *
     * \note
     * The object takes ownership of the Property instance.
     *
     * @return false if the Qt Meta Object property was added by this method
     * (otherwise the property was already defined and true is returned if it was successfully updated)
     */
    virtual bool addProperty(Property*);

    /**
     *  Get a Property given its name
     *  @param name the property name
     *  @return NULL if the name does not match any property name
     *
     *  @note
     *  This method is required so that the ObjectController can take advantage of the camitk properties.
     *
     *  @see Property, ObjectController
     */
    Q_INVOKABLE virtual Property* getProperty(QString name);

    /**
     *  Remove a CamiTK property of this object
     */
    virtual void removeProperty(Property*);

    /// get the current number of property
    virtual unsigned int getNumberOfProperties() const;

    /// get the name of the property at the given index, null string if index is out of bounds (i.e., isNull() == true)
    virtual QString getPropertyName(unsigned int index) const;

    /// get the value of the property at the given index, a non valid QVariant if the index is out of bounds (i.e., isValid() == false)
    virtual QVariant getPropertyValue(unsigned int index);

    /// conveniant method to get the value of a given property, returns a non valid QVariant if no property with that name exists
    virtual QVariant getPropertyValue(const QString name);

    /// initializes all property values from setting values found in the given group name
    void loadFromSettings(const QString& settingGroupName);

    /// save setting in the given group name using all the property values
    void saveToSettings(const QString& settingGroupName);

    /// utility method to transform property name to camel case. Quite useful to make sure all settings are stored as lowerCamelCase
    static QString toCamelCase(const QString&);

private:
    /// @name Property management
    ///@{
    /// list of CamiTK properties decorating the object
    QMap<QString, Property*> propertiesMap;
    ///@}
};

}

#endif // PROPERTYOBJECT_H
