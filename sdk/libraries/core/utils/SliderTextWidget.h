/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SLIDER_TEXT_WIDGET_H
#define SLIDER_TEXT_WIDGET_H

// -- Core stuff
#include "CamiTKAPI.h"

// -- QT stuff
#include <QColor>
#include <QLabel>
#include <QLineEdit>
#include <QSlider>


namespace camitk {
/**
 * @ingroup group_sdk_libraries_core_utils
 *
 * @brief
 * This widget allows you to use a slider with a lineedit in a Dialog Box.
 * The [min,max] interval is divided by 100 line steps (10 page steps),
 * the slider controling the variation in percentage.
 *
 * 1 label is used to give the parameter a name (default is objectName())
 *
 * Your own initialization: in YourDialog class,you can init a SliderTextWidget
 * using setText(..) and init(..).
 *
 * In line edit, if the value is superior or inferior than the initial bounds, the
 * bounds are automatically updated.
 *
 *
 */
class CAMITK_API SliderTextWidget : public QWidget {
    Q_OBJECT

public:
    /// Default constructor, name is automatically used as the text label
    SliderTextWidget(QWidget* parent = nullptr, Qt::WindowFlags fl = nullptr);

    /// Destructor
    ~SliderTextWidget() override;

    /// set the text label (name of the manipulated data)
    void setName(const QString&);

    /** initialize slider and lineedit with the label, min, max, and current value
      * Default values are min=0, max=100 and current value=50
      */
    void init(double min = 0.0, double max = 100.0, double value = 50.0);

    /// Get the current value
    double getValue() const;

    /// Update slider and lineedit GUI, emit the valueChanged signal only if the boolean is true (default=no)
    void setValue(const double, bool emitValueChanged = false);

signals:

    /// Signal emitted when the value has changed (either directly using the slider or when the user pressed return)
    void valueChanged();


protected slots:
    /// when the user press return in the line edit
    void returnPressed();

    /// Update the value of the lineedit when slider moves
    void valueChanged(int);

    /// When the user change the text
    void textModified(const QString&);

private:
    /// update the slider position
    void updateSlider();

    /// Update the line edit value
    void updateLineEdit();

    /// convert from slider value to double
    double sliderToValue(const int);

    /// convert from value to slider
    int valueToSlider(const double);

    /// the current value
    double value;

    /// the line edit bg color
    QColor bgColor;

    /// the min real value
    double min;

    /// the max real value
    double max;

    /// the text label
    QLabel* label;

    /// the line edit
    QLineEdit* lineEdit;

    /// the slider
    QSlider* slider;
};

inline double SliderTextWidget::getValue() const {
    return value;
}

}
#endif // SLIDERTEXTWIDGET_H
