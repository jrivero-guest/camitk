/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include "Component.h"
#include "Viewer.h"
#include "Property.h"
#include "Action.h"
#include "Frame.h"
#include "Log.h"

// -- QT stuff
#include <QEvent>
#include <QMetaObject>
#include <QSet>
#include <utility>

namespace camitk {
// --------------------------------------------------------
// -------------------- static members --------------------
// --------------------------------------------------------
QSet<Viewer*> Component::allViewers;


// -------------------- constructor --------------------
Component::Component(QString file, QString n, Representation s) :  myParentNode(nullptr), myFileName(std::move(file)), myService(s), myName(std::move(n)) {
    init();
}


Component::Component(Component* parentComponent, const QString& n, Representation s) : myParentNode(parentComponent), myService(s), myName(n) {
    if (myParentNode == nullptr) {
        throw AbortException(tr("Inconsistency: cannot instanciate a sub component with a null parent, please use the parent component pointer as the first parameter of the constructor or use the top-level Component constructor.").toStdString());
    }
    init();
    myFileName = n;
    // register as a parent child
    myParentNode->addChild(this);
}

// -------------------- default destructor --------------------
Component::~Component() {
    // delete all reference of this component in the application
    Application::removeComponent(this);

    // delete all its children Components
    deleteChildren();

    // unregister if I got a parent component
    if (Component* parent = getParentComponent()) {
        parent->removeChild(this);
    }

    // erase all the viewers
    myViewers.clear();

    // clear the adaptee
    if (myGeometry) {
        delete myGeometry;
    }
    myGeometry = nullptr;

    if (mySlice) {
        delete mySlice;
    }
    mySlice = nullptr;

    // delete Frame
    if (myFrame) {
        delete myFrame;
    }
    myFrame = nullptr;

    // remove from selection
    if (isSelectedFlag) {
        setSelected(false, false);
    }

    // delete all properties
    foreach (Property* prop, propertyMap.values()) {
        delete prop;
    }
    propertyMap.clear();

    CAMITK_TRACE(tr("Deleted"))
}

// -------------------- init --------------------
void Component::init() {
    // insert the Component in the list of top-level components as well as the full list
    Application::addComponent(this);

    modifiedFlag = false;
    childrenComponent.clear();

    // initialize representation
    myGeometry = nullptr;
    mySlice = nullptr;
    myFrame = nullptr;
    actionsMenu = nullptr;

    // create the Frame (does not depend on any representation, should be created for any Component)
    myFrame = new Frame();

    // initialize other state
    isSelectedFlag = false;

    // install a filter to get the modification of the dynamic properties (see event method)
    installEventFilter(this);

    // by default, the 1st PropertyExplorer tab is displayed
    this->setIndexOfPropertyExplorerTab(0);

    CAMITK_TRACE(tr("Initialized"))
}


// -------------------- isInstanceOf --------------------
bool Component::isInstanceOf(QString className) const {
    if (getHierarchy().contains(className)) {
        return true;
    }
    else {
        return false;
    }
}

// -------------------- getHierarchy --------------------
QStringList Component::getHierarchy() const {
    const QMetaObject* qmetaObject = metaObject();
    QStringList classnameList;
    while (qmetaObject && !QString(qmetaObject->className()).contains("InterfaceProperty")) {
        classnameList.append(QString(qmetaObject->className()).remove(0, QString(qmetaObject->className()).lastIndexOf(':') + 1));
        if (qmetaObject->superClass()) {
            qmetaObject = qmetaObject->superClass();
        }
    }
    return classnameList;
}

// ---------------------- event ----------------------------
bool Component::event(QEvent* e) {
    if (e->type() == QEvent::DynamicPropertyChange) {
        e->accept();
        auto* chev = dynamic_cast<QDynamicPropertyChangeEvent*>(e);

        if (! chev) {
            return false;
        }

        // set the corresponding property (field) by calling the updateProperty method
        // warning: to avoid never ending notification loops, do this only if signals are not blocked
        if (!signalsBlocked()) {
            updateProperty(chev->propertyName(), property(chev->propertyName()));
        }

        return true;
    }

    return QObject::event(e);
}


// ---------------------- updateProperty ----------------------------
void Component::updateProperty(QString name, QVariant value) {
    CAMITK_INFO(tr("Component::updateProperty: The value of property \"%1\" has changed to: \"%2\"\n"
                   "If you want to take this change/property into account, please redefine method \"updateProperty(QString name, QVariant value)\" in class %3").arg(name, value.toString(), metaObject()->className()))
}

// ---------------------- getProperty ----------------------------
Property* Component::getProperty(QString name) {
    return propertyMap.value(name);
}

// ---------------------- addProperty ----------------------------
bool Component::addProperty(Property* prop) {

    // from QObject documentation, section "Detailed Description":
    // "To avoid never ending notification loops you can temporarily block signals with blockSignals()."
    blockSignals(true);
    // add a dynamic Qt Meta Object property with the same name
    bool returnStatus = setProperty(prop->getName().toStdString().c_str(), prop->getInitialValue());
    // add to the map
    propertyMap.insert(prop->getName(), prop);
    blockSignals(false);

    return returnStatus;
}

// -------------------- isTopLevel --------------------
bool Component::isTopLevel() const {
    return (myParentNode == nullptr);
}

// -------------------- setParent ------------------
void Component::setParent(InterfaceNode* pes) {
    if (myParentNode == pes || pes == this) {
        return;
    }

    if (myParentNode != nullptr) {
        // tell my parent I have been adopted by someone else
        myParentNode->removeChild(this);
        // avoid infinite recursion
        myParentNode = nullptr;
    }
    else {
        // remove this from the top-level list if needed
        Application::getTopLevelComponentList().removeAll(this);
    }

    myParentNode = pes;
}

// -------------------- getParentComponent --------------------
Component* Component::getParentComponent() {
    return (Component*)myParentNode;
}

// -------------------- getTopLevelComponent --------------------
Component* Component::getTopLevelComponent() {
    if (!isTopLevel()) {
        return getParentComponent()->getTopLevelComponent();
    }
    else {
        return this;
    }
}

// -------------------- setVisibility --------------------
void Component::setVisibility(Viewer* v, bool b) {
    QMap<Viewer*, bool>::iterator it = myViewers.find(v);
    if (it == myViewers.end()) {
        // insert the new viewer with the corresponding boolean
        myViewers.insert(v, b);
    }
    else {
        it.value() = b;
    }
}

// -------------------- getVisibility --------------------
bool Component::getVisibility(Viewer* v) const {
    QMap<Viewer*, bool>::const_iterator it = myViewers.constFind(v);
    if (it == myViewers.end()) {
        return false;
    }
    else {
        return it.value();
    }
}

// -------------------- refresh --------------------
void Component::refresh() const {
    for (QMap<Viewer*, bool>::const_iterator it = myViewers.begin(); it != myViewers.end(); it++) {
        if (it.value()) {
            it.key()->refresh();
        }
    }
}

// -------------------- refreshInterfaceNode --------------------
void Component::refreshInterfaceNode() {
    for (QMap<Viewer*, bool>::const_iterator it = myViewers.begin(); it != myViewers.end(); it++) {
        if (it.value()) {
            it.key()->refreshInterfaceNode(this);
        }
    }
}



// -------------------- getService --------------------
Component::Representation Component::getRepresentation() const {
    static bool checked = false; // only check once for each Component
    if (!checked) {
        checked = true;
        QString representationString;
        QString instantiatedMember;
        QString shouldInstanciateMember;
        bool instanciationError = false;

        // check the service
        switch (myService) {
            case GEOMETRY:
                representationString = "GEOMETRY";
                shouldInstanciateMember = "myGeometry";
                instanciationError = (myGeometry == nullptr);
                break;
            case SLICE:
                representationString = "SLICE";
                shouldInstanciateMember = "mySlice";
                instanciationError = (mySlice == nullptr);
                break;
            case NO_REPRESENTATION:
                instantiatedMember = (myGeometry != nullptr) ? "myGeometry" : (mySlice != nullptr) ? "mySlice" : "<<INTERNAL_ERROR>>";
                instanciationError = (mySlice != nullptr || myGeometry != nullptr);
                break;
        }
        if (instanciationError) {
            const QMetaObject* qmetaObject = metaObject();
            QString classname = qmetaObject->className();
            if (myService != NO_REPRESENTATION) {
                // the Component has to instanciate
                CAMITK_ERROR(tr("Component class \"%1\" implements service %2 but does not instanciate it!\n"
                                "\tPlease instanciate %3 in initRepresentation() or change the implemented representation in the constructor.").arg(classname, representationString, shouldInstanciateMember))
            }
            else {
                // no representation should be instantiated!
                CAMITK_ERROR(tr("Component class \"%1\" has no implemented representation (NO_REPRESENTATION) but %2 is instantiated. Please do not instantiate %3 in initRepresentation() or change the implemented service in the constructor.").arg(classname, instantiatedMember, instantiatedMember))
            }
        }
    }
    return myService;
}

// ---------------- actionLessThan ----------------
extern bool actionLessThan(const camitk::Action* a1, const camitk::Action* a2);

// -------------------- getActionMenu -----------------------
QMenu* Component::getActionMenu() {

    if (actionsMenu == nullptr) {
        actionsMenu = new QMenu();

        //-- add all actions sorted by family
        ActionList allActions = Application::getActions(this);
        QMap<QString, ActionSet*> familyMap;
        foreach (Action* action, allActions) {
            ActionSet* familySet = familyMap.value(action->getFamily().toLower());
            if (!familySet) {
                familySet = new ActionSet;
                familyMap.insert(action->getFamily().toLower(), familySet);
            }
            familySet->insert(action);
        }

        //-- create one sub menu per family (unless there is only one action)
        foreach (ActionSet* familySet, familyMap.values()) {
            // sort actions by name
            ActionList familyList = familySet->toList();
            qSort(familyList.begin(), familyList.end(), actionLessThan);
            if (familyList.size() >= 1) {
                QMenu* familyMenu = actionsMenu->addMenu(familyList.first()->getFamily());
                foreach (Action* action, familyList) {
                    familyMenu->addAction(action->getQAction());
                }
            }
            else {
                actionsMenu->addAction(familyList.first()->getQAction());
            }
        }

        // no menu if no action
        if (actionsMenu->actions().size() == 0) {
            delete actionsMenu;
            actionsMenu = nullptr;
        }
    }
    return actionsMenu;
}


// -----------------------------------------------------------------------
//
//                        InterfaceGeometry methods
//
// -----------------------------------------------------------------------


// -------------------- getRenderingModes --------------------
const InterfaceGeometry::RenderingModes Component::getRenderingModes() const {
    if (myGeometry)
        // return the Rendering mode of myGeometry, is exists
    {
        return myGeometry->getRenderingModes();
    }
    else {
        // else return the added rendering mode of all children
        if (childrenComponent.size() > 0) {
            InterfaceGeometry::RenderingModes m = InterfaceGeometry::None;
            foreach (Component* childComponent, childrenComponent) {
                m |= childComponent->getRenderingModes();
            }
            return m;
        }
    }

    // Should never comes to this extremity
    return None;
}

// -------------------- getActorColor --------------------
void Component::getActorColor(InterfaceGeometry::RenderingModes m, double d[4])  {
    if (myGeometry) {
        // get the color of myGeometry, is exists, and finished
        myGeometry->getActorColor(m, d);
    }
    else {
        // return the firts existing Color in my children
        int i = 0;
        bool found = false;
        while (i < childrenComponent.size() && !found) {
            childrenComponent[i]->getActorColor(m, d);
            found = (d[0] != 0.0 || d[1] != 0.0 || d[2] != 0.0 || d[3] != 0.0);
            i++;
        }
        if (!found) {
            for (unsigned int j = 0; j < 4; j++) {
                d[j] = 0.0;
            }
        }
    }
}

// -------------------- getBounds --------------------
void Component::getBounds(double* bounds) {
    invoke1(myGeometry, getBounds, bounds)
    else {
        bounds[0] = bounds[2] = bounds[4] = 0.0;
        bounds[1] = bounds[3] = bounds[5] = 1.0;
        // compute bounds using the children's
        foreach (Component* childComponent, childrenComponent) {
            double childBounds[6]; //xmin,xmax, ymin,ymax, zmin,zmax
            // get child bounds
            childComponent->getBounds(childBounds);
            // check compared to global bound
            for (int i = 0; i < 3; i++) {
                // compare min
                if (bounds[i * 2] > childBounds[i * 2]) {
                    bounds[i * 2] = childBounds[i * 2];
                }
                // compare max
                if (bounds[i * 2 + 1] < childBounds[i * 2 + 1]) {
                    bounds[i * 2 + 1] = childBounds[i * 2 + 1];
                }
            }
        }

    }
}

// -------------------- getBoundingRadius --------------------
double Component::getBoundingRadius() {
    if (myGeometry) {
        return myGeometry->getBoundingRadius();
    }
    else {
        // compute bounding radius using the children's
        double radius = 0.0;
        foreach (Component* childComponent, childrenComponent) {
            double childRadius = childComponent->getBoundingRadius();
            if (childRadius > radius) {
                radius = childRadius;
            }
        }
        return radius;
    }
}






// -----------------------------------------------------------------------
//
//                        InterfaceBitMap methods
//
// -----------------------------------------------------------------------


// -------------------- getNumberOfSlices --------------------
int Component::getNumberOfSlices() const {
    if (mySlice) {
        return mySlice->getNumberOfSlices();
    }
    else {
        return -1;
    }
}

// -------------------- getSlice --------------------
int Component::getSlice() const {
    if (mySlice) {
        return mySlice->getSlice();
    }
    else {
        return -1;
    }
}

// -------------------- attachChild --------------------
void Component::attachChild(InterfaceNode* childNode) {
    auto* comp = dynamic_cast<Component*>(childNode);
    if (comp != nullptr) {
        // add a sub item
        if (!childrenComponent.contains(comp)) {
            // add it to the list
            childrenComponent.append(comp);
        }
    }
}

// -------------------- addChild --------------------
void Component::addChild(InterfaceNode* childNode) {
    attachChild(childNode);
    childNode->setParent(this);
}


// -------------------- setSelected --------------------
void Component::setSelected(const bool b, const bool recursive) {
    isSelectedFlag = b;
    // maintain the children selection state as well
    if (recursive) {
        foreach (Component* child, childrenComponent) {
            child->setSelected(b, recursive);
        }
    }

    //-- Now add to global selection
    // do that only in the end, so that the last selected will be this Component (which is the one called first!)
    Application::setSelected(this, b);
}

// -------------------- getFileName --------------------
const QString Component::getFileName() const {
    return myFileName;
}

// -------------------- getFileName --------------------
void Component::setFileName(const QString& fName) {
    myFileName = fName;
}

// -------------------- removeChild --------------------
void Component::removeChild(InterfaceNode* childNode) {
    if (childrenComponent.removeAll((Component*)childNode)) {
        // remove myself from the childNode's parent item list
        childNode->setParent(nullptr);
    }
}

// -------------------- deleteChildren --------------------
void Component::deleteChildren() {
    // calling clear() on a QSet does not delete the instances
    // (the destuctor of points is not called, the Component are not deleted from the memory)
    foreach (Component* childComp, childrenComponent) {
        if (Application::isAlive(childComp)) {
            delete childComp;
        }
    }
    childrenComponent.clear();
}

// -------------------- setGlyphType --------------------
void Component::setGlyphType(const camitk::InterfaceGeometry::GlyphTypes type, const double size) {
    if (myGeometry) {
        myGeometry->setGlyphType(type, size);
    }
}

// -----------------------------------------------------------------------
//
//                        InterfaceFrame methods
//
// -----------------------------------------------------------------------

// -------------------- getFrameName --------------------
const QString& Component::getFrameName() const {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        return myName;
    }
    else {
        return myFrame->getFrameName();
    }
}

// -------------------- getParentFrame --------------------
InterfaceFrame* Component::getParentFrame() const {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        return nullptr;
    }
    else {
        return myFrame->getParentFrame();
    }
}

// -------------------- setParentFrame --------------------
void Component::setParentFrame(InterfaceFrame* frame, bool keepTransform) {
    if (myFrame == nullptr) {
        CAMITK_ERROR("delegate myFrame is not instantiated.")
    }
    else {
        myFrame->setParentFrame(frame, keepTransform);
    }
}

// -------------------- getChildrenFrame --------------------
const QVector<InterfaceFrame*>& Component::getChildrenFrame() const {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        // error / undefined behaviour
    }
    return myFrame->getChildrenFrame();
}

// -------------------- getTransformFromWorld --------------------
const vtkSmartPointer<vtkTransform> Component::getTransformFromWorld() const {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        return nullptr;
    }
    return myFrame->getTransformFromWorld();
}

// -------------------- getTransform --------------------
const vtkSmartPointer<vtkTransform> Component::getTransform() const {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        return nullptr;
    }
    return myFrame->getTransform();
}

// -------------------- getTransformFromFrame --------------------
const vtkSmartPointer<vtkTransform> Component::getTransformFromFrame(InterfaceFrame* frame) const {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        return nullptr;
    }
    return myFrame->getTransformFromFrame(frame);
}


// -------------------- getFrameAxisActor --------------------
vtkSmartPointer<vtkAxesActor> Component::getFrameAxisActor() {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        return nullptr;
    }
    else {
        return myFrame->getFrameAxisActor();
    }
}

// -------------------- getFrameVisibility --------------------
bool Component::getFrameVisibility(Viewer* viewer) const {
    if (myFrame == nullptr) {
        CAMITK_ERROR(tr("myFrame is not instantiated."))
        return false;
    }
    else {
        return myFrame->getFrameVisibility(viewer);
    }
}

}

