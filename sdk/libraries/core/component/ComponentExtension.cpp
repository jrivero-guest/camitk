/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include "ComponentExtension.h"
#include "Application.h"
#include "Log.h"

namespace camitk {

// -------------------- constructor --------------------
ComponentExtension::ComponentExtension() {
    autoload = false;
}

// -------------------- save --------------------
bool ComponentExtension::save(Component* component) const {
    QString extension = QFileInfo(component->getFileName()).completeSuffix();

    CAMITK_WARNING(tr("Extension %1 can not export the component:\n"
                      "\"%2\"\nTo the file format \".%3\"\n"
                      "This is not implemented yet.").arg(getName(), component->getName(), extension))

    return false;
}

// -------------------- initResources --------------------
void ComponentExtension::initResources() {
    // Get the selected language
    QString selectedLanguage = Application::getSelectedLanguage();

    // if a language is defined, then try to load the translation file
    if (!selectedLanguage.isEmpty()) {
        QString componentExtensionDirName = QDir(this->getLocation()).dirName();
        // remove any occurence of "-debug.dll" or ".so" or ".dylib" on the extension file name
        componentExtensionDirName.remove("-debug.dll").remove(".so").remove(".dylib");
        QString languageFile = ":/translate_" + componentExtensionDirName + "/translate/translate_" + selectedLanguage + ".qm";
        auto* translator = new QTranslator();
        if (translator->load(languageFile)) {
            QCoreApplication::installTranslator(translator);
        }
        else {
            CAMITK_INFO(tr("Cannot load file: %1").arg(languageFile))
        }
    }
}



}
