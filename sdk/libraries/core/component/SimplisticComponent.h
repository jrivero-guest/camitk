/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMPLISTICCOMPONENT_H
#define SIMPLISTICCOMPONENT_H

#include "CamiTKAPI.h"
#include "Component.h"
namespace  camitk {

/**
 * This class has been implemented to be able to instanciate a very basic component
 * with NO_REPRESENTATION.
 *
 * Caution !
 * The class Component has been created non instanciable on purpose !
 * This class SHOULD not replace a real Component inherited class handing data.
 * It is a helper class for those who need to use Interface node and/or Interface frame only.
 *
 * This is why this class should be final (no inheritance allowed) if it was writen in Java,
 * has no representation
 * (neither Slice nor Geometry) and reimplements addProperty
 *  to prevent from adding property (if you need a property, this means that you handle data,
 *  this means that you need a proper component).
 *
 */
class CAMITK_API SimplisticComponent : public Component {
    Q_OBJECT

public:
    static SimplisticComponent* createInstance(QString name);

    /// this component has no representation
    void initRepresentation() {}

    /// This simplistic component cannot have properties
    /// If you need properties, it means that you need a
    ///  particualr component to handle them
    bool addProperty(Property*) override;

private:
    SimplisticComponent(QString name);

};
}
#endif // SIMPLISTICCOMPONENT_H
