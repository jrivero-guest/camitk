/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SINGLEIMAGEVOLUMECOMPONENT_H
#define SINGLEIMAGEVOLUMECOMPONENT_H


// -- Core stuff
#include "Component.h"
#include "Slice.h"

// -- QT stuff classes
class QMenu;

// -- VTK stuff
#include <vtkImageReslice.h>
#include <vtkWindowLevelLookupTable.h>
#include <vtkImageChangeInformation.h>

// -- VTK stuff classes
class vtkImageClip;

namespace camitk {
/**
 * @ingroup group_sdk_libraries_core_component_image
 *
 * @brief
 * This Component manages a set of images, destined to be seen in a single orientation only
 *  (axial OR sagittal OR coronal).
 * It does implement Slice representation, not Geometry.
 *
 *
 */
class CAMITK_API SingleImageComponent : public camitk::Component {
    Q_OBJECT

    /// Set Axial, Coronal and Sagittal Slices visible in 3D
    Q_PROPERTY(bool viewSliceIn3D READ getViewSliceIn3D WRITE setViewSliceIn3D)

public:
    /// Constructor
    SingleImageComponent(Component* parentComponent, Slice::SliceOrientation, const QString& name, vtkSmartPointer<vtkWindowLevelLookupTable> lut);

    ~SingleImageComponent() override = default;

    /// rewritten from Component so that the Component can call the ManagerComponent
    void pixelPicked(double, double, double) override;

    /// rewritten to synchronize everyone
    void setSelected(const bool, const bool) override;

    /// rewritten to forbid any transorm from the ImageComponent
    /// (either all the ImageComponent moves or I do not move (without my other SingleImages...).
    void setTransform(vtkSmartPointer<vtkTransform>) override;
    void resetTransform() override;
    void translate(double, double, double) override;
    void rotate(double, double, double) override;
    void rotateVTK(double, double, double) override;
    void setTransformTranslation(double, double, double) override;
    void setTransformTranslationVTK(double, double, double) override;
    void setTransformRotation(double, double, double) override;
    void setTransformRotationVTK(double, double, double) override;

    /// new method used to call the Component set selected
    void singleImageSelected(const bool);

    bool getViewSliceIn3D() const;

    /** set the visibility in 3D and refresh the 3D viewers
     * @param viewSliceIn3D the boolean that tell if the visibility in 3D is on or not
     */
    void  setViewSliceIn3D(bool viewSliceIn3D);

protected:

    /** The concrete building of the Service (Slice in this case, for a 2D representation). */
    void initRepresentation() override;

protected:
    Slice::SliceOrientation sliceOrientation;

    vtkSmartPointer<vtkWindowLevelLookupTable> lut;

    /// View THIS slice in 3D Viewer
    bool viewSliceIn3D;

};

}

#endif
