/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef __itkVTKImageToImageFilter_h
#define __itkVTKImageToImageFilter_h

// -- itk stuff
#include "itkVTKImageImport.h"

// -- vtk stuff
#include "vtkImageExport.h"
#include "vtkImageData.h"
#include "vtkSmartPointer.h"

#ifndef vtkFloatingPointType
#define vtkFloatingPointType float
#endif

namespace itk {

/**
 *  @ingroup group_sdk_libraries_core_component_image
 *
 *  @brief
 *  Converts a VTK image into an ITK image and plugs a
 *  vtk data pipeline to an ITK datapipeline.
 *
 *  This class puts together an itkVTKImageImporter and a vtkImageExporter.
 *  It takes care of the details related to the connection of ITK and VTK
 *  pipelines. The User will perceive this filter as an adaptor to which
 *  a vtkImage can be plugged as input and an itk::Image is produced as
 *  output.
 *
 * \ingroup   ImageFilters
 */
template <class TOutputImage >
class ITK_EXPORT VTKImageToImageFilter : public ProcessObject {
public:
    /** Standard class typedefs. */
    typedef VTKImageToImageFilter       Self;
    typedef ProcessObject             Superclass;
    typedef SmartPointer<Self>        Pointer;
    typedef SmartPointer<const Self>  ConstPointer;

    /** Method for creation through the object factory. */
    itkNewMacro(Self);

    /** Run-time type information (and related methods). */
    itkTypeMacro(VTKImageToImageFilter, ProcessObject);

    /** Some typedefs. */
    typedef TOutputImage OutputImageType;
    typedef typename    OutputImageType::ConstPointer    OutputImagePointer;
    typedef VTKImageImport< OutputImageType >   ImporterFilterType;
    typedef typename ImporterFilterType::Pointer         ImporterFilterPointer;

    /** Get the output in the form of a vtkImage.
        This call is delegated to the internal vtkImageImporter filter  */
    const OutputImageType*   GetOutput() const;

    /** Set the input in the form of a vtkImageData */
    void SetInput(vtkSmartPointer<vtkImageData>);

    /** Return the internal VTK image exporter filter.
        This is intended to facilitate users the access
        to methods in the exporter */
    vtkSmartPointer<vtkImageExport> GetExporter() const;

    /** Return the internal ITK image importer filter.
        This is intended to facilitate users the access
        to methods in the importer */
    ImporterFilterType* GetImporter() const;

    /** This call delegate the update to the importer */
    void Update();

protected:
    VTKImageToImageFilter();
    virtual ~VTKImageToImageFilter();

private:
    VTKImageToImageFilter(const Self&); //purposely not implemented
    void operator=(const Self&); //purposely not implemented

    ImporterFilterPointer       m_Importer;
    vtkSmartPointer<vtkImageExport> m_Exporter;

};

} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkVTKImageToImageFilter.txx"
#endif

#endif



