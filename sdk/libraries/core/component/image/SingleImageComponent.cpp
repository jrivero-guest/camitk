/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core image component stuff
#include "SingleImageComponent.h"
#include "ImageComponent.h"

// -- Core stuff
#include "InteractiveViewer.h"
#include "Log.h"
#include "Application.h"
#include "Frame.h"

// -- VTK stuff
#include <vtkUnstructuredGrid.h>
#include <vtkImageClip.h>
#include <vtkImageChangeInformation.h>
#include <vtkProperty.h>

#include <cmath>

namespace camitk {
// -------------------- constructor  --------------------
SingleImageComponent::SingleImageComponent(Component* parentComponent, Slice::SliceOrientation sliceOrientation, const QString& name, vtkSmartPointer<vtkWindowLevelLookupTable> lut)
    : Component(parentComponent, name, Component::SLICE) {

    this->sliceOrientation = sliceOrientation;

    this->lut = lut;

    // set my parent image as my parent frame
    auto* myParentFrame = (Frame*) dynamic_cast<Frame*>(parentComponent->getFrame());
    this->setParentFrame(myParentFrame);

    // build the slice 3D
    initRepresentation();

    // by default, this slice is not shown in 3D
    this->viewSliceIn3D = false;

}

// -------------------- setSelected --------------------
void SingleImageComponent::setSelected(const bool b, const bool r) {
    dynamic_cast<ImageComponent*>(getParent())->setSelected(b, false);
}

// -------------------- singleImageSelected --------------------
void SingleImageComponent::singleImageSelected(const bool b) {
    Component::setSelected(b, false);
}

// -------------------- getViewSliceIn3D --------------------
bool SingleImageComponent::getViewSliceIn3D() const {
    return viewSliceIn3D;
}

// -------------------- setViewSliceIn3D --------------------
void SingleImageComponent::setViewSliceIn3D(bool toggle) {
    this->viewSliceIn3D = toggle;
    this->setVisibility(InteractiveViewer::get3DViewer(), viewSliceIn3D);
}

// -------------------- initRepresentation --------------------
void SingleImageComponent::initRepresentation() {
    mySlice = new Slice(dynamic_cast<ImageComponent*>(getParentComponent())->getImageData(), sliceOrientation, lut);
    mySlice->setImageWorldTransform(getTransformFromWorld());

    switch (sliceOrientation) {
        case Slice::AXIAL:
        case Slice::AXIAL_NEURO:
            setVisibility(InteractiveViewer::getAxialViewer(), true);
            break;
        case Slice::CORONAL:
            setVisibility(InteractiveViewer::getCoronalViewer(), true);
            break;
        case Slice::SAGITTAL:
            setVisibility(InteractiveViewer::getSagittalViewer(), true);
            break;
        case Slice::ARBITRARY:
            setVisibility(InteractiveViewer::getArbitraryViewer(), true);
            break;
    }
}

// ---------------------- pixelPicked  ----------------------------
void SingleImageComponent::pixelPicked(double i, double j, double k) {
    ((ImageComponent*)getParent())->pixelPicked(i, j, k, this);
}

/// rewritten to forbid any transorm from the ImageComponent
// -------------------- setTransform --------------------
void SingleImageComponent::setTransform(vtkSmartPointer<vtkTransform> transform) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- resetTransform --------------------
void SingleImageComponent::resetTransform() {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- rotate --------------------
void SingleImageComponent::rotate(double aroundX, double aroundY, double aroundZ) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- rotateVTK --------------------
void SingleImageComponent::rotateVTK(double aroundX, double aroundY, double aroundZ) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- setTransformRotation --------------------
void SingleImageComponent::setTransformRotation(double aroundX, double aroundY, double aroundZ) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- setTransformRotationVTK --------------------
void SingleImageComponent::setTransformRotationVTK(double aroundX, double aroundY, double aroundZ) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- setTransformTranslation --------------------
void SingleImageComponent::setTransformTranslation(double x, double y, double z) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- setTransformTranslationVTK --------------------
void SingleImageComponent::setTransformTranslationVTK(double x, double y, double z) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}

// -------------------- translate --------------------
void SingleImageComponent::translate(double x, double y, double z) {
    // Do nothing, my parent (the ImageComponent) should move !!!
}


}
