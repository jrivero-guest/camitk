/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef IMAGE_COMPONENT_EXTENSION_H
#define IMAGE_COMPONENT_EXTENSION_H


// -- Core image component stuff
#include "CamiTKAPI.h"
#include "ImageComponent.h"

// -- Core stuff
#include "ComponentExtension.h"

// -- QT stuff
#include <QObject>
#include <QMap>


namespace camitk {
/**
 * @ingroup group_sdk_libraries_core_component_image
 *
 * @brief
 * Abstract class created to support export file formats for ImageComponent
 * Any data component plugin creating an ImageComponent should inherit from this class.
 *
 */

class CAMITK_API ImageComponentExtension : public ComponentExtension {

public:

    /// get the plugin name
    QString getName() const override;

    /// get the plugin description (can be html)
    QString getDescription() const override;

    /// get a new instance from data stored in a file
    /// This method may throw an AbortException if a problem occurs.
    Component* open(const QString&) = 0;

protected:
    /// Constructor protected because the class is virtual
    ImageComponentExtension();

    /// the destructor
    ~ImageComponentExtension() = default;

};

}

#endif // IMAGE_COMPONENT_EXTENSION_H
