// Includes from Vtk
#include <vtkCellData.h>
#include <vtkSmartPointer.h>
#include <vtkPointSet.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkAbstractTransform.h>
#include <vtkAppendPolyData.h>
#include <vtkArrowSource.h>
#include <vtkMatrix4x4.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkAxesActor.h>
#include <vtkTextProperty.h>
#include <vtkProperty2D.h>
#include <vtkCaptionActor2D.h>
#include <vtkTextActor.h>

// Local Includes
#include "Frame.h"
#include "Viewer.h"

namespace camitk {

int Frame::nbTotalFrames = 0;

// ------------------------- Constructors ------------------------
Frame::Frame(vtkSmartPointer<vtkTransform> transform, Frame* parentFrame) {
    initAttributes();

    if (transform != nullptr) {
        setTransform(transform);
    }

    setParentFrame(parentFrame);
    this->parentFrame = parentFrame;
    this->transformWorldToMe->Identity();
    this->transformWorldToMe->PostMultiply();
    this->transformWorldToMe->Concatenate(transformParentToMe);
    this->transformWorldToMe->Update();

    if (this->parentFrame != nullptr) {
        this->transformWorldToMe->Concatenate(this->parentFrame->getTransformFromWorld());
        this->transformWorldToMe->Update();
        this->parentFrame->addFrameChild(this);
    }
}


// Private help mehtod
void Frame::initAttributes() {
    frameName = "Frame" + QString::number(nbTotalFrames);
    nbTotalFrames ++;

    this->axes = nullptr;
    this->parentFrame = nullptr;

    this->transformParentToMe =  vtkSmartPointer<vtkTransform>::New();
    this->transformParentToMe->Identity();

    this->transformWorldToMe = vtkSmartPointer<vtkTransform>::New();
    this->transformWorldToMe->Identity();
    this->transformWorldToMe->PostMultiply();

    this->representationTransformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
    this->representationTransformFilter->SetTransform(transformWorldToMe);
    this->representationTransformFilter->GlobalWarningDisplayOff();

    this->childrenFrame.clear();
}


// ------------------- Default Destructor ----------------------------------
Frame::~Frame() {

    if (parentFrame != nullptr) {
        parentFrame->removeFrameChild(this);
    }

    //  Declare my children as orpheans
    foreach (InterfaceFrame* f, childrenFrame) {
        f->setParentFrame(nullptr);
    }

    this->childrenFrame.clear();
    this->frameViewers.clear();

}

// -----------------* Hierarchy accessors / Modifyers *-----------------
// ------------------- getFrameName ----------------------------------
const QString& Frame::getFrameName() const {
    return frameName;
}

// ------------------- setFrameName ----------------------------------
void Frame::setFrameName(QString name) {
    this->frameName = name;
}

// ------------------- getParentFrame ----------------------------------
InterfaceFrame* Frame::getParentFrame() const {
    return parentFrame;
}

// ------------------- getChildrenFrame ----------------------------------
const QVector<InterfaceFrame*>& Frame::getChildrenFrame() const {
    return childrenFrame;
}

// ------------------- setParentFrame ----------------------------------
void Frame::setParentFrame(InterfaceFrame* parent, bool keepTransform) {
    InterfaceFrame* checkedParent = parent;

    if (checkedParent) {
        // setParentFrame(this) should not be possible...
        // Neither should be setParentFrame from one of my child
        if (checkedParent->getFrameName() == getFrameName()) {
            checkedParent = nullptr;
        }
        else {
            // compute all the descendants of the current frame
            QVector<InterfaceFrame*> descendants = computeDescendants(this);
            // check if one of the descendant if the checked parent
            foreach (InterfaceFrame* descendant, descendants) {
                if (descendant->getFrameName() == checkedParent->getFrameName()) {
                    //remove the new parent in the list of children
                    removeFrameChild(checkedParent);
                    //In this usecase, the former descendant is attached to the world frame
                    checkedParent->setParentFrame(nullptr);
                    break;
                }
            }
        }
    }

    // If we don't keep transform => update it in order for the frame not to move during parent transition
    if (!keepTransform) {
        vtkSmartPointer<vtkTransform> newParentTransform = nullptr;
        newParentTransform = getTransformFromFrame(checkedParent);
        setTransform(newParentTransform);
    }

    // tell my former parent that I am no more its child
    if (parentFrame != nullptr) {
        parentFrame->removeFrameChild(this);
    }

    this->parentFrame = checkedParent;
    this->transformWorldToMe->Identity();
    this->transformWorldToMe->PostMultiply();
    this->transformWorldToMe->Concatenate(transformParentToMe);
    this->transformWorldToMe->Update();

    if (this->parentFrame != nullptr) {
        this->transformWorldToMe->Concatenate(this->parentFrame->getTransformFromWorld());
        this->transformWorldToMe->Update();
        this->parentFrame->addFrameChild(this);
    }

}

// ------------------- addFrameChild ----------------------------------
void Frame::addFrameChild(InterfaceFrame* frame) {
    if (!childrenFrame.contains(frame)) {
        childrenFrame.append(frame);
    }
}

void Frame::removeFrameChild(InterfaceFrame* frame) {
    if (childrenFrame.indexOf(frame) != -1) {
        childrenFrame.remove(childrenFrame.indexOf(frame));
    }
}
// -----------------* Transforms accessors / Modifyers *-----------------
// ------------------- getTransformFromWorld ----------------------------------
const vtkSmartPointer<vtkTransform> Frame::getTransformFromWorld() const {
    return transformWorldToMe;
}

// ------------------- getTransform ----------------------------------
const vtkSmartPointer<vtkTransform> Frame::getTransform() const {
    return transformParentToMe;
}

// ------------------- getTransformFromFrame ----------------------------------
const vtkSmartPointer<vtkTransform> Frame::getTransformFromFrame(InterfaceFrame* frame) const {
    // World -> Me
    vtkSmartPointer<vtkMatrix4x4> worldToMeMatrix = transformWorldToMe->GetMatrix();

    // World -> Frame
    vtkSmartPointer<vtkMatrix4x4> worldToFrameMatrix;
    if (frame == nullptr) { // Frame IS the world Frame !
        worldToFrameMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
        worldToFrameMatrix->Identity();
    }
    else {
        worldToFrameMatrix = frame->getTransformFromWorld()->GetMatrix();
    }

    // Frame -> World
    vtkSmartPointer<vtkMatrix4x4> frameToWorldMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
    vtkMatrix4x4::Invert(worldToFrameMatrix, frameToWorldMatrix);

    // Frame -> Me = Frame -> World -> Me
    vtkSmartPointer<vtkMatrix4x4> frameToMeMatrix = vtkSmartPointer<vtkMatrix4x4>::New();
    vtkMatrix4x4::Multiply4x4(frameToWorldMatrix, worldToMeMatrix, frameToMeMatrix);

    vtkSmartPointer<vtkTransform> result = vtkSmartPointer<vtkTransform>::New();
    result->SetMatrix(frameToMeMatrix);

    return result;
}

// ------------------- setTransform ----------------------------------
void Frame::setTransform(vtkSmartPointer<vtkTransform> transform) {
    transformParentToMe->SetMatrix(transform->GetMatrix());
    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}

// ------------------- resetTransform ----------------------------------
void Frame::resetTransform() {
    transformParentToMe->Identity();
    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}

// ------------------- translate ----------------------------------
void Frame::translate(double x, double y, double z) {
    transformParentToMe->Translate(x, y, z);
    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}


// ------------------- rotate ----------------------------------
void Frame::rotate(double aroundX, double aroundY, double aroundZ) {
    double* initialPos = transformParentToMe->GetPosition();

    transformParentToMe->Translate(-initialPos[0], -initialPos[1], -initialPos[2]);

    transformParentToMe->RotateX(aroundX);
    transformParentToMe->RotateY(aroundY);
    transformParentToMe->RotateZ(aroundZ);
    transformParentToMe->Update();

    transformParentToMe->Translate(initialPos[0], initialPos[1], initialPos[2]);

    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}

// ------------------- rotateVTK ----------------------------------
void Frame::rotateVTK(double aroundX, double aroundY, double aroundZ) {
    double* initialPos = transformParentToMe->GetPosition();

    transformParentToMe->Translate(-initialPos[0], -initialPos[1], -initialPos[2]);

    transformParentToMe->RotateZ(aroundZ);
    transformParentToMe->RotateX(aroundX);
    transformParentToMe->RotateY(aroundY);
    transformParentToMe->Update();

    transformParentToMe->Translate(initialPos[0], initialPos[1], initialPos[2]);

    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}

// ------------------- setTransformTranslation --------------------------
void Frame::setTransformTranslation(double x, double y, double z) {
    double* initialRotation = transformParentToMe->GetOrientation();

    transformParentToMe->Identity();
    transformParentToMe->RotateX(initialRotation[0]);
    transformParentToMe->RotateY(initialRotation[1]);
    transformParentToMe->RotateZ(initialRotation[2]);
    transformParentToMe->Update();
    transformParentToMe->Translate(x, y, z);

    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}

// ------------------- setTransformTranslationVTK --------------------------
void Frame::setTransformTranslationVTK(double x, double y, double z) {
    double* initialRotation = transformParentToMe->GetOrientation();

    transformParentToMe->Identity();
    // VTK rotation order is Z, X, Y
    transformParentToMe->RotateZ(initialRotation[2]);
    transformParentToMe->RotateX(initialRotation[0]);
    transformParentToMe->RotateY(initialRotation[1]);
    transformParentToMe->Update();
    transformParentToMe->Translate(x, y, z);
    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}

// ------------------- setTransformRotation ------------------------------
void Frame::setTransformRotation(double aroundX, double aroundY, double aroundZ) {
    double* initialTranslation = transformParentToMe->GetPosition();

    transformParentToMe->Identity();
    transformParentToMe->RotateX(aroundX);
    transformParentToMe->RotateY(aroundY);
    transformParentToMe->RotateZ(aroundZ);
    transformParentToMe->Update();
    transformParentToMe->Translate(initialTranslation[0], initialTranslation[1], initialTranslation[2]);

    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}

// ------------------- setTransformRotationVTK ------------------------------
void Frame::setTransformRotationVTK(double aroundX, double aroundY, double aroundZ) {
    double* initialTranslation = transformParentToMe->GetPosition();

    transformParentToMe->Identity();
    // VTK rotation order is Z, X, Y
    transformParentToMe->RotateZ(aroundZ);
    transformParentToMe->RotateX(aroundX);
    transformParentToMe->RotateY(aroundY);
    transformParentToMe->Update();
    transformParentToMe->Translate(initialTranslation[0], initialTranslation[1], initialTranslation[2]);
    transformParentToMe->Update();
    transformWorldToMe->Update();
    representationTransformFilter->Update();
}



// ------------------- getFrameActor ----------------------------------
vtkSmartPointer<vtkAxesActor> Frame::getFrameAxisActor() {

    if (axes == nullptr) {
        axes = vtkSmartPointer<vtkAxesActor>::New();
        axes->SetShaftTypeToCylinder();
        axes->SetXAxisLabelText("x");
        axes->SetYAxisLabelText("y");
        axes->SetZAxisLabelText("z");
        axes->SetTotalLength(1.0, 1.0, 1.0); // unit length
        vtkSmartPointer<vtkTextProperty> axeXTextProp = vtkSmartPointer<vtkTextProperty>::New();
        axeXTextProp->SetFontSize(10);
        axeXTextProp->BoldOn();
        axeXTextProp->ItalicOn();
        axeXTextProp->ShadowOff();
        axeXTextProp->SetFontFamilyToArial();
        axes->GetXAxisCaptionActor2D()->SetCaptionTextProperty(axeXTextProp);
        // remove the autoscaling so that the font size is smaller than default autoscale
        axes->GetXAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
        // set the color
        axes->GetXAxisCaptionActor2D()->GetCaptionTextProperty()->SetColor(0.9, 0.4, 0.4);
        // make sure the label can be hidden by any geometry, like the axes
        axes->GetXAxisCaptionActor2D()->GetProperty()->SetDisplayLocationToBackground();

        vtkSmartPointer<vtkTextProperty> axeYTextProp = vtkSmartPointer<vtkTextProperty>::New();
        axeYTextProp->ShallowCopy(axeXTextProp);
        axes->GetYAxisCaptionActor2D()->SetCaptionTextProperty(axeYTextProp);
        axes->GetYAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
        axes->GetYAxisCaptionActor2D()->GetCaptionTextProperty()->SetColor(0.4, 0.9, 0.4);
        axes->GetYAxisCaptionActor2D()->GetProperty()->SetDisplayLocationToBackground();

        vtkSmartPointer<vtkTextProperty> axeZTextProp = vtkSmartPointer<vtkTextProperty>::New();
        axeZTextProp->ShallowCopy(axeXTextProp);
        axes->GetZAxisCaptionActor2D()->SetCaptionTextProperty(axeZTextProp);
        axes->GetZAxisCaptionActor2D()->GetTextActor()->SetTextScaleModeToNone();
        axes->GetZAxisCaptionActor2D()->GetCaptionTextProperty()->SetColor(0.4, 0.4, 0.9);
        axes->GetZAxisCaptionActor2D()->GetProperty()->SetDisplayLocationToBackground();

        axes->SetUserTransform(transformWorldToMe);

    }

    return axes;
}

// ------------------- setFrameVisibility ----------------------------------
void Frame::setFrameVisibility(Viewer* viewer, bool visible) {
    QMap<Viewer*, bool>::iterator it = this->frameViewers.find(viewer);

    if (it == frameViewers.end()) {
        frameViewers.insert(viewer, visible);
    }
    else {
        it.value() = visible;
    }

}

// ------------------- setFrameVisibility ----------------------------------
bool Frame::getFrameVisibility(Viewer* viewer) const {
    QMap<Viewer*, bool>::const_iterator it = this->frameViewers.find(viewer);

    if (it == frameViewers.end()) {
        return false;
    }
    else {
        return it.value();
    }
}

// ------------------- computeDescendants -------------------------------
QVector<InterfaceFrame*> Frame::computeDescendants(InterfaceFrame* frame) {

    QVector<InterfaceFrame*> descendants;

    // recursively call to get all the descendants of 'frame'
    foreach (InterfaceFrame* child, frame->getChildrenFrame()) {
        descendants.append(child);
        descendants += computeDescendants(child);
    }

    return descendants;
}

} // namespace
