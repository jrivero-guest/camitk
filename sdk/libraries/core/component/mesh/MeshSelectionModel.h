/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MESH_SELECTION_MODEL_H
#define MESH_SELECTION_MODEL_H

#include <QAbstractTableModel>

#include<vtkSmartPointer.h>

class vtkSelectionNode;
class vtkAbstractArray;

namespace camitk {

class MeshComponent;

/**
 * Qt model for mesh selection
 * This class use the Qt model/view design.
 */
class MeshSelectionModel : public QAbstractTableModel {

    Q_OBJECT

public :

    enum InsertionPolicy {
        REPLACE,
        MERGE,
        SUBSTRACT,
        DISCARD
    };

    /**
     * @brief Constructor
     */
    MeshSelectionModel(MeshComponent* const meshComp);

    /**
     * @brief Number of selection
     */
    int rowCount(const QModelIndex& parent = QModelIndex()) const ;

    /**
     * @brief Number of selection columns
     */
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    /**
     * @brief Model data
     */
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    /**
     * @brief Data header
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    /**
     * @brief Insert a selection.
     */
    int insertSelection(const QString& name, int fieldType, int contentType,  vtkSmartPointer< vtkAbstractArray > array, InsertionPolicy policy = REPLACE);

    int removeSelection(const QString& name);

    /**
     * @brief Edit selection data
     */
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

//     /**
//      * @brief Get the selection list
//      */
//     QMap< QString, vtkSmartPointer<vtkSelectionNode> >& getSelectionList();
//
//     /**
//      * @brief Update the selection
//      */
//     void updateSelection(const QString& name);
//
//     /**
//      * @brief Refresh the model
//      */
//     void refresh();

private :

    MeshComponent* meshComponent; /// The component where the selection are stored
    QMap< int, QString > fieldName;
    QMap< int, QString > contentName;

};

}

#endif
