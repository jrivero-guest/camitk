/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MESH_DATA_MODEL_H
#define MESH_DATA_MODEL_H

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>

#include "MeshComponent.h"

namespace camitk {

/**
 * @ingroup group_sdk_libraries_core_component_mesh
 *
 * @brief
 * Qt model for mesh data.
 * This class use the Qt model/view design.
 *
 * CamiTK intern class to represent mesh data.
 */
class MeshDataModel : public QAbstractTableModel {

    Q_OBJECT

public :

    /**
     * @brief Constructor
     */
    MeshDataModel(MeshComponent* meshComp);

    /**
     * @brief Number of data arrays
     */
    int rowCount(const QModelIndex& parent = QModelIndex()) const ;

    /**
     * @brief Number of data arrays columns
     */
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    /**
     * @brief Model data, called when the view is refreshing visualization
     */
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    /**
     * @brief Header data
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    /**
     * @brief Edit data, called when the data are modified by the user (view)
     */
    bool setData(const QModelIndex& index, const QVariant& value, int role) override;

    /// Returns if a given model index is editable, checkable....
    /// @see QAbstractTableModel
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    /**
     * @brief Refresh the model
     */
    void refresh();

private :

    /// The component where the data are stored
    MeshComponent* meshComponent;

    /// determine the current field type of the item depending on the row index, compute the index of the data in its field category
    /// and retrieve the type of data and the name of the array.
    void getRowInfo(const int, int* dataIndex, MeshComponent::FieldType*, MeshComponent::DataType*, QString&) const;

};

// -----------------------------------------------------

/**
 *  CamiTK intern class to help automatically sort or show specific data.
 *
 *  For example: this is used in the InteractiveViewer to show a combobox of all the point and cell scalar data.
 */
class MeshDataFilterModel : public QSortFilterProxyModel {

    Q_OBJECT

public :

    MeshDataFilterModel(int fieldFilter = MeshComponent::POINTS | MeshComponent::CELLS | MeshComponent::MESH,
                        int dataFilter = MeshComponent::SCALARS | MeshComponent::VECTORS | MeshComponent::TENSORS | MeshComponent::OTHERS,
                        QObject* parent = nullptr);

    void setFieldTypeFilter(int fieldFilter);

    void setDataTypeFilter(int dataFilter);

protected :

    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;

private :

    int fieldTypeFilter;

    int dataTypeFilter;

};

}

#endif
