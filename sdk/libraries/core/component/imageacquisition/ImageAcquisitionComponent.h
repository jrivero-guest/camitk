/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef IMAGEACQUISITION_COMPONENT_H
#define IMAGEACQUISITION_COMPONENT_H

// -- CamiTK Core stuff
#include <Component.h>
#include <ImageComponent.h>

namespace camitk {
/**
  * @brief
  * This class describes what is a generic Image Acquisition Component  which derives from Component.
  * To support a new acquisition device, you need to write a new ImageAcquisitionComponent subclass and
  * add it to your CEP, write a new class that inherits from this class.
  *
  * In order to inherits from this class, the followed methods have to be redefined:
  * -initImager
  * -startImaging2D
  * -stopImaging2D
  * -startImaging3D
  * -stopImaging3D
  * -singleAcquisition2D
  * -singleAcquisition3D
  * -setImageComponent2D();
  * -getImageComponent2D();
  * -setImageComponent3D();
  * -getImageComponent3D();
  * -isImaging2D();
  * -isImaging3D();
  */

class CAMITK_API ImageAcquisitionComponent : public Component {

    Q_OBJECT

public:

    /// constructor
    ImageAcquisitionComponent(Component* component, const QString& name);

    /// constructor
    ImageAcquisitionComponent(const QString& file, const QString& name);

    /// destructor
    ~ImageAcquisitionComponent() override = default;

    /// pure virtual method to define a way to initialize your acquisition device
    virtual void initImager() = 0;

    /// virtual method to define a way to start the 2D acquisition done with the device (i.e : running a acquisition in live mode...)
    virtual void startImaging2D();

    /// virtual method to define a way to stop the 2D continuous acquisition performed by your device
    virtual void stopImaging2D();

    /// virtual method to define a way to start the 3D continuous acquisition performed by your device
    virtual void startImaging3D();

    /// virtual method to define a way to stop the 3D continuous acquisition performed by your device
    virtual void stopImaging3D();

    /// virtual method to define a way to acquire one 2D ImageComponent by your device
    virtual void singleAcquisition2D();

    /// virtual method to define a way to acquire one 3D ImageComponent by your device
    virtual void singleAcquisition3D();

    /** Method to set the 2D ImageComponent used by ImageAcquisitionComponent
    * param input the 2D ImageComponent refreshed by ImageAcquisitionComponent
    */
    void setImageComponent2D(ImageComponent* input);

    /// Method to get the 2D ImageComponent used by ImageAcquisitionComponent
    ImageComponent* getImageComponent2D();

    /** Method to set the 3D ImageComponent used by ImageAcquisitionComponent
    * param input the 3D ImageComponent refreshed by ImageAcquisitionComponent
    */
    void setImageComponent3D(ImageComponent* input);

    /// Method to get the 3D ImageComponent used by ImageAcquisitionComponent
    ImageComponent* getImageComponent3D();

    /// Method to know if ImageAcquisitionComponent is currently imaging in 2D
    bool isImaging2D();

    /// Method to know if ImageAcquisitionComponent is currently imaging in 3D
    bool isImaging3D();

protected:

    /// A pointer to the working 2D ImageComponent, either create by user or automatically created by ImageAcquisitionComponent
    ImageComponent* workingImageComponent2D;

    /// A pointer to the working 3D ImageComponent, either create by user or automatically created by ImageAcquisitionComponent
    ImageComponent* workingImageComponent3D;

    /// A boolean to know is the ImageAcquisitionComponent is imaging in 2D
    bool imaging2D;

    /// A boolean to know is the ImageAcquisitionComponent is imaging in 3D
    bool imaging3D;

};

}
#endif //IMAGEACQUISITION_COMPONENT_EXTENSION_H
