/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/


//-- CamiTK Stuff
#include "ImageAcquisitionComponentExtension.h"
#include <ExtensionManager.h>
#include "ImageAcquisitionComponent.h"


namespace camitk {

//---------------------------constructor-------------------------------
ImageAcquisitionComponentExtension::ImageAcquisitionComponentExtension(): ComponentExtension() {
}

//------------------------------------getImagers----------------------------------------
QStringList ImageAcquisitionComponentExtension::getImagerList() {
    //-- get componentextension list
    QList<ComponentExtension*> compExt = ExtensionManager::getComponentExtensionsList();
    QStringList listImageAcqui;
    foreach (ComponentExtension* ce, compExt) {
        if (dynamic_cast<ImageAcquisitionComponentExtension*>(ce)) {
            // a new ImageAcquisitionComponentExtension:: we add to the list
            auto* im = dynamic_cast<ImageAcquisitionComponentExtension*>(ce);
            listImageAcqui.append(im->getName());
        }
    }
    return listImageAcqui;
}

//------------------------------------getInstance----------------------------------------
ImageAcquisitionComponent* ImageAcquisitionComponentExtension::getInstance(QString imagerName, Component* parent, QString name) {
    ImageAcquisitionComponent* instance = nullptr;

    // -- look for the component extension that has a name equals to imagerName
    QList<ComponentExtension*> compExtList = ExtensionManager::getComponentExtensionsList();
    int i = 0;
    while (i < compExtList.size() && compExtList.at(i)->getName() != imagerName) {
        i++;
    }

    //-- if found ask the component extension for a new instance
    if (i < compExtList.size()) {
        auto* compExt = dynamic_cast<ImageAcquisitionComponentExtension*>(compExtList.at(i));
        if (compExt) {
            instance = compExt->getInstance(parent, name);
        }
    }

    return instance;
}
}
