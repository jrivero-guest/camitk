/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CAMITK_COMPONENT_H
#define CAMITK_COMPONENT_H

// -- Core stuff
#include "InterfaceNode.h"
#include "InterfaceGeometry.h"
#include "InterfaceBitMap.h"
#include "InterfaceProperty.h"
#include "InterfaceFrame.h"
#include "AbortException.h"

// -- QT stuff
#include <QPixmap>
#include <QMenu>
#include <QVector>

// -- vtk stuff
#include <vtkWindowLevelLookupTable.h>
#include <vtkImageData.h>
#include <vtkPointSet.h>
#include <vtkSmartPointer.h>
#include <vtkAlgorithmOutput.h>
#include <vtkActor.h>
#include <vtkAxesActor.h>
#include <vtkActor2D.h>
#include <vtkImageActor.h>
#include <vtkTransform.h>

// -- vtk stuff Classes
class vtkActor;
class vtkTexture;
class vtkPointSet;
class vtkUnstructuredGridAlgorithm;
class vtkDataSetToUnstructuredGridFilter;
class vtkWindowLevelLookupTable;

// -----------------------------------------------------------------------
//
//                           Delegation macros
//                     (And your dream comes true)
//
// -----------------------------------------------------------------------

/** invoke macros:
  * Check the HANDLER pointer and if non-null call its METHOD,
  * eventually using PARAMeters
  */
#define invoke0(HANDLER,METHOD) \
if (HANDLER) \
    HANDLER->METHOD();

#define invoke1(HANDLER,METHOD,PARAM) \
if (HANDLER) \
    HANDLER->METHOD(PARAM);

#define invoke2(HANDLER,METHOD,PARAM1,PARAM2) \
if (HANDLER) \
    HANDLER->METHOD(PARAM1,PARAM2);

#define invoke3(HANDLER,METHOD,PARAM1,PARAM2,PARAM3) \
if (HANDLER) \
    HANDLER->METHOD(PARAM1,PARAM2,PARAM3);

#define invoke4(HANDLER,METHOD,PARAM1,PARAM2,PARAM3,PARAM4) \
if (HANDLER) \
    HANDLER->METHOD(PARAM1,PARAM2,PARAM3,PARAM4);

/** invokeGet macros:
  * Check the HANDLER pointer and if non-null call its METHOD,
  * eventually using PARAMeters
  */
#define invokeGet0(HANDLER,METHOD) \
if (HANDLER) \
    return HANDLER->METHOD();

#define invokeGet1(HANDLER,METHOD,PARAM) \
if (HANDLER) \
    return HANDLER->METHOD(PARAM);

#define invokeGet2(HANDLER,METHOD,PARAM1,PARAM2) \
if (HANDLER) \
    return HANDLER->METHOD(PARAM1,PARAM2);

#define invokeGet3(HANDLER,METHOD,PARAM1,PARAM2,PARAM3) \
if (HANDLER) \
    return HANDLER->METHOD(PARAM1,PARAM2,PARAM3);

#define invokeGet4(HANDLER,METHOD,PARAM1,PARAM2,PARAM3,PARAM4) \
if (HANDLER) \
    return HANDLER->METHOD(PARAM1,PARAM2,PARAM3,PARAM4);

/** invokeChildren macros:
  * Call a given METHOD eventually with PARAM for all childrenComponent
  */
#define invokeChildren0(METHOD) \
foreach (Component *child, childrenComponent) { \
                                                child->METHOD(); \
                                              }

#define invokeChildren1(METHOD,PARAM) \
foreach (Component *child, childrenComponent) { \
                                                child->METHOD(PARAM); \
                                              }

#define invokeChildren2(METHOD,PARAM1,PARAM2) \
foreach (Component *child, childrenComponent) { \
                                                child->METHOD(PARAM1,PARAM2); \
                                              }

#define invokeChildren3(METHOD,PARAM1,PARAM2,PARAM3) \
foreach (Component *child, childrenComponent) { \
                                                child->METHOD(PARAM1,PARAM2,PARAM3); \
                                              }

#define invokeChildren4(METHOD,PARAM1,PARAM2,PARAM3,PARAM4) \
foreach (Component *child, childrenComponent) { \
                                                child->METHOD(PARAM1,PARAM2,PARAM3,PARAM4); \
                                              }

/** delegate macros:
  * completely delegates METHOD to HANDLER, eventually using parameters of given PARAM_TYPE.
  * As these macros call the corresponding invoke macros, the non-nullity of HANDLER
  * is always checked before actually calling METHOD.
  */
#define delegate0(HANDLER,METHOD) \
virtual void METHOD() { \
                        invoke0(HANDLER,METHOD) \
                      }

#define delegate1(HANDLER,METHOD,PARAM_TYPE) \
virtual void METHOD(PARAM_TYPE param) { \
                                        invoke1(HANDLER,METHOD,param) \
                                      }

#define delegate2(HANDLER,METHOD,PARAM_TYPE1,PARAM_TYPE2) \
virtual void METHOD(PARAM_TYPE1 param1, PARAM_TYPE2 param2) { \
                                                              invoke2(HANDLER,METHOD,param1,param2) \
                                                            }

#define delegate3(HANDLER,METHOD,PARAM_TYPE1,PARAM_TYPE2,PARAM_TYPE3) \
virtual void METHOD(PARAM_TYPE1 param1, PARAM_TYPE2 param2, PARAM_TYPE3 param3) { \
                                                                                  invoke3(HANDLER,METHOD,param1,param2,param3) \
                                                                                }

#define delegate4(HANDLER,METHOD,PARAM_TYPE1,PARAM_TYPE2,PARAM_TYPE3, PARAM_TYPE4) \
virtual void METHOD(PARAM_TYPE1 param1, PARAM_TYPE2 param2, PARAM_TYPE3 param3, PARAM_TYPE4 param4) { \
                                                                                                      invoke4(HANDLER,METHOD,param1,param2,param3,param4) \
                                                                                                    }

/** delegateGet macros:
  * Same as delegate macro but for an accessor non-const METHOD, returns a value of type TYPE given by HANDLER;
  * eventually as a PARAM_TYPE parameter.
  * if HANDLER is NULL, return 0.
  * (which should automatically be converted to false for bool, NULL for pointers...)
  */
#define delegateGet0(HANDLER,METHOD,TYPE) \
virtual TYPE METHOD() { \
                        invokeGet0(HANDLER,METHOD) \
                        else \
                        return 0; \
                      }

#define delegateGet1(HANDLER,METHOD,TYPE,PARAM_TYPE) \
virtual TYPE METHOD(PARAM_TYPE param) { \
                                        invokeGet1(HANDLER,METHOD,param) \
                                        else \
                                        return 0; \
                                      }

#define delegateGet2(HANDLER,METHOD,TYPE,PARAM1_TYPE,PARAM2_TYPE) \
virtual TYPE METHOD(PARAM1_TYPE param1, PARAM2_TYPE param2) { \
                                        invokeGet2(HANDLER,METHOD,param1,param2) \
                                        else \
                                        return 0; \
                                      }
/** delegateConstGet macros:
  * Same as delegateGet but for const METHOD
  */
#define delegateConstGet0(HANDLER,METHOD,TYPE) \
virtual TYPE METHOD() const { \
                              invokeGet0(HANDLER,METHOD) \
                              else \
                              return 0; \
                            }

#define delegateConstGet1(HANDLER,METHOD,TYPE,PARAM_TYPE) \
virtual TYPE METHOD(PARAM_TYPE param) const { \
                                              invokeGet1(HANDLER,METHOD,param) \
                                              else \
                                              return 0; \
                                            }

/** delegateAndInvokeChildren macros:
  * Same as delegate but also calls METHOD, eventually with PARAM_TYPE, for all the childrenComponent.
  * First uses the corresponding invoke macro, then the corresponding invokeChildren macro.
  */
#define delegateAndInvokeChildren1(HANDLER,METHOD,PARAM_TYPE) \
virtual void METHOD(PARAM_TYPE param) { \
                                        invoke1(HANDLER,METHOD,param) \
                                        invokeChildren1(METHOD,param) \
                                      }

#define delegateAndInvokeChildren2(HANDLER,METHOD,PARAM_TYPE1,PARAM_TYPE2) \
virtual void METHOD(PARAM_TYPE1 param1, PARAM_TYPE2 param2) { \
                                                              invoke2(HANDLER,METHOD,param1,param2) \
                                                              invokeChildren2(METHOD,param1,param2) \
                                                            }

#define delegateAndInvokeChildren1Array(HANDLER,METHOD,PARAM_TYPE1,PARAM_TYPE2,DIM) \
virtual void METHOD(PARAM_TYPE1 param1, PARAM_TYPE2 param2[DIM]) { \
                                                                   invoke2(HANDLER,METHOD,param1,param2) \
                                                                   invokeChildren2(METHOD,param1,param2) \
                                                                 }

#define delegateAndInvokeChildren3(HANDLER,METHOD,PARAM_TYPE1,PARAM_TYPE2,PARAM_TYPE3) \
virtual void METHOD(PARAM_TYPE1 param1, PARAM_TYPE2 param2, PARAM_TYPE3 param3) { \
                                                                                  invoke3(HANDLER,METHOD,param1,param2,param3) \
                                                                                  invokeChildren3(METHOD,param1,param2,param3) \
                                                                                }

#define delegateAndInvokeChildren4(HANDLER,METHOD,PARAM_TYPE1,PARAM_TYPE2,PARAM_TYPE3,PARAM_TYPE4) \
virtual void METHOD(PARAM_TYPE1 param1, PARAM_TYPE2 param2, PARAM_TYPE3 param3,PARAM_TYPE4 param4) { \
                                                                                                     invoke4(HANDLER,METHOD,param1,param2,param3,param4) \
                                                                                                     invokeChildren4(METHOD,param1,param2,param3,param4) \
                                                                                                   }


namespace camitk {
// -- Core stuff classes
class Geometry;
class Slice;
class Viewer;
class Frame;


/**
*
* @ingroup group_sdk_libraries_core_component
*
* @brief
* A Component represents something that
* could be included in the explorer view, the interactive 3D viewer,
* and that could have or not a contextual
* popup menu (open by a right click in the explorer),
* a property dialog (to change some properties)
* Thus, a Component inherits from many abstract classes.
* A Component can only have one implemented representation.
*
* For CAMITK core developers:
* This class uses the Object Adapter Design Pattern (aka delegate pattern)
* to delegates all InterfaceGeometry and InterfaceBitMap to respectively myGeometry:Geometry and mySlice:InterfaceBitMap
* It handles the InterfaceNode without delegation.
* Considering this Design Pattern, Component is the Adaptor and Geometry and InterfaceBitMap are the Adaptee classes.
*
* This class has some static member to manage all the currently instantiated Components
* as well as the currently selected Components.
*
* Actions generally use setPointSet() (for InterfaceGeometry) and setOriginalVolume (for InterfaceBitMap) to do
* some data processing and directly modify the low-level Vtk data.
* It is thus very <b>important</b> to rewrite these methods in your Component subclass to takes the actions' modification
* into account in your low-level data.
*
* Dynamic properties: if your Component defines some dynamic property, you might want to updateProperty() in order
* to update the internal state of your object when a dynamic property has been changed.
* @see ObjComponent for a good example
*
* It is extensively using Qt Meta-Object system (concepts and implementation).
* see http://doc.qt.nokia.com/latest/metaobjects.html
*
*
*
*/
class CAMITK_API Component : public InterfaceProperty, public InterfaceNode, public InterfaceGeometry, public InterfaceBitMap, public InterfaceFrame {
    Q_OBJECT

public:
    /** \enum Representation The different representation that can be implemented to represent this Component in the InteractiveViewer.
      * use getRepresentation() to get the information about a specific Component.
      * \note the representation cannot be NULL; if a Component
      * does not have any representation, then getRepresentation() should return NO_REPRESENTATION (default).
      */
    enum Representation {
        GEOMETRY, ///< this Component can be displayed as a GEOMETRY
        SLICE, ///< this Component can be displayed as a SLICE
        NO_REPRESENTATION      ///< this Component has no representation implemented
    };

    /** @name Component top level methods
    * All the methods specific to a data component (but not described in any abstract representation classes)
    */
    ///@{

    /** Component constructor for top-level component (please use the other constructor for sub-level components).
     *  parentComponent is set to NULL (=> isTopLevel() will return true).
     *  @param file the file to get the data from
     *  @param name the Component name
     *  @param rep the representation concretely implemented by this Component (default=NO_REPRESENTATION)
     */
    Component(QString file, QString name, Representation rep = NO_REPRESENTATION);

    /** Component constructor for a Component that is a child of another Component
     *  You should not use this constructor for a top-level component.
     *  This method may throw an AbortException if a problem occurs.
     *
     *  @param parentComponent the parent Component
     *  @param name the Component name
     *  @param rep the representation implemented by this Component (default=NO_REPRESENTATION)
     *  @throws AbortException if parentComponent is nullptr.
     */
    Component(Component* parentComponent, const QString& name, Representation rep = NO_REPRESENTATION);

    /** default destructor.
     *  The Component class destructor is automatically called after the inherited destructor has finished (C++ standard).
     *  This destructor delete all the children, clear all this component's viewer list, delete all helper class instance
     *  (Geometry, Slice or Frame), which in turns will delete the VTK pipeline and any additional prop,
     *  and finally delete and all additional CamiTK Properties.
     *  @see deleteChildren()
     */
    ~Component() override;

    /** return the type of representation concretely implemented by this Component in the InteractiveViewer.
      * \note if a Component does not have any representation, then getRepresentation() returns NO_REPRESENTATION (default).
      */
    Representation getRepresentation() const;

    /// return true if this component is a top-level component
    bool isTopLevel() const;

    /// get the parent component
    virtual Component* getParentComponent();

    /// get the top-level component
    virtual Component* getTopLevelComponent();

    /// get the associated frame
    virtual InterfaceFrame* getFrame();

    /// set the modified flag
    virtual void setModified(bool modified = true);

    /// set the modified flag
    virtual bool getModified() const;

    /// set the visibility for a viewer
    virtual void setVisibility(Viewer*, bool);

    /// get the visibility of a viewer
    virtual bool getVisibility(Viewer*) const;

    /// refresh all the viewer that are currently displaying this Component
    virtual void refresh() const;

    /** Asks all viewers that are currently displaying this Component to
     *  rebuild the representation of the InterfaceNode for this Component.
     *  It calls the method update(Component *) of the class Viewer.
     *  This method does nothing except for the Explorer where it does not only refresh,
     *  but remove this and re-load the Component.
     */
    virtual void refreshInterfaceNode();

    /// Check if this data component is selected
    virtual bool isSelected() const;

    /** Update the selection flag.
     * @param b the value of the flag (true means "is selected")
     * @param recursive if true (default), also updates the children Component selection flags.
     */
    virtual void setSelected(const bool b, const bool recursive = true);

    /// get the file name where the data have to be stored/were stored
    const QString getFileName() const;

    /// set the file name where the data have to be stored
    void setFileName(const QString&);

    /// Overriden from QObject, this one is only intercepting signal for dynamic property changed (see constructor).
    bool event(QEvent* e) override;

    /// Get a QMenu that contains all the action that can be applied to this component.
    QMenu* getActionMenu();
    ///@}

    /**
      * @name InterfaceProperty
      * All the implemented InterfaceProperty methods
      */
    ///@{
    /// Get the inheritance hierachy of this Component instance as a list of QString
    QStringList getHierarchy() const override;

    /// Assert that a Component instance really inherits from a given className
    bool isInstanceOf(QString className) const override;

    /**
     * get the number of alternative property widgets
     * @see PropertyExplorer
     */
    unsigned int getNumberOfPropertyWidget() {
        return 0;
    }

    /**
     * Get the ith alternative property widget
     * @see PropertyExplorer
     */
    QWidget* getPropertyWidgetAt(unsigned int i) {
        return nullptr;
    }

    /** Get the property object that could be understood by PropertyEditor.
     *  Returns this as any Component instance can manage its list of dynamic properties (and Component inherits
     *  from InterfaceProperty ).
     *  You can also have a separate class to manage your Component properties. In this case, just overide this
     *  method and return the corresponding instance.
     *  @see PropertyExplorer
     *  @see ObjectController
     */
    QObject* getPropertyObject() {
        return this;
    }

    /** update property: if you this method, do not forget to call the superclass method
     *  for the property not managed locally in order to properly manage all inherited dynamic properties.
     * This method is called when a dynamic property has to be udpated
     * @param name the name of the dynamic property
     * @param value the new value to take into account
     */
    void updateProperty(QString name, QVariant value) override;

    /**
     * Set the index of the tab in the ProperlyExplorer to select for display.
     * The ProperlyExplorer may features several tabs of widget.
     * This method allows to select the one to select for display in a given context.
     * @param index the index to select in the tab of the ProperlyExplorer.
     * @see PropertyExplorer
     **/
    inline void setIndexOfPropertyExplorerTab(unsigned int index) {
        this->indexOfPropertyExplorerTab = index;
    }

    /**
     * Get the index of the tab in the ProperlyExplorer to select for display.
     * The ProperlyExplorer may features several tabs of widget.
     * This method allows to select the one to select for display in a given context.
     * @return the index to select in the tab of the ProperlyExplorer.
     * @see PropertyExplorer
     **/
    inline unsigned int getIndexOfPropertyExplorerTab() {
        return this->indexOfPropertyExplorerTab;
    }

    /** Get a Property given its name
     *  @param name the property name
     *  @return NULL if the name does not match any property name
     *
     *  @see Property
     */
    Q_INVOKABLE Property* getProperty(QString name) override;

    /** Add a new CamiTK property to the component.
     * If the property already exist, it will just change its value.
     *
     * \note
     * The component takes ownership of the Property instance.
     *
     * @return false if the Qt Meta Object property was added by this method (otherwise the property was already defined and true is returned if it was successfully updated)
     */
    bool addProperty(Property*) override;
    ///@}

    /**
      * @name InterfaceNode
      * All the implemented InterfaceNode methods
      */
    ///@{
    //-- the methods below are commented because the default comment in InterfaceNode lacks some information...
    void addChild(InterfaceNode*) override;
    void attachChild(InterfaceNode*) override;
    /** remove  from the the sub item vector.
      * This method automatically update the parentComponent of  (set to NULL).
      */
    void removeChild(InterfaceNode*) override;

    /// set the parent Component. This method automatically remove this Component from its previous (if already had one parent Component)
    void setParent(InterfaceNode*) override;

    //--not commented because Doxygen automatically use the inherited documentation (set INHERIT_DOCS flag to YES in the Doxyfile)
    void deleteChildren() override;
    QString getName() const override;
    void setName(const QString&) override;
    const ComponentList& getChildren() override;
    bool doubleClicked() override;
    InterfaceNode* getParent() override;
    QPixmap getIcon() override;

    /** A component name is not displayed in italic by default.
     * You must redefine this method in you inherited Component to change this behaviour.
     */
    bool inItalic() const override;

    /// get the popup menu to display (always return NULL, overwrite this method if you want to give here you own popup)
    QMenu* getPopupMenu(QWidget* parent = nullptr) {
        return nullptr;
    }
    /// @}

    /**
      * @name InterfaceGeometry
      * All the implemented InterfaceGeometry methods (delegated or not, see also Component.cpp)
      */
    ///@{
    /// get the string used to display the label, do the same as getName
    const QString getLabel() const override;

    /// set the string used to display the label, do the same as setName
    void setLabel(QString newName) override;

    delegateGet0(myGeometry, getPointSet, vtkSmartPointer<vtkPointSet>)

    delegate1(myGeometry, setPointSet, vtkSmartPointer<vtkPointSet>)

    delegate1(myGeometry, setPointData, vtkSmartPointer<vtkDataArray>)

    delegateConstGet0(myGeometry, getDataPort, vtkSmartPointer<vtkAlgorithmOutput>)

    delegate1(myGeometry, setDataConnection, vtkSmartPointer<vtkAlgorithmOutput>)

    delegateGet1(myGeometry, getActor, vtkSmartPointer<vtkActor>, const RenderingModes)

    // TODO : uses an object myRepresentation (which is a Geometry or a Slice)
    // to use a single delegate macro
    vtkSmartPointer<vtkProp> getProp(const QString& param) {
        if (myGeometry) {
            return myGeometry->getProp(param);
        }
        else if (mySlice) {
            return mySlice->getProp(param);
        }
        return nullptr;
    }

    unsigned int getNumberOfProp() const {
        if (myGeometry) {
            return myGeometry->getNumberOfProp();
        }
        else if (mySlice) {
            return mySlice->getNumberOfProp();
        }
        return 0;
    }

    vtkSmartPointer<vtkProp> getProp(unsigned int index) {
        if (myGeometry) {
            return myGeometry->getProp(index);
        }
        else if (mySlice) {
            return mySlice->getProp(index);
        }
        return nullptr;
    }

    bool addProp(const QString& name, vtkSmartPointer<vtkProp> prop) {
        if (myGeometry) {
            return myGeometry->addProp(name, prop);
        }
        else if (mySlice) {
            return mySlice->addProp(name, prop);
        }
        return false;
    }


    bool removeProp(const QString& name) {
        if (myGeometry) {
            return myGeometry->removeProp(name);
        }
        else if (mySlice) {
            return mySlice->removeProp(name);
        }
        return false;
    }
    // END TODO


    /** an inherited class can redefine this method something specific.
     *  Default behaviour: do nothing.
     */
    void pointPicked(vtkIdType, bool) {}

    /** an inherited class can redefine this method something specific.
     *  Default behaviour: do nothing.
     */
    void cellPicked(vtkIdType, bool) {}

    // --

    /// compute the object's bounding box [xmin,xmax, ymin,ymax, zmin,zmax], see Component.cpp
    void getBounds(double* bounds) override;

    /** compute the object's bounding sphere radius, @see Component.cpp
      * @return the bounding radius of the Geometry or -1 if there is no Geometry
      */
    double getBoundingRadius() override;

    delegate4(myGeometry, setPointPosition, const unsigned int, const double, const double, const double)

    delegateAndInvokeChildren1(myGeometry, setRenderingModes, const RenderingModes)

    /// see Component.cpp
    const InterfaceGeometry::RenderingModes getRenderingModes() const override;

    delegateAndInvokeChildren1(myGeometry, setEnhancedModes, const EnhancedModes)

    delegateConstGet0(myGeometry, getEnhancedModes, const EnhancedModes)

    delegateAndInvokeChildren1Array(myGeometry, setActorColor, const RenderingModes, double, 4)

    delegateAndInvokeChildren4(myGeometry, setActorColor, const RenderingModes, const double, const double, const double)

    /// see Component.cpp
    void getActorColor(const RenderingModes, double [4]) override;

    delegateAndInvokeChildren3(myGeometry, setColor, const double, const double, const double)

    delegateAndInvokeChildren4(myGeometry, setColor, const double, const double, const double, const double)

    delegateAndInvokeChildren2(myGeometry, setActorOpacity, const RenderingModes, const double)

    delegateConstGet1(myGeometry, getActorOpacity, double, const RenderingModes)

    delegateAndInvokeChildren1(myGeometry, setOpacity, const double)

    delegate2(myGeometry, setMapperScalarRange, double, double)

    delegate1(myGeometry, setTexture, vtkSmartPointer<vtkTexture>)

    void setGlyphType(const GlyphTypes type, const double size = 0.0) override;

    delegate1(myGeometry, setLinesAsTubes, bool)

    delegate1(myGeometry, setMeshWorldTransform, vtkSmartPointer<vtkTransform>)

    ///@}

    /**
    * @name InterfaceBitMap
    * All the implemented InterfaceBitMap methods
    */
    ///@{
    delegateConstGet0(mySlice, getImageData, vtkSmartPointer<vtkImageData>)

    delegate1(mySlice, setOriginalVolume, vtkSmartPointer<vtkImageData>)

    delegateConstGet0(mySlice, get2DImageActor, vtkSmartPointer<vtkImageActor>)

    delegateConstGet0(mySlice, get3DImageActor, vtkSmartPointer<vtkImageActor>)

    delegateConstGet0(mySlice, getPickPlaneActor, vtkSmartPointer<vtkActor>)

    delegateGet0(mySlice, getPixelActor, vtkSmartPointer<vtkActor>)

//    delegateGet0(mySlice, get2DAxesActor, vtkSmartPointer<vtkAxesActor>)

    delegate3(mySlice, pixelPicked, double, double, double)

    delegate0(mySlice, updatePickPlane)

    delegate1(mySlice, setSlice, int)

    delegate3(mySlice, setSlice, double, double, double)

    delegateConstGet0(mySlice, getNumberOfColors, int)

    delegate3(mySlice, setPixelRealPosition, double, double, double)

    delegate1(mySlice, setImageWorldTransform, vtkSmartPointer<vtkTransform>)

    /// see Component.cpp
    int getNumberOfSlices() const override;

    /// see Component.cpp
    int getSlice() const override;
    ///@}


    /**
     * @name InterfaceFrame
     * All the implemented InterfaceFrame methods
     */
    ///@{
    const QString& getFrameName() const override;

    delegate1(myFrame, setFrameName, QString)

    InterfaceFrame* getParentFrame() const override;

    void setParentFrame(InterfaceFrame* frame, bool keepTransform = true) override;

    const QVector<InterfaceFrame*>& getChildrenFrame() const override;

    const vtkSmartPointer<vtkTransform> getTransformFromWorld() const override;

    const vtkSmartPointer<vtkTransform> getTransform() const override;

    const vtkSmartPointer<vtkTransform> getTransformFromFrame(InterfaceFrame* frame) const override;

    delegate1(myFrame, setTransform, vtkSmartPointer<vtkTransform>)

    delegate0(myFrame, resetTransform)

    delegate3(myFrame, translate, double, double, double)

    delegate3(myFrame, rotate, double, double, double)

    delegate3(myFrame, rotateVTK, double, double, double)

    delegate3(myFrame, setTransformTranslation, double, double, double)

    delegate3(myFrame, setTransformTranslationVTK, double, double, double)

    delegate3(myFrame, setTransformRotation, double, double, double)

    delegate3(myFrame, setTransformRotationVTK, double, double, double)

    vtkSmartPointer<vtkAxesActor> getFrameAxisActor() override;

    delegate2(myFrame, setFrameVisibility, Viewer*, bool)

    bool getFrameVisibility(Viewer* viewer) const override;

    delegate1(myFrame, addFrameChild, InterfaceFrame*)

    delegate1(myFrame, removeFrameChild, InterfaceFrame*)
    ///@}


protected:
    /// myGeometry is the 3d representation of this Component, the Component delegates all InterfaceGeometry activity to myGeometry (delegation pattern)
    InterfaceGeometry* myGeometry;

    /// mySlice is the slice representation of this data component, the Component delegates all InterfaceBitMap activity to mySlice (delegation pattern)
    InterfaceBitMap* mySlice;

    /// who is the boss? The Component!
    InterfaceNode* myParentNode;

    /// myFrame is the pose (position and orientation) of the Component in the world frame, the Component delegates all InterfaceFrame activity to myFrame (delegation pattern)
    InterfaceFrame* myFrame;

    /// The explorer sub items
    ComponentList childrenComponent;

    /// tells if this particular Component is selected or not
    bool isSelectedFlag;

    /// the modificatio flag (could be extended to manage a undo/redo list)
    bool modifiedFlag;

    /// the file name from which the Component is loaded
    QString myFileName;

    /// The PropertyExplorer tab index to select once refreshed
    unsigned int indexOfPropertyExplorerTab;


private:
    /** @name Instance members
      */
    ///@{

    /// method called in constructors for general initialization
    void init();

    /// the service implemented to be represented in the InteractiveViewer
    Representation myService;

    /// my name
    QString myName;

    /** Instanciate the concrete representation (either InterfaceGeometry or InterfaceBitMap) if needed.
      * This method has to instanciate Slice (mySlice) or Geometry (myGeometry) that does all the work for this Component, i.e. the adaptee handler.
      * Generally this method should be called in the Component constructor.
      */
    virtual void initRepresentation() = 0;

    /// Where is this Component currently viewed
    QMap<Viewer*, bool> myViewers;

    /// the action menu for this component
    QMenu* actionsMenu;

    /// list of CamiTK property decorating the dynamic properties
    QMap<QString, Property*> propertyMap;
    ///@}

    /** @name Static members
      * All the static member (manage the application-wide list of Component + the application-wide current selection + viewers)
      */
    ///@{
    /// set of used viewers
    static QSet<Viewer*> allViewers;
    ///@}

};


// -------------------- isSelected --------------------
inline bool Component::isSelected() const {
    return isSelectedFlag;
}

// -------------------- doubleClicked --------------------
inline bool Component::doubleClicked() {
    // always false by default. You must overload this method in Components to change its behaviour.
    return false;
}

// -------------------- getChildren --------------------
inline const ComponentList& Component::getChildren() {
    return childrenComponent;
}

// -------------------- getName --------------------
inline QString Component::getName() const {
    return myName;
}

// -------------------- getParent --------------------
inline InterfaceNode* Component::getParent() {
    return ((Component*) myParentNode);
}

// -------------------- getFrame --------------------
inline InterfaceFrame* Component::getFrame() {
    return myFrame;
}

// -------------------- getPixmap ------------------
inline QPixmap Component::getIcon() {
    return QPixmap(0, 0); // this is a NULL QPixmap in the Qt sense. QPixmap::isNull will then return true;
}

// -------------------- inItalic --------------------
inline bool Component::inItalic() const {
    return false;
}

// -------------------- setName --------------------
inline void Component::setName(const QString& n) {
    myName = n;
    if (myGeometry) {
        myGeometry->setLabel(n);
    }
}

// -------------------- setModified --------------------
inline void Component::setModified(bool modification) {
    modifiedFlag = modification;
}

// -------------------- getModified --------------------
inline bool Component::getModified() const {
    return modifiedFlag;
}

// -------------------- getModified --------------------
inline const QString Component::getLabel() const {
    return getName();
}
// -------------------- getModified --------------------
inline void Component::setLabel(QString newName) {
    setLabel(newName);
}

}

#endif


