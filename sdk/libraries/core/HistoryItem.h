/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef HISTORYITEM_H
#define HISTORYITEM_H

// Qt stuffs
#include <QString>
#include <QVariant>
#include <QList>

// CamiTK stuffs
#include <Component.h>
#include "HistoryComponent.h"

namespace camitk {

/**
 * @ingroup group_sdk_libraries_core
 *
 * @brief
 * HistoryItem class describes the entry of an action used in a pipeline, in the history.
 *
 * In CamiTK, every Action are stored in a history, which can be saved as a XML file. \n
 * Each history entry is an instance of HistoryItem and provides information about the processed action :
 * - the name of the action processed.
 * - the properties (name, values).
 * - its input components, stored as HistoryComponent instances.
 * - its output components, stored as HistoryComponent instances.
 *
 * History items are mainly useful for getting back to a previous state by undoing an action or for scripting by storing in a file a pipeline of actions written in Python. \n
 *
 * The history is stored in the Application class and uses the "construct on first use" idiom/design-pattern (with singletons). \n
 * It therefore avoids the infamous "static initialization order fiasco",
 * see http://www.parashift.com/c++-faq/ctors.html
 *
 * @see HistoryComponent
 */
class CAMITK_API HistoryItem {
private:

    /// The name of the action in the pipeline
    QString name;

    /// The properties stored for this action
    QMap<QByteArray, QVariant> properties;
    /// The input components for this action
    QList<HistoryComponent> inputHistoryComponents;
    /// The outpu components for this action
    QList<HistoryComponent> outputHistoryComponents;

public:
    /// Empty constructor
    HistoryItem() = default;

    /// Default constructor
    /// @param name
    /// name of the action stored in the history
    HistoryItem(QString name);

    /// Virtual destructor
    virtual ~HistoryItem() = default;

    /// Get the name of the corresponding action in the pipeline.
    /// @return the name of the action in the pipeline.
    QString getName();

    /// Add a property of the corresponding action to the history item.
    /// @param name Name of the property to addProperty.
    /// @param value Value of the property to add (no type specified).
    void addProperty(QByteArray name, QVariant value);


    /// Get the input components of the history item action.
    /// @return The input components required to the corresponding action to work.
    QList<HistoryComponent> getInputHistoryComponents();

    /// Set the input components of the history item action.
    /// @param inputHistoryComponents The list of input HistoryComponent.
    void setInputHistoryComponents(QList<HistoryComponent> inputHistoryComponents);

    /// Get the output components of the history item action.
    /// @return The output components that the action has created / modified.
    QList<HistoryComponent> getOutputHistoryComponents();

    /// Set the output components of the history item action.
    /// @param outputHistoryComponents The list of output HistoryComponent.
    void setOutputHistoryComponents(QList<HistoryComponent> outputHistoryComponents);

};

}

#endif // HISTORYITEM_H
