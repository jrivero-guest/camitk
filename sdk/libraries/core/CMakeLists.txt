# name of the camitk core target 
if (PACKAGING_NSIS)
   set(CAMITK_CORE_LIB_TARGET_NAME library_${CAMITK_CORE_LIB_NAME})
else()
    set(CAMITK_CORE_LIB_TARGET_NAME library-${CAMITK_CORE_LIB_NAME})
endif()


# Don't forget to include output directory, otherwise
# the UI file won't be wrapped!
include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/action
  ${CMAKE_CURRENT_SOURCE_DIR}/application
  ${CMAKE_CURRENT_SOURCE_DIR}/component
  ${CMAKE_CURRENT_SOURCE_DIR}/component/mesh
  ${CMAKE_CURRENT_SOURCE_DIR}/component/image
  ${CMAKE_CURRENT_SOURCE_DIR}/operators
  ${CMAKE_CURRENT_SOURCE_DIR}/utils
  ${CMAKE_CURRENT_SOURCE_DIR}/viewer
  ${CAMITK_INCLUDE_DIRECTORIES}
)

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# find all sources files, set rules for qt files...
gather_headers_and_sources(${CAMITK_CORE_LIB_TARGET_NAME}) 

# Needed for a correct CamiTK core plugin management (decl export/import) on WIN32
add_definitions(-DCOMPILE_CAMITK_API)

# prepare the library specific info (SONAME...)
set(CAMITKCORE_LIBRARY_PROPERTIES ${CAMITKCORE_LIBRARY_PROPERTIES}
    VERSION   "${CAMITK_VERSION_MAJOR}.${CAMITK_VERSION_MINOR}.${CAMITK_VERSION_PATCH}"
    SOVERSION "${CAMITK_VERSION_MAJOR}"
)

# camitkcore only depends on qtpropertybrowser, which is a public library
# add the library path as an additionnal to look for.
link_directories(${CAMITK_BUILD_BIN_DIR} ${CAMITK_PUBLIC_LIB_DIR})

# CAMITK_CORE_LIB_NAME needs to be SHARED and loaded only ONCE otherwise the extensions
# can have their own copy of CAMITK_CORE_LIB_NAME, which will generates loads of problems
# because of the Singleton design pattern and various factory bits in CAMITK_CORE_LIB_NAME.
add_library(${CAMITK_CORE_LIB_TARGET_NAME} SHARED ${${CAMITK_CORE_LIB_TARGET_NAME}_SOURCES})

# linking with Qt5
qt5_use_modules(${CAMITK_CORE_LIB_TARGET_NAME} ${CAMITK_QT_COMPONENTS})

# Update XML Project description adding this target as a subproject of the main CamiTK project with
# its dependencies (in the CMake target point of view)
if(PACKAGING_NSIS)
    camitk_register_subproject(CORELIB ${CAMITK_CORE_LIB_TARGET_NAME} DEPENDENCIES library_qtpropertybrowser)
else()
    camitk_register_subproject(CORELIB ${CAMITK_CORE_LIB_TARGET_NAME} DEPENDENCIES library-qtpropertybrowser)
endif()

# set the library specific info (SONAME...)
set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES ${CAMITKCORE_LIBRARY_PROPERTIES} LINK_INTERFACE_LIBRARIES "")
if (WIN32 AND MSVC)
    # property required for camitk file association on windows platform compilation with Visual Studio
    set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES COMPILE_FLAGS "/Zc:wchar_t-"
                                                        DEBUG_POSTFIX ${CAMITK_DEBUG_POSTFIX}
    )
endif()

target_link_libraries(${CAMITK_CORE_LIB_TARGET_NAME} ${CAMITK_LIBRARIES})

# output directory
if (MSVC)
    set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CAMITK_BUILD_BIN_DIR}
                                                                    LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CAMITK_BUILD_BIN_DIR}
                                                                    LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CAMITK_BUILD_BIN_DIR}
    )
    set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CAMITK_BUILD_BIN_DIR}
                                                                    RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CAMITK_BUILD_BIN_DIR}
                                                                    RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CAMITK_BUILD_BIN_DIR}
    )
    set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY ${CAMITK_BUILD_BIN_DIR}
                                                                    ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CAMITK_BUILD_BIN_DIR}
                                                                    ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CAMITK_BUILD_BIN_DIR}                                                        
    )
else()
    set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CAMITK_BUILD_PUBLIC_LIB_DIR}
                                                                    LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CAMITK_BUILD_PUBLIC_LIB_DIR}
                                                                    LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CAMITK_BUILD_PUBLIC_LIB_DIR}
    )
    set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CAMITK_BUILD_PUBLIC_LIB_DIR}
                                                                    RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CAMITK_BUILD_PUBLIC_LIB_DIR}
                                                                    RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CAMITK_BUILD_PUBLIC_LIB_DIR}
    )
    set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY ${CAMITK_BUILD_PUBLIC_LIB_DIR}
                                                                    ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CAMITK_BUILD_PUBLIC_LIB_DIR}
                                                                    ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CAMITK_BUILD_PUBLIC_LIB_DIR}                                                        
    )
endif()

if(PACKAGING_NSIS)
    add_dependencies(${CAMITK_CORE_LIB_TARGET_NAME} library_qtpropertybrowser)
    # add the dependency to the qtpropertybrowser automoc target
    set_property(TARGET ${CAMITK_CORE_LIB_TARGET_NAME} APPEND PROPERTY AUTOGEN_TARGET_DEPENDS library_qtpropertybrowser)
else()
    add_dependencies(${CAMITK_CORE_LIB_TARGET_NAME} library-qtpropertybrowser)
    # add the dependency to the qtpropertybrowser automoc target
    set_property(TARGET ${CAMITK_CORE_LIB_TARGET_NAME} APPEND PROPERTY AUTOGEN_TARGET_DEPENDS library-qtpropertybrowser)
endif()


# -- headers to install 
# TODO create a manually selected list (some headers are just needed internally)
# TODO create few specific include files like: CamiTKCore and CamiTKComponent that
# include most of the needed headers and that can be used simply e.g.
# #include <CamiTKComponent>
# i.e. without the trailing .h
file(GLOB_RECURSE ${CAMITK_CORE_LIB_TARGET_NAME}_INSTALL_H *.h *.txx)

#--------------
# installation
#--------------
export_headers(${${CAMITK_CORE_LIB_TARGET_NAME}_INSTALL_H} 
               COMPONENT ${CAMITK_CORE_LIB_TARGET_NAME}
               GROUP libraries
)

# change the output library name (without the prefix 'library')
set_target_properties(${CAMITK_CORE_LIB_TARGET_NAME}
                       PROPERTIES OUTPUT_NAME ${CAMITK_CORE_LIB_NAME}
)

# lib installation
install(TARGETS ${CAMITK_CORE_LIB_TARGET_NAME}
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib/${CAMITK_SHORT_VERSION_STRING}
        COMPONENT ${CAMITK_CORE_LIB_TARGET_NAME}
)

cpack_add_component(    ${CAMITK_CORE_LIB_TARGET_NAME}
                        DISPLAY_NAME ${CAMITK_CORE_LIB_TARGET_NAME}
                        DESCRIPTION "The core of CamiTK allows other extensions to work together (required for any extension to work)."
                        REQUIRED
                        GROUP SDK
                        )

# Recursively update the shiboken path variable containing the CamiTK SDK tree structure
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR} CACHE INTERNAL "") 
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR}/action CACHE INTERNAL "") 
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR}/application CACHE INTERNAL "") 
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR}/component CACHE INTERNAL "") 
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR}/component/image CACHE INTERNAL "") 
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR}/component/mesh CACHE INTERNAL "") 
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR}/utils CACHE INTERNAL "") 
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR}/viewer CACHE INTERNAL "") 
