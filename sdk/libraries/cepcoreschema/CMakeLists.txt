# XSD really needed
find_package(XSD REQUIRED)

    # Create lib files from xsd
    file (GLOB_RECURSE XSD_INPUT_FILES *.xsd)

    # generates headers from scratch
    unset(XSD_H)
    unset(XSD_SRCS)

    # generates headers from scratch
    foreach(xsdFile ${XSD_INPUT_FILES})
        get_filename_component(xsdFileName ${xsdFile} NAME_WE)
        add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.hxx ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.cxx
                           COMMAND ${XSD_EXECUTABLE}
                           ARGS cxx-tree ${XSD_CXX_STD_FLAG} --generate-polymorphic --generate-serialization --namespace-map =cepcoreschema --polymorphic-type Method --polymorphic-type Criteria ${xsdFile}
                           DEPENDS ${xsdFile})
        set(XSD_H ${XSD_H} ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.hxx)
        set(XSD_SRCS ${XSD_SRCS} ${CMAKE_CURRENT_BINARY_DIR}/${xsdFileName}.cxx)
    endforeach()

camitk_library(STATIC
               SOURCES ${XSD_H} ${XSD_SRCS}
               NEEDS_XSD
               HEADERS_TO_INSTALL ${XSD_H}
               CEP_NAME SDK
               DESCRIPTION "Enable data-binding in XML of CEP core basic classes. Required by the application wizard and cepgenerator."
)
