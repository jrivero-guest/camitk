#!/bin/bash
readAndInsert() {
    while read -r line
    do
    if [[ $line =~ xsd:include ]]; then
        file=$(echo $line | sed -e 's/.*="\(.*\)".*/\1/g')   
        echo "<!-- $file -->"
        readAndInsert $file
    else
        if [ "$2" == "top" ]; then
            echo $line
        else
            case $line in
                *encoding*)
                    ;;
                *xsd\:schema*)
                    ;;
                *http://camitk.imag.fr/cepcoreschema*)
                    ;;
                elementFormDefault*)
                    ;;
                \>)
                    ;;
                *)
                    echo $line
                    ;;
            esac
        fi
    fi
    done < $1
}

readAndInsert $1 top

