<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>OffExtension</name>
    <message>
        <source>OFF &lt;em&gt;.off&lt;/em&gt; files in &lt;b&gt;CamiTK&lt;/b&gt;.(very few support!) &lt;br&gt;(c)TIMC-IMAG UJF 2012</source>
        <translation>Les fichiers OFF &lt;em&gt;.off&lt;/em&gt; dans &lt;b&gt;CamiTK&lt;/b&gt;.(trés peu de support!) &lt;br&gt;(c)TIMC-IMAG UJF 2012</translation>
    </message>
    <message>
        <source>The selected component does not have any points or cells. This is an invalid mesh.</source>
        <translation>Le component sélectionné n&apos;a pas de point ou de cellule. Ce maillage est non valide.</translation>
    </message>
</context>
</TS>
