/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "OffComponent.h"
#include "OffExtension.h"

//-- Qt
#include <QFileInfo>

//-- VTK
#include <vtkCellType.h>
#include <vtkCell.h>

//-- CamiTK
#include <Log.h>
using namespace camitk;

// --------------- getName -------------------
QString OffExtension::getName() const {
    return "Off Component";
}

// --------------- getDescription -------------------
QString OffExtension::getDescription() const {
    return tr("OFF <em>.off</em> files in <b>CamiTK</b> <br>(c) Univ. Grenoble Alpes");
}

// --------------- getFileExtensions -------------------
QStringList OffExtension::getFileExtensions() const {
    QStringList ext;
    ext << "off";
    return ext;
}

// --------------- open -------------------
camitk::Component* OffExtension::open(const QString& fileName) {
    return new OffComponent(fileName);
}

// --------------- save -------------------
bool OffExtension::save(Component* component) const {
    bool saveOk = true;

    vtkSmartPointer<vtkPointSet> ps =  component->getPointSet();

    if (ps == NULL || ps->GetNumberOfPoints() == 0 || ps->GetNumberOfCells() == 0) {
        CAMITK_WARNING(tr("Save as Off: the selected component does not have any points or cells. This is an invalid mesh."))
        return false;
    }

    QString baseFilename = QFileInfo(component->getFileName()).absolutePath() + "/" + QFileInfo(component->getFileName()).completeBaseName();

    QString offFilename = baseFilename + ".off";

    ofstream offFile(offFilename.toUtf8());
    double xyz[3];
    offFile << "OFF" << endl;
    // write the number of points, cells and edges
    offFile << ps->GetPoints()->GetNumberOfPoints()  << " " << ps->GetNumberOfCells() << " " << ps->GetNumberOfElements(vtkPointSet::EDGE) << endl;
    // list the points
    for (int i = 0; i < ps->GetPoints()->GetNumberOfPoints(); i++) {
        ps->GetPoints()->GetPoint(i, xyz);
        // write atom
        offFile << xyz[0] << " " << xyz[1] << " " << xyz[2] << endl;
    }

    for (vtkIdType i = 0; i < ps->GetNumberOfCells() ; i++) {
        switch (ps->GetCell(i)->GetCellType()) {
            case VTK_TRIANGLE :
                offFile << "3 ";
                for (vtkIdType j = 0; j < ps->GetCell(i)->GetNumberOfPoints(); j++) {
                    offFile << ps->GetCell(i)->GetPointId(j) << " ";
                }
                offFile << endl;
                break;
            case VTK_QUAD :
                offFile << "4 ";
                for (vtkIdType j = 0; j < ps->GetCell(i)->GetNumberOfPoints(); j++) {
                    offFile << ps->GetCell(i)->GetPointId(j) << " ";
                }
                offFile << endl;
                break;
            default:
                saveOk = false;
                CAMITK_ERROR("Unsupported cell type (only triangles and quad are supported).")
                break;
        }
    }

    offFile.close();

    return saveOk;
}
