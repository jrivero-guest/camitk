/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef VRMLCOMPONENT_H
#define VRMLCOMPONENT_H

#include <Component.h>
#include <MeshComponent.h>

#include <QColorDialog>
/**
 *  @ingroup group_sdk_components_vrml
 *
 *  @brief
 *  The manager of the VRML data.
 *  This also shows how to create a component composed by subcomponent (here all subcomponents are MeshComponent).
 *
 *  If the VRML document is composed by sub object (VRML children), they will appear as
 *  unspecialized MeshComponent children component of this component.
 *
 *  \note VRMLComponent is made of MeshComponent but is not a MeshComponent itself as it has no
 *  representation.
 *
 **/
class VRMLComponent : public camitk::Component {

public:
    /// default constructor
    /// This method may throw an AbortException if a problem occurs.
    VRMLComponent(const QString& file);

    /// do nothing to init the representation, as all representation are done in the sub-component
    virtual void initRepresentation() {};
};

#endif // VRMLCOMPONENT_H
