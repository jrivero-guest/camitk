/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "VRMLComponentExtension.h"

#include "VRMLComponent.h"

//-- CamiTK stuff
#include <InteractiveViewer.h>
#include <Log.h>
using namespace camitk;

//-- vtk stuff
#include <vtkVRMLExporter.h>

// --------------- getName -------------------
QString VRMLComponentExtension::getName() const {
    return "VRML 2 Component";
}

// --------------- getDescription -------------------
QString VRMLComponentExtension::getDescription() const {
    return tr("Manage VRML 2 <em>.wrl .vrml</em> files in <b>CamiTK</b>.(very few support!)");
}

// --------------- getFileExtensions -------------------
QStringList VRMLComponentExtension::getFileExtensions() const {
    QStringList ext;
    ext << "vrml" << "wrl";
    return ext;
}

// --------------- open -------------------
Component* VRMLComponentExtension::open(const QString& fileName) {
    return new VRMLComponent(fileName);
}

// --------------- save -------------------
bool VRMLComponentExtension::save(Component* component) const {
    // NOTE: in Vtk an exporter can only export all the scene, while a Vtk writer is able to save a single mesh
    // As there are no vtkVRMLWriter, the whole scene is exported (so close everything you don't
    // want to have in the resulting vrml file!)
    MeshComponent* mesh = dynamic_cast<MeshComponent*>(component);
    if (mesh == nullptr) {
        CAMITK_WARNING(tr("Component \"%1\" is not of type \"MeshComponent\". Save aborted").arg(component->getName()))
        return false;
    }
    else if (mesh->getPointSet()) {
        vtkSmartPointer<vtkVRMLExporter> exporter = vtkSmartPointer<vtkVRMLExporter>::New();
        exporter->SetInput(InteractiveViewer::get3DViewer()->getRendererWidget()->GetRenderWindow());
        exporter->SetFileName(mesh->getFileName().toStdString().c_str());
        exporter->Write();
        return true;
    }
    else {
        return false;
    }
}

