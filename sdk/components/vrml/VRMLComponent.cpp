/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#include "VRMLComponent.h"

#include <QFileInfo>

//-- vtk
#include <vtkVRMLImporter.h>
#include <vtkActorCollection.h>
#include <vtkMapper.h>
#include <vtkRenderer.h>
#include <QEvent>

using namespace camitk;

// -------------------- default constructor  --------------------
VRMLComponent::VRMLComponent(const QString& file) : Component(file, QFileInfo(file).baseName()) {

    // create a VTK importer to read the VRML file
    vtkSmartPointer<vtkVRMLImporter> importer = vtkSmartPointer<vtkVRMLImporter>::New();
    importer->SetFileName(file.toStdString().c_str());
    importer->Update();

    // get all private actors point set and create the corresponding MeshComponent with it
    vtkSmartPointer<vtkActorCollection> actors = importer->GetRenderer()->GetActors();
    actors->InitTraversal();
    for (unsigned int i = 0; i < (unsigned int)actors->GetNumberOfItems(); i++) {
        vtkSmartPointer<vtkActor> actor = vtkActor::SafeDownCast(actors->GetItemAsObject(i));
        if (actor) {
            vtkSmartPointer<vtkPointSet> ps = vtkPointSet::SafeDownCast(actor->GetMapper()->GetInput());
            importer->Update();
            if (ps) {
                new MeshComponent(this, ps, QString("VRML object #") + QString::number(actors->GetNumberOfItems() - i - 1));
            }
        }
    }
}


