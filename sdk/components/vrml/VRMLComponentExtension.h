/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VRMLCOMPONENTEXTENSION_H
#define VRMLCOMPONENTEXTENSION_H

#include <MeshComponentExtension.h>
/**
 *  @ingroup group_sdk_components_vrml
 *
 *  @brief
 *  This VRML Component allows you to manipulate VRML files (unstructured grid and
  * polydata files).
  *
  * This component shows:
  * - how to build subcomponents
  * - how to extend a MeshComponent
  * - add an save to facility, @see save()
  *
  *  Thanks to this extension, any mesh component of CamiTK can now be exported to VRML
  *  (Although this is not really a good example as VTK does not know how to export a single
  *  component to VRML but can only export the whole scene, see save() comment).
 *
 **/
class VRMLComponentExtension : public camitk::MeshComponentExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ComponentExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.component.vrml")

public:
    /// the constructor (do nothing really)
    VRMLComponentExtension() : MeshComponentExtension() {}

    /// get the plugin name
    virtual QString getName() const;

    /// get the plugin description (can be html)
    virtual QString getDescription() const;

    /// get the list of managed extensions
    virtual QStringList getFileExtensions() const;

    /** get a new instance from data stored in a file
     *  This method may throw an AbortException if a problem occurs.
     */
    virtual camitk::Component* open(const QString&);

    /** save the given MeshComponent to VRML (does not have to be top-level).
     *
     *  @return false if the operation was not performed properly or not performed at all.
     */
    virtual bool save(camitk::Component*) const;

protected:
    /// the destructor
    virtual ~VRMLComponentExtension() = default;
};

#endif
