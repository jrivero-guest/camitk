/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "StlExtension.h"

// include generated components headers
#include "StlComponent.h"
#include <vtkSTLWriter.h>

using namespace camitk;

// --------------- getName -------------------
QString StlExtension::getName() const {
    return "STL Component";
}

// --------------- getDescription -------------------
QString StlExtension::getDescription() const {
    return tr("Manage STL format, see <a href=\"https://en.wikipedia.org/wiki/STL_%28file_format%29\">https://en.wikipedia.org/wiki/STL_%28file_format%29</a>");
}

// --------------- getFileExtensions -------------------
QStringList StlExtension::getFileExtensions() const {
    QStringList ext;
    ext << "stl" << "STL";

    return ext;
}

// --------------- open -------------------
Component* StlExtension::open(const QString& fileName) {
    return new StlComponent(fileName);
}

// --------------- save --------------------
bool StlExtension::save(Component* component) const {
    bool saveOk;
    component->refresh();
    component->getFileName();

    vtkSmartPointer<vtkSTLWriter> exporter = vtkSmartPointer<vtkSTLWriter>::New();
    exporter->SetFileName(component->getFileName().toUtf8().constData());
    exporter->SetInputConnection(component->getDataPort());
    saveOk = exporter->Write();

    return saveOk;
}

