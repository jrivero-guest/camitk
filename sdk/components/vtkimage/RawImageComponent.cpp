/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core image component stuff
#include "RawImageComponent.h"

// -- VTK stuff
#include <vtkImageReader2.h>

using namespace camitk;

// -------------------- constructor --------------------
RawImageComponent::RawImageComponent(const QString& fileName)
    : ImageComponent(fileName) {
    if (!fileName.isNull()) {
        // restore the normal cursor
        QApplication::restoreOverrideCursor();

        myDialog = new RawDataDialog(fileName);
        if (myDialog->exec() == QDialog::Accepted) {
            createComponent();
        }
        else {
            throw AbortException("Opening cancelled by user\n");
        }
    }
    else {
        throw AbortException("No file name\n");
    }
}

// -------------------- destructor --------------------
RawImageComponent::~RawImageComponent() {
    if (myDialog) {
        delete (myDialog);
    }
}

// -------------------- createComponent --------------------
void RawImageComponent::createComponent() {
    QString filename       = myDialog->getFilename();
    int dimX               = myDialog->getDimX();
    int dimY               = myDialog->getDimY();
    int dimZ               = myDialog->getDimZ();
    int voxelType          = myDialog->getVoxelType();
    bool bigEndianType     = myDialog->isBigEndian();
    int nbScalarComponents = myDialog->getNbScalarComponents();
    double voxelSizeX      = myDialog->getVoxelSizeX();
    double voxelSizeY      = myDialog->getVoxelSizeY();
    double voxelSizeZ      = myDialog->getVoxelSizeZ();
    double Ox              = myDialog->getOriginX();
    double Oy              = myDialog->getOriginY();
    double Oz              = myDialog->getOriginZ();
    int headerSize         = myDialog->getHeaderSize();
    bool lowerLeftOrigin   = myDialog->isLowerLeftOrigin();
    orientation            = myDialog->getOrientation();

    // Reader and image variables
    vtkSmartPointer<vtkImageReader2> reader = vtkSmartPointer<vtkImageReader2>::New();
    vtkSmartPointer<vtkImageData> image = NULL;

    reader->SetFileName(filename.toStdString().c_str());
    if (dimZ > 1) {
        reader->SetFileDimensionality(3);
    }
    reader->SetDataExtent(0, dimX - 1, 0, dimY - 1, 0, dimZ - 1);
    reader->SetDataScalarType(voxelType);
    if (bigEndianType) {
        reader->SetDataByteOrderToBigEndian();
    }
    reader->SetNumberOfScalarComponents(nbScalarComponents);
    reader->SetDataSpacing(voxelSizeX, voxelSizeY, voxelSizeZ);
    reader->SetDataOrigin(Ox, Oy, Oz);
    if (lowerLeftOrigin) {
        reader->FileLowerLeftOn();
    }
    else {
        reader->FileLowerLeftOff();
    }
    reader->SetHeaderSize(headerSize);
    image = reader->GetOutput();

    try {
        reader->Update();
    }
    catch (...) {
        throw AbortException("VTK was unable to read file " + filename.toStdString());
    }

    setImageData(image, false, orientation);

}


