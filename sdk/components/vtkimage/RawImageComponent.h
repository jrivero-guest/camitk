/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef RAW_IMAGE_COMPONENT_H
#define RAW_IMAGE_COMPONENT_H


// -- raw data image component stuff
#include "RawDataDialog.h"

// -- Core image component stuff
#include <ImageComponent.h>
#include <ImageOrientationHelper.h>

// -- VTK stuff
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
/**
 * @ingroup group_sdk_components_vtkimage
 *
 * @brief
 * This class manage raw images, i.e., images that are directly loaded from a file containing only the binary values of the voxels.
 * The user has to fill a form in order to describe the parameters (volume and voxel sizes, binary type...)
 *
 **/
class RawImageComponent : public camitk::ImageComponent  {
    Q_OBJECT
public:
    /** default constructor: give it the name of the file containing the data
     *  This method may throw an AbortException if a problem occurs.
     */
    RawImageComponent(const QString&);

    /// Destructor
    ~RawImageComponent();

    /// Create the component using the different option given by the user in the dialog box
    virtual void createComponent();

protected:
    /// The UI asking for the user to select information on the Raw image
    RawDataDialog* myDialog;

private:
    /// The orientation of the image
    camitk::ImageOrientationHelper::PossibleImageOrientations orientation;

};



#endif // RAW_IMAGE_COMPONENT_H
