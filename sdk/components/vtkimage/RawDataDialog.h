/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef RAWDATADIALOG_H
#define RAWDATADIALOG_H

// -- Core image component stuff
#include "ui_RawDataDialog.h"
#include <ImageOrientationHelper.h>


// -- Core image component stuff classes
class RawImageComponent;

// -- QT stuff
#include <QDialog>

/**
 * @ingroup group_sdk_components_vtkimage
 *
 * @brief
 * Input of the parameter to read a raw data file
 *
 **/
class RawDataDialog : public QDialog {
    Q_OBJECT

public:
    /**Default Constructor*/
    RawDataDialog(QString filename);
    /**Destructor*/
    ~RawDataDialog() override;

    /// Initializes the dialog options
    void init();

    /**
    * @name Accessors
    * Get the raw data information
    */
    ///@{
    int getVoxelType();
    int getDimX();
    int getDimY();
    int getDimZ();
    bool isBigEndian();
    int getNbScalarComponents();
    double getVoxelSizeX();
    double getVoxelSizeY();
    double getVoxelSizeZ();
    double getOriginX();
    double getOriginY();
    double getOriginZ();
    int getHeaderSize();
    bool isLowerLeftOrigin();
    QString getFilename();
    camitk::ImageOrientationHelper::PossibleImageOrientations getOrientation();
    ///@}


public slots :
    virtual void voxelTypeChanged(int);

private :
    QString filename;

    // Qt model designed by QtDesigner (with qt4, nor more inheritance from the ui)
    Ui::RawDataDialog ui;

} ;
#endif // RAWDATADIALOG_H


