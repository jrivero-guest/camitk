/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VTK_IMAGE_COMPONENT_H
#define VTK_IMAGE_COMPONENT_H

#include <vtkImageData.h>
#include <ImageComponent.h>


#include "VtkImageComponentAPI.h"
#include <QMetaProperty>
/**
 * @ingroup group_sdk_components_vtkimage
 *
 * @brief
 * This class manage vtk images, i.e images that can be loaded in CamiTK by using an VTK importer.
 *
 **/
class VTK_IMAGE_COMPONENT_API VtkImageComponent : public camitk::ImageComponent  {
    Q_OBJECT
public:
    /** default constructor: give it the name of the file containing the data
     *  This method may throw an AbortException if a problem occurs.
     */
    VtkImageComponent(const QString&);

    /// needed for deleting
    virtual ~VtkImageComponent() = default;

    /** actually create the component from the file
     *  This method may throw an AbortException if a problem occurs.
     */
    virtual void createComponent(const QString&);

private:
    /**
     * Read the TranformMatrix tag (rotation of the image) from the input MHA/MHD image.
     * This tag can store a 2x2 matrix (for 2D images represented in 2D) and 4x4 matrix
     * for 2D or 3D images represented in 3D.
     *
     * @param fileName The input MHA / MHD file name to read the tag from.
     * @return A 4x4 homogenious matrix containing the Rotation information of the image
     */
    vtkSmartPointer<vtkMatrix4x4> readMetaImageTransformMatrix(const QString& fileName);


};



#endif // VTK_IMAGE_COMPONENT_H
