/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// ----- VTK_COMPONENT_API.h
#ifndef VTK_COMPONENT_API_H
#define VTK_COMPONENT_API_H

// -----------------------------------------------------------------------
//
//                           VTK_COMPONENT_API
//
// -----------------------------------------------------------------------

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the COMPILE_VTK_COMPONENT_API
// flag defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// VTK_COMPONENT_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

#if defined(_WIN32) // MSVC and mingw

#ifdef COMPILE_VTK_COMPONENT_API
#define VTK_COMPONENT_API __declspec(dllexport)
#else
#define VTK_COMPONENT_API __declspec(dllimport)
#endif // COMPILE_VTK_COMPONENT_API

#else // for all other platforms IMAGES_API is defined to be "nothing"

#ifndef VTK_COMPONENT_API
#define VTK_COMPONENT_API
#endif // VTK_COMPONENT_API

#endif // MSVC and mingw

#endif ///VTK_COMPONENT_API_H
