<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>VtkMeshComponent</name>
    <message>
        <source>Ambient Lightning Coefficient. Warning: this cannot be saved in the VTK file.</source>
        <translation>Coefficient d&apos;éclairement Ambient. Attention: cela ne peut pas être sauvé dans le fichier VTK.</translation>
    </message>
    <message>
        <source>Diffuse Lightning Coefficient. Warning: this cannot be saved in the VTK file.</source>
        <translation>Coefficient d&apos;éclairement diffus. Attention: cela ne peut pas être sauvé dans le fichier VTK.</translation>
    </message>
    <message>
        <source>Specular Lightning Coefficient. Warning: this cannot be saved in the VTK file.</source>
        <translation>Coefficient d&apos;éclairement spéculaire. Attention: cela ne peut pas être sauvé dans le fichier VTK.</translation>
    </message>
    <message>
        <source>Specular Power. Warning: this cannot be saved in the VTK file.</source>
        <translation>Puissance spéculaire.. Attention: cela ne peut pas être sauvé dans le fichier VTK.</translation>
    </message>
    <message>
        <source>The object&apos;s opacity. 1.0 is totally opaque and 0.0 is completely transparent. Warning: this cannot be saved in the VTK file.</source>
        <translation>L&apos;opacité de l&apos;objet. 1.0 est totalement opaque et 0.0 est complêtement transparent. Attention: cela ne peut pas être sauvé dans le fichier VTK.</translation>
    </message>
    <message>
        <source>Specular Surface Color</source>
        <translation>Couleur de surface spéculaire</translation>
    </message>
</context>
<context>
    <name>VtkMeshComponentExtension</name>
    <message>
        <source>Manage VTK &lt;em&gt;.vtk&lt;/em&gt; files in &lt;b&gt;CamiTK&lt;/b&gt;.&lt;br/&gt;CamiTK is based on vtk. Lots of things are possible with a vtk model!</source>
        <translation>Gérer les fichier VTK &lt;em&gt;.vtk&lt;/em&gt; dans &lt;b&gt;CamiTK&lt;/b&gt;.&lt;br/&gt;CamiTK est basé sur vtk. Beaucoup de choses sont possible avec le modèle vtk!</translation>
    </message>
</context>
</TS>
