/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MSH_COMPONENT_H
#define MSH_COMPONENT_H

#include <MeshComponent.h>
#include "MshComponentAPI.h"

/**
 * @ingroup group_sdk_components_msh
 *
 * @brief
 * This simple MSH Component allows you to manipulate .msh legacy and V2 files (gmsh).
 *
 * @see
 * www.geuz.org/gmsh/doc/texinfo/gmsh.html for more information about this format
 *
 **/
class MSH_COMPONENT_API MshComponent : public camitk::MeshComponent {

    Q_OBJECT

public:

    /// default constructor
    /// This method may throw an AbortException if a problem occurs.
    MshComponent(const QString& file);

private :

    void readLegacyFormat(std::ifstream& fileStream);

    void readV2Format(std::ifstream& fileStream);

};

#endif
