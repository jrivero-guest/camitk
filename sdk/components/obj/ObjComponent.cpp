/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ObjComponent.h"

#include <QFileInfo>

#include <vtkOBJReader.h>
#include <vtkProperty.h>

using namespace camitk;

// -------------------- default constructor  --------------------
ObjComponent::ObjComponent(const QString& file) : MeshComponent(file) {

    // use the file name without extension as component name
    setName(QFileInfo(file).baseName());

    //-- build the vtkPoinSet from the file
    // create an obj importer to read the OBJ file
    vtkSmartPointer<vtkOBJReader> importer = vtkSmartPointer<vtkOBJReader>::New();
    importer->SetFileName(getFileName().toUtf8().constData());
    importer->Update();

    // instanciate the Geometry
    initRepresentation(importer->GetOutput());
}


