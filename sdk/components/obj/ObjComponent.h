/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef OBJ_COMPONENT_H
#define OBJ_COMPONENT_H

#include <MeshComponent.h>
/**
 * @ingroup group_sdk_components_obj
 *
 * @brief
 * The manager of the objdata (alias wavefront format).
 *  This component is using VtkObjReader to import the file and shows how
 *  to add static and dynamic properties to a component.
 *
 **/
class ObjComponent : public camitk::MeshComponent {
    Q_OBJECT

public:
    /// default constructor
    /// This method may throw an AbortException if a problem occurs.
    ObjComponent(const QString& file);

private:

};

#endif
