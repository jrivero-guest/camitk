/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SAVEACTIONSTATE_H
#define SAVEACTIONSTATE_H

#include "ActionState.h"

/**
 * @ingroup group_sdk_application_asm
 *
 * @brief
 * The asm application uses a state machine. This class implements the state of action save.
 *
 */
class SaveActionState : public ActionState {

public:
    typedef struct {
        QString name;
        QString type;
        QString extension;
        QString directory;
    } saveComponentsInfo;

    /**
     */
    SaveActionState(QState* parent, QString name, QString description,
                    QString defaultSaveDirName, QTextStream* logStream = nullptr);

    void setInput(QVector<saveComponentsInfo> inputComponentsInfo);

    virtual camitk::Action::ApplyStatus applyAction();

private:
    QString defaultSaveDirName;

    /* Input components
     * - names
     * - types
     * - saving extension
     * - saving directories
     */
    QVector<saveComponentsInfo> inputComponentsInfo;

};
#endif // SAVEACTIONSTATE_H
