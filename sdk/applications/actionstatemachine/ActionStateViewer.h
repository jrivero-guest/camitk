/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONSTATEVIEWER_H
#define ACTIONSTATEVIEWER_H

// Qt stuff
#include <QString>
#include <QStackedWidget>

// CamiTK stuff
#include "Viewer.h"

// Local includes
#include "ActionState.h"

/**
 * @ingroup group_sdk_application_asm
 *
 * @brief
 * The current action state viewer.
 *
 */
class ActionStateViewer : public camitk::Viewer {
    Q_OBJECT

public:
    /// returns the unique instance oh ActionStateViewer
    static ActionStateViewer* getActionStateViewer();

    /// get the viewer widget. @param parent the parent widget for the viewer widget
    virtual QWidget* getWidget(QWidget* parent = nullptr);

    /// returns the number of Component that are displayed by this viewer
    virtual unsigned int numberOfViewedComponent() {
        return 0;
    };

    /// refresh the view (can be interesting to know which other viewer is calling this)
    virtual void refresh(Viewer* whoIsAsking = nullptr) {};


    void setState(ActionState* actionState);

private:
    ActionStateViewer();

    /// the singleton ActionStateViewer instance
    static ActionStateViewer* actionStateViewer;

    QWidget* myWidget;
    QString name;

    /// actions stacked widget of the viewer
    QStackedWidget* actionStateWidgetStack;

    /// index of the empty widget,
    /// used when no action is active or when
    /// no action has been used for the currently selected components
    int emptyActionWidgetIndex;

};

#endif // ACTIONSTATEVIEWER_H
