/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONTRANSITION_H
#define ACTIONTRANSITION_H

#include <QSignalTransition>
#include <QString>
#include <QTime>
#include <QMap>
#include <QTextStream>
#include <QStringList>
#include <QPushButton>

class ActionState;
/**
 * @ingroup group_sdk_application_asm
 *
 * @brief
 * This class handle a transition between two states (including previous/next buttons).
 * Specific things can happen for CamiTK SCXML transition (e.g., apply the selected action before going to the next, closing components...)
 *
 * The transition's name is set as the button's text.
 *
 * Example:
 *  \code
 *  <camitk:onTransition>
 *      <!-- Closing some components -->
 *      <camitk:close>
 *          <camitk:component type="ImageComponent" name="inputImage"/>
 *          <!-- even if image2 is modified, it will be closed without asking the user for a filename -->
 *          <camitk:component type="ImageComponent" name="generatedImage" force="true"/>
 *      </camitk:close>
 *  </camitk:onTransition>
 *  \endcode
 *
 */
class ActionTransition : public QSignalTransition {
    Q_OBJECT

public:

    /// constructor: takes at least the state and signal considered for the transition
    ActionTransition(QPushButton* sender, const char* signal, QState* sourceState = nullptr, bool applyPreviousAction = true, QTextStream* logStream = nullptr);

    /// add a component's name and type to the list of component to close during the transition.
    /// Set force to "true" in order to close the component independently of its modified flag.
    void addComponentToClose(QString compName, QString compType, bool force = false);

    /// called during the transition
    void onTransition(QEvent* e) override;

    /// Check the name of the transition (i.e. text of the button)
    bool isNamed(QString) const;

    /// programmatically activate the transition (during autoNext), i.e. call "click" on the button
    void autoNext();

private:
    /// should the action be applied during the transition (default set to true)
    bool applyPreviousAction;

    /// Map containing all the name/type of the components to be closed during the transition
    QMap<QString, QString> componentsToClose;

    /// list of the components' name that should be closed even if they are modified
    QStringList componentsToForceClose;

    /// local pointer to the log stream
    QTextStream* logStream;

    /// Keep track of time (needed for the log)
    QTime* startTime;

    /// the button that controls this transition
    QPushButton* myButton;
};
#endif // ACTIONTRANSITION_H
