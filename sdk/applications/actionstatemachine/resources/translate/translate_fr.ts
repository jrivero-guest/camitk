<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ActionStateMachine</name>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="69"/>
        <source>Please select an xml file</source>
        <translation>Veuillez sélectionner un fichier xml</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="76"/>
        <location filename="../../ActionStateMachine.cpp" line="89"/>
        <source>Please select a directory where to save log and component files</source>
        <translation>Veuillez sélectionner un répertoire où sauver les fichiers log et component</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="158"/>
        <source>File not found: the file </source>
        <translation>Fichier introuvable: le fichier</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="158"/>
        <source> was not found</source>
        <translation>était introuvable</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="160"/>
        <source>Error: cannot find file </source>
        <translation>Erreur:fichier introuvable</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="160"/>
        <source> sorry...</source>
        <translation>désolé...</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="165"/>
        <location filename="../../ActionStateMachine.cpp" line="173"/>
        <source>File </source>
        <translation>le Fichier</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="165"/>
        <source> have no valid root (not a valid XML file format)</source>
        <translation>n&apos;a pas de racine valide (format de fichier invalide)</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="173"/>
        <source> is not a SCXML file (</source>
        <translation>n&apos;est pas un fichier SCXML (</translation>
    </message>
    <message>
        <location filename="../../ActionStateMachine.cpp" line="173"/>
        <source>root node not equals to &quot;scxml&quot;)</source>
        <translation>le noeud de la racine n&apos;est pas égale à &quot;scxml&quot;)</translation>
    </message>
</context>
<context>
    <name>ActionStateWidget</name>
    <message>
        <location filename="../../ActionStateWidget.ui" line="132"/>
        <source>State Name</source>
        <translation>Nom de l&apos;état</translation>
    </message>
    <message>
        <location filename="../../ActionStateWidget.ui" line="142"/>
        <source>State Description</source>
        <translation>Description de l&apos;état</translation>
    </message>
</context>
</TS>
