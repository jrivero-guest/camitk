<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : log2html.xsl
    Created on : 11 février 2013, 15:00
    Author     : cfouard
    Description:
        Show an ActionStateMachine application log in a readable way
/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:log='http://ujf-grenoble.fr/camitk/logApplication'>
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>Log of Application <xsl:value-of select="/log:application/log:name"/></title>
                <style type="text/css">
body {
    background: gray;
}

.mainFrame {
    border: 3px solid gray;
    background: white;
    -moz-border-radius: 20px;
    -webkit-border-radius: 20px;
    -khtml-border-radius: 20px;
    border-radius: 40px;
    margin-top:50px;
    margin-bottom:50px;
    margin-right:50px;
    margin-left:50px;
}

.stateFrame {
    margin-top:50px;
    margin-bottom:50px;
    margin-right:50px;
    margin-left:50px;

    /* make rounded borders */
    border: 3px solid #ccc;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    -khtml-border-radius: 10px;
    border-radius: 10px;

    /* make a nice linear graidant */
    background: #eef; /* Old browsers */
    background: -moz-linear-gradient(top,  #eec; 0%, #ccc 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eee), color-stop(100%,#ccc)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #eee 0%,#ccc 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #eee 0%,#ccc 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #eee 0%,#ccc 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #eee 0%,#ccc 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eee', endColorstr='#ccc',GradientType=0 ); /* IE6-9 */

}

h1 {
    font-weight: bold;
    font-size: 25px;
    text-align: center;
}

h2 {
    font-size: 20px;
    text-align: center;
    font-style: italic;
}
h3 {
    font-size: 20px;
    text-align: center;    
}
table {
    border-width:1px;
    border-style:solid;
    border-color:#eee;
    width:90%;
    margin-left:5%;
    margin-right:5%;
 }

td {
    text-align: center;
    border-width:1px;
    border-style:solid;
    border-color:#ccc;
}
.enhanced {
    font-size: 20px;
    font-style: italic;
    font-weight: bold;
}
.italic {
    font-style: italic;
}
table.simple {
    border-width:0px;
}
td.simple {
    border-width: 0px;
}

.stateColor {
    color: darkblue;
}
.actionColor {
    color: darkred;
}
                </style>
            </head>
            <body>
                <div class="mainFrame">                    
                    <div class="stateFrame">
                        <h1><xsl:value-of select="/log:application/log:name"/></h1>
                        <h2>Log file</h2>
                        <h3> Runned the <xsl:value-of select="/log:application/log:startDate"/></h3>
                        <h3> Start: <xsl:value-of select="/log:application/log:startTime"/> Stop: <xsl:value-of select="/log:application/log:endTime"/> </h3>
                        <h2> duration:  <xsl:call-template name="formatTime">
                                            <xsl:with-param name="timeElapsed" select="/log:application/log:timeEnlapsed"/>
                                        </xsl:call-template>
                        </h2>
                        <h3>Input xml file name: <xsl:value-of select="/log:application/log:inputXmlFile"/></h3>
                    </div>

                    <div class="stateFrame">
                        <xsl:variable name="timeStateTotal"  select="sum(log:application/log:state/log:timeEnlapsed)"/>
                        <xsl:variable name="timeActionTotal" select="sum(log:application/log:transition/log:timeEnlapsed)"/>

                        <h3>Total time within states (action triggering): 
                            <span class="stateColor">
                                <xsl:call-template name="formatTime">
                                    <xsl:with-param name="timeElapsed" select="sum(log:application/log:state/log:timeEnlapsed)"/>
                                </xsl:call-template>
                            (i.e., <xsl:value-of select="sum(log:application/log:state/log:timeEnlapsed)"/> ms)</span>
                        </h3>
                       
                        <h3>Total time on action application: 
                            <span class="actionColor">
                                <xsl:call-template name="formatTime">
                                    <xsl:with-param name="timeElapsed" select="sum(log:application/log:transition/log:timeEnlapsed)"/>
                                </xsl:call-template>
                                (i.e., <xsl:value-of select="sum(log:application/log:transition/log:timeEnlapsed)"/> ms)</span>
                        </h3>
                        <table>                            
                            <tr>
                                <td><h2>State</h2></td><td><h2>Duration</h2></td>
                            </tr>                            
                            <xsl:apply-templates select="/log:application/log:state | /log:application/log:transition"/>
                        </table>
                    </div>
                    
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="log:state">
        <xsl:variable name="count">
            <xsl:number/>
        </xsl:variable>
        
        <tr>
            <td>
                <span class="stateColor"><div class="enhanced">
                <xsl:value-of select="$count"/> ) <xsl:value-of select="log:name"/>
                </div></span>
            </td>
            <td>
                <span class="stateColor">
                    <xsl:call-template name="formatTime">
                        <xsl:with-param name="timeElapsed" select="log:timeEnlapsed"/>
                    </xsl:call-template>
                </span>
            </td>
         </tr>
    </xsl:template>
    <xsl:template match="log:transition">
        <xsl:apply-templates select="log:applyAction | log:closing"/>
    </xsl:template>
    <xsl:template match="log:applyAction">
        
        <tr>
            <td colspan="2">
                <span class="actionColor">
                Action Applied: <span class="enhanced"><xsl:value-of select="log:name"/></span>
                with status <span class="enhanced"><xsl:value-of select="log:status"/></span>
                </span>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr><td><span class="actionColor">Parameters</span></td>
                        <td><span class="actionColor">Inputs</span></td>
                        <td><span class="actionColor">Outputs</span></td></tr>
                    <tr>
                        <td><xsl:apply-templates select="log:parameters"/></td>
                        <td><xsl:apply-templates select="log:inputs"/></td>
                        <td><xsl:apply-templates select="log:outputs"/></td>
                    </tr>
                </table>
            </td>
            <td>
                <span class="actionColor">
                    <xsl:call-template name="formatTime">
                        <xsl:with-param name="timeElapsed" select="../log:timeEnlapsed"/>
                    </xsl:call-template>
                </span>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="log:closing">
       <tr>
            <td colspan="2">
                <span class="actionColor"><span class="enhanced">Closing:</span></span>                
            </td>
        </tr>
        <tr>
            <td>
                <table class="simple">
                    <xsl:apply-templates select="log:component"/>
                </table>
            </td>    
            <td>
                <span class="actionColor">
                    <xsl:call-template name="formatTime">
                        <xsl:with-param name="timeElapsed" select="../log:timeEnlapsed"/>
                    </xsl:call-template>
                </span>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template match="log:parameters">
        <table class="simple">
            <tr>
                <td>
                    <span class="actionColor"><span class="italic">Name</span></span>
                </td>
                <td><span class="actionColor"><span class="italic">Value</span></span>
                </td>
            </tr>
            <xsl:apply-templates select="log:parameter"/>
        </table>
    </xsl:template>

    <xsl:template match="log:parameter">
        <tr>
            <td>
                <span class="actionColor"><xsl:value-of select="@name"/></span>
            </td>
            <td>
                <span class="actionColor"><xsl:value-of select="@value"/></span>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="log:outputs">
        <table class="simple">
            <tr>
                <td>
                    <span class="actionColor"><span class="italic">Type</span></span>
                </td>
                <td>
                    <span class="actionColor"><span class="italic">Name</span></span>
                </td>
            </tr>
            <xsl:apply-templates select="log:component"/>
        </table>
    </xsl:template>

    <xsl:template match="log:inputs">
        <table class="simple">
            <tr>
                <td>
                    <span class="actionColor"><span class="italic">Type</span></span>
                </td>
                <td>
                    <span class="actionColor"><span class="italic">Name</span></span>
                </td>
            </tr>
            <xsl:apply-templates select="log:component"/>
        </table>
    </xsl:template>

    <xsl:template match="log:component">
        <tr>
            <td>
                <span class="actionColor"><xsl:value-of select="@type"/></span>
            </td>
            <td>
                <span class="actionColor"><xsl:value-of select="@name"/></span>
            </td>
        </tr>
    </xsl:template>
    
    <xsl:template name="formatTime">
        <xsl:param name="timeElapsed" select="/log:application/log:timeEnlapsed"/>
        <!--
            int milisec = (startTime->elapsed());
            int mili    = milisec % 1000;
            int seconds = ((milisec % (3600000)) % (60000)) / 1000;
            int minutes = (milisec % (3600000)) / (60000);
            int hours   = milisec / (3600000);
        -->
        <xsl:variable name="hours"    select="floor($timeElapsed div 3600000)"/>
        <xsl:variable name="minutes"  select="floor(($timeElapsed - $hours * 3600000) div 60000)"/>
        <xsl:variable name="secondes" select="floor(($timeElapsed - $hours * 3600000 - $minutes * 60000) div 1000)"/>
        <xsl:variable name="msec"     select="floor($timeElapsed - $hours * 3600000 - $minutes * 60000 -$secondes * 1000)"/>        <xsl:if test="$hours > 0"><xsl:value-of select="$hours"/> h </xsl:if>
        <xsl:if test="$minutes > 0"><xsl:value-of select="$minutes"/> min </xsl:if>
        <xsl:if test="$secondes > 0"><xsl:value-of select="$secondes"/> s </xsl:if>
        <xsl:if test="$msec > 0"><xsl:value-of select="$msec"/> ms</xsl:if>
    </xsl:template>
</xsl:stylesheet>
