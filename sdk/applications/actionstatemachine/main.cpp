/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Application Action State Machine Stuff
#include "ActionStateMachine.h"
#include <Core.h>
#include <Log.h>

#include <QTimer>
#include <QDir>

#include "CommandLineOptions.hxx"

using namespace camitk;

// usage for this application
void usage(char* appName) {
    std::cerr << appName << std::endl;
    std::cerr << std::endl;
    std::cerr << appName << std::endl;
    std::cerr << "Build using " << Core::version << std::endl;
    std::cerr << std::endl;
    options::print_usage(std::cerr);
}

int main(int argc, char** argv) {
    try {
        int end; // End of options.
        options o(argc, argv, end);

        if (o.help()) {
            usage(argv[0]);
            return EXIT_SUCCESS;
        }
        else if (o.file() == "" || o.output_dir() == "") {
            usage(argv[0]);
            return EXIT_SUCCESS;
        }
        else {
            // log all trace to output dir
            Log::getLogger()->setLogFileDirectory(QDir(o.output_dir().c_str()));
            Log::getLogger()->setLogToFile(true);
            Log::getLogger()->setLogLevel(InterfaceLogger::TRACE);

            // create a camitk application
            ActionStateMachine am(argc, argv, QString(o.file().c_str()), QString(o.output_dir().c_str()));

            // automatically call autonext slot at application start up
            if (o.autonext()) {
                QTimer::singleShot(0, &am, SLOT(autoNext()));
            }

            return am.exec();
        }
    }
    catch (const cli::exception& e) {
        cerr << e << endl;
        usage(argv[0]);
        return EXIT_FAILURE;
    }
    catch (const camitk::AbortException& e) {
        std::cout << argv[0] << " aborted..." << std::endl << "camitk AbortException:" << std::endl << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e) {
        std::cout << argv[0] << " aborted..." << std::endl << "std exception:" << std::endl << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...) {
        std::cout << argv[0] << " aborted..." << std::endl << "Unknown Exception" << std::endl;
        return EXIT_FAILURE;
    }

}

