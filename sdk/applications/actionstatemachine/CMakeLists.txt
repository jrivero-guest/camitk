# use the application extension macro
camitk_application(NEEDS_QT_MODULES #we use QtXML
                   ADDITIONAL_SOURCES CommandLineOptions.cxx CommandLineOptions.hxx CommandLineOptions.ixx
                   CEP_NAME SDK
                   DESCRIPTION "Simple action state machine to pipeline actions from an XML file"
)

# Recursively update the shiboken path variable containing the CamiTK SDK tree structure
set(SHIBOKEN_CAMITK_SDK_PATH ${SHIBOKEN_CAMITK_SDK_PATH}:${CMAKE_CURRENT_SOURCE_DIR} CACHE INTERNAL "") 


