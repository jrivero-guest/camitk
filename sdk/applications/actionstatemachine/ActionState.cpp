/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ActionState.h"
//FIXME #include "ActionStateViewer.h"

// -- CamiTK Core stuff
#include <Application.h>
#include <Component.h>
#include <ActionWidget.h>

// -- Qt stuff
#include <QPushButton>
#include <QMetaProperty>

#include <Log.h>


using namespace camitk;

// ------------- constructor -----------------
ActionState::ActionState(QState* parent, QString name, QString description, QTextStream* logStream)
    : QState(parent) {
    this->name = name;
    this->description = description;
    this->logStream = logStream;
    this->myAction = nullptr;
    this->myStateWidget = new ActionStateWidget(this);
}

// ------------- setAction -----------------
void ActionState::setAction(Action* action, QMap<QString, QVariant> parameters, QMap<QString, QString> inputComponentNames, QMap<QString, QString> outputComponentNames) {
    this->myAction = action;
    this->parameters = parameters;
    this->inputComponentNames  = inputComponentNames;
    this->outputComponentNames = outputComponentNames;

    // auto update properties by default
    if (myAction) {
        myAction->setAutoUpdateProperties(true);
    }
}

// ------------- getName -----------------
QString ActionState::getName() {
    return name;
}

// ------------- getDescription -----------------
QString ActionState::getDescription() {
    return description;
}

// ------------- getWidget -----------------
ActionStateWidget* ActionState::getWidget() {
    return myStateWidget;
}

// ------------- addActionTransition -----------------
ActionTransition* ActionState::addActionTransition(QString transitionName, QAbstractState* nextState, bool applyAction, QVector<Action::ApplyStatus> disableConditions) {

    QPushButton* button = myStateWidget->addTransitionButton(transitionName);
    ActionTransition* transition = new ActionTransition(button, SIGNAL(clicked()), this, applyAction, logStream);
    transition->setTargetState(nextState);

    for (QVector<Action::ApplyStatus>::const_iterator it = disableConditions.begin();
            it != disableConditions.end(); it++) {
        Action::ApplyStatus status = (*it);
        QMap<Action::ApplyStatus, QVector<QPushButton* > >::iterator buts = conditionalButtons.find(status);
        if (buts != conditionalButtons.end()) {
            buts.value().append(button);
        }
        else {
            QVector<QPushButton*> vect;
            vect.append(button);
            conditionalButtons.insert(status, vect);
        }
    }

    return transition;

}

// ------------- applyAction -----------------
Action::ApplyStatus ActionState::applyAction() {
    Action::ApplyStatus status(Action::TRIGGERED);

    if (myAction != NULL) {
        if (logStream != nullptr) {
            (*logStream) << "\t\t<applyAction>" << endl;
        }

        // apply the action
        status = myAction->applyInPipeline();

        // set the right names to outputComponents
        ComponentList outputComps = myAction->getOutputComponents();
        ComponentList::const_iterator outIt;
        QMap<QString, QString>::const_iterator namesIt;
        namesIt = outputComponentNames.begin();
        if ((logStream != NULL) && (outputComps.size() > 0)) {
            (*logStream) << "\t\t\t<outputs>" << endl;
        }
        for (outIt = outputComps.begin(); outIt != outputComps.end(); outIt++) {
            if (namesIt != outputComponentNames.end()) {
                QString compName = namesIt.key();
                QString compType = namesIt.value();
                if ((*outIt)->isInstanceOf(compType)) {
                    (*outIt)->setName(compName);
                    namesIt++;
                    if (logStream != NULL) {
                        (*logStream) << "\t\t\t\t<component name='" ;
                        (*logStream) << compName << "' type='" << compType << "'/>" << endl;
                    }
                }
            }
        }
        if ((logStream != NULL) && (outputComps.size() > 0)) {
            (*logStream) << "\t\t\t</outputs>" << endl;
        }

        Application::refresh();

        if (logStream != nullptr) {
            (*logStream) << "\t\t\t<status>" << Action::getStatusAsString(status) << "</status>" << endl;
            (*logStream) << "\t\t</applyAction>" << endl;
        }
    }

    return status;
}

// ------------- setAlternativeDesc -----------------
void ActionState::setAlternativeDesc(QString altDescText, QVector<Action::ApplyStatus> statusList) {
    for (QVector<Action::ApplyStatus>::const_iterator it = statusList.begin();
            it != statusList.end(); it++) {
        Action::ApplyStatus status = (*it);
        conditionalDescriptions.insert(status, altDescText);
    }

}

// ------------- setPreviousActionStatus -----------------
void ActionState::setPreviousActionStatus(camitk::Action::ApplyStatus status) {
    // Change the description
    QMap<Action::ApplyStatus, QString>::const_iterator desc = conditionalDescriptions.find(status);
    if (desc != conditionalDescriptions.end()) {
        myStateWidget->setDescription(desc.value());
    }
    else {
        myStateWidget->setDescription(description);
    }

    // Inhibate/Re-enable proper buttons
    for (QMap<Action::ApplyStatus, QVector<QPushButton* > >::const_iterator it = conditionalButtons.begin();
            it != conditionalButtons.end(); it++) {
        Action::ApplyStatus itStatus = it.key();
        QVector<QPushButton*> itButtons = it.value();
        for (QVector<QPushButton*>::const_iterator but = itButtons.begin(); but != itButtons.end(); but++) {
            if (itStatus == status) {
                (*but)->setEnabled(false);
            }
            else {
                (*but)->setEnabled(true);
            }
        }
    }
}

// ------------- autoNextEntry -----------------
void ActionState::autoNextEntry() {
    onEntry(new QEvent(QEvent::ApplicationStateChange));
}

// ------------- onEntry -----------------
void ActionState::onEntry(QEvent* event) {

    if (logStream != nullptr) {
        //-- log the start
        startTime = new QTime();
        startTime->start();

        (*logStream) << "\t<state>" << endl;
        (*logStream) << "\t\t<name>" << this->name << "</name>" << endl;
        (*logStream) << "\t\t<startTime>" << startTime->toString("hh:mm:ss:zzz") << "</startTime>" << endl;
    }

    //-- set the parameters default values
    if (myAction != NULL) {
        for (QMap<QString, QVariant>::const_iterator it = parameters.begin(); it != parameters.end(); it++) {
            myAction->setProperty(it.key().toStdString().c_str(), it.value().toString());
        }

        // log the parameters
        (*logStream) << "\t\t\t<name>" << myAction->getName() << "</name>" << endl;
        // Properties
        int nbStaticProps = myAction->metaObject()->propertyCount();
        QList<QByteArray> propertyNames = myAction->dynamicPropertyNames();
        // static properties
        if ((nbStaticProps > 0) || (! propertyNames.isEmpty())) {
            (*logStream) << "\t\t\t<parameters>" << endl;
            for (int index = 0 ; index < nbStaticProps; index++) {
                QString staticPropName = myAction->metaObject()->property(index).name();
                (*logStream) << "\t\t\t\t<parameter name='";
                (*logStream) << staticPropName << "' value='";
                (*logStream) << myAction->property(staticPropName.toStdString().c_str()).toString() ;
                (*logStream) << "' static='true'/>" << endl;
            }

            // dynamic properties
            for (QList<QByteArray>::const_iterator it = propertyNames.begin(); it != propertyNames.end(); it++) {
                (*logStream) << "\t\t\t\t<parameter name='";
                (*logStream) << (*it) << "' value='" << myAction->property(*it).toString() << "'/>" << endl;
            }
            (*logStream) << "\t\t\t</parameters>" << endl;
        }


        //-- set the input components
        ComponentList allComps = Application::getAllComponents();
        QMap<QString, QString>::const_iterator namesIt;
        ComponentList::const_iterator compIt;

        // log inputs
        if (inputComponentNames.size() > 0) {
            (*logStream) << "\t\t\t<inputs>" << endl;
        }

        // search for proper input considering name and type
        ComponentList inputComps;
        for (namesIt = inputComponentNames.begin(); namesIt != inputComponentNames.end(); namesIt++) {
            QString compName = namesIt.key();
            QString compType = namesIt.value();

            // Look for the corresponding component into the list of all components
            compIt = allComps.begin();
            while ((compIt != allComps.end()) && !((*compIt)->getName() == compName && (*compIt)->isInstanceOf(compType))) {
                compIt++;
            }

            if (compIt != allComps.end()) {
                (*logStream) << "\t\t\t\t<component name='";
                (*logStream) << (*compIt)->getName() << "' type='" << compType << "'/>" << endl;
                inputComps.append((*compIt));
            }
        }

        if (inputComponentNames.size() > 0) {
            (*logStream) << "\t\t\t</inputs>" << endl;
        }

        myAction->setInputComponents(inputComps);

        //-- reset modification flags on input components to avoid mismatching output component
        foreach (Component* inputComponent, inputComps) {
            inputComponent->setModified(false);
        }

        //-- set the widget if there is a CamiTK action
        ActionWidget* actionWidget = dynamic_cast<ActionWidget*>(this->myAction->getWidget());
        // if the Action has a widget inheriting from ActionWidget, force the buttons' visibility to false
        if (actionWidget) {
            actionWidget->setButtonVisibility(false);
        }
        // set the widget
        this->myStateWidget->setActionWidget(this->myAction->getWidget());
    }
}

// ------------- autoNextExit -----------------
void ActionState::autoNextExit() {
    onExit(new QEvent(QEvent::ApplicationStateChange));
}

// -------------onExit -----------------
void ActionState::onExit(QEvent* event) {
    if (logStream != nullptr) {
        QTime endTime = QTime::currentTime();
        (*logStream) << "\t\t<!-- exiting " << this->name << " -->" << endl;
        (*logStream) << "\t\t<endTime>" << endTime.toString("hh:mm:ss:zzz") << "</endTime>" << endl;
        (*logStream) << "\t\t<timeEnlapsed unit='ms'>" << (startTime->elapsed()) << "</timeEnlapsed>" << endl;
        (*logStream) << "\t</state>" << endl;
    }
}

// ------------- getInputComponents -----------------
const QMap< QString, QString >& ActionState::getInputComponents() {
    return inputComponentNames;
}

// ------------- getOutputComponents -----------------
const QMap< QString, QString >& ActionState::getOutputComponents() {
    return outputComponentNames;
}
