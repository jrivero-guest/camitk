/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ActionStateWidget.h"
#include "Action.h"
#include "ActionState.h"

ActionStateWidget::ActionStateWidget(ActionState* actionState) {
    ui.setupUi(this);
    myActionState = actionState;
    ui.stateNameLabel->setText(myActionState->getName());
    ui.stateDescriptionLabel->setText(myActionState->getDescription());
    ui.actionFrame->hide();
}

QPushButton* ActionStateWidget::addTransitionButton(QString text) {
    QPushButton* button = new QPushButton(text, this);
    ui.buttonsLayout->addWidget(button);

    return button;
}

void ActionStateWidget::setActionWidget(QWidget* widget) {
    if (widget != nullptr) {
        ui.actionPropertyLayout->addWidget(widget);
        ui.actionFrame->show();
    }
}

void ActionStateWidget::setDescription(QString description) {
    ui.stateDescriptionLabel->setText(description);
}


//QPushButton * ActionStateWidget::setFinal() {
//	const QPalette pal(Qt::red); // enumeration is Qt::GlobalColor
//	QPushButton * button = new QPushButton("Quit", this);
//	button->setAutoFillBackground(true);  //this must be set for palette to work
//	button->setPalette(pal); // assign color palette to button
//
//	ui.buttonsLayout->addWidget(button);
//
//	return button;
//
//}