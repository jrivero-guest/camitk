/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentCreationState.h"
#include "WizardMainWindow.h"
#include "ComponentExtensionCreationState.h"

#include "ComponentDescriptionWidget.h"
#include "ComponentDescriptionState.h"
#include "ComponentAddPropertyWidget.h"
#include "ComponentAddPropertyState.h"
#include "ComponentSummaryWidget.h"
#include "ComponentSummaryState.h"

#include <ComponentExtension.hxx>
#include <Component.hxx>

/**  Constructor */
ComponentCreationState::ComponentCreationState(QString name, WizardMainWindow* mainWindow, ComponentExtensionCreationState* parent)
    : QState(parent) {
    this->name = name;
    this->cancelled = false;
    this->domComponent = nullptr;
    this->domComponentExtension = nullptr;

    createSubStates(mainWindow);
}

void ComponentCreationState::resetDomComponentExtension(cepcoreschema::ComponentExtension* domComponentExtension) {
    this->domComponentExtension = domComponentExtension;
}


void ComponentCreationState::onEntry(QEvent* event) {
    this->cancelled = false;

    if (domComponent != nullptr) {
        delete domComponent;
        domComponent = nullptr;
    }

    domComponent = new cepcoreschema::Component("A Component",  "A component description", "None");

    componentDescriptionState->resetDomComponent(domComponent, domComponentExtension);
    componentAddPropertyState->resetDomComponent(domComponent);
    componentSummaryState->resetComponent(domComponent);

}

void ComponentCreationState::onExit(QEvent* event) {
    if (! cancelled) {
        domComponentExtension->components().component().push_back(*domComponent);
    }
    else {
        if (domComponent != nullptr) {
            delete domComponent;
            domComponent = nullptr;
        }
    }
}

void ComponentCreationState::componentFinished() {
    cancelled = false;
    emit nextCCS();
}

void ComponentCreationState::componentCancelled() {
    cancelled = true;
    emit nextCCS();
}

void ComponentCreationState::createSubStates(WizardMainWindow* mainWindow) {
    componentDescriptionWidget = new ComponentDescriptionWidget(nullptr);
    componentDescriptionState = new ComponentDescriptionState("Component Description", componentDescriptionWidget, mainWindow, this);

    componentAddPropertyWidget = new ComponentAddPropertyWidget(nullptr);
    componentAddPropertyState = new ComponentAddPropertyState("Component Add Property", componentAddPropertyWidget, mainWindow, this);

    componentSummaryWidget = new ComponentSummaryWidget(nullptr);
    componentSummaryState = new ComponentSummaryState("Component Summary", componentSummaryWidget, mainWindow, this);

    this->setInitialState(componentDescriptionState);

    componentDescriptionState->addTransition(componentDescriptionWidget, SIGNAL(next()), componentAddPropertyState);

    componentAddPropertyState->addTransition(componentAddPropertyWidget, SIGNAL(next()), componentSummaryState);
    componentAddPropertyState->addTransition(componentAddPropertyWidget, SIGNAL(previous()), componentDescriptionState);

    componentSummaryState->addTransition(componentSummaryWidget, SIGNAL(previous()), componentAddPropertyState);

    // Cancel
    QObject::connect(componentDescriptionWidget, SIGNAL(cancel()), this, SLOT(componentCancelled()));
    QObject::connect(componentAddPropertyWidget, SIGNAL(cancel()), this, SLOT(componentCancelled()));
    QObject::connect(componentSummaryWidget,     SIGNAL(cancel()), this, SLOT(componentCancelled()));


    QObject::connect(componentSummaryWidget, SIGNAL(next()), this, SLOT(componentFinished()));

}
