/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef COMPONENTPROPERTYWIDGET_H
#define COMPONENTPROPERTYWIDGET_H

// Include GUI  automatically generated file
#include "ui_ComponentPropertyWidget.h"

// includes from Qt
#include <QWidget>
#include <QString>


class ComponentAddPropertyWidget;

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to define a component property.
 *
 */
class  ComponentPropertyWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    ComponentPropertyWidget(ComponentAddPropertyWidget* parent);
    /**  Destructor */
    ~ComponentPropertyWidget() override = default;

    void setRequiredName();
    void unsetRequiredName();

    QString getName();
    QString getType();
    QString getDefaultValue();
    QString getDescription();
    QString getUnit();


public slots:
    virtual void remove();
    virtual void typeChanged(QString);
    virtual void nameChanged(QString);


private:
    /// Qt Designer defined GUI
    Ui::ComponentPropertyWidget ui;

    /// Has to keep track of its parent in case it has to be removed:
    /// The remove button is on this widget, and it cannot "auto-remove" from its parent.
    ComponentAddPropertyWidget* parent;

};
#endif
