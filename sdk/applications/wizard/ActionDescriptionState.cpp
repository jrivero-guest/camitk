/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionDescriptionState.h"
#include "ActionDescriptionWidget.h"
#include "WizardMainWindow.h"
#include "ActionCreationState.h"

// includes from coreschema
#include <Action.hxx>
#include <ActionExtension.hxx>
#include <ItkFilter.hxx>
#include <Cep.hxx>
#include <Components.hxx>

// includes from cepgenerator
#include <ClassNameHandler.h>

// temporary
#include <iostream>


ActionDescriptionState::ActionDescriptionState(QString name, ActionDescriptionWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, ActionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domAction = nullptr;
    this->domActionExtension = nullptr;
    this->domCep = domCep;
}

void ActionDescriptionState::resetDomAction(cepcoreschema::Action* domAction, cepcoreschema::ActionExtension* domActionExtension) {
    this->domAction = domAction;
    this->domActionExtension = domActionExtension;
    auto* actionDescriptionWidget = dynamic_cast<ActionDescriptionWidget*>(widget);
    if (actionDescriptionWidget) {
        QString actionExtensionName = domActionExtension->name().c_str();
        actionDescriptionWidget->setToDefault(actionExtensionName);
    }
}

void ActionDescriptionState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    auto* actionDescriptionWidget = dynamic_cast<ActionDescriptionWidget*>(widget);
    if (actionDescriptionWidget) {
        QString ActionExtensionName = domActionExtension->name().c_str();
        actionDescriptionWidget->setToDefault(ActionExtensionName);
        if ((domAction != NULL) && (QString("An Action") != QString(domAction->name().c_str()))) {
            actionDescriptionWidget->setName(QString(domAction->name().c_str()));
            actionDescriptionWidget->setDescription(QString(domAction->description().c_str()));
        }

        // Check existing CEP components
        // CS: problem to get components name.
        // The loop was under component extension, the corresction is to loop under components for each componentExtension.
        if (domCep->componentExtensions().present()) {
            cepcoreschema::ComponentExtensions::componentExtension_sequence cepComponentExtensions = domCep->componentExtensions().get().componentExtension();
            cepcoreschema::ComponentExtensions::componentExtension_iterator itCompExt;
            // Loop on ComponentExtensions
            for (itCompExt = cepComponentExtensions.begin(); itCompExt != cepComponentExtensions.end(); itCompExt++) {
                cepcoreschema::Components::component_iterator itComp;
                // Loop on components for each componentExtension.
                for (itComp = itCompExt->components().component().begin(); itComp != itCompExt->components().component().end(); itComp++) {
                    QString componentName = (*itComp).name().c_str();
                    componentName = ClassNameHandler::getClassName(componentName);
                    actionDescriptionWidget->addPossibleComponent(componentName);
                }
            }
        }
    }

}

void ActionDescriptionState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* actionDescriptionWidget = dynamic_cast<ActionDescriptionWidget*>(widget);

    if (actionDescriptionWidget) {
        QString actionName = actionDescriptionWidget->getActionName();
        QString actionDescription = actionDescriptionWidget->getActionDescription();
        QString componentName = actionDescriptionWidget->getComponentName();

        domAction->name(actionName.toStdString());
        domAction->description(actionDescription.toStdString());
        domAction->component(componentName.toStdString());

        if (actionDescriptionWidget->isItkFilter()) {
            std::cout << "ITK Filter" << std::endl;
            QString outputType = actionDescriptionWidget->getOutputType();
            std::cout << "Type: " << outputType.toStdString() << std::endl;

            cepcoreschema::ItkFilter theFilter(outputType.toStdString());
            domAction->classification().itkFilter(theFilter);
        }

    }

}
