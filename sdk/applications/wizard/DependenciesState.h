/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef DEPENDENCIESSTATE_H
#define DEPENDENCIESSTATE_H

#include "WizardState.h"

class DependenciesWidget;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Cep;
class Dependencies;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Basic state to define dependencies
 *
 */
class DependenciesState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    DependenciesState(QString name, DependenciesWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, QState* parent);
    /**  Destructor */
    ~DependenciesState() override = default;

protected:
    void onEntry(QEvent* event) override;
    void onExit(QEvent* event) override;

    cepcoreschema::Dependencies* getDependencies();


    cepcoreschema::Cep* domCep;


};
#endif
