/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionCreationState.h"
#include "WizardMainWindow.h"
#include "ActionExtensionCreationState.h"

#include "ActionDescriptionWidget.h"
#include "ActionDescriptionState.h"
#include "ActionAddParameterWidget.h"
#include "ActionAddParameterState.h"
#include "ActionClassificationWidget.h"
#include "ActionClassificationState.h"
#include "ActionSummaryWidget.h"
#include "ActionSummaryState.h"

#include <ActionExtension.hxx>
#include <Action.hxx>
#include <Classification.hxx>

ActionCreationState::ActionCreationState(QString name, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, ActionExtensionCreationState* parent) : QState(parent) {
    this->name = name;
    this->cancelled = false;
    this->domAction = nullptr;

    this->domActionExtension = nullptr;
    this->domCep = domCep;

    createSubStates(mainWindow);
}

void ActionCreationState::resetDomActionExtension(cepcoreschema::ActionExtension* domActionExtension) {
    this->domActionExtension = domActionExtension;
}

void ActionCreationState::onEntry(QEvent* event) {
    this->cancelled = false;
    cepcoreschema::Classification classification("No Family");

    if (domAction != nullptr) {
        delete domAction;
        domAction = nullptr;
    }

    domAction = new cepcoreschema::Action("An Action",  "An action description", "None", classification);
    actionDescriptionState->resetDomAction(domAction, domActionExtension);
    actionAddParameterState->resetDomAction(domAction);
    actionClassificationState->resetDomAction(domAction);
    actionSummaryState->resetAction(domAction);

}

void ActionCreationState::onExit(QEvent* event) {
    if (! cancelled) {
        domActionExtension->actions().action().push_back((*domAction));
    }
    else {
        if (domAction != nullptr) {
            delete domAction;
            domAction = nullptr;
        }
    }
}

void ActionCreationState::actionFinished() {
    cancelled = false;
    emit nextACS();
}

void ActionCreationState::actionCancelled() {
    cancelled = true;
    emit nextACS();
}

void ActionCreationState::createSubStates(WizardMainWindow* mainWindow) {
    actionDescriptionWidget = new ActionDescriptionWidget(nullptr);
    actionDescriptionState = new ActionDescriptionState("Action Description",  actionDescriptionWidget, mainWindow, domCep, this);

    actionAddParameterWidget = new ActionAddParameterWidget(nullptr);
    actionAddParameterState = new ActionAddParameterState("Action Creation",  actionAddParameterWidget, mainWindow, this);

    actionClassificationWidget = new ActionClassificationWidget(nullptr);
    actionClassificationState = new ActionClassificationState("Action Classification",  actionClassificationWidget, mainWindow, this);

    actionSummaryWidget = new ActionSummaryWidget(nullptr);
    actionSummaryState  = new ActionSummaryState("Action Summary", actionSummaryWidget, mainWindow, this);

    this->setInitialState(actionDescriptionState);

    actionDescriptionState->addTransition(actionDescriptionWidget, SIGNAL(next()), actionAddParameterState);

    actionAddParameterState->addTransition(actionAddParameterWidget, SIGNAL(next()), actionClassificationState);
    actionAddParameterState->addTransition(actionAddParameterWidget, SIGNAL(previous()), actionDescriptionState);

    actionClassificationState->addTransition(actionClassificationWidget, SIGNAL(next()), actionSummaryState);
    actionClassificationState->addTransition(actionClassificationWidget, SIGNAL(previous()), actionAddParameterState);

    actionSummaryState->addTransition(actionSummaryWidget, SIGNAL(previous()), actionClassificationState);

    QObject::connect(actionDescriptionWidget, SIGNAL(cancel()), this, SLOT(actionCancelled()));
    QObject::connect(actionAddParameterWidget, SIGNAL(cancel()), this, SLOT(actionCancelled()));
    QObject::connect(actionClassificationWidget, SIGNAL(cancel()), this, SLOT(actionCancelled()));
    QObject::connect(actionSummaryWidget, SIGNAL(cancel()), this, SLOT(actionCancelled()));


    QObject::connect(actionSummaryWidget, SIGNAL(next()), this, SLOT(actionFinished()));

}
