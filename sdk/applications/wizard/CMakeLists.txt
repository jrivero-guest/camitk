camitk_application(NEEDS_XSD
                   NEEDS_XERCESC
                   NEEDS_CEP_LIBRARIES cepgenerator cepcoreschema
                   CEP_NAME SDK
                   ADDITIONAL_SOURCES CommandLineOptions.cxx CommandLineOptions.hxx CommandLineOptions.ixx
                   DESCRIPTION "Create CEP and file skeletons from GUI"
)
set(CAMITK_WIZARD_DEBUG CACHE BOOL FALSE)

# If this variable is true, then the wizard's user control is squeezed. To quickly test the application. 
if (CAMITK_WIZARD_DEBUG)
    add_definitions(-D_WIZARD_QUESTIONS_SQUEEZE)
endif (CAMITK_WIZARD_DEBUG)
                      
