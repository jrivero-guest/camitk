/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentAddPropertyWidget.h"

// local files
#include "ComponentPropertyWidget.h"
#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

ComponentAddPropertyWidget::ComponentAddPropertyWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault();
}

void ComponentAddPropertyWidget::nextButtonClicked() {
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (isOneNonAsciiParameter()) {
        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if (isOneNoName()) {
        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultPropNoName);
    }
    else {
        emit next();
    }
#else
    emit next();
#endif
}

void ComponentAddPropertyWidget::previousButtonClicked() {
    emit previous();
}

void ComponentAddPropertyWidget::cancelButtonClicked() {
    emit cancel();
}

bool ComponentAddPropertyWidget::isOneNoName() {
    bool oneNoName = false;
    QList<ComponentPropertyWidget*>::const_iterator it = list.begin();
    while ((it != list.end()) && (! oneNoName)) {
        QString paramName = (*it)->getName();
        if (paramName.isEmpty()) {
            (*it)->setRequiredName();
            oneNoName = true;
        }
        else {
            (*it)->unsetRequiredName();
        }
        it++;
    }

    return oneNoName;
}

bool ComponentAddPropertyWidget::isOneNonAsciiParameter() {
    bool oneNonAscii = false;
    QList<ComponentPropertyWidget*>::const_iterator it = list.begin();
    while ((it != list.end()) && (! oneNonAscii)) {
        QString paramName = (*it)->getName();
        QString paramType = (*it)->getType();
        QString paramDescription = (*it)->getDescription();
        QString paramDefaultValue = (*it)->getDefaultValue();

        if ((paramName.toUtf8() != paramName.toLatin1()) ||
                (paramType.toUtf8() != paramType.toLatin1()) ||
                (paramDescription.toUtf8() != paramDescription.toLatin1()) ||
                (paramDefaultValue.toUtf8() != paramDefaultValue.toLatin1())) {
            oneNonAscii = true;
        }
        it++;
    }

    return oneNonAscii;
}

void ComponentAddPropertyWidget::setToDefault() {
    // Remove existing properties
    foreach (ComponentPropertyWidget* widget, list) {
        ui.propertiesLayout->removeWidget(widget);
        if (widget != NULL) {
            delete widget;
        }
    }

    list.clear();
}

void ComponentAddPropertyWidget::addPropertyClicked() {
    auto* parameter = new ComponentPropertyWidget(this);
    ui.propertiesLayout->addWidget(parameter);
    list.append(parameter);
}

void ComponentAddPropertyWidget::removeProperty(ComponentPropertyWidget* widget) {
    ui.propertiesLayout->removeWidget(widget);
    list.removeAt(list.indexOf(widget));
    if (widget != nullptr) {
        delete widget;
    }
}

QList<ComponentPropertyWidget*> ComponentAddPropertyWidget::getComponentPropertyWidgets() {
    return list;
}

