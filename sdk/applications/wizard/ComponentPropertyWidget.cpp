/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentPropertyWidget.h"

// Local files
#include "ComponentAddPropertyWidget.h"
#include "DefaultGUIText.h"

// CepGenerator files
#include <ParameterGenerator.h>



ComponentPropertyWidget::ComponentPropertyWidget(ComponentAddPropertyWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    this->parent = parent;
    typeChanged(ui.typeComboBox->currentText());
}

void ComponentPropertyWidget::remove() {
    parent->removeProperty(this);

}

void ComponentPropertyWidget::nameChanged(QString name) {
    ui.groupBox->setTitle(name);
}

void ComponentPropertyWidget::typeChanged(QString typeName) {
    QString defValue;
    defValue = ParameterGenerator::getTypeDefaultValue(typeName);
    ui.defaultLineEdit->setText(defValue);
}

QString ComponentPropertyWidget::getName() {
    return ui.nameLineEdit->text();
}

QString ComponentPropertyWidget::getType() {
    return ui.typeComboBox->currentText();
}

QString ComponentPropertyWidget::getDefaultValue() {
    return ui.defaultLineEdit->text();
}

QString ComponentPropertyWidget::getDescription() {
    return ui.descriptionPlainTextEdit->toPlainText();
}

void ComponentPropertyWidget::setRequiredName() {
    ui.nameLabelStar->setStyleSheet(enhancedStyle);
    ui.requiredFieldLabel->setStyleSheet(enhancedStyle);
}

void ComponentPropertyWidget::unsetRequiredName() {
    ui.nameLabelStar->setStyleSheet(normalStyle);
    ui.requiredFieldLabel->setStyleSheet(normalStyle);
}

QString ComponentPropertyWidget::getUnit() {
    return ui.unitPlainTextEdit->toPlainText();
}
