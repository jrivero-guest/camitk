/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentExtensionDependenciesState.h"
#include "WizardMainWindow.h"
#include "ComponentExtensionCreationState.h"
#include "DependenciesWidget.h"

// includes from coreschema
#include <ComponentExtension.hxx>
#include <Components.hxx>
#include <Component.hxx>


ComponentExtensionDependenciesState::ComponentExtensionDependenciesState(QString name, DependenciesWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, ComponentExtensionCreationState* parent)
    : DependenciesState(name, widget, mainWindow, domCep, parent) {
    this->domComponentExtension = nullptr;
    widget->setElement("Component Extension");
}

void ComponentExtensionDependenciesState::resetDomComponentExtension(cepcoreschema::ComponentExtension* domComponentExtension) {
    this->domComponentExtension = domComponentExtension;
    auto* dependenciesWidget = dynamic_cast<DependenciesWidget*>(widget);
    if (dependenciesWidget != nullptr) {
        dependenciesWidget->setToDefault();
    }

}

void ComponentExtensionDependenciesState::onEntry(QEvent* event) {
    DependenciesState::onEntry(event);

}


void ComponentExtensionDependenciesState::onExit(QEvent* event) {
    DependenciesState::onExit(event);
    cepcoreschema::Dependencies* domDeps = getDependencies();
    if (domDeps != nullptr) {
        if (domDeps->dependency().size() > 0) {
            domComponentExtension->dependencies((*domDeps));
        }
    }
}

