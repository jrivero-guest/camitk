/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepCreateOrModifyWidget.h"

// Qt files
#include <QMessageBox>


CepCreateOrModifyWidget::CepCreateOrModifyWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    ui.createRadioButton->setChecked(true);
}

void CepCreateOrModifyWidget::nextButtonClicked() {
    if (ui.createRadioButton->isChecked()) {
        emit newCep();
    }
    else {
        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, tr("Feature not implemented"), tr("This feature is not implemented yet, sorry...\n"));
        ui.createRadioButton->setChecked(true);
        emit existingCep();
    }
}

void CepCreateOrModifyWidget::backButtonClicked() {
    emit backCep();
}
