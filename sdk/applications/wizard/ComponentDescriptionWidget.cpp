/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentDescriptionWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

ComponentDescriptionWidget::ComponentDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault(defaultComponentExtensionName);

}

void ComponentDescriptionWidget::nextButtonClicked() {
    QString componentName = ui.componentNameItself->text();
    QString componentDescription = ui.componentDescriptionItself->toPlainText();
    QString fileSuffix = ui.suffixItself->text();

#ifndef _WIZARD_QUESTIONS_SQUEEZE

    if (componentName == componentExtensionName) {
        ui.componentNameStar->setStyleSheet(enhancedStyle);
        ui.componentDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultComponentNameAndExtension);
    }
    else if (componentName.toUtf8() != componentName.toLatin1()) {
        ui.componentNameStar->setStyleSheet(enhancedStyle);
        ui.componentDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((componentName.isEmpty()) || (componentName == defaultComponentName)) {
        ui.componentNameStar->setStyleSheet(enhancedStyle);
        ui.componentDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealComponentName);
    }
    else if (componentDescription.toUtf8() != componentDescription.toLatin1()) {
        ui.componentNameStar->setStyleSheet(normalStyle);
        ui.componentDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((componentDescription.isEmpty()) || (componentDescription == defaultComponentDescription)) {
        ui.componentNameStar->setStyleSheet(normalStyle);
        ui.componentDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealComponentDescription);
    }
    else if (fileSuffix.toUtf8() != fileSuffix.toLatin1()) {
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.componentNameStar->setStyleSheet(normalStyle);
        ui.componentDescriptionStar->setStyleSheet(normalStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if (fileSuffix == defaultComponentSuffix) {
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.componentNameStar->setStyleSheet(normalStyle);
        ui.componentDescriptionStar->setStyleSheet(normalStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealFileSuffix);
    }

    else {
        emit next();
    }
#else
    emit next();
#endif
}

void ComponentDescriptionWidget::cancelButtonClicked() {
    emit cancel();
}

void ComponentDescriptionWidget::setName(QString name) {
    ui.componentNameItself->setText(name);
}

void ComponentDescriptionWidget::setDescription(QString description) {
    ui.componentDescriptionItself->setPlainText(description);
}

void ComponentDescriptionWidget::setFileSuffix(QString suffix) {
    ui.suffixItself->setText(suffix);
}

void ComponentDescriptionWidget::setRepresentation(QString representation) {
    if (representation == "Image") {
        ui.representationImageRadioButton->setChecked(true);
    }
    else if (representation == "Mesh") {
        ui.representationMeshRadioButton->setChecked(true);
    }
    else {
        ui.representationNoneRadioButton->setChecked(true);
    }
}

QString ComponentDescriptionWidget::getComponentName() {
    return ui.componentNameItself->text();
}

QString ComponentDescriptionWidget::getComponentDescription() {
    return ui.componentDescriptionItself->toPlainText();
}

QString ComponentDescriptionWidget::getSuffix() {
    QString suffix = ui.suffixItself->text();
    return suffix;
}

QString ComponentDescriptionWidget::get3DRepresentation() {
    QString representation;
    if (ui.representationImageRadioButton->isChecked()) {
        representation = "Image";
    }
    else if (ui.representationMeshRadioButton->isChecked()) {
        representation = "Mesh";
    }
    else { // None
        representation = "None";
    }

    return representation;
}

void ComponentDescriptionWidget::setToDefault(QString componentExtensionName) {

    this->componentExtensionName = componentExtensionName;

    ui.componentNameItself->setText(defaultComponentName);
    ui.componentDescriptionItself->setPlainText(defaultComponentDescription);
    ui.suffixItself->setText(defaultComponentSuffix);

    ui.componentNameStar->setStyleSheet(normalStyle);
    ui.componentDescriptionStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);
    ui.representationNoneRadioButton->setChecked(true);

}

