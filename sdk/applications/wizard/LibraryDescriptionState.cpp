/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "LibraryDescriptionState.h"
#include "LibraryDescriptionWidget.h"
#include "WizardMainWindow.h"
#include "LibraryCreationState.h"

// includes from coreschema
#include <Library.hxx>

LibraryDescriptionState::LibraryDescriptionState(QString name, LibraryDescriptionWidget* widget, WizardMainWindow* mainWindow, LibraryCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domLibrary = nullptr;
}

void LibraryDescriptionState::resetDomLibrary(cepcoreschema::Library* domLibrary) {
    this->domLibrary = domLibrary;
    auto* libraryDescriptionWidget = dynamic_cast<LibraryDescriptionWidget*>(widget);
    if (libraryDescriptionWidget != nullptr) {
        libraryDescriptionWidget->setToDefault();
    }
}
void LibraryDescriptionState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* libraryDescriptionWidget = dynamic_cast<LibraryDescriptionWidget*>(widget);
    if (libraryDescriptionWidget != nullptr) {
        QString libraryName = libraryDescriptionWidget->getLibraryName();
        QString libraryDescription = libraryDescriptionWidget->getLibraryDescription();
        bool isStatic = libraryDescriptionWidget->getStatic();

        domLibrary->name(libraryName.toStdString());
        domLibrary->description(libraryDescription.toStdString());
        domLibrary->static_(isStatic);
    }
}
