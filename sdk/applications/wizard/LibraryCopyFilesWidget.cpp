/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "LibraryCopyFilesWidget.h"

#include "DefaultGUIText.h"

#include <Log.h>

// Qt files
#include <QFileDialog>

LibraryCopyFilesWidget::LibraryCopyFilesWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault();
}

void LibraryCopyFilesWidget::setToDefault() {
    ui.explanationLabel->setText(defaultCopyFilesExplanation);
    emptyFileNames();
}
void LibraryCopyFilesWidget::nextButtonClicked() {
    emit next();
}

void LibraryCopyFilesWidget::previousButtonClicked() {
    emit previous();
}

void LibraryCopyFilesWidget::cancelButtonClicked() {
    emit cancel();
}

void LibraryCopyFilesWidget::addFile(QString file) {
    QString toBeInserted = "<li>" + file + "</li>\n";
    int index = fileNamesString.lastIndexOf("</ul>");

    fileNamesString.insert(index, toBeInserted);
    ui.copiedFilesTextEdit->setText(fileNamesString);

}

void LibraryCopyFilesWidget::addFileClicked() {
    QStringList filesList = QFileDialog::getOpenFileNames(this, tr("Select one or more files"), "/home");
    foreach (QString fileName, filesList) {
        addLibraryName(fileName);
    }
}

QStringList LibraryCopyFilesWidget::getFileNames() {
    return filenames;
}

void LibraryCopyFilesWidget::emptyFileNames() {
    filenames.clear();
    fileNamesString = defaultCopiedFiles;
    ui.copiedFilesTextEdit->setText(fileNamesString);
}

void LibraryCopyFilesWidget::addLibraryName(QString libraryFileName) {
    this->filenames.append(libraryFileName);

    QString toBeInserted = "<li>" + libraryFileName + "</li>\n";
    int index = fileNamesString.lastIndexOf("</ul>");

    fileNamesString.insert(index, toBeInserted);
    ui.copiedFilesTextEdit->setText(fileNamesString);
}
