/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionClassificationWidget.h"

// Local files
#include "ActionTagWidget.h"
#include "DefaultGUIText.h"
// Qt files
#include <QMessageBox>

ActionClassificationWidget::ActionClassificationWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault();
}

void ActionClassificationWidget::setToDefault() {
    ui.actionFamilyItself->setText(defaultFamily);
    ui.actionFamilyStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);

    // Empty Tags
    foreach (ActionTagWidget* tag, list) {
        ui.actionTagsLayout->removeWidget(tag);
        if (tag != NULL) {
            delete tag;
        }
    }
    list.clear();
}

void ActionClassificationWidget::nextButtonClicked() {
    QString family = ui.actionFamilyItself->text();
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (family.toUtf8() != family.toLatin1()) {
        ui.actionFamilyStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if (isOneNonAsciiTag()) {
        ui.actionFamilyStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((family.isEmpty()) || (family == defaultFamily)) {
        ui.actionFamilyStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealFamily);
    }
    else {
        ui.actionFamilyStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);
        emit next();
    }
#else
    emit next();
#endif
}

void ActionClassificationWidget::previousButtonClicked() {
    emit previous();
}


void ActionClassificationWidget::cancelButtonClicked() {
    emit cancel();
}

bool ActionClassificationWidget::isOneNonAsciiTag() {
    bool oneNonAscii = false;
    QList<ActionTagWidget*>::const_iterator it  = list.begin();

    while ((it != list.end()) && (! oneNonAscii)) {
        QString tag = (*it)->getTag();
        if (tag.toUtf8() != tag.toLatin1()) {
            oneNonAscii = true;
        }
        it++;
    }
    return oneNonAscii;
}

void ActionClassificationWidget::addTagClicked() {
    auto* tag = new ActionTagWidget(this);
    ui.actionTagsLayout->addWidget(tag);
    list.append(tag);
}

QString ActionClassificationWidget::getFamily() {
    return ui.actionFamilyItself->text();
}

void ActionClassificationWidget::removeTag(ActionTagWidget* widget) {
    ui.actionTagsLayout->removeWidget(widget);
    list.removeAt(list.indexOf(widget));
    if (widget != nullptr) {
        delete widget;
    }
}

QList<ActionTagWidget*> ActionClassificationWidget::getTagWidgets() {
    return list;
}
