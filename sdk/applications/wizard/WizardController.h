/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef WIZARDCONTROLLER_H
#define WIZARDCONTROLLER_H

// Import from Qt
#include <QStateMachine>

class WizardMainWindow;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Cep;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Controller class, based on MVP architecture.
 *
 * The wizard is constructed on a state machine. \n
 * Each state is created in the constructor of this class and is connected with others trought this controller. \n
 * Moreover, this class creates the XML DOM document to generate the corresponding CEP. \n
 *
 */
class WizardController {

public:
    /** Constructor: creation of all the interfaces and the connections */
    WizardController();

    /** Destructor of the Controller */
    ~WizardController();

    /** Method to start */
    void launch();

private:
    QStateMachine machine;
    WizardMainWindow* mainWindow;

    /**
        XML DOM Element create by CodeSynthesis.
        This cep instance contains all of the informations of the xml file
        */
    cepcoreschema::Cep* domCep;

    QString  devDirectoryName;
};
#endif
