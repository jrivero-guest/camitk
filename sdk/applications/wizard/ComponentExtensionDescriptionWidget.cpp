/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentExtensionDescriptionWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

ComponentExtensionDescriptionWidget::ComponentExtensionDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ComponentExtensionDescriptionWidget::setToDefault() {
    ui.explanationLabel->setText(defaultComponentExtensionExplanation);
    ui.componentExtensionNameItself->setText(defaultComponentExtensionName);
    ui.componentExtensionDescriptionItself->setPlainText(defaultComponentExtensionDescription);
    ui.componentExtensionNameStar->setStyleSheet(normalStyle);
    ui.componentExtensionDescriptionStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);
}

void ComponentExtensionDescriptionWidget::cancelButtonClicked() {
    emit cancel();
}

void ComponentExtensionDescriptionWidget::nextButtonClicked() {
    QString componentExtensionName = ui.componentExtensionNameItself->text();
    QString componentExtensionDescription = ui.componentExtensionDescriptionItself->toPlainText();

#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (componentExtensionName.toUtf8() != componentExtensionName.toLatin1()) {
        ui.componentExtensionNameStar->setStyleSheet(enhancedStyle);
        ui.componentExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((componentExtensionName.isEmpty()) || (componentExtensionName == defaultComponentExtensionName)) {
        ui.componentExtensionNameStar->setStyleSheet(enhancedStyle);
        ui.componentExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealExtensionName);
    }
    else if (componentExtensionDescription.toUtf8() != componentExtensionDescription.toLatin1()) {
        ui.componentExtensionNameStar->setStyleSheet(normalStyle);
        ui.componentExtensionDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);

    }
    else if ((componentExtensionDescription.isEmpty()) || (componentExtensionDescription == defaultComponentExtensionDescription)) {
        ui.componentExtensionNameStar->setStyleSheet(normalStyle);
        ui.componentExtensionDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealExtensionDescription);
    }
    else {
        ui.componentExtensionNameStar->setStyleSheet(normalStyle);
        ui.componentExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);

        emit next();
    }
#else
    emit next();
#endif


}

QString ComponentExtensionDescriptionWidget::getComponentExtensionName() {
    return ui.componentExtensionNameItself->text();
}

QString ComponentExtensionDescriptionWidget::getComponentExtensionDescription() {
    return ui.componentExtensionDescriptionItself->toPlainText();
}


