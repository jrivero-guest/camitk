/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentSummaryWidget.h"

#include "DefaultGUIText.h"

ComponentSummaryWidget::ComponentSummaryWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ComponentSummaryWidget::setToDefault() {
    ui.summaryLabel->setText("");
}

void ComponentSummaryWidget::nextButtonClicked() {
    emit next();
}

void ComponentSummaryWidget::cancelButtonClicked() {
    emit cancel();
}

void ComponentSummaryWidget::previousButtonClicked() {
    emit previous();
}



void ComponentSummaryWidget::setSummary(QString name, QString description, QString representation, QStringList properties) {
    QString text = defaultComponentSummary;

    text = text.replace(QRegExp("@NAME@"), name);
    text = text.replace(QRegExp("@DESCRIPTION@"), description);
    text = text.replace(QRegExp("@REPRESENTATION@"), representation);

    QString parametersList = "";
    foreach (QString el, properties) {
        parametersList += "<li>" + el + "</li>\n";
    }
    text = text.replace(QRegExp("@PROPERTIES_LIST@"), parametersList);


    ui.componentSummaryLabel->setText(text);
}




