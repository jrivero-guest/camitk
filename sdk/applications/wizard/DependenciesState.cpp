/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "DependenciesState.h"
#include "DependenciesWidget.h"
#include "WizardMainWindow.h"
#include "ActionExtensionDependenciesState.h"

// Includes from cepgenerator
#include <ClassNameHandler.h>

// includes from coreschema
#include <Cep.hxx>
#include <Dependencies.hxx>
#include <Libraries.hxx>
#include <ComponentExtensions.hxx>
#include <ActionExtensions.hxx>

// temporary include
#include <iostream>


DependenciesState::DependenciesState(QString name, DependenciesWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, QState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domCep = domCep;
}

void DependenciesState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    auto* dependenciesWidget = dynamic_cast<DependenciesWidget*>(widget);
    if (dependenciesWidget != nullptr) {
        // Update existing Libraries
        QStringList cepLibraries;
        if (domCep->libraries().present()) {
            cepcoreschema::Libraries::library_sequence domLibrary = domCep->libraries().get().library();
            cepcoreschema::Libraries::library_const_iterator it;
            for (it = domLibrary.begin(); it != domLibrary.end(); it++) {
                cepLibraries << (*it).name().c_str();
            }
        }
        dependenciesWidget->updateCEPLibraries(cepLibraries);

        // Update existing Components
        QMap<QString, bool> cepComponents;
        if (domCep->componentExtensions().present()) {
            cepcoreschema::ComponentExtensions::componentExtension_sequence domComponents = domCep->componentExtensions().get().componentExtension();
            cepcoreschema::ComponentExtensions::componentExtension_iterator it;
            for (it = domComponents.begin(); it != domComponents.end(); it++) {
                QString compName((*it).name().c_str());
                bool compDepend = false;

                ActionExtensionDependenciesState* actionExtState =                     dynamic_cast<ActionExtensionDependenciesState*>(this);
                if (actionExtState) {
                    // check for that component if an action is working on it => if it is the case, comp is forced dependent
                    cepcoreschema::ActionExtension* domActionExtension = actionExtState->domActionExtension;
                    cepcoreschema::Actions::action_sequence domActions = domActionExtension->actions().action();
                    cepcoreschema::Actions::action_iterator ait;
                    for (ait = domActions.begin(); ait != domActions.end(); ait++) {
                        cepcoreschema::Action action = (*ait);
                        if (action.component() == compName.toStdString()) {
                            compDepend = true;
                        }
                    }
                }
                cepComponents.insert((*it).name().c_str(), compDepend);
            }
        }
        dependenciesWidget->updateCEPComponents(cepComponents);

        // Update existing Actions
        QStringList cepActions;
        if (domCep->actionExtensions().present()) {
            cepcoreschema::ActionExtensions::actionExtension_sequence domActions = domCep->actionExtensions().get().actionExtension();
            cepcoreschema::ActionExtensions::actionExtension_iterator it;
            for (it = domActions.begin(); it != domActions.end(); it++) {
                cepActions << (*it).name().c_str();
            }
        }
        dependenciesWidget->updateCEPActions(cepActions);

    }

}

cepcoreschema::Dependencies* DependenciesState::getDependencies() {
    cepcoreschema::Dependencies* domDependencies = new cepcoreschema::Dependencies();
    auto* dependenciesWidget = dynamic_cast<DependenciesWidget*>(widget);
    if (dependenciesWidget != nullptr) {
        // Get Internal CEP dependencies
        QStringList cepLibraries = dependenciesWidget->getCEPLibrariesDependencies();
        foreach (QString lib, cepLibraries) {
            QString libName = ClassNameHandler::getDirectoryName(lib);
            cepcoreschema::Dependency dep("cepLibrary", libName.toStdString());
            domDependencies->dependency().push_back(dep);
        }
        QStringList cepComponents = dependenciesWidget->getCEPComponentsDependencies();
        foreach (QString comp, cepComponents) {
            QString compName = ClassNameHandler::getDirectoryName(comp);
            cepcoreschema::Dependency dep("component", compName.toStdString());
            domDependencies->dependency().push_back(dep);
        }
        QStringList cepActions = dependenciesWidget->getCEPActionsDependencies();
        foreach (QString action, cepActions) {
            QString actionName = ClassNameHandler::getDirectoryName(action);
            cepcoreschema::Dependency dep("action", actionName.toStdString());
            domDependencies->dependency().push_back(dep);
        }

        // Get CamiTK dependencies
        QStringList camitkLibraries = dependenciesWidget->getCamiTKLibrariesDependencies();
        foreach (QString lib, camitkLibraries) {
            QString libName = ClassNameHandler::getDirectoryName(lib);
            cepcoreschema::Dependency dep("cepLibrary", libName.toStdString());
            domDependencies->dependency().push_back(dep);
        }
        QStringList camitkComponents = dependenciesWidget->getCamiTKComponentsDependencies();
        foreach (QString comp, camitkComponents) {
            QString compName = ClassNameHandler::getDirectoryName(comp);
            cepcoreschema::Dependency dep("component", compName.toStdString());
            domDependencies->dependency().push_back(dep);
        }
        QStringList camitkActions = dependenciesWidget->getCamiTKActionsDependencies();
        foreach (QString action, camitkActions) {
            QString actionName = ClassNameHandler::getDirectoryName(action);
            cepcoreschema::Dependency dep("action", actionName.toStdString());
            domDependencies->dependency().push_back(dep);
        }

        // Get Other libraries dependencies
        QStringList externalLibs = dependenciesWidget->getExternalLibsDependencies();
        foreach (QString lib, externalLibs) {
            cepcoreschema::Dependency dep("library", lib.toStdString());
            domDependencies->dependency().push_back(dep);
        }
    }
    return domDependencies;
}

void DependenciesState::onExit(QEvent* event) {
    WizardState::onExit(event);
}

