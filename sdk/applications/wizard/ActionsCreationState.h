/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONSCREATIONSTATE_H
#define ACTIONSCREATIONSTATE_H

#include "WizardState.h"

class ActionsCreationWidget;
class ActionExtensionCreationState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class ActionExtension;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create actions
 *
 */
class ActionsCreationState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ActionsCreationState(QString name, ActionsCreationWidget* widget, WizardMainWindow* mainWindow, ActionExtensionCreationState* parent);

    /**  Destructor */
    ~ActionsCreationState() override = default;

    void resetDomActionExtension(cepcoreschema::ActionExtension* domActionExtension);

protected:
    void onEntry(QEvent* event) override;
    void onExit(QEvent* event) override;

private:
    cepcoreschema::ActionExtension* domActionExtension;

};
#endif
