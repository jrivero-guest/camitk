/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ExtensionSummaryWidget.h"

#include "DefaultGUIText.h"

ExtensionSummaryWidget::ExtensionSummaryWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ExtensionSummaryWidget::setToDefault() {
    ui.extensionSummaryLabel->setText("");
}

void ExtensionSummaryWidget::nextButtonClicked() {
    emit next();
}

void ExtensionSummaryWidget::cancelButtonClicked() {
    emit cancel();
}

void ExtensionSummaryWidget::previousButtonClicked() {
    emit previous();
}


void ExtensionSummaryWidget::setElement(QString element) {
    this->element = element;

    QString text;
    // Title Bar
    text = ui.labelExtension->text();
    text.replace(QRegExp("@ELEMENT@"), element);
    ui.labelExtension->setText(text);

    text = ui.labelElements->text();
    text.replace(QRegExp("@ELEMENTDETAILS@"), element.left(element.indexOf(" ")));
    ui.labelElements->setText(text);

}

void ExtensionSummaryWidget::setSummary(QString name, QString description, QStringList elements, QStringList dependencies) {
    QString text = defaultExtensionSummary;

    text = text.replace(QRegExp("@ELEMENT@"), element);
    text = text.replace(QRegExp("@NAME@"), name);
    text = text.replace(QRegExp("@DESCRIPTION@"), description);

    QString elementsList = "";
    foreach (QString el, elements) {
        elementsList += "<li>" + el + "</li>\n";
    }
    text = text.replace(QRegExp("@ELEMENTS_LIST@"), elementsList);

    QString dependenciesList = "";
    foreach (QString dep, dependencies) {
        dependenciesList += "<li>" + dep + "</li>\n";
    }
    text = text.replace(QRegExp("@DEPENDENCIES_LIST@"), dependenciesList);

    ui.extensionSummaryLabel->setText(text);
}




