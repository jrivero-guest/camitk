/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef WIZARDSTATE_H
#define WIZARDSTATE_H

#include <QState>
#include <QWidget>

class WizardMainWindow;

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Generic state of the wizard.
 *
 * Inherited from QState. \n
 * A state is defined by its name, its widget and its mainWindow. It can be linked too to a state parent.
 *
 */
class  WizardState : public QState  {

    Q_OBJECT;

public:
    /**  Constructor */
    WizardState(QString name, QWidget* widget, WizardMainWindow* mainWidnow);
    /**  Constructor */
    WizardState(QString name, QWidget* widget, WizardMainWindow* mainWidnow, QState* parent);
    /**  Destructor */
    ~WizardState() override = default;

    QWidget* getWidget();
    QString getName();

protected:
    /// Reimplemented from QState
    /// @{
    void onEntry(QEvent* event) override;

    void onExit(QEvent* event) override;
    ///@}

    QString name;
    QWidget* widget;
    WizardMainWindow* mainWindow;
};
#endif
