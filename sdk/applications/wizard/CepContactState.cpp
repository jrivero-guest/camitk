/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepContactState.h"
#include "CepContactWidget.h"
#include "WizardMainWindow.h"

#include <Cep.hxx>
#include <Contact.hxx>
#include <Email.hxx>


CepContactState::CepContactState(QString name, CepContactWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep) : WizardState(name, widget, mainWindow) {
    this->domCep = domCep;
}

void CepContactState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
    auto* cepContactWidget = dynamic_cast<CepContactWidget*>(widget);
    if (cepContactWidget) {
        QString cepName = domCep->name().c_str();
        cepContactWidget->setCepName(cepName);
    }
}

void CepContactState::onExit(QEvent* event) {
    WizardState::onExit(event);

    auto* cepContactWidget = dynamic_cast<CepContactWidget*>(widget);
    if (cepContactWidget) {
        // Contact email(s)
        // TODO: be able to handle several mail addresses
        QString contactMail = cepContactWidget->getMail();
        cepcoreschema::Email theMail(contactMail.toStdString());
        cepcoreschema::Contact::email_sequence& mailSeq(domCep->contact().email());
        mailSeq.clear();
        mailSeq.push_back(theMail);

        // Licence
        if (cepContactWidget->hasUserLicence()) {
            domCep->copyright(cepContactWidget->getUserLicence().toStdString());
        }
    }

}



