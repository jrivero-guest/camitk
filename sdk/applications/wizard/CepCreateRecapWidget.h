/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef CEPCREATERECAPWIDGET_H
#define CEPCREATERECAPWIDGET_H

// Include GUI  automatically generated file
#include "ui_CepCreateRecapWidget.h"

// includes from Qt
#include <QWidget>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to summarize the creation of the CEP
 *
 */
class  CepCreateRecapWidget : public QWidget  {

    Q_OBJECT;

public:
    /**  Constructor */
    CepCreateRecapWidget(QWidget* parent);
    /**  Destructor */
    ~CepCreateRecapWidget() override = default;

    void setNameItself(QString name);
    void setDescriptionItself(QString description);
    void setContactItself(QString contact);

    void emptyExistingActionExtensions();
    void addActionExtension(QString actionExtensionName);

    void emptyExistingComponentExtensions();
    void addComponentExtension(QString componentExtensionName);

    void emptyExistingLibraries();
    void addLibrary(QString libraryNamme);

public slots:
    virtual void nextButtonClicked();
    virtual void addActionExtensionClicked();
    virtual void addComponentExtensionClicked();
    virtual void addLibrariesClicked();

signals:
    void next();
    void addLibrary();
    void addActionExtension();
    void addComponentExtension();

private:

    Ui::CepCreateRecapWidget ui;

    QString createdLibrariesString;
    QString createdActionsString;
    QString createdComponentsString;
};
#endif
