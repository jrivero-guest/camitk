/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "LibraryDependenciesState.h"
#include "WizardMainWindow.h"
#include "LibraryCreationState.h"
#include "DependenciesWidget.h"

// includes from coreschema
#include <Library.hxx>


LibraryDependenciesState::LibraryDependenciesState(QString name, DependenciesWidget* widget, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep, LibraryCreationState* parent)
    : DependenciesState(name, widget, mainWindow, domCep, parent) {
    this->domLibrary = nullptr;
    widget->setElement("Library");
}

void LibraryDependenciesState::resetDomLibrary(cepcoreschema::Library* domLibrary) {
    this->domLibrary = domLibrary;

}

void LibraryDependenciesState::onExit(QEvent* event) {
    DependenciesState::onExit(event);
    cepcoreschema::Dependencies* domDeps = getDependencies();
    if (domDeps != nullptr) {
        if (domDeps->dependency().size() > 0) {
            domLibrary->dependencies((*domDeps));
        }
    }

}

