/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef LIBRARYCREATIONSTATE_H
#define LIBRARYCREATIONSTATE_H

#include <QState>
#include <QWidget>

class WizardMainWindow;
// Sub States
class LibraryDescriptionWidget;
class LibraryDescriptionState;
class LibraryCopyFilesWidget;
class LibraryCopyFilesState;
class DependenciesWidget;
class LibraryDependenciesState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Cep;
class Library;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create library
 *
 * This state manages sub-state to create one library.
 *
 */
class  LibraryCreationState : public QState  {

    Q_OBJECT;

public:
    /**  Constructor */
    LibraryCreationState(QString name, WizardMainWindow* mainWidnow, cepcoreschema::Cep* domCep, QMap<QString, QStringList>* libraryFilesMap);
    /**  Destructor */
    ~LibraryCreationState() override = default;

signals:
    void next();

public slots:
    virtual void extensionFinished();
    virtual void extensionCancelled();

protected:
    /// Reimplemented from QState
    /// @{
    void onEntry(QEvent* event) override;

    void onExit(QEvent* event) override;
    ///@}

    // Sub States
    LibraryDescriptionWidget* libraryDescriptionWidget;
    LibraryDescriptionState*   libraryDescriptionState;
    LibraryCopyFilesWidget*    libraryCopyFilesWidget;
    LibraryCopyFilesState*     libraryCopyFilesState;
    DependenciesWidget*        libraryDependenciesWidget;
    LibraryDependenciesState* libraryDependenciesState;

private:

    bool cancelled;

    void createSubStates(WizardMainWindow* mainWindow);

    QString name;
    cepcoreschema::Cep* domCep;
    cepcoreschema::Library* domLibrary;

    QMap<QString, QStringList>* libraryFilesMap;

};
#endif
