/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONCLASSIFICATIONSTATE_H
#define ACTIONCLASSIFICATIONSTATE_H

#include "WizardState.h"

class ActionClassificationWidget;
class ActionCreationState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class Action;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to class the action and to attribute a tag to it.
 *
 */
class ActionClassificationState : public WizardState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ActionClassificationState(QString name, ActionClassificationWidget* widget, WizardMainWindow* mainWindow, ActionCreationState* parent);

    /**  Destructor */
    ~ActionClassificationState() override = default;

    void resetDomAction(cepcoreschema::Action* domAction);

protected:
    void onExit(QEvent* event) override;
    void onEntry(QEvent* event) override;

private:
    cepcoreschema::Action* domAction;

};
#endif
