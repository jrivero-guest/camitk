/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionAddParameterWidget.h"

// local files
#include "ActionParameterWidget.h"
#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

ActionAddParameterWidget::ActionAddParameterWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault();
}

void ActionAddParameterWidget::nextButtonClicked() {
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (isOneNonAsciiParameter()) {
        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if (isOneNoName()) {
        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultParamNoName);
    }
    else {
        emit next();
    }
#else
    emit next();
#endif
}

void ActionAddParameterWidget::previousButtonClicked() {
    emit previous();
}

void ActionAddParameterWidget::cancelButtonClicked() {
    emit cancel();
}

bool ActionAddParameterWidget::isOneNoName() {
    bool oneNoName = false;
    QList<ActionParameterWidget*>::const_iterator it = list.begin();
    while ((it != list.end()) && (! oneNoName)) {
        QString paramName = (*it)->getName();
        if (paramName.isEmpty()) {
            (*it)->setRequiredName();
            oneNoName = true;
        }
        else {
            (*it)->unsetRequiredName();
        }
        it++;
    }

    return oneNoName;
}

bool ActionAddParameterWidget::isOneNonAsciiParameter() {
    bool oneNonAscii = false;
    QList<ActionParameterWidget*>::const_iterator it = list.begin();
    while ((it != list.end()) && (! oneNonAscii)) {
        QString paramName = (*it)->getName();
        QString paramType = (*it)->getType();
        QString paramDescription = (*it)->getDescription();
        QString paramDefaultValue = (*it)->getDefaultValue();

        if ((paramName.toUtf8() != paramName.toLatin1()) ||
                (paramType.toUtf8() != paramType.toLatin1()) ||
                (paramDescription.toUtf8() != paramDescription.toLatin1()) ||
                (paramDefaultValue.toUtf8() != paramDefaultValue.toLatin1())) {
            oneNonAscii = true;
        }
        it++;
    }
    return oneNonAscii;
}

void ActionAddParameterWidget::setToDefault() {
    // Remove existing parameters
    foreach (ActionParameterWidget* widget, list) {
        ui.parametersLayout->removeWidget(widget);
        if (widget != NULL) {
            delete widget;
        }
    }
    list.clear();

}

void ActionAddParameterWidget::addParameterClicked() {
    auto* parameter = new ActionParameterWidget(this);
    ui.parametersLayout->addWidget(parameter);
    list.append(parameter);
}

void ActionAddParameterWidget::removeParameter(ActionParameterWidget* widget) {
    ui.parametersLayout->removeWidget(widget);
    list.removeAt(list.indexOf(widget));
    if (widget != nullptr) {
        delete widget;
    }
}

QList<ActionParameterWidget*> ActionAddParameterWidget::getActionParameterWidgets() {
    return list;
}

