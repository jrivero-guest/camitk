/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "LibraryDescriptionWidget.h"
#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

LibraryDescriptionWidget::LibraryDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    setToDefault();
}

void LibraryDescriptionWidget::setToDefault() {
    ui.explanationLabel->setText(defaultLibraryExplanation);
    ui.libraryNameItself->setText(defaultLibraryName);
    ui.libraryDescriptionItself->setPlainText(defaultLibraryDescription);
}

void LibraryDescriptionWidget::nextButtonClicked() {
    QString libraryName = ui.libraryNameItself->text();
    QString libraryDescription = ui.libraryDescriptionItself->toPlainText();

#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (libraryName.toUtf8() != libraryName.toLatin1()) {
        ui.libraryNameStar->setStyleSheet(enhancedStyle);
        ui.libraryDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }

    else if ((libraryName.isEmpty()) || (libraryName == defaultLibraryName)) {
        ui.libraryNameStar->setStyleSheet(enhancedStyle);
        ui.libraryDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealLibraryName);
    }
    else if (libraryDescription.toUtf8() != libraryDescription.toLatin1()) {
        ui.libraryNameStar->setStyleSheet(normalStyle);
        ui.libraryDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }

    else if ((libraryDescription.isEmpty()) || (libraryDescription == defaultLibraryName)) {
        ui.libraryNameStar->setStyleSheet(normalStyle);
        ui.libraryDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealLibraryDescription);
    }
    else {
        ui.libraryNameStar->setStyleSheet(normalStyle);
        ui.libraryDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);

        emit next();
    }
#else
    emit next();
#endif
}

void LibraryDescriptionWidget::setLibraryName(QString name) {
    ui.libraryNameItself->setText(name);
}

void LibraryDescriptionWidget::setLibraryDescription(QString description) {
    ui.libraryDescriptionItself->setPlainText(description);
}

void LibraryDescriptionWidget::setStatic(bool isStatic) {
    if (isStatic) {
        ui.staticRadioButton->setChecked(true);
    }
    else {
        ui.dynamicRadioButton->setChecked(true);
    }
}

void LibraryDescriptionWidget::cancelButtonClicked() {
    ui.libraryNameStar->setStyleSheet(normalStyle);
    ui.libraryDescriptionStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);
    emit cancel();
}

QString LibraryDescriptionWidget::getLibraryName() {
    return ui.libraryNameItself->text();
}

QString LibraryDescriptionWidget::getLibraryDescription() {
    return ui.libraryDescriptionItself->toPlainText();
}

bool LibraryDescriptionWidget::getStatic() {
    return ui.staticRadioButton->isChecked();
}

