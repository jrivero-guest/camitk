/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionExtensionDescriptionWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>


ActionExtensionDescriptionWidget::ActionExtensionDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ActionExtensionDescriptionWidget::cancelButtonClicked() {
    emit cancel();
}

void ActionExtensionDescriptionWidget::nextButtonClicked() {
    QString actionExtensionName = ui.actionExtensionNameItself->text();
    QString actionExtensionDescription = ui.actionExtensionDescriptionItself->toPlainText();

#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (actionExtensionName.toUtf8() != actionExtensionName.toLatin1()) {
        ui.actionExtensionNameStar->setStyleSheet(enhancedStyle);
        ui.actionExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((actionExtensionName.isEmpty()) || (actionExtensionName == defaultActionExtensionName)) {
        ui.actionExtensionNameStar->setStyleSheet(enhancedStyle);
        ui.actionExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealExtensionName);
    }
    else if (actionExtensionDescription.toUtf8() != actionExtensionDescription.toLatin1()) {
        ui.actionExtensionNameStar->setStyleSheet(normalStyle);
        ui.actionExtensionDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if ((actionExtensionDescription.isEmpty()) || (actionExtensionDescription == defaultActionExtensionDescription)) {
        ui.actionExtensionNameStar->setStyleSheet(normalStyle);
        ui.actionExtensionDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealExtensionDescription);
    }
    else {
        ui.actionExtensionNameStar->setStyleSheet(normalStyle);
        ui.actionExtensionDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);
        emit next();
    }
#else
    emit next();
#endif
}

void ActionExtensionDescriptionWidget::setToDefault() {
    ui.explanationLabel->setText(defaultActionExtensionExplanation);
    ui.actionExtensionNameItself->setText(defaultActionExtensionName);
    ui.actionExtensionDescriptionItself->setPlainText(defaultActionExtensionDescription);
    ui.actionExtensionNameStar->setStyleSheet(normalStyle);
    ui.actionExtensionDescriptionStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);
}

QString ActionExtensionDescriptionWidget::getActionExtensionName() {
    return ui.actionExtensionNameItself->text();
}

QString ActionExtensionDescriptionWidget::getCepDescription() {
    return ui.actionExtensionDescriptionItself->toPlainText();
}


