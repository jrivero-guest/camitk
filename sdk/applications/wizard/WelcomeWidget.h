/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef WELCOMEWIDGET_H
#define WELCOMEWIDGET_H

// Include GUI  automatically generated file
#include "ui_WelcomeWidget.h"

// includes from Qt
#include <QWidget>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Class linked with welcome interface which allows user to choose what he wants to create ( action, component etc..)
 *
 */
class  WelcomeWidget : public QWidget  {

    Q_OBJECT;

public:
    WelcomeWidget(QWidget* parent);
    ~WelcomeWidget() override = default;

public slots:
    virtual void nextButtonClicked();

signals:
    void next();

private:

    Ui::WelcomeWidget ui;
};
#endif
