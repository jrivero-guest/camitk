/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONDESCRIPTIONWIDGET_H
#define ACTIONDESCRIPTIONWIDGET_H

// Include GUI  automatically generated file
#include "ui_ActionDescriptionWidget.h"

// includes from Qt
#include <QWidget>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to describe action
 *
 */
class  ActionDescriptionWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor * */
    ActionDescriptionWidget(QWidget* parent);

    /**  Destructor */
    ~ActionDescriptionWidget() override = default;

    void setToDefault(QString actionExtensionName);
    void addPossibleComponent(QString possibleComponent);

    void setName(QString name);
    void setDescription(QString description);
    void setComponent(QString component);

    QString getActionName();
    QString getActionDescription();
    QString getComponentName();
    bool isItkFilter();
    QString getOutputType();

public slots:
    virtual void nextButtonClicked();
    virtual void cancelButtonClicked();
    virtual void componentChanged(QString);
    virtual void itkFilterClicked(bool);

signals:
    void next();
    void cancel();

private:
    QStringList possibleComponents;
    Ui::ActionDescriptionWidget ui;

    QString actionExtensionName;
};
#endif
