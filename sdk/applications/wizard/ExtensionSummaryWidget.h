/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef EXTENSIONSUMMARYWIDGET_H
#define EXTENSIONSUMMARYWIDGET_H

// Include GUI  automatically generated file
#include "ui_ExtensionSummaryWidget.h"

// includes from Qt
#include <QWidget>
#include <QStringList>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to summarize the extension
 *
 */
class  ExtensionSummaryWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    ExtensionSummaryWidget(QWidget* parent);
    /**  Destructor */
    ~ExtensionSummaryWidget() override = default;

    void setToDefault();
    void setElement(QString element);
    void setSummary(QString name, QString description, QStringList elements, QStringList dependencies);

public slots:
    virtual void nextButtonClicked();
    virtual void cancelButtonClicked();
    virtual void previousButtonClicked();

signals:
    void next();
    void previous();
    void cancel();

private:
    Ui::ExtensionSummaryWidget ui;

    QString element;

};
#endif
