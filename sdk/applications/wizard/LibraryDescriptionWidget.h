/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef LIBRARYDESCRIPTIONWIDGET_H
#define LIBRARYDESCRIPTIONWIDGET_H

// Include GUI  automatically generated file
#include "ui_LibraryDescriptionWidget.h"

// includes from Qt
#include <QWidget>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to describe a library.
 *
 */
class  LibraryDescriptionWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    LibraryDescriptionWidget(QWidget* parent);
    /**  Destructor */
    ~LibraryDescriptionWidget() override = default;

    void setToDefault();

    void setLibraryName(QString name);
    void setLibraryDescription(QString description);
    void setStatic(bool isStatic);

    QString getLibraryName();
    QString getLibraryDescription();
    bool getStatic();

public slots:
    virtual void nextButtonClicked();
    virtual void cancelButtonClicked();

signals:
    void next();
    void cancel();

private:

    Ui::LibraryDescriptionWidget ui;
};
#endif
