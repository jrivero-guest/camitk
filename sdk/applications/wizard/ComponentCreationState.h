/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef COMPONENTCREATIONSTATE_H
#define COMPONENTCREATIONSTATE_H

#include <QState>
#include <QWidget>

class WizardMainWindow;
class ComponentExtensionCreationState;

// Sub-States
class ComponentDescriptionWidget;
class ComponentDescriptionState;
class ComponentAddPropertyWidget;
class ComponentAddPropertyState;
class ComponentSummaryWidget;
class ComponentSummaryState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class ComponentExtension;
class Component;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create one component
 *
 * This state manages sub-state to create one component.
 *
 */
class  ComponentCreationState : public QState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ComponentCreationState(QString name, WizardMainWindow* mainWidnow, ComponentExtensionCreationState* parent);
    /**  Destructor */
    ~ComponentCreationState() override = default;

    void resetDomComponentExtension(cepcoreschema::ComponentExtension* domComponentExtension);

signals:
    void nextCCS();

public slots:
    virtual void componentFinished();
    virtual void componentCancelled();

protected:
    /// Reimplemented from QState
    /// @{
    void onEntry(QEvent* event) override;

    void onExit(QEvent* event) override;
    ///@}

    // Substates (to be updated with domComponent at each entry)
    ComponentDescriptionWidget* componentDescriptionWidget;
    ComponentDescriptionState*   componentDescriptionState;
    ComponentAddPropertyWidget* componentAddPropertyWidget;
    ComponentAddPropertyState*   componentAddPropertyState;
    ComponentSummaryWidget*      componentSummaryWidget;
    ComponentSummaryState*       componentSummaryState;

private:
    void createSubStates(WizardMainWindow* mainWindow);

    bool cancelled;
    QString name;
    cepcoreschema::ComponentExtension* domComponentExtension;
    cepcoreschema::Component* domComponent;

};
#endif
