/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionSummaryWidget.h"

#include "DefaultGUIText.h"

ActionSummaryWidget::ActionSummaryWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
}

void ActionSummaryWidget::setToDefault() {
    ui.summaryLabel->setText("");
}

void ActionSummaryWidget::nextButtonClicked() {
    emit next();
}

void ActionSummaryWidget::cancelButtonClicked() {
    emit cancel();
}

void ActionSummaryWidget::previousButtonClicked() {
    emit previous();
}



void ActionSummaryWidget::setSummary(QString name, QString description, QString component,
                                     QStringList parameters, QString family, QStringList tags) {
    QString text = defaultActionSummary;

    text = text.replace(QRegExp("@NAME@"), name);
    text = text.replace(QRegExp("@DESCRIPTION@"), description);
    text = text.replace(QRegExp("@COMPONENT@"), component);
    text = text.replace(QRegExp("@FAMILY@"), family);

    QString parametersList = "";
    foreach (QString el, parameters) {
        parametersList += "<li>" + el + "</li>\n";
    }
    text = text.replace(QRegExp("@PARAMETERS_LIST@"), parametersList);

    QString tagsList = "";
    foreach (QString dep, tags) {
        tagsList += "<li>" + dep + "</li>\n";
    }
    text = text.replace(QRegExp("@TAGS_LIST@"), tagsList);


    ui.actionSummaryLabel->setText(text);
}




