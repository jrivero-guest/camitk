/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONCREATIONSTATE_H
#define ACTIONCREATIONSTATE_H

#include <QState>
#include <QWidget>

class WizardMainWindow;
class ActionExtensionCreationState;

// Sub-states
class ActionDescriptionWidget;
class ActionDescriptionState;
class ActionAddParameterWidget;
class ActionAddParameterState;
class ActionClassificationWidget;
class ActionClassificationState;
class ActionSummaryWidget;
class ActionSummaryState;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class ActionExtension;
class Action;
class Cep;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to create one action.
 *
 * This state manages sub-state to create one action.
 *
 */
class  ActionCreationState : public QState  {

    Q_OBJECT;

public:
    /**  Constructor */
    ActionCreationState(QString name, WizardMainWindow* mainWidnow, cepcoreschema::Cep* domCep, ActionExtensionCreationState* parent);

    /**  Destructor */
    ~ActionCreationState() override = default;

    void resetDomActionExtension(cepcoreschema::ActionExtension* domActionExtension);

signals:
    void nextACS();

public slots:
    virtual void actionFinished();
    virtual void actionCancelled();

protected:
    /// Reimplemented from QState
    /// @{
    void onEntry(QEvent* event) override;

    void onExit(QEvent* event) override;
    ///@}

    /// Substates (to be updated with domAction at each entry)
    ActionDescriptionWidget*     actionDescriptionWidget;
    ActionDescriptionState*      actionDescriptionState;
    ActionAddParameterWidget*    actionAddParameterWidget;
    ActionAddParameterState*     actionAddParameterState;
    ActionClassificationWidget* actionClassificationWidget;
    ActionClassificationState*   actionClassificationState;
    ActionSummaryWidget*         actionSummaryWidget;
    ActionSummaryState*          actionSummaryState;


private:
    void createSubStates(WizardMainWindow* mainWindow);

    bool cancelled;
    QString name;
    cepcoreschema::ActionExtension* domActionExtension;
    cepcoreschema::Action* domAction;
    cepcoreschema::Cep* domCep;

};
#endif
