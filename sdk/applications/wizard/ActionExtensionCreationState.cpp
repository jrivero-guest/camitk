/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionExtensionCreationState.h"
#include "WizardMainWindow.h"

#include "ActionExtensionDescriptionWidget.h"
#include "ActionExtensionDescriptionState.h"
#include "ActionsCreationWidget.h"
#include "ActionsCreationState.h"
#include "ActionCreationState.h"
#include "ActionExtensionDependenciesState.h"
#include "DependenciesWidget.h"
#include "ExtensionSummaryWidget.h"
#include "ExtensionSummaryState.h"

#include <Cep.hxx>
#include <Actions.hxx>
#include <ActionExtension.hxx>

ActionExtensionCreationState::ActionExtensionCreationState(QString name, WizardMainWindow* mainWindow, cepcoreschema::Cep* domCep) : QState() {
    this->cancelled = false;
    this->name = name;
    this->domCep = domCep;
    this->domActionExtension = nullptr;

    createSubStates(mainWindow);
}

void ActionExtensionCreationState::onEntry(QEvent* event) {
    this->cancelled = false;
    if (this->domActionExtension != nullptr) {
        delete domActionExtension;
        domActionExtension = nullptr;
    }

    cepcoreschema::Actions theActions;
    this->domActionExtension = new cepcoreschema::ActionExtension("An Action Extension", "An action extension description", theActions);

    actionExtensionDescriptionState->resetDomActionExtension(domActionExtension);
    actionsCreationState->resetDomActionExtension(domActionExtension);
    actionCreationState->resetDomActionExtension(domActionExtension);
    actionExtensionDependenciesState->resetDomActionExtension(domActionExtension);
    actionExtensionSummaryState->setActionExtension(domActionExtension);
}

void ActionExtensionCreationState::onExit(QEvent* event) {
    if (! cancelled) {
        cepcoreschema::Actions someActions;
        if (domCep->actionExtensions().present()) {
            domCep->actionExtensions().get().actionExtension().push_back((*domActionExtension));
        }
        else {
            cepcoreschema::ActionExtensions theExtensions;
            theExtensions.actionExtension().push_back((*domActionExtension));
            domCep->actionExtensions(theExtensions);
        }
    }
    else {
        if (domActionExtension != nullptr) {
            delete domActionExtension;
            domActionExtension = nullptr;
        }
    }

}


void ActionExtensionCreationState::extensionFinished() {
    cancelled = false;
    emit next();
}

void ActionExtensionCreationState::extensionCancelled() {
    cancelled = true;
    emit next();
}

void ActionExtensionCreationState::createSubStates(WizardMainWindow* mainWindow) {
    actionExtensionDescriptionWidget = new ActionExtensionDescriptionWidget(nullptr);
    actionExtensionDescriptionState = new ActionExtensionDescriptionState("Action Extension",  actionExtensionDescriptionWidget, mainWindow, this);

    actionsCreationWidget = new ActionsCreationWidget(nullptr);
    actionsCreationState = new ActionsCreationState("Actions Creation",  actionsCreationWidget, mainWindow, this);

    actionCreationState = new ActionCreationState("Action Creation", mainWindow, domCep, this);

    actionExtensionDependenciesWidget = new DependenciesWidget(nullptr);
    actionExtensionDependenciesState = new ActionExtensionDependenciesState("Action Extension Dependencies",  actionExtensionDependenciesWidget, mainWindow, domCep, this);

    actionExtensionSummaryWidget = new ExtensionSummaryWidget(nullptr);
    actionExtensionSummaryState = new ExtensionSummaryState("Action Extension Summary", actionExtensionSummaryWidget, "Action", mainWindow, this);


    actionExtensionDescriptionState->addTransition(actionExtensionDescriptionWidget, SIGNAL(next()), actionsCreationState);
    actionsCreationState->addTransition(actionsCreationWidget, SIGNAL(newAction()), actionCreationState);
    actionsCreationState->addTransition(actionsCreationWidget, SIGNAL(next()), actionExtensionDependenciesState);
    actionsCreationState->addTransition(actionsCreationWidget, SIGNAL(previous()), actionExtensionDescriptionState);

    actionCreationState->addTransition(actionCreationState, SIGNAL(nextACS()), actionsCreationState);

    actionExtensionDependenciesState->addTransition(actionExtensionDependenciesWidget, SIGNAL(previous()), actionsCreationState);
    actionExtensionDependenciesState->addTransition(actionExtensionDependenciesWidget, SIGNAL(next()), actionExtensionSummaryState);

    actionExtensionSummaryState->addTransition(actionExtensionSummaryWidget, SIGNAL(previous()), actionExtensionDependenciesState);
    QObject::connect(actionExtensionSummaryWidget, SIGNAL(next()), this, SLOT(extensionFinished()));

    // Cancel
    QObject::connect(actionExtensionDescriptionWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(actionsCreationWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(actionExtensionDependenciesWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));
    QObject::connect(actionExtensionSummaryWidget, SIGNAL(cancel()), this, SLOT(extensionCancelled()));

    this->setInitialState(actionExtensionDescriptionState);
}
