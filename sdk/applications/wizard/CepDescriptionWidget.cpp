/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "CepDescriptionWidget.h"

#include "DefaultGUIText.h"

// Qt files
#include <QMessageBox>

CepDescriptionWidget::CepDescriptionWidget(QWidget* parent) : QWidget(parent) {
    ui.setupUi(this);
    ui.cepNameLineEdit->setText(defaultCepName);
    ui.cepDescriptionTextEdit->setHtml(defaultCepDescription);
}

void CepDescriptionWidget::nextButtonClicked() {
    QString cepName = ui.cepNameLineEdit->text();

    QString cepDescription = ui.cepDescriptionTextEdit->toPlainText();
#ifndef _WIZARD_QUESTIONS_SQUEEZE
    if (cepName.isEmpty() || cepName == defaultCepName) {
        ui.cepDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.cepNameStar->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealCepName);
    }
    else if (cepName.toUtf8() != cepName.toLatin1()) {
        ui.cepDescriptionStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);
        ui.cepNameStar->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else if (cepDescription.isEmpty() || cepDescription.contains("should be replaced")) {
        ui.cepNameStar->setStyleSheet(normalStyle);
        ui.cepDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultRealCepDescription);
    }
    else if (cepDescription.toUtf8() != cepDescription.toLatin1()) {
        ui.cepNameStar->setStyleSheet(normalStyle);
        ui.cepDescriptionStar->setStyleSheet(enhancedStyle);
        ui.requiredLabel->setStyleSheet(enhancedStyle);

        // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING in the wizard
        QMessageBox::warning(this, defaultBeforeGoingFurther, defaultAscii);
    }
    else {
        ui.cepNameStar->setStyleSheet(normalStyle);
        ui.cepNameStar->setStyleSheet(normalStyle);
        ui.requiredLabel->setStyleSheet(normalStyle);
        emit next();
    }
#else
    emit next();
#endif

}

void CepDescriptionWidget::previousButtonClicked() {
    ui.cepNameStar->setStyleSheet(normalStyle);
    ui.cepDescriptionStar->setStyleSheet(normalStyle);
    ui.requiredLabel->setStyleSheet(normalStyle);
    emit previous();
}


QString CepDescriptionWidget::getCepName() {
    QString cepName = ui.cepNameLineEdit->text();
    return cepName;
}

QString CepDescriptionWidget::getCepDescription() {
    QString cepDescription = ui.cepDescriptionTextEdit->toPlainText();
    return cepDescription;
}
