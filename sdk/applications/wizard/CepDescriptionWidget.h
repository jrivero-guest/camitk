/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef CEPDESCRIPTIONWIDGET_H
#define CEPDESCRIPTIONWIDGET_H

// Include GUI  automatically generated file
#include "ui_CepDescriptionWidget.h"

// includes from Qt
#include <QWidget>

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to describe the CEP
 *
 */
class  CepDescriptionWidget : public QWidget  {

    Q_OBJECT;

public:
    /**  Constructor */
    CepDescriptionWidget(QWidget* parent);

    /**  Destructor */
    ~CepDescriptionWidget() override = default;

    QString getCepName();
    QString getCepDescription();

public slots:
    virtual void nextButtonClicked();
    virtual void previousButtonClicked();

signals:
    void next();
    void previous();

private:

    Ui::CepDescriptionWidget ui;
};
#endif
