/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentsCreationState.h"
#include "ComponentsCreationWidget.h"
#include "WizardMainWindow.h"
#include "ComponentExtensionCreationState.h"

// includes from coreschema
#include <ComponentExtension.hxx>

/**  Constructor */
ComponentsCreationState::ComponentsCreationState(QString name, ComponentsCreationWidget* widget, WizardMainWindow* mainWindow, ComponentExtensionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domComponentExtension = nullptr;
}

void ComponentsCreationState::resetDomComponentExtension(cepcoreschema::ComponentExtension* domComponentExtension) {
    this->domComponentExtension = domComponentExtension;
    auto* componentsCreationWidget = dynamic_cast<ComponentsCreationWidget*>(widget);
    if (componentsCreationWidget != nullptr) {
        componentsCreationWidget->setToDefault();
    }
}

void ComponentsCreationState::onEntry(QEvent* event) {
    WizardState::onEntry(event);

    auto* componentsCreationWidget = dynamic_cast<ComponentsCreationWidget*>(widget);
    if (componentsCreationWidget != nullptr) {
        QString componentExtensionName = domComponentExtension->name().c_str();
        componentsCreationWidget->setGroupBoxTitle(componentExtensionName);

        componentsCreationWidget->emptyExistingComponents();
        cepcoreschema::Components theComponents = domComponentExtension->components();
        cepcoreschema::Components::component_iterator it;
        for (it = theComponents.component().begin(); it != theComponents.component().end(); it++) {
            QString componentName = (*it).name().c_str();
            componentsCreationWidget->addComponentName(componentName);
        }
    }
}

void ComponentsCreationState::onExit(QEvent* event) {
    WizardState::onExit(event);
}
