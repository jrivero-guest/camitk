/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ACTIONADDPARAMETERWIDGET_H
#define ACTIONADDPARAMETERWIDGET_H

// Include GUI  automatically generated file
#include "ui_ActionAddParameterWidget.h"

// includes from Qt
#include <QWidget>
#include <QList>

class ActionParameterWidget;

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * Widget to add or remove parameters to the action
 *
 * The widget manages a list of ActionParameterWidget to define each parameter.
 *
 */
class  ActionAddParameterWidget : public QWidget {

    Q_OBJECT;

public:
    /**  Constructor */
    ActionAddParameterWidget(QWidget* parent);

    /**  Destructor */
    ~ActionAddParameterWidget() override = default;

    void setToDefault();

    void removeParameter(ActionParameterWidget* widget);
    QList<ActionParameterWidget*> getActionParameterWidgets();


public slots:
    virtual void nextButtonClicked();
    virtual void previousButtonClicked();
    virtual void cancelButtonClicked();
    virtual void addParameterClicked();

signals:
    void next();
    void previous();
    void cancel();

private:
    bool isOneNoName();
    bool isOneNonAsciiParameter();

    Ui::ActionAddParameterWidget ui;
    QList<ActionParameterWidget*> list;
};
#endif
