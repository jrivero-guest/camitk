/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef EXTENSIONSUMMARYSTATE_H
#define EXTENSIONSUMMARYSTATE_H

#include "WizardState.h"

class ExtensionSummaryWidget;

// Dependency from cepcoreschema
// Declaration here to avoid declaration in dependant projects.
namespace cepcoreschema {
class ActionExtension;
class ComponentExtension;
}

/**
 * @ingroup group_sdk_application_wizard
 *
 * @brief
 * State to summarize extension
 *
 */
class ExtensionSummaryState : public WizardState  {

    Q_OBJECT;

public:
    /** Constructor*/
    ExtensionSummaryState(QString name, ExtensionSummaryWidget* widget, QString type, WizardMainWindow* mainWindow, QState* parent);

    /** Destructor*/
    ~ExtensionSummaryState() override = default;

    void setActionExtension(cepcoreschema::ActionExtension* domActionExtension);
    void setComponentExtension(cepcoreschema::ComponentExtension* domComponentExtension);

protected:
    void onEntry(QEvent* event) override;
    void onExit(QEvent* event) override;

    QString type;

    cepcoreschema::ActionExtension* domActionExtension;
    cepcoreschema::ComponentExtension* domComponentExtension;

};
#endif
