/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionExtensionDescriptionState.h"
#include "ActionExtensionDescriptionWidget.h"
#include "WizardMainWindow.h"
#include "ActionExtensionCreationState.h"

// includes from coreschema
#include <ActionExtension.hxx>


ActionExtensionDescriptionState::ActionExtensionDescriptionState(QString name, ActionExtensionDescriptionWidget* widget, WizardMainWindow* mainWindow, ActionExtensionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domActionExtension = nullptr;
}

void ActionExtensionDescriptionState::resetDomActionExtension(cepcoreschema::ActionExtension* domActionExtension) {
    this->domActionExtension = domActionExtension;
    auto* actionExtensionDescriptionWidget = dynamic_cast<ActionExtensionDescriptionWidget*>(widget);
    if (actionExtensionDescriptionWidget != nullptr) {
        actionExtensionDescriptionWidget->setToDefault();
    }
}

void ActionExtensionDescriptionState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* actionExtensionDescriptionWidget = dynamic_cast<ActionExtensionDescriptionWidget*>(widget);
    if (actionExtensionDescriptionWidget != nullptr) {
        QString actionExtensionName = actionExtensionDescriptionWidget->getActionExtensionName();
        QString actionExtensionDescription = actionExtensionDescriptionWidget->getCepDescription();
        domActionExtension->name(actionExtensionName.toStdString());
        domActionExtension->description(actionExtensionDescription.toStdString());
    }
}
