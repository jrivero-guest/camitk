/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ComponentExtensionDescriptionState.h"
#include "ComponentExtensionDescriptionWidget.h"
#include "WizardMainWindow.h"
#include "ComponentExtensionCreationState.h"

// includes from coreschema
#include <ComponentExtension.hxx>


ComponentExtensionDescriptionState::ComponentExtensionDescriptionState(QString name, ComponentExtensionDescriptionWidget* widget, WizardMainWindow* mainWindow, ComponentExtensionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domComponentExtension = nullptr;
}

void ComponentExtensionDescriptionState::resetDomComponentExtension(cepcoreschema::ComponentExtension* domComponentExtension) {
    this->domComponentExtension = domComponentExtension;
    auto* componentExtensionDescriptionWidget = dynamic_cast<ComponentExtensionDescriptionWidget*>(widget);
    if (componentExtensionDescriptionWidget != nullptr) {
        componentExtensionDescriptionWidget->setToDefault();
    }

}

void ComponentExtensionDescriptionState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* componentExtensionDescriptionWidget = dynamic_cast<ComponentExtensionDescriptionWidget*>(widget);
    if (componentExtensionDescriptionWidget != nullptr) {
        QString componentExtensionName = componentExtensionDescriptionWidget->getComponentExtensionName();
        QString componentExtensionDescription = componentExtensionDescriptionWidget->getComponentExtensionDescription();
        domComponentExtension->name(componentExtensionName.toStdString());
        domComponentExtension->description(componentExtensionDescription.toStdString());
    }
}
