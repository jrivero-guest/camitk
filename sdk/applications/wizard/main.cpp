/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "WizardController.h"

// CamiTK stuff
#include <Core.h>

//Imports from Qt
#include <QApplication>
#include <iostream>

// CLI stuff
#include "CommandLineOptions.hxx"

// usage for this application
void usage(char* appName) {
    std::cerr << appName << std::endl;
    std::cerr << std::endl;
    std::cerr << "Usage: " << appName << " [options]" << std::endl;
    std::cerr << "Build using " << camitk::Core::version << std::endl;
    std::cerr << std::endl;
    std::cerr << "Options:" << endl;
    options::print_usage(std::cerr);
}


int main(int argc, char* argv[]) {
    try {
        QApplication app(argc, argv);

        int end; // End of options.
        options o(argc, argv, end);

        if (o.help()) {
            usage(argv[0]);
            return EXIT_SUCCESS;
        }
        else {
            // print all types of versions (just using the char* defined in CamiTKVersion.h at configuration time)
            if (o.version()) {
                std::cout << argv[0] << " build using " << camitk::Core::version << std::endl;
                return EXIT_SUCCESS;
            }
            else {
                // call to controller
                auto* controller = new WizardController();
                controller->launch();
                return app.exec();
            }
        }
    }
    catch (const cli::exception& e) {
        std::cerr << e << std::endl;
        usage(argv[0]);
        return EXIT_FAILURE;
    }
    catch (const camitk::AbortException& e) {
        std::cout << argv[0] << " aborted..." << std::endl << "camitk AbortException:" << std::endl << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e) {
        std::cout << argv[0] << " aborted..." << std::endl << "std exception:" << std::endl << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...) {
        std::cout << argv[0] << " aborted..." << std::endl << "Unknown Exception" << std::endl;
        return EXIT_FAILURE;
    }
}


