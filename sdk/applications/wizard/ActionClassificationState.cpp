/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Include .h file
#include "ActionClassificationState.h"
#include "ActionClassificationWidget.h"
#include "ActionTagWidget.h"
#include "WizardMainWindow.h"
#include "ActionCreationState.h"

// includes from coreschema
#include <Action.hxx>
#include <Classification.hxx>



ActionClassificationState::ActionClassificationState(QString name, ActionClassificationWidget* widget, WizardMainWindow* mainWindow, ActionCreationState* parent)
    : WizardState(name, widget, mainWindow, parent) {
    this->domAction = nullptr;
}

void ActionClassificationState::resetDomAction(cepcoreschema::Action* domAction) {
    this->domAction = domAction;
    auto* actionClassificationWidget = dynamic_cast<ActionClassificationWidget*>(widget);
    if (actionClassificationWidget != nullptr) {
        actionClassificationWidget->setToDefault();
    }
}

void ActionClassificationState::onEntry(QEvent* event) {
    WizardState::onEntry(event);
}

void ActionClassificationState::onExit(QEvent* event) {
    WizardState::onExit(event);
    auto* actionClassificationWidget = dynamic_cast<ActionClassificationWidget*>(widget);
    if (actionClassificationWidget != nullptr) {
        QString family = actionClassificationWidget->getFamily();
        cepcoreschema::Classification aClassification(family.toStdString());
        // Get the Tags
        QList<ActionTagWidget*> tagWidgets = actionClassificationWidget->getTagWidgets();
        // Check if itk filter information has been provided
        if (domAction->classification().itkFilter().present()) {
            aClassification.itkFilter(domAction->classification().itkFilter());
        }

        foreach (ActionTagWidget*   tagWidget, tagWidgets) {
            QString tag = tagWidget->getTag();
            aClassification.tag().push_back(tag.toStdString().c_str());
        }
        domAction->classification(aClassification);
    }
}

