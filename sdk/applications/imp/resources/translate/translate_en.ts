<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ImpMainWindow</name>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="147"/>
        <source>Application Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="147"/>
        <source>This application &lt;b&gt;needs&lt;/b&gt; the Application Action Extension and could not find it in any of the action extension directories (&lt;tt&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="147"/>
        <source>&lt;/tt&gt;).&lt;br/&gt;&lt;br/&gt;Please check your CamiTK configuration and/or installation&lt;br/&gt;&lt;br/&gt;E.g., run &lt;tt&gt;camitk-config --config&lt;/tt&gt; to check the action extension directories and your installation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="161"/>
        <source>Recent Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="175"/>
        <source>&amp;Preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="176"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="177"/>
        <source>Edit the preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="178"/>
        <source>Preferences

Edit the settings and preferences of imp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="182"/>
        <source>&amp;Save history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="183"/>
        <location filename="../../ImpMainWindow.cpp" line="184"/>
        <source>Save the history of actions processed as an SCXML file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="188"/>
        <source>Menu bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="191"/>
        <location filename="../../ImpMainWindow.cpp" line="192"/>
        <source>Hide/show the menu bar (Ctrl-M to show again).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="193"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="206"/>
        <source>Status bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="209"/>
        <location filename="../../ImpMainWindow.cpp" line="210"/>
        <source>Hide/show the status bar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="213"/>
        <source>Reset Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="214"/>
        <source>Reset all windows in the initial state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="228"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="231"/>
        <source>Open &amp;Data Directory...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="256"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="263"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="266"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="268"/>
        <location filename="../../ImpMainWindow.cpp" line="269"/>
        <source>What&apos;s This and Viewer Keyboard Shortcuts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="280"/>
        <source>&amp;Actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="359"/>
        <source>Hide menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="359"/>
        <source>The menu is going to be hidden. There is only one way to make it reappear: you need to press CTRL+M again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="457"/>
        <source>Error loading directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="485"/>
        <source>Toggle Viewers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="492"/>
        <source>Other Toggle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="500"/>
        <source>Edit application preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="572"/>
        <source>Open Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="572"/>
        <source>Recent document:&lt;br/&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="572"/>
        <source>&lt;br/&gt;Does not exist anymore, sorry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="585"/>
        <source>&amp;%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
