<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ImpMainWindow</name>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="147"/>
        <source>Application Error</source>
        <translation>Erreur dans l&apos;Application</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="147"/>
        <source>This application &lt;b&gt;needs&lt;/b&gt; the Application Action Extension and could not find it in any of the action extension directories (&lt;tt&gt;</source>
        <translation>Cette applicationn &lt;b&gt;necessite&lt;/b&gt; l&apos;Application Action Extension et ne peut pas la trouverdans les répertoires action extension (&lt;tt&gt;</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="147"/>
        <source>&lt;/tt&gt;).&lt;br/&gt;&lt;br/&gt;Please check your CamiTK configuration and/or installation&lt;br/&gt;&lt;br/&gt;E.g., run &lt;tt&gt;camitk-config --config&lt;/tt&gt; to check the action extension directories and your installation.</source>
        <translation>&lt;/tt&gt;).&lt;br/&gt;&lt;br/&gt;Veuillez vérifier votre configuration Camitk et/ou installation&lt;br/&gt;&lt;br/&gt;E.g., run &lt;tt&gt;camitk-config --config&lt;/tt&gt; pour vérifier le répertoire action extension et votre installation.</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="161"/>
        <source>Recent Documents</source>
        <translation>Documents Récents</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="175"/>
        <source>&amp;Preferences...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="176"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="177"/>
        <source>Edit the preferences</source>
        <translation>Editer les préferences</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="178"/>
        <source>Preferences

Edit the settings and preferences of imp</source>
        <translation>Préférences

Editer les paramêtres et préférences de imp </translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="182"/>
        <source>&amp;Save history</source>
        <translation>&amp;Sauver l&apos;histoire</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="183"/>
        <location filename="../../ImpMainWindow.cpp" line="184"/>
        <source>Save the history of actions processed as an SCXML file.</source>
        <translation>Sauver l&apos;histoire des actions calculées comme un fichier SCXML.</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="188"/>
        <source>Menu bar</source>
        <translation>Barre de menu</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="191"/>
        <location filename="../../ImpMainWindow.cpp" line="192"/>
        <source>Hide/show the menu bar (Ctrl-M to show again).</source>
        <translation>Cacher/montrer la barre de menu (Ctrl-M to show again).</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="193"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="206"/>
        <source>Status bar</source>
        <translation>Barre d&apos;état</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="209"/>
        <location filename="../../ImpMainWindow.cpp" line="210"/>
        <source>Hide/show the status bar.</source>
        <translation>Cacher/montrer la barre d&apos;état.</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="213"/>
        <source>Reset Windows</source>
        <translation>Reset des Fenêtres</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="214"/>
        <source>Reset all windows in the initial state</source>
        <translation>Reset de toutes les fenêtres dans leur état initial</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="228"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="231"/>
        <source>Open &amp;Data Directory...</source>
        <translation>Ouvrir Répertoire de &amp;Données...</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="256"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="263"/>
        <source>&amp;View</source>
        <translation>&amp;Visionner</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="266"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="268"/>
        <location filename="../../ImpMainWindow.cpp" line="269"/>
        <source>What&apos;s This and Viewer Keyboard Shortcuts.</source>
        <translation>Qu&apos;est ce que c&apos;est et Inspecteur de raccouircis clavier.</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="280"/>
        <source>&amp;Actions</source>
        <translation>&amp;Actions</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="359"/>
        <source>Hide menu</source>
        <translation>Cacher le menu</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="359"/>
        <source>The menu is going to be hidden. There is only one way to make it reappear: you need to press CTRL+M again.</source>
        <translation>Le menu va être caché. Il y a seulement un manière pour le faire réapparaître : tapez un autre CTRL+M.</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="457"/>
        <source>Error loading directory:</source>
        <translation>Erreur au chargement du répertoire:</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="485"/>
        <source>Toggle Viewers</source>
        <translation>Alterner les Visionneurs</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="492"/>
        <source>Other Toggle</source>
        <translation>Autre alternance</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="500"/>
        <source>Edit application preferences...</source>
        <translation>Editer les préférences des applications...</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="572"/>
        <source>Open Recent</source>
        <translation>Récemment Ouvert </translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="572"/>
        <source>Recent document:&lt;br/&gt;</source>
        <translation>Document Récent:&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="572"/>
        <source>&lt;br/&gt;Does not exist anymore, sorry...</source>
        <translation>&lt;br/&gt;n&apos;existe plus actuellement, désolé ...</translation>
    </message>
    <message>
        <location filename="../../ImpMainWindow.cpp" line="585"/>
        <source>&amp;%1 %2</source>
        <translation></translation>
    </message>
</context>
</TS>
