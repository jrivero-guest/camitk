/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include <Application.h>
#include <Core.h>
#include <ExtensionManager.h>
#include <MainWindow.h>
#include <Log.h>
using namespace camitk;

#include <iostream>
#include <QDateTime>

#include "CommandLineOptions.hxx"
#include "CamiTKVersionInformation.h"

// usage for this application
void usage(char* appName) {
    std::cerr << appName << std::endl;
    std::cerr << std::endl;
    std::cerr << "Usage: " << appName << " [options]" << std::endl;
    std::cerr << "Build using " << Core::version << std::endl;
    std::cerr << std::endl;
    std::cerr << "Options:" << endl;
    options::print_usage(std::cerr);
}

int main(int argc, char* argv[]) {
    try {
        int end; // End of options.
        options o(argc, argv, end);

        // if specific help or no options provided
        if (o.help()) {
            usage(argv[0]);
            return EXIT_SUCCESS;
        }
        else {
            // print all types of versions (just using the char* defined in CamiTKVersion.h at configuration time)
            if (o.version()) {
                std::cout << argv[0] << " build using " << Core::version << std::endl;
                if (compiledFromPackageSource()) {
                    std::cout << "Compiled using package source." << std::endl;
                }
                else {
                    std::cout << "Compiled using git";
                    if (hasMoreGitInformation()) { // Check if CAMITK_GIT_FOUND can be set to "1" otherwise we never get into this 'if'
                        std::cout << " Hash: " << gitHash() << ", Date: " << gitDate();
                    }
                    std::cout << "." << std::endl;
                }
                return EXIT_SUCCESS;
            }
            else if (o.complete_version()) {
                std::cout << Core::version << std::endl;
                return EXIT_SUCCESS;
            }
            else if (o.short_version()) {
                std::cout << Core::shortVersion << std::endl;
                return EXIT_SUCCESS;
            }
            else if (o.time_stamp()) {
                QDateTime now = QDateTime::currentDateTime();
                std::cout << now.toString("yyyy-MM-dd").toStdString() << "T" << now.toString("hh:mm:ss").toStdString() << std::endl;
                return EXIT_SUCCESS;
            }
            else if (o.print_paths()) {
                // init the camitk application context without auto-loading all extensions, log level is NONE
                Log::getLogger()->setLogLevel(InterfaceLogger::NONE);
                Application a("camitk-config", argc, argv, false, false);
                std::cout << Core::getPaths().toStdString() << std::endl;
                return EXIT_SUCCESS;
            }
            else if (o.camitk_dir()) {
                // init the camitk application context without auto-loading all extensions, log level is NONE
                Log::getLogger()->setLogLevel(InterfaceLogger::NONE);
                Application a("camitk-config", argc, argv, false, false);
                std::cout << Core::getGlobalInstallDir().toStdString() << std::endl;
                return EXIT_SUCCESS;
            }
            else if (o.config()) {
                // init the camitk application context (auto-load of all extensions), log level is WARNING
                Log::getLogger()->setLogLevel(InterfaceLogger::WARNING);
                Application a("camitk-config", argc, argv, true, false);
                // avoid all redirection to console
                MainWindow* defaultMainWindow = dynamic_cast<MainWindow*>(a.getMainWindow());
                defaultMainWindow->redirectToConsole(false);
                // print the configuration on the std::cout
                std::cout << Core::getConfig().toStdString() << std::endl;
                return EXIT_SUCCESS;
            }
            else if (o.bug_report_info()) {
                // init the camitk application context (auto-load of all extensions), log level is INFO
                Log::getLogger()->setLogLevel(InterfaceLogger::INFO);
                Application a("camitk-config", argc, argv, true, false);
                // avoid all redirection to console
                MainWindow* defaultMainWindow = dynamic_cast<MainWindow*>(a.getMainWindow());
                defaultMainWindow->redirectToConsole(false);
                // print the configuration on the std::cout
                std::cout << Core::getBugReport().toStdString() << std::endl;
                return EXIT_SUCCESS;
            }
            else {
                usage(argv[0]);
                //TODO: Show some GUI (for e.g. with "copy to clipboard" and "email" buttons)
                //return a.exec();
                return EXIT_SUCCESS;
            }
        }
    }
    catch (const cli::exception& e) {
        cerr << e << endl;
        usage(argv[0]);
        return EXIT_FAILURE;
    }

}
