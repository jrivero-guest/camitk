/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef FRAMEEDITOR_H
#define FRAMEEDITOR_H

#include "Action.h"
#include "Component.h"

#include <QVector>
#include <QLineEdit>

#include "ui_FrameEditor.h"

/** This action allows you to apply a linear transformation
 * (translation,rotation around axes) on the top level selected Frame
 */
class FrameEditor : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    FrameEditor(camitk::ActionExtension*);

    /// Destructor
    virtual ~FrameEditor();

    /// this method creates and returns the widget containing the user interface for the action
    virtual QWidget* getWidget();

public slots:
    /**
     * Update the component's frame with the translation and rotation parameters of the widget.
     * This keeps synchronized the frame with the value selected by the user
     */
    virtual ApplyStatus apply();


    virtual void displayFrameToggled(bool);
    virtual void frameSizeChanged(int val);

    virtual void setParentFrame();

    virtual void transformationChanged();
    virtual void setTransformation();

    virtual void translationChanged();
    virtual void translate();
    virtual void setTranslation();

    virtual void rotationChanged();
    virtual void rotate();
    virtual void setRotation();


protected:
    virtual void initializeDialogWithCurrentComponent();

private:
    /// initialize the dialog
    void init();

    /// the Qt Gui
    Ui::FrameEditor ui;

    QWidget* myWidget;

    camitk::Component* currentComponent;

    QVector< QVector<QLineEdit*> > transfromMatrixElements;

};

#endif // FRAMEEDITOR_H
