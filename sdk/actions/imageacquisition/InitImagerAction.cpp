/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK includes
#include "InitImagerAction.h"
#include <Property.h>
#include <Application.h>

using namespace camitk;

// --------------- Constructor -------------------
InitImagerAction::InitImagerAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("InitImagerAction");
    setDescription("initialize the acquisition device");
    setComponent("ImageAcquisitionComponent");

    // Setting classification family and tags
    setFamily("Image Acquisition");
    // Tags allow the user to search the actions trhough themes
    // You can add tags here with the method addTag("tagName");

    // Setting the action's parameters
    // If you want to add parameters to your action, you can add them
    // using properties
    // addParameter(new Property(tr("Property name"), "Property value", tr("Property description"), "Property unit"));

}

// --------------- destructor -------------------
InitImagerAction::~InitImagerAction() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- apply -------------------
Action::ApplyStatus InitImagerAction::apply() {

    foreach (Component* comp, getTargets()) {
        ImageAcquisitionComponent* input = dynamic_cast<ImageAcquisitionComponent*>(comp);
        process(input);
    }

    return SUCCESS;
}

void InitImagerAction::process(ImageAcquisitionComponent* comp) {

    comp->initImager();

    Application::refresh();

}


