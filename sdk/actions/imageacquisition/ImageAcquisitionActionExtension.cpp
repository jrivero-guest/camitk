/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#include "ImageAcquisitionActionExtension.h"

// include generated actions headers
#include "InitImagerAction.h"
#include "StartImaging2DAction.h"
#include "StopImaging2DAction.h"
#include "StartImaging3DAction.h"
#include "StopImaging3DAction.h"
#include "SingleAcquisition2DAction.h"
#include "SingleAcquisition3DAction.h"

// --------------- getActions -------------------
void ImageAcquisitionActionExtension::init() {
    // Creating and registering the instance of InitImager
    // Creating and registering the instance of StartImaging2D
    // Creating and registering the instance of StopImaging2D
    // Creating and registering the instance of StartImaging3D
    // Creating and registering the instance of StopImaging3D
    // Creating and registering the instance of SingleAcquisition2D
    // Creating and registering the instance of SingleAcquisition3D
    registerNewAction(InitImagerAction);
    registerNewAction(StartImaging2DAction);
    registerNewAction(StopImaging2DAction);
    registerNewAction(StartImaging3DAction);
    registerNewAction(StopImaging3DAction);
    registerNewAction(SingleAcquisition2DAction);
    registerNewAction(SingleAcquisition3DAction);
}

