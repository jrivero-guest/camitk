/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SPHERE_TOPOLOGY_H
#define SPHERE_TOPOLOGY_H

#include <Action.h>

/**
 * @ingroup group_sdk_actions_mesh_basictopology
 *
 * @brief
 * Create a sphere mesh.
 *
 * The sphere topology action widget allows you to select the desired dimensions of your sphere.
 * \image html actions/basic_topology_sphere.png "The sphere topology widget." width=10cm
 *
 */
class SphereTopology : public camitk::Action {
    Q_OBJECT

public:

    /// the constructor
    SphereTopology(camitk::ActionExtension*);

    /// the destructor
    virtual ~SphereTopology() = default;

    /// method called when the action when the action is triggered (i.e. started)
    //virtual QWidget * getWidget();

public slots:

    /// method called when the action is applied
    virtual ApplyStatus apply();

private:
    QWidget* myWidget;

};

#endif // SPHERE_TOPOLOGY_H
