/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef GRID_TOPOLOGY_H
#define GRID_TOPOLOGY_H

#include <vtkUnstructuredGrid.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include <Action.h>

// TODO : support for structured grid and path in CamiTK

/**
 * @ingroup group_sdk_actions_mesh_basictopology
 *
 * @brief
 * Create a 3D/2D/1D grid mesh.
 *
 * The grid topology action widget allows you to select the desired dimensions of your sphere.
 * \image html actions/basic_topology_grid.png "The grid topology widget." width=10cm
 */
class GridTopology : public camitk::Action {

public:

    /// the constructor
    GridTopology(camitk::ActionExtension*);

    /// the destructor
    virtual ~GridTopology() = default;

public slots:

    /// method called when the action is applied
    virtual ApplyStatus apply();
private:
    /**
     * @brief Build 1D grid (path).
     *
     * @param grid grid
     * @return void
     **/
    void build1DGrid(vtkSmartPointer<vtkUnstructuredGrid> grid);

    /**
     * @brief Build 2D grid.
     *
     * @param grid grid
     * @return void
     **/
    void build2DGrid(vtkSmartPointer<vtkUnstructuredGrid> grid);

    /**
     * @brief Build 3D grid.
     *
     * @param grid grid
     * @return void
     **/
    void build3DGrid(vtkSmartPointer<vtkUnstructuredGrid> grid);

    /**
     * @brief Build line network.
     *
     * @param grid grid
     * @return void
     **/
    void buildNetwork(vtkSmartPointer<vtkUnstructuredGrid> grid);

};

#endif // GRID_TOPOLOGY_H
