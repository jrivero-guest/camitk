/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef RIGID_TRANSFORM_H
#define RIGID_TRANSFORM_H

#include "Action.h"
#include "Component.h"

#include <vtkTransform.h>
#include <vtkTransformFilter.h>
#include <vtkSmartPointer.h>

#include "ui_RigidTransformDialog.h"


using vtkSmartPointerTransformFilter = vtkSmartPointer<vtkTransformFilter>;

/**
 * @ingroup group_sdk_actions_mesh_basicmesh
 *
 * @brief
 * This action allows you to apply a linear transformation (translation,rotation around axes,scaling)
 * the currently selected MeshComponents.
 * This action uses a widget to select its different input parameters.
 *
 * \image html actions/basic_mesh_rigid_transformation.png "The rigid transform widget." width=10cm
 *
 */
class RigidTransform : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    RigidTransform(camitk::ActionExtension*);

    /// Destructor
    virtual ~RigidTransform();

    /// this method creates and returns the widget containing the user interface for the action
    virtual QWidget* getWidget();

public slots:
    /// method called when the action is applied
    virtual ApplyStatus apply();

private slots:
    /// Slot called when the button Preview is clicked
    void preview();

    /// Slot called when the button Cancel is clicked, reset the transformation to identity (don't transform)
    void close();

    /** Slot called when the slider is moved, update the transformation of the selected Component (just for visualization).
      *	@param forceUpdate force the visualization of the current transformation (default is false)
      */
    void update(bool forceUpdate = false);

    /// Slot loading a transformation from a file
    void load();

    /// Slot saving a transformation in a file
    void save();

    /// slot called when reset is ckicked, reset the transformation to Identity
    void reset();

private:
    /// initialize the dialog
    void init();

    /// current transformation
    vtkSmartPointer<vtkTransform> transformation;

    /// current transform filters (one per selected object)
    QList< vtkSmartPointerTransformFilter > filterList;

    /// the Qt Gui
    Ui::RigidTransformDialog myUI;

    /// the dialog
    QDialog* dialog;

};

#endif // RIGID_TRANSFORM_H
