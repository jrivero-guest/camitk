/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef BASIC_MESH_EXTENSION_H
#define BASIC_MESH_EXTENSION_H

#include <ActionExtension.h>
#include <QPushButton>
#include <QDialogButtonBox>

/**
 * @ingroup group_sdk_actions_mesh_basicmesh
 *
 * @brief
 * The basic mesh action extension.
 *
 */
class BasicMeshExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.action.basicmesh")

public:
    /// the constructor (needed to initialize the icon resources)
    BasicMeshExtension();

    /// the destructor
    virtual ~BasicMeshExtension() = default;

    /// initialize all the actions
    virtual void init();

    /// Method that return the action extension name
    virtual QString getName() {
        return "Basic Mesh Extension";
    };

    /// Method that return the action extension descrption
    virtual QString getDescription() {
        return "This extension provides various basic actions for mesh components";
    };

};

#endif // BASIC_MESH_EXTENSION_H
