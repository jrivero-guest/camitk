/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "BasicMeshExtension.h"
#include "RigidTransform.h"
#include "RenderingOption.h"
#include "ChangeColor.h"
#include "MeshPicking.h"
#include "CenterMesh.h"
#include "MeshQuality.h"
#include "MeshToImageStencil.h"
#include "ComputeNormals.h"
#include "ExtractSelection.h"

#include <Action.h>

// --------------- constructor -------------------
BasicMeshExtension::BasicMeshExtension() : ActionExtension() {
    // initialize the icon resources
    Q_INIT_RESOURCE(BasicMeshExtensionIcons);
}

// --------------- init -------------------
void BasicMeshExtension::init() {
    registerNewAction(ChangeColor);
    registerNewAction(RenderingOption);
    registerNewAction(RigidTransform);
    registerNewAction(MeshPicking);
    registerNewAction(CenterMesh);
    registerNewAction(MeshQuality);
    registerNewAction(MeshToImageStencil);
    registerNewAction(ComputeNormals);
    registerNewAction(ExtractSelection);
}
