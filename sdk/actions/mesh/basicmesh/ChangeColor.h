/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CHANGECOLOR_H
#define CHANGECOLOR_H

#include <QObject>
#include <Action.h>

/**
 * @ingroup group_sdk_actions_mesh_basicmesh
 *
 * @brief
 * Change the color of the rendering of the current selected mesh.
 * Use its widget (pop-up) to select the desired color.
 *
 * \image html actions/basic_mesh_change_color.png "The change color pop-up." width=10cm
 *
 */
class ChangeColor : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    ChangeColor(camitk::ActionExtension*);

    /// the destructor
    virtual ~ChangeColor() = default;

    /// There are no parameters for this action. The GUI is run as a one-shot dialog in apply
    virtual QWidget* getWidget() {
        return nullptr;
    }

public slots:
    /// method called when the action is applied
    virtual ApplyStatus apply();

};

#endif // CHANGECOLOR_H
