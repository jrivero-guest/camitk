/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ChangeColor.h"

#include <Component.h>
#include <Log.h>

using namespace camitk;

#include <QColorDialog>

// --------------- constructor -------------------
ChangeColor::ChangeColor(ActionExtension* extension) : Action(extension) {
    setName("Change Color");
    setEmbedded(false);
    setDescription(tr("Change the surface, wireframe or points colors of objects"));
    setComponent("MeshComponent");
    setFamily("Basic Mesh");
    setIcon(QPixmap(":/changeColor"));
    addTag(tr("Color"));
}

// --------------- apply -------------------
Action::ApplyStatus ChangeColor::apply() {
    // get an initial color from the first selected item
    double actorColor[4];
    getTargets().last()->getActorColor(getTargets().last()->getRenderingModes(), actorColor);

    QColor currentColor;
    currentColor.setRgbF(actorColor[0], actorColor[1], actorColor[2], actorColor[3]);

    QColor newColor = QColorDialog::getColor(currentColor, NULL, QString("Change color of ") + getTargets().size() + QString(" components"), QColorDialog::ShowAlphaChannel);

    if (newColor.isValid()) {
        actorColor[0] = newColor.redF();
        actorColor[1] = newColor.greenF();
        actorColor[2] = newColor.blueF();
        actorColor[3] = newColor.alphaF();

        // change the color for the selected Component modes
        foreach (Component* comp, getTargets()) {
            comp->setActorColor(comp->getRenderingModes(), actorColor);
        }

        getTargets().last()->refresh();

        return SUCCESS;
    }
    else {
        CAMITK_WARNING(tr("Invalid color. Action Aborted."))
        return ABORTED;
    }
}
