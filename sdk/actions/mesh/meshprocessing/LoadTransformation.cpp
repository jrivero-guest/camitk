/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "LoadTransformation.h"

// Qt includes
#include <QFileDialog>
#include <QPushButton>
#include <QLayout>

// VTK includes
#include <vtkTransform.h>
#include <vtkPolyData.h>
#include <vtkTransformFilter.h>
#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>

// CamiTK includes
#include <Application.h>
#include <ActionWidget.h>
#include <Log.h>

using namespace camitk;

// --------------- Constructor -------------------
LoadTransformation::LoadTransformation(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Load Transformation");
    setDescription(tr("Load Transformation"));
    setComponent("MeshComponent");

    // Setting classification family and tags
    setFamily("Mesh Processing");
    // Tags allow the user to search the actions trhough themes
    // You cad add tags here with the method addTag("tagName");

    // DO NOT Put any GUI instanciation here,
    // If you need, do it in getWidget() method, using lazy instanciation
}

// --------------- destructor -------------------
LoadTransformation::~LoadTransformation() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- openTransformation -------------------
void LoadTransformation::openTransformation() {

    // get the transformation file name
    fn = QFileDialog::getOpenFileName(NULL, "Load Transformation");
}

// --------------- apply -------------------
Action::ApplyStatus LoadTransformation::apply() {

    foreach (Component* comp, getTargets()) {
        MeshComponent* input = dynamic_cast<MeshComponent*>(comp);
        loadTransformationFile();
    }

    return SUCCESS;
}

// --------------- loadTransformationFile -------------------
void LoadTransformation::loadTransformationFile() {

    if (!fn.isEmpty()) {
        //-- load the transformation
        std::ifstream in(fn.toStdString().c_str());

        vtkSmartPointer<vtkTransform> transformation = vtkSmartPointer<vtkTransform>::New();
        double x, y, z, t;
        for (unsigned int i = 0; i < 4; i++) {
            in >> x >> y >> z >> t;
            transformation->GetMatrix()->SetElement(i, 0, x);
            transformation->GetMatrix()->SetElement(i, 1, y);
            transformation->GetMatrix()->SetElement(i, 2, z);
            transformation->GetMatrix()->SetElement(i, 3, t);
        }

        //-- apply the transformation to targetMesh data set
        vtkSmartPointer<vtkPointSet> newData;
        MeshComponent* targetMesh = dynamic_cast<MeshComponent*>(getTargets().last());
        if (targetMesh) {
            if (vtkPolyData::SafeDownCast(targetMesh->getPointSet())) {
                newData = vtkSmartPointer<vtkPolyData>::New();
            }
            if (vtkUnstructuredGrid::SafeDownCast(targetMesh->getPointSet())) {
                newData = vtkSmartPointer<vtkPolyData>::New();
            }
            else {
                return;
            }

            // apply a vtk transform filter and get the result
            vtkSmartPointer<vtkTransformFilter> vtkFilter = vtkSmartPointer<vtkTransformFilter>::New();
            vtkFilter->SetTransform(transformation);
            vtkFilter->SetInputConnection(targetMesh->getDataPort());
            targetMesh->setDataConnection(vtkFilter->GetOutputPort());

            vtkSmartPointer<vtkPointSet> result = vtkPointSet::SafeDownCast(vtkFilter->GetOutputDataObject(0));
            if (result) {
                targetMesh->setPointSet(result);
            }

            targetMesh->setModified();

        }
    }
    else {
        CAMITK_WARNING(tr("Transformation file is missing, please provide one."))
    }

}

// --------------- getWidget -------------------
QWidget* LoadTransformation::getWidget() {
    // Use lazy instanciation (instanciate only once and when needed)
    // We will return the default action widget with an additionnal button

    // build or update the widget
    if (!actionWidget) {
        // Setting the widget containing the parameters, using the default widget
        actionWidget = new ActionWidget(this);

        QPushButton* loadtransformation = new QPushButton("Load Transformation");

        actionWidget->layout()->addWidget(loadtransformation);
        QObject::connect(loadtransformation, SIGNAL(released()), SLOT(openTransformation()));
    }
    else {
        // make sure the widget has updated targets
        dynamic_cast<ActionWidget*>(actionWidget)->update();
    }

    return actionWidget;
}

