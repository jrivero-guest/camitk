/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "InvertMesh.h"

#include <Application.h>

#include <vtkPolyDataNormals.h>
#include <vtkSmartPointer.h>
#include <vtkCallbackCommand.h>

using namespace camitk;


// --------------- Constructor -------------------
InvertMesh::InvertMesh(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Invert Mesh");
    setDescription(tr("Invert the mesh faces"));
    setComponent("MeshComponent");

    // Setting classification family and tags
    setFamily("Mesh Processing");
    addTag(tr("Inside Out"));
    addTag(tr("Normal"));
    addTag(tr("Flip"));
}

// --------------- destructor -------------------
InvertMesh::~InvertMesh() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- apply -------------------
Action::ApplyStatus InvertMesh::apply() {
    foreach (Component* comp, getTargets()) {
        MeshComponent* input = dynamic_cast<MeshComponent*>(comp);
        process(input);
    }
    return SUCCESS;
}

// --------------- process -------------------
void InvertMesh::process(MeshComponent* comp) {

    vtkSmartPointer<vtkPolyDataNormals> polydataNormals = vtkSmartPointer<vtkPolyDataNormals>::New();
    vtkSmartPointer<vtkPolyData> poly = vtkPolyData::SafeDownCast(comp->getPointSet());
    vtkSmartPointer<vtkCallbackCommand> progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
    progressCallback->SetCallback(&Application::vtkProgressFunction);

    polydataNormals->SetInputData(poly);
    polydataNormals->SetFlipNormals(1);
    polydataNormals->AddObserver(vtkCommand::ProgressEvent, progressCallback);
    polydataNormals->Update();

    comp->setPointSet(vtkPointSet::SafeDownCast(polydataNormals->GetOutput()));
    comp->refresh();

    // restore the normal cursor and progress bar
    Application::resetProgressBar();
    QApplication::restoreOverrideCursor();
}


