/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef COMPUTE_CURVATURES_HPP
#define COMPUTE_CURVATURES_HPP

#include <Action.h>
#include <vtkCurvatures.h>

class ComputeCurvatures : public camitk::Action {

    Q_OBJECT

public:

    enum CurvatureType {
        GAUSSIAN = VTK_CURVATURE_GAUSS,
        MEAN = VTK_CURVATURE_MEAN,
        MAXIMUM = VTK_CURVATURE_MAXIMUM,
        MINIMUM = VTK_CURVATURE_MINIMUM,
    };
    Q_ENUMS(CurvatureType)

    ComputeCurvatures(camitk::ActionExtension* ext);

    virtual ~ComputeCurvatures() = default;

public slots :

    /// method applied when the action is called
    virtual ApplyStatus apply();

};

Q_DECLARE_METATYPE(ComputeCurvatures::CurvatureType)

#endif // COMPUTE_CURVATURES_HPP
