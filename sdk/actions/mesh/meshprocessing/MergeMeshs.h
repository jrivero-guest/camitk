/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef MERGEMESHS_H
#define MERGEMESHS_H

#include <Action.h>

#include <MeshComponent.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCell.h>

/** MergeMeshs action creates new mesh from two input meshs by merging points which are exactly at the same coordinates and remove duplicate cells.
  * The first mesh is kept as a priority.
  *
  */
class MergeMeshs : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    MergeMeshs(camitk::ActionExtension* extension);

    /// Default Destructor
    virtual ~MergeMeshs();

private:
    /**
     * @brief print for each element the k associated
     **/
    void printKToFile();

    /// Check if one point of the first tetra is not a point of the second tetra ==> tetra are not equal.
    bool duplicateTetra(std::vector< std::vector< double > > t1, std::vector< std::vector< double > > t2);

    /// Reconstructed mesh
    std::vector<double> kVec;

public slots:
    /** this method is automatically called when the action is triggered.
      * Use getTargets() QList to get the list of component to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();

protected:
    /// Reconstruct a mesh without duplicating cells
    vtkSmartPointer<vtkUnstructuredGrid> removeDuplicateCells(vtkSmartPointer< vtkPointSet > input, int nbCellMesh1, bool saveIndices);
};
#endif // MERGEMESHS_H
