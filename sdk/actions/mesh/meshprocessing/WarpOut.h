/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef WARPOUT_H
#define WARPOUT_H

#include <Action.h>
/**
 * @ingroup group_sdk_actions_mesh_meshprocessing
 *
 * @brief
 * This action extracts the outer surface of a multiple surface polydata model and warp it outward.
 *
 * It assumes the outside surface is closed and there is only one outside surface.
 * It also assumes that the center of the model lies inside the outer surface.
 *
 * @note
 * Grow only the outward surface of a mesh.
 *
 */
class WarpOut : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    WarpOut(camitk::ActionExtension*);

    /// the destructor
    virtual ~WarpOut() = default;

public slots:
    /// method called when the action is applied
    virtual ApplyStatus apply();

};

#endif // WARPOUT_H
