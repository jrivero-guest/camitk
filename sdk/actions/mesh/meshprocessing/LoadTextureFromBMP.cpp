/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "LoadTextureFromBMP.h"

// Qt includes
#include <QFileDialog>
#include <QPushButton>
#include <QLayout>

// VTK includes
#include <vtkBMPReader.h>
#include <vtkImageFlip.h>
#include <vtkTexture.h>

// CamiTK includes
#include <Application.h>
#include <ActionWidget.h>
#include <Log.h>

using namespace camitk;


// --------------- Constructor -------------------
LoadTextureFromBMP::LoadTextureFromBMP(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Load Texture From BMP");
    setDescription(tr("Load Texture From BMP"));
    setComponent("MeshComponent");

    // Setting classification family and tags
    setFamily("Mesh Processing");

    // DO NOT Put any GUI instanciation here,
    // If you need, do it in getWidget() method, using lazy instanciation
}

// --------------- destructor -------------------
LoadTextureFromBMP::~LoadTextureFromBMP() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- loadBMP -------------------
void LoadTextureFromBMP::loadBMP() {
    trFile = QFileDialog::getOpenFileName(NULL, tr("Open BMP File"), QString(), "BMP File (*.bmp)");
}

// --------------- apply -------------------
Action::ApplyStatus LoadTextureFromBMP::apply() {

    foreach (Component* comp, getTargets()) {
        MeshComponent* input = dynamic_cast<MeshComponent*>(comp);
        process(input);
    }

    return SUCCESS;
}

// --------------- process -------------------
void LoadTextureFromBMP::process(MeshComponent* comp) {

    if (!trFile.isEmpty()) {
        vtkSmartPointer<vtkBMPReader> bmpReader = vtkSmartPointer<vtkBMPReader>::New();
        bmpReader->SetFileName(trFile.toStdString().c_str());

        // Create a brand new imageYFlip.
        vtkSmartPointer<vtkImageFlip> imageYFlip = vtkSmartPointer<vtkImageFlip>::New();
        imageYFlip->SetInputConnection(bmpReader->GetOutputPort());
        imageYFlip->SetFilteredAxis(1);

        vtkSmartPointer<vtkTexture> texture = vtkSmartPointer<vtkTexture>::New();
        texture->SetInputConnection(imageYFlip->GetOutputPort());

        comp->setTexture(texture);
    }
    else {
        CAMITK_WARNING(tr("Texture file is missing, please provide a .bmp file."))
    }
}

// --------------- getWidget -------------------
QWidget* LoadTextureFromBMP::getWidget() {
    // Use lazy instanciation (instanciate only once and when needed)
    // We will return the default action widget with an additionnal button

    // build or update the widget
    if (!actionWidget) {
        // Setting the widget containing the parameters, using the default widget
        actionWidget = new ActionWidget(this);

        //-- Smoothing button
        QPushButton* loadbmpbutton = new QPushButton("Load BMP");

        actionWidget->layout()->addWidget(loadbmpbutton);
        QObject::connect(loadbmpbutton, SIGNAL(released()), SLOT(loadBMP()));
    }
    else {
        // make sure the widget has updated targets
        dynamic_cast<ActionWidget*>(actionWidget)->update();
    }

    return actionWidget;
}


