/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef ICP_REGISTRATION_H
#define ICP_REGISTRATION_H

#include <QObject>

#include <Action.h>

/**
 * @ingroup group_sdk_actions_mesh_meshprocessing
 *
 * @brief
 * Perform an Iterative Closest Point registration between two meshes.
 *
 */
class ICPRegistration : public camitk::Action {

    Q_OBJECT

public:

    enum DistanceMeasureType {
        RMS,
        ABS
    };
    Q_ENUMS(DistanceMeasureType)

public:

    /// the constructor
    ICPRegistration(camitk::ActionExtension* ext);

    /// the destructor
    virtual ~ICPRegistration() = default;


public slots :

    /// method applied when the action is called
    virtual ApplyStatus apply();

protected :

    DistanceMeasureType distanceMeasureType;

};

Q_DECLARE_METATYPE(ICPRegistration::DistanceMeasureType)

#endif // ICP_REGISTRATION_H
