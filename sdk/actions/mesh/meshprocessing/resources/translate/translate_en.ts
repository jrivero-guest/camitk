<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>AppendMeshes</name>
    <message>
        <location filename="../../AppendMeshes.cpp" line="44"/>
        <source>Append several meshes in one mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AppendMeshes.cpp" line="48"/>
        <source>Merge Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AppendMeshes.cpp" line="48"/>
        <source>Define if close points are merged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CleanPolyData</name>
    <message>
        <location filename="../../CleanPolyData.cpp" line="43"/>
        <source>Merge duplicate points, and/or remove unused points and/or remove degenerate cells.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CleanPolyData.cpp" line="48"/>
        <source>vtkCleanPolyData</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../CleanPolyData.cpp" line="49"/>
        <source>Clean vtkPolyData</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputeCurvatures</name>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="17"/>
        <source>Computes curvatures of a surface.&lt;br/&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="20"/>
        <source>curvatures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="22"/>
        <source>Curvature type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="22"/>
        <source>The type of curvature to compute on the surfacic mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Gaussian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Mean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="25"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ComputeCurvatures.cpp" line="37"/>
        <source>Compute Curvatures...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Decimation</name>
    <message>
        <location filename="../../Decimation.cpp" line="43"/>
        <source>This action reduce the number of triangles in a triangle mesh, forming a good approximation to the original geometry. &lt;br/&gt;&lt;br /&gt; The algorithm proceeds as follows. Each vertex in the mesh is classified and inserted into a priority queue. The priority is based on the error to delete the vertex and retriangulate the hole. Vertices that cannot be deleted or triangulated (at this point in the algorithm) are skipped. Then, each vertex in the priority queue is processed (i.e., deleted followed by hole triangulation using edge collapse). This continues until the priority queue is empty. Next, all remaining vertices are processed, and the mesh is split into separate pieces along sharp edges or at non-manifold attachment points and reinserted into the priority queue. Again, the priority queue is processed until empty. If the desired reduction is still not achieved, the remaining vertices are split as necessary (in a recursive fashion) so that it is possible to eliminate every triangle as necessary. &lt;br/&gt;&lt;br /&gt;To use this object, at a minimum you need to specify the parameter &lt;b&gt;Decimation percentage&lt;/b&gt;. The algorithm is guaranteed to generate a reduced mesh at this level as long as the following four conditions are met:&lt;ul&gt;                    &lt;li&gt;topology modification is allowed (i.e., the parameter &lt;b&gt;Preserve topology&lt;/b&gt; is false); &lt;/li&gt;                   &lt;li&gt;mesh splitting is enabled (i.e., the parameter &lt;b&gt;Splitting&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the algorithm is allowed to modify the boundaries of the mesh (i.e., the parameter &lt;b&gt;Boundary vertex deletion?&lt;/b&gt; is true); &lt;/li&gt;                   &lt;li&gt;the maximum allowable error (i.e., the parameter &lt;b&gt;Maximum error&lt;/b&gt;) is set to &lt;i&gt;1.0e+38f&lt;/i&gt; (default value). &lt;/ul&gt;             Other important parameters to adjust are the &lt;b&gt;Feature angle&lt;/b&gt; and &lt;b&gt;Split angle&lt;/b&gt; parameters, since these can impact the quality of the final mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="52"/>
        <source>Decimation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="53"/>
        <source>Simplify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="59"/>
        <source>Decimation percentage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="59"/>
        <source>Specify the desired reduction in the total number of polygons. &lt;br/&gt;Because of various constraints, this level of reduction may not be realized. If you want to guarantee a particular reduction, you must turn off &lt;b&gt;Preserve topology?&lt;/b&gt; and &lt;b&gt;Boundary vertex deletion?&lt;/b&gt;, turn on &lt;b&gt;Split mesh?&lt;/b&gt;, and set the &lt;b&gt;Maximum error&lt;/b&gt; to its maximum value (these parameters are initialized this way by default). </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="65"/>
        <source>Preserve topology?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="65"/>
        <source>Turn on/off whether to preserve the topology of the original mesh. &lt;br/&gt;If on, mesh splitting and hole elimination will not occur. This may limit the maximum reduction that may be achieved. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="68"/>
        <source>Maximum error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="68"/>
        <source>Set the largest decimation error that is allowed during the decimation process.&lt;br/&gt; This may limit the maximum reduction that may be achieved. The maximum error is specified as a fraction of the maximum length of the input data bounding box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="74"/>
        <source>Feature angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="74"/>
        <source>Specify the mesh feature angle. &lt;br/&gt;This angle is used to define what an edge is (i.e., if the surface normal between two adjacent triangles is &gt;= FeatureAngle, an edge exists). </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="80"/>
        <source>Split edge?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="80"/>
        <source>Turn on/off the splitting of the mesh at corners, along edges, at non-manifold points, or anywhere else a split is required. &lt;br/&gt;Turning splitting off will better preserve the original topology of the mesh, but you may not obtain the requested reduction. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="83"/>
        <source>Split angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="83"/>
        <source>Specify the mesh split angle. This angle is used to control the splitting of the mesh. &lt;br/&gt;A split line exists when the surface normals between two edge connected triangles are &gt;= SplitAngle. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="89"/>
        <source>Boundary vertex deletion?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="89"/>
        <source>Turn on/off the deletion of vertices on the boundary of a mesh. &lt;br/&gt;This may limit the maximum reduction that may be achieved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="92"/>
        <source>Max triangles for 1 vertex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="92"/>
        <source>If the number of triangles connected to a vertex exceeds &lt;b&gt;Max triangles for 1 vertex&lt;/b&gt;, then the vertex will be split. &lt;br/&gt;&lt;br/&gt;NOTE: the complexity of the triangulation algorithm is proportional to Degree^2. &lt;br/&gt;Setting degree small can improve the performance of the algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="97"/>
        <source>Inflection ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="97"/>
        <source>Specify the inflection point ratio. &lt;br/&gt;An inflection point occurs when the ratio of reduction error between two iterations is greater than or equal to the InflectionPointRatio. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Decimation.cpp" line="111"/>
        <source>Performing Decimation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportAsMDL</name>
    <message>
        <location filename="../../ExportAsMDL.cpp" line="48"/>
        <source>Export As MDL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExportAsMDL.cpp" line="67"/>
        <source>Save As MDL...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExportAsMDL.cpp" line="67"/>
        <source>MDL format(*.mdl)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractEdges</name>
    <message>
        <location filename="../../ExtractEdges.cpp" line="42"/>
        <source>Extract edges from a mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractEdges.cpp" line="45"/>
        <source>ExtractEdges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractEdges.cpp" line="54"/>
        <source>Extracting edges...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractSurface</name>
    <message>
        <location filename="../../ExtractSurface.cpp" line="41"/>
        <source>Extract Surface from Volumetric Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractSurface.cpp" line="44"/>
        <source>ExtractSurface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ExtractSurface.cpp" line="53"/>
        <source>Extracting Surface...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FillWithPoints</name>
    <message>
        <location filename="../../FillWithPoints.cpp" line="48"/>
        <source>Fill a surfacic mesh with regularly spaced nodes by creating new nodes inside the mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="52"/>
        <source>Add Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="54"/>
        <source>Points per bucket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="54"/>
        <source>The number of points per bucket. &lt;br /&gt;A bucket represent a group of points in the input mesh.&lt;br /&gt; The less this number is, the higher the output mesh points density will be.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="59"/>
        <source>Randomize?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FillWithPoints.cpp" line="59"/>
        <source>Randomize the position of the added points by +/- 0.5</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ICPRegistration</name>
    <message>
        <location filename="../../ICPRegistration.cpp" line="43"/>
        <source>Iterative Closest Point algorithm bewteen two mesh.&lt;br/&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="44"/>
        <source>At least two mesh components must be selected :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="46"/>
        <source>&lt;li&gt; The first one is the source mesh (the one to be registered) &lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="47"/>
        <source>&lt;li&gt; The second one is the target mesh &lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="51"/>
        <source>registration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="54"/>
        <source>Number of iterations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="54"/>
        <source>The number of iteration of the ICP algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="57"/>
        <source>Distance mesure type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ICPRegistration.cpp" line="57"/>
        <source>The distance mesure type use by the ICP algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InvertMesh</name>
    <message>
        <location filename="../../InvertMesh.cpp" line="44"/>
        <source>Invert the mesh faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InvertMesh.cpp" line="49"/>
        <source>Inside Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InvertMesh.cpp" line="50"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../InvertMesh.cpp" line="51"/>
        <source>Flip</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadTextureFromBMP</name>
    <message>
        <location filename="../../LoadTextureFromBMP.cpp" line="50"/>
        <source>LoadTextureFromBMP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../LoadTextureFromBMP.cpp" line="69"/>
        <source>Open BMP File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadTransformation</name>
    <message>
        <location filename="../../LoadTransformation.cpp" line="54"/>
        <source>LoadTransformation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MergeMeshs</name>
    <message>
        <location filename="../../MergeMeshs.cpp" line="53"/>
        <source>&lt;br&gt;Create a new mesh from two input meshs and merge them. &lt;ul&gt;                    &lt;li&gt; If points are exactly at the same coordinates, they will be merged. &lt;/li&gt;                    &lt;li&gt; If cells (tetras, triangles) are overlapped, just one is kept (Points must be exactly at the same coordinates).&lt;/li&gt;                    &lt;li&gt; If a triangle is enclosed in a volume during the merging action, it will be removed.&lt;/li&gt;&lt;/ul&gt;                    The parameters below help you to print a file which contains an indice value for each element of each mesh after they are merged. If the parameter &lt;b&gt;File path&lt;/b&gt; is empty, the file is not printed.                    &lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING:&lt;/b&gt;At this stage, this action works only with triangular surfacic mesh or tetrahedral and/or triangular volume mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="64"/>
        <source>Merge Meshes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="66"/>
        <source>Value for mesh 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="66"/>
        <location filename="../../MergeMeshs.cpp" line="71"/>
        <source>In the file, each triangle will be saved with its (indice, value)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="71"/>
        <source>Value for mesh 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="76"/>
        <source>File path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MergeMeshs.cpp" line="76"/>
        <source>The place where to save the file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeshClipping</name>
    <message>
        <location filename="../../MeshClipping.cpp" line="56"/>
        <source>Interactive Mesh Clipping in the 3D Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="60"/>
        <source>Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="61"/>
        <source>Clipping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="62"/>
        <source>3D Interaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="364"/>
        <source>Raw clipping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="366"/>
        <source>Smooth clipping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="379"/>
        <source>Clip Selected Components only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MeshClipping.cpp" line="381"/>
        <source>Clip All Components</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveDisplacementFromTransformation</name>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="52"/>
        <source>Save the displacement from the last translation in a text file. Alternatively, load a transformation from                    a text file and apply it on the selected Mesh component.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="72"/>
        <source>Save As Ansys...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="166"/>
        <source>Load Transformation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SaveDisplacementFromTransformation.cpp" line="167"/>
        <source>Output file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmoothFilter</name>
    <message>
        <location filename="../../SmoothFilter.cpp" line="47"/>
        <source>This filter adjusts point positions using Laplacian smoothing. &lt;br/&gt;It makes the cells better shaped and the vertices more evenly distributed. &lt;br /&gt; The effect is to &quot;relax&quot; the mesh, making the cells better shaped and the vertices more evenly distributed. Note that this filter operates on the lines, polygons, and triangle strips composing an instance of vtkPolyData. Vertex or poly-vertex cells are never modified. &lt;br /&gt;&lt;br /&gt;                      The algorithm proceeds as follows. For each vertex v, a topological and geometric analysis is performed to determine which vertices are connected to v, and which cells are connected to v. Then, a connectivity array is constructed for each vertex. (The connectivity array is a list of lists of vertices that directly attach to each vertex.) Next, an iteration phase begins over all vertices. For each vertex v, the coordinates of v are modified according to an average of the connected vertices. (A relaxation factor is available to control the amount of displacement of v). The process repeats for each vertex. This pass over the list of vertices is a single iteration. Many iterations (generally around 20 or so) are repeated until the desired result is obtained. &lt;br/&gt;&lt;br/&gt;                       There are some special parameters used to control the execution of this filter. (These parameters basically control what vertices can be smoothed, and the creation of the connectivity array.) The &lt;b&gt;Boundary smoothing&lt;/b&gt; paramerter enables/disables the smoothing operation on vertices that are on the &quot;boundary&quot; of the mesh. A boundary vertex is one that is surrounded by a semi-cycle of polygons (or used by a single line). &lt;br/&gt;&lt;br/&gt;                      Another important parameter is &lt;b&gt;Feature edge smoothing&lt;/b&gt;. If this ivar is enabled, then interior vertices are classified as either &quot;simple&quot;, &quot;interior edge&quot;, or &quot;fixed&quot;, and smoothed differently. (Interior vertices are manifold vertices surrounded by a cycle of polygons; or used by two line cells.) The classification is based on the number of feature edges attached to v. A feature edge occurs when the angle between the two surface normals of a polygon sharing an edge is greater than the FeatureAngle ivar. Then, vertices used by no feature edges are classified &quot;simple&quot;, vertices used by exactly two feature edges are classified &quot;interior edge&quot;, and all others are &quot;fixed&quot; vertices.&lt;br/&gt;&lt;br/&gt;                       Once the classification is known, the vertices are smoothed differently. Corner (i.e., fixed) vertices are not smoothed at all. Simple vertices are smoothed as before (i.e., average of connected vertex coordinates). Interior edge vertices are smoothed only along their two connected edges, and only if the angle between the edges is less than the EdgeAngle ivar.&lt;br/&gt;&lt;br/&gt;                      The total smoothing can be controlled by the &lt;b&gt;The Number of iterations&lt;/b&gt; which is a cap on the maximum number of smoothing passes.&lt;br/&gt;&lt;br/&gt;                      Note that this action does not create a new component, but modify the selected one(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="60"/>
        <source>SMO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="63"/>
        <source>Boundary smoothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="63"/>
        <source>Turn on/off the smoothing of vertices on the boundary of the mesh.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="66"/>
        <source>Feature edge smoothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="66"/>
        <source>Turn on/off smoothing along sharp interior edges.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="69"/>
        <source>Number of iterations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="69"/>
        <source>Specify the number of iterations for Laplacian smoothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="74"/>
        <source>Edge angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="74"/>
        <source>Specify the edge angle to control smoothing along edges (either interior or boundary).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="80"/>
        <source>Feature angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="80"/>
        <source>Specify the feature angle for sharp edge identification.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="86"/>
        <source>Relaxation factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SmoothFilter.cpp" line="86"/>
        <source>Specify the relaxation factor for Laplacian smoothing. &lt;br/&gt; As in all iterative methods, the stability of the process is sensitive to this parameter. In general, small relaxation factors and large numbers of iterations are more stable than larger relaxation factors and smaller numbers of iterations.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WarpOut</name>
    <message>
        <location filename="../../WarpOut.cpp" line="58"/>
        <source>Move the outside points along the normal in order to thicken a volumic mesh (works only with closed- centered- mesh).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="63"/>
        <source>Grow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="64"/>
        <source>Thicken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="66"/>
        <source>Displacement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="66"/>
        <source>The length of the displacement of the mesh toward its normals.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="94"/>
        <source>Warp Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="94"/>
        <source>Invalid mesh: the selected component (&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../WarpOut.cpp" line="94"/>
        <source>&quot;) is not an unstructured grid nor a polydata.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
