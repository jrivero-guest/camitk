/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <MeshComponent.h>
#include <Log.h>
#include <Application.h>
using namespace camitk;

#include <vtkUnstructuredGrid.h>
#include <vtkGeometryFilter.h>
#include <vtkCleanPolyData.h>
#include <vtkCallbackCommand.h>

#include "ExtractSurface.h"

// -------------------- ExtractSurface --------------------
ExtractSurface::ExtractSurface(ActionExtension* extension) : Action(extension) {
    this->setName("Extract Surface");
    this->setDescription(tr("Extract Surface from Volumetric Mesh"));
    this->setComponent("MeshComponent");
    this->setFamily("Mesh Processing");
    this->addTag(tr("ExtractSurface"));
}

// --------------- apply -------------------
Action::ApplyStatus ExtractSurface::apply() {
    // set waiting cursor and status bar
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    Application::showStatusBarMessage(tr("Extracting Surface..."));
    Application::resetProgressBar();
    vtkSmartPointer<vtkCallbackCommand> progressCallback = vtkSmartPointer<vtkCallbackCommand>::New();
    progressCallback->SetCallback(&Application::vtkProgressFunction);

    // use the last target
    MeshComponent* targetMesh = dynamic_cast<MeshComponent*>(getTargets().last());

    vtkSmartPointer<vtkGeometryFilter> geometryFilter = vtkSmartPointer<vtkGeometryFilter>::New();
    vtkSmartPointer<vtkCleanPolyData> cleanFilter = vtkSmartPointer<vtkCleanPolyData>::New();

    geometryFilter->SetInputData(targetMesh->getPointSet());
    cleanFilter->SetInputConnection(geometryFilter->GetOutputPort());
    cleanFilter->AddObserver(vtkCommand::ProgressEvent, progressCallback);
    cleanFilter->Update();

    // create a new mesh Component
    new MeshComponent(cleanFilter->GetOutput(), targetMesh->getName() + " surface");
    Application::refresh();

    // restore the normal cursor and progress bar
    Application::resetProgressBar();
    Application::showStatusBarMessage("");
    QApplication::restoreOverrideCursor();
    return SUCCESS;
}
