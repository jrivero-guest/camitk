/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef FILLWITHPOINTS_H
#define FILLWITHPOINTS_H

#include <Action.h>
/**
 * @ingroup group_sdk_actions_mesh_meshprocessing
 *
 * @brief
 * Fill a surfacic mesh with regularly spaced nodes (create new nodes inside the mesh).
 *
 * @note
 * Use the camitk action viewer default widget.
 *
 */
class FillWithPoints : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    FillWithPoints(camitk::ActionExtension*);

    /// the destructor
    virtual ~FillWithPoints() = default;

public slots:
    /// method called when the action is applied
    virtual ApplyStatus apply();
};

#endif // FILLWITHPOINTS_H
