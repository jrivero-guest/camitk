/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SELECT_LAST_INSTANCIATED_ACTION_H
#define SELECT_LAST_INSTANCIATED_ACTION_H

#include <Action.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * Save the current selected component.
 * This action allows one to select the last instantiated component.
 *
 * \note only the last instantiated component will be selected
 *
 * \note the name of this class and the name of the action is mispelled.
 * The spelling mistake is kept for API backward compatibility reason.
 */
class SelectLastInstanciatedAction : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    SelectLastInstanciatedAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~SelectLastInstanciatedAction() = default;

    /// Returns NULL: no widget at all for this action
    virtual QWidget* getWidget();

public slots:
    /// apply the action select the last instantiated component
    virtual ApplyStatus apply();

};
#endif // SELECT_LAST_INSTANCIATED_ACTION_H
