/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ApplicationActionExtension.h"

#include <Action.h>
#include "OpenAction.h"
#include "OpenFile.h"
#include "SelectLastInstanciatedAction.h"
#include "ClearSelectionAction.h"
#include "SetPathToTestData.h"
#include "SaveAction.h"
#include "CloseAction.h"
#include "CloseAllAction.h"
#include "SaveAsAction.h"
#include "SaveAllAction.h"
#include "QuitAction.h"
#include "ToggleConsoleAction.h"
#include "AboutAction.h"
#include "RemoveLastInstanciatedAction.h"
#include "ShowAxialViewer.h"
#include "ShowCoronalViewer.h"
#include "ShowSagittalViewer.h"
#include "Show3DViewer.h"
#include "ShowAllViewers.h"
#include "ShowArbitraryViewer.h"
#include "ChangeLanguage.h"
#include "LoggerParameters.h"

// --------------- constructor -------------------
ApplicationActionExtension::ApplicationActionExtension() : ActionExtension() {};

// --------------- init -------------------
void ApplicationActionExtension::init() {
    registerNewAction(OpenAction);
    registerNewAction(OpenFile);
    registerNewAction(SelectLastInstanciatedAction);
    registerNewAction(ClearSelectionAction);
    registerNewAction(SetPathToTestData);
    registerNewAction(SaveAction);
    registerNewAction(SaveAsAction);
    registerNewAction(SaveAllAction);
    registerNewAction(CloseAction);
    registerNewAction(CloseAllAction);
    registerNewAction(QuitAction);
    registerNewAction(ToggleConsoleAction);
    registerNewAction(AboutAction);
    registerNewAction(RemoveLastInstanciatedAction);
    registerNewAction(ChangeLanguage);
    // -- medical image viewer manipulation
    registerNewAction(ShowAxialViewer);
    registerNewAction(ShowCoronalViewer);
    registerNewAction(ShowSagittalViewer);
    registerNewAction(Show3DViewer);
    registerNewAction(ShowAllViewers);
    // disabled as long as this viewer is not working
    // registerNewAction(ShowArbitraryViewer);
    registerNewAction(LoggerParameters);
}
