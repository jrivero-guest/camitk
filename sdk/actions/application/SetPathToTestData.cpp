/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "SetPathToTestData.h"

// CamiTK
#include <Core.h>
#include <Log.h>

using namespace camitk;

// --------------- constructor -------------------
SetPathToTestData::SetPathToTestData(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Set Path To Test Data");
    setEmbedded(false);
    setDescription(tr("Set the working directory path to the test data dir (useful for test purpose)"));
    setComponent("");

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Path"));
    addTag(tr("Test"));
    addTag(tr("TestData"));
}

// --------------- getWidget --------------
QWidget* SetPathToTestData::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus SetPathToTestData::apply() {
    if (QDir::setCurrent(Core::getTestDataDir())) {
        return SUCCESS;
    }
    else {
        CAMITK_ERROR(tr("Can not set current working directory to path \"%1\"").arg(Core::getTestDataDir()))
        return ERROR;
    }
}

