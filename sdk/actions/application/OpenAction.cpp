/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "OpenAction.h"

// CamiTK
#include <Application.h>
#include <Log.h>
#include <ExtensionManager.h>

// Qt
#include <QFileDialog>

using namespace camitk;

// --------------- constructor -------------------
OpenAction::OpenAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Open");
    setEmbedded(false);
    setDescription(tr("Open data (component) from a file"));
    setComponent("");

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Open"));

    // add a shortcut
    setIcon(QPixmap(":/fileOpen"));
    getQAction()->setShortcut(QKeySequence::Open);
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);
}

// --------------- destructor -------------------
OpenAction::~OpenAction() {
}

// --------------- getWidget --------------
QWidget* OpenAction::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus OpenAction::apply() {
    Application::showStatusBarMessage(tr("Opening file..."));

    // filter: extension of known file format
    QString filter;
    QString allKnownFiles; //example: (*.xml *.vtk *.wrl)

    // first item = all known files
    filter += QString("All known files (*.") + ExtensionManager::getFileExtensions().join(" *.") + ");;";

    // add the extension of loaded and valid plugins
    foreach (ComponentExtension* ext, ExtensionManager::getComponentExtensionsList()) {
        filter += ext->getName() + " (*." + ext->getFileExtensions().join(" *.") + ");;";
    }

    // Open more than one file
    QStringList files = QFileDialog::getOpenFileNames(NULL, tr("Select One or More Files to Open"), Application::getLastUsedDirectory().absolutePath(), filter);

    if (!files.isEmpty()) {
        QStringList listOfFiles = files; // as stated by the Qt doc (although without further information): "If you want to iterate over the list of files, you should iterate over a copy."
        QStringList::const_iterator fileIterator = listOfFiles.begin();
        while (fileIterator != listOfFiles.constEnd() && Application::open(*fileIterator) != NULL) {
            ++fileIterator;
        }
        if (fileIterator == listOfFiles.constEnd()) {
            Application::showStatusBarMessage(tr("All files opened."));
            return SUCCESS;
        }
        else {
            Application::showStatusBarMessage(tr("Error opening files: ") + (*fileIterator));
            CAMITK_ERROR(tr("Some files can not be opened. Action failed."))
            return ERROR;
        }
    }
    else {
        Application::showStatusBarMessage(tr("Open aborted."));
        // CCC Exception: just a trace message as the user voluntarily aborted the action
        CAMITK_TRACE(tr("A file is required. Action aborted."))
        return ABORTED;
    }
}


