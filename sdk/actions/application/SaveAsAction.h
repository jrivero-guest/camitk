/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SAVEASACTION_H
#define SAVEASACTION_H

#include <QObject>
#include <Action.h>
#include <Component.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * Save as the current selected component.
 */
class SaveAsAction : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    SaveAsAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~SaveAsAction();

    /// Returns NULL: no permanent widget for this action. The GUI is run shown a one-shot dialog in apply
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
    * It ask for a filename and call ExtensionManager saveAs method
    */
    virtual ApplyStatus apply();

};
#endif // SAVEASACTION_H
