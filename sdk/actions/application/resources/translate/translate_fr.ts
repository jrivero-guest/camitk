<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AboutAction</name>
    <message>
        <location filename="../../AboutAction.cpp" line="43"/>
        <source>Show a little dialog about the application</source>
        <translation>Montrer une petite information à propos de l&apos;application</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="49"/>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="65"/>
        <source>About </source>
        <translation>A propos</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="76"/>
        <source>Build with </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="79"/>
        <source>Please visit &lt;a href=&apos;http://camitk.imag.fr&apos;&gt;camitk.imag.fr&lt;/a&gt; for more information.&lt;br/&gt;</source>
        <translation>Veuillez visiter &lt;a href=&apos;http://camitk.imag.fr&apos;&gt;camitk.imag.fr&lt;/a&gt; pour plus d&apos;informations.&lt;br/&gt;</translation>
    </message>
    <message>
        <location filename="../../AboutAction.cpp" line="80"/>
        <source>(c) UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>BugReportDialog</name>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="14"/>
        <source>Report Bug Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="32"/>
        <source>Generated bug report to complete and paste in the :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="74"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../src/sdk/actions/application/BugReportDialog.ui" line="81"/>
        <source>Copy to Clilpboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangeLanguage</name>
    <message>
        <location filename="../../ChangeLanguage.cpp" line="145"/>
        <source>Information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ChangeLanguage.cpp" line="145"/>
        <source>To apply translation, restart the application !</source>
        <translation>Pour appliquer la traduction, redémarrer votre application !</translation>
    </message>
</context>
<context>
    <name>ClearSelectionAction</name>
    <message>
        <location filename="../../ClearSelectionAction.cpp" line="38"/>
        <source>Clear the list of selected item</source>
        <translation>Effacer la liste des items selectionnés</translation>
    </message>
    <message>
        <location filename="../../ClearSelectionAction.cpp" line="43"/>
        <source>Clear Selection</source>
        <translation>Effacer la sélection</translation>
    </message>
</context>
<context>
    <name>CloseAction</name>
    <message>
        <location filename="../../CloseAction.cpp" line="36"/>
        <source>Close the currently selected components</source>
        <translation>Fermer les components couramment sélectionnés</translation>
    </message>
    <message>
        <location filename="../../CloseAction.cpp" line="42"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>CloseAllAction</name>
    <message>
        <location filename="../../CloseAllAction.cpp" line="59"/>
        <source>Closing all the documents...</source>
        <translation>Fermeture de tous les documents ...</translation>
    </message>
    <message>
        <source>Close all components, prompting for additional information if needed</source>
        <translation type="obsolete">Fermer tous les components </translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Fermer</translation>
    </message>
</context>
<context>
    <name>OpenAction</name>
    <message>
        <location filename="../../OpenAction.cpp" line="41"/>
        <source>Open data (component) from a file</source>
        <translation>Ouvrir les données (component) à partir d&apos;un fichier</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="48"/>
        <source>Open</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="68"/>
        <source>Opening file...</source>
        <translation>Ouverture du fichier ...</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="83"/>
        <source>Select One or More Files to Open</source>
        <translation>Sélectionner Un ou Plusieurs Fichiers à Ouvrir</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="91"/>
        <source>All files loaded.</source>
        <translation>Tous les fichiers sont chargés.</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="94"/>
        <source>Error loading files: </source>
        <translation>Erreur au chargement des fichiers :</translation>
    </message>
    <message>
        <location filename="../../OpenAction.cpp" line="98"/>
        <source>Open aborted.</source>
        <translation>Ouverture abandonnée.</translation>
    </message>
</context>
<context>
    <name>OpenFile</name>
    <message>
        <location filename="../../OpenFile.cpp" line="45"/>
        <source>Open data (component) from a given file</source>
        <translation>Ouvrir les données (component) à partir d&apos;un fichier donné</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="53"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="62"/>
        <source>File name</source>
        <translation>Nom du fichier</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="62"/>
        <source>The name of the file to open.</source>
        <translation>Le nom du fichier à ouvrir.</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="85"/>
        <source>File Name:</source>
        <translation>Nom du Fichier:</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="89"/>
        <source>Browse</source>
        <translation>Naviguer</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="100"/>
        <source>Open Selected File</source>
        <translation>Ouvrir le Fichier Sélectionné</translation>
    </message>
    <message>
        <location filename="../../OpenFile.cpp" line="143"/>
        <source>Select One File to Open</source>
        <translation>Sélectionner Un Fichier à Ouvrir</translation>
    </message>
</context>
<context>
    <name>QuitAction</name>
    <message>
        <location filename="../../QuitAction.cpp" line="39"/>
        <source>Exit the application, prompting for additional information if needed</source>
        <translation>Sortir de l&apos;application </translation>
    </message>
    <message>
        <location filename="../../QuitAction.cpp" line="45"/>
        <source>Quit Application</source>
        <translation>Quitter l&apos;Application</translation>
    </message>
    <message>
        <location filename="../../QuitAction.cpp" line="46"/>
        <source>Exit Application</source>
        <translation>Sortir de l&apos;Application</translation>
    </message>
</context>
<context>
    <name>RemoveLastInstanciatedAction</name>
    <message>
        <location filename="../../RemoveLastInstanciatedAction.cpp" line="39"/>
        <source>Remove the last top level component that was instanciated without saving it</source>
        <translation>Supprimer le dernier top level component qui était initialisé sans être sauvé</translation>
    </message>
    <message>
        <location filename="../../RemoveLastInstanciatedAction.cpp" line="44"/>
        <source>Delete Component</source>
        <translation>Détruire Component</translation>
    </message>
</context>
<context>
    <name>SaveAction</name>
    <message>
        <location filename="../../SaveAction.cpp" line="37"/>
        <source>Save all the top-level of the selected components</source>
        <translation>Sauver le top-level des components sélectionnés</translation>
    </message>
    <message>
        <location filename="../../SaveAction.cpp" line="43"/>
        <source>Save</source>
        <translation>Sauver</translation>
    </message>
</context>
<context>
    <name>SaveAllAction</name>
    <message>
        <location filename="../../SaveAllAction.cpp" line="43"/>
        <source>Saves all the currently loaded data</source>
        <translation>Sauver toutes les données chargées courantes</translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="49"/>
        <source>Save All</source>
        <translation>Tout Sauver</translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="64"/>
        <source>Saving all data...</source>
        <translation>Sauvegarde de toutes les données ...</translation>
    </message>
    <message>
        <location filename="../../SaveAllAction.cpp" line="73"/>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
</context>
<context>
    <name>SaveAsAction</name>
    <message>
        <location filename="../../SaveAsAction.cpp" line="44"/>
        <source>Save the currently selected data under a different filename or format</source>
        <translation>Sauver les données sélectionnées courantes sous un fichier ou format différent</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="50"/>
        <source>Save As</source>
        <translation>Sauver Comme</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="71"/>
        <source>Saving currently selected component under new filename or format...</source>
        <translation>Sauvegarde du componet récemment selectionné sous un nouveau non de fichier ou format ...</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="172"/>
        <location filename="../../SaveAsAction.cpp" line="177"/>
        <source>Save File As...</source>
        <translation>Sauvegarde du Fichier sous ...</translation>
    </message>
    <message>
        <location filename="../../SaveAsAction.cpp" line="193"/>
        <source>Saving aborted</source>
        <translation>Sauvegarde abandonnée</translation>
    </message>
</context>
<context>
    <name>SelectLastInstanciatedAction</name>
    <message>
        <location filename="../../SelectLastInstanciatedAction.cpp" line="38"/>
        <source>Select the last top level component that was instanciated</source>
        <translation>Sélectionner le dernier component top-level qui était initialisé</translation>
    </message>
    <message>
        <location filename="../../SelectLastInstanciatedAction.cpp" line="43"/>
        <source>Select Component</source>
        <translation>Sélectionner le Component</translation>
    </message>
</context>
<context>
    <name>SetPathToTestData</name>
    <message>
        <location filename="../../SetPathToTestData.cpp" line="39"/>
        <source>Set the working directory path to the test data dir (useful for test purpose)</source>
        <translation>Initialise le chemin du répertoire de travail avec le repertoire test data (utile pour les tests)</translation>
    </message>
    <message>
        <location filename="../../SetPathToTestData.cpp" line="44"/>
        <source>Path</source>
        <translation>Chemin</translation>
    </message>
    <message>
        <location filename="../../SetPathToTestData.cpp" line="45"/>
        <source>Test</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../SetPathToTestData.cpp" line="46"/>
        <source>TestData</source>
        <translation>Donnée de Test</translation>
    </message>
</context>
<context>
    <name>Show3DViewer</name>
    <message>
        <location filename="../../Show3DViewer.cpp" line="35"/>
        <source>Show the 3D Viewer Only</source>
        <translation>Montrer seulement l&apos;Afficheur 3D</translation>
    </message>
    <message>
        <location filename="../../Show3DViewer.cpp" line="40"/>
        <source>3D Viewer</source>
        <translation>Afficheur 3D</translation>
    </message>
    <message>
        <location filename="../../Show3DViewer.cpp" line="41"/>
        <source>Geometry</source>
        <translation>Géométrie</translation>
    </message>
    <message>
        <location filename="../../Show3DViewer.cpp" line="44"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
</context>
<context>
    <name>ShowAllViewers</name>
    <message>
        <location filename="../../ShowAllViewers.cpp" line="35"/>
        <source>Show Classical Medical Image Viewer (Axial, Coronal, Sagittal and 3D in a 4 panels)</source>
        <translation>Montrer l&apos;Afficheur d&apos;Images Médicales Classique (Axial, Coronale, Sagittale et 3D en 4 panneaux)</translation>
    </message>
    <message>
        <location filename="../../ShowAllViewers.cpp" line="40"/>
        <source>All Viewers</source>
        <translation>Tous les Afficheurs</translation>
    </message>
    <message>
        <location filename="../../ShowAllViewers.cpp" line="41"/>
        <source>Medical Image Viewer</source>
        <translation>Afficheur d&apos;Image Médicale</translation>
    </message>
    <message>
        <location filename="../../ShowAllViewers.cpp" line="44"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
</context>
<context>
    <name>ShowArbitraryViewer</name>
    <message>
        <location filename="../../ShowArbitraryViewer.cpp" line="35"/>
        <source>Show the Arbitrary Slice Viewer Only</source>
        <translation>Montrer Seulement l&apos;Afficheur de Tranche Arbitraire</translation>
    </message>
    <message>
        <location filename="../../ShowArbitraryViewer.cpp" line="40"/>
        <source>Arbitrary Slice Viewer</source>
        <translation>Afficheur de Tranche Arbitraire</translation>
    </message>
    <message>
        <location filename="../../ShowArbitraryViewer.cpp" line="41"/>
        <source>Slice</source>
        <translation>Coupe</translation>
    </message>
    <message>
        <location filename="../../ShowArbitraryViewer.cpp" line="44"/>
        <source>Ctrl+5</source>
        <translation>Ctrl+5</translation>
    </message>
</context>
<context>
    <name>ShowAxialViewer</name>
    <message>
        <location filename="../../ShowAxialViewer.cpp" line="35"/>
        <source>Show the Axial Viewer Only</source>
        <translation>Montrer seulement l&apos;Afficheur Axial</translation>
    </message>
    <message>
        <location filename="../../ShowAxialViewer.cpp" line="40"/>
        <source>Axial Viewer</source>
        <translation>Afficheur Axial</translation>
    </message>
    <message>
        <location filename="../../ShowAxialViewer.cpp" line="41"/>
        <source>Slice</source>
        <translation>Coupe</translation>
    </message>
    <message>
        <location filename="../../ShowAxialViewer.cpp" line="44"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
</context>
<context>
    <name>ShowConsoleAction</name>
    <message>
        <location filename="../../ShowConsoleAction.cpp" line="37"/>
        <source>Show the redirection console window (view debugging information, cout, cerr,...)</source>
        <translation>Montrer la fenêtre de la redirection de la console  (view debugging information, cout, cerr,...)</translation>
    </message>
    <message>
        <location filename="../../ShowConsoleAction.cpp" line="43"/>
        <source>Console</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ShowConsoleAction.cpp" line="44"/>
        <source>Terminal</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ShowCoronalViewer</name>
    <message>
        <location filename="../../ShowCoronalViewer.cpp" line="35"/>
        <source>Show the Coronal Viewer Only</source>
        <translation>Montrer Seulement l&apos;Afficheur Coronale</translation>
    </message>
    <message>
        <location filename="../../ShowCoronalViewer.cpp" line="40"/>
        <source>Coronal Viewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../ShowCoronalViewer.cpp" line="41"/>
        <source>Slice</source>
        <translation>Coupe</translation>
    </message>
    <message>
        <location filename="../../ShowCoronalViewer.cpp" line="44"/>
        <source>Ctrl+4</source>
        <translation>Ctrl+4</translation>
    </message>
</context>
<context>
    <name>ShowSagittalViewer</name>
    <message>
        <location filename="../../ShowSagittalViewer.cpp" line="35"/>
        <source>Show the Sagittal Viewer Only</source>
        <translation>Montrer Seulement l&apos;Afficheur Sagittal</translation>
    </message>
    <message>
        <location filename="../../ShowSagittalViewer.cpp" line="40"/>
        <source>Sagittal Viewer</source>
        <translation>Afficheur Sagittal</translation>
    </message>
    <message>
        <location filename="../../ShowSagittalViewer.cpp" line="41"/>
        <source>Slice</source>
        <translation>Coupe</translation>
    </message>
    <message>
        <location filename="../../ShowSagittalViewer.cpp" line="44"/>
        <source>Ctrl+3</source>
        <translation>Ctrl+3</translation>
    </message>
</context>
<context>
    <name>ReportBugDialog</name>
    <message>
        <location filename="../../../../src/sdk/actions/application/ReportBugDialog.cpp" line="40"/>
        <source>&lt;a href=&apos;https://bugzilla-timc.imag.fr/&apos;&gt;Bugzilla Bug Report Tool&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../../../Dev/CamiTK/camitk/sdk/actions/application/ReportBugDialog.cpp" line="63"/>
        <source>&lt;b&gt;&lt;font color=&apos;red&apos;&gt;Impossible to load </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
