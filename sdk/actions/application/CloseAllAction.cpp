/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "CloseAllAction.h"

#include <Application.h>
#include <Log.h>

using namespace camitk;

// --------------- constructor -------------------
CloseAllAction::CloseAllAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Close All");
    setEmbedded(false);
    setDescription("Close all components, prompting for additional information if needed");
    setComponent("");
    setIcon(QPixmap(":/fileClose"));

    // Setting classification family and tags
    setFamily("Application");
    addTag("Close");

}

// --------------- destructor -------------------
CloseAllAction::~CloseAllAction() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* CloseAllAction::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus CloseAllAction::apply() {

    Application::showStatusBarMessage(tr("Closing all the documents..."));

    // clear the selection
    Application::clearSelectedComponents();

    // close 'em all
    bool continueClosing = true;
    while (continueClosing && Application::getTopLevelComponents().size() > 0) {
        Component* comp = Application::getTopLevelComponents().first();
        continueClosing = Application::close(comp);
    }

    if (continueClosing) {
        return SUCCESS;
    }
    else {
        // CCC Exception: just a trace message as the user voluntarily aborted the action
        CAMITK_TRACE(tr("Canceled by user. Action Aborted."))
        return ABORTED;
    }
}

