/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "AboutAction.h"
#include "ReportBugDialog.h"

#include <Application.h>
#include <Core.h>
using namespace camitk;

#include <QBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QPushButton>

#include <vtkVersion.h>

// --------------- constructor -------------------
AboutAction::AboutAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("About...");
    setEmbedded(false);
    setDescription(tr("Show a little dialog about the application"));
    setComponent("");
    setIcon(QPixmap(":/camiTKIcon"));

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("About"));
}

// --------------- destructor -------------------
AboutAction::~AboutAction() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* AboutAction::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus AboutAction::apply() {
    QDialog* aboutDialog = new QDialog();
    aboutDialog->setWindowTitle(tr("About ") + Application::getName() + "...");
    QVBoxLayout* dialogLayout = new QVBoxLayout;
    QHBoxLayout* informationLayout = new QHBoxLayout;
    QGridLayout* buttonGrid = new QGridLayout;
    QPushButton* reportBugButton = new QPushButton(aboutDialog);
    reportBugButton->setText("Report Bug");

    ReportBugDialog* bugDialog = new ReportBugDialog();

    bugDialog->setBugReport();

    QLabel* logo = new QLabel;
    logo->setPixmap(QPixmap(":/camiTKIcon"));

    QLabel* text = new QLabel;
    text->setTextFormat(Qt::RichText);
    text->setOpenExternalLinks(true);
    text->setText("<b>" + Application::getName() + "</b><br/>"
                  + tr("Build with ")
                  + QString(Core::version) + "<br/>"
                  + "VTK: " + QString(vtkVersion::GetVTKVersion()) + " - Qt: " + QString(qVersion()) + "<br/><br/>"
                  + tr("Please visit <a href='http://camitk.imag.fr'>camitk.imag.fr</a> for more information.<br/>")
                  + tr("(c) Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525"));

    // get the proper text height
    text->adjustSize();
    // fix the logo's height
    logo->setFixedSize(text->height(), text->height());
    logo->setScaledContents(true);

    informationLayout->addWidget(logo);
    informationLayout->addWidget(text);
    dialogLayout->addLayout(informationLayout);
    dialogLayout->addLayout(buttonGrid);

    QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    connect(buttonBox, SIGNAL(accepted()), aboutDialog, SLOT(accept()));
    connect(reportBugButton, SIGNAL(clicked()), bugDialog, SLOT(exec()));
    buttonGrid->addWidget(buttonBox, 0, 1);
    buttonGrid->addWidget(reportBugButton, 0, 2);

    aboutDialog->setLayout(dialogLayout);
    aboutDialog->setWindowIcon(QPixmap(":/camiTKIcon"));
    aboutDialog->exec();

    return SUCCESS;
}
