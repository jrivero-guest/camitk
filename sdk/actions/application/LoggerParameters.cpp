/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "LoggerParameters.h"

#include <Log.h>
#include <Property.h>

#include <QFileDialog>


using namespace camitk;

// --------------- constructor -------------------
LoggerParameters::LoggerParameters(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Logger Parameters");
    setDescription(tr("Play along with logger parameters: level and other specifications.<br/><b>Note:</b> This action does <i>only</i> modify the log interaction temporarily. Please use the application preference dialog or the console tool buttons to save the logger parameter in settings."));
    setComponent("");

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Log"));

    myWidget = nullptr;
}

// --------------- destructor -------------------
LoggerParameters::~LoggerParameters() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget -------------------
QWidget* LoggerParameters::getWidget() {
    if (myWidget == NULL) {
        myWidget = new QWidget();
        ui.setupUi(myWidget);

        //-- Logger levels

        for (const InterfaceLogger::LogLevel l : {
        InterfaceLogger::NONE, InterfaceLogger::ERROR, InterfaceLogger::WARNING, InterfaceLogger::INFO, InterfaceLogger::TRACE
    }) {
            ui.loggerLevelComboBox->addItem(Log::getLevelAsString(l));
        }
        ui.loggerLevelComboBox->setCurrentText(Log::getLevelAsString(Log::getLogger()->getLogLevel()));

        for (const InterfaceLogger::LogLevel l : {
        InterfaceLogger::NONE, InterfaceLogger::ERROR, InterfaceLogger::WARNING, InterfaceLogger::INFO, InterfaceLogger::TRACE
    }) {
            ui.loggerMessageBoxLevelComboBox->addItem(Log::getLevelAsString(l));
        }
        ui.loggerMessageBoxLevelComboBox->setCurrentText(Log::getLevelAsString(Log::getLogger()->getMessageBoxLevel()));

        ui.logDebugInfoCheckBox->setChecked(Log::getLogger()->getDebugInformation());
        ui.logTimeStampInfoCheckBox->setChecked(Log::getLogger()->getTimeStampInformation());

        //-- Logger outputs

        ui.logToStdOutCheckBox->setChecked(Log::getLogger()->getLogToStandardOutput());
        ui.logToFileCheckBox->setChecked(Log::getLogger()->getLogToFile());
        ui.logFileDirectoryLineEdit->setText(Log::getLogger()->getLogFileInfo().path());
        ui.logFileNameLineEdit->setText(Log::getLogger()->getLogFileInfo().fileName());

        enableLogToFileParameters();

        connect(ui.loggerLevelComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(loggerLevelChanged()));
        connect(ui.loggerMessageBoxLevelComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(messageBoxlLevelChanged()));
        connect(ui.logDebugInfoCheckBox, SIGNAL(stateChanged(int)), this, SLOT(logDebugInfoChanged()));
        connect(ui.logTimeStampInfoCheckBox, SIGNAL(stateChanged(int)), this, SLOT(logTimeStampInfoChanged()));

        connect(ui.logToStdOutCheckBox, SIGNAL(stateChanged(int)), this, SLOT(logToStdOutChanged()));
        connect(ui.logToFileCheckBox, SIGNAL(stateChanged(int)), this, SLOT(logToFileChanged()));
        connect(ui.logFileDirectoryLineEdit, SIGNAL(editingFinished()), this, SLOT(logFileDirectoryTextEditChanged()));
        connect(ui.logFileDirectoryPushButton, SIGNAL(clicked()), this, SLOT(logFileDirectoryButtonClicked()));
        connect(ui.tracePushButton,   SIGNAL(clicked()), this, SLOT(traceButtonClicked()));
        connect(ui.infoPushButton,    SIGNAL(clicked()), this, SLOT(infoButtonClicked()));
        connect(ui.warningPushButton, SIGNAL(clicked()), this, SLOT(warningButtonClicked()));
        connect(ui.errorPushButton,   SIGNAL(clicked()), this, SLOT(errorButtonClicked()));
    }
    return myWidget;
}

// --------------- enableLogToFileParameters -------------------
void LoggerParameters::enableLogToFileParameters() {
    bool enabled = Log::getLogger()->getLogToFile();
    ui.logFileDirectoryLineEdit->setReadOnly(!enabled);
    ui.logFileDirectoryPushButton->setEnabled(enabled);
}


// --------------- loggerLevelChanged -------------------
void LoggerParameters::loggerLevelChanged() {
    QString levelString = ui.loggerLevelComboBox->currentText();
    InterfaceLogger::LogLevel logLevel = Log::getLevelFromString(levelString);

    Log::getLogger()->setLogLevel(logLevel);
}

// --------------- messageBoxlLevelChanged -------------------
void LoggerParameters::messageBoxlLevelChanged() {
    QString levelString = ui.loggerMessageBoxLevelComboBox->currentText();
    InterfaceLogger::LogLevel logLevel = Log::getLevelFromString(levelString);

    Log::getLogger()->setMessageBoxLevel(logLevel);

}

// --------------- logDebugInfoChanged -------------------
void LoggerParameters::logDebugInfoChanged() {
    Log::getLogger()->setDebugInformation(ui.logDebugInfoCheckBox->isChecked());
}

// --------------- logTimeStampInfoChanged -------------------
void LoggerParameters::logTimeStampInfoChanged() {
    Log::getLogger()->setTimeStampInformation(ui.logTimeStampInfoCheckBox->isChecked());
}

// --------------- logToStdOutChanged -------------------
void LoggerParameters::logToStdOutChanged() {
    Log::getLogger()->setLogToStandardOutput(ui.logToStdOutCheckBox->isChecked());
}

// --------------- logToFileChanged -------------------
void LoggerParameters::logToFileChanged() {
    Log::getLogger()->setLogToFile(ui.logToFileCheckBox->isChecked());
    enableLogToFileParameters();
}

// --------------- logFileDirectoryButtonClicked -------------------
void LoggerParameters::logFileDirectoryButtonClicked() {
    bool signalState = ui.logFileDirectoryLineEdit->blockSignals(true);
    QString directory = QFileDialog::getExistingDirectory(NULL, "Please choose a directory where to save log file");
    ui.logFileDirectoryLineEdit->setText(directory);
    Log::getLogger()->setLogFileDirectory(directory);
    Log::getLogger()->setLogToFile(true);
    ui.logFileNameLineEdit->setText(Log::getLogger()->getLogFileInfo().fileName());

    ui.logFileDirectoryLineEdit->blockSignals(signalState);
}

// --------------- logFileDirectoryTextEditChanged -------------------
void LoggerParameters::logFileDirectoryTextEditChanged() {
    QString directory = ui.logFileDirectoryLineEdit->text();
    Log::getLogger()->setLogFileDirectory(directory);
    Log::getLogger()->setLogToFile(true);
    ui.logFileNameLineEdit->setText(Log::getLogger()->getLogFileInfo().fileName());
}

// --------------- traceButtonClicked -------------------
void LoggerParameters::traceButtonClicked() {
    QString logMessage = ui.logMessageLineEdit->text();
    CAMITK_TRACE(logMessage)
}

// --------------- infoButtonClicked -------------------
void LoggerParameters::infoButtonClicked() {
    QString logMessage = ui.logMessageLineEdit->text();
    CAMITK_INFO(logMessage)
}

// --------------- warningButtonClicked -------------------
void LoggerParameters::warningButtonClicked() {
    QString logMessage = ui.logMessageLineEdit->text();
    CAMITK_WARNING(logMessage)
}

// --------------- errorButtonClicked -------------------
void LoggerParameters::errorButtonClicked() {
    QString logMessage = ui.logMessageLineEdit->text();
    CAMITK_ERROR(logMessage)
}

// --------------- apply -------------------
Action::ApplyStatus LoggerParameters::apply() {

    return SUCCESS;
}




