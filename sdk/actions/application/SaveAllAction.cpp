/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "SaveAllAction.h"

#include <Application.h>
#include <ImageComponent.h>
#include <ImageComponentExtension.h>
#include <MeshComponent.h>
#include <MeshComponentExtension.h>
#include <Log.h>

#include <QFileDialog>

using namespace camitk;


// --------------- constructor -------------------
SaveAllAction::SaveAllAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Save All");
    setEmbedded(false);
    setDescription(tr("Saves all the currently loaded data"));
    setComponent("Component");
    setIcon(QPixmap(":/fileSaveAll"));

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Save All"));
}

// --------------- destructor -------------------
SaveAllAction::~SaveAllAction() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* SaveAllAction::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus SaveAllAction::apply() {
    Application::showStatusBarMessage(tr("Saving all data..."));

    // and save 'em all
    QList<Component*>::const_iterator it = Application::getTopLevelComponents().constBegin();
    while (it != Application::getTopLevelComponents().constEnd() && Application::save(*it)) {
        ++it;
    }

    Application::showStatusBarMessage(tr("Ready."));

    if (it == Application::getTopLevelComponents().constEnd()) {
        return SUCCESS;
    }
    else {
        CAMITK_WARNING(tr("Canceled. Action Aborted."))
        return ABORTED;
    }
}

