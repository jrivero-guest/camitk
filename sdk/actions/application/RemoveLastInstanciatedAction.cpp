/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

//-- CamiTK
#include <Application.h>
#include <Component.h>
#include <Log.h>

#include "RemoveLastInstanciatedAction.h"

using namespace camitk;

// --------------- constructor -------------------
RemoveLastInstanciatedAction::RemoveLastInstanciatedAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Remove Last Instanciated Top Level Component");
    setEmbedded(false);
    setDescription(tr("Remove the last top level component that was instantiated without saving it"));
    setComponent("");

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Delete Component"));
}

// --------------- getWidget --------------
QWidget* RemoveLastInstanciatedAction::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus RemoveLastInstanciatedAction::apply() {
    const ComponentList& allComponent = Application::getAllComponents();

    if (allComponent.size() > 0) {
        // Select the last instantiated component
        Component* lastComponent = allComponent.last();

        // select the top level component to remove
        while (lastComponent->getParentComponent() != NULL) {
            lastComponent = lastComponent->getParentComponent();
        }

        delete lastComponent;
        // calling the component destructor is suffisient, it automatically updates the application lists
        Application::refresh();
        return SUCCESS;
    }

    CAMITK_ERROR(tr("No component to delete."))
    return ERROR;
}
