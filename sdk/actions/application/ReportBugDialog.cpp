/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/

#include "ReportBugDialog.h"

// Qt stuff
#include <QClipboard>

// -------------------- constructor --------------------
ReportBugDialog::ReportBugDialog(QDialog* parent) : QDialog(parent) {
    ui = new Ui::BugReportDialog();

    ui->setupUi(this);
    connect(ui->copyButton, SIGNAL(clicked()), this, SLOT(copyReportToClipBoard()));

    ui->bugReportTextEdit->setText("<font color='red'>Processing, please wait.....</font>");
}

// -------------------- showBugReport --------------------
void ReportBugDialog::showBugReport(const QString& bugReport) {
    ui->bugReportTextEdit->setText(bugReport);
}

// -------------------- setBugReport --------------------
void ReportBugDialog::setBugReport() {
    auto* bGProcess = new GenerateBugReportProcess();
    connect(bGProcess, SIGNAL(resultReady(QString)), this, SLOT(showBugReport(QString)));
    connect(bGProcess, SIGNAL(finished()), bGProcess, SLOT(deleteLater()));
    bGProcess->start();
}

// -------------------- copyReportToClipBoard --------------------
void ReportBugDialog::copyReportToClipBoard() {
    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(ui->bugReportTextEdit->toPlainText());
}

