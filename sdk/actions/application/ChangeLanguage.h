/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CHANGE_LANGUAGE_H
#define CHANGE_LANGUAGE_H

#include <Action.h>

class QFrame;
class QLineEdit;
class QComboBox;

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * When triggered, the ChangeLanguage automatically opens the given setting file language.
 */
class ChangeLanguage : public camitk::Action {
    Q_OBJECT
public:
    /// Default Constructor
    ChangeLanguage(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~ChangeLanguage();
    /// Returns NULL: no permanent widget for this action. The GUI is shown as a one-shot dialog in apply
    virtual QWidget* getWidget();

public slots:

    /// apply the action i.e. show a file dialog and open the selected file(s)
    virtual ApplyStatus apply();

private:
    QFrame* myWidget;
    QLineEdit* fileBrowserLineEdit1;
    QLineEdit* fileBrowserLineEdit2;
    QComboBox* languageList;

    struct languageAndFlag {
        QString language;
        QString languageAbreviation;
        QString flagfilename;
    };
    QList <languageAndFlag> listLanguageAndFlag;
};
#endif // OPEN_FILE_H
