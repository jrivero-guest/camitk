/*****************************************************************************
* $CAMITK_LICENCE_BEGIN$
*
* CamiTK - Computer Assisted Medical Intervention ToolKit
* (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
*
* Visit http://camitk.imag.fr for more information
*
* This file is part of CamiTK.
*
* CamiTK is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License version 3
* only, as published by the Free Software Foundation.
*
* CamiTK is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License version 3 for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
*
* $CAMITK_LICENCE_END$
****************************************************************************/

#ifndef REPORTBUGWIDGET_H
#define REPORTBUGWIDGET_H

#include "ui_BugReportDialog.h"
#include "GenerateBugReportProcess.h"

#include <QDialog>
class QProcess;

class ReportBugDialog : public QDialog {
    Q_OBJECT
    QThread generateBugReportProcess;
public:
    /// Default construtor
    ReportBugDialog(QDialog* parent = nullptr);

    /// Start thread to print the config
    void setBugReport();

    /// Destructor
    ~ReportBugDialog() override = default;

private:
    Ui::BugReportDialog* ui;

private slots:
    /// copy all the bug report to the OS clipboard
    void copyReportToClipBoard();

    /// write the config information into the GUI
    void showBugReport(const QString& bugReport);
};

#endif // REPORTBUGWIDGET_H
