/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "SaveAction.h"

#include <Application.h>
#include <Log.h>

using namespace camitk;


// --------------- constructor -------------------
SaveAction::SaveAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Save");
    setEmbedded(false);
    setDescription(tr("Save all the top-level of the selected components"));
    setComponent("Component");
    setIcon(QPixmap(":/fileSave"));

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Save"));

    // add a shortcut
    getQAction()->setShortcut(QKeySequence::Save);
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);
}

// --------------- destructor -------------------
SaveAction::~SaveAction() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* SaveAction::getWidget() {
    return nullptr;
}

// --------------- apply -------------------
Action::ApplyStatus SaveAction::apply() {
    QSet<Component*> allTopLevel;
    // fill the toplevel component set
    foreach (Component* comp, getTargets()) {
        allTopLevel.insert(comp->getTopLevelComponent());
    }

    // Save all selected
    QSet<Component*>::const_iterator it = allTopLevel.constBegin();

    while (it != allTopLevel.constEnd() && Application::save(*it)) {
        ++it;
    }

    if (it == allTopLevel.constEnd()) {
        return SUCCESS;
    }
    else {
        CAMITK_WARNING(tr("Canceled. Action Aborted."))
        return ABORTED;
    }
}
