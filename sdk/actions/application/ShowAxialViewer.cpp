/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ShowAxialViewer.h"

#include <MedicalImageViewer.h>
using namespace camitk;

// --------------- constructor -------------------
ShowAxialViewer::ShowAxialViewer(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Show Axial Viewer");
    setEmbedded(false);
    setDescription(tr("Show the Axial Viewer Only"));
    setComponent("");

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Axial Viewer"));
    addTag(tr("Slice"));

    // add a shortcut
    getQAction()->setShortcut(tr("Ctrl+2"));
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);

    // can be checked or not (on/off), useful when group in a QActionGroup (radio buttons)
    getQAction()->setCheckable(true);
}

// --------------- destructor -------------------
ShowAxialViewer::~ShowAxialViewer() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* ShowAxialViewer::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus ShowAxialViewer::apply() {
    MedicalImageViewer::getInstance()->setVisibleViewer(MedicalImageViewer::VIEWER_AXIAL);
    return SUCCESS;
}
