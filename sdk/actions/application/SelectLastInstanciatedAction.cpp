/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "SelectLastInstanciatedAction.h"

// CamiTK
#include <Application.h>
#include <Log.h>

using namespace camitk;

// --------------- constructor -------------------
SelectLastInstanciatedAction::SelectLastInstanciatedAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Select Last Instanciated Top Level Component");
    setEmbedded(false);
    setDescription(tr("Select the last top level component that was instantiated"));
    setComponent("");

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Select Component"));
}

// --------------- getWidget --------------
QWidget* SelectLastInstanciatedAction::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus SelectLastInstanciatedAction::apply() {
    const ComponentList& allComponent = Application::getAllComponents();

    // unselect all others top level components
    for (int i = 0; i < allComponent.size(); i++) {
        allComponent.at(i)->getTopLevelComponent()->setSelected(false);
    }

    // select the last instanciated component
    if (!allComponent.isEmpty()) {
        Component* lastComponent = allComponent.last();
        // select the top level component associated to this component
        while (lastComponent->getParentComponent() != NULL) {
            lastComponent = lastComponent->getParentComponent();
        }
        // select it
        lastComponent->setSelected(true);

        // refresh all viewers of the application to consider the newly selected top level component
        Application::refresh();
        return SUCCESS;
    }
    else {
        CAMITK_ERROR(tr("No top-level component instanciated. Nothing to select."))
        return ERROR;
    }
}

