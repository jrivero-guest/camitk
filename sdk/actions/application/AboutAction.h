/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ABOUTACTION_H
#define ABOUTACTION_H

#include <Action.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * The AboutAction class handles the pop-up "About CamiTK" with information about CamiTK.
 *
 **/
class AboutAction : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    AboutAction(camitk::ActionExtension* extension);

    /// Default Destructor
    virtual ~AboutAction();

    /// Returns NULL: no permanent widget for this action. The GUI is run shown a one-shot dialog in apply
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
      * it shows a little about dialog.
      */
    virtual ApplyStatus apply();
};
#endif // ABOUTACTION_H
