/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "QuitAction.h"

// CamiTK
#include <Application.h>
#include <MainWindow.h>
#include <Component.h>

using namespace camitk;

// --------------- constructor -------------------
QuitAction::QuitAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Quit");
    setEmbedded(false);
    setDescription(tr("Exit the application, prompting for additional information if needed"));
    setComponent("");
    setIcon(QPixmap(":/fileQuit"));

    // Setting classification family and tags
    setFamily("Application");
    addTag(tr("Quit Application"));
    addTag(tr("Exit Application"));

    // add a shortcut
    getQAction()->setShortcut(QKeySequence::Quit);
    getQAction()->setShortcutContext(Qt::ApplicationShortcut);
}

// --------------- getWidget --------------
QWidget* QuitAction::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus QuitAction::apply() {
    Application::getMainWindow()->close();
    return SUCCESS;
}

