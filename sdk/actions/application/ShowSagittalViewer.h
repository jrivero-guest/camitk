/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SHOWSAGITTALVIEWER_H
#define SHOWSAGITTALVIEWER_H

#include <Action.h>

/**
 * @ingroup group_sdk_actions_application
 *
 * @brief
 * Within the MedicalImageViewer, display the sagittal viewer.
 *
 */
class ShowSagittalViewer : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    ShowSagittalViewer(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~ShowSagittalViewer();

    /// Returns NULL: no widget for this action.
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
      * it calls setVisibility on the MedicalImageViewer instance
      */
    virtual ApplyStatus apply();
};
#endif // SHOWSAGITTALVIEWER_H
