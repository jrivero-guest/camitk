<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ImageReconstructionAction</name>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="67"/>
        <source>Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="67"/>
        <source>Grey level value corresponding to the isovalue the isosurface represents.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="73"/>
        <source>Keep only largest component?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="73"/>
        <source>Do we keep only the largest component?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="77"/>
        <source>Subsample original image?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="77"/>
        <source>Do we subsample input image for faster computation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="80"/>
        <source>Subsampled image width (X)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="80"/>
        <source>The width (X dimension) of the subsampled image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="85"/>
        <source>Subsampled image height (Y)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="85"/>
        <source>The height (Y dimension) of the subsampled image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="90"/>
        <source>Subsampled image depth (Z)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ImageReconstructionAction.cpp" line="90"/>
        <source>The depth (Z dimension) of the subsampled image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
