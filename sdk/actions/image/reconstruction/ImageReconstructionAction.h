/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef ImageReconstructionAction_H
#define ImageReconstructionAction_H

// -- CamiTK includes
#include <Action.h>
#include <ImageComponent.h>

/**
 * @ingroup group_sdk_actions_image_reconstruction
 *
 * @brief
 * The class ImageReconstructionAction interfaces the reconstruction
 * of 3D volumes from a set of images (ImageComponent), typically using the marching cubes algorithm.
 *
 */
class ImageReconstructionAction : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    ImageReconstructionAction(camitk::ActionExtension*);

    /// the destructor
    virtual ~ImageReconstructionAction();

    /// this method creates and returns the widget containing the user interface for the action
    //virtual QWidget * getWidget();

public slots:
    /// method called when the action is applied (nothing to do here, everything will be done by the widget)
    virtual ApplyStatus apply();

private:
    /// Returns an isosurface reconstructed with the Marching Cubes algorithm, from the original set of slices.
    vtkSmartPointer<vtkPointSet> getMarchingCubesReconstruction();

    /// The initial images, used for the reconstruction.
    camitk::ImageComponent* myComponent;



};

//**************************************************************************
#endif
