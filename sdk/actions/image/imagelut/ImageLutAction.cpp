/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ImageComponent.h"
#include "Application.h"

#include "ImageLutAction.h"
#include "ImageLutWidget.h"


using namespace camitk;



// --------------- constructor -------------------
ImageLutAction::ImageLutAction(ActionExtension* extension) : Action(extension) {
    setName("Lut");
    setDescription("Modify the LUT of an image components");
    setComponent("ImageComponent");
    setFamily("ImageLut");
    addTag("Lut");
}

// --------------- getWidget -------------------
QWidget* ImageLutAction::getWidget() {
    //-- create the widget if needed
    if (!actionWidget) {
        actionWidget = new ImageLutWidget();
    }

    //--  update the LUT values (the getTargets() might have changed since the last call)
    dynamic_cast<ImageLutWidget*>(actionWidget)->updateComponent(dynamic_cast<ImageComponent*>(getTargets().last()));

    return actionWidget;
}

// --------------- getWidget -------------------
Action::ApplyStatus ImageLutAction::apply() {
    return Action::SUCCESS;
}

