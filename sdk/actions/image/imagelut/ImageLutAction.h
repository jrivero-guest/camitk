/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef IMAGELUTACTION_H
#define IMAGELUTACTION_H

#include <QObject>
#include <Action.h>

/**
 * @ingroup group_sdk_actions_image_lut
 *
 * @brief
 * The lut action itself. It only display its widget.
 *
 */
class ImageLutAction : public camitk::Action {

public:
    /// the constructor
    ImageLutAction(camitk::ActionExtension*);

    /// the destructor
    virtual ~ImageLutAction() = default;

    /// this method creates and returns the widget containing the user interface for the action
    virtual QWidget* getWidget();

public slots:
    /// method called when the action is applied (nothing to do here, everything will be done by the widget)
    virtual ApplyStatus apply();
};

#endif // IMAGELUTACTION_H
