/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SHOWARBITRARYSLICEIN3D_H
#define SHOWARBITRARYSLICEIN3D_H

#include <Action.h>
#include <Component.h>

/**
 * @ingroup group_sdk_actions_image_showin3d
 *
 * @brief
 * Shows the arbitrary slice of the @ref camitk::ImageComponent in the 3D viewer.
 *
 * @note
 * The arbitrary slice is not visible by default.
 * This action allows you to display it within the 3D viewer.
 *
 */
class ShowArbitrarySliceIn3D : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    ShowArbitrarySliceIn3D(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~ShowArbitrarySliceIn3D();

    /// Returns NULL: no permanent widget for this action. The GUI is run shown a one-shot dialog in apply
    virtual QWidget* getWidget();

public slots:
    /** this method is automatically called when the action is triggered.
      * It gets all the targets' top level component and close them.
      */
    virtual ApplyStatus apply();
};
#endif // SHOWARBITRARYSLICEIN3D_H
