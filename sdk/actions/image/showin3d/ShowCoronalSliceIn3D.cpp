/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ShowCoronalSliceIn3D.h"

#include <ImageComponent.h>
#include <SingleImageComponent.h>
#include <InteractiveViewer.h>

using namespace camitk;

// --------------- constructor -------------------
ShowCoronalSliceIn3D::ShowCoronalSliceIn3D(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Show Coronal Slice In 3D");
    setEmbedded(false);
    setDescription(tr("Toggle display of the coronal slice(s) in 3D"));
    setComponent("ImageComponent");

    // Setting classification family and tags
    setFamily("Display");
    addTag(tr("3D Viewer"));

}

// --------------- destructor -------------------
ShowCoronalSliceIn3D::~ShowCoronalSliceIn3D() {
    // do not delete the widget has it might have been used in the ActionViewer (i.e. the ownership might have been taken by the stacked widget)
}

// --------------- getWidget --------------
QWidget* ShowCoronalSliceIn3D::getWidget() {
    return NULL;
}

// --------------- apply -------------------
Action::ApplyStatus ShowCoronalSliceIn3D::apply() {
    foreach (Component* comp, getTargets()) {
        ImageComponent* img = dynamic_cast<ImageComponent*>(comp);
        if (img) {
            img->getCoronalSlices()->setViewSliceIn3D(! img->getCoronalSlices()->getViewSliceIn3D());
            Application::refresh();
        }
    }

    return SUCCESS;
}
