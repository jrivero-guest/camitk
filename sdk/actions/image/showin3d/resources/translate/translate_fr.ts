<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ShowArbitrarySliceIn3D</name>
    <message>
        <source>Toggle display of the arbitrary slice(s) in 3D</source>
        <translation>Inverser l&apos;affichage de(s) coupe(s) arbritraire(s) en 3D</translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation>Afficheur 3D</translation>
    </message>
</context>
<context>
    <name>ShowAxialSliceIn3D</name>
    <message>
        <source>Toggle display of the axial slice in 3D</source>
        <translation>Inverser l&apos;affichage de(s) coupe(s) axiale(s) en 3D</translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation>Afficheur 3D</translation>
    </message>
</context>
<context>
    <name>ShowCoronalSliceIn3D</name>
    <message>
        <source>Toggle display of the coronal slice(s) in 3D</source>
        <translation>Inverser l&apos;affichage de(s) coupe(s) coronale(s) en 3D</translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation>Affichage 3D</translation>
    </message>
</context>
<context>
    <name>ShowImageIn3D</name>
    <message>
        <source>Shows the Image Volume in 3D viewer</source>
        <translation>Montre le Volume de l&apos;image dans l&apos;afficfheur 3D</translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation>Afficheur 3D</translation>
    </message>
</context>
<context>
    <name>ShowSagittalSliceIn3D</name>
    <message>
        <source>Toggle display of the sagittal slice(s) in 3D</source>
        <translation>Inverser l&apos;affichage de(s) coupe(s) sagittale(s) en 3D</translation>
    </message>
    <message>
        <source>3D Viewer</source>
        <translation>Afficheur 3D</translation>
    </message>
</context>
</TS>
