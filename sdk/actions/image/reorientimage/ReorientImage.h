/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef REORIENTIMAGE_H
#define REORIENTIMAGE_H

// Include Action.h to be able to inherit from Action
#include <Action.h>
// Include ui_ReorientImage.h to be able to declare an instance of Ui::ReorientImage
#include "ui_ReorientImage.h"

class vtkTransform;
class vtkAnnotatedCubeActor;
namespace camitk {
class Geometry;
class ImageComponent;
class InteractiveViewer;
}

class ReorientImage : public camitk::Action {
    Q_OBJECT

public:
    /// the constructor
    ReorientImage(camitk::ActionExtension*);

    /// Destructor
    virtual ~ReorientImage();

    /// this method creates and returns the widget containing the user interface for the action
    virtual QWidget* getWidget();

    void showApplyButton(bool show);

public slots:
    /// method called when the action is applied
    virtual ApplyStatus apply();

    virtual void rcsChanged(const QString index);

    virtual void modelChanged(bool displayMaleModel);

private:
    /// initialize the dialog
    void initDialog();

    /// Build Geometries
    void buildGeometries();

    void buildAxes();
    void buildCube();
    void buildTransforms();

    void initInternalViewer();

    void setAxesOrientation(const QString orientation);

    camitk::InteractiveViewer* internalViewer;

    /// Actually perform the image transformation
    ApplyStatus process(camitk::ImageComponent* image);

    /// the Qt Gui
    Ui::ReorientImage ui;

    /// the dialog
    QDialog* dialog;

    /// axes actor
    vtkSmartPointer<vtkAxesActor> axes;

    /// annotated cube actor
    vtkSmartPointer<vtkAnnotatedCubeActor> annotatedCube;

    vtkSmartPointer<vtkTransform> orientationTransform;
    // The cube should not rotate, just translate...
    vtkSmartPointer<vtkTransform> orientationCubeTransform;

    vtkSmartPointer<vtkTransform> cubeTransform;
    vtkSmartPointer<vtkTransform> axesTransform;

    camitk::Geometry* modelBoundingBox;
    camitk::Geometry* maleModel;
    camitk::Geometry* femaleModel;


    QMap<QChar, QString> lettersMeaning;

//    camitk::ImageComponent * img;

};

#endif // REORIENTIMAGE_H
