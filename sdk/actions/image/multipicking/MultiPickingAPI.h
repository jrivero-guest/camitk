/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// ----- MultiPickingAPI.h
#ifndef MULTIPICKING_API_H
#define MULTIPICKING_API_H

// -----------------------------------------------------------------------
//
//                        MULTIPICKING_API
//
// -----------------------------------------------------------------------
// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the COMPILE_CAMITK_API
// flag defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// CAMITK_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#if defined(_WIN32) // MSVC and mingw
#ifdef COMPILE_MULTIPICKING_ACTION_API
#define MULTIPICKING_API __declspec(dllexport)
#else
#define MULTIPICKING_API __declspec(dllimport)
#endif
#else
// for all other platforms CAMITK_API is defined to be "nothing"
#ifndef MULTIPICKING_API
#define MULTIPICKING_API
#endif
#endif // MSVC and mingw

// -----------------------------------------------------------------------
// It seems that MSVC does not understand exception specification
// If I understand it well, when _declspec() is used, there is a default
// nothrow attribute.
// I did not find the throw attribute. It seems that msvc is therefore ignoring the
// specification of the type of the exception.
// The compiler therefore issues a warning.
// The following line is to avoid this particular warning.
// The best would be to ask msvc not only to take the exception into account, but also
// its type. Anyway, I did not find how to do that anywhere, and I am not sure this is
// possible...
#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4290 )
#endif // MSVC only

#endif // MULTIPICKING_API_H
