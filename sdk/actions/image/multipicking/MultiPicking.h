/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MULTIPICKINGACTION_H
#define MULTIPICKINGACTION_H

#include <QObject>
#include <Action.h>
#include "PickedPixelMap.h"

class MultiPickingWidget;

/**
 * @ingroup group_sdk_actions_image_multipicking
 *
 * @brief
 * This action allows the user to store a list of picked pixels.
 *
 */
class MultiPicking : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    MultiPicking(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~MultiPicking() = default;

    /// Returns NULL as this action has no parameters
    virtual QWidget* getWidget();

public slots:
    /// method called when the action is applied (nothing to do here, everything will be done by the widget)
    virtual ApplyStatus apply() {
        return SUCCESS;
    };

private:

    /// the widget component
    MultiPickingWidget* actionWidget;

};
#endif // MULTIPICKINGACTION_H
