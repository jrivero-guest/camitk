<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MultiPicking</name>
    <message>
        <location filename="../../MultiPicking.cpp" line="39"/>
        <source>Action to manage several pixel points in an image (coordinates and index).</source>
        <translation>Action pour gérer plusieurs points de type pixel dans une image (coordonnées et index).</translation>
    </message>
    <message>
        <location filename="../../MultiPicking.cpp" line="42"/>
        <source>Picking</source>
        <translation>Récolte</translation>
    </message>
    <message>
        <location filename="../../MultiPicking.cpp" line="43"/>
        <source>Select Pixels</source>
        <translation>Sélectionner des Pixels</translation>
    </message>
</context>
<context>
    <name>MultiPickingWidget</name>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Cordonnées Monde Actuelles</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="25"/>
        <source>Real World Coordinates</source>
        <translation>Index Voxell</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="39"/>
        <source>Voxell index</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="103"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="118"/>
        <source>Import CSV File</source>
        <translation>Importer Fichier CSV</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="142"/>
        <source>Save as CSV File</source>
        <translation>Sauver comme Fichier CSV</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="199"/>
        <source>Open a pixel list file</source>
        <translation>Ouvrir un fichier liste pixel</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="199"/>
        <location filename="../../MultiPickingWidget.cpp" line="215"/>
        <source>CSV Files (*.csv)</source>
        <translation>Fichier CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="214"/>
        <source>Save the picked points</source>
        <translation>Sauver les points sélectionnés</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="336"/>
        <source>The point (</source>
        <translation>Le point(</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="337"/>
        <source>), does not belong to the image </source>
        <translation>), n&apos;appartienne pas à l&apos;image</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="337"/>
        <source>. Sorry...</source>
        <translation>. Désolé...</translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="338"/>
        <source>Impossible Operation</source>
        <translation>Opération impossible</translation>
    </message>
</context>
</TS>
