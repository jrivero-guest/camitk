<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>MultiPicking</name>
    <message>
        <location filename="../../MultiPicking.cpp" line="39"/>
        <source>Action to manage several pixel points in an image (coordinates and index).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPicking.cpp" line="42"/>
        <source>Picking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPicking.cpp" line="43"/>
        <source>Select Pixels</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultiPickingWidget</name>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="25"/>
        <source>Real World Coordinates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="39"/>
        <source>Voxell index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="103"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="118"/>
        <source>Import CSV File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.ui" line="142"/>
        <source>Save as CSV File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="199"/>
        <source>Open a pixel list file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="199"/>
        <location filename="../../MultiPickingWidget.cpp" line="215"/>
        <source>CSV Files (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="214"/>
        <source>Save the picked points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="336"/>
        <source>The point (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="337"/>
        <source>), does not belong to the image </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="337"/>
        <source>. Sorry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../MultiPickingWidget.cpp" line="338"/>
        <source>Impossible Operation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
