/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef PICKEDPIXELMAP_H
#define PICKEDPIXELMAP_H

#include <QWidget>
#include <QVector3D>
#include <QCoreApplication>

#include "MultiPickingAPI.h"
#include "ImageComponent.h"


/**
 * @ingroup group_sdk_actions_image_multipicking
 *
 * @brief
 * This class is dedicated to manage an ImageComponent with a list of selected/picked pixels.
 * This class allows one to manage a generic list of pixels too (add, remove, modify and save some points).
 *
 * Since Frame Usage in CamiTK, PickedPixelMap stores 4 lists:
 * - pixelIndexList which stores the picked pixel position as image indices, i.e. integers number of voxels since the image origin (line first, then columns, then depth)
 * - localCoordIndexList which stores the pixel position as image real coordinates with the zero of the image at the origin of the image, i.e. this list takes voxel size into account
 * - realCoordIndexList which stores the pixel positions in the real world frame, that may not be the image frame.
 * - pixelValue: for each index/coord/real coord, stores the gray level value of the image.
 *
 * No convertion between the lists is done in this class. These conversions are done at pixel clickin by the ImageComponent.
 *
 */
class MULTIPICKING_API PickedPixelMap {

    Q_DECLARE_TR_FUNCTIONS(PickedPixelMap)

public:
    /// constructor
    PickedPixelMap(camitk::ImageComponent*);

    /// destructor
    virtual ~PickedPixelMap();

    /** Allows one to remove one pixel in the list
     * @param id the index in the list of the pixel to remove
     */
    void removePixel(int id);

    /** Allows one to modify one pixel in the list
     * @param row the index in the list of the pixel to modify using the last picked pixel
     */
    void modifyPixel(int row);

    /// Allows one to add a pixel in the list using the last picked pixel
    void addPixel();

    /** save the list of pixel in a given file base name (CSV format, separator = comma).
     *  ".csv" is automatically added if needed.
     * the format is : index (int) , i (int) , j (int) , k (int) , x (double) , y (double) , z (double)
     * @param fileName the location where the file will be saved
     */
    void savePixelList(QString fileName);

    /** open a list of pixel in a given file base name (CSV format, separator = comma).
     *  the format is : index (int) , i (int) , j (int) , k (int) , x (double) , y (double) , z (double)
     **/
    void openPixelList(QString fileName);

    /// return the list of pixel index
    QList<QVector3D>* getPixelIndexList();

    /// return the list of coordinates pixel index
    QList<QVector3D>* getCoordIndexList();

    /// return the list of coordinates in real world frame
    QList<QVector3D>* getRealWorldList();

    /// return the liste of pixel values
    QList<double>* getPixelValueList();

    /// the managed ImageComponent
    camitk::ImageComponent* getImage();

    /// Empties the pixel list.
    void resetPixelList();

private:

    /// this is a list of pixels (as i,j,k indexes in the 3 directions)
    QList<QVector3D>* pixelIndexList;

    /// list of corresponding image coordinates, i.e. taking voxel size into account, but in the local frame of the image
    QList<QVector3D>* coordIndexList;

    /// list of corresponding coordinates in real world frame.
    QList<QVector3D>* realCoordList;

    /// list of corresponding pixel values
    QList<double>* pixelValueList;

    /// ImageComponent where the pixels are clicked
    camitk::ImageComponent* image;

};

#endif // PICKEDPIXELMAP_H
