/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef TRANSPARENCYPOINTWIDGET_H
#define TRANSPARENCYPOINTWIDGET_H

// Qt
#include <QMainWindow>
#include <QString>
// Include GUI  automatically generated from the file
// ColorPointWidget.ui (to be modified via QtDesigner).
#include "ui_TransparencyPointWidget.h"


/**
 * @ingroup group_sdk_actions_image_volumerendering
 *
 * @brief
 * This widget handle specifically the transparency in the output image.
 *
 * @note
 * The transparency is stored on the alpha channel of the voxel.
 * This widget is stored within the @ref camitk::VolumeRenderingWidget
 *
 * \image html actions/volume_rendering_2.png "The transparency widget appears in the top." width=10cm
 *
 */
class TransparencyPointWidget : public QWidget  {
    Q_OBJECT


public:
    TransparencyPointWidget(QWidget* dad);
    TransparencyPointWidget(QWidget* dad, int grayLevel, double transparency);

    int getGrayLevel();
    double getTransparency();


public slots:
    virtual void remove();
    virtual void grayLevelChanged(int);
    virtual void transparencyChanged(double);

private:
    Ui_TransparencyPointWidget ui;
    QWidget* dad;

};
#endif // TRANSPARENCYPOINTWIDGET_H
