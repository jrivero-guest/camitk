<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ColorPointWidget</name>
    <message>
        <location filename="../../ColorPointWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.ui" line="20"/>
        <source>Gray Level</source>
        <translation>Niveau de Gris</translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.ui" line="63"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.cpp" line="38"/>
        <location filename="../../ColorPointWidget.cpp" line="52"/>
        <location filename="../../ColorPointWidget.cpp" line="68"/>
        <source>background-color: </source>
        <translation>Couleur d&apos;arrière plan:</translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.cpp" line="64"/>
        <source>Please choose a color</source>
        <translation>Veuillez choisir une couleur</translation>
    </message>
</context>
<context>
    <name>GradientOpacityWidget</name>
    <message>
        <location filename="../../GradientOpacityWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../GradientOpacityWidget.ui" line="20"/>
        <source>Gray Level</source>
        <translation>Niveau de Gris</translation>
    </message>
    <message>
        <location filename="../../GradientOpacityWidget.ui" line="63"/>
        <source>Opacity</source>
        <translation>Opacité</translation>
    </message>
</context>
<context>
    <name>TransparencyPointWidget</name>
    <message>
        <location filename="../../TransparencyPointWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../TransparencyPointWidget.ui" line="20"/>
        <source>Gray Level</source>
        <translation>Niveau de Gris</translation>
    </message>
    <message>
        <location filename="../../TransparencyPointWidget.ui" line="57"/>
        <source>Transparency</source>
        <translation>Transparence</translation>
    </message>
</context>
<context>
    <name>VolumeRenderingAction</name>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="71"/>
        <source>Volume rendering of 3D medical image using ray casting&lt;br/&gt;				   (Use Ctrl+R to render a selected image volume)</source>
        <translation>Rendu du volume d&apos;une image médicale 3D utilisant raycasting &lt;br/&gt;				   (Utiliser Ctrl+R pour avoir un rendu d&apos;une volume d&apos;une image sélectionnée)</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="77"/>
        <source>Volume rendering</source>
        <translation>Rendu du volume</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="78"/>
        <source>volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="79"/>
        <source>render</source>
        <translation>rendu</translation>
    </message>
</context>
<context>
    <name>VolumeRenderingWidget</name>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="23"/>
        <source>Volume Rendering</source>
        <translation>Rendu du Volume</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="36"/>
        <source>Image</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="44"/>
        <source>Input Image</source>
        <translation>Image en entrée</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="51"/>
        <source>no input</source>
        <translation>pas d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="117"/>
        <source>Transparency</source>
        <translation>Transparence</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="170"/>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="223"/>
        <source>Gradient Opacity</source>
        <translation>Gradient d&apos;Opacité</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="276"/>
        <source>Shading Properties</source>
        <translation>Propriété des ombrages</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="284"/>
        <source>Ambiant Light</source>
        <translation>Lumière d&apos;ambiance</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="327"/>
        <source>Diffuse Light</source>
        <translation>Lumière diffuse</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="364"/>
        <source>Specular Light</source>
        <translation>Lumière en mirroir </translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="425"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="276"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="460"/>
        <source>File not found: the file </source>
        <translation>Fichier introuvable: le fichier</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="276"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="460"/>
        <source> was not found</source>
        <translation>est introuvable</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="278"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="462"/>
        <source>Error: cannot fine file </source>
        <translation>Erreur: fichier introuvable</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="278"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="462"/>
        <source> sorry...
</source>
        <translation>désolé...</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="282"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="291"/>
        <source>File </source>
        <translation>le Fichier</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="282"/>
        <source> have no valid root (not a valid XML file format).
</source>
        <translation>n&apos;as pas une racine valide (format du fichier XML non valide).</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="291"/>
        <source> is not in the right xml file format.
</source>
        <translation>n&apos;est pas dans le format de fichier correct.</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="373"/>
        <source>Open Volume Rendering Colormap</source>
        <translation>Ouverture de la Table de Couleur du rendu volumique</translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="378"/>
        <source>Save Volume Rendering Colormap</source>
        <translation>Sauvegarde de la Table de Couleur du rendu volumique</translation>
    </message>
</context>
</TS>
