<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>ColorPointWidget</name>
    <message>
        <location filename="../../ColorPointWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.ui" line="20"/>
        <source>Gray Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.ui" line="63"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.cpp" line="38"/>
        <location filename="../../ColorPointWidget.cpp" line="52"/>
        <location filename="../../ColorPointWidget.cpp" line="68"/>
        <source>background-color: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ColorPointWidget.cpp" line="64"/>
        <source>Please choose a color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GradientOpacityWidget</name>
    <message>
        <location filename="../../GradientOpacityWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../GradientOpacityWidget.ui" line="20"/>
        <source>Gray Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../GradientOpacityWidget.ui" line="63"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransparencyPointWidget</name>
    <message>
        <location filename="../../TransparencyPointWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../TransparencyPointWidget.ui" line="20"/>
        <source>Gray Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../TransparencyPointWidget.ui" line="57"/>
        <source>Transparency</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeRenderingAction</name>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="71"/>
        <source>Volume rendering of 3D medical image using ray casting&lt;br/&gt;				   (Use Ctrl+R to render a selected image volume)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="77"/>
        <source>Volume rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="78"/>
        <source>volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingAction.cpp" line="79"/>
        <source>render</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VolumeRenderingWidget</name>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="23"/>
        <source>Volume Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="36"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="44"/>
        <source>Input Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="51"/>
        <source>no input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="117"/>
        <source>Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="170"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="223"/>
        <source>Gradient Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="276"/>
        <source>Shading Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="284"/>
        <source>Ambiant Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="327"/>
        <source>Diffuse Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="364"/>
        <source>Specular Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.ui" line="425"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="276"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="460"/>
        <source>File not found: the file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="276"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="460"/>
        <source> was not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="278"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="462"/>
        <source>Error: cannot fine file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="278"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="462"/>
        <source> sorry...
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="282"/>
        <location filename="../../VolumeRenderingWidget.cpp" line="291"/>
        <source>File </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="282"/>
        <source> have no valid root (not a valid XML file format).
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="291"/>
        <source> is not in the right xml file format.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="373"/>
        <source>Open Volume Rendering Colormap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../VolumeRenderingWidget.cpp" line="378"/>
        <source>Save Volume Rendering Colormap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
