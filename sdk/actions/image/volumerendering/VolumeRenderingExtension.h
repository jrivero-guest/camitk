/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VOLUMERENDERINGEXTENSION_H
#define VOLUMERENDERINGEXTENSION_H

#include <ActionExtension.h>

/**
 * @ingroup group_sdk_actions_image_volumerendering
 *
 * @brief
 * The volume rendering action extension.
 *
 */
class VolumeRenderingExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.sdk.action.volumerendering")

public:
    /// the constructor
    VolumeRenderingExtension() : ActionExtension() {}

    /// the destructor
    virtual ~VolumeRenderingExtension() {}

    /// Method that return the action extension name
    virtual QString getName() {
        return "VolumeRenderingExtension";
    };

    /// Method that return the action extension descrption
    virtual QString getDescription() {
        return "This extension allows user to compute volume rendering on .mha images.";
    };
    /// initialize all the actions
    virtual void init();

};

#endif // VOLUMERENDERINGEXTENSION



