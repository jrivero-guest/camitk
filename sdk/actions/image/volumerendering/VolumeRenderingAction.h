/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef VOLUMERENDERINGACTION_H
#define VOLUMERENDERINGACTION_H

#include <QObject>
#include <Action.h>

#include <ImageComponent.h>

#include "VolumeRenderingWidget.h"

/**
 * @ingroup group_sdk_actions_image_volumerendering
 *
 * @brief
 * This action perform a volume rendering.
 *
 * Take as an input a gray-level @ref camitk::ImageComponent and renders a 3D color image.
 *
 */
class VolumeRenderingAction : public camitk::Action {
    Q_OBJECT

public:
    /// Default Constructor
    VolumeRenderingAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~VolumeRenderingAction() = default;


    void createVolumeRendering(camitk::ImageComponent* comp);


public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();

    // Returns NULL: no permanent widget for this action. The GUI is run shown a one-shot dialog in apply
    // A special widget will be displaied to edit colors.
    virtual QWidget* getWidget();


private:
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent* comp);


    /// no default widget but a VolumeRenderingWidget displaied when necessary
    VolumeRenderingWidget* myWidget;

    /// Name given to the Volume rendering actor attached to the Volume Rendering subComponant
    /// of a volume image
    QString volumeName;


};
#endif // VOLUMERENDERINGACTION_H
