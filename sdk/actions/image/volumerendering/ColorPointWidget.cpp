/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// - Qt stuff
#include <QColor>
#include <QColorDialog>

// - Local stuff
#include "ColorPointWidget.h"
#include "VolumeRenderingWidget.h"

ColorPointWidget::ColorPointWidget(QWidget* dad) : QWidget(dad) {
    ui.setupUi(this);
    this->dad = dad;
    this->currentColor = QColor(255, 255, 255, 255);
    QString s = tr("background-color: ");
    ui.colorPushButton->setStyleSheet(s + currentColor.name());

}

ColorPointWidget::ColorPointWidget(QWidget* dad, int grayLevel, QColor color) : QWidget(dad) {
    ui.setupUi(this);
    this->dad = dad;
    this->currentColor = QColor(255, 255, 255, 255);

    ui.GrayLevelSpinBox->setValue(grayLevel);

    if (color.isValid()) {
        currentColor = color;
    }
    QString s = tr("background-color: ");
    ui.colorPushButton->setStyleSheet(s + currentColor.name());

}

void ColorPointWidget::remove() {
    auto* dadVR = dynamic_cast<VolumeRenderingWidget*>(this->dad);
    if (dadVR) {
        dadVR->removeColorPoint(this);
    }
}

void ColorPointWidget::colorButtonClicked() {
    QColor color = QColorDialog::getColor(currentColor, this, tr("Please choose a color"), QColorDialog::ShowAlphaChannel);

    if (color.isValid()) {
        currentColor = color;
        QString s = tr("background-color: ");
        ui.colorPushButton->setStyleSheet(s + currentColor.name());
    }

}

int ColorPointWidget::getGrayLevel() {
    return ui.GrayLevelSpinBox->value();
}

QColor ColorPointWidget::getColor() {
    return currentColor;
}

void ColorPointWidget::grayLevelChanged(int gray) {
    bool oldState = ui.grayLevelSlider->blockSignals(true);
    ui.grayLevelSlider->setValue(gray);
    ui.grayLevelSlider->blockSignals(oldState);
}