/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef SETANGLESACTION_H
#define SETANGLESACTION_H

// CamiTK stuff
#include <Action.h>
#include <ImageComponent.h>

#include "AnglesSetterWidget.h"

/**
 * @ingroup group_sdk_actions_image_arbitraryslice
 *
 * @brief
 * This action simply display the widget allowing the user to select an angle to orientate the arbitrary slice.
 */
class SetAnglesAction : public camitk::Action {

    Q_OBJECT

public:

    /// Default Constructor
    SetAnglesAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~SetAnglesAction();

    /// Return the arbitrary slice angles setter widget
    virtual QWidget* getWidget();

public slots:
    /** This method returns always SUCCESS as the action aims at displaying its widget to be used in order to control the
     *  arbitrary slice display.
     */
    virtual ApplyStatus apply() {
        return SUCCESS;
    }

};

#endif // SETANGLESACTION_H

