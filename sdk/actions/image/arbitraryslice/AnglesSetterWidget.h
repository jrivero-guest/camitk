/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef AnglesSetterWidget_h
#define AnglesSetterWidget_h

#include "ui_AnglesSetterWidget.h"

#include <QWidget>

/**
 * @ingroup group_sdk_actions_image_arbitraryslice
 *
 * @brief
 * This class describes the QWidget that allows user to change the X,Y ans Z angle of the arbitrary slice
 * of a 2D image. Moreover it also allows one to change the slice number using a slider.
 * \note
 *  For initialization purpose, widget must be used for only one component (arbitrary).
 *  If several component are selected, do not build / display it.
 *
 *  Use corresponding .ui file created with Qt Designer.
 */
class AnglesSetterWidget : public QWidget {
    Q_OBJECT

public:
    /// Default construtor
    AnglesSetterWidget(QWidget* parent = 0);

    /// Destructor
    virtual ~AnglesSetterWidget();

private:
    Ui::AnglesSetterWidget ui;

private:
    /// update the angle dialog slider (text + value)
    void updateAngleSlider(QDial* dial, QLabel* label);

private slots:

    /// Update slice number to be displayed
    void updateSlice(int sliceNumber);

    /// Method that update the angle dialog slider (text + value)
    void xAngleDialValueChanged(int value);
    void yAngleDialValueChanged(int value);
    void zAngleDialValueChanged(int value);

};

#endif // AnglesSetterWidget_h
