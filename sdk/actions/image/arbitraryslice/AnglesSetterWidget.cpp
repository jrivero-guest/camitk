/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "AnglesSetterWidget.h"

// CamiTK stuff
#include <InteractiveViewer.h>
#include <Application.h>
#include <MainWindow.h>
#include <ImageComponent.h>

// Qt stuff
#include <QSpinBox>

using namespace camitk;

// -------------------- constructor --------------------
AnglesSetterWidget::AnglesSetterWidget(QWidget* parent): QWidget(parent) {
    ui.setupUi(this);

    // Ui slice number change slot connection
    connect(ui.sliceSpinBox, SIGNAL(valueChanged(int)), this, SLOT(updateSlice(int)));
    connect(ui.sliceSlider, SIGNAL(valueChanged(int)), this, SLOT(updateSlice(int))) ;

    // Ui angles slot connection
    connect(ui.xAngleDial, SIGNAL(valueChanged(int)), this, SLOT(xAngleDialValueChanged(int)));
    connect(ui.yAngleDial, SIGNAL(valueChanged(int)), this, SLOT(yAngleDialValueChanged(int)));
    connect(ui.zAngleDial, SIGNAL(valueChanged(int)), this, SLOT(zAngleDialValueChanged(int)));

    // Initialize the slider
    // The action calling this widget must be applyied on only one IMAGE component.
    // Therefore, selectedComponents are counted to 5, the top image level component and its slices (axial, coronal, sagital and arbitrary).
    // selectedComponent[1] => arbitrary slice of the top level component
    ComponentList selectedComponents = Application::getSelectedComponents();
    if (selectedComponents.size() > 0) {
        ui.sliceSlider->setMaximum(selectedComponents[1]->getNumberOfSlices() - 1);
        ui.sliceSlider->setValue(selectedComponents[1]->getSlice());
        ui.sliceSpinBox->setMaximum(selectedComponents[1]->getNumberOfSlices() - 1);
        ui.sliceSpinBox->setValue(selectedComponents[1]->getSlice());
    }
    else {
        // TODO display error message, log ...
    }
}

// -------------------- destructor --------------------
AnglesSetterWidget::~AnglesSetterWidget() {
    ;
}

// -------------------- updateSlice --------------------
void AnglesSetterWidget::updateSlice(int sliceNumber) {
    // Ui slider and spin box are mapped and thus automatically updated when a change occurs.
    InteractiveViewer::getArbitraryViewer()->sliderChanged(sliceNumber);
    InteractiveViewer::getArbitraryViewer()->refresh();
    InteractiveViewer::get3DViewer()->refresh();
}

// -------------------- xAngleDialValueChanged --------------------
void AnglesSetterWidget::xAngleDialValueChanged(int value) {
    InteractiveViewer::getArbitraryViewer()->xAngleChanged(value);
    updateAngleSlider(ui.xAngleDial, ui.xAngleValue);
    InteractiveViewer::getArbitraryViewer()->refresh();
    InteractiveViewer::get3DViewer()->refresh();
}

// -------------------- yAngleDialValueChanged --------------------
void AnglesSetterWidget::yAngleDialValueChanged(int value) {
    InteractiveViewer::getArbitraryViewer()->yAngleChanged(value);
    updateAngleSlider(ui.yAngleDial, ui.yAngleValue);
    InteractiveViewer::getArbitraryViewer()->refresh();
    InteractiveViewer::get3DViewer()->refresh();
}

// -------------------- zAngleDialValueChanged --------------------
void AnglesSetterWidget::zAngleDialValueChanged(int value) {
    InteractiveViewer::getArbitraryViewer()->zAngleChanged(value);
    updateAngleSlider(ui.zAngleDial, ui.zAngleValue);
    InteractiveViewer::getArbitraryViewer()->refresh();
    InteractiveViewer::get3DViewer()->refresh();
}

// -------------------- updateAngleSlider --------------------
void AnglesSetterWidget::updateAngleSlider(QDial* dial, QLabel* label) {
    dial->blockSignals(true);
    dial->setMinimum(0);
    dial->setMaximum(360);
    dial->blockSignals(false);

    if (label == ui.xAngleValue) {
        label->setText("Angle X : <tt>" + QString("%1").arg(dial->value(), 3) + "</tt>" + QChar(0x00B0));
    }
    else if (label == ui.yAngleValue) {
        label->setText("Angle Y : <tt>" + QString("%1").arg(dial->value(), 3) + "</tt>" + QChar(0x00B0));
    }
    else if (label == ui.zAngleValue) {
        label->setText("Angle Z : <tt>" + QString("%1").arg(dial->value(), 3) + "</tt>" + QChar(0x00B0));
    }

    label->update();
}

