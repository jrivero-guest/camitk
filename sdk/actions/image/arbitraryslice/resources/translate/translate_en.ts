<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>AnglesSetterWidget</name>
    <message>
        <location filename="../../AnglesSetterWidget.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AnglesSetterWidget.ui" line="53"/>
        <source>Slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../../AnglesSetterWidget.ui" line="98"/>
        <source>Angle X : 0°</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../../AnglesSetterWidget.ui" line="129"/>
        <source>Angle Y : 0°</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../../AnglesSetterWidget.ui" line="160"/>
        <source>Angle Z : 0°</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetAnglesAction</name>
    <message>
        <location filename="../../SetAnglesAction.cpp" line="41"/>
        <source>This action allows user to adjust the angle of the slice of the arbitrary viewer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SetAnglesAction.cpp" line="46"/>
        <source>arbitrary slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SetAnglesAction.cpp" line="47"/>
        <source>arbitrary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../SetAnglesAction.cpp" line="48"/>
        <source>angle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
