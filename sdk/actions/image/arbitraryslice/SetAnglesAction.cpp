/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "SetAnglesAction.h"

using namespace camitk;

// --------------- Constructor -------------------
SetAnglesAction::SetAnglesAction(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Adjust Angles");
    setDescription(tr("This action allows user to adjust the angle of the slice of the arbitrary viewer."));
    setComponent("ImageComponent");

    // Setting classification family and tags
    setFamily("Arbitrary Slice");
    addTag(tr("arbitrary slice"));
    addTag(tr("arbitrary"));
    addTag(tr("angle"));

    actionWidget = 0;

    setEmbedded(true);
}

// --------------- destructor -------------------
SetAnglesAction::~SetAnglesAction() {
}

// --------------- getWidget -------------------
QWidget* SetAnglesAction::getWidget() {
    if (!actionWidget) {
        actionWidget = new AnglesSetterWidget();
    }

    return actionWidget;
}
