/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef RESAMPLEACTION_H
#define RESAMPLEACTION_H


#include <Action.h>

#include <ImageComponent.h>

/** ResampleAction rescales an image to the given number of pixels, the output scalar type can be changed .
  * This class use the vtkImageShiftScale class.
  */
class ResampleAction : public camitk::Action {

    Q_OBJECT

public:

    /**
    \enum Enum_Scalar_Type
    \brief output type of the resampling
    */
    enum ScalarType {
        SAME_AS_INPUT,
        UNSIGNED_CHAR,
        CHAR,
        UNSIGNED_SHORT,
        SHORT,
        UNSIGNED_INT,
        INT,
        FLOAT,
        DOUBLE
    };

    Q_ENUMS(ScalarType)

    /// Default Constructor
    ResampleAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~ResampleAction();

    /// Retrieve the scalar type
    ScalarType getScalarType();

public slots:
    /// method called when the action is applied
    virtual ApplyStatus apply();


private:

    ///Scalar type of the output
    ScalarType Scalar_Type;
    /// helper method to simplify the target component processing
    virtual void process(camitk::ImageComponent*);
};

Q_DECLARE_METATYPE(ResampleAction::ScalarType)
#endif // RESAMPLEACTION_H

