/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include <QList>
#include <QVector3D>
#include <PickedPixelMap.h>
#include <Application.h>

#include "BoxVOIWidget.h"
using namespace camitk;

BoxVOIWidget::BoxVOIWidget(Action* action)
    : QWidget() {
    this->myAction = action;
    ui.setupUi(this);
    pickingW = new MultiPickingWidget(this);
    ui.boxPointsGroupBox->layout()->addWidget(pickingW);
    ui.descriptionBoxVOI->viewport()->setAutoFillBackground(false);

    QObject::connect(ui.applyButton, SIGNAL(clicked()), myAction, SLOT(apply()));

}

QList<QVector3D>* BoxVOIWidget::getSeedPoints(ImageComponent* image) {

    return pickingW->getPickedPixelMap(image)->getPixelIndexList();
}


void BoxVOIWidget::updateComponent(ImageComponent* image) {

    ui.componentName->setText(image->getName());
    pickingW->updateComponent(image);
}
