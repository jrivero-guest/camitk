# ------------------------------------------
# Project version / CamiTKConfig generation
# ------------------------------------------
# A non-white space unique name for the CEP
set(CEP_NAME "SDK")

# Description of this CEP in few lines
set(CEP_DESCRIPTION "CamiTK SDK (Software Development Kit) is the base of all CEP.
This CEP is the base of CamiTK and is required to do any development in CamiTK. It provides the CamiTK core library, some essential component and action extensions to support the basic mesh and medical image formats, as well camitk-imp, camitk-wizard and camitk-asm (the action state machine application). The SDK is based on VTK and Qt, and that's all. Additionally to CamiTK core, it provides three useful libraries: coreschema (the CamiTK Core XML data-binding library), qtpropertybrowser (to build GUI for Qt's meta-property) and qcustomplot (simple but powerful plot widget for Qt).")

# Default application to run (change this if this cep provide an application extensions you want to run by default on MSVC)
set(CEP_DEFAULT_APPLICATION "camitk-imp")

# postfix for MSVC debug version
set(CAMITK_DEBUG_POSTFIX "-debug")

# define static version useful for package release (without any svn revision, see communityedition top level CMakeLists.txt)
set(CAMITK_VERSION_STRING "${CAMITK_PROJECT_NAME} ${CAMITK_VERSION_MAJOR}.${CAMITK_VERSION_MINOR}.${CAMITK_VERSION_PATCH}")
# prepare the file for the camitk package source build
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/libraries/core/CamiTKVersion.h.in ${CMAKE_BINARY_DIR}/CamiTKPackageVersion.h @ONLY)

# Source package has a file CamiTKVersion.h in the source tree
if(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/libraries/core/CamiTKVersion.h")
    set(CAMITK_SOURCE_PACKAGE_VERSION "0")
    # If this file does not exist => this is a svn checkouted source tree.
    find_package(Git QUIET)
    if(GIT_FOUND)
        set(CAMITK_GIT_FOUND "1")
        # Call a macro Git_WC_INFO which is define in
        # GetGitInfo.cmake to get all information from
        # the svn command itself
        include(cmake/modules/macros/GetGitInfo.cmake)
        get_git_info(${CMAKE_CURRENT_SOURCE_DIR})
        set(CAMITK_VERSION_GIT ".${CAMITK_GIT_BRANCH}.${CAMITK_GIT_ABBREVIATED_HASH}")
        set(CAMITK_GIT_FOUND "1")
        set(CAMITK_GIT_HASH "${CAMITK_GIT_HASH}")
        set(CAMITK_GIT_DATE "${CAMITK_GIT_COMMITER_DATE}")
    else()
        set(CAMITK_VERSION_GIT ".git")
        set(CAMITK_GIT_FOUND "0")
    endif()    

    # specific version string using the git revision
    # Transform "/" to "-" as "/" in git branch name generates problems when storing settings (it adds an extra directory)
    string(REPLACE "/" "-" CAMITK_ESCAPED_VERSION_GIT ${CAMITK_VERSION_GIT})
    set(CAMITK_VERSION_STRING "${CAMITK_VERSION_STRING}${CAMITK_ESCAPED_VERSION_GIT}")
else()
    set(CAMITK_GIT_FOUND "0")
    set(CAMITK_SOURCE_PACKAGE_VERSION "1")
endif()

# prepare the CamiTKConfig.cmake for installation (first delete it to force regeneration)
execute_process(COMMAND ${CMAKE_COMMAND} -E remove -f ${CMAKE_BINARY_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cmake/CamiTKConfig.cmake)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules/CamiTKConfig.cmake.in ${CMAKE_BINARY_DIR}/share/${CAMITK_SHORT_VERSION_STRING}/cmake/CamiTKConfig.cmake @ONLY)

# now create the correct version header
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/libraries/core/CamiTKVersion.h.in ${CMAKE_CURRENT_BINARY_DIR}/libraries/core/CamiTKVersion.h)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR})

message(STATUS "Welcome to ${CAMITK_VERSION_STRING} SDK compilation")
