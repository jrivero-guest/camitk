<!-- physical model is a generic representation for 3D physical model (FEM, spring mass network, phymulob...) -->
<physicalModel name="Truth Cube 0%" nrOfAtoms="729"
 nrOfExclusiveComponents="4"
 nrOfInformativeComponents="4"
 nrOfCells="1844"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent  name="element list" >
<nrOfStructures value="729"/>
<atom>
<atomProperties index="0" x="-40" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="1" x="-30" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="2" x="-20" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="3" x="-10" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="4" x="0" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="5" x="10" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="6" x="20" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="7" x="30" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="8" x="40" y="-37" z="-57"  />
</atom>
<atom>
<atomProperties index="9" x="-40" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="10" x="-29" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="11" x="-20" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="12" x="-10" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="13" x="0" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="14" x="10" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="15" x="20" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="16" x="30" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="17" x="40" y="-27" z="-57"  />
</atom>
<atom>
<atomProperties index="18" x="-40" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="19" x="-30" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="20" x="-20" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="21" x="-10" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="22" x="0" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="23" x="10" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="24" x="20" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="25" x="30" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="26" x="40" y="-17" z="-57"  />
</atom>
<atom>
<atomProperties index="27" x="-40" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="28" x="-30" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="29" x="-20" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="30" x="-10" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="31" x="0" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="32" x="10" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="33" x="20" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="34" x="30" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="35" x="40" y="-7" z="-57"  />
</atom>
<atom>
<atomProperties index="36" x="-40" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="37" x="-30" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="38" x="-20" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="39" x="-10" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="40" x="0" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="41" x="10" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="42" x="20" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="43" x="30" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="44" x="40" y="3" z="-57"  />
</atom>
<atom>
<atomProperties index="45" x="-40" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="46" x="-30" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="47" x="-20" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="48" x="-10" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="49" x="0" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="50" x="10" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="51" x="20" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="52" x="30" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="53" x="40" y="13" z="-57"  />
</atom>
<atom>
<atomProperties index="54" x="-40" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="55" x="-30" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="56" x="-20" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="57" x="-10" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="58" x="0" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="59" x="10" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="60" x="20" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="61" x="30" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="62" x="40" y="23" z="-57"  />
</atom>
<atom>
<atomProperties index="63" x="-40" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="64" x="-30" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="65" x="-20" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="66" x="-10" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="67" x="0" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="68" x="10" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="69" x="20" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="70" x="30" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="71" x="40" y="33" z="-57"  />
</atom>
<atom>
<atomProperties index="72" x="-40" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="73" x="-30" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="74" x="-20" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="75" x="-10" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="76" x="0" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="77" x="10" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="78" x="20" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="79" x="30" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="80" x="40" y="43" z="-57"  />
</atom>
<atom>
<atomProperties index="81" x="-40" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="82" x="-30" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="83" x="-20" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="84" x="-10" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="85" x="0" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="86" x="10" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="87" x="20" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="88" x="30" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="89" x="40" y="-37" z="-47"  />
</atom>
<atom>
<atomProperties index="90" x="-40" y="-27" z="-47"  />
</atom>
<atom>
<atomProperties index="98" x="40" y="-27" z="-47"  />
</atom>
<atom>
<atomProperties index="99" x="-40" y="-17" z="-47"  />
</atom>
<atom>
<atomProperties index="107" x="40" y="-17" z="-47"  />
</atom>
<atom>
<atomProperties index="108" x="-40" y="-7" z="-47"  />
</atom>
<atom>
<atomProperties index="116" x="40" y="-7" z="-47"  />
</atom>
<atom>
<atomProperties index="117" x="-40" y="3" z="-47"  />
</atom>
<atom>
<atomProperties index="125" x="40" y="3" z="-47"  />
</atom>
<atom>
<atomProperties index="126" x="-40" y="13" z="-47"  />
</atom>
<atom>
<atomProperties index="134" x="40" y="13" z="-47"  />
</atom>
<atom>
<atomProperties index="135" x="-40" y="23" z="-47"  />
</atom>
<atom>
<atomProperties index="143" x="40" y="23" z="-47"  />
</atom>
<atom>
<atomProperties index="144" x="-40" y="33" z="-47"  />
</atom>
<atom>
<atomProperties index="152" x="40" y="33" z="-47"  />
</atom>
<atom>
<atomProperties index="153" x="-40" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="154" x="-30" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="155" x="-20" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="156" x="-10" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="157" x="0" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="158" x="10" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="159" x="20" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="160" x="30" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="161" x="40" y="43" z="-47"  />
</atom>
<atom>
<atomProperties index="162" x="-40" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="163" x="-30" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="164" x="-20" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="165" x="-10" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="166" x="0" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="167" x="10" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="168" x="20" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="169" x="30" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="170" x="40" y="-37" z="-37"  />
</atom>
<atom>
<atomProperties index="171" x="-40" y="-27" z="-37"  />
</atom>
<atom>
<atomProperties index="179" x="40" y="-27" z="-37"  />
</atom>
<atom>
<atomProperties index="180" x="-40" y="-17" z="-37"  />
</atom>
<atom>
<atomProperties index="188" x="40" y="-17" z="-37"  />
</atom>
<atom>
<atomProperties index="189" x="-40" y="-7" z="-37"  />
</atom>
<atom>
<atomProperties index="197" x="40" y="-7" z="-37"  />
</atom>
<atom>
<atomProperties index="198" x="-40" y="3" z="-37"  />
</atom>
<atom>
<atomProperties index="206" x="40" y="3" z="-37"  />
</atom>
<atom>
<atomProperties index="207" x="-40" y="13" z="-37"  />
</atom>
<atom>
<atomProperties index="215" x="40" y="13" z="-37"  />
</atom>
<atom>
<atomProperties index="216" x="-40" y="23" z="-37"  />
</atom>
<atom>
<atomProperties index="224" x="40" y="23" z="-37"  />
</atom>
<atom>
<atomProperties index="225" x="-40" y="33" z="-37"  />
</atom>
<atom>
<atomProperties index="233" x="40" y="33" z="-37"  />
</atom>
<atom>
<atomProperties index="234" x="-40" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="235" x="-30" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="236" x="-20" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="237" x="-10" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="238" x="0" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="239" x="10" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="240" x="20" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="241" x="30" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="242" x="40" y="43" z="-37"  />
</atom>
<atom>
<atomProperties index="243" x="-40" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="244" x="-30" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="245" x="-20" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="246" x="-10" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="247" x="0" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="248" x="10" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="249" x="20" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="250" x="30" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="251" x="40" y="-37" z="-27"  />
</atom>
<atom>
<atomProperties index="252" x="-40" y="-27" z="-27"  />
</atom>
<atom>
<atomProperties index="260" x="40" y="-27" z="-27"  />
</atom>
<atom>
<atomProperties index="261" x="-40" y="-17" z="-27"  />
</atom>
<atom>
<atomProperties index="269" x="40" y="-17" z="-27"  />
</atom>
<atom>
<atomProperties index="270" x="-40" y="-7" z="-27"  />
</atom>
<atom>
<atomProperties index="278" x="40" y="-7" z="-27"  />
</atom>
<atom>
<atomProperties index="279" x="-40" y="3" z="-27"  />
</atom>
<atom>
<atomProperties index="287" x="40" y="3" z="-27"  />
</atom>
<atom>
<atomProperties index="288" x="-40" y="13" z="-27"  />
</atom>
<atom>
<atomProperties index="296" x="40" y="13" z="-27"  />
</atom>
<atom>
<atomProperties index="297" x="-40" y="23" z="-27"  />
</atom>
<atom>
<atomProperties index="305" x="40" y="23" z="-27"  />
</atom>
<atom>
<atomProperties index="306" x="-40" y="33" z="-27"  />
</atom>
<atom>
<atomProperties index="314" x="40" y="33" z="-27"  />
</atom>
<atom>
<atomProperties index="315" x="-40" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="316" x="-30" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="317" x="-20" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="318" x="-10" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="319" x="0" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="320" x="10" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="321" x="20" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="322" x="30" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="323" x="40" y="43" z="-27"  />
</atom>
<atom>
<atomProperties index="324" x="-40" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="325" x="-30" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="326" x="-20" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="327" x="-10" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="328" x="0" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="329" x="10" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="330" x="20" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="331" x="30" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="332" x="40" y="-37" z="-17"  />
</atom>
<atom>
<atomProperties index="333" x="-40" y="-27" z="-17"  />
</atom>
<atom>
<atomProperties index="341" x="40" y="-27" z="-17"  />
</atom>
<atom>
<atomProperties index="342" x="-40" y="-17" z="-17"  />
</atom>
<atom>
<atomProperties index="350" x="40" y="-17" z="-17"  />
</atom>
<atom>
<atomProperties index="351" x="-40" y="-7" z="-17"  />
</atom>
<atom>
<atomProperties index="359" x="40" y="-7" z="-17"  />
</atom>
<atom>
<atomProperties index="360" x="-40" y="3" z="-17"  />
</atom>
<atom>
<atomProperties index="368" x="40" y="3" z="-17"  />
</atom>
<atom>
<atomProperties index="369" x="-40" y="13" z="-17"  />
</atom>
<atom>
<atomProperties index="377" x="40" y="13" z="-17"  />
</atom>
<atom>
<atomProperties index="378" x="-40" y="23" z="-17"  />
</atom>
<atom>
<atomProperties index="386" x="40" y="23" z="-17"  />
</atom>
<atom>
<atomProperties index="387" x="-40" y="33" z="-17"  />
</atom>
<atom>
<atomProperties index="395" x="40" y="33" z="-17"  />
</atom>
<atom>
<atomProperties index="396" x="-40" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="397" x="-30" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="398" x="-20" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="399" x="-10" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="400" x="0" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="401" x="10" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="402" x="20" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="403" x="30" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="404" x="40" y="43" z="-17"  />
</atom>
<atom>
<atomProperties index="405" x="-40" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="406" x="-30" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="407" x="-20" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="408" x="-10" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="409" x="0" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="410" x="10" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="411" x="20" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="412" x="30" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="413" x="40" y="-37" z="-7"  />
</atom>
<atom>
<atomProperties index="414" x="-40" y="-27" z="-7"  />
</atom>
<atom>
<atomProperties index="422" x="40" y="-27" z="-7"  />
</atom>
<atom>
<atomProperties index="423" x="-40" y="-17" z="-7"  />
</atom>
<atom>
<atomProperties index="431" x="40" y="-17" z="-7"  />
</atom>
<atom>
<atomProperties index="432" x="-40" y="-7" z="-7"  />
</atom>
<atom>
<atomProperties index="440" x="40" y="-7" z="-7"  />
</atom>
<atom>
<atomProperties index="441" x="-40" y="3" z="-7"  />
</atom>
<atom>
<atomProperties index="449" x="40" y="3" z="-7"  />
</atom>
<atom>
<atomProperties index="450" x="-40" y="13" z="-7"  />
</atom>
<atom>
<atomProperties index="458" x="40" y="13" z="-7"  />
</atom>
<atom>
<atomProperties index="459" x="-40" y="23" z="-7"  />
</atom>
<atom>
<atomProperties index="467" x="40" y="23" z="-7"  />
</atom>
<atom>
<atomProperties index="468" x="-40" y="33" z="-7"  />
</atom>
<atom>
<atomProperties index="476" x="40" y="33" z="-7"  />
</atom>
<atom>
<atomProperties index="477" x="-40" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="478" x="-30" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="479" x="-20" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="480" x="-10" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="481" x="0" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="482" x="10" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="483" x="20" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="484" x="30" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="485" x="40" y="43" z="-7"  />
</atom>
<atom>
<atomProperties index="486" x="-40" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="487" x="-30" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="488" x="-20" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="489" x="-10" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="490" x="0" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="491" x="10" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="492" x="20" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="493" x="30" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="494" x="40" y="-37" z="3"  />
</atom>
<atom>
<atomProperties index="495" x="-40" y="-27" z="3"  />
</atom>
<atom>
<atomProperties index="503" x="40" y="-27" z="3"  />
</atom>
<atom>
<atomProperties index="504" x="-40" y="-17" z="3"  />
</atom>
<atom>
<atomProperties index="512" x="40" y="-17" z="3"  />
</atom>
<atom>
<atomProperties index="513" x="-40" y="-7" z="3"  />
</atom>
<atom>
<atomProperties index="521" x="40" y="-7" z="3"  />
</atom>
<atom>
<atomProperties index="522" x="-40" y="3" z="3"  />
</atom>
<atom>
<atomProperties index="530" x="40" y="3" z="3"  />
</atom>
<atom>
<atomProperties index="531" x="-40" y="13" z="3"  />
</atom>
<atom>
<atomProperties index="539" x="40" y="13" z="3"  />
</atom>
<atom>
<atomProperties index="540" x="-40" y="23" z="3"  />
</atom>
<atom>
<atomProperties index="548" x="40" y="23" z="3"  />
</atom>
<atom>
<atomProperties index="549" x="-40" y="33" z="3"  />
</atom>
<atom>
<atomProperties index="557" x="40" y="33" z="3"  />
</atom>
<atom>
<atomProperties index="558" x="-40" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="559" x="-30" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="560" x="-20" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="561" x="-10" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="562" x="0" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="563" x="10" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="564" x="20" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="565" x="30" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="566" x="40" y="43" z="3"  />
</atom>
<atom>
<atomProperties index="567" x="-40" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="568" x="-30" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="569" x="-20" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="570" x="-10" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="571" x="0" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="572" x="10" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="573" x="20" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="574" x="30" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="575" x="40" y="-37" z="13"  />
</atom>
<atom>
<atomProperties index="576" x="-40" y="-27" z="13"  />
</atom>
<atom>
<atomProperties index="584" x="40" y="-27" z="13"  />
</atom>
<atom>
<atomProperties index="585" x="-40" y="-17" z="13"  />
</atom>
<atom>
<atomProperties index="593" x="40" y="-17" z="13"  />
</atom>
<atom>
<atomProperties index="594" x="-40" y="-7" z="13"  />
</atom>
<atom>
<atomProperties index="602" x="40" y="-7" z="13"  />
</atom>
<atom>
<atomProperties index="603" x="-40" y="3" z="13"  />
</atom>
<atom>
<atomProperties index="611" x="40" y="3" z="13"  />
</atom>
<atom>
<atomProperties index="612" x="-40" y="13" z="13"  />
</atom>
<atom>
<atomProperties index="620" x="40" y="13" z="13"  />
</atom>
<atom>
<atomProperties index="621" x="-40" y="23" z="13"  />
</atom>
<atom>
<atomProperties index="629" x="40" y="23" z="13"  />
</atom>
<atom>
<atomProperties index="630" x="-40" y="33" z="13"  />
</atom>
<atom>
<atomProperties index="638" x="40" y="33" z="13"  />
</atom>
<atom>
<atomProperties index="639" x="-40" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="640" x="-30" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="641" x="-20" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="642" x="-10" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="643" x="0" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="644" x="10" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="645" x="20" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="646" x="30" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="647" x="40" y="43" z="13"  />
</atom>
<atom>
<atomProperties index="648" x="-40" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="649" x="-30" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="650" x="-20" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="651" x="-10" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="652" x="0" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="653" x="10" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="654" x="20" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="655" x="30" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="656" x="40" y="-37" z="23"  />
</atom>
<atom>
<atomProperties index="657" x="-40" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="658" x="-29" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="659" x="-20" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="660" x="-10" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="661" x="0" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="662" x="10" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="663" x="20" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="664" x="30" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="665" x="40" y="-27" z="23"  />
</atom>
<atom>
<atomProperties index="666" x="-40" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="667" x="-30" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="668" x="-20" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="669" x="-10" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="670" x="0" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="671" x="10" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="672" x="20" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="673" x="30" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="674" x="40" y="-17" z="23"  />
</atom>
<atom>
<atomProperties index="675" x="-40" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="676" x="-30" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="677" x="-20" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="678" x="-10" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="679" x="0" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="680" x="10" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="681" x="20" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="682" x="30" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="683" x="40" y="-7" z="23"  />
</atom>
<atom>
<atomProperties index="684" x="-40" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="685" x="-30" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="686" x="-20" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="687" x="-10" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="688" x="0" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="689" x="10" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="690" x="20" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="691" x="30" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="692" x="40" y="3" z="23"  />
</atom>
<atom>
<atomProperties index="693" x="-40" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="694" x="-30" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="695" x="-20" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="696" x="-10" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="697" x="0" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="698" x="10" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="699" x="20" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="700" x="30" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="701" x="40" y="13" z="23"  />
</atom>
<atom>
<atomProperties index="702" x="-40" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="703" x="-30" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="704" x="-20" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="705" x="-10" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="706" x="0" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="707" x="10" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="708" x="20" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="709" x="30" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="710" x="40" y="23" z="23"  />
</atom>
<atom>
<atomProperties index="711" x="-40" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="712" x="-30" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="713" x="-20" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="714" x="-10" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="715" x="0" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="716" x="10" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="717" x="20" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="718" x="30" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="719" x="40" y="33" z="23"  />
</atom>
<atom>
<atomProperties index="720" x="-40" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="721" x="-30" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="722" x="-20" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="723" x="-10" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="724" x="0" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="725" x="10" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="726" x="20" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="727" x="30" y="43" z="23"  />
</atom>
<atom>
<atomProperties index="728" x="40" y="43" z="23"  />
</atom>

<!-- beads starts here -->
<atom>
<atomProperties index="577" x="-29.69" y="-28.86" z="12.62"  />
</atom>
<atom>
<atomProperties index="496" x="-29.07" y="-27.43" z="3.33"  />
</atom>
<atom>
<atomProperties index="415" x="-28.76" y="-26.88" z="-6.49"  />
</atom>
<atom>
<atomProperties index="334" x="-28.91" y="-27.44" z="-16.83"  />
</atom>
<atom>
<atomProperties index="253" x="-28.95" y="-26.88" z="-26.91"  />
</atom>
<atom>
<atomProperties index="172" x="-28.76" y="-26.88" z="-37.29"  />
</atom>
<atom>
<atomProperties index="91" x="-28.76" y="-26.88" z="-47.31"  />
</atom>
<atom>
<atomProperties index="586" x="-30.16" y="-18.75" z="12.43"  />
</atom>
<atom>
<atomProperties index="505" x="-29.18" y="-17.45" z="3.31"  />
</atom>
<atom>
<atomProperties index="424" x="-28.95" y="-17.5" z="-6.68"  />
</atom>
<atom>
<atomProperties index="343" x="-29.01" y="-17.4" z="-16.88"  />
</atom>
<atom>
<atomProperties index="262" x="-29.14" y="-16.88" z="-26.77"  />
</atom>
<atom>
<atomProperties index="181" x="-28.72" y="-17.45" z="-37.14"  />
</atom>
<atom>
<atomProperties index="100" x="-28.72" y="-17.33" z="-47.28"  />
</atom>
<atom>
<atomProperties index="595" x="-30.76" y="-8.13" z="12.29"  />
</atom>
<atom>
<atomProperties index="514" x="-29.22" y="-6.88" z="3.07"  />
</atom>
<atom>
<atomProperties index="433" x="-29.13" y="-6.88" z="-6.87"  />
</atom>
<atom>
<atomProperties index="352" x="-29.79" y="-7.58" z="-17.02"  />
</atom>
<atom>
<atomProperties index="271" x="-28.95" y="-6.88" z="-26.91"  />
</atom>
<atom>
<atomProperties index="190" x="-28.76" y="-6.88" z="-37.29"  />
</atom>
<atom>
<atomProperties index="109" x="-28.93" y="-7.49" z="-47.15"  />
</atom>
<atom>
<atomProperties index="604" x="-30.05" y="1.33" z="12.27"  />
</atom>
<atom>
<atomProperties index="523" x="-29.5" y="3.13" z="3.15"  />
</atom>
<atom>
<atomProperties index="442" x="-29.5" y="3.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="361" x="-29.1" y="3.13" z="-17.22"  />
</atom>
<atom>
<atomProperties index="280" x="-29.1" y="3.13" z="-26.94"  />
</atom>
<atom>
<atomProperties index="199" x="-29.13" y="3.13" z="-37.29"  />
</atom>
<atom>
<atomProperties index="118" x="-28.87" y="3.13" z="-47.5"  />
</atom>
<atom>
<atomProperties index="613" x="-30.27" y="12.52" z="12.08"  />
</atom>
<atom>
<atomProperties index="532" x="-29.74" y="13.13" z="3"  />
</atom>
<atom>
<atomProperties index="451" x="-29.35" y="13.13" z="-6.91"  />
</atom>
<atom>
<atomProperties index="370" x="-30.62" y="13.13" z="-17.26"  />
</atom>
<atom>
<atomProperties index="289" x="-29.14" y="13.13" z="-27.09"  />
</atom>
<atom>
<atomProperties index="208" x="-29.08" y="12.43" z="-37.47"  />
</atom>
<atom>
<atomProperties index="127" x="-28.82" y="12.57" z="-47.49"  />
</atom>
<atom>
<atomProperties index="622" x="-30.69" y="23.83" z="12.29"  />
</atom>
<atom>
<atomProperties index="541" x="-30.11" y="23.12" z="3"  />
</atom>
<atom>
<atomProperties index="460" x="-30.08" y="22.49" z="-7.03"  />
</atom>
<atom>
<atomProperties index="379" x="-30.24" y="23.12" z="-17.26"  />
</atom>
<atom>
<atomProperties index="298" x="-29.32" y="22.63" z="-27.28"  />
</atom>
<atom>
<atomProperties index="217" x="-29.13" y="23.12" z="-37.67"  />
</atom>
<atom>
<atomProperties index="136" x="-28.72" y="22.67" z="-47.65"  />
</atom>
<atom>
<atomProperties index="631" x="-30.71" y="34.08" z="12.22"  />
</atom>
<atom>
<atomProperties index="550" x="-29.89" y="33.63" z="3"  />
</atom>
<atom>
<atomProperties index="469" x="-29.99" y="33.13" z="-7.05"  />
</atom>
<atom>
<atomProperties index="388" x="-29.53" y="33.13" z="-17.22"  />
</atom>
<atom>
<atomProperties index="307" x="-29.5" y="33.13" z="-27.28"  />
</atom>
<atom>
<atomProperties index="226" x="-29.5" y="33.13" z="-37.67"  />
</atom>
<atom>
<atomProperties index="145" x="-29.32" y="33.13" z="-47.69"  />
</atom>
<atom>
<atomProperties index="578" x="-19.25" y="-29.37" z="12.58"  />
</atom>
<atom>
<atomProperties index="497" x="-19.26" y="-27.48" z="3.37"  />
</atom>
<atom>
<atomProperties index="416" x="-17.81" y="-26.88" z="-6.49"  />
</atom>
<atom>
<atomProperties index="335" x="-18.93" y="-26.88" z="-16.89"  />
</atom>
<atom>
<atomProperties index="254" x="-18.55" y="-26.88" z="-26.91"  />
</atom>
<atom>
<atomProperties index="173" x="-18.77" y="-26.88" z="-36.96"  />
</atom>
<atom>
<atomProperties index="92" x="-18.55" y="-26.88" z="-47.13"  />
</atom>
<atom>
<atomProperties index="587" x="-20.04" y="-19.37" z="12.43"  />
</atom>
<atom>
<atomProperties index="506" x="-19.2" y="-17.62" z="3.24"  />
</atom>
<atom>
<atomProperties index="425" x="-18.62" y="-16.88" z="-6.69"  />
</atom>
<atom>
<atomProperties index="344" x="-18.77" y="-18.12" z="-16.85"  />
</atom>
<atom>
<atomProperties index="263" x="-18.74" y="-16.88" z="-26.9"  />
</atom>
<atom>
<atomProperties index="182" x="-18.74" y="-16.88" z="-37.29"  />
</atom>
<atom>
<atomProperties index="101" x="-18.74" y="-16.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="596" x="-20.07" y="-10.05" z="12.38"  />
</atom>
<atom>
<atomProperties index="515" x="-19.2" y="-6.88" z="3.24"  />
</atom>
<atom>
<atomProperties index="434" x="-19.11" y="-6.88" z="-6.87"  />
</atom>
<atom>
<atomProperties index="353" x="-19.14" y="-6.88" z="-16.92"  />
</atom>
<atom>
<atomProperties index="272" x="-18.74" y="-6.88" z="-26.9"  />
</atom>
<atom>
<atomProperties index="191" x="-18.93" y="-6.88" z="-37.3"  />
</atom>
<atom>
<atomProperties index="110" x="-18.73" y="-7.38" z="-47.16"  />
</atom>
<atom>
<atomProperties index="605" x="-20.78" y="0.98" z="12.18"  />
</atom>
<atom>
<atomProperties index="524" x="-19.52" y="3.13" z="3"  />
</atom>
<atom>
<atomProperties index="443" x="-18.77" y="3.13" z="-6.83"  />
</atom>
<atom>
<atomProperties index="362" x="-19.23" y="3.13" z="-17.07"  />
</atom>
<atom>
<atomProperties index="281" x="-18.77" y="3.13" z="-26.94"  />
</atom>
<atom>
<atomProperties index="200" x="-19.11" y="3.13" z="-37.29"  />
</atom>
<atom>
<atomProperties index="119" x="-19.06" y="3.59" z="-47.27"  />
</atom>
<atom>
<atomProperties index="614" x="-20.18" y="11.88" z="12.38"  />
</atom>
<atom>
<atomProperties index="533" x="-19.85" y="13.13" z="3.15"  />
</atom>
<atom>
<atomProperties index="452" x="-19.11" y="13.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="371" x="-19.64" y="13.13" z="-17.1"  />
</atom>
<atom>
<atomProperties index="290" x="-19.11" y="13.13" z="-26.9"  />
</atom>
<atom>
<atomProperties index="209" x="-19.11" y="13.13" z="-37.29"  />
</atom>
<atom>
<atomProperties index="128" x="-18.74" y="13.13" z="-47.31"  />
</atom>
<atom>
<atomProperties index="623" x="-21.15" y="23.42" z="12.14"  />
</atom>
<atom>
<atomProperties index="542" x="-19.67" y="23.12" z="2.97"  />
</atom>
<atom>
<atomProperties index="461" x="-19.49" y="23.12" z="-7.05"  />
</atom>
<atom>
<atomProperties index="380" x="-18.37" y="23.12" z="-17.26"  />
</atom>
<atom>
<atomProperties index="299" x="-19.09" y="23.12" z="-27.24"  />
</atom>
<atom>
<atomProperties index="218" x="-19.09" y="23.12" z="-37.63"  />
</atom>
<atom>
<atomProperties index="137" x="-18.93" y="22.5" z="-47.5"  />
</atom>
<atom>
<atomProperties index="632" x="-20.73" y="33.58" z="12.06"  />
</atom>
<atom>
<atomProperties index="551" x="-19.82" y="33.13" z="3.12"  />
</atom>
<atom>
<atomProperties index="470" x="-19.69" y="33.74" z="-7.03"  />
</atom>
<atom>
<atomProperties index="389" x="-19.11" y="33.13" z="-17.26"  />
</atom>
<atom>
<atomProperties index="308" x="-19.46" y="33.13" z="-27.24"  />
</atom>
<atom>
<atomProperties index="227" x="-19.46" y="33.13" z="-37.63"  />
</atom>
<atom>
<atomProperties index="146" x="-19.11" y="33.13" z="-47.69"  />
</atom>
<atom>
<atomProperties index="579" x="-8.61" y="-28.92" z="12.85"  />
</atom>
<atom>
<atomProperties index="498" x="-8.78" y="-26.88" z="3.58"  />
</atom>
<atom>
<atomProperties index="417" x="-8.54" y="-26.88" z="-6.61"  />
</atom>
<atom>
<atomProperties index="336" x="-8.73" y="-26.88" z="-16.7"  />
</atom>
<atom>
<atomProperties index="255" x="-8.35" y="-26.88" z="-26.9"  />
</atom>
<atom>
<atomProperties index="174" x="-8.57" y="-26.88" z="-37.09"  />
</atom>
<atom>
<atomProperties index="93" x="-8.72" y="-26.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="588" x="-8.91" y="-19.96" z="12.66"  />
</atom>
<atom>
<atomProperties index="507" x="-9.12" y="-16.88" z="3.19"  />
</atom>
<atom>
<atomProperties index="426" x="-8.69" y="-16.88" z="-6.53"  />
</atom>
<atom>
<atomProperties index="345" x="-8.93" y="-17.49" z="-16.72"  />
</atom>
<atom>
<atomProperties index="264" x="-8.69" y="-16.88" z="-26.87"  />
</atom>
<atom>
<atomProperties index="183" x="-8.73" y="-16.88" z="-37.11"  />
</atom>
<atom>
<atomProperties index="102" x="-8.72" y="-16.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="597" x="-8.58" y="-8.98" z="12.6"  />
</atom>
<atom>
<atomProperties index="516" x="-9.09" y="-6.88" z="3.15"  />
</atom>
<atom>
<atomProperties index="435" x="-8.98" y="-6.88" z="-6.69"  />
</atom>
<atom>
<atomProperties index="354" x="-9.07" y="-6.88" z="-16.85"  />
</atom>
<atom>
<atomProperties index="273" x="-8.91" y="-6.88" z="-26.91"  />
</atom>
<atom>
<atomProperties index="192" x="-9.07" y="-6.88" z="-37.26"  />
</atom>
<atom>
<atomProperties index="111" x="-8.68" y="-7.45" z="-47.1"  />
</atom>
<atom>
<atomProperties index="606" x="-10.39" y="1.88" z="12.43"  />
</atom>
<atom>
<atomProperties index="525" x="-9.31" y="3.13" z="3.11"  />
</atom>
<atom>
<atomProperties index="444" x="-10.17" y="4.38" z="-6.9"  />
</atom>
<atom>
<atomProperties index="363" x="-9.09" y="3.13" z="-16.88"  />
</atom>
<atom>
<atomProperties index="282" x="-8.69" y="3.13" z="-26.94"  />
</atom>
<atom>
<atomProperties index="201" x="-9.09" y="3.13" z="-37.29"  />
</atom>
<atom>
<atomProperties index="120" x="-8.87" y="3.13" z="-47.17"  />
</atom>
<atom>
<atomProperties index="615" x="-10.03" y="12.65" z="12.27"  />
</atom>
<atom>
<atomProperties index="534" x="-9.46" y="13.13" z="3.15"  />
</atom>
<atom>
<atomProperties index="453" x="-9.09" y="13.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="372" x="-9.44" y="13.13" z="-17.22"  />
</atom>
<atom>
<atomProperties index="291" x="-9.46" y="13.88" z="-27.08"  />
</atom>
<atom>
<atomProperties index="210" x="-9.09" y="13.13" z="-37.29"  />
</atom>
<atom>
<atomProperties index="129" x="-8.72" y="13.13" z="-47.31"  />
</atom>
<atom>
<atomProperties index="624" x="-10.44" y="22.78" z="12.12"  />
</atom>
<atom>
<atomProperties index="543" x="-9.44" y="23.12" z="2.82"  />
</atom>
<atom>
<atomProperties index="462" x="-9.12" y="23.12" z="-6.9"  />
</atom>
<atom>
<atomProperties index="381" x="-9.46" y="23.12" z="-17.26"  />
</atom>
<atom>
<atomProperties index="300" x="-9.12" y="23.12" z="-27.24"  />
</atom>
<atom>
<atomProperties index="219" x="-9.07" y="23.12" z="-37.63"  />
</atom>
<atom>
<atomProperties index="138" x="-9.09" y="23.12" z="-47.31"  />
</atom>
<atom>
<atomProperties index="633" x="-11.04" y="33.8" z="12.08"  />
</atom>
<atom>
<atomProperties index="552" x="-9.47" y="33.13" z="2.97"  />
</atom>
<atom>
<atomProperties index="471" x="-9.28" y="34.38" z="-6.87"  />
</atom>
<atom>
<atomProperties index="390" x="-9.46" y="33.13" z="-17.26"  />
</atom>
<atom>
<atomProperties index="309" x="-9.09" y="33.13" z="-27.28"  />
</atom>
<atom>
<atomProperties index="228" x="-8.72" y="33.13" z="-37.67"  />
</atom>
<atom>
<atomProperties index="147" x="-9.42" y="33.13" z="-47.53"  />
</atom>
<atom>
<atomProperties index="580" x="2.3" y="-28.71" z="12.63"  />
</atom>
<atom>
<atomProperties index="499" x="0.78" y="-26.88" z="3.48"  />
</atom>
<atom>
<atomProperties index="418" x="2.04" y="-26.88" z="-6.49"  />
</atom>
<atom>
<atomProperties index="337" x="1.31" y="-27.37" z="-16.65"  />
</atom>
<atom>
<atomProperties index="256" x="1.64" y="-26.88" z="-26.57"  />
</atom>
<atom>
<atomProperties index="175" x="1.48" y="-26.88" z="-36.93"  />
</atom>
<atom>
<atomProperties index="94" x="1.48" y="-26.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="589" x="2.04" y="-20" z="12.62"  />
</atom>
<atom>
<atomProperties index="508" x="0.96" y="-16.88" z="3.49"  />
</atom>
<atom>
<atomProperties index="427" x="2.41" y="-16.88" z="-6.49"  />
</atom>
<atom>
<atomProperties index="346" x="0.95" y="-16.88" z="-16.85"  />
</atom>
<atom>
<atomProperties index="265" x="1.64" y="-16.88" z="-26.87"  />
</atom>
<atom>
<atomProperties index="184" x="1.33" y="-16.88" z="-36.95"  />
</atom>
<atom>
<atomProperties index="103" x="0.93" y="-16.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="598" x="1.3" y="-9.38" z="12.43"  />
</atom>
<atom>
<atomProperties index="517" x="1.04" y="-6.88" z="3.33"  />
</atom>
<atom>
<atomProperties index="436" x="2.01" y="-6.88" z="-6.83"  />
</atom>
<atom>
<atomProperties index="355" x="0.58" y="-6.88" z="-16.85"  />
</atom>
<atom>
<atomProperties index="274" x="1.3" y="-6.88" z="-26.9"  />
</atom>
<atom>
<atomProperties index="193" x="1.64" y="-6.88" z="-36.96"  />
</atom>
<atom>
<atomProperties index="112" x="1.3" y="-6.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="607" x="1.39" y="1.2" z="12.46"  />
</atom>
<atom>
<atomProperties index="526" x="1.15" y="3.13" z="3.19"  />
</atom>
<atom>
<atomProperties index="445" x="1.33" y="3.13" z="-6.83"  />
</atom>
<atom>
<atomProperties index="364" x="0.56" y="3.13" z="-16.88"  />
</atom>
<atom>
<atomProperties index="283" x="1.3" y="3.13" z="-26.9"  />
</atom>
<atom>
<atomProperties index="202" x="1.27" y="3.13" z="-37.26"  />
</atom>
<atom>
<atomProperties index="121" x="1.26" y="3.13" z="-47.16"  />
</atom>
<atom>
<atomProperties index="616" x="1.17" y="11.88" z="12.28"  />
</atom>
<atom>
<atomProperties index="535" x="0.93" y="13.13" z="3.15"  />
</atom>
<atom>
<atomProperties index="454" x="1.48" y="13.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="373" x="1.27" y="13.13" z="-16.92"  />
</atom>
<atom>
<atomProperties index="292" x="0.96" y="13.13" z="-26.94"  />
</atom>
<atom>
<atomProperties index="211" x="1.11" y="13.13" z="-37.3"  />
</atom>
<atom>
<atomProperties index="130" x="1.06" y="13.13" z="-47.16"  />
</atom>
<atom>
<atomProperties index="625" x="0.74" y="23.12" z="12.25"  />
</atom>
<atom>
<atomProperties index="544" x="0.73" y="23.12" z="3.04"  />
</atom>
<atom>
<atomProperties index="463" x="1.3" y="23.12" z="-6.87"  />
</atom>
<atom>
<atomProperties index="382" x="0.9" y="23.12" z="-16.92"  />
</atom>
<atom>
<atomProperties index="301" x="0.92" y="23.12" z="-27.09"  />
</atom>
<atom>
<atomProperties index="220" x="0.96" y="23.12" z="-37.33"  />
</atom>
<atom>
<atomProperties index="139" x="0.93" y="23.12" z="-47.31"  />
</atom>
<atom>
<atomProperties index="634" x="-0.02" y="33.86" z="12.07"  />
</atom>
<atom>
<atomProperties index="553" x="0.55" y="33.62" z="2.98"  />
</atom>
<atom>
<atomProperties index="472" x="0.55" y="33.13" z="-7.05"  />
</atom>
<atom>
<atomProperties index="391" x="0.93" y="33.13" z="-17.26"  />
</atom>
<atom>
<atomProperties index="310" x="0.9" y="33.13" z="-27.24"  />
</atom>
<atom>
<atomProperties index="229" x="0.92" y="33.13" z="-37.48"  />
</atom>
<atom>
<atomProperties index="148" x="0.93" y="33.13" z="-47.31"  />
</atom>
<atom>
<atomProperties index="581" x="12.29" y="-28.62" z="12.83"  />
</atom>
<atom>
<atomProperties index="500" x="11.69" y="-26.88" z="3.53"  />
</atom>
<atom>
<atomProperties index="419" x="11.32" y="-26.88" z="-6.49"  />
</atom>
<atom>
<atomProperties index="338" x="11.32" y="-26.88" z="-16.51"  />
</atom>
<atom>
<atomProperties index="257" x="11.5" y="-26.88" z="-26.54"  />
</atom>
<atom>
<atomProperties index="176" x="11.69" y="-26.88" z="-36.92"  />
</atom>
<atom>
<atomProperties index="95" x="11.69" y="-26.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="590" x="12.8" y="-19.37" z="12.8"  />
</atom>
<atom>
<atomProperties index="509" x="12.03" y="-16.88" z="3.49"  />
</atom>
<atom>
<atomProperties index="428" x="12.43" y="-16.88" z="-6.49"  />
</atom>
<atom>
<atomProperties index="347" x="12.03" y="-16.88" z="-16.55"  />
</atom>
<atom>
<atomProperties index="266" x="11.66" y="-16.88" z="-26.57"  />
</atom>
<atom>
<atomProperties index="185" x="11.69" y="-16.88" z="-36.92"  />
</atom>
<atom>
<atomProperties index="104" x="11.66" y="-16.88" z="-46.91"  />
</atom>
<atom>
<atomProperties index="599" x="12.76" y="-8.61" z="12.78"  />
</atom>
<atom>
<atomProperties index="518" x="10.99" y="-6.88" z="3.37"  />
</atom>
<atom>
<atomProperties index="437" x="11.69" y="-6.88" z="-6.49"  />
</atom>
<atom>
<atomProperties index="356" x="11.29" y="-6.88" z="-16.85"  />
</atom>
<atom>
<atomProperties index="275" x="11.29" y="-6.88" z="-26.87"  />
</atom>
<atom>
<atomProperties index="194" x="10.98" y="-6.88" z="-36.95"  />
</atom>
<atom>
<atomProperties index="113" x="11.5" y="-6.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="608" x="11.91" y="1.88" z="12.39"  />
</atom>
<atom>
<atomProperties index="527" x="10.95" y="3.13" z="3.15"  />
</atom>
<atom>
<atomProperties index="446" x="11.69" y="3.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="365" x="11.32" y="3.13" z="-16.88"  />
</atom>
<atom>
<atomProperties index="284" x="11.32" y="3.13" z="-26.9"  />
</atom>
<atom>
<atomProperties index="203" x="11.34" y="3.13" z="-37.26"  />
</atom>
<atom>
<atomProperties index="122" x="11.08" y="3.13" z="-47.16"  />
</atom>
<atom>
<atomProperties index="617" x="11.91" y="11.88" z="12.39"  />
</atom>
<atom>
<atomProperties index="536" x="10.8" y="13.13" z="3.19"  />
</atom>
<atom>
<atomProperties index="455" x="11.69" y="13.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="374" x="11.17" y="13.85" z="-16.92"  />
</atom>
<atom>
<atomProperties index="293" x="10.95" y="13.13" z="-26.9"  />
</atom>
<atom>
<atomProperties index="212" x="10.6" y="13.13" z="-37.26"  />
</atom>
<atom>
<atomProperties index="131" x="11.5" y="13.13" z="-47.13"  />
</atom>
<atom>
<atomProperties index="626" x="11.32" y="23.12" z="12.06"  />
</atom>
<atom>
<atomProperties index="545" x="11.23" y="23.13" z="2.87"  />
</atom>
<atom>
<atomProperties index="464" x="10.98" y="23.12" z="-6.9"  />
</atom>
<atom>
<atomProperties index="383" x="10.57" y="23.12" z="-17.07"  />
</atom>
<atom>
<atomProperties index="302" x="10.98" y="23.12" z="-26.94"  />
</atom>
<atom>
<atomProperties index="221" x="10.38" y="23.12" z="-37.41"  />
</atom>
<atom>
<atomProperties index="140" x="10.58" y="23.12" z="-47.31"  />
</atom>
<atom>
<atomProperties index="635" x="10.61" y="33.56" z="12.06"  />
</atom>
<atom>
<atomProperties index="554" x="10.83" y="33.13" z="2.97"  />
</atom>
<atom>
<atomProperties index="473" x="10.95" y="33.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="392" x="10.6" y="33.13" z="-17.22"  />
</atom>
<atom>
<atomProperties index="311" x="10.94" y="33.13" z="-27.09"  />
</atom>
<atom>
<atomProperties index="230" x="10.61" y="33.13" z="-37.33"  />
</atom>
<atom>
<atomProperties index="149" x="11.32" y="33.13" z="-47.31"  />
</atom>
<atom>
<atomProperties index="582" x="23.14" y="-28.59" z="12.76"  />
</atom>
<atom>
<atomProperties index="501" x="21.52" y="-26.88" z="3.64"  />
</atom>
<atom>
<atomProperties index="420" x="22.27" y="-26.88" z="-6.31"  />
</atom>
<atom>
<atomProperties index="339" x="21.71" y="-26.88" z="-16.51"  />
</atom>
<atom>
<atomProperties index="258" x="21.74" y="-26.88" z="-26.56"  />
</atom>
<atom>
<atomProperties index="177" x="21.59" y="-26.88" z="-36.74"  />
</atom>
<atom>
<atomProperties index="96" x="21.52" y="-26.88" z="-46.76"  />
</atom>
<atom>
<atomProperties index="591" x="23.41" y="-19.96" z="12.76"  />
</atom>
<atom>
<atomProperties index="510" x="21.12" y="-17.45" z="3.48"  />
</atom>
<atom>
<atomProperties index="429" x="22.43" y="-17.31" z="-6.46"  />
</atom>
<atom>
<atomProperties index="348" x="21.37" y="-16.88" z="-16.54"  />
</atom>
<atom>
<atomProperties index="267" x="21.68" y="-16.88" z="-26.57"  />
</atom>
<atom>
<atomProperties index="186" x="21.71" y="-16.88" z="-36.92"  />
</atom>
<atom>
<atomProperties index="105" x="21.68" y="-16.88" z="-46.91"  />
</atom>
<atom>
<atomProperties index="600" x="23.16" y="-8.58" z="12.74"  />
</atom>
<atom>
<atomProperties index="519" x="21.56" y="-6.88" z="3.48"  />
</atom>
<atom>
<atomProperties index="438" x="21.87" y="-6.23" z="-6.5"  />
</atom>
<atom>
<atomProperties index="357" x="21.08" y="-6.88" z="-16.71"  />
</atom>
<atom>
<atomProperties index="276" x="21.37" y="-6.88" z="-26.56"  />
</atom>
<atom>
<atomProperties index="195" x="21.37" y="-6.88" z="-36.95"  />
</atom>
<atom>
<atomProperties index="114" x="21.34" y="-6.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="609" x="22.45" y="1.88" z="12.43"  />
</atom>
<atom>
<atomProperties index="528" x="21.31" y="3.13" z="3.19"  />
</atom>
<atom>
<atomProperties index="447" x="21.79" y="3.13" z="-6.58"  />
</atom>
<atom>
<atomProperties index="366" x="21.34" y="3.13" z="-16.88"  />
</atom>
<atom>
<atomProperties index="285" x="21.52" y="3.13" z="-26.91"  />
</atom>
<atom>
<atomProperties index="204" x="21.15" y="3.13" z="-37.11"  />
</atom>
<atom>
<atomProperties index="123" x="21.34" y="3.13" z="-46.94"  />
</atom>
<atom>
<atomProperties index="618" x="22.09" y="12.48" z="12.27"  />
</atom>
<atom>
<atomProperties index="537" x="21.31" y="13.13" z="3.19"  />
</atom>
<atom>
<atomProperties index="456" x="22.27" y="13.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="375" x="20.97" y="13.13" z="-16.88"  />
</atom>
<atom>
<atomProperties index="294" x="21.35" y="13.59" z="-26.91"  />
</atom>
<atom>
<atomProperties index="213" x="21.36" y="13.13" z="-37.26"  />
</atom>
<atom>
<atomProperties index="132" x="21.52" y="13.13" z="-47.13"  />
</atom>
<atom>
<atomProperties index="627" x="22.21" y="23.12" z="12.28"  />
</atom>
<atom>
<atomProperties index="546" x="20.94" y="23.93" z="3.11"  />
</atom>
<atom>
<atomProperties index="465" x="21.71" y="23.12" z="-6.87"  />
</atom>
<atom>
<atomProperties index="384" x="20.63" y="23.12" z="-16.92"  />
</atom>
<atom>
<atomProperties index="303" x="21.15" y="23.75" z="-27.09"  />
</atom>
<atom>
<atomProperties index="222" x="21.34" y="23.12" z="-37.29"  />
</atom>
<atom>
<atomProperties index="141" x="21.12" y="23.12" z="-47.27"  />
</atom>
<atom>
<atomProperties index="636" x="21.71" y="33.95" z="12.05"  />
</atom>
<atom>
<atomProperties index="555" x="20.78" y="33.53" z="2.99"  />
</atom>
<atom>
<atomProperties index="474" x="21.71" y="33.13" z="-6.87"  />
</atom>
<atom>
<atomProperties index="393" x="20.56" y="33.13" z="-17.04"  />
</atom>
<atom>
<atomProperties index="312" x="21.33" y="33.62" z="-27.1"  />
</atom>
<atom>
<atomProperties index="231" x="20.97" y="33.13" z="-37.29"  />
</atom>
<atom>
<atomProperties index="150" x="20.97" y="33.13" z="-47.31"  />
</atom>
<atom>
<atomProperties index="583" x="32.47" y="-28.12" z="12.8"  />
</atom>
<atom>
<atomProperties index="502" x="31.77" y="-26.88" z="3.75"  />
</atom>
<atom>
<atomProperties index="421" x="32.22" y="-26.88" z="-6.32"  />
</atom>
<atom>
<atomProperties index="340" x="31.73" y="-26.88" z="-16.51"  />
</atom>
<atom>
<atomProperties index="259" x="32.29" y="-26.88" z="-26.54"  />
</atom>
<atom>
<atomProperties index="178" x="31.7" y="-26.88" z="-36.89"  />
</atom>
<atom>
<atomProperties index="97" x="31.61" y="-26.88" z="-46.76"  />
</atom>
<atom>
<atomProperties index="592" x="32.69" y="-19" z="12.77"  />
</atom>
<atom>
<atomProperties index="511" x="31.36" y="-16.88" z="3.71"  />
</atom>
<atom>
<atomProperties index="430" x="32.59" y="-16.88" z="-6.32"  />
</atom>
<atom>
<atomProperties index="349" x="31.36" y="-16.88" z="-16.51"  />
</atom>
<atom>
<atomProperties index="268" x="32.1" y="-16.88" z="-26.53"  />
</atom>
<atom>
<atomProperties index="187" x="31.73" y="-16.88" z="-36.92"  />
</atom>
<atom>
<atomProperties index="106" x="31.54" y="-16.88" z="-46.83"  />
</atom>
<atom>
<atomProperties index="601" x="32.46" y="-7.7" z="12.8"  />
</atom>
<atom>
<atomProperties index="520" x="31.73" y="-6.88" z="3.53"  />
</atom>
<atom>
<atomProperties index="439" x="32.28" y="-6.32" z="-6.43"  />
</atom>
<atom>
<atomProperties index="358" x="31.73" y="-6.88" z="-16.51"  />
</atom>
<atom>
<atomProperties index="277" x="31.67" y="-6.32" z="-26.72"  />
</atom>
<atom>
<atomProperties index="196" x="31.54" y="-6.88" z="-36.92"  />
</atom>
<atom>
<atomProperties index="115" x="31.36" y="-6.88" z="-46.94"  />
</atom>
<atom>
<atomProperties index="610" x="32.88" y="3.13" z="12.65"  />
</atom>
<atom>
<atomProperties index="529" x="31.32" y="3.57" z="3.49"  />
</atom>
<atom>
<atomProperties index="448" x="32.1" y="3.13" z="-6.49"  />
</atom>
<atom>
<atomProperties index="367" x="31.33" y="3.13" z="-16.85"  />
</atom>
<atom>
<atomProperties index="286" x="31.34" y="3.94" z="-26.87"  />
</atom>
<atom>
<atomProperties index="205" x="31.69" y="3.13" z="-37.07"  />
</atom>
<atom>
<atomProperties index="124" x="30.99" y="3.13" z="-46.94"  />
</atom>
<atom>
<atomProperties index="619" x="32.35" y="12.74" z="12.59"  />
</atom>
<atom>
<atomProperties index="538" x="31.17" y="13.13" z="3.34"  />
</atom>
<atom>
<atomProperties index="457" x="32.13" y="14.37" z="-6.53"  />
</atom>
<atom>
<atomProperties index="376" x="30.99" y="13.13" z="-16.88"  />
</atom>
<atom>
<atomProperties index="295" x="31.7" y="13.65" z="-26.83"  />
</atom>
<atom>
<atomProperties index="214" x="31.36" y="13.13" z="-37.11"  />
</atom>
<atom>
<atomProperties index="133" x="31.24" y="13.13" z="-47.13"  />
</atom>
<atom>
<atomProperties index="628" x="31.88" y="23.12" z="12.47"  />
</atom>
<atom>
<atomProperties index="547" x="30.96" y="23.12" z="3.19"  />
</atom>
<atom>
<atomProperties index="466" x="31.91" y="23.62" z="-6.87"  />
</atom>
<atom>
<atomProperties index="385" x="31.36" y="23.12" z="-16.88"  />
</atom>
<atom>
<atomProperties index="304" x="31.32" y="23.57" z="-26.94"  />
</atom>
<atom>
<atomProperties index="223" x="31.36" y="23.12" z="-37.29"  />
</atom>
<atom>
<atomProperties index="142" x="31.34" y="22.66" z="-47.26"  />
</atom>
<atom>
<atomProperties index="637" x="32.1" y="34.38" z="12.25"  />
</atom>
<atom>
<atomProperties index="556" x="30.8" y="33.13" z="3.15"  />
</atom>
<atom>
<atomProperties index="475" x="31.73" y="34.38" z="-6.87"  />
</atom>
<atom>
<atomProperties index="394" x="30.58" y="33.13" z="-16.92"  />
</atom>
<atom>
<atomProperties index="313" x="31.24" y="33.61" z="-27.17"  />
</atom>
<atom>
<atomProperties index="232" x="31.21" y="33.13" z="-37.34"  />
</atom>
<atom>
<atomProperties index="151" x="31.14" y="33.13" z="-47.36"  />
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Exclusive Components " >
<structuralComponent  name="Regions"  mass="1"  targetPosition="0 0 0"  viscosityW="1" >
<nrOfStructures value="1"/>
<cell>
<cellProperties   type="POLY_VERTEX"  name="cubeVol-Sub8"  materialType="elastic"  shapeW="100" />
<color r="0.8" g="0.8" b="0.2" a="1" />
<nrOfStructures value="729"/>
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="9" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="27" />
<atomRef index="28" />
<atomRef index="29" />
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="73" />
<atomRef index="74" />
<atomRef index="75" />
<atomRef index="76" />
<atomRef index="77" />
<atomRef index="78" />
<atomRef index="79" />
<atomRef index="80" />
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="90" />
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="96" />
<atomRef index="97" />
<atomRef index="98" />
<atomRef index="99" />
<atomRef index="100" />
<atomRef index="101" />
<atomRef index="102" />
<atomRef index="103" />
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="106" />
<atomRef index="107" />
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="113" />
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="116" />
<atomRef index="117" />
<atomRef index="118" />
<atomRef index="119" />
<atomRef index="120" />
<atomRef index="121" />
<atomRef index="122" />
<atomRef index="123" />
<atomRef index="124" />
<atomRef index="125" />
<atomRef index="126" />
<atomRef index="127" />
<atomRef index="128" />
<atomRef index="129" />
<atomRef index="130" />
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="135" />
<atomRef index="136" />
<atomRef index="137" />
<atomRef index="138" />
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="141" />
<atomRef index="142" />
<atomRef index="143" />
<atomRef index="144" />
<atomRef index="145" />
<atomRef index="146" />
<atomRef index="147" />
<atomRef index="148" />
<atomRef index="149" />
<atomRef index="150" />
<atomRef index="151" />
<atomRef index="152" />
<atomRef index="153" />
<atomRef index="154" />
<atomRef index="155" />
<atomRef index="156" />
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="161" />
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="171" />
<atomRef index="172" />
<atomRef index="173" />
<atomRef index="174" />
<atomRef index="175" />
<atomRef index="176" />
<atomRef index="177" />
<atomRef index="178" />
<atomRef index="179" />
<atomRef index="180" />
<atomRef index="181" />
<atomRef index="182" />
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="185" />
<atomRef index="186" />
<atomRef index="187" />
<atomRef index="188" />
<atomRef index="189" />
<atomRef index="190" />
<atomRef index="191" />
<atomRef index="192" />
<atomRef index="193" />
<atomRef index="194" />
<atomRef index="195" />
<atomRef index="196" />
<atomRef index="197" />
<atomRef index="198" />
<atomRef index="199" />
<atomRef index="200" />
<atomRef index="201" />
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="205" />
<atomRef index="206" />
<atomRef index="207" />
<atomRef index="208" />
<atomRef index="209" />
<atomRef index="210" />
<atomRef index="211" />
<atomRef index="212" />
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="215" />
<atomRef index="216" />
<atomRef index="217" />
<atomRef index="218" />
<atomRef index="219" />
<atomRef index="220" />
<atomRef index="221" />
<atomRef index="222" />
<atomRef index="223" />
<atomRef index="224" />
<atomRef index="225" />
<atomRef index="226" />
<atomRef index="227" />
<atomRef index="228" />
<atomRef index="229" />
<atomRef index="230" />
<atomRef index="231" />
<atomRef index="232" />
<atomRef index="233" />
<atomRef index="234" />
<atomRef index="235" />
<atomRef index="236" />
<atomRef index="237" />
<atomRef index="238" />
<atomRef index="239" />
<atomRef index="240" />
<atomRef index="241" />
<atomRef index="242" />
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="251" />
<atomRef index="252" />
<atomRef index="253" />
<atomRef index="254" />
<atomRef index="255" />
<atomRef index="256" />
<atomRef index="257" />
<atomRef index="258" />
<atomRef index="259" />
<atomRef index="260" />
<atomRef index="261" />
<atomRef index="262" />
<atomRef index="263" />
<atomRef index="264" />
<atomRef index="265" />
<atomRef index="266" />
<atomRef index="267" />
<atomRef index="268" />
<atomRef index="269" />
<atomRef index="270" />
<atomRef index="271" />
<atomRef index="272" />
<atomRef index="273" />
<atomRef index="274" />
<atomRef index="275" />
<atomRef index="276" />
<atomRef index="277" />
<atomRef index="278" />
<atomRef index="279" />
<atomRef index="280" />
<atomRef index="281" />
<atomRef index="282" />
<atomRef index="283" />
<atomRef index="284" />
<atomRef index="285" />
<atomRef index="286" />
<atomRef index="287" />
<atomRef index="288" />
<atomRef index="289" />
<atomRef index="290" />
<atomRef index="291" />
<atomRef index="292" />
<atomRef index="293" />
<atomRef index="294" />
<atomRef index="295" />
<atomRef index="296" />
<atomRef index="297" />
<atomRef index="298" />
<atomRef index="299" />
<atomRef index="300" />
<atomRef index="301" />
<atomRef index="302" />
<atomRef index="303" />
<atomRef index="304" />
<atomRef index="305" />
<atomRef index="306" />
<atomRef index="307" />
<atomRef index="308" />
<atomRef index="309" />
<atomRef index="310" />
<atomRef index="311" />
<atomRef index="312" />
<atomRef index="313" />
<atomRef index="314" />
<atomRef index="315" />
<atomRef index="316" />
<atomRef index="317" />
<atomRef index="318" />
<atomRef index="319" />
<atomRef index="320" />
<atomRef index="321" />
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="327" />
<atomRef index="328" />
<atomRef index="329" />
<atomRef index="330" />
<atomRef index="331" />
<atomRef index="332" />
<atomRef index="333" />
<atomRef index="334" />
<atomRef index="335" />
<atomRef index="336" />
<atomRef index="337" />
<atomRef index="338" />
<atomRef index="339" />
<atomRef index="340" />
<atomRef index="341" />
<atomRef index="342" />
<atomRef index="343" />
<atomRef index="344" />
<atomRef index="345" />
<atomRef index="346" />
<atomRef index="347" />
<atomRef index="348" />
<atomRef index="349" />
<atomRef index="350" />
<atomRef index="351" />
<atomRef index="352" />
<atomRef index="353" />
<atomRef index="354" />
<atomRef index="355" />
<atomRef index="356" />
<atomRef index="357" />
<atomRef index="358" />
<atomRef index="359" />
<atomRef index="360" />
<atomRef index="361" />
<atomRef index="362" />
<atomRef index="363" />
<atomRef index="364" />
<atomRef index="365" />
<atomRef index="366" />
<atomRef index="367" />
<atomRef index="368" />
<atomRef index="369" />
<atomRef index="370" />
<atomRef index="371" />
<atomRef index="372" />
<atomRef index="373" />
<atomRef index="374" />
<atomRef index="375" />
<atomRef index="376" />
<atomRef index="377" />
<atomRef index="378" />
<atomRef index="379" />
<atomRef index="380" />
<atomRef index="381" />
<atomRef index="382" />
<atomRef index="383" />
<atomRef index="384" />
<atomRef index="385" />
<atomRef index="386" />
<atomRef index="387" />
<atomRef index="388" />
<atomRef index="389" />
<atomRef index="390" />
<atomRef index="391" />
<atomRef index="392" />
<atomRef index="393" />
<atomRef index="394" />
<atomRef index="395" />
<atomRef index="396" />
<atomRef index="397" />
<atomRef index="398" />
<atomRef index="399" />
<atomRef index="400" />
<atomRef index="401" />
<atomRef index="402" />
<atomRef index="403" />
<atomRef index="404" />
<atomRef index="405" />
<atomRef index="406" />
<atomRef index="407" />
<atomRef index="408" />
<atomRef index="409" />
<atomRef index="410" />
<atomRef index="411" />
<atomRef index="412" />
<atomRef index="413" />
<atomRef index="414" />
<atomRef index="415" />
<atomRef index="416" />
<atomRef index="417" />
<atomRef index="418" />
<atomRef index="419" />
<atomRef index="420" />
<atomRef index="421" />
<atomRef index="422" />
<atomRef index="423" />
<atomRef index="424" />
<atomRef index="425" />
<atomRef index="426" />
<atomRef index="427" />
<atomRef index="428" />
<atomRef index="429" />
<atomRef index="430" />
<atomRef index="431" />
<atomRef index="432" />
<atomRef index="433" />
<atomRef index="434" />
<atomRef index="435" />
<atomRef index="436" />
<atomRef index="437" />
<atomRef index="438" />
<atomRef index="439" />
<atomRef index="440" />
<atomRef index="441" />
<atomRef index="442" />
<atomRef index="443" />
<atomRef index="444" />
<atomRef index="445" />
<atomRef index="446" />
<atomRef index="447" />
<atomRef index="448" />
<atomRef index="449" />
<atomRef index="450" />
<atomRef index="451" />
<atomRef index="452" />
<atomRef index="453" />
<atomRef index="454" />
<atomRef index="455" />
<atomRef index="456" />
<atomRef index="457" />
<atomRef index="458" />
<atomRef index="459" />
<atomRef index="460" />
<atomRef index="461" />
<atomRef index="462" />
<atomRef index="463" />
<atomRef index="464" />
<atomRef index="465" />
<atomRef index="466" />
<atomRef index="467" />
<atomRef index="468" />
<atomRef index="469" />
<atomRef index="470" />
<atomRef index="471" />
<atomRef index="472" />
<atomRef index="473" />
<atomRef index="474" />
<atomRef index="475" />
<atomRef index="476" />
<atomRef index="477" />
<atomRef index="478" />
<atomRef index="479" />
<atomRef index="480" />
<atomRef index="481" />
<atomRef index="482" />
<atomRef index="483" />
<atomRef index="484" />
<atomRef index="485" />
<atomRef index="486" />
<atomRef index="487" />
<atomRef index="488" />
<atomRef index="489" />
<atomRef index="490" />
<atomRef index="491" />
<atomRef index="492" />
<atomRef index="493" />
<atomRef index="494" />
<atomRef index="495" />
<atomRef index="496" />
<atomRef index="497" />
<atomRef index="498" />
<atomRef index="499" />
<atomRef index="500" />
<atomRef index="501" />
<atomRef index="502" />
<atomRef index="503" />
<atomRef index="504" />
<atomRef index="505" />
<atomRef index="506" />
<atomRef index="507" />
<atomRef index="508" />
<atomRef index="509" />
<atomRef index="510" />
<atomRef index="511" />
<atomRef index="512" />
<atomRef index="513" />
<atomRef index="514" />
<atomRef index="515" />
<atomRef index="516" />
<atomRef index="517" />
<atomRef index="518" />
<atomRef index="519" />
<atomRef index="520" />
<atomRef index="521" />
<atomRef index="522" />
<atomRef index="523" />
<atomRef index="524" />
<atomRef index="525" />
<atomRef index="526" />
<atomRef index="527" />
<atomRef index="528" />
<atomRef index="529" />
<atomRef index="530" />
<atomRef index="531" />
<atomRef index="532" />
<atomRef index="533" />
<atomRef index="534" />
<atomRef index="535" />
<atomRef index="536" />
<atomRef index="537" />
<atomRef index="538" />
<atomRef index="539" />
<atomRef index="540" />
<atomRef index="541" />
<atomRef index="542" />
<atomRef index="543" />
<atomRef index="544" />
<atomRef index="545" />
<atomRef index="546" />
<atomRef index="547" />
<atomRef index="548" />
<atomRef index="549" />
<atomRef index="550" />
<atomRef index="551" />
<atomRef index="552" />
<atomRef index="553" />
<atomRef index="554" />
<atomRef index="555" />
<atomRef index="556" />
<atomRef index="557" />
<atomRef index="558" />
<atomRef index="559" />
<atomRef index="560" />
<atomRef index="561" />
<atomRef index="562" />
<atomRef index="563" />
<atomRef index="564" />
<atomRef index="565" />
<atomRef index="566" />
<atomRef index="567" />
<atomRef index="568" />
<atomRef index="569" />
<atomRef index="570" />
<atomRef index="571" />
<atomRef index="572" />
<atomRef index="573" />
<atomRef index="574" />
<atomRef index="575" />
<atomRef index="576" />
<atomRef index="577" />
<atomRef index="578" />
<atomRef index="579" />
<atomRef index="580" />
<atomRef index="581" />
<atomRef index="582" />
<atomRef index="583" />
<atomRef index="584" />
<atomRef index="585" />
<atomRef index="586" />
<atomRef index="587" />
<atomRef index="588" />
<atomRef index="589" />
<atomRef index="590" />
<atomRef index="591" />
<atomRef index="592" />
<atomRef index="593" />
<atomRef index="594" />
<atomRef index="595" />
<atomRef index="596" />
<atomRef index="597" />
<atomRef index="598" />
<atomRef index="599" />
<atomRef index="600" />
<atomRef index="601" />
<atomRef index="602" />
<atomRef index="603" />
<atomRef index="604" />
<atomRef index="605" />
<atomRef index="606" />
<atomRef index="607" />
<atomRef index="608" />
<atomRef index="609" />
<atomRef index="610" />
<atomRef index="611" />
<atomRef index="612" />
<atomRef index="613" />
<atomRef index="614" />
<atomRef index="615" />
<atomRef index="616" />
<atomRef index="617" />
<atomRef index="618" />
<atomRef index="619" />
<atomRef index="620" />
<atomRef index="621" />
<atomRef index="622" />
<atomRef index="623" />
<atomRef index="624" />
<atomRef index="625" />
<atomRef index="626" />
<atomRef index="627" />
<atomRef index="628" />
<atomRef index="629" />
<atomRef index="630" />
<atomRef index="631" />
<atomRef index="632" />
<atomRef index="633" />
<atomRef index="634" />
<atomRef index="635" />
<atomRef index="636" />
<atomRef index="637" />
<atomRef index="638" />
<atomRef index="639" />
<atomRef index="640" />
<atomRef index="641" />
<atomRef index="642" />
<atomRef index="643" />
<atomRef index="644" />
<atomRef index="645" />
<atomRef index="646" />
<atomRef index="647" />
<atomRef index="648" />
<atomRef index="649" />
<atomRef index="650" />
<atomRef index="651" />
<atomRef index="652" />
<atomRef index="653" />
<atomRef index="654" />
<atomRef index="655" />
<atomRef index="656" />
<atomRef index="657" />
<atomRef index="658" />
<atomRef index="659" />
<atomRef index="660" />
<atomRef index="661" />
<atomRef index="662" />
<atomRef index="663" />
<atomRef index="664" />
<atomRef index="665" />
<atomRef index="666" />
<atomRef index="667" />
<atomRef index="668" />
<atomRef index="669" />
<atomRef index="670" />
<atomRef index="671" />
<atomRef index="672" />
<atomRef index="673" />
<atomRef index="674" />
<atomRef index="675" />
<atomRef index="676" />
<atomRef index="677" />
<atomRef index="678" />
<atomRef index="679" />
<atomRef index="680" />
<atomRef index="681" />
<atomRef index="682" />
<atomRef index="683" />
<atomRef index="684" />
<atomRef index="685" />
<atomRef index="686" />
<atomRef index="687" />
<atomRef index="688" />
<atomRef index="689" />
<atomRef index="690" />
<atomRef index="691" />
<atomRef index="692" />
<atomRef index="693" />
<atomRef index="694" />
<atomRef index="695" />
<atomRef index="696" />
<atomRef index="697" />
<atomRef index="698" />
<atomRef index="699" />
<atomRef index="700" />
<atomRef index="701" />
<atomRef index="702" />
<atomRef index="703" />
<atomRef index="704" />
<atomRef index="705" />
<atomRef index="706" />
<atomRef index="707" />
<atomRef index="708" />
<atomRef index="709" />
<atomRef index="710" />
<atomRef index="711" />
<atomRef index="712" />
<atomRef index="713" />
<atomRef index="714" />
<atomRef index="715" />
<atomRef index="716" />
<atomRef index="717" />
<atomRef index="718" />
<atomRef index="719" />
<atomRef index="720" />
<atomRef index="721" />
<atomRef index="722" />
<atomRef index="723" />
<atomRef index="724" />
<atomRef index="725" />
<atomRef index="726" />
<atomRef index="727" />
<atomRef index="728" />
</cell>
</structuralComponent>
<structuralComponent  name="Neighborhoods" >
<color r="0.8" g="0.8" b="0.2" a="1" />
<nrOfStructures value="729"/>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #0" />
<nrOfStructures value="3"/>
<atomRef index="1" />
<atomRef index="9" />
<atomRef index="81" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #1" />
<nrOfStructures value="4"/>
<atomRef index="0" />
<atomRef index="2" />
<atomRef index="10" />
<atomRef index="82" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #2" />
<nrOfStructures value="4"/>
<atomRef index="1" />
<atomRef index="3" />
<atomRef index="11" />
<atomRef index="83" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #3" />
<nrOfStructures value="4"/>
<atomRef index="2" />
<atomRef index="4" />
<atomRef index="12" />
<atomRef index="84" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #4" />
<nrOfStructures value="4"/>
<atomRef index="3" />
<atomRef index="5" />
<atomRef index="13" />
<atomRef index="85" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #5" />
<nrOfStructures value="4"/>
<atomRef index="4" />
<atomRef index="6" />
<atomRef index="14" />
<atomRef index="86" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #6" />
<nrOfStructures value="4"/>
<atomRef index="5" />
<atomRef index="7" />
<atomRef index="15" />
<atomRef index="87" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #7" />
<nrOfStructures value="4"/>
<atomRef index="6" />
<atomRef index="8" />
<atomRef index="16" />
<atomRef index="88" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #8" />
<nrOfStructures value="3"/>
<atomRef index="7" />
<atomRef index="17" />
<atomRef index="89" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #9" />
<nrOfStructures value="4"/>
<atomRef index="0" />
<atomRef index="10" />
<atomRef index="18" />
<atomRef index="90" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #10" />
<nrOfStructures value="5"/>
<atomRef index="1" />
<atomRef index="9" />
<atomRef index="11" />
<atomRef index="19" />
<atomRef index="91" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #11" />
<nrOfStructures value="5"/>
<atomRef index="2" />
<atomRef index="10" />
<atomRef index="12" />
<atomRef index="20" />
<atomRef index="92" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #12" />
<nrOfStructures value="5"/>
<atomRef index="3" />
<atomRef index="11" />
<atomRef index="13" />
<atomRef index="21" />
<atomRef index="93" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #13" />
<nrOfStructures value="5"/>
<atomRef index="4" />
<atomRef index="12" />
<atomRef index="14" />
<atomRef index="22" />
<atomRef index="94" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #14" />
<nrOfStructures value="5"/>
<atomRef index="5" />
<atomRef index="13" />
<atomRef index="15" />
<atomRef index="23" />
<atomRef index="95" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #15" />
<nrOfStructures value="5"/>
<atomRef index="6" />
<atomRef index="14" />
<atomRef index="16" />
<atomRef index="24" />
<atomRef index="96" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #16" />
<nrOfStructures value="5"/>
<atomRef index="7" />
<atomRef index="15" />
<atomRef index="17" />
<atomRef index="25" />
<atomRef index="97" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #17" />
<nrOfStructures value="4"/>
<atomRef index="8" />
<atomRef index="16" />
<atomRef index="26" />
<atomRef index="98" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #18" />
<nrOfStructures value="4"/>
<atomRef index="9" />
<atomRef index="19" />
<atomRef index="27" />
<atomRef index="99" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #19" />
<nrOfStructures value="5"/>
<atomRef index="10" />
<atomRef index="18" />
<atomRef index="20" />
<atomRef index="28" />
<atomRef index="100" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #20" />
<nrOfStructures value="5"/>
<atomRef index="11" />
<atomRef index="19" />
<atomRef index="21" />
<atomRef index="29" />
<atomRef index="101" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #21" />
<nrOfStructures value="5"/>
<atomRef index="12" />
<atomRef index="20" />
<atomRef index="22" />
<atomRef index="30" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #22" />
<nrOfStructures value="5"/>
<atomRef index="13" />
<atomRef index="21" />
<atomRef index="23" />
<atomRef index="31" />
<atomRef index="103" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #23" />
<nrOfStructures value="5"/>
<atomRef index="14" />
<atomRef index="22" />
<atomRef index="24" />
<atomRef index="32" />
<atomRef index="104" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #24" />
<nrOfStructures value="5"/>
<atomRef index="15" />
<atomRef index="23" />
<atomRef index="25" />
<atomRef index="33" />
<atomRef index="105" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #25" />
<nrOfStructures value="5"/>
<atomRef index="16" />
<atomRef index="24" />
<atomRef index="26" />
<atomRef index="34" />
<atomRef index="106" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #26" />
<nrOfStructures value="4"/>
<atomRef index="17" />
<atomRef index="25" />
<atomRef index="35" />
<atomRef index="107" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #27" />
<nrOfStructures value="4"/>
<atomRef index="18" />
<atomRef index="28" />
<atomRef index="36" />
<atomRef index="108" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #28" />
<nrOfStructures value="5"/>
<atomRef index="19" />
<atomRef index="27" />
<atomRef index="29" />
<atomRef index="37" />
<atomRef index="109" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #29" />
<nrOfStructures value="5"/>
<atomRef index="20" />
<atomRef index="28" />
<atomRef index="30" />
<atomRef index="38" />
<atomRef index="110" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #30" />
<nrOfStructures value="5"/>
<atomRef index="21" />
<atomRef index="29" />
<atomRef index="31" />
<atomRef index="39" />
<atomRef index="111" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #31" />
<nrOfStructures value="5"/>
<atomRef index="22" />
<atomRef index="30" />
<atomRef index="32" />
<atomRef index="40" />
<atomRef index="112" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #32" />
<nrOfStructures value="5"/>
<atomRef index="23" />
<atomRef index="31" />
<atomRef index="33" />
<atomRef index="41" />
<atomRef index="113" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #33" />
<nrOfStructures value="5"/>
<atomRef index="24" />
<atomRef index="32" />
<atomRef index="34" />
<atomRef index="42" />
<atomRef index="114" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #34" />
<nrOfStructures value="5"/>
<atomRef index="25" />
<atomRef index="33" />
<atomRef index="35" />
<atomRef index="43" />
<atomRef index="115" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #35" />
<nrOfStructures value="4"/>
<atomRef index="26" />
<atomRef index="34" />
<atomRef index="44" />
<atomRef index="116" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #36" />
<nrOfStructures value="4"/>
<atomRef index="27" />
<atomRef index="37" />
<atomRef index="45" />
<atomRef index="117" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #37" />
<nrOfStructures value="5"/>
<atomRef index="28" />
<atomRef index="36" />
<atomRef index="38" />
<atomRef index="46" />
<atomRef index="118" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #38" />
<nrOfStructures value="5"/>
<atomRef index="29" />
<atomRef index="37" />
<atomRef index="39" />
<atomRef index="47" />
<atomRef index="119" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #39" />
<nrOfStructures value="5"/>
<atomRef index="30" />
<atomRef index="38" />
<atomRef index="40" />
<atomRef index="48" />
<atomRef index="120" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #40" />
<nrOfStructures value="5"/>
<atomRef index="31" />
<atomRef index="39" />
<atomRef index="41" />
<atomRef index="49" />
<atomRef index="121" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #41" />
<nrOfStructures value="5"/>
<atomRef index="32" />
<atomRef index="40" />
<atomRef index="42" />
<atomRef index="50" />
<atomRef index="122" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #42" />
<nrOfStructures value="5"/>
<atomRef index="33" />
<atomRef index="41" />
<atomRef index="43" />
<atomRef index="51" />
<atomRef index="123" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #43" />
<nrOfStructures value="5"/>
<atomRef index="34" />
<atomRef index="42" />
<atomRef index="44" />
<atomRef index="52" />
<atomRef index="124" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #44" />
<nrOfStructures value="4"/>
<atomRef index="35" />
<atomRef index="43" />
<atomRef index="53" />
<atomRef index="125" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #45" />
<nrOfStructures value="4"/>
<atomRef index="36" />
<atomRef index="46" />
<atomRef index="54" />
<atomRef index="126" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #46" />
<nrOfStructures value="5"/>
<atomRef index="37" />
<atomRef index="45" />
<atomRef index="47" />
<atomRef index="55" />
<atomRef index="127" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #47" />
<nrOfStructures value="5"/>
<atomRef index="38" />
<atomRef index="46" />
<atomRef index="48" />
<atomRef index="56" />
<atomRef index="128" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #48" />
<nrOfStructures value="5"/>
<atomRef index="39" />
<atomRef index="47" />
<atomRef index="49" />
<atomRef index="57" />
<atomRef index="129" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #49" />
<nrOfStructures value="5"/>
<atomRef index="40" />
<atomRef index="48" />
<atomRef index="50" />
<atomRef index="58" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #50" />
<nrOfStructures value="5"/>
<atomRef index="41" />
<atomRef index="49" />
<atomRef index="51" />
<atomRef index="59" />
<atomRef index="131" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #51" />
<nrOfStructures value="5"/>
<atomRef index="42" />
<atomRef index="50" />
<atomRef index="52" />
<atomRef index="60" />
<atomRef index="132" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #52" />
<nrOfStructures value="5"/>
<atomRef index="43" />
<atomRef index="51" />
<atomRef index="53" />
<atomRef index="61" />
<atomRef index="133" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #53" />
<nrOfStructures value="4"/>
<atomRef index="44" />
<atomRef index="52" />
<atomRef index="62" />
<atomRef index="134" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #54" />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="55" />
<atomRef index="63" />
<atomRef index="135" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #55" />
<nrOfStructures value="5"/>
<atomRef index="46" />
<atomRef index="54" />
<atomRef index="56" />
<atomRef index="64" />
<atomRef index="136" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #56" />
<nrOfStructures value="5"/>
<atomRef index="47" />
<atomRef index="55" />
<atomRef index="57" />
<atomRef index="65" />
<atomRef index="137" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #57" />
<nrOfStructures value="5"/>
<atomRef index="48" />
<atomRef index="56" />
<atomRef index="58" />
<atomRef index="66" />
<atomRef index="138" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #58" />
<nrOfStructures value="5"/>
<atomRef index="49" />
<atomRef index="57" />
<atomRef index="59" />
<atomRef index="67" />
<atomRef index="139" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #59" />
<nrOfStructures value="5"/>
<atomRef index="50" />
<atomRef index="58" />
<atomRef index="60" />
<atomRef index="68" />
<atomRef index="140" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #60" />
<nrOfStructures value="5"/>
<atomRef index="51" />
<atomRef index="59" />
<atomRef index="61" />
<atomRef index="69" />
<atomRef index="141" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #61" />
<nrOfStructures value="5"/>
<atomRef index="52" />
<atomRef index="60" />
<atomRef index="62" />
<atomRef index="70" />
<atomRef index="142" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #62" />
<nrOfStructures value="4"/>
<atomRef index="53" />
<atomRef index="61" />
<atomRef index="71" />
<atomRef index="143" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #63" />
<nrOfStructures value="4"/>
<atomRef index="54" />
<atomRef index="64" />
<atomRef index="72" />
<atomRef index="144" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #64" />
<nrOfStructures value="5"/>
<atomRef index="55" />
<atomRef index="63" />
<atomRef index="65" />
<atomRef index="73" />
<atomRef index="145" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #65" />
<nrOfStructures value="5"/>
<atomRef index="56" />
<atomRef index="64" />
<atomRef index="66" />
<atomRef index="74" />
<atomRef index="146" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #66" />
<nrOfStructures value="5"/>
<atomRef index="57" />
<atomRef index="65" />
<atomRef index="67" />
<atomRef index="75" />
<atomRef index="147" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #67" />
<nrOfStructures value="5"/>
<atomRef index="58" />
<atomRef index="66" />
<atomRef index="68" />
<atomRef index="76" />
<atomRef index="148" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #68" />
<nrOfStructures value="5"/>
<atomRef index="59" />
<atomRef index="67" />
<atomRef index="69" />
<atomRef index="77" />
<atomRef index="149" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #69" />
<nrOfStructures value="5"/>
<atomRef index="60" />
<atomRef index="68" />
<atomRef index="70" />
<atomRef index="78" />
<atomRef index="150" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #70" />
<nrOfStructures value="5"/>
<atomRef index="61" />
<atomRef index="69" />
<atomRef index="71" />
<atomRef index="79" />
<atomRef index="151" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #71" />
<nrOfStructures value="4"/>
<atomRef index="62" />
<atomRef index="70" />
<atomRef index="80" />
<atomRef index="152" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #72" />
<nrOfStructures value="3"/>
<atomRef index="63" />
<atomRef index="73" />
<atomRef index="153" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #73" />
<nrOfStructures value="4"/>
<atomRef index="64" />
<atomRef index="72" />
<atomRef index="74" />
<atomRef index="154" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #74" />
<nrOfStructures value="4"/>
<atomRef index="65" />
<atomRef index="73" />
<atomRef index="75" />
<atomRef index="155" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #75" />
<nrOfStructures value="4"/>
<atomRef index="66" />
<atomRef index="74" />
<atomRef index="76" />
<atomRef index="156" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #76" />
<nrOfStructures value="4"/>
<atomRef index="67" />
<atomRef index="75" />
<atomRef index="77" />
<atomRef index="157" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #77" />
<nrOfStructures value="4"/>
<atomRef index="68" />
<atomRef index="76" />
<atomRef index="78" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #78" />
<nrOfStructures value="4"/>
<atomRef index="69" />
<atomRef index="77" />
<atomRef index="79" />
<atomRef index="159" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #79" />
<nrOfStructures value="4"/>
<atomRef index="70" />
<atomRef index="78" />
<atomRef index="80" />
<atomRef index="160" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #80" />
<nrOfStructures value="3"/>
<atomRef index="71" />
<atomRef index="79" />
<atomRef index="161" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #81" />
<nrOfStructures value="4"/>
<atomRef index="0" />
<atomRef index="82" />
<atomRef index="90" />
<atomRef index="162" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #82" />
<nrOfStructures value="5"/>
<atomRef index="1" />
<atomRef index="81" />
<atomRef index="83" />
<atomRef index="91" />
<atomRef index="163" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #83" />
<nrOfStructures value="5"/>
<atomRef index="2" />
<atomRef index="82" />
<atomRef index="84" />
<atomRef index="92" />
<atomRef index="164" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #84" />
<nrOfStructures value="5"/>
<atomRef index="3" />
<atomRef index="83" />
<atomRef index="85" />
<atomRef index="93" />
<atomRef index="165" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #85" />
<nrOfStructures value="5"/>
<atomRef index="4" />
<atomRef index="84" />
<atomRef index="86" />
<atomRef index="94" />
<atomRef index="166" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #86" />
<nrOfStructures value="5"/>
<atomRef index="5" />
<atomRef index="85" />
<atomRef index="87" />
<atomRef index="95" />
<atomRef index="167" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #87" />
<nrOfStructures value="5"/>
<atomRef index="6" />
<atomRef index="86" />
<atomRef index="88" />
<atomRef index="96" />
<atomRef index="168" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #88" />
<nrOfStructures value="5"/>
<atomRef index="7" />
<atomRef index="87" />
<atomRef index="89" />
<atomRef index="97" />
<atomRef index="169" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #89" />
<nrOfStructures value="4"/>
<atomRef index="8" />
<atomRef index="88" />
<atomRef index="98" />
<atomRef index="170" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #90" />
<nrOfStructures value="5"/>
<atomRef index="9" />
<atomRef index="81" />
<atomRef index="91" />
<atomRef index="99" />
<atomRef index="171" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #91" />
<nrOfStructures value="6"/>
<atomRef index="10" />
<atomRef index="92" />
<atomRef index="90" />
<atomRef index="100" />
<atomRef index="82" />
<atomRef index="172" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #92" />
<nrOfStructures value="6"/>
<atomRef index="11" />
<atomRef index="173" />
<atomRef index="91" />
<atomRef index="93" />
<atomRef index="101" />
<atomRef index="83" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #93" />
<nrOfStructures value="6"/>
<atomRef index="102" />
<atomRef index="84" />
<atomRef index="92" />
<atomRef index="94" />
<atomRef index="12" />
<atomRef index="174" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #94" />
<nrOfStructures value="6"/>
<atomRef index="13" />
<atomRef index="95" />
<atomRef index="93" />
<atomRef index="85" />
<atomRef index="103" />
<atomRef index="175" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #95" />
<nrOfStructures value="6"/>
<atomRef index="14" />
<atomRef index="94" />
<atomRef index="86" />
<atomRef index="96" />
<atomRef index="104" />
<atomRef index="176" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #96" />
<nrOfStructures value="6"/>
<atomRef index="15" />
<atomRef index="95" />
<atomRef index="87" />
<atomRef index="97" />
<atomRef index="105" />
<atomRef index="177" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #97" />
<nrOfStructures value="6"/>
<atomRef index="16" />
<atomRef index="88" />
<atomRef index="96" />
<atomRef index="98" />
<atomRef index="178" />
<atomRef index="106" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #98" />
<nrOfStructures value="5"/>
<atomRef index="17" />
<atomRef index="89" />
<atomRef index="97" />
<atomRef index="107" />
<atomRef index="179" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #99" />
<nrOfStructures value="5"/>
<atomRef index="18" />
<atomRef index="90" />
<atomRef index="100" />
<atomRef index="108" />
<atomRef index="180" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #100" />
<nrOfStructures value="6"/>
<atomRef index="109" />
<atomRef index="91" />
<atomRef index="99" />
<atomRef index="101" />
<atomRef index="19" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #101" />
<nrOfStructures value="6"/>
<atomRef index="20" />
<atomRef index="92" />
<atomRef index="100" />
<atomRef index="102" />
<atomRef index="182" />
<atomRef index="110" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #102" />
<nrOfStructures value="6"/>
<atomRef index="21" />
<atomRef index="101" />
<atomRef index="93" />
<atomRef index="103" />
<atomRef index="111" />
<atomRef index="183" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #103" />
<nrOfStructures value="6"/>
<atomRef index="22" />
<atomRef index="184" />
<atomRef index="102" />
<atomRef index="104" />
<atomRef index="112" />
<atomRef index="94" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #104" />
<nrOfStructures value="6"/>
<atomRef index="185" />
<atomRef index="105" />
<atomRef index="103" />
<atomRef index="95" />
<atomRef index="113" />
<atomRef index="23" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #105" />
<nrOfStructures value="6"/>
<atomRef index="24" />
<atomRef index="114" />
<atomRef index="104" />
<atomRef index="96" />
<atomRef index="106" />
<atomRef index="186" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #106" />
<nrOfStructures value="6"/>
<atomRef index="25" />
<atomRef index="97" />
<atomRef index="105" />
<atomRef index="187" />
<atomRef index="115" />
<atomRef index="107" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #107" />
<nrOfStructures value="5"/>
<atomRef index="26" />
<atomRef index="98" />
<atomRef index="106" />
<atomRef index="116" />
<atomRef index="188" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #108" />
<nrOfStructures value="5"/>
<atomRef index="27" />
<atomRef index="99" />
<atomRef index="109" />
<atomRef index="117" />
<atomRef index="189" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #109" />
<nrOfStructures value="6"/>
<atomRef index="28" />
<atomRef index="108" />
<atomRef index="100" />
<atomRef index="110" />
<atomRef index="118" />
<atomRef index="190" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #110" />
<nrOfStructures value="6"/>
<atomRef index="29" />
<atomRef index="101" />
<atomRef index="191" />
<atomRef index="111" />
<atomRef index="119" />
<atomRef index="109" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #111" />
<nrOfStructures value="6"/>
<atomRef index="192" />
<atomRef index="120" />
<atomRef index="102" />
<atomRef index="110" />
<atomRef index="112" />
<atomRef index="30" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #112" />
<nrOfStructures value="6"/>
<atomRef index="31" />
<atomRef index="113" />
<atomRef index="111" />
<atomRef index="103" />
<atomRef index="121" />
<atomRef index="193" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #113" />
<nrOfStructures value="6"/>
<atomRef index="32" />
<atomRef index="104" />
<atomRef index="112" />
<atomRef index="114" />
<atomRef index="194" />
<atomRef index="122" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #114" />
<nrOfStructures value="6"/>
<atomRef index="33" />
<atomRef index="115" />
<atomRef index="113" />
<atomRef index="105" />
<atomRef index="123" />
<atomRef index="195" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #115" />
<nrOfStructures value="6"/>
<atomRef index="116" />
<atomRef index="106" />
<atomRef index="114" />
<atomRef index="196" />
<atomRef index="124" />
<atomRef index="34" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #116" />
<nrOfStructures value="5"/>
<atomRef index="35" />
<atomRef index="107" />
<atomRef index="115" />
<atomRef index="125" />
<atomRef index="197" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #117" />
<nrOfStructures value="5"/>
<atomRef index="36" />
<atomRef index="108" />
<atomRef index="118" />
<atomRef index="126" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #118" />
<nrOfStructures value="6"/>
<atomRef index="37" />
<atomRef index="119" />
<atomRef index="109" />
<atomRef index="117" />
<atomRef index="127" />
<atomRef index="199" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #119" />
<nrOfStructures value="6"/>
<atomRef index="118" />
<atomRef index="110" />
<atomRef index="38" />
<atomRef index="120" />
<atomRef index="128" />
<atomRef index="200" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #120" />
<nrOfStructures value="6"/>
<atomRef index="119" />
<atomRef index="111" />
<atomRef index="39" />
<atomRef index="121" />
<atomRef index="129" />
<atomRef index="201" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #121" />
<nrOfStructures value="6"/>
<atomRef index="40" />
<atomRef index="112" />
<atomRef index="120" />
<atomRef index="202" />
<atomRef index="130" />
<atomRef index="122" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #122" />
<nrOfStructures value="6"/>
<atomRef index="41" />
<atomRef index="121" />
<atomRef index="113" />
<atomRef index="123" />
<atomRef index="131" />
<atomRef index="203" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #123" />
<nrOfStructures value="6"/>
<atomRef index="122" />
<atomRef index="114" />
<atomRef index="42" />
<atomRef index="124" />
<atomRef index="132" />
<atomRef index="204" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #124" />
<nrOfStructures value="6"/>
<atomRef index="115" />
<atomRef index="43" />
<atomRef index="123" />
<atomRef index="125" />
<atomRef index="133" />
<atomRef index="205" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #125" />
<nrOfStructures value="5"/>
<atomRef index="44" />
<atomRef index="116" />
<atomRef index="124" />
<atomRef index="134" />
<atomRef index="206" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #126" />
<nrOfStructures value="5"/>
<atomRef index="45" />
<atomRef index="117" />
<atomRef index="127" />
<atomRef index="135" />
<atomRef index="207" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #127" />
<nrOfStructures value="6"/>
<atomRef index="136" />
<atomRef index="118" />
<atomRef index="126" />
<atomRef index="128" />
<atomRef index="46" />
<atomRef index="208" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #128" />
<nrOfStructures value="6"/>
<atomRef index="209" />
<atomRef index="119" />
<atomRef index="127" />
<atomRef index="137" />
<atomRef index="129" />
<atomRef index="47" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #129" />
<nrOfStructures value="6"/>
<atomRef index="48" />
<atomRef index="120" />
<atomRef index="138" />
<atomRef index="130" />
<atomRef index="128" />
<atomRef index="210" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #130" />
<nrOfStructures value="6"/>
<atomRef index="49" />
<atomRef index="121" />
<atomRef index="129" />
<atomRef index="211" />
<atomRef index="139" />
<atomRef index="131" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #131" />
<nrOfStructures value="6"/>
<atomRef index="50" />
<atomRef index="122" />
<atomRef index="212" />
<atomRef index="132" />
<atomRef index="140" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #132" />
<nrOfStructures value="6"/>
<atomRef index="133" />
<atomRef index="123" />
<atomRef index="131" />
<atomRef index="51" />
<atomRef index="141" />
<atomRef index="213" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #133" />
<nrOfStructures value="6"/>
<atomRef index="52" />
<atomRef index="124" />
<atomRef index="142" />
<atomRef index="134" />
<atomRef index="132" />
<atomRef index="214" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #134" />
<nrOfStructures value="5"/>
<atomRef index="53" />
<atomRef index="125" />
<atomRef index="133" />
<atomRef index="143" />
<atomRef index="215" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #135" />
<nrOfStructures value="5"/>
<atomRef index="54" />
<atomRef index="126" />
<atomRef index="136" />
<atomRef index="144" />
<atomRef index="216" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #136" />
<nrOfStructures value="6"/>
<atomRef index="55" />
<atomRef index="135" />
<atomRef index="127" />
<atomRef index="137" />
<atomRef index="145" />
<atomRef index="217" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #137" />
<nrOfStructures value="6"/>
<atomRef index="56" />
<atomRef index="136" />
<atomRef index="128" />
<atomRef index="138" />
<atomRef index="146" />
<atomRef index="218" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #138" />
<nrOfStructures value="6"/>
<atomRef index="57" />
<atomRef index="129" />
<atomRef index="137" />
<atomRef index="219" />
<atomRef index="147" />
<atomRef index="139" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #139" />
<nrOfStructures value="6"/>
<atomRef index="58" />
<atomRef index="130" />
<atomRef index="138" />
<atomRef index="140" />
<atomRef index="220" />
<atomRef index="148" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #140" />
<nrOfStructures value="6"/>
<atomRef index="59" />
<atomRef index="131" />
<atomRef index="139" />
<atomRef index="149" />
<atomRef index="141" />
<atomRef index="221" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #141" />
<nrOfStructures value="6"/>
<atomRef index="60" />
<atomRef index="142" />
<atomRef index="140" />
<atomRef index="132" />
<atomRef index="150" />
<atomRef index="222" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #142" />
<nrOfStructures value="6"/>
<atomRef index="61" />
<atomRef index="133" />
<atomRef index="141" />
<atomRef index="223" />
<atomRef index="151" />
<atomRef index="143" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #143" />
<nrOfStructures value="5"/>
<atomRef index="62" />
<atomRef index="134" />
<atomRef index="142" />
<atomRef index="152" />
<atomRef index="224" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #144" />
<nrOfStructures value="5"/>
<atomRef index="63" />
<atomRef index="135" />
<atomRef index="145" />
<atomRef index="153" />
<atomRef index="225" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #145" />
<nrOfStructures value="6"/>
<atomRef index="64" />
<atomRef index="136" />
<atomRef index="144" />
<atomRef index="226" />
<atomRef index="154" />
<atomRef index="146" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #146" />
<nrOfStructures value="6"/>
<atomRef index="65" />
<atomRef index="147" />
<atomRef index="145" />
<atomRef index="137" />
<atomRef index="155" />
<atomRef index="227" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #147" />
<nrOfStructures value="6"/>
<atomRef index="66" />
<atomRef index="138" />
<atomRef index="146" />
<atomRef index="156" />
<atomRef index="148" />
<atomRef index="228" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #148" />
<nrOfStructures value="6"/>
<atomRef index="67" />
<atomRef index="139" />
<atomRef index="147" />
<atomRef index="229" />
<atomRef index="157" />
<atomRef index="149" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #149" />
<nrOfStructures value="6"/>
<atomRef index="230" />
<atomRef index="140" />
<atomRef index="148" />
<atomRef index="150" />
<atomRef index="68" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #150" />
<nrOfStructures value="6"/>
<atomRef index="69" />
<atomRef index="141" />
<atomRef index="231" />
<atomRef index="151" />
<atomRef index="159" />
<atomRef index="149" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #151" />
<nrOfStructures value="6"/>
<atomRef index="70" />
<atomRef index="142" />
<atomRef index="150" />
<atomRef index="160" />
<atomRef index="152" />
<atomRef index="232" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #152" />
<nrOfStructures value="5"/>
<atomRef index="71" />
<atomRef index="143" />
<atomRef index="151" />
<atomRef index="161" />
<atomRef index="233" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #153" />
<nrOfStructures value="4"/>
<atomRef index="72" />
<atomRef index="144" />
<atomRef index="154" />
<atomRef index="234" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #154" />
<nrOfStructures value="5"/>
<atomRef index="73" />
<atomRef index="145" />
<atomRef index="153" />
<atomRef index="155" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #155" />
<nrOfStructures value="5"/>
<atomRef index="74" />
<atomRef index="146" />
<atomRef index="154" />
<atomRef index="156" />
<atomRef index="236" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #156" />
<nrOfStructures value="5"/>
<atomRef index="75" />
<atomRef index="147" />
<atomRef index="155" />
<atomRef index="157" />
<atomRef index="237" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #157" />
<nrOfStructures value="5"/>
<atomRef index="76" />
<atomRef index="148" />
<atomRef index="156" />
<atomRef index="158" />
<atomRef index="238" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #158" />
<nrOfStructures value="5"/>
<atomRef index="77" />
<atomRef index="149" />
<atomRef index="157" />
<atomRef index="159" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #159" />
<nrOfStructures value="5"/>
<atomRef index="78" />
<atomRef index="150" />
<atomRef index="158" />
<atomRef index="160" />
<atomRef index="240" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #160" />
<nrOfStructures value="5"/>
<atomRef index="79" />
<atomRef index="151" />
<atomRef index="159" />
<atomRef index="161" />
<atomRef index="241" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #161" />
<nrOfStructures value="4"/>
<atomRef index="80" />
<atomRef index="152" />
<atomRef index="160" />
<atomRef index="242" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #162" />
<nrOfStructures value="4"/>
<atomRef index="81" />
<atomRef index="163" />
<atomRef index="171" />
<atomRef index="243" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #163" />
<nrOfStructures value="5"/>
<atomRef index="82" />
<atomRef index="162" />
<atomRef index="164" />
<atomRef index="172" />
<atomRef index="244" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #164" />
<nrOfStructures value="5"/>
<atomRef index="83" />
<atomRef index="163" />
<atomRef index="165" />
<atomRef index="173" />
<atomRef index="245" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #165" />
<nrOfStructures value="5"/>
<atomRef index="84" />
<atomRef index="164" />
<atomRef index="166" />
<atomRef index="174" />
<atomRef index="246" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #166" />
<nrOfStructures value="5"/>
<atomRef index="85" />
<atomRef index="165" />
<atomRef index="167" />
<atomRef index="175" />
<atomRef index="247" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #167" />
<nrOfStructures value="5"/>
<atomRef index="86" />
<atomRef index="166" />
<atomRef index="168" />
<atomRef index="176" />
<atomRef index="248" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #168" />
<nrOfStructures value="5"/>
<atomRef index="87" />
<atomRef index="167" />
<atomRef index="169" />
<atomRef index="177" />
<atomRef index="249" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #169" />
<nrOfStructures value="5"/>
<atomRef index="88" />
<atomRef index="168" />
<atomRef index="170" />
<atomRef index="178" />
<atomRef index="250" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #170" />
<nrOfStructures value="4"/>
<atomRef index="89" />
<atomRef index="169" />
<atomRef index="179" />
<atomRef index="251" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #171" />
<nrOfStructures value="5"/>
<atomRef index="90" />
<atomRef index="162" />
<atomRef index="172" />
<atomRef index="180" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #172" />
<nrOfStructures value="6"/>
<atomRef index="91" />
<atomRef index="163" />
<atomRef index="253" />
<atomRef index="173" />
<atomRef index="181" />
<atomRef index="171" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #173" />
<nrOfStructures value="6"/>
<atomRef index="92" />
<atomRef index="164" />
<atomRef index="172" />
<atomRef index="254" />
<atomRef index="182" />
<atomRef index="174" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #174" />
<nrOfStructures value="6"/>
<atomRef index="183" />
<atomRef index="165" />
<atomRef index="173" />
<atomRef index="175" />
<atomRef index="93" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #175" />
<nrOfStructures value="6"/>
<atomRef index="94" />
<atomRef index="166" />
<atomRef index="174" />
<atomRef index="176" />
<atomRef index="256" />
<atomRef index="184" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #176" />
<nrOfStructures value="6"/>
<atomRef index="177" />
<atomRef index="167" />
<atomRef index="175" />
<atomRef index="95" />
<atomRef index="185" />
<atomRef index="257" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #177" />
<nrOfStructures value="6"/>
<atomRef index="96" />
<atomRef index="168" />
<atomRef index="258" />
<atomRef index="178" />
<atomRef index="186" />
<atomRef index="176" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #178" />
<nrOfStructures value="6"/>
<atomRef index="97" />
<atomRef index="177" />
<atomRef index="169" />
<atomRef index="179" />
<atomRef index="187" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #179" />
<nrOfStructures value="5"/>
<atomRef index="98" />
<atomRef index="170" />
<atomRef index="178" />
<atomRef index="188" />
<atomRef index="260" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #180" />
<nrOfStructures value="5"/>
<atomRef index="99" />
<atomRef index="171" />
<atomRef index="181" />
<atomRef index="189" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #181" />
<nrOfStructures value="6"/>
<atomRef index="100" />
<atomRef index="182" />
<atomRef index="180" />
<atomRef index="172" />
<atomRef index="190" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #182" />
<nrOfStructures value="6"/>
<atomRef index="101" />
<atomRef index="173" />
<atomRef index="181" />
<atomRef index="183" />
<atomRef index="263" />
<atomRef index="191" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #183" />
<nrOfStructures value="6"/>
<atomRef index="102" />
<atomRef index="182" />
<atomRef index="174" />
<atomRef index="184" />
<atomRef index="192" />
<atomRef index="264" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #184" />
<nrOfStructures value="6"/>
<atomRef index="103" />
<atomRef index="183" />
<atomRef index="175" />
<atomRef index="185" />
<atomRef index="193" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #185" />
<nrOfStructures value="6"/>
<atomRef index="104" />
<atomRef index="176" />
<atomRef index="194" />
<atomRef index="186" />
<atomRef index="184" />
<atomRef index="266" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #186" />
<nrOfStructures value="6"/>
<atomRef index="195" />
<atomRef index="177" />
<atomRef index="185" />
<atomRef index="187" />
<atomRef index="105" />
<atomRef index="267" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #187" />
<nrOfStructures value="6"/>
<atomRef index="106" />
<atomRef index="178" />
<atomRef index="186" />
<atomRef index="188" />
<atomRef index="268" />
<atomRef index="196" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #188" />
<nrOfStructures value="5"/>
<atomRef index="107" />
<atomRef index="179" />
<atomRef index="187" />
<atomRef index="197" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #189" />
<nrOfStructures value="5"/>
<atomRef index="108" />
<atomRef index="180" />
<atomRef index="190" />
<atomRef index="198" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #190" />
<nrOfStructures value="6"/>
<atomRef index="109" />
<atomRef index="181" />
<atomRef index="191" />
<atomRef index="271" />
<atomRef index="199" />
<atomRef index="189" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #191" />
<nrOfStructures value="6"/>
<atomRef index="110" />
<atomRef index="192" />
<atomRef index="190" />
<atomRef index="182" />
<atomRef index="200" />
<atomRef index="272" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #192" />
<nrOfStructures value="6"/>
<atomRef index="111" />
<atomRef index="183" />
<atomRef index="191" />
<atomRef index="193" />
<atomRef index="273" />
<atomRef index="201" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #193" />
<nrOfStructures value="6"/>
<atomRef index="112" />
<atomRef index="184" />
<atomRef index="192" />
<atomRef index="194" />
<atomRef index="274" />
<atomRef index="202" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #194" />
<nrOfStructures value="6"/>
<atomRef index="275" />
<atomRef index="193" />
<atomRef index="185" />
<atomRef index="195" />
<atomRef index="203" />
<atomRef index="113" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #195" />
<nrOfStructures value="6"/>
<atomRef index="114" />
<atomRef index="276" />
<atomRef index="194" />
<atomRef index="196" />
<atomRef index="204" />
<atomRef index="186" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #196" />
<nrOfStructures value="6"/>
<atomRef index="115" />
<atomRef index="197" />
<atomRef index="195" />
<atomRef index="187" />
<atomRef index="205" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #197" />
<nrOfStructures value="5"/>
<atomRef index="116" />
<atomRef index="188" />
<atomRef index="196" />
<atomRef index="206" />
<atomRef index="278" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #198" />
<nrOfStructures value="5"/>
<atomRef index="117" />
<atomRef index="189" />
<atomRef index="199" />
<atomRef index="207" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #199" />
<nrOfStructures value="6"/>
<atomRef index="118" />
<atomRef index="198" />
<atomRef index="190" />
<atomRef index="200" />
<atomRef index="208" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #200" />
<nrOfStructures value="6"/>
<atomRef index="199" />
<atomRef index="191" />
<atomRef index="201" />
<atomRef index="119" />
<atomRef index="209" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #201" />
<nrOfStructures value="6"/>
<atomRef index="120" />
<atomRef index="282" />
<atomRef index="202" />
<atomRef index="200" />
<atomRef index="192" />
<atomRef index="210" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #202" />
<nrOfStructures value="6"/>
<atomRef index="121" />
<atomRef index="193" />
<atomRef index="211" />
<atomRef index="203" />
<atomRef index="201" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #203" />
<nrOfStructures value="6"/>
<atomRef index="212" />
<atomRef index="194" />
<atomRef index="202" />
<atomRef index="204" />
<atomRef index="122" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #204" />
<nrOfStructures value="6"/>
<atomRef index="205" />
<atomRef index="195" />
<atomRef index="203" />
<atomRef index="123" />
<atomRef index="213" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #205" />
<nrOfStructures value="6"/>
<atomRef index="124" />
<atomRef index="196" />
<atomRef index="204" />
<atomRef index="286" />
<atomRef index="214" />
<atomRef index="206" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #206" />
<nrOfStructures value="5"/>
<atomRef index="125" />
<atomRef index="197" />
<atomRef index="205" />
<atomRef index="215" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #207" />
<nrOfStructures value="5"/>
<atomRef index="126" />
<atomRef index="198" />
<atomRef index="208" />
<atomRef index="216" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #208" />
<nrOfStructures value="6"/>
<atomRef index="207" />
<atomRef index="199" />
<atomRef index="127" />
<atomRef index="209" />
<atomRef index="217" />
<atomRef index="289" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #209" />
<nrOfStructures value="6"/>
<atomRef index="210" />
<atomRef index="200" />
<atomRef index="208" />
<atomRef index="128" />
<atomRef index="218" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #210" />
<nrOfStructures value="6"/>
<atomRef index="129" />
<atomRef index="201" />
<atomRef index="291" />
<atomRef index="211" />
<atomRef index="219" />
<atomRef index="209" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #211" />
<nrOfStructures value="6"/>
<atomRef index="130" />
<atomRef index="202" />
<atomRef index="210" />
<atomRef index="212" />
<atomRef index="292" />
<atomRef index="220" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #212" />
<nrOfStructures value="6"/>
<atomRef index="131" />
<atomRef index="203" />
<atomRef index="211" />
<atomRef index="213" />
<atomRef index="293" />
<atomRef index="221" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #213" />
<nrOfStructures value="6"/>
<atomRef index="294" />
<atomRef index="204" />
<atomRef index="132" />
<atomRef index="214" />
<atomRef index="222" />
<atomRef index="212" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #214" />
<nrOfStructures value="6"/>
<atomRef index="133" />
<atomRef index="205" />
<atomRef index="213" />
<atomRef index="223" />
<atomRef index="215" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #215" />
<nrOfStructures value="5"/>
<atomRef index="134" />
<atomRef index="206" />
<atomRef index="214" />
<atomRef index="224" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #216" />
<nrOfStructures value="5"/>
<atomRef index="135" />
<atomRef index="207" />
<atomRef index="217" />
<atomRef index="225" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #217" />
<nrOfStructures value="6"/>
<atomRef index="216" />
<atomRef index="208" />
<atomRef index="136" />
<atomRef index="218" />
<atomRef index="226" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #218" />
<nrOfStructures value="6"/>
<atomRef index="137" />
<atomRef index="217" />
<atomRef index="209" />
<atomRef index="219" />
<atomRef index="227" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #219" />
<nrOfStructures value="6"/>
<atomRef index="138" />
<atomRef index="210" />
<atomRef index="218" />
<atomRef index="228" />
<atomRef index="220" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #220" />
<nrOfStructures value="6"/>
<atomRef index="139" />
<atomRef index="211" />
<atomRef index="301" />
<atomRef index="221" />
<atomRef index="229" />
<atomRef index="219" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #221" />
<nrOfStructures value="6"/>
<atomRef index="220" />
<atomRef index="212" />
<atomRef index="140" />
<atomRef index="222" />
<atomRef index="230" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #222" />
<nrOfStructures value="6"/>
<atomRef index="141" />
<atomRef index="213" />
<atomRef index="221" />
<atomRef index="303" />
<atomRef index="231" />
<atomRef index="223" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #223" />
<nrOfStructures value="6"/>
<atomRef index="142" />
<atomRef index="214" />
<atomRef index="232" />
<atomRef index="224" />
<atomRef index="222" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #224" />
<nrOfStructures value="5"/>
<atomRef index="143" />
<atomRef index="215" />
<atomRef index="223" />
<atomRef index="233" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #225" />
<nrOfStructures value="5"/>
<atomRef index="144" />
<atomRef index="216" />
<atomRef index="226" />
<atomRef index="234" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #226" />
<nrOfStructures value="6"/>
<atomRef index="145" />
<atomRef index="217" />
<atomRef index="225" />
<atomRef index="227" />
<atomRef index="307" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #227" />
<nrOfStructures value="6"/>
<atomRef index="146" />
<atomRef index="218" />
<atomRef index="226" />
<atomRef index="236" />
<atomRef index="228" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #228" />
<nrOfStructures value="6"/>
<atomRef index="219" />
<atomRef index="147" />
<atomRef index="227" />
<atomRef index="229" />
<atomRef index="237" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #229" />
<nrOfStructures value="6"/>
<atomRef index="148" />
<atomRef index="220" />
<atomRef index="228" />
<atomRef index="230" />
<atomRef index="310" />
<atomRef index="238" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #230" />
<nrOfStructures value="6"/>
<atomRef index="229" />
<atomRef index="221" />
<atomRef index="149" />
<atomRef index="231" />
<atomRef index="239" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #231" />
<nrOfStructures value="6"/>
<atomRef index="240" />
<atomRef index="222" />
<atomRef index="230" />
<atomRef index="232" />
<atomRef index="150" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #232" />
<nrOfStructures value="6"/>
<atomRef index="313" />
<atomRef index="223" />
<atomRef index="241" />
<atomRef index="233" />
<atomRef index="231" />
<atomRef index="151" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #233" />
<nrOfStructures value="5"/>
<atomRef index="152" />
<atomRef index="224" />
<atomRef index="232" />
<atomRef index="242" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #234" />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="225" />
<atomRef index="235" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #235" />
<nrOfStructures value="5"/>
<atomRef index="154" />
<atomRef index="226" />
<atomRef index="234" />
<atomRef index="236" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #236" />
<nrOfStructures value="5"/>
<atomRef index="155" />
<atomRef index="227" />
<atomRef index="235" />
<atomRef index="237" />
<atomRef index="317" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #237" />
<nrOfStructures value="5"/>
<atomRef index="156" />
<atomRef index="228" />
<atomRef index="236" />
<atomRef index="238" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #238" />
<nrOfStructures value="5"/>
<atomRef index="157" />
<atomRef index="229" />
<atomRef index="237" />
<atomRef index="239" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #239" />
<nrOfStructures value="5"/>
<atomRef index="158" />
<atomRef index="230" />
<atomRef index="238" />
<atomRef index="240" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #240" />
<nrOfStructures value="5"/>
<atomRef index="159" />
<atomRef index="231" />
<atomRef index="239" />
<atomRef index="241" />
<atomRef index="321" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #241" />
<nrOfStructures value="5"/>
<atomRef index="160" />
<atomRef index="232" />
<atomRef index="240" />
<atomRef index="242" />
<atomRef index="322" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #242" />
<nrOfStructures value="4"/>
<atomRef index="161" />
<atomRef index="233" />
<atomRef index="241" />
<atomRef index="323" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #243" />
<nrOfStructures value="4"/>
<atomRef index="162" />
<atomRef index="244" />
<atomRef index="252" />
<atomRef index="324" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #244" />
<nrOfStructures value="5"/>
<atomRef index="163" />
<atomRef index="243" />
<atomRef index="245" />
<atomRef index="253" />
<atomRef index="325" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #245" />
<nrOfStructures value="5"/>
<atomRef index="164" />
<atomRef index="244" />
<atomRef index="246" />
<atomRef index="254" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #246" />
<nrOfStructures value="5"/>
<atomRef index="165" />
<atomRef index="245" />
<atomRef index="247" />
<atomRef index="255" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #247" />
<nrOfStructures value="5"/>
<atomRef index="166" />
<atomRef index="246" />
<atomRef index="248" />
<atomRef index="256" />
<atomRef index="328" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #248" />
<nrOfStructures value="5"/>
<atomRef index="167" />
<atomRef index="247" />
<atomRef index="249" />
<atomRef index="257" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #249" />
<nrOfStructures value="5"/>
<atomRef index="168" />
<atomRef index="248" />
<atomRef index="250" />
<atomRef index="258" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #250" />
<nrOfStructures value="5"/>
<atomRef index="169" />
<atomRef index="249" />
<atomRef index="251" />
<atomRef index="259" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #251" />
<nrOfStructures value="4"/>
<atomRef index="170" />
<atomRef index="250" />
<atomRef index="260" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #252" />
<nrOfStructures value="5"/>
<atomRef index="171" />
<atomRef index="243" />
<atomRef index="253" />
<atomRef index="261" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #253" />
<nrOfStructures value="6"/>
<atomRef index="252" />
<atomRef index="244" />
<atomRef index="172" />
<atomRef index="254" />
<atomRef index="262" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #254" />
<nrOfStructures value="6"/>
<atomRef index="255" />
<atomRef index="245" />
<atomRef index="173" />
<atomRef index="253" />
<atomRef index="263" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #255" />
<nrOfStructures value="6"/>
<atomRef index="254" />
<atomRef index="246" />
<atomRef index="174" />
<atomRef index="256" />
<atomRef index="264" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #256" />
<nrOfStructures value="6"/>
<atomRef index="175" />
<atomRef index="247" />
<atomRef index="255" />
<atomRef index="337" />
<atomRef index="265" />
<atomRef index="257" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #257" />
<nrOfStructures value="6"/>
<atomRef index="176" />
<atomRef index="248" />
<atomRef index="266" />
<atomRef index="258" />
<atomRef index="256" />
<atomRef index="338" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #258" />
<nrOfStructures value="6"/>
<atomRef index="177" />
<atomRef index="249" />
<atomRef index="257" />
<atomRef index="339" />
<atomRef index="267" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #259" />
<nrOfStructures value="6"/>
<atomRef index="178" />
<atomRef index="250" />
<atomRef index="258" />
<atomRef index="340" />
<atomRef index="268" />
<atomRef index="260" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #260" />
<nrOfStructures value="5"/>
<atomRef index="179" />
<atomRef index="251" />
<atomRef index="259" />
<atomRef index="269" />
<atomRef index="341" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #261" />
<nrOfStructures value="5"/>
<atomRef index="180" />
<atomRef index="252" />
<atomRef index="262" />
<atomRef index="270" />
<atomRef index="342" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #262" />
<nrOfStructures value="6"/>
<atomRef index="181" />
<atomRef index="271" />
<atomRef index="253" />
<atomRef index="263" />
<atomRef index="261" />
<atomRef index="343" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #263" />
<nrOfStructures value="6"/>
<atomRef index="182" />
<atomRef index="264" />
<atomRef index="262" />
<atomRef index="254" />
<atomRef index="272" />
<atomRef index="344" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #264" />
<nrOfStructures value="6"/>
<atomRef index="183" />
<atomRef index="255" />
<atomRef index="263" />
<atomRef index="265" />
<atomRef index="345" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #265" />
<nrOfStructures value="6"/>
<atomRef index="184" />
<atomRef index="266" />
<atomRef index="264" />
<atomRef index="256" />
<atomRef index="274" />
<atomRef index="346" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #266" />
<nrOfStructures value="6"/>
<atomRef index="265" />
<atomRef index="257" />
<atomRef index="185" />
<atomRef index="267" />
<atomRef index="275" />
<atomRef index="347" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #267" />
<nrOfStructures value="6"/>
<atomRef index="186" />
<atomRef index="266" />
<atomRef index="258" />
<atomRef index="268" />
<atomRef index="276" />
<atomRef index="348" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #268" />
<nrOfStructures value="6"/>
<atomRef index="267" />
<atomRef index="259" />
<atomRef index="187" />
<atomRef index="269" />
<atomRef index="277" />
<atomRef index="349" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #269" />
<nrOfStructures value="5"/>
<atomRef index="188" />
<atomRef index="260" />
<atomRef index="268" />
<atomRef index="278" />
<atomRef index="350" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #270" />
<nrOfStructures value="5"/>
<atomRef index="189" />
<atomRef index="261" />
<atomRef index="271" />
<atomRef index="279" />
<atomRef index="351" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #271" />
<nrOfStructures value="6"/>
<atomRef index="190" />
<atomRef index="262" />
<atomRef index="280" />
<atomRef index="272" />
<atomRef index="270" />
<atomRef index="352" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #272" />
<nrOfStructures value="6"/>
<atomRef index="263" />
<atomRef index="191" />
<atomRef index="271" />
<atomRef index="273" />
<atomRef index="281" />
<atomRef index="353" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #273" />
<nrOfStructures value="6"/>
<atomRef index="274" />
<atomRef index="264" />
<atomRef index="272" />
<atomRef index="192" />
<atomRef index="282" />
<atomRef index="354" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #274" />
<nrOfStructures value="6"/>
<atomRef index="265" />
<atomRef index="193" />
<atomRef index="273" />
<atomRef index="275" />
<atomRef index="283" />
<atomRef index="355" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #275" />
<nrOfStructures value="6"/>
<atomRef index="194" />
<atomRef index="266" />
<atomRef index="356" />
<atomRef index="276" />
<atomRef index="284" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #276" />
<nrOfStructures value="6"/>
<atomRef index="195" />
<atomRef index="267" />
<atomRef index="275" />
<atomRef index="285" />
<atomRef index="277" />
<atomRef index="357" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #277" />
<nrOfStructures value="6"/>
<atomRef index="278" />
<atomRef index="268" />
<atomRef index="196" />
<atomRef index="276" />
<atomRef index="286" />
<atomRef index="358" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #278" />
<nrOfStructures value="5"/>
<atomRef index="197" />
<atomRef index="269" />
<atomRef index="277" />
<atomRef index="287" />
<atomRef index="359" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #279" />
<nrOfStructures value="5"/>
<atomRef index="198" />
<atomRef index="270" />
<atomRef index="280" />
<atomRef index="288" />
<atomRef index="360" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #280" />
<nrOfStructures value="6"/>
<atomRef index="361" />
<atomRef index="281" />
<atomRef index="279" />
<atomRef index="271" />
<atomRef index="289" />
<atomRef index="199" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #281" />
<nrOfStructures value="6"/>
<atomRef index="272" />
<atomRef index="200" />
<atomRef index="280" />
<atomRef index="282" />
<atomRef index="290" />
<atomRef index="362" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #282" />
<nrOfStructures value="6"/>
<atomRef index="201" />
<atomRef index="363" />
<atomRef index="281" />
<atomRef index="283" />
<atomRef index="291" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #283" />
<nrOfStructures value="6"/>
<atomRef index="202" />
<atomRef index="284" />
<atomRef index="282" />
<atomRef index="274" />
<atomRef index="292" />
<atomRef index="364" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #284" />
<nrOfStructures value="6"/>
<atomRef index="203" />
<atomRef index="283" />
<atomRef index="275" />
<atomRef index="285" />
<atomRef index="293" />
<atomRef index="365" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #285" />
<nrOfStructures value="6"/>
<atomRef index="204" />
<atomRef index="284" />
<atomRef index="276" />
<atomRef index="286" />
<atomRef index="294" />
<atomRef index="366" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #286" />
<nrOfStructures value="6"/>
<atomRef index="367" />
<atomRef index="277" />
<atomRef index="205" />
<atomRef index="287" />
<atomRef index="295" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #287" />
<nrOfStructures value="5"/>
<atomRef index="206" />
<atomRef index="278" />
<atomRef index="286" />
<atomRef index="296" />
<atomRef index="368" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #288" />
<nrOfStructures value="5"/>
<atomRef index="207" />
<atomRef index="279" />
<atomRef index="289" />
<atomRef index="297" />
<atomRef index="369" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #289" />
<nrOfStructures value="6"/>
<atomRef index="298" />
<atomRef index="280" />
<atomRef index="288" />
<atomRef index="290" />
<atomRef index="208" />
<atomRef index="370" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #290" />
<nrOfStructures value="6"/>
<atomRef index="299" />
<atomRef index="281" />
<atomRef index="289" />
<atomRef index="291" />
<atomRef index="209" />
<atomRef index="371" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #291" />
<nrOfStructures value="6"/>
<atomRef index="210" />
<atomRef index="282" />
<atomRef index="290" />
<atomRef index="292" />
<atomRef index="372" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #292" />
<nrOfStructures value="6"/>
<atomRef index="211" />
<atomRef index="291" />
<atomRef index="283" />
<atomRef index="293" />
<atomRef index="301" />
<atomRef index="373" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #293" />
<nrOfStructures value="6"/>
<atomRef index="292" />
<atomRef index="284" />
<atomRef index="212" />
<atomRef index="294" />
<atomRef index="302" />
<atomRef index="374" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #294" />
<nrOfStructures value="6"/>
<atomRef index="303" />
<atomRef index="285" />
<atomRef index="293" />
<atomRef index="295" />
<atomRef index="213" />
<atomRef index="375" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #295" />
<nrOfStructures value="6"/>
<atomRef index="214" />
<atomRef index="294" />
<atomRef index="286" />
<atomRef index="296" />
<atomRef index="304" />
<atomRef index="376" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #296" />
<nrOfStructures value="5"/>
<atomRef index="215" />
<atomRef index="287" />
<atomRef index="295" />
<atomRef index="305" />
<atomRef index="377" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #297" />
<nrOfStructures value="5"/>
<atomRef index="216" />
<atomRef index="288" />
<atomRef index="298" />
<atomRef index="306" />
<atomRef index="378" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #298" />
<nrOfStructures value="6"/>
<atomRef index="217" />
<atomRef index="307" />
<atomRef index="379" />
<atomRef index="297" />
<atomRef index="289" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #299" />
<nrOfStructures value="6"/>
<atomRef index="298" />
<atomRef index="290" />
<atomRef index="218" />
<atomRef index="300" />
<atomRef index="308" />
<atomRef index="380" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #300" />
<nrOfStructures value="6"/>
<atomRef index="219" />
<atomRef index="291" />
<atomRef index="381" />
<atomRef index="301" />
<atomRef index="309" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #301" />
<nrOfStructures value="6"/>
<atomRef index="220" />
<atomRef index="292" />
<atomRef index="300" />
<atomRef index="310" />
<atomRef index="302" />
<atomRef index="382" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #302" />
<nrOfStructures value="6"/>
<atomRef index="293" />
<atomRef index="221" />
<atomRef index="301" />
<atomRef index="303" />
<atomRef index="311" />
<atomRef index="383" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #303" />
<nrOfStructures value="6"/>
<atomRef index="222" />
<atomRef index="294" />
<atomRef index="312" />
<atomRef index="304" />
<atomRef index="302" />
<atomRef index="384" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #304" />
<nrOfStructures value="6"/>
<atomRef index="223" />
<atomRef index="295" />
<atomRef index="303" />
<atomRef index="385" />
<atomRef index="313" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #305" />
<nrOfStructures value="5"/>
<atomRef index="224" />
<atomRef index="296" />
<atomRef index="304" />
<atomRef index="314" />
<atomRef index="386" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #306" />
<nrOfStructures value="5"/>
<atomRef index="225" />
<atomRef index="297" />
<atomRef index="307" />
<atomRef index="315" />
<atomRef index="387" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #307" />
<nrOfStructures value="6"/>
<atomRef index="306" />
<atomRef index="298" />
<atomRef index="226" />
<atomRef index="308" />
<atomRef index="316" />
<atomRef index="388" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #308" />
<nrOfStructures value="6"/>
<atomRef index="227" />
<atomRef index="317" />
<atomRef index="307" />
<atomRef index="309" />
<atomRef index="389" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #309" />
<nrOfStructures value="6"/>
<atomRef index="228" />
<atomRef index="390" />
<atomRef index="308" />
<atomRef index="310" />
<atomRef index="300" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #310" />
<nrOfStructures value="6"/>
<atomRef index="229" />
<atomRef index="309" />
<atomRef index="301" />
<atomRef index="311" />
<atomRef index="319" />
<atomRef index="391" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #311" />
<nrOfStructures value="6"/>
<atomRef index="230" />
<atomRef index="310" />
<atomRef index="302" />
<atomRef index="312" />
<atomRef index="320" />
<atomRef index="392" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #312" />
<nrOfStructures value="6"/>
<atomRef index="313" />
<atomRef index="303" />
<atomRef index="311" />
<atomRef index="231" />
<atomRef index="321" />
<atomRef index="393" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #313" />
<nrOfStructures value="6"/>
<atomRef index="232" />
<atomRef index="304" />
<atomRef index="322" />
<atomRef index="314" />
<atomRef index="312" />
<atomRef index="394" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #314" />
<nrOfStructures value="5"/>
<atomRef index="233" />
<atomRef index="305" />
<atomRef index="313" />
<atomRef index="323" />
<atomRef index="395" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #315" />
<nrOfStructures value="4"/>
<atomRef index="234" />
<atomRef index="306" />
<atomRef index="316" />
<atomRef index="396" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #316" />
<nrOfStructures value="5"/>
<atomRef index="235" />
<atomRef index="307" />
<atomRef index="315" />
<atomRef index="317" />
<atomRef index="397" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #317" />
<nrOfStructures value="5"/>
<atomRef index="236" />
<atomRef index="308" />
<atomRef index="316" />
<atomRef index="318" />
<atomRef index="398" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #318" />
<nrOfStructures value="5"/>
<atomRef index="237" />
<atomRef index="309" />
<atomRef index="317" />
<atomRef index="319" />
<atomRef index="399" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #319" />
<nrOfStructures value="5"/>
<atomRef index="238" />
<atomRef index="310" />
<atomRef index="318" />
<atomRef index="320" />
<atomRef index="400" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #320" />
<nrOfStructures value="5"/>
<atomRef index="239" />
<atomRef index="311" />
<atomRef index="319" />
<atomRef index="321" />
<atomRef index="401" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #321" />
<nrOfStructures value="5"/>
<atomRef index="240" />
<atomRef index="312" />
<atomRef index="320" />
<atomRef index="322" />
<atomRef index="402" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #322" />
<nrOfStructures value="5"/>
<atomRef index="241" />
<atomRef index="313" />
<atomRef index="321" />
<atomRef index="323" />
<atomRef index="403" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #323" />
<nrOfStructures value="4"/>
<atomRef index="242" />
<atomRef index="314" />
<atomRef index="322" />
<atomRef index="404" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #324" />
<nrOfStructures value="4"/>
<atomRef index="243" />
<atomRef index="325" />
<atomRef index="333" />
<atomRef index="405" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #325" />
<nrOfStructures value="5"/>
<atomRef index="244" />
<atomRef index="324" />
<atomRef index="326" />
<atomRef index="334" />
<atomRef index="406" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #326" />
<nrOfStructures value="5"/>
<atomRef index="245" />
<atomRef index="325" />
<atomRef index="327" />
<atomRef index="335" />
<atomRef index="407" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #327" />
<nrOfStructures value="5"/>
<atomRef index="246" />
<atomRef index="326" />
<atomRef index="328" />
<atomRef index="336" />
<atomRef index="408" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #328" />
<nrOfStructures value="5"/>
<atomRef index="247" />
<atomRef index="327" />
<atomRef index="329" />
<atomRef index="337" />
<atomRef index="409" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #329" />
<nrOfStructures value="5"/>
<atomRef index="248" />
<atomRef index="328" />
<atomRef index="330" />
<atomRef index="338" />
<atomRef index="410" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #330" />
<nrOfStructures value="5"/>
<atomRef index="249" />
<atomRef index="329" />
<atomRef index="331" />
<atomRef index="339" />
<atomRef index="411" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #331" />
<nrOfStructures value="5"/>
<atomRef index="250" />
<atomRef index="330" />
<atomRef index="332" />
<atomRef index="340" />
<atomRef index="412" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #332" />
<nrOfStructures value="4"/>
<atomRef index="251" />
<atomRef index="331" />
<atomRef index="341" />
<atomRef index="413" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #333" />
<nrOfStructures value="5"/>
<atomRef index="252" />
<atomRef index="324" />
<atomRef index="334" />
<atomRef index="342" />
<atomRef index="414" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #334" />
<nrOfStructures value="6"/>
<atomRef index="253" />
<atomRef index="333" />
<atomRef index="325" />
<atomRef index="335" />
<atomRef index="343" />
<atomRef index="415" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #335" />
<nrOfStructures value="6"/>
<atomRef index="326" />
<atomRef index="254" />
<atomRef index="334" />
<atomRef index="336" />
<atomRef index="344" />
<atomRef index="416" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #336" />
<nrOfStructures value="6"/>
<atomRef index="345" />
<atomRef index="327" />
<atomRef index="335" />
<atomRef index="337" />
<atomRef index="255" />
<atomRef index="417" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #337" />
<nrOfStructures value="6"/>
<atomRef index="328" />
<atomRef index="256" />
<atomRef index="336" />
<atomRef index="338" />
<atomRef index="346" />
<atomRef index="418" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #338" />
<nrOfStructures value="6"/>
<atomRef index="257" />
<atomRef index="339" />
<atomRef index="329" />
<atomRef index="337" />
<atomRef index="347" />
<atomRef index="419" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #339" />
<nrOfStructures value="6"/>
<atomRef index="330" />
<atomRef index="258" />
<atomRef index="338" />
<atomRef index="340" />
<atomRef index="348" />
<atomRef index="420" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #340" />
<nrOfStructures value="6"/>
<atomRef index="339" />
<atomRef index="331" />
<atomRef index="259" />
<atomRef index="341" />
<atomRef index="349" />
<atomRef index="421" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #341" />
<nrOfStructures value="5"/>
<atomRef index="260" />
<atomRef index="332" />
<atomRef index="340" />
<atomRef index="350" />
<atomRef index="422" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #342" />
<nrOfStructures value="5"/>
<atomRef index="261" />
<atomRef index="333" />
<atomRef index="343" />
<atomRef index="351" />
<atomRef index="423" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #343" />
<nrOfStructures value="6"/>
<atomRef index="424" />
<atomRef index="334" />
<atomRef index="344" />
<atomRef index="342" />
<atomRef index="262" />
<atomRef index="352" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #344" />
<nrOfStructures value="6"/>
<atomRef index="263" />
<atomRef index="345" />
<atomRef index="343" />
<atomRef index="335" />
<atomRef index="353" />
<atomRef index="425" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #345" />
<nrOfStructures value="6"/>
<atomRef index="264" />
<atomRef index="336" />
<atomRef index="344" />
<atomRef index="346" />
<atomRef index="426" />
<atomRef index="354" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #346" />
<nrOfStructures value="6"/>
<atomRef index="347" />
<atomRef index="337" />
<atomRef index="345" />
<atomRef index="265" />
<atomRef index="355" />
<atomRef index="427" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #347" />
<nrOfStructures value="6"/>
<atomRef index="266" />
<atomRef index="338" />
<atomRef index="356" />
<atomRef index="348" />
<atomRef index="346" />
<atomRef index="428" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #348" />
<nrOfStructures value="6"/>
<atomRef index="267" />
<atomRef index="347" />
<atomRef index="339" />
<atomRef index="349" />
<atomRef index="357" />
<atomRef index="429" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #349" />
<nrOfStructures value="6"/>
<atomRef index="268" />
<atomRef index="340" />
<atomRef index="348" />
<atomRef index="358" />
<atomRef index="350" />
<atomRef index="430" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #350" />
<nrOfStructures value="5"/>
<atomRef index="269" />
<atomRef index="341" />
<atomRef index="349" />
<atomRef index="359" />
<atomRef index="431" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #351" />
<nrOfStructures value="5"/>
<atomRef index="270" />
<atomRef index="342" />
<atomRef index="352" />
<atomRef index="360" />
<atomRef index="432" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #352" />
<nrOfStructures value="6"/>
<atomRef index="351" />
<atomRef index="343" />
<atomRef index="271" />
<atomRef index="353" />
<atomRef index="361" />
<atomRef index="433" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #353" />
<nrOfStructures value="6"/>
<atomRef index="354" />
<atomRef index="344" />
<atomRef index="352" />
<atomRef index="272" />
<atomRef index="362" />
<atomRef index="434" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #354" />
<nrOfStructures value="6"/>
<atomRef index="273" />
<atomRef index="353" />
<atomRef index="345" />
<atomRef index="355" />
<atomRef index="363" />
<atomRef index="435" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #355" />
<nrOfStructures value="6"/>
<atomRef index="274" />
<atomRef index="346" />
<atomRef index="364" />
<atomRef index="356" />
<atomRef index="354" />
<atomRef index="436" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #356" />
<nrOfStructures value="6"/>
<atomRef index="275" />
<atomRef index="437" />
<atomRef index="357" />
<atomRef index="355" />
<atomRef index="365" />
<atomRef index="347" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #357" />
<nrOfStructures value="6"/>
<atomRef index="356" />
<atomRef index="348" />
<atomRef index="276" />
<atomRef index="358" />
<atomRef index="366" />
<atomRef index="438" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #358" />
<nrOfStructures value="6"/>
<atomRef index="439" />
<atomRef index="277" />
<atomRef index="359" />
<atomRef index="357" />
<atomRef index="367" />
<atomRef index="349" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #359" />
<nrOfStructures value="5"/>
<atomRef index="278" />
<atomRef index="350" />
<atomRef index="358" />
<atomRef index="368" />
<atomRef index="440" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #360" />
<nrOfStructures value="5"/>
<atomRef index="279" />
<atomRef index="351" />
<atomRef index="361" />
<atomRef index="369" />
<atomRef index="441" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #361" />
<nrOfStructures value="6"/>
<atomRef index="280" />
<atomRef index="352" />
<atomRef index="442" />
<atomRef index="362" />
<atomRef index="370" />
<atomRef index="360" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #362" />
<nrOfStructures value="6"/>
<atomRef index="281" />
<atomRef index="361" />
<atomRef index="353" />
<atomRef index="363" />
<atomRef index="371" />
<atomRef index="443" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #363" />
<nrOfStructures value="6"/>
<atomRef index="364" />
<atomRef index="354" />
<atomRef index="362" />
<atomRef index="282" />
<atomRef index="372" />
<atomRef index="444" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #364" />
<nrOfStructures value="6"/>
<atomRef index="283" />
<atomRef index="445" />
<atomRef index="363" />
<atomRef index="365" />
<atomRef index="373" />
<atomRef index="355" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #365" />
<nrOfStructures value="6"/>
<atomRef index="446" />
<atomRef index="284" />
<atomRef index="364" />
<atomRef index="366" />
<atomRef index="374" />
<atomRef index="356" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #366" />
<nrOfStructures value="6"/>
<atomRef index="285" />
<atomRef index="367" />
<atomRef index="365" />
<atomRef index="357" />
<atomRef index="375" />
<atomRef index="447" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #367" />
<nrOfStructures value="6"/>
<atomRef index="286" />
<atomRef index="358" />
<atomRef index="366" />
<atomRef index="368" />
<atomRef index="448" />
<atomRef index="376" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #368" />
<nrOfStructures value="5"/>
<atomRef index="287" />
<atomRef index="359" />
<atomRef index="367" />
<atomRef index="377" />
<atomRef index="449" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #369" />
<nrOfStructures value="5"/>
<atomRef index="288" />
<atomRef index="360" />
<atomRef index="370" />
<atomRef index="378" />
<atomRef index="450" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #370" />
<nrOfStructures value="6"/>
<atomRef index="289" />
<atomRef index="361" />
<atomRef index="369" />
<atomRef index="451" />
<atomRef index="379" />
<atomRef index="371" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #371" />
<nrOfStructures value="6"/>
<atomRef index="290" />
<atomRef index="362" />
<atomRef index="370" />
<atomRef index="372" />
<atomRef index="452" />
<atomRef index="380" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #372" />
<nrOfStructures value="6"/>
<atomRef index="291" />
<atomRef index="363" />
<atomRef index="371" />
<atomRef index="381" />
<atomRef index="373" />
<atomRef index="453" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #373" />
<nrOfStructures value="6"/>
<atomRef index="372" />
<atomRef index="364" />
<atomRef index="292" />
<atomRef index="374" />
<atomRef index="382" />
<atomRef index="454" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #374" />
<nrOfStructures value="6"/>
<atomRef index="293" />
<atomRef index="365" />
<atomRef index="455" />
<atomRef index="373" />
<atomRef index="383" />
<atomRef index="375" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #375" />
<nrOfStructures value="6"/>
<atomRef index="376" />
<atomRef index="366" />
<atomRef index="374" />
<atomRef index="294" />
<atomRef index="384" />
<atomRef index="456" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #376" />
<nrOfStructures value="6"/>
<atomRef index="295" />
<atomRef index="367" />
<atomRef index="375" />
<atomRef index="457" />
<atomRef index="385" />
<atomRef index="377" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #377" />
<nrOfStructures value="5"/>
<atomRef index="296" />
<atomRef index="368" />
<atomRef index="376" />
<atomRef index="386" />
<atomRef index="458" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #378" />
<nrOfStructures value="5"/>
<atomRef index="297" />
<atomRef index="369" />
<atomRef index="379" />
<atomRef index="387" />
<atomRef index="459" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #379" />
<nrOfStructures value="6"/>
<atomRef index="380" />
<atomRef index="370" />
<atomRef index="378" />
<atomRef index="298" />
<atomRef index="388" />
<atomRef index="460" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #380" />
<nrOfStructures value="6"/>
<atomRef index="299" />
<atomRef index="371" />
<atomRef index="381" />
<atomRef index="461" />
<atomRef index="389" />
<atomRef index="379" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #381" />
<nrOfStructures value="6"/>
<atomRef index="390" />
<atomRef index="372" />
<atomRef index="380" />
<atomRef index="382" />
<atomRef index="300" />
<atomRef index="462" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #382" />
<nrOfStructures value="6"/>
<atomRef index="301" />
<atomRef index="373" />
<atomRef index="381" />
<atomRef index="383" />
<atomRef index="463" />
<atomRef index="391" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #383" />
<nrOfStructures value="6"/>
<atomRef index="302" />
<atomRef index="374" />
<atomRef index="392" />
<atomRef index="382" />
<atomRef index="384" />
<atomRef index="464" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #384" />
<nrOfStructures value="6"/>
<atomRef index="393" />
<atomRef index="375" />
<atomRef index="383" />
<atomRef index="385" />
<atomRef index="303" />
<atomRef index="465" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #385" />
<nrOfStructures value="6"/>
<atomRef index="394" />
<atomRef index="376" />
<atomRef index="384" />
<atomRef index="386" />
<atomRef index="304" />
<atomRef index="466" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #386" />
<nrOfStructures value="5"/>
<atomRef index="305" />
<atomRef index="377" />
<atomRef index="385" />
<atomRef index="395" />
<atomRef index="467" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #387" />
<nrOfStructures value="5"/>
<atomRef index="306" />
<atomRef index="378" />
<atomRef index="388" />
<atomRef index="396" />
<atomRef index="468" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #388" />
<nrOfStructures value="6"/>
<atomRef index="387" />
<atomRef index="379" />
<atomRef index="307" />
<atomRef index="389" />
<atomRef index="397" />
<atomRef index="469" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #389" />
<nrOfStructures value="6"/>
<atomRef index="380" />
<atomRef index="308" />
<atomRef index="388" />
<atomRef index="390" />
<atomRef index="398" />
<atomRef index="470" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #390" />
<nrOfStructures value="6"/>
<atomRef index="309" />
<atomRef index="381" />
<atomRef index="389" />
<atomRef index="391" />
<atomRef index="471" />
<atomRef index="399" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #391" />
<nrOfStructures value="6"/>
<atomRef index="310" />
<atomRef index="382" />
<atomRef index="390" />
<atomRef index="400" />
<atomRef index="392" />
<atomRef index="472" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #392" />
<nrOfStructures value="6"/>
<atomRef index="473" />
<atomRef index="391" />
<atomRef index="383" />
<atomRef index="393" />
<atomRef index="401" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #393" />
<nrOfStructures value="6"/>
<atomRef index="402" />
<atomRef index="384" />
<atomRef index="392" />
<atomRef index="394" />
<atomRef index="312" />
<atomRef index="474" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #394" />
<nrOfStructures value="6"/>
<atomRef index="313" />
<atomRef index="393" />
<atomRef index="385" />
<atomRef index="395" />
<atomRef index="403" />
<atomRef index="475" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #395" />
<nrOfStructures value="5"/>
<atomRef index="314" />
<atomRef index="386" />
<atomRef index="394" />
<atomRef index="404" />
<atomRef index="476" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #396" />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="387" />
<atomRef index="397" />
<atomRef index="477" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #397" />
<nrOfStructures value="5"/>
<atomRef index="316" />
<atomRef index="388" />
<atomRef index="396" />
<atomRef index="398" />
<atomRef index="478" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #398" />
<nrOfStructures value="5"/>
<atomRef index="317" />
<atomRef index="389" />
<atomRef index="397" />
<atomRef index="399" />
<atomRef index="479" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #399" />
<nrOfStructures value="5"/>
<atomRef index="318" />
<atomRef index="390" />
<atomRef index="398" />
<atomRef index="400" />
<atomRef index="480" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #400" />
<nrOfStructures value="5"/>
<atomRef index="319" />
<atomRef index="391" />
<atomRef index="399" />
<atomRef index="401" />
<atomRef index="481" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #401" />
<nrOfStructures value="5"/>
<atomRef index="320" />
<atomRef index="392" />
<atomRef index="400" />
<atomRef index="402" />
<atomRef index="482" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #402" />
<nrOfStructures value="5"/>
<atomRef index="321" />
<atomRef index="393" />
<atomRef index="401" />
<atomRef index="403" />
<atomRef index="483" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #403" />
<nrOfStructures value="5"/>
<atomRef index="322" />
<atomRef index="394" />
<atomRef index="402" />
<atomRef index="404" />
<atomRef index="484" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #404" />
<nrOfStructures value="4"/>
<atomRef index="323" />
<atomRef index="395" />
<atomRef index="403" />
<atomRef index="485" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #405" />
<nrOfStructures value="4"/>
<atomRef index="324" />
<atomRef index="406" />
<atomRef index="414" />
<atomRef index="486" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #406" />
<nrOfStructures value="5"/>
<atomRef index="325" />
<atomRef index="405" />
<atomRef index="407" />
<atomRef index="415" />
<atomRef index="487" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #407" />
<nrOfStructures value="5"/>
<atomRef index="326" />
<atomRef index="406" />
<atomRef index="408" />
<atomRef index="416" />
<atomRef index="488" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #408" />
<nrOfStructures value="5"/>
<atomRef index="327" />
<atomRef index="407" />
<atomRef index="409" />
<atomRef index="417" />
<atomRef index="489" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #409" />
<nrOfStructures value="5"/>
<atomRef index="328" />
<atomRef index="408" />
<atomRef index="410" />
<atomRef index="418" />
<atomRef index="490" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #410" />
<nrOfStructures value="5"/>
<atomRef index="329" />
<atomRef index="409" />
<atomRef index="411" />
<atomRef index="419" />
<atomRef index="491" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #411" />
<nrOfStructures value="5"/>
<atomRef index="330" />
<atomRef index="410" />
<atomRef index="412" />
<atomRef index="420" />
<atomRef index="492" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #412" />
<nrOfStructures value="5"/>
<atomRef index="331" />
<atomRef index="411" />
<atomRef index="413" />
<atomRef index="421" />
<atomRef index="493" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #413" />
<nrOfStructures value="4"/>
<atomRef index="332" />
<atomRef index="412" />
<atomRef index="422" />
<atomRef index="494" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #414" />
<nrOfStructures value="5"/>
<atomRef index="333" />
<atomRef index="405" />
<atomRef index="415" />
<atomRef index="423" />
<atomRef index="495" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #415" />
<nrOfStructures value="6"/>
<atomRef index="414" />
<atomRef index="406" />
<atomRef index="334" />
<atomRef index="416" />
<atomRef index="424" />
<atomRef index="496" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #416" />
<nrOfStructures value="6"/>
<atomRef index="335" />
<atomRef index="415" />
<atomRef index="417" />
<atomRef index="407" />
<atomRef index="425" />
<atomRef index="497" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #417" />
<nrOfStructures value="6"/>
<atomRef index="336" />
<atomRef index="408" />
<atomRef index="498" />
<atomRef index="418" />
<atomRef index="426" />
<atomRef index="416" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #418" />
<nrOfStructures value="6"/>
<atomRef index="337" />
<atomRef index="409" />
<atomRef index="499" />
<atomRef index="419" />
<atomRef index="427" />
<atomRef index="417" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #419" />
<nrOfStructures value="6"/>
<atomRef index="338" />
<atomRef index="410" />
<atomRef index="418" />
<atomRef index="428" />
<atomRef index="420" />
<atomRef index="500" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #420" />
<nrOfStructures value="6"/>
<atomRef index="339" />
<atomRef index="411" />
<atomRef index="429" />
<atomRef index="421" />
<atomRef index="419" />
<atomRef index="501" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #421" />
<nrOfStructures value="6"/>
<atomRef index="422" />
<atomRef index="412" />
<atomRef index="420" />
<atomRef index="340" />
<atomRef index="430" />
<atomRef index="502" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #422" />
<nrOfStructures value="5"/>
<atomRef index="341" />
<atomRef index="413" />
<atomRef index="421" />
<atomRef index="431" />
<atomRef index="503" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #423" />
<nrOfStructures value="5"/>
<atomRef index="342" />
<atomRef index="414" />
<atomRef index="424" />
<atomRef index="432" />
<atomRef index="504" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #424" />
<nrOfStructures value="6"/>
<atomRef index="343" />
<atomRef index="433" />
<atomRef index="423" />
<atomRef index="505" />
<atomRef index="415" />
<atomRef index="425" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #425" />
<nrOfStructures value="6"/>
<atomRef index="344" />
<atomRef index="506" />
<atomRef index="424" />
<atomRef index="426" />
<atomRef index="434" />
<atomRef index="416" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #426" />
<nrOfStructures value="6"/>
<atomRef index="507" />
<atomRef index="427" />
<atomRef index="425" />
<atomRef index="417" />
<atomRef index="435" />
<atomRef index="345" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #427" />
<nrOfStructures value="6"/>
<atomRef index="346" />
<atomRef index="418" />
<atomRef index="508" />
<atomRef index="428" />
<atomRef index="436" />
<atomRef index="426" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #428" />
<nrOfStructures value="6"/>
<atomRef index="347" />
<atomRef index="419" />
<atomRef index="427" />
<atomRef index="509" />
<atomRef index="437" />
<atomRef index="429" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #429" />
<nrOfStructures value="6"/>
<atomRef index="420" />
<atomRef index="348" />
<atomRef index="428" />
<atomRef index="430" />
<atomRef index="438" />
<atomRef index="510" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #430" />
<nrOfStructures value="6"/>
<atomRef index="349" />
<atomRef index="421" />
<atomRef index="429" />
<atomRef index="439" />
<atomRef index="431" />
<atomRef index="511" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #431" />
<nrOfStructures value="5"/>
<atomRef index="350" />
<atomRef index="422" />
<atomRef index="430" />
<atomRef index="440" />
<atomRef index="512" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #432" />
<nrOfStructures value="5"/>
<atomRef index="351" />
<atomRef index="423" />
<atomRef index="433" />
<atomRef index="441" />
<atomRef index="513" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #433" />
<nrOfStructures value="6"/>
<atomRef index="434" />
<atomRef index="424" />
<atomRef index="432" />
<atomRef index="352" />
<atomRef index="442" />
<atomRef index="514" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #434" />
<nrOfStructures value="6"/>
<atomRef index="353" />
<atomRef index="433" />
<atomRef index="425" />
<atomRef index="435" />
<atomRef index="443" />
<atomRef index="515" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #435" />
<nrOfStructures value="6"/>
<atomRef index="354" />
<atomRef index="426" />
<atomRef index="434" />
<atomRef index="436" />
<atomRef index="516" />
<atomRef index="444" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #436" />
<nrOfStructures value="6"/>
<atomRef index="517" />
<atomRef index="437" />
<atomRef index="435" />
<atomRef index="427" />
<atomRef index="445" />
<atomRef index="355" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #437" />
<nrOfStructures value="6"/>
<atomRef index="446" />
<atomRef index="428" />
<atomRef index="436" />
<atomRef index="438" />
<atomRef index="356" />
<atomRef index="518" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #438" />
<nrOfStructures value="6"/>
<atomRef index="357" />
<atomRef index="519" />
<atomRef index="437" />
<atomRef index="439" />
<atomRef index="447" />
<atomRef index="429" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #439" />
<nrOfStructures value="6"/>
<atomRef index="358" />
<atomRef index="440" />
<atomRef index="438" />
<atomRef index="430" />
<atomRef index="448" />
<atomRef index="520" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #440" />
<nrOfStructures value="5"/>
<atomRef index="359" />
<atomRef index="431" />
<atomRef index="439" />
<atomRef index="449" />
<atomRef index="521" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #441" />
<nrOfStructures value="5"/>
<atomRef index="360" />
<atomRef index="432" />
<atomRef index="442" />
<atomRef index="450" />
<atomRef index="522" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #442" />
<nrOfStructures value="6"/>
<atomRef index="451" />
<atomRef index="433" />
<atomRef index="441" />
<atomRef index="443" />
<atomRef index="361" />
<atomRef index="523" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #443" />
<nrOfStructures value="6"/>
<atomRef index="362" />
<atomRef index="442" />
<atomRef index="434" />
<atomRef index="444" />
<atomRef index="452" />
<atomRef index="524" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #444" />
<nrOfStructures value="6"/>
<atomRef index="363" />
<atomRef index="435" />
<atomRef index="443" />
<atomRef index="525" />
<atomRef index="453" />
<atomRef index="445" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #445" />
<nrOfStructures value="6"/>
<atomRef index="364" />
<atomRef index="436" />
<atomRef index="444" />
<atomRef index="446" />
<atomRef index="526" />
<atomRef index="454" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #446" />
<nrOfStructures value="6"/>
<atomRef index="365" />
<atomRef index="527" />
<atomRef index="445" />
<atomRef index="447" />
<atomRef index="455" />
<atomRef index="437" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #447" />
<nrOfStructures value="6"/>
<atomRef index="366" />
<atomRef index="448" />
<atomRef index="446" />
<atomRef index="438" />
<atomRef index="456" />
<atomRef index="528" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #448" />
<nrOfStructures value="6"/>
<atomRef index="367" />
<atomRef index="529" />
<atomRef index="447" />
<atomRef index="449" />
<atomRef index="457" />
<atomRef index="439" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #449" />
<nrOfStructures value="5"/>
<atomRef index="368" />
<atomRef index="440" />
<atomRef index="448" />
<atomRef index="458" />
<atomRef index="530" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #450" />
<nrOfStructures value="5"/>
<atomRef index="369" />
<atomRef index="441" />
<atomRef index="451" />
<atomRef index="459" />
<atomRef index="531" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #451" />
<nrOfStructures value="6"/>
<atomRef index="450" />
<atomRef index="442" />
<atomRef index="370" />
<atomRef index="452" />
<atomRef index="460" />
<atomRef index="532" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #452" />
<nrOfStructures value="6"/>
<atomRef index="371" />
<atomRef index="443" />
<atomRef index="533" />
<atomRef index="453" />
<atomRef index="461" />
<atomRef index="451" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #453" />
<nrOfStructures value="6"/>
<atomRef index="372" />
<atomRef index="462" />
<atomRef index="452" />
<atomRef index="444" />
<atomRef index="454" />
<atomRef index="534" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #454" />
<nrOfStructures value="6"/>
<atomRef index="373" />
<atomRef index="445" />
<atomRef index="463" />
<atomRef index="453" />
<atomRef index="455" />
<atomRef index="535" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #455" />
<nrOfStructures value="6"/>
<atomRef index="464" />
<atomRef index="446" />
<atomRef index="454" />
<atomRef index="456" />
<atomRef index="374" />
<atomRef index="536" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #456" />
<nrOfStructures value="6"/>
<atomRef index="457" />
<atomRef index="447" />
<atomRef index="455" />
<atomRef index="375" />
<atomRef index="465" />
<atomRef index="537" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #457" />
<nrOfStructures value="6"/>
<atomRef index="466" />
<atomRef index="448" />
<atomRef index="456" />
<atomRef index="458" />
<atomRef index="376" />
<atomRef index="538" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #458" />
<nrOfStructures value="5"/>
<atomRef index="377" />
<atomRef index="449" />
<atomRef index="457" />
<atomRef index="467" />
<atomRef index="539" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #459" />
<nrOfStructures value="5"/>
<atomRef index="378" />
<atomRef index="450" />
<atomRef index="460" />
<atomRef index="468" />
<atomRef index="540" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #460" />
<nrOfStructures value="6"/>
<atomRef index="379" />
<atomRef index="461" />
<atomRef index="459" />
<atomRef index="451" />
<atomRef index="469" />
<atomRef index="541" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #461" />
<nrOfStructures value="6"/>
<atomRef index="460" />
<atomRef index="452" />
<atomRef index="380" />
<atomRef index="462" />
<atomRef index="470" />
<atomRef index="542" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #462" />
<nrOfStructures value="6"/>
<atomRef index="463" />
<atomRef index="453" />
<atomRef index="461" />
<atomRef index="381" />
<atomRef index="471" />
<atomRef index="543" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #463" />
<nrOfStructures value="6"/>
<atomRef index="382" />
<atomRef index="544" />
<atomRef index="462" />
<atomRef index="464" />
<atomRef index="472" />
<atomRef index="454" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #464" />
<nrOfStructures value="6"/>
<atomRef index="383" />
<atomRef index="455" />
<atomRef index="463" />
<atomRef index="473" />
<atomRef index="465" />
<atomRef index="545" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #465" />
<nrOfStructures value="6"/>
<atomRef index="384" />
<atomRef index="456" />
<atomRef index="464" />
<atomRef index="546" />
<atomRef index="474" />
<atomRef index="466" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #466" />
<nrOfStructures value="6"/>
<atomRef index="385" />
<atomRef index="457" />
<atomRef index="475" />
<atomRef index="467" />
<atomRef index="465" />
<atomRef index="547" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #467" />
<nrOfStructures value="5"/>
<atomRef index="386" />
<atomRef index="458" />
<atomRef index="466" />
<atomRef index="476" />
<atomRef index="548" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #468" />
<nrOfStructures value="5"/>
<atomRef index="387" />
<atomRef index="459" />
<atomRef index="469" />
<atomRef index="477" />
<atomRef index="549" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #469" />
<nrOfStructures value="6"/>
<atomRef index="478" />
<atomRef index="460" />
<atomRef index="468" />
<atomRef index="470" />
<atomRef index="388" />
<atomRef index="550" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #470" />
<nrOfStructures value="6"/>
<atomRef index="389" />
<atomRef index="461" />
<atomRef index="469" />
<atomRef index="551" />
<atomRef index="479" />
<atomRef index="471" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #471" />
<nrOfStructures value="6"/>
<atomRef index="390" />
<atomRef index="462" />
<atomRef index="472" />
<atomRef index="552" />
<atomRef index="480" />
<atomRef index="470" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #472" />
<nrOfStructures value="6"/>
<atomRef index="391" />
<atomRef index="471" />
<atomRef index="463" />
<atomRef index="473" />
<atomRef index="481" />
<atomRef index="553" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #473" />
<nrOfStructures value="6"/>
<atomRef index="392" />
<atomRef index="464" />
<atomRef index="472" />
<atomRef index="554" />
<atomRef index="482" />
<atomRef index="474" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #474" />
<nrOfStructures value="6"/>
<atomRef index="465" />
<atomRef index="393" />
<atomRef index="473" />
<atomRef index="475" />
<atomRef index="483" />
<atomRef index="555" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #475" />
<nrOfStructures value="6"/>
<atomRef index="394" />
<atomRef index="466" />
<atomRef index="474" />
<atomRef index="484" />
<atomRef index="476" />
<atomRef index="556" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #476" />
<nrOfStructures value="5"/>
<atomRef index="395" />
<atomRef index="467" />
<atomRef index="475" />
<atomRef index="485" />
<atomRef index="557" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #477" />
<nrOfStructures value="4"/>
<atomRef index="396" />
<atomRef index="468" />
<atomRef index="478" />
<atomRef index="558" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #478" />
<nrOfStructures value="5"/>
<atomRef index="397" />
<atomRef index="469" />
<atomRef index="477" />
<atomRef index="479" />
<atomRef index="559" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #479" />
<nrOfStructures value="5"/>
<atomRef index="398" />
<atomRef index="470" />
<atomRef index="478" />
<atomRef index="480" />
<atomRef index="560" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #480" />
<nrOfStructures value="5"/>
<atomRef index="399" />
<atomRef index="471" />
<atomRef index="479" />
<atomRef index="481" />
<atomRef index="561" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #481" />
<nrOfStructures value="5"/>
<atomRef index="400" />
<atomRef index="472" />
<atomRef index="480" />
<atomRef index="482" />
<atomRef index="562" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #482" />
<nrOfStructures value="5"/>
<atomRef index="401" />
<atomRef index="473" />
<atomRef index="481" />
<atomRef index="483" />
<atomRef index="563" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #483" />
<nrOfStructures value="5"/>
<atomRef index="402" />
<atomRef index="474" />
<atomRef index="482" />
<atomRef index="484" />
<atomRef index="564" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #484" />
<nrOfStructures value="5"/>
<atomRef index="403" />
<atomRef index="475" />
<atomRef index="483" />
<atomRef index="485" />
<atomRef index="565" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #485" />
<nrOfStructures value="4"/>
<atomRef index="404" />
<atomRef index="476" />
<atomRef index="484" />
<atomRef index="566" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #486" />
<nrOfStructures value="4"/>
<atomRef index="405" />
<atomRef index="487" />
<atomRef index="495" />
<atomRef index="567" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #487" />
<nrOfStructures value="5"/>
<atomRef index="406" />
<atomRef index="486" />
<atomRef index="488" />
<atomRef index="496" />
<atomRef index="568" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #488" />
<nrOfStructures value="5"/>
<atomRef index="407" />
<atomRef index="487" />
<atomRef index="489" />
<atomRef index="497" />
<atomRef index="569" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #489" />
<nrOfStructures value="5"/>
<atomRef index="408" />
<atomRef index="488" />
<atomRef index="490" />
<atomRef index="498" />
<atomRef index="570" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #490" />
<nrOfStructures value="5"/>
<atomRef index="409" />
<atomRef index="489" />
<atomRef index="491" />
<atomRef index="499" />
<atomRef index="571" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #491" />
<nrOfStructures value="5"/>
<atomRef index="410" />
<atomRef index="490" />
<atomRef index="492" />
<atomRef index="500" />
<atomRef index="572" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #492" />
<nrOfStructures value="5"/>
<atomRef index="411" />
<atomRef index="491" />
<atomRef index="493" />
<atomRef index="501" />
<atomRef index="573" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #493" />
<nrOfStructures value="5"/>
<atomRef index="412" />
<atomRef index="492" />
<atomRef index="494" />
<atomRef index="502" />
<atomRef index="574" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #494" />
<nrOfStructures value="4"/>
<atomRef index="413" />
<atomRef index="493" />
<atomRef index="503" />
<atomRef index="575" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #495" />
<nrOfStructures value="5"/>
<atomRef index="414" />
<atomRef index="486" />
<atomRef index="496" />
<atomRef index="504" />
<atomRef index="576" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #496" />
<nrOfStructures value="6"/>
<atomRef index="415" />
<atomRef index="487" />
<atomRef index="505" />
<atomRef index="495" />
<atomRef index="497" />
<atomRef index="577" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #497" />
<nrOfStructures value="6"/>
<atomRef index="416" />
<atomRef index="488" />
<atomRef index="496" />
<atomRef index="498" />
<atomRef index="578" />
<atomRef index="506" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #498" />
<nrOfStructures value="6"/>
<atomRef index="417" />
<atomRef index="489" />
<atomRef index="507" />
<atomRef index="497" />
<atomRef index="499" />
<atomRef index="579" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #499" />
<nrOfStructures value="6"/>
<atomRef index="490" />
<atomRef index="418" />
<atomRef index="498" />
<atomRef index="500" />
<atomRef index="508" />
<atomRef index="580" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #500" />
<nrOfStructures value="6"/>
<atomRef index="419" />
<atomRef index="491" />
<atomRef index="499" />
<atomRef index="581" />
<atomRef index="509" />
<atomRef index="501" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #501" />
<nrOfStructures value="6"/>
<atomRef index="500" />
<atomRef index="492" />
<atomRef index="420" />
<atomRef index="502" />
<atomRef index="510" />
<atomRef index="582" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #502" />
<nrOfStructures value="6"/>
<atomRef index="421" />
<atomRef index="493" />
<atomRef index="501" />
<atomRef index="583" />
<atomRef index="511" />
<atomRef index="503" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #503" />
<nrOfStructures value="5"/>
<atomRef index="422" />
<atomRef index="494" />
<atomRef index="502" />
<atomRef index="512" />
<atomRef index="584" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #504" />
<nrOfStructures value="5"/>
<atomRef index="423" />
<atomRef index="495" />
<atomRef index="505" />
<atomRef index="513" />
<atomRef index="585" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #505" />
<nrOfStructures value="6"/>
<atomRef index="586" />
<atomRef index="496" />
<atomRef index="424" />
<atomRef index="506" />
<atomRef index="514" />
<atomRef index="504" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #506" />
<nrOfStructures value="6"/>
<atomRef index="507" />
<atomRef index="497" />
<atomRef index="425" />
<atomRef index="505" />
<atomRef index="515" />
<atomRef index="587" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #507" />
<nrOfStructures value="6"/>
<atomRef index="426" />
<atomRef index="498" />
<atomRef index="588" />
<atomRef index="508" />
<atomRef index="516" />
<atomRef index="506" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #508" />
<nrOfStructures value="6"/>
<atomRef index="427" />
<atomRef index="589" />
<atomRef index="507" />
<atomRef index="509" />
<atomRef index="517" />
<atomRef index="499" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #509" />
<nrOfStructures value="6"/>
<atomRef index="428" />
<atomRef index="510" />
<atomRef index="508" />
<atomRef index="500" />
<atomRef index="518" />
<atomRef index="590" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #510" />
<nrOfStructures value="6"/>
<atomRef index="429" />
<atomRef index="591" />
<atomRef index="509" />
<atomRef index="511" />
<atomRef index="519" />
<atomRef index="501" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #511" />
<nrOfStructures value="6"/>
<atomRef index="430" />
<atomRef index="512" />
<atomRef index="502" />
<atomRef index="510" />
<atomRef index="520" />
<atomRef index="592" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #512" />
<nrOfStructures value="5"/>
<atomRef index="431" />
<atomRef index="503" />
<atomRef index="511" />
<atomRef index="521" />
<atomRef index="593" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #513" />
<nrOfStructures value="5"/>
<atomRef index="432" />
<atomRef index="504" />
<atomRef index="514" />
<atomRef index="522" />
<atomRef index="594" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #514" />
<nrOfStructures value="6"/>
<atomRef index="505" />
<atomRef index="433" />
<atomRef index="513" />
<atomRef index="515" />
<atomRef index="523" />
<atomRef index="595" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #515" />
<nrOfStructures value="6"/>
<atomRef index="596" />
<atomRef index="506" />
<atomRef index="514" />
<atomRef index="516" />
<atomRef index="434" />
<atomRef index="524" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #516" />
<nrOfStructures value="6"/>
<atomRef index="515" />
<atomRef index="507" />
<atomRef index="435" />
<atomRef index="517" />
<atomRef index="525" />
<atomRef index="597" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #517" />
<nrOfStructures value="6"/>
<atomRef index="598" />
<atomRef index="518" />
<atomRef index="516" />
<atomRef index="508" />
<atomRef index="526" />
<atomRef index="436" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #518" />
<nrOfStructures value="6"/>
<atomRef index="519" />
<atomRef index="509" />
<atomRef index="517" />
<atomRef index="437" />
<atomRef index="527" />
<atomRef index="599" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #519" />
<nrOfStructures value="6"/>
<atomRef index="438" />
<atomRef index="600" />
<atomRef index="518" />
<atomRef index="520" />
<atomRef index="528" />
<atomRef index="510" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #520" />
<nrOfStructures value="6"/>
<atomRef index="529" />
<atomRef index="511" />
<atomRef index="519" />
<atomRef index="521" />
<atomRef index="439" />
<atomRef index="601" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #521" />
<nrOfStructures value="5"/>
<atomRef index="440" />
<atomRef index="512" />
<atomRef index="520" />
<atomRef index="530" />
<atomRef index="602" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #522" />
<nrOfStructures value="5"/>
<atomRef index="441" />
<atomRef index="513" />
<atomRef index="523" />
<atomRef index="531" />
<atomRef index="603" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #523" />
<nrOfStructures value="6"/>
<atomRef index="442" />
<atomRef index="514" />
<atomRef index="522" />
<atomRef index="604" />
<atomRef index="532" />
<atomRef index="524" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #524" />
<nrOfStructures value="6"/>
<atomRef index="443" />
<atomRef index="605" />
<atomRef index="525" />
<atomRef index="523" />
<atomRef index="533" />
<atomRef index="515" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #525" />
<nrOfStructures value="6"/>
<atomRef index="444" />
<atomRef index="524" />
<atomRef index="516" />
<atomRef index="526" />
<atomRef index="534" />
<atomRef index="606" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #526" />
<nrOfStructures value="6"/>
<atomRef index="445" />
<atomRef index="525" />
<atomRef index="517" />
<atomRef index="527" />
<atomRef index="535" />
<atomRef index="607" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #527" />
<nrOfStructures value="6"/>
<atomRef index="528" />
<atomRef index="518" />
<atomRef index="526" />
<atomRef index="446" />
<atomRef index="536" />
<atomRef index="608" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #528" />
<nrOfStructures value="6"/>
<atomRef index="447" />
<atomRef index="529" />
<atomRef index="527" />
<atomRef index="519" />
<atomRef index="537" />
<atomRef index="609" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #529" />
<nrOfStructures value="6"/>
<atomRef index="528" />
<atomRef index="520" />
<atomRef index="448" />
<atomRef index="530" />
<atomRef index="538" />
<atomRef index="610" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #530" />
<nrOfStructures value="5"/>
<atomRef index="449" />
<atomRef index="521" />
<atomRef index="529" />
<atomRef index="539" />
<atomRef index="611" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #531" />
<nrOfStructures value="5"/>
<atomRef index="450" />
<atomRef index="522" />
<atomRef index="532" />
<atomRef index="540" />
<atomRef index="612" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #532" />
<nrOfStructures value="6"/>
<atomRef index="533" />
<atomRef index="523" />
<atomRef index="531" />
<atomRef index="613" />
<atomRef index="541" />
<atomRef index="451" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #533" />
<nrOfStructures value="6"/>
<atomRef index="532" />
<atomRef index="524" />
<atomRef index="452" />
<atomRef index="534" />
<atomRef index="542" />
<atomRef index="614" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #534" />
<nrOfStructures value="6"/>
<atomRef index="453" />
<atomRef index="543" />
<atomRef index="525" />
<atomRef index="533" />
<atomRef index="535" />
<atomRef index="615" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #535" />
<nrOfStructures value="6"/>
<atomRef index="616" />
<atomRef index="526" />
<atomRef index="534" />
<atomRef index="544" />
<atomRef index="536" />
<atomRef index="454" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #536" />
<nrOfStructures value="6"/>
<atomRef index="455" />
<atomRef index="527" />
<atomRef index="545" />
<atomRef index="537" />
<atomRef index="535" />
<atomRef index="617" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #537" />
<nrOfStructures value="6"/>
<atomRef index="456" />
<atomRef index="528" />
<atomRef index="536" />
<atomRef index="538" />
<atomRef index="618" />
<atomRef index="546" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #538" />
<nrOfStructures value="6"/>
<atomRef index="529" />
<atomRef index="619" />
<atomRef index="537" />
<atomRef index="539" />
<atomRef index="547" />
<atomRef index="457" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #539" />
<nrOfStructures value="5"/>
<atomRef index="458" />
<atomRef index="530" />
<atomRef index="538" />
<atomRef index="548" />
<atomRef index="620" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #540" />
<nrOfStructures value="5"/>
<atomRef index="459" />
<atomRef index="531" />
<atomRef index="541" />
<atomRef index="549" />
<atomRef index="621" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #541" />
<nrOfStructures value="6"/>
<atomRef index="460" />
<atomRef index="540" />
<atomRef index="532" />
<atomRef index="542" />
<atomRef index="550" />
<atomRef index="622" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #542" />
<nrOfStructures value="6"/>
<atomRef index="543" />
<atomRef index="533" />
<atomRef index="541" />
<atomRef index="461" />
<atomRef index="551" />
<atomRef index="623" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #543" />
<nrOfStructures value="6"/>
<atomRef index="542" />
<atomRef index="534" />
<atomRef index="462" />
<atomRef index="544" />
<atomRef index="552" />
<atomRef index="624" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #544" />
<nrOfStructures value="6"/>
<atomRef index="545" />
<atomRef index="535" />
<atomRef index="543" />
<atomRef index="463" />
<atomRef index="553" />
<atomRef index="625" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #545" />
<nrOfStructures value="6"/>
<atomRef index="464" />
<atomRef index="536" />
<atomRef index="544" />
<atomRef index="554" />
<atomRef index="546" />
<atomRef index="626" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #546" />
<nrOfStructures value="6"/>
<atomRef index="545" />
<atomRef index="537" />
<atomRef index="465" />
<atomRef index="547" />
<atomRef index="555" />
<atomRef index="627" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #547" />
<nrOfStructures value="6"/>
<atomRef index="466" />
<atomRef index="538" />
<atomRef index="556" />
<atomRef index="548" />
<atomRef index="546" />
<atomRef index="628" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #548" />
<nrOfStructures value="5"/>
<atomRef index="467" />
<atomRef index="539" />
<atomRef index="547" />
<atomRef index="557" />
<atomRef index="629" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #549" />
<nrOfStructures value="5"/>
<atomRef index="468" />
<atomRef index="540" />
<atomRef index="550" />
<atomRef index="558" />
<atomRef index="630" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #550" />
<nrOfStructures value="6"/>
<atomRef index="469" />
<atomRef index="549" />
<atomRef index="541" />
<atomRef index="551" />
<atomRef index="559" />
<atomRef index="631" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #551" />
<nrOfStructures value="6"/>
<atomRef index="552" />
<atomRef index="542" />
<atomRef index="550" />
<atomRef index="470" />
<atomRef index="560" />
<atomRef index="632" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #552" />
<nrOfStructures value="6"/>
<atomRef index="471" />
<atomRef index="543" />
<atomRef index="551" />
<atomRef index="553" />
<atomRef index="633" />
<atomRef index="561" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #553" />
<nrOfStructures value="6"/>
<atomRef index="472" />
<atomRef index="552" />
<atomRef index="544" />
<atomRef index="554" />
<atomRef index="562" />
<atomRef index="634" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #554" />
<nrOfStructures value="6"/>
<atomRef index="563" />
<atomRef index="545" />
<atomRef index="553" />
<atomRef index="555" />
<atomRef index="473" />
<atomRef index="635" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #555" />
<nrOfStructures value="6"/>
<atomRef index="556" />
<atomRef index="546" />
<atomRef index="554" />
<atomRef index="474" />
<atomRef index="564" />
<atomRef index="636" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #556" />
<nrOfStructures value="6"/>
<atomRef index="475" />
<atomRef index="547" />
<atomRef index="555" />
<atomRef index="637" />
<atomRef index="565" />
<atomRef index="557" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #557" />
<nrOfStructures value="5"/>
<atomRef index="476" />
<atomRef index="548" />
<atomRef index="556" />
<atomRef index="566" />
<atomRef index="638" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #558" />
<nrOfStructures value="4"/>
<atomRef index="477" />
<atomRef index="549" />
<atomRef index="559" />
<atomRef index="639" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #559" />
<nrOfStructures value="5"/>
<atomRef index="478" />
<atomRef index="550" />
<atomRef index="558" />
<atomRef index="560" />
<atomRef index="640" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #560" />
<nrOfStructures value="5"/>
<atomRef index="479" />
<atomRef index="551" />
<atomRef index="559" />
<atomRef index="561" />
<atomRef index="641" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #561" />
<nrOfStructures value="5"/>
<atomRef index="480" />
<atomRef index="552" />
<atomRef index="560" />
<atomRef index="562" />
<atomRef index="642" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #562" />
<nrOfStructures value="5"/>
<atomRef index="481" />
<atomRef index="553" />
<atomRef index="561" />
<atomRef index="563" />
<atomRef index="643" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #563" />
<nrOfStructures value="5"/>
<atomRef index="482" />
<atomRef index="554" />
<atomRef index="562" />
<atomRef index="564" />
<atomRef index="644" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #564" />
<nrOfStructures value="5"/>
<atomRef index="483" />
<atomRef index="555" />
<atomRef index="563" />
<atomRef index="565" />
<atomRef index="645" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #565" />
<nrOfStructures value="5"/>
<atomRef index="484" />
<atomRef index="556" />
<atomRef index="564" />
<atomRef index="566" />
<atomRef index="646" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #566" />
<nrOfStructures value="4"/>
<atomRef index="485" />
<atomRef index="557" />
<atomRef index="565" />
<atomRef index="647" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #567" />
<nrOfStructures value="4"/>
<atomRef index="486" />
<atomRef index="568" />
<atomRef index="576" />
<atomRef index="648" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #568" />
<nrOfStructures value="5"/>
<atomRef index="487" />
<atomRef index="567" />
<atomRef index="569" />
<atomRef index="577" />
<atomRef index="649" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #569" />
<nrOfStructures value="5"/>
<atomRef index="488" />
<atomRef index="568" />
<atomRef index="570" />
<atomRef index="578" />
<atomRef index="650" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #570" />
<nrOfStructures value="5"/>
<atomRef index="489" />
<atomRef index="569" />
<atomRef index="571" />
<atomRef index="579" />
<atomRef index="651" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #571" />
<nrOfStructures value="5"/>
<atomRef index="490" />
<atomRef index="570" />
<atomRef index="572" />
<atomRef index="580" />
<atomRef index="652" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #572" />
<nrOfStructures value="5"/>
<atomRef index="491" />
<atomRef index="571" />
<atomRef index="573" />
<atomRef index="581" />
<atomRef index="653" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #573" />
<nrOfStructures value="5"/>
<atomRef index="492" />
<atomRef index="572" />
<atomRef index="574" />
<atomRef index="582" />
<atomRef index="654" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #574" />
<nrOfStructures value="5"/>
<atomRef index="493" />
<atomRef index="573" />
<atomRef index="575" />
<atomRef index="583" />
<atomRef index="655" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #575" />
<nrOfStructures value="4"/>
<atomRef index="494" />
<atomRef index="574" />
<atomRef index="584" />
<atomRef index="656" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #576" />
<nrOfStructures value="5"/>
<atomRef index="495" />
<atomRef index="567" />
<atomRef index="577" />
<atomRef index="585" />
<atomRef index="657" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #577" />
<nrOfStructures value="6"/>
<atomRef index="496" />
<atomRef index="578" />
<atomRef index="576" />
<atomRef index="568" />
<atomRef index="586" />
<atomRef index="658" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #578" />
<nrOfStructures value="6"/>
<atomRef index="497" />
<atomRef index="569" />
<atomRef index="579" />
<atomRef index="577" />
<atomRef index="659" />
<atomRef index="587" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #579" />
<nrOfStructures value="6"/>
<atomRef index="498" />
<atomRef index="660" />
<atomRef index="578" />
<atomRef index="580" />
<atomRef index="588" />
<atomRef index="570" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #580" />
<nrOfStructures value="6"/>
<atomRef index="499" />
<atomRef index="571" />
<atomRef index="579" />
<atomRef index="661" />
<atomRef index="589" />
<atomRef index="581" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #581" />
<nrOfStructures value="6"/>
<atomRef index="580" />
<atomRef index="572" />
<atomRef index="500" />
<atomRef index="582" />
<atomRef index="590" />
<atomRef index="662" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #582" />
<nrOfStructures value="6"/>
<atomRef index="573" />
<atomRef index="501" />
<atomRef index="581" />
<atomRef index="583" />
<atomRef index="591" />
<atomRef index="663" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #583" />
<nrOfStructures value="6"/>
<atomRef index="592" />
<atomRef index="574" />
<atomRef index="582" />
<atomRef index="584" />
<atomRef index="502" />
<atomRef index="664" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #584" />
<nrOfStructures value="5"/>
<atomRef index="503" />
<atomRef index="575" />
<atomRef index="583" />
<atomRef index="593" />
<atomRef index="665" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #585" />
<nrOfStructures value="5"/>
<atomRef index="504" />
<atomRef index="576" />
<atomRef index="586" />
<atomRef index="594" />
<atomRef index="666" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #586" />
<nrOfStructures value="6"/>
<atomRef index="587" />
<atomRef index="577" />
<atomRef index="585" />
<atomRef index="505" />
<atomRef index="595" />
<atomRef index="667" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #587" />
<nrOfStructures value="6"/>
<atomRef index="506" />
<atomRef index="578" />
<atomRef index="586" />
<atomRef index="668" />
<atomRef index="596" />
<atomRef index="588" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #588" />
<nrOfStructures value="6"/>
<atomRef index="587" />
<atomRef index="579" />
<atomRef index="507" />
<atomRef index="589" />
<atomRef index="597" />
<atomRef index="669" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #589" />
<nrOfStructures value="6"/>
<atomRef index="590" />
<atomRef index="598" />
<atomRef index="588" />
<atomRef index="508" />
<atomRef index="580" />
<atomRef index="670" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #590" />
<nrOfStructures value="6"/>
<atomRef index="509" />
<atomRef index="581" />
<atomRef index="589" />
<atomRef index="591" />
<atomRef index="671" />
<atomRef index="599" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #591" />
<nrOfStructures value="6"/>
<atomRef index="510" />
<atomRef index="582" />
<atomRef index="590" />
<atomRef index="672" />
<atomRef index="600" />
<atomRef index="592" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #592" />
<nrOfStructures value="6"/>
<atomRef index="601" />
<atomRef index="583" />
<atomRef index="591" />
<atomRef index="593" />
<atomRef index="511" />
<atomRef index="673" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #593" />
<nrOfStructures value="5"/>
<atomRef index="512" />
<atomRef index="584" />
<atomRef index="592" />
<atomRef index="602" />
<atomRef index="674" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #594" />
<nrOfStructures value="5"/>
<atomRef index="513" />
<atomRef index="585" />
<atomRef index="595" />
<atomRef index="603" />
<atomRef index="675" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #595" />
<nrOfStructures value="6"/>
<atomRef index="676" />
<atomRef index="514" />
<atomRef index="594" />
<atomRef index="596" />
<atomRef index="604" />
<atomRef index="586" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #596" />
<nrOfStructures value="6"/>
<atomRef index="515" />
<atomRef index="677" />
<atomRef index="595" />
<atomRef index="597" />
<atomRef index="605" />
<atomRef index="587" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #597" />
<nrOfStructures value="6"/>
<atomRef index="516" />
<atomRef index="596" />
<atomRef index="588" />
<atomRef index="598" />
<atomRef index="606" />
<atomRef index="678" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #598" />
<nrOfStructures value="6"/>
<atomRef index="599" />
<atomRef index="589" />
<atomRef index="597" />
<atomRef index="517" />
<atomRef index="607" />
<atomRef index="679" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #599" />
<nrOfStructures value="6"/>
<atomRef index="518" />
<atomRef index="598" />
<atomRef index="590" />
<atomRef index="600" />
<atomRef index="608" />
<atomRef index="680" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #600" />
<nrOfStructures value="6"/>
<atomRef index="609" />
<atomRef index="591" />
<atomRef index="599" />
<atomRef index="601" />
<atomRef index="519" />
<atomRef index="681" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #601" />
<nrOfStructures value="6"/>
<atomRef index="520" />
<atomRef index="682" />
<atomRef index="602" />
<atomRef index="600" />
<atomRef index="610" />
<atomRef index="592" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #602" />
<nrOfStructures value="5"/>
<atomRef index="521" />
<atomRef index="593" />
<atomRef index="601" />
<atomRef index="611" />
<atomRef index="683" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #603" />
<nrOfStructures value="5"/>
<atomRef index="522" />
<atomRef index="594" />
<atomRef index="604" />
<atomRef index="612" />
<atomRef index="684" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #604" />
<nrOfStructures value="6"/>
<atomRef index="523" />
<atomRef index="595" />
<atomRef index="685" />
<atomRef index="603" />
<atomRef index="613" />
<atomRef index="605" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #605" />
<nrOfStructures value="6"/>
<atomRef index="524" />
<atomRef index="596" />
<atomRef index="604" />
<atomRef index="686" />
<atomRef index="614" />
<atomRef index="606" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #606" />
<nrOfStructures value="6"/>
<atomRef index="525" />
<atomRef index="597" />
<atomRef index="605" />
<atomRef index="607" />
<atomRef index="687" />
<atomRef index="615" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #607" />
<nrOfStructures value="6"/>
<atomRef index="526" />
<atomRef index="598" />
<atomRef index="606" />
<atomRef index="616" />
<atomRef index="608" />
<atomRef index="688" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #608" />
<nrOfStructures value="6"/>
<atomRef index="527" />
<atomRef index="599" />
<atomRef index="607" />
<atomRef index="617" />
<atomRef index="609" />
<atomRef index="689" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #609" />
<nrOfStructures value="6"/>
<atomRef index="528" />
<atomRef index="600" />
<atomRef index="608" />
<atomRef index="618" />
<atomRef index="610" />
<atomRef index="690" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #610" />
<nrOfStructures value="6"/>
<atomRef index="611" />
<atomRef index="601" />
<atomRef index="609" />
<atomRef index="529" />
<atomRef index="619" />
<atomRef index="691" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #611" />
<nrOfStructures value="5"/>
<atomRef index="530" />
<atomRef index="602" />
<atomRef index="610" />
<atomRef index="620" />
<atomRef index="692" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #612" />
<nrOfStructures value="5"/>
<atomRef index="531" />
<atomRef index="603" />
<atomRef index="613" />
<atomRef index="621" />
<atomRef index="693" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #613" />
<nrOfStructures value="6"/>
<atomRef index="532" />
<atomRef index="612" />
<atomRef index="622" />
<atomRef index="614" />
<atomRef index="604" />
<atomRef index="694" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #614" />
<nrOfStructures value="6"/>
<atomRef index="533" />
<atomRef index="615" />
<atomRef index="613" />
<atomRef index="605" />
<atomRef index="623" />
<atomRef index="695" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #615" />
<nrOfStructures value="6"/>
<atomRef index="606" />
<atomRef index="534" />
<atomRef index="614" />
<atomRef index="616" />
<atomRef index="624" />
<atomRef index="696" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #616" />
<nrOfStructures value="6"/>
<atomRef index="535" />
<atomRef index="607" />
<atomRef index="615" />
<atomRef index="625" />
<atomRef index="617" />
<atomRef index="697" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #617" />
<nrOfStructures value="6"/>
<atomRef index="536" />
<atomRef index="608" />
<atomRef index="616" />
<atomRef index="618" />
<atomRef index="698" />
<atomRef index="626" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #618" />
<nrOfStructures value="6"/>
<atomRef index="537" />
<atomRef index="619" />
<atomRef index="617" />
<atomRef index="609" />
<atomRef index="627" />
<atomRef index="699" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #619" />
<nrOfStructures value="6"/>
<atomRef index="618" />
<atomRef index="610" />
<atomRef index="538" />
<atomRef index="620" />
<atomRef index="628" />
<atomRef index="700" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #620" />
<nrOfStructures value="5"/>
<atomRef index="539" />
<atomRef index="611" />
<atomRef index="619" />
<atomRef index="629" />
<atomRef index="701" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #621" />
<nrOfStructures value="5"/>
<atomRef index="540" />
<atomRef index="612" />
<atomRef index="622" />
<atomRef index="630" />
<atomRef index="702" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #622" />
<nrOfStructures value="6"/>
<atomRef index="623" />
<atomRef index="613" />
<atomRef index="621" />
<atomRef index="541" />
<atomRef index="631" />
<atomRef index="703" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #623" />
<nrOfStructures value="6"/>
<atomRef index="624" />
<atomRef index="632" />
<atomRef index="622" />
<atomRef index="542" />
<atomRef index="614" />
<atomRef index="704" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #624" />
<nrOfStructures value="6"/>
<atomRef index="543" />
<atomRef index="633" />
<atomRef index="623" />
<atomRef index="625" />
<atomRef index="705" />
<atomRef index="615" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #625" />
<nrOfStructures value="6"/>
<atomRef index="626" />
<atomRef index="616" />
<atomRef index="624" />
<atomRef index="544" />
<atomRef index="634" />
<atomRef index="706" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #626" />
<nrOfStructures value="6"/>
<atomRef index="635" />
<atomRef index="617" />
<atomRef index="625" />
<atomRef index="627" />
<atomRef index="545" />
<atomRef index="707" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #627" />
<nrOfStructures value="6"/>
<atomRef index="636" />
<atomRef index="618" />
<atomRef index="626" />
<atomRef index="628" />
<atomRef index="546" />
<atomRef index="708" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #628" />
<nrOfStructures value="6"/>
<atomRef index="629" />
<atomRef index="619" />
<atomRef index="627" />
<atomRef index="547" />
<atomRef index="637" />
<atomRef index="709" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #629" />
<nrOfStructures value="5"/>
<atomRef index="548" />
<atomRef index="620" />
<atomRef index="628" />
<atomRef index="638" />
<atomRef index="710" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #630" />
<nrOfStructures value="5"/>
<atomRef index="549" />
<atomRef index="621" />
<atomRef index="631" />
<atomRef index="639" />
<atomRef index="711" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #631" />
<nrOfStructures value="6"/>
<atomRef index="550" />
<atomRef index="712" />
<atomRef index="630" />
<atomRef index="632" />
<atomRef index="640" />
<atomRef index="622" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #632" />
<nrOfStructures value="6"/>
<atomRef index="631" />
<atomRef index="623" />
<atomRef index="551" />
<atomRef index="633" />
<atomRef index="641" />
<atomRef index="713" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #633" />
<nrOfStructures value="6"/>
<atomRef index="632" />
<atomRef index="624" />
<atomRef index="552" />
<atomRef index="634" />
<atomRef index="642" />
<atomRef index="714" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #634" />
<nrOfStructures value="6"/>
<atomRef index="553" />
<atomRef index="625" />
<atomRef index="633" />
<atomRef index="635" />
<atomRef index="715" />
<atomRef index="643" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #635" />
<nrOfStructures value="6"/>
<atomRef index="636" />
<atomRef index="626" />
<atomRef index="634" />
<atomRef index="554" />
<atomRef index="644" />
<atomRef index="716" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #636" />
<nrOfStructures value="6"/>
<atomRef index="627" />
<atomRef index="555" />
<atomRef index="635" />
<atomRef index="637" />
<atomRef index="645" />
<atomRef index="717" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #637" />
<nrOfStructures value="6"/>
<atomRef index="556" />
<atomRef index="628" />
<atomRef index="636" />
<atomRef index="638" />
<atomRef index="718" />
<atomRef index="646" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #638" />
<nrOfStructures value="5"/>
<atomRef index="557" />
<atomRef index="629" />
<atomRef index="637" />
<atomRef index="647" />
<atomRef index="719" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #639" />
<nrOfStructures value="4"/>
<atomRef index="558" />
<atomRef index="630" />
<atomRef index="640" />
<atomRef index="720" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #640" />
<nrOfStructures value="5"/>
<atomRef index="559" />
<atomRef index="631" />
<atomRef index="639" />
<atomRef index="641" />
<atomRef index="721" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #641" />
<nrOfStructures value="5"/>
<atomRef index="560" />
<atomRef index="632" />
<atomRef index="640" />
<atomRef index="642" />
<atomRef index="722" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #642" />
<nrOfStructures value="5"/>
<atomRef index="561" />
<atomRef index="633" />
<atomRef index="641" />
<atomRef index="643" />
<atomRef index="723" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #643" />
<nrOfStructures value="5"/>
<atomRef index="562" />
<atomRef index="634" />
<atomRef index="642" />
<atomRef index="644" />
<atomRef index="724" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #644" />
<nrOfStructures value="5"/>
<atomRef index="563" />
<atomRef index="635" />
<atomRef index="643" />
<atomRef index="645" />
<atomRef index="725" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #645" />
<nrOfStructures value="5"/>
<atomRef index="564" />
<atomRef index="636" />
<atomRef index="644" />
<atomRef index="646" />
<atomRef index="726" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #646" />
<nrOfStructures value="5"/>
<atomRef index="565" />
<atomRef index="637" />
<atomRef index="645" />
<atomRef index="647" />
<atomRef index="727" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #647" />
<nrOfStructures value="4"/>
<atomRef index="566" />
<atomRef index="638" />
<atomRef index="646" />
<atomRef index="728" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #648" />
<nrOfStructures value="3"/>
<atomRef index="567" />
<atomRef index="649" />
<atomRef index="657" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #649" />
<nrOfStructures value="4"/>
<atomRef index="568" />
<atomRef index="648" />
<atomRef index="650" />
<atomRef index="658" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #650" />
<nrOfStructures value="4"/>
<atomRef index="569" />
<atomRef index="649" />
<atomRef index="651" />
<atomRef index="659" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #651" />
<nrOfStructures value="4"/>
<atomRef index="570" />
<atomRef index="650" />
<atomRef index="652" />
<atomRef index="660" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #652" />
<nrOfStructures value="4"/>
<atomRef index="571" />
<atomRef index="651" />
<atomRef index="653" />
<atomRef index="661" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #653" />
<nrOfStructures value="4"/>
<atomRef index="572" />
<atomRef index="652" />
<atomRef index="654" />
<atomRef index="662" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #654" />
<nrOfStructures value="4"/>
<atomRef index="573" />
<atomRef index="653" />
<atomRef index="655" />
<atomRef index="663" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #655" />
<nrOfStructures value="4"/>
<atomRef index="574" />
<atomRef index="654" />
<atomRef index="656" />
<atomRef index="664" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #656" />
<nrOfStructures value="3"/>
<atomRef index="575" />
<atomRef index="655" />
<atomRef index="665" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #657" />
<nrOfStructures value="4"/>
<atomRef index="576" />
<atomRef index="648" />
<atomRef index="658" />
<atomRef index="666" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #658" />
<nrOfStructures value="5"/>
<atomRef index="577" />
<atomRef index="649" />
<atomRef index="657" />
<atomRef index="659" />
<atomRef index="667" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #659" />
<nrOfStructures value="5"/>
<atomRef index="578" />
<atomRef index="650" />
<atomRef index="658" />
<atomRef index="660" />
<atomRef index="668" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #660" />
<nrOfStructures value="5"/>
<atomRef index="579" />
<atomRef index="651" />
<atomRef index="659" />
<atomRef index="661" />
<atomRef index="669" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #661" />
<nrOfStructures value="5"/>
<atomRef index="580" />
<atomRef index="652" />
<atomRef index="660" />
<atomRef index="662" />
<atomRef index="670" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #662" />
<nrOfStructures value="5"/>
<atomRef index="581" />
<atomRef index="653" />
<atomRef index="661" />
<atomRef index="663" />
<atomRef index="671" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #663" />
<nrOfStructures value="5"/>
<atomRef index="582" />
<atomRef index="654" />
<atomRef index="662" />
<atomRef index="664" />
<atomRef index="672" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #664" />
<nrOfStructures value="5"/>
<atomRef index="583" />
<atomRef index="655" />
<atomRef index="663" />
<atomRef index="665" />
<atomRef index="673" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #665" />
<nrOfStructures value="4"/>
<atomRef index="584" />
<atomRef index="656" />
<atomRef index="664" />
<atomRef index="674" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #666" />
<nrOfStructures value="4"/>
<atomRef index="585" />
<atomRef index="657" />
<atomRef index="667" />
<atomRef index="675" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #667" />
<nrOfStructures value="5"/>
<atomRef index="586" />
<atomRef index="658" />
<atomRef index="666" />
<atomRef index="668" />
<atomRef index="676" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #668" />
<nrOfStructures value="5"/>
<atomRef index="587" />
<atomRef index="659" />
<atomRef index="667" />
<atomRef index="669" />
<atomRef index="677" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #669" />
<nrOfStructures value="5"/>
<atomRef index="588" />
<atomRef index="660" />
<atomRef index="668" />
<atomRef index="670" />
<atomRef index="678" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #670" />
<nrOfStructures value="5"/>
<atomRef index="589" />
<atomRef index="661" />
<atomRef index="669" />
<atomRef index="671" />
<atomRef index="679" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #671" />
<nrOfStructures value="5"/>
<atomRef index="590" />
<atomRef index="662" />
<atomRef index="670" />
<atomRef index="672" />
<atomRef index="680" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #672" />
<nrOfStructures value="5"/>
<atomRef index="591" />
<atomRef index="663" />
<atomRef index="671" />
<atomRef index="673" />
<atomRef index="681" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #673" />
<nrOfStructures value="5"/>
<atomRef index="592" />
<atomRef index="664" />
<atomRef index="672" />
<atomRef index="674" />
<atomRef index="682" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #674" />
<nrOfStructures value="4"/>
<atomRef index="593" />
<atomRef index="665" />
<atomRef index="673" />
<atomRef index="683" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #675" />
<nrOfStructures value="4"/>
<atomRef index="594" />
<atomRef index="666" />
<atomRef index="676" />
<atomRef index="684" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #676" />
<nrOfStructures value="5"/>
<atomRef index="595" />
<atomRef index="667" />
<atomRef index="675" />
<atomRef index="677" />
<atomRef index="685" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #677" />
<nrOfStructures value="5"/>
<atomRef index="596" />
<atomRef index="668" />
<atomRef index="676" />
<atomRef index="678" />
<atomRef index="686" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #678" />
<nrOfStructures value="5"/>
<atomRef index="597" />
<atomRef index="669" />
<atomRef index="677" />
<atomRef index="679" />
<atomRef index="687" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #679" />
<nrOfStructures value="5"/>
<atomRef index="598" />
<atomRef index="670" />
<atomRef index="678" />
<atomRef index="680" />
<atomRef index="688" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #680" />
<nrOfStructures value="5"/>
<atomRef index="599" />
<atomRef index="671" />
<atomRef index="679" />
<atomRef index="681" />
<atomRef index="689" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #681" />
<nrOfStructures value="5"/>
<atomRef index="600" />
<atomRef index="672" />
<atomRef index="680" />
<atomRef index="682" />
<atomRef index="690" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #682" />
<nrOfStructures value="5"/>
<atomRef index="601" />
<atomRef index="673" />
<atomRef index="681" />
<atomRef index="683" />
<atomRef index="691" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #683" />
<nrOfStructures value="4"/>
<atomRef index="602" />
<atomRef index="674" />
<atomRef index="682" />
<atomRef index="692" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #684" />
<nrOfStructures value="4"/>
<atomRef index="603" />
<atomRef index="675" />
<atomRef index="685" />
<atomRef index="693" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #685" />
<nrOfStructures value="5"/>
<atomRef index="604" />
<atomRef index="676" />
<atomRef index="684" />
<atomRef index="686" />
<atomRef index="694" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #686" />
<nrOfStructures value="5"/>
<atomRef index="605" />
<atomRef index="677" />
<atomRef index="685" />
<atomRef index="687" />
<atomRef index="695" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #687" />
<nrOfStructures value="5"/>
<atomRef index="606" />
<atomRef index="678" />
<atomRef index="686" />
<atomRef index="688" />
<atomRef index="696" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #688" />
<nrOfStructures value="5"/>
<atomRef index="607" />
<atomRef index="679" />
<atomRef index="687" />
<atomRef index="689" />
<atomRef index="697" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #689" />
<nrOfStructures value="5"/>
<atomRef index="608" />
<atomRef index="680" />
<atomRef index="688" />
<atomRef index="690" />
<atomRef index="698" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #690" />
<nrOfStructures value="5"/>
<atomRef index="609" />
<atomRef index="681" />
<atomRef index="689" />
<atomRef index="691" />
<atomRef index="699" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #691" />
<nrOfStructures value="5"/>
<atomRef index="610" />
<atomRef index="682" />
<atomRef index="690" />
<atomRef index="692" />
<atomRef index="700" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #692" />
<nrOfStructures value="4"/>
<atomRef index="611" />
<atomRef index="683" />
<atomRef index="691" />
<atomRef index="701" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #693" />
<nrOfStructures value="4"/>
<atomRef index="612" />
<atomRef index="684" />
<atomRef index="694" />
<atomRef index="702" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #694" />
<nrOfStructures value="5"/>
<atomRef index="613" />
<atomRef index="685" />
<atomRef index="693" />
<atomRef index="695" />
<atomRef index="703" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #695" />
<nrOfStructures value="5"/>
<atomRef index="614" />
<atomRef index="686" />
<atomRef index="694" />
<atomRef index="696" />
<atomRef index="704" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #696" />
<nrOfStructures value="5"/>
<atomRef index="615" />
<atomRef index="687" />
<atomRef index="695" />
<atomRef index="697" />
<atomRef index="705" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #697" />
<nrOfStructures value="5"/>
<atomRef index="616" />
<atomRef index="688" />
<atomRef index="696" />
<atomRef index="698" />
<atomRef index="706" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #698" />
<nrOfStructures value="5"/>
<atomRef index="617" />
<atomRef index="689" />
<atomRef index="697" />
<atomRef index="699" />
<atomRef index="707" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #699" />
<nrOfStructures value="5"/>
<atomRef index="618" />
<atomRef index="690" />
<atomRef index="698" />
<atomRef index="700" />
<atomRef index="708" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #700" />
<nrOfStructures value="5"/>
<atomRef index="619" />
<atomRef index="691" />
<atomRef index="699" />
<atomRef index="701" />
<atomRef index="709" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #701" />
<nrOfStructures value="4"/>
<atomRef index="620" />
<atomRef index="692" />
<atomRef index="700" />
<atomRef index="710" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #702" />
<nrOfStructures value="4"/>
<atomRef index="621" />
<atomRef index="693" />
<atomRef index="703" />
<atomRef index="711" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #703" />
<nrOfStructures value="5"/>
<atomRef index="622" />
<atomRef index="694" />
<atomRef index="702" />
<atomRef index="704" />
<atomRef index="712" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #704" />
<nrOfStructures value="5"/>
<atomRef index="623" />
<atomRef index="695" />
<atomRef index="703" />
<atomRef index="705" />
<atomRef index="713" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #705" />
<nrOfStructures value="5"/>
<atomRef index="624" />
<atomRef index="696" />
<atomRef index="704" />
<atomRef index="706" />
<atomRef index="714" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #706" />
<nrOfStructures value="5"/>
<atomRef index="625" />
<atomRef index="697" />
<atomRef index="705" />
<atomRef index="707" />
<atomRef index="715" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #707" />
<nrOfStructures value="5"/>
<atomRef index="626" />
<atomRef index="698" />
<atomRef index="706" />
<atomRef index="708" />
<atomRef index="716" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #708" />
<nrOfStructures value="5"/>
<atomRef index="627" />
<atomRef index="699" />
<atomRef index="707" />
<atomRef index="709" />
<atomRef index="717" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #709" />
<nrOfStructures value="5"/>
<atomRef index="628" />
<atomRef index="700" />
<atomRef index="708" />
<atomRef index="710" />
<atomRef index="718" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #710" />
<nrOfStructures value="4"/>
<atomRef index="629" />
<atomRef index="701" />
<atomRef index="709" />
<atomRef index="719" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #711" />
<nrOfStructures value="4"/>
<atomRef index="630" />
<atomRef index="702" />
<atomRef index="712" />
<atomRef index="720" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #712" />
<nrOfStructures value="5"/>
<atomRef index="631" />
<atomRef index="703" />
<atomRef index="711" />
<atomRef index="713" />
<atomRef index="721" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #713" />
<nrOfStructures value="5"/>
<atomRef index="632" />
<atomRef index="704" />
<atomRef index="712" />
<atomRef index="714" />
<atomRef index="722" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #714" />
<nrOfStructures value="5"/>
<atomRef index="633" />
<atomRef index="705" />
<atomRef index="713" />
<atomRef index="715" />
<atomRef index="723" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #715" />
<nrOfStructures value="5"/>
<atomRef index="634" />
<atomRef index="706" />
<atomRef index="714" />
<atomRef index="716" />
<atomRef index="724" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #716" />
<nrOfStructures value="5"/>
<atomRef index="635" />
<atomRef index="707" />
<atomRef index="715" />
<atomRef index="717" />
<atomRef index="725" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #717" />
<nrOfStructures value="5"/>
<atomRef index="636" />
<atomRef index="708" />
<atomRef index="716" />
<atomRef index="718" />
<atomRef index="726" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #718" />
<nrOfStructures value="5"/>
<atomRef index="637" />
<atomRef index="709" />
<atomRef index="717" />
<atomRef index="719" />
<atomRef index="727" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #719" />
<nrOfStructures value="4"/>
<atomRef index="638" />
<atomRef index="710" />
<atomRef index="718" />
<atomRef index="728" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #720" />
<nrOfStructures value="3"/>
<atomRef index="639" />
<atomRef index="711" />
<atomRef index="721" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #721" />
<nrOfStructures value="4"/>
<atomRef index="640" />
<atomRef index="712" />
<atomRef index="720" />
<atomRef index="722" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #722" />
<nrOfStructures value="4"/>
<atomRef index="641" />
<atomRef index="713" />
<atomRef index="721" />
<atomRef index="723" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #723" />
<nrOfStructures value="4"/>
<atomRef index="642" />
<atomRef index="714" />
<atomRef index="722" />
<atomRef index="724" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #724" />
<nrOfStructures value="4"/>
<atomRef index="643" />
<atomRef index="715" />
<atomRef index="723" />
<atomRef index="725" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #725" />
<nrOfStructures value="4"/>
<atomRef index="644" />
<atomRef index="716" />
<atomRef index="724" />
<atomRef index="726" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #726" />
<nrOfStructures value="4"/>
<atomRef index="645" />
<atomRef index="717" />
<atomRef index="725" />
<atomRef index="727" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #727" />
<nrOfStructures value="4"/>
<atomRef index="646" />
<atomRef index="718" />
<atomRef index="726" />
<atomRef index="728" />
</cell>
<cell>
<cellProperties   type="POLY_VERTEX"  name="Atom #728" />
<nrOfStructures value="3"/>
<atomRef index="647" />
<atomRef index="719" />
<atomRef index="727" />
</cell>
</structuralComponent>
<multiComponent name="Enclosed Volumes" >
<structuralComponent  name="volume #0"  mode="WIREFRAME_AND_SURFACE" >
<color r="1" g="0.89" b="0.53" a="1" />
<nrOfStructures value="384"/>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="0" />
<atomRef index="9" />
<atomRef index="10" />
<atomRef index="1" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="1" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="2" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="2" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="3" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="3" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="4" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="4" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="5" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="5" />
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="6" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="6" />
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="7" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="7" />
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="8" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="9" />
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="10" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="10" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="11" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="11" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="12" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="12" />
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="13" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="13" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="14" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="14" />
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="15" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="15" />
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="16" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="16" />
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="17" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="18" />
<atomRef index="27" />
<atomRef index="28" />
<atomRef index="19" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="19" />
<atomRef index="28" />
<atomRef index="29" />
<atomRef index="20" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="20" />
<atomRef index="29" />
<atomRef index="30" />
<atomRef index="21" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="21" />
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="22" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="22" />
<atomRef index="31" />
<atomRef index="32" />
<atomRef index="23" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="23" />
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="24" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="24" />
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="25" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="25" />
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="26" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="27" />
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="28" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="28" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="29" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="29" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="30" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="30" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="31" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="32" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="32" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="33" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="33" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="34" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="34" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="35" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="36" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="37" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="37" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="38" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="38" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="39" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="39" />
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="40" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="40" />
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="41" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="41" />
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="42" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="42" />
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="43" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="43" />
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="46" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="46" />
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="47" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="47" />
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="48" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="48" />
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="49" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="49" />
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="50" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="50" />
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="51" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="51" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="52" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="52" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="53" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="54" />
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="55" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="55" />
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="56" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="56" />
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="57" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="57" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="58" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="58" />
<atomRef index="67" />
<atomRef index="68" />
<atomRef index="59" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="59" />
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="60" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="60" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="61" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="61" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="62" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="63" />
<atomRef index="72" />
<atomRef index="73" />
<atomRef index="64" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="64" />
<atomRef index="73" />
<atomRef index="74" />
<atomRef index="65" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="65" />
<atomRef index="74" />
<atomRef index="75" />
<atomRef index="66" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="66" />
<atomRef index="75" />
<atomRef index="76" />
<atomRef index="67" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="67" />
<atomRef index="76" />
<atomRef index="77" />
<atomRef index="68" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="68" />
<atomRef index="77" />
<atomRef index="78" />
<atomRef index="69" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="69" />
<atomRef index="78" />
<atomRef index="79" />
<atomRef index="70" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="70" />
<atomRef index="79" />
<atomRef index="80" />
<atomRef index="71" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="648" />
<atomRef index="649" />
<atomRef index="658" />
<atomRef index="657" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="649" />
<atomRef index="650" />
<atomRef index="659" />
<atomRef index="658" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="650" />
<atomRef index="651" />
<atomRef index="660" />
<atomRef index="659" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="651" />
<atomRef index="652" />
<atomRef index="661" />
<atomRef index="660" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="652" />
<atomRef index="653" />
<atomRef index="662" />
<atomRef index="661" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="653" />
<atomRef index="654" />
<atomRef index="663" />
<atomRef index="662" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="654" />
<atomRef index="655" />
<atomRef index="664" />
<atomRef index="663" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="655" />
<atomRef index="656" />
<atomRef index="665" />
<atomRef index="664" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="657" />
<atomRef index="658" />
<atomRef index="667" />
<atomRef index="666" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="658" />
<atomRef index="659" />
<atomRef index="668" />
<atomRef index="667" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="659" />
<atomRef index="660" />
<atomRef index="669" />
<atomRef index="668" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="660" />
<atomRef index="661" />
<atomRef index="670" />
<atomRef index="669" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="661" />
<atomRef index="662" />
<atomRef index="671" />
<atomRef index="670" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="662" />
<atomRef index="663" />
<atomRef index="672" />
<atomRef index="671" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="663" />
<atomRef index="664" />
<atomRef index="673" />
<atomRef index="672" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="664" />
<atomRef index="665" />
<atomRef index="674" />
<atomRef index="673" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="666" />
<atomRef index="667" />
<atomRef index="676" />
<atomRef index="675" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="667" />
<atomRef index="668" />
<atomRef index="677" />
<atomRef index="676" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="668" />
<atomRef index="669" />
<atomRef index="678" />
<atomRef index="677" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="669" />
<atomRef index="670" />
<atomRef index="679" />
<atomRef index="678" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="670" />
<atomRef index="671" />
<atomRef index="680" />
<atomRef index="679" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="671" />
<atomRef index="672" />
<atomRef index="681" />
<atomRef index="680" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="672" />
<atomRef index="673" />
<atomRef index="682" />
<atomRef index="681" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="673" />
<atomRef index="674" />
<atomRef index="683" />
<atomRef index="682" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="675" />
<atomRef index="676" />
<atomRef index="685" />
<atomRef index="684" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="676" />
<atomRef index="677" />
<atomRef index="686" />
<atomRef index="685" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="677" />
<atomRef index="678" />
<atomRef index="687" />
<atomRef index="686" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="678" />
<atomRef index="679" />
<atomRef index="688" />
<atomRef index="687" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="679" />
<atomRef index="680" />
<atomRef index="689" />
<atomRef index="688" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="680" />
<atomRef index="681" />
<atomRef index="690" />
<atomRef index="689" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="681" />
<atomRef index="682" />
<atomRef index="691" />
<atomRef index="690" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="682" />
<atomRef index="683" />
<atomRef index="692" />
<atomRef index="691" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="684" />
<atomRef index="685" />
<atomRef index="694" />
<atomRef index="693" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="685" />
<atomRef index="686" />
<atomRef index="695" />
<atomRef index="694" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="686" />
<atomRef index="687" />
<atomRef index="696" />
<atomRef index="695" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="687" />
<atomRef index="688" />
<atomRef index="697" />
<atomRef index="696" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="688" />
<atomRef index="689" />
<atomRef index="698" />
<atomRef index="697" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="689" />
<atomRef index="690" />
<atomRef index="699" />
<atomRef index="698" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="690" />
<atomRef index="691" />
<atomRef index="700" />
<atomRef index="699" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="691" />
<atomRef index="692" />
<atomRef index="701" />
<atomRef index="700" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="693" />
<atomRef index="694" />
<atomRef index="703" />
<atomRef index="702" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="694" />
<atomRef index="695" />
<atomRef index="704" />
<atomRef index="703" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="695" />
<atomRef index="696" />
<atomRef index="705" />
<atomRef index="704" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="696" />
<atomRef index="697" />
<atomRef index="706" />
<atomRef index="705" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="697" />
<atomRef index="698" />
<atomRef index="707" />
<atomRef index="706" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="698" />
<atomRef index="699" />
<atomRef index="708" />
<atomRef index="707" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="699" />
<atomRef index="700" />
<atomRef index="709" />
<atomRef index="708" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="700" />
<atomRef index="701" />
<atomRef index="710" />
<atomRef index="709" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="702" />
<atomRef index="703" />
<atomRef index="712" />
<atomRef index="711" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="703" />
<atomRef index="704" />
<atomRef index="713" />
<atomRef index="712" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="704" />
<atomRef index="705" />
<atomRef index="714" />
<atomRef index="713" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="705" />
<atomRef index="706" />
<atomRef index="715" />
<atomRef index="714" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="706" />
<atomRef index="707" />
<atomRef index="716" />
<atomRef index="715" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="707" />
<atomRef index="708" />
<atomRef index="717" />
<atomRef index="716" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="708" />
<atomRef index="709" />
<atomRef index="718" />
<atomRef index="717" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="709" />
<atomRef index="710" />
<atomRef index="719" />
<atomRef index="718" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="711" />
<atomRef index="712" />
<atomRef index="721" />
<atomRef index="720" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="712" />
<atomRef index="713" />
<atomRef index="722" />
<atomRef index="721" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="713" />
<atomRef index="714" />
<atomRef index="723" />
<atomRef index="722" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="714" />
<atomRef index="715" />
<atomRef index="724" />
<atomRef index="723" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="715" />
<atomRef index="716" />
<atomRef index="725" />
<atomRef index="724" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="716" />
<atomRef index="717" />
<atomRef index="726" />
<atomRef index="725" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="717" />
<atomRef index="718" />
<atomRef index="727" />
<atomRef index="726" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="718" />
<atomRef index="719" />
<atomRef index="728" />
<atomRef index="727" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="0" />
<atomRef index="81" />
<atomRef index="90" />
<atomRef index="9" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="81" />
<atomRef index="162" />
<atomRef index="171" />
<atomRef index="90" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="162" />
<atomRef index="243" />
<atomRef index="252" />
<atomRef index="171" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="243" />
<atomRef index="324" />
<atomRef index="333" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="324" />
<atomRef index="405" />
<atomRef index="414" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="405" />
<atomRef index="486" />
<atomRef index="495" />
<atomRef index="414" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="486" />
<atomRef index="567" />
<atomRef index="576" />
<atomRef index="495" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="567" />
<atomRef index="648" />
<atomRef index="657" />
<atomRef index="576" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="9" />
<atomRef index="90" />
<atomRef index="99" />
<atomRef index="18" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="90" />
<atomRef index="171" />
<atomRef index="180" />
<atomRef index="99" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="171" />
<atomRef index="252" />
<atomRef index="261" />
<atomRef index="180" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="333" />
<atomRef index="342" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="333" />
<atomRef index="414" />
<atomRef index="423" />
<atomRef index="342" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="414" />
<atomRef index="495" />
<atomRef index="504" />
<atomRef index="423" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="495" />
<atomRef index="576" />
<atomRef index="585" />
<atomRef index="504" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="576" />
<atomRef index="657" />
<atomRef index="666" />
<atomRef index="585" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="18" />
<atomRef index="99" />
<atomRef index="108" />
<atomRef index="27" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="99" />
<atomRef index="180" />
<atomRef index="189" />
<atomRef index="108" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="180" />
<atomRef index="261" />
<atomRef index="270" />
<atomRef index="189" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="342" />
<atomRef index="351" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="342" />
<atomRef index="423" />
<atomRef index="432" />
<atomRef index="351" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="423" />
<atomRef index="504" />
<atomRef index="513" />
<atomRef index="432" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="504" />
<atomRef index="585" />
<atomRef index="594" />
<atomRef index="513" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="585" />
<atomRef index="666" />
<atomRef index="675" />
<atomRef index="594" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="27" />
<atomRef index="108" />
<atomRef index="117" />
<atomRef index="36" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="108" />
<atomRef index="189" />
<atomRef index="198" />
<atomRef index="117" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="189" />
<atomRef index="270" />
<atomRef index="279" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="270" />
<atomRef index="351" />
<atomRef index="360" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="351" />
<atomRef index="432" />
<atomRef index="441" />
<atomRef index="360" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="432" />
<atomRef index="513" />
<atomRef index="522" />
<atomRef index="441" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="513" />
<atomRef index="594" />
<atomRef index="603" />
<atomRef index="522" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="594" />
<atomRef index="675" />
<atomRef index="684" />
<atomRef index="603" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="36" />
<atomRef index="117" />
<atomRef index="126" />
<atomRef index="45" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="117" />
<atomRef index="198" />
<atomRef index="207" />
<atomRef index="126" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="198" />
<atomRef index="279" />
<atomRef index="288" />
<atomRef index="207" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="360" />
<atomRef index="369" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="360" />
<atomRef index="441" />
<atomRef index="450" />
<atomRef index="369" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="441" />
<atomRef index="522" />
<atomRef index="531" />
<atomRef index="450" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="522" />
<atomRef index="603" />
<atomRef index="612" />
<atomRef index="531" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="603" />
<atomRef index="684" />
<atomRef index="693" />
<atomRef index="612" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="126" />
<atomRef index="135" />
<atomRef index="54" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="126" />
<atomRef index="207" />
<atomRef index="216" />
<atomRef index="135" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="207" />
<atomRef index="288" />
<atomRef index="297" />
<atomRef index="216" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="369" />
<atomRef index="378" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="369" />
<atomRef index="450" />
<atomRef index="459" />
<atomRef index="378" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="450" />
<atomRef index="531" />
<atomRef index="540" />
<atomRef index="459" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="531" />
<atomRef index="612" />
<atomRef index="621" />
<atomRef index="540" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="612" />
<atomRef index="693" />
<atomRef index="702" />
<atomRef index="621" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="54" />
<atomRef index="135" />
<atomRef index="144" />
<atomRef index="63" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="135" />
<atomRef index="216" />
<atomRef index="225" />
<atomRef index="144" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="216" />
<atomRef index="297" />
<atomRef index="306" />
<atomRef index="225" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="378" />
<atomRef index="387" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="378" />
<atomRef index="459" />
<atomRef index="468" />
<atomRef index="387" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="459" />
<atomRef index="540" />
<atomRef index="549" />
<atomRef index="468" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="540" />
<atomRef index="621" />
<atomRef index="630" />
<atomRef index="549" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="621" />
<atomRef index="702" />
<atomRef index="711" />
<atomRef index="630" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="63" />
<atomRef index="144" />
<atomRef index="153" />
<atomRef index="72" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="144" />
<atomRef index="225" />
<atomRef index="234" />
<atomRef index="153" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="306" />
<atomRef index="315" />
<atomRef index="234" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="387" />
<atomRef index="396" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="387" />
<atomRef index="468" />
<atomRef index="477" />
<atomRef index="396" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="468" />
<atomRef index="549" />
<atomRef index="558" />
<atomRef index="477" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="549" />
<atomRef index="630" />
<atomRef index="639" />
<atomRef index="558" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="630" />
<atomRef index="711" />
<atomRef index="720" />
<atomRef index="639" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="8" />
<atomRef index="17" />
<atomRef index="98" />
<atomRef index="89" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="89" />
<atomRef index="98" />
<atomRef index="179" />
<atomRef index="170" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="170" />
<atomRef index="179" />
<atomRef index="260" />
<atomRef index="251" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="251" />
<atomRef index="260" />
<atomRef index="341" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="332" />
<atomRef index="341" />
<atomRef index="422" />
<atomRef index="413" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="413" />
<atomRef index="422" />
<atomRef index="503" />
<atomRef index="494" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="494" />
<atomRef index="503" />
<atomRef index="584" />
<atomRef index="575" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="575" />
<atomRef index="584" />
<atomRef index="665" />
<atomRef index="656" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="17" />
<atomRef index="26" />
<atomRef index="107" />
<atomRef index="98" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="98" />
<atomRef index="107" />
<atomRef index="188" />
<atomRef index="179" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="179" />
<atomRef index="188" />
<atomRef index="269" />
<atomRef index="260" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="260" />
<atomRef index="269" />
<atomRef index="350" />
<atomRef index="341" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="341" />
<atomRef index="350" />
<atomRef index="431" />
<atomRef index="422" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="422" />
<atomRef index="431" />
<atomRef index="512" />
<atomRef index="503" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="503" />
<atomRef index="512" />
<atomRef index="593" />
<atomRef index="584" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="584" />
<atomRef index="593" />
<atomRef index="674" />
<atomRef index="665" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="26" />
<atomRef index="35" />
<atomRef index="116" />
<atomRef index="107" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="107" />
<atomRef index="116" />
<atomRef index="197" />
<atomRef index="188" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="188" />
<atomRef index="197" />
<atomRef index="278" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="278" />
<atomRef index="359" />
<atomRef index="350" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="350" />
<atomRef index="359" />
<atomRef index="440" />
<atomRef index="431" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="431" />
<atomRef index="440" />
<atomRef index="521" />
<atomRef index="512" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="512" />
<atomRef index="521" />
<atomRef index="602" />
<atomRef index="593" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="593" />
<atomRef index="602" />
<atomRef index="683" />
<atomRef index="674" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="35" />
<atomRef index="44" />
<atomRef index="125" />
<atomRef index="116" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="116" />
<atomRef index="125" />
<atomRef index="206" />
<atomRef index="197" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="197" />
<atomRef index="206" />
<atomRef index="287" />
<atomRef index="278" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="287" />
<atomRef index="368" />
<atomRef index="359" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="359" />
<atomRef index="368" />
<atomRef index="449" />
<atomRef index="440" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="440" />
<atomRef index="449" />
<atomRef index="530" />
<atomRef index="521" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="521" />
<atomRef index="530" />
<atomRef index="611" />
<atomRef index="602" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="602" />
<atomRef index="611" />
<atomRef index="692" />
<atomRef index="683" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="44" />
<atomRef index="53" />
<atomRef index="134" />
<atomRef index="125" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="125" />
<atomRef index="134" />
<atomRef index="215" />
<atomRef index="206" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="206" />
<atomRef index="215" />
<atomRef index="296" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="296" />
<atomRef index="377" />
<atomRef index="368" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="368" />
<atomRef index="377" />
<atomRef index="458" />
<atomRef index="449" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="449" />
<atomRef index="458" />
<atomRef index="539" />
<atomRef index="530" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="530" />
<atomRef index="539" />
<atomRef index="620" />
<atomRef index="611" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="611" />
<atomRef index="620" />
<atomRef index="701" />
<atomRef index="692" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="53" />
<atomRef index="62" />
<atomRef index="143" />
<atomRef index="134" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="134" />
<atomRef index="143" />
<atomRef index="224" />
<atomRef index="215" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="215" />
<atomRef index="224" />
<atomRef index="305" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="296" />
<atomRef index="305" />
<atomRef index="386" />
<atomRef index="377" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="377" />
<atomRef index="386" />
<atomRef index="467" />
<atomRef index="458" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="458" />
<atomRef index="467" />
<atomRef index="548" />
<atomRef index="539" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="539" />
<atomRef index="548" />
<atomRef index="629" />
<atomRef index="620" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="620" />
<atomRef index="629" />
<atomRef index="710" />
<atomRef index="701" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="62" />
<atomRef index="71" />
<atomRef index="152" />
<atomRef index="143" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="143" />
<atomRef index="152" />
<atomRef index="233" />
<atomRef index="224" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="224" />
<atomRef index="233" />
<atomRef index="314" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="305" />
<atomRef index="314" />
<atomRef index="395" />
<atomRef index="386" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="386" />
<atomRef index="395" />
<atomRef index="476" />
<atomRef index="467" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="467" />
<atomRef index="476" />
<atomRef index="557" />
<atomRef index="548" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="548" />
<atomRef index="557" />
<atomRef index="638" />
<atomRef index="629" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="629" />
<atomRef index="638" />
<atomRef index="719" />
<atomRef index="710" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="71" />
<atomRef index="80" />
<atomRef index="161" />
<atomRef index="152" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="152" />
<atomRef index="161" />
<atomRef index="242" />
<atomRef index="233" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="233" />
<atomRef index="242" />
<atomRef index="323" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="323" />
<atomRef index="404" />
<atomRef index="395" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="395" />
<atomRef index="404" />
<atomRef index="485" />
<atomRef index="476" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="476" />
<atomRef index="485" />
<atomRef index="566" />
<atomRef index="557" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="557" />
<atomRef index="566" />
<atomRef index="647" />
<atomRef index="638" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="638" />
<atomRef index="647" />
<atomRef index="728" />
<atomRef index="719" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="82" />
<atomRef index="81" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="83" />
<atomRef index="82" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="2" />
<atomRef index="3" />
<atomRef index="84" />
<atomRef index="83" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="85" />
<atomRef index="84" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="86" />
<atomRef index="85" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="87" />
<atomRef index="86" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="88" />
<atomRef index="87" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="89" />
<atomRef index="88" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="163" />
<atomRef index="162" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="164" />
<atomRef index="163" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="165" />
<atomRef index="164" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="166" />
<atomRef index="165" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="167" />
<atomRef index="166" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="168" />
<atomRef index="167" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="169" />
<atomRef index="168" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="170" />
<atomRef index="169" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="244" />
<atomRef index="243" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="245" />
<atomRef index="244" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="246" />
<atomRef index="245" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="247" />
<atomRef index="246" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="248" />
<atomRef index="247" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="249" />
<atomRef index="248" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="250" />
<atomRef index="249" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="251" />
<atomRef index="250" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="325" />
<atomRef index="324" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="326" />
<atomRef index="325" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="327" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="328" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="329" />
<atomRef index="328" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="330" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="331" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="250" />
<atomRef index="251" />
<atomRef index="332" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="406" />
<atomRef index="405" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="407" />
<atomRef index="406" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="326" />
<atomRef index="327" />
<atomRef index="408" />
<atomRef index="407" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="327" />
<atomRef index="328" />
<atomRef index="409" />
<atomRef index="408" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="328" />
<atomRef index="329" />
<atomRef index="410" />
<atomRef index="409" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="329" />
<atomRef index="330" />
<atomRef index="411" />
<atomRef index="410" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="330" />
<atomRef index="331" />
<atomRef index="412" />
<atomRef index="411" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="331" />
<atomRef index="332" />
<atomRef index="413" />
<atomRef index="412" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="405" />
<atomRef index="406" />
<atomRef index="487" />
<atomRef index="486" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="406" />
<atomRef index="407" />
<atomRef index="488" />
<atomRef index="487" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="407" />
<atomRef index="408" />
<atomRef index="489" />
<atomRef index="488" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="408" />
<atomRef index="409" />
<atomRef index="490" />
<atomRef index="489" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="409" />
<atomRef index="410" />
<atomRef index="491" />
<atomRef index="490" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="410" />
<atomRef index="411" />
<atomRef index="492" />
<atomRef index="491" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="411" />
<atomRef index="412" />
<atomRef index="493" />
<atomRef index="492" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="412" />
<atomRef index="413" />
<atomRef index="494" />
<atomRef index="493" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="486" />
<atomRef index="487" />
<atomRef index="568" />
<atomRef index="567" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="487" />
<atomRef index="488" />
<atomRef index="569" />
<atomRef index="568" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="488" />
<atomRef index="489" />
<atomRef index="570" />
<atomRef index="569" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="489" />
<atomRef index="490" />
<atomRef index="571" />
<atomRef index="570" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="490" />
<atomRef index="491" />
<atomRef index="572" />
<atomRef index="571" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="491" />
<atomRef index="492" />
<atomRef index="573" />
<atomRef index="572" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="492" />
<atomRef index="493" />
<atomRef index="574" />
<atomRef index="573" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="493" />
<atomRef index="494" />
<atomRef index="575" />
<atomRef index="574" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="567" />
<atomRef index="568" />
<atomRef index="649" />
<atomRef index="648" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="568" />
<atomRef index="569" />
<atomRef index="650" />
<atomRef index="649" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="569" />
<atomRef index="570" />
<atomRef index="651" />
<atomRef index="650" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="570" />
<atomRef index="571" />
<atomRef index="652" />
<atomRef index="651" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="571" />
<atomRef index="572" />
<atomRef index="653" />
<atomRef index="652" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="572" />
<atomRef index="573" />
<atomRef index="654" />
<atomRef index="653" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="573" />
<atomRef index="574" />
<atomRef index="655" />
<atomRef index="654" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="574" />
<atomRef index="575" />
<atomRef index="656" />
<atomRef index="655" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="72" />
<atomRef index="153" />
<atomRef index="154" />
<atomRef index="73" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="73" />
<atomRef index="154" />
<atomRef index="155" />
<atomRef index="74" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="74" />
<atomRef index="155" />
<atomRef index="156" />
<atomRef index="75" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="75" />
<atomRef index="156" />
<atomRef index="157" />
<atomRef index="76" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="76" />
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="77" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="77" />
<atomRef index="158" />
<atomRef index="159" />
<atomRef index="78" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="78" />
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="79" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="79" />
<atomRef index="160" />
<atomRef index="161" />
<atomRef index="80" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="234" />
<atomRef index="235" />
<atomRef index="154" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="154" />
<atomRef index="235" />
<atomRef index="236" />
<atomRef index="155" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="155" />
<atomRef index="236" />
<atomRef index="237" />
<atomRef index="156" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="156" />
<atomRef index="237" />
<atomRef index="238" />
<atomRef index="157" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="157" />
<atomRef index="238" />
<atomRef index="239" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="158" />
<atomRef index="239" />
<atomRef index="240" />
<atomRef index="159" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="159" />
<atomRef index="240" />
<atomRef index="241" />
<atomRef index="160" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="160" />
<atomRef index="241" />
<atomRef index="242" />
<atomRef index="161" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="234" />
<atomRef index="315" />
<atomRef index="316" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="235" />
<atomRef index="316" />
<atomRef index="317" />
<atomRef index="236" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="236" />
<atomRef index="317" />
<atomRef index="318" />
<atomRef index="237" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="237" />
<atomRef index="318" />
<atomRef index="319" />
<atomRef index="238" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="238" />
<atomRef index="319" />
<atomRef index="320" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="239" />
<atomRef index="320" />
<atomRef index="321" />
<atomRef index="240" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="240" />
<atomRef index="321" />
<atomRef index="322" />
<atomRef index="241" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="241" />
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="242" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="396" />
<atomRef index="397" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="316" />
<atomRef index="397" />
<atomRef index="398" />
<atomRef index="317" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="317" />
<atomRef index="398" />
<atomRef index="399" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="318" />
<atomRef index="399" />
<atomRef index="400" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="319" />
<atomRef index="400" />
<atomRef index="401" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="320" />
<atomRef index="401" />
<atomRef index="402" />
<atomRef index="321" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="321" />
<atomRef index="402" />
<atomRef index="403" />
<atomRef index="322" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="322" />
<atomRef index="403" />
<atomRef index="404" />
<atomRef index="323" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="396" />
<atomRef index="477" />
<atomRef index="478" />
<atomRef index="397" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="397" />
<atomRef index="478" />
<atomRef index="479" />
<atomRef index="398" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="398" />
<atomRef index="479" />
<atomRef index="480" />
<atomRef index="399" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="399" />
<atomRef index="480" />
<atomRef index="481" />
<atomRef index="400" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="400" />
<atomRef index="481" />
<atomRef index="482" />
<atomRef index="401" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="401" />
<atomRef index="482" />
<atomRef index="483" />
<atomRef index="402" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="402" />
<atomRef index="483" />
<atomRef index="484" />
<atomRef index="403" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="403" />
<atomRef index="484" />
<atomRef index="485" />
<atomRef index="404" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="477" />
<atomRef index="558" />
<atomRef index="559" />
<atomRef index="478" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="478" />
<atomRef index="559" />
<atomRef index="560" />
<atomRef index="479" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="479" />
<atomRef index="560" />
<atomRef index="561" />
<atomRef index="480" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="480" />
<atomRef index="561" />
<atomRef index="562" />
<atomRef index="481" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="481" />
<atomRef index="562" />
<atomRef index="563" />
<atomRef index="482" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="482" />
<atomRef index="563" />
<atomRef index="564" />
<atomRef index="483" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="483" />
<atomRef index="564" />
<atomRef index="565" />
<atomRef index="484" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="484" />
<atomRef index="565" />
<atomRef index="566" />
<atomRef index="485" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="558" />
<atomRef index="639" />
<atomRef index="640" />
<atomRef index="559" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="559" />
<atomRef index="640" />
<atomRef index="641" />
<atomRef index="560" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="560" />
<atomRef index="641" />
<atomRef index="642" />
<atomRef index="561" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="561" />
<atomRef index="642" />
<atomRef index="643" />
<atomRef index="562" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="562" />
<atomRef index="643" />
<atomRef index="644" />
<atomRef index="563" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="563" />
<atomRef index="644" />
<atomRef index="645" />
<atomRef index="564" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="564" />
<atomRef index="645" />
<atomRef index="646" />
<atomRef index="565" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="565" />
<atomRef index="646" />
<atomRef index="647" />
<atomRef index="566" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="639" />
<atomRef index="720" />
<atomRef index="721" />
<atomRef index="640" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="640" />
<atomRef index="721" />
<atomRef index="722" />
<atomRef index="641" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="641" />
<atomRef index="722" />
<atomRef index="723" />
<atomRef index="642" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="642" />
<atomRef index="723" />
<atomRef index="724" />
<atomRef index="643" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="643" />
<atomRef index="724" />
<atomRef index="725" />
<atomRef index="644" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="644" />
<atomRef index="725" />
<atomRef index="726" />
<atomRef index="645" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="645" />
<atomRef index="726" />
<atomRef index="727" />
<atomRef index="646" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="646" />
<atomRef index="727" />
<atomRef index="728" />
<atomRef index="647" />
</cell>
</structuralComponent>
</multiComponent>
<structuralComponent  name="Elements"  externW="0"  mass="0"  viscosityW="0" >
<color r="0.8" g="0.8" b="0.2" a="1" />
<nrOfStructures value="512"/>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="10" />
<atomRef index="9" />
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="91" />
<atomRef index="90" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="11" />
<atomRef index="10" />
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="92" />
<atomRef index="91" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="2" />
<atomRef index="3" />
<atomRef index="12" />
<atomRef index="11" />
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="93" />
<atomRef index="92" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="13" />
<atomRef index="12" />
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="94" />
<atomRef index="93" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="14" />
<atomRef index="13" />
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="95" />
<atomRef index="94" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="15" />
<atomRef index="14" />
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="96" />
<atomRef index="95" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="16" />
<atomRef index="15" />
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="97" />
<atomRef index="96" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="17" />
<atomRef index="16" />
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="98" />
<atomRef index="97" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="9" />
<atomRef index="10" />
<atomRef index="19" />
<atomRef index="18" />
<atomRef index="90" />
<atomRef index="91" />
<atomRef index="100" />
<atomRef index="99" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="20" />
<atomRef index="19" />
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="101" />
<atomRef index="100" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="21" />
<atomRef index="20" />
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="102" />
<atomRef index="101" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="22" />
<atomRef index="21" />
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="103" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="23" />
<atomRef index="22" />
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="104" />
<atomRef index="103" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="24" />
<atomRef index="23" />
<atomRef index="95" />
<atomRef index="96" />
<atomRef index="105" />
<atomRef index="104" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="25" />
<atomRef index="24" />
<atomRef index="96" />
<atomRef index="97" />
<atomRef index="106" />
<atomRef index="105" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="26" />
<atomRef index="25" />
<atomRef index="97" />
<atomRef index="98" />
<atomRef index="107" />
<atomRef index="106" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="28" />
<atomRef index="27" />
<atomRef index="99" />
<atomRef index="100" />
<atomRef index="109" />
<atomRef index="108" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="29" />
<atomRef index="28" />
<atomRef index="100" />
<atomRef index="101" />
<atomRef index="110" />
<atomRef index="109" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="30" />
<atomRef index="29" />
<atomRef index="101" />
<atomRef index="102" />
<atomRef index="111" />
<atomRef index="110" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="31" />
<atomRef index="30" />
<atomRef index="102" />
<atomRef index="103" />
<atomRef index="112" />
<atomRef index="111" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="32" />
<atomRef index="31" />
<atomRef index="103" />
<atomRef index="104" />
<atomRef index="113" />
<atomRef index="112" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="33" />
<atomRef index="32" />
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="114" />
<atomRef index="113" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="34" />
<atomRef index="33" />
<atomRef index="105" />
<atomRef index="106" />
<atomRef index="115" />
<atomRef index="114" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="35" />
<atomRef index="34" />
<atomRef index="106" />
<atomRef index="107" />
<atomRef index="116" />
<atomRef index="115" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="27" />
<atomRef index="28" />
<atomRef index="37" />
<atomRef index="36" />
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="118" />
<atomRef index="117" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="28" />
<atomRef index="29" />
<atomRef index="38" />
<atomRef index="37" />
<atomRef index="109" />
<atomRef index="110" />
<atomRef index="119" />
<atomRef index="118" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="29" />
<atomRef index="30" />
<atomRef index="39" />
<atomRef index="38" />
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="120" />
<atomRef index="119" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="40" />
<atomRef index="39" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="121" />
<atomRef index="120" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="31" />
<atomRef index="32" />
<atomRef index="41" />
<atomRef index="40" />
<atomRef index="112" />
<atomRef index="113" />
<atomRef index="122" />
<atomRef index="121" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="42" />
<atomRef index="41" />
<atomRef index="113" />
<atomRef index="114" />
<atomRef index="123" />
<atomRef index="122" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="43" />
<atomRef index="42" />
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="124" />
<atomRef index="123" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="44" />
<atomRef index="43" />
<atomRef index="115" />
<atomRef index="116" />
<atomRef index="125" />
<atomRef index="124" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="46" />
<atomRef index="45" />
<atomRef index="117" />
<atomRef index="118" />
<atomRef index="127" />
<atomRef index="126" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="47" />
<atomRef index="46" />
<atomRef index="118" />
<atomRef index="119" />
<atomRef index="128" />
<atomRef index="127" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="48" />
<atomRef index="47" />
<atomRef index="119" />
<atomRef index="120" />
<atomRef index="129" />
<atomRef index="128" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="49" />
<atomRef index="48" />
<atomRef index="120" />
<atomRef index="121" />
<atomRef index="130" />
<atomRef index="129" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="50" />
<atomRef index="49" />
<atomRef index="121" />
<atomRef index="122" />
<atomRef index="131" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="51" />
<atomRef index="50" />
<atomRef index="122" />
<atomRef index="123" />
<atomRef index="132" />
<atomRef index="131" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="52" />
<atomRef index="51" />
<atomRef index="123" />
<atomRef index="124" />
<atomRef index="133" />
<atomRef index="132" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="53" />
<atomRef index="52" />
<atomRef index="124" />
<atomRef index="125" />
<atomRef index="134" />
<atomRef index="133" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="55" />
<atomRef index="54" />
<atomRef index="126" />
<atomRef index="127" />
<atomRef index="136" />
<atomRef index="135" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="56" />
<atomRef index="55" />
<atomRef index="127" />
<atomRef index="128" />
<atomRef index="137" />
<atomRef index="136" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="57" />
<atomRef index="56" />
<atomRef index="128" />
<atomRef index="129" />
<atomRef index="138" />
<atomRef index="137" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="58" />
<atomRef index="57" />
<atomRef index="129" />
<atomRef index="130" />
<atomRef index="139" />
<atomRef index="138" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="59" />
<atomRef index="58" />
<atomRef index="130" />
<atomRef index="131" />
<atomRef index="140" />
<atomRef index="139" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="60" />
<atomRef index="59" />
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="141" />
<atomRef index="140" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="61" />
<atomRef index="60" />
<atomRef index="132" />
<atomRef index="133" />
<atomRef index="142" />
<atomRef index="141" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="62" />
<atomRef index="61" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="143" />
<atomRef index="142" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="64" />
<atomRef index="63" />
<atomRef index="135" />
<atomRef index="136" />
<atomRef index="145" />
<atomRef index="144" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="65" />
<atomRef index="64" />
<atomRef index="136" />
<atomRef index="137" />
<atomRef index="146" />
<atomRef index="145" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="66" />
<atomRef index="65" />
<atomRef index="137" />
<atomRef index="138" />
<atomRef index="147" />
<atomRef index="146" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="67" />
<atomRef index="66" />
<atomRef index="138" />
<atomRef index="139" />
<atomRef index="148" />
<atomRef index="147" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="68" />
<atomRef index="67" />
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="149" />
<atomRef index="148" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="69" />
<atomRef index="68" />
<atomRef index="140" />
<atomRef index="141" />
<atomRef index="150" />
<atomRef index="149" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="70" />
<atomRef index="69" />
<atomRef index="141" />
<atomRef index="142" />
<atomRef index="151" />
<atomRef index="150" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="71" />
<atomRef index="70" />
<atomRef index="142" />
<atomRef index="143" />
<atomRef index="152" />
<atomRef index="151" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="73" />
<atomRef index="72" />
<atomRef index="144" />
<atomRef index="145" />
<atomRef index="154" />
<atomRef index="153" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="74" />
<atomRef index="73" />
<atomRef index="145" />
<atomRef index="146" />
<atomRef index="155" />
<atomRef index="154" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="75" />
<atomRef index="74" />
<atomRef index="146" />
<atomRef index="147" />
<atomRef index="156" />
<atomRef index="155" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="76" />
<atomRef index="75" />
<atomRef index="147" />
<atomRef index="148" />
<atomRef index="157" />
<atomRef index="156" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="67" />
<atomRef index="68" />
<atomRef index="77" />
<atomRef index="76" />
<atomRef index="148" />
<atomRef index="149" />
<atomRef index="158" />
<atomRef index="157" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="78" />
<atomRef index="77" />
<atomRef index="149" />
<atomRef index="150" />
<atomRef index="159" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="79" />
<atomRef index="78" />
<atomRef index="150" />
<atomRef index="151" />
<atomRef index="160" />
<atomRef index="159" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="80" />
<atomRef index="79" />
<atomRef index="151" />
<atomRef index="152" />
<atomRef index="161" />
<atomRef index="160" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="91" />
<atomRef index="90" />
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="172" />
<atomRef index="171" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="92" />
<atomRef index="91" />
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="173" />
<atomRef index="172" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="93" />
<atomRef index="92" />
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="174" />
<atomRef index="173" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="94" />
<atomRef index="93" />
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="175" />
<atomRef index="174" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="95" />
<atomRef index="94" />
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="176" />
<atomRef index="175" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="96" />
<atomRef index="95" />
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="177" />
<atomRef index="176" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="97" />
<atomRef index="96" />
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="178" />
<atomRef index="177" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="98" />
<atomRef index="97" />
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="179" />
<atomRef index="178" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="90" />
<atomRef index="91" />
<atomRef index="100" />
<atomRef index="99" />
<atomRef index="171" />
<atomRef index="172" />
<atomRef index="181" />
<atomRef index="180" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="101" />
<atomRef index="100" />
<atomRef index="172" />
<atomRef index="173" />
<atomRef index="182" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="102" />
<atomRef index="101" />
<atomRef index="173" />
<atomRef index="174" />
<atomRef index="183" />
<atomRef index="182" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="103" />
<atomRef index="102" />
<atomRef index="174" />
<atomRef index="175" />
<atomRef index="184" />
<atomRef index="183" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="104" />
<atomRef index="103" />
<atomRef index="175" />
<atomRef index="176" />
<atomRef index="185" />
<atomRef index="184" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="95" />
<atomRef index="96" />
<atomRef index="105" />
<atomRef index="104" />
<atomRef index="176" />
<atomRef index="177" />
<atomRef index="186" />
<atomRef index="185" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="96" />
<atomRef index="97" />
<atomRef index="106" />
<atomRef index="105" />
<atomRef index="177" />
<atomRef index="178" />
<atomRef index="187" />
<atomRef index="186" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="97" />
<atomRef index="98" />
<atomRef index="107" />
<atomRef index="106" />
<atomRef index="178" />
<atomRef index="179" />
<atomRef index="188" />
<atomRef index="187" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="99" />
<atomRef index="100" />
<atomRef index="109" />
<atomRef index="108" />
<atomRef index="180" />
<atomRef index="181" />
<atomRef index="190" />
<atomRef index="189" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="100" />
<atomRef index="101" />
<atomRef index="110" />
<atomRef index="109" />
<atomRef index="181" />
<atomRef index="182" />
<atomRef index="191" />
<atomRef index="190" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="101" />
<atomRef index="102" />
<atomRef index="111" />
<atomRef index="110" />
<atomRef index="182" />
<atomRef index="183" />
<atomRef index="192" />
<atomRef index="191" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="102" />
<atomRef index="103" />
<atomRef index="112" />
<atomRef index="111" />
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="193" />
<atomRef index="192" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="103" />
<atomRef index="104" />
<atomRef index="113" />
<atomRef index="112" />
<atomRef index="184" />
<atomRef index="185" />
<atomRef index="194" />
<atomRef index="193" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="114" />
<atomRef index="113" />
<atomRef index="185" />
<atomRef index="186" />
<atomRef index="195" />
<atomRef index="194" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="105" />
<atomRef index="106" />
<atomRef index="115" />
<atomRef index="114" />
<atomRef index="186" />
<atomRef index="187" />
<atomRef index="196" />
<atomRef index="195" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="106" />
<atomRef index="107" />
<atomRef index="116" />
<atomRef index="115" />
<atomRef index="187" />
<atomRef index="188" />
<atomRef index="197" />
<atomRef index="196" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="118" />
<atomRef index="117" />
<atomRef index="189" />
<atomRef index="190" />
<atomRef index="199" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="109" />
<atomRef index="110" />
<atomRef index="119" />
<atomRef index="118" />
<atomRef index="190" />
<atomRef index="191" />
<atomRef index="200" />
<atomRef index="199" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="120" />
<atomRef index="119" />
<atomRef index="191" />
<atomRef index="192" />
<atomRef index="201" />
<atomRef index="200" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="121" />
<atomRef index="120" />
<atomRef index="192" />
<atomRef index="193" />
<atomRef index="202" />
<atomRef index="201" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="112" />
<atomRef index="113" />
<atomRef index="122" />
<atomRef index="121" />
<atomRef index="193" />
<atomRef index="194" />
<atomRef index="203" />
<atomRef index="202" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="113" />
<atomRef index="114" />
<atomRef index="123" />
<atomRef index="122" />
<atomRef index="194" />
<atomRef index="195" />
<atomRef index="204" />
<atomRef index="203" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="124" />
<atomRef index="123" />
<atomRef index="195" />
<atomRef index="196" />
<atomRef index="205" />
<atomRef index="204" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="115" />
<atomRef index="116" />
<atomRef index="125" />
<atomRef index="124" />
<atomRef index="196" />
<atomRef index="197" />
<atomRef index="206" />
<atomRef index="205" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="117" />
<atomRef index="118" />
<atomRef index="127" />
<atomRef index="126" />
<atomRef index="198" />
<atomRef index="199" />
<atomRef index="208" />
<atomRef index="207" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="118" />
<atomRef index="119" />
<atomRef index="128" />
<atomRef index="127" />
<atomRef index="199" />
<atomRef index="200" />
<atomRef index="209" />
<atomRef index="208" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="119" />
<atomRef index="120" />
<atomRef index="129" />
<atomRef index="128" />
<atomRef index="200" />
<atomRef index="201" />
<atomRef index="210" />
<atomRef index="209" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="120" />
<atomRef index="121" />
<atomRef index="130" />
<atomRef index="129" />
<atomRef index="201" />
<atomRef index="202" />
<atomRef index="211" />
<atomRef index="210" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="121" />
<atomRef index="122" />
<atomRef index="131" />
<atomRef index="130" />
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="212" />
<atomRef index="211" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="122" />
<atomRef index="123" />
<atomRef index="132" />
<atomRef index="131" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="213" />
<atomRef index="212" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="123" />
<atomRef index="124" />
<atomRef index="133" />
<atomRef index="132" />
<atomRef index="204" />
<atomRef index="205" />
<atomRef index="214" />
<atomRef index="213" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="124" />
<atomRef index="125" />
<atomRef index="134" />
<atomRef index="133" />
<atomRef index="205" />
<atomRef index="206" />
<atomRef index="215" />
<atomRef index="214" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="126" />
<atomRef index="127" />
<atomRef index="136" />
<atomRef index="135" />
<atomRef index="207" />
<atomRef index="208" />
<atomRef index="217" />
<atomRef index="216" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="127" />
<atomRef index="128" />
<atomRef index="137" />
<atomRef index="136" />
<atomRef index="208" />
<atomRef index="209" />
<atomRef index="218" />
<atomRef index="217" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="128" />
<atomRef index="129" />
<atomRef index="138" />
<atomRef index="137" />
<atomRef index="209" />
<atomRef index="210" />
<atomRef index="219" />
<atomRef index="218" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="129" />
<atomRef index="130" />
<atomRef index="139" />
<atomRef index="138" />
<atomRef index="210" />
<atomRef index="211" />
<atomRef index="220" />
<atomRef index="219" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="130" />
<atomRef index="131" />
<atomRef index="140" />
<atomRef index="139" />
<atomRef index="211" />
<atomRef index="212" />
<atomRef index="221" />
<atomRef index="220" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="141" />
<atomRef index="140" />
<atomRef index="212" />
<atomRef index="213" />
<atomRef index="222" />
<atomRef index="221" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="132" />
<atomRef index="133" />
<atomRef index="142" />
<atomRef index="141" />
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="223" />
<atomRef index="222" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="143" />
<atomRef index="142" />
<atomRef index="214" />
<atomRef index="215" />
<atomRef index="224" />
<atomRef index="223" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="135" />
<atomRef index="136" />
<atomRef index="145" />
<atomRef index="144" />
<atomRef index="216" />
<atomRef index="217" />
<atomRef index="226" />
<atomRef index="225" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="136" />
<atomRef index="137" />
<atomRef index="146" />
<atomRef index="145" />
<atomRef index="217" />
<atomRef index="218" />
<atomRef index="227" />
<atomRef index="226" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="137" />
<atomRef index="138" />
<atomRef index="147" />
<atomRef index="146" />
<atomRef index="218" />
<atomRef index="219" />
<atomRef index="228" />
<atomRef index="227" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="138" />
<atomRef index="139" />
<atomRef index="148" />
<atomRef index="147" />
<atomRef index="219" />
<atomRef index="220" />
<atomRef index="229" />
<atomRef index="228" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="149" />
<atomRef index="148" />
<atomRef index="220" />
<atomRef index="221" />
<atomRef index="230" />
<atomRef index="229" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="140" />
<atomRef index="141" />
<atomRef index="150" />
<atomRef index="149" />
<atomRef index="221" />
<atomRef index="222" />
<atomRef index="231" />
<atomRef index="230" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="141" />
<atomRef index="142" />
<atomRef index="151" />
<atomRef index="150" />
<atomRef index="222" />
<atomRef index="223" />
<atomRef index="232" />
<atomRef index="231" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="142" />
<atomRef index="143" />
<atomRef index="152" />
<atomRef index="151" />
<atomRef index="223" />
<atomRef index="224" />
<atomRef index="233" />
<atomRef index="232" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="144" />
<atomRef index="145" />
<atomRef index="154" />
<atomRef index="153" />
<atomRef index="225" />
<atomRef index="226" />
<atomRef index="235" />
<atomRef index="234" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="145" />
<atomRef index="146" />
<atomRef index="155" />
<atomRef index="154" />
<atomRef index="226" />
<atomRef index="227" />
<atomRef index="236" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="146" />
<atomRef index="147" />
<atomRef index="156" />
<atomRef index="155" />
<atomRef index="227" />
<atomRef index="228" />
<atomRef index="237" />
<atomRef index="236" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="147" />
<atomRef index="148" />
<atomRef index="157" />
<atomRef index="156" />
<atomRef index="228" />
<atomRef index="229" />
<atomRef index="238" />
<atomRef index="237" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="148" />
<atomRef index="149" />
<atomRef index="158" />
<atomRef index="157" />
<atomRef index="229" />
<atomRef index="230" />
<atomRef index="239" />
<atomRef index="238" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="149" />
<atomRef index="150" />
<atomRef index="159" />
<atomRef index="158" />
<atomRef index="230" />
<atomRef index="231" />
<atomRef index="240" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="150" />
<atomRef index="151" />
<atomRef index="160" />
<atomRef index="159" />
<atomRef index="231" />
<atomRef index="232" />
<atomRef index="241" />
<atomRef index="240" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="151" />
<atomRef index="152" />
<atomRef index="161" />
<atomRef index="160" />
<atomRef index="232" />
<atomRef index="233" />
<atomRef index="242" />
<atomRef index="241" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="172" />
<atomRef index="171" />
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="253" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="173" />
<atomRef index="172" />
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="254" />
<atomRef index="253" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="174" />
<atomRef index="173" />
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="255" />
<atomRef index="254" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="175" />
<atomRef index="174" />
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="256" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="176" />
<atomRef index="175" />
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="257" />
<atomRef index="256" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="177" />
<atomRef index="176" />
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="258" />
<atomRef index="257" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="178" />
<atomRef index="177" />
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="259" />
<atomRef index="258" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="179" />
<atomRef index="178" />
<atomRef index="250" />
<atomRef index="251" />
<atomRef index="260" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="171" />
<atomRef index="172" />
<atomRef index="181" />
<atomRef index="180" />
<atomRef index="252" />
<atomRef index="253" />
<atomRef index="262" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="172" />
<atomRef index="173" />
<atomRef index="182" />
<atomRef index="181" />
<atomRef index="253" />
<atomRef index="254" />
<atomRef index="263" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="173" />
<atomRef index="174" />
<atomRef index="183" />
<atomRef index="182" />
<atomRef index="254" />
<atomRef index="255" />
<atomRef index="264" />
<atomRef index="263" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="174" />
<atomRef index="175" />
<atomRef index="184" />
<atomRef index="183" />
<atomRef index="255" />
<atomRef index="256" />
<atomRef index="265" />
<atomRef index="264" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="175" />
<atomRef index="176" />
<atomRef index="185" />
<atomRef index="184" />
<atomRef index="256" />
<atomRef index="257" />
<atomRef index="266" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="176" />
<atomRef index="177" />
<atomRef index="186" />
<atomRef index="185" />
<atomRef index="257" />
<atomRef index="258" />
<atomRef index="267" />
<atomRef index="266" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="177" />
<atomRef index="178" />
<atomRef index="187" />
<atomRef index="186" />
<atomRef index="258" />
<atomRef index="259" />
<atomRef index="268" />
<atomRef index="267" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="178" />
<atomRef index="179" />
<atomRef index="188" />
<atomRef index="187" />
<atomRef index="259" />
<atomRef index="260" />
<atomRef index="269" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="180" />
<atomRef index="181" />
<atomRef index="190" />
<atomRef index="189" />
<atomRef index="261" />
<atomRef index="262" />
<atomRef index="271" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="181" />
<atomRef index="182" />
<atomRef index="191" />
<atomRef index="190" />
<atomRef index="262" />
<atomRef index="263" />
<atomRef index="272" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="182" />
<atomRef index="183" />
<atomRef index="192" />
<atomRef index="191" />
<atomRef index="263" />
<atomRef index="264" />
<atomRef index="273" />
<atomRef index="272" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="193" />
<atomRef index="192" />
<atomRef index="264" />
<atomRef index="265" />
<atomRef index="274" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="184" />
<atomRef index="185" />
<atomRef index="194" />
<atomRef index="193" />
<atomRef index="265" />
<atomRef index="266" />
<atomRef index="275" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="185" />
<atomRef index="186" />
<atomRef index="195" />
<atomRef index="194" />
<atomRef index="266" />
<atomRef index="267" />
<atomRef index="276" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="186" />
<atomRef index="187" />
<atomRef index="196" />
<atomRef index="195" />
<atomRef index="267" />
<atomRef index="268" />
<atomRef index="277" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="187" />
<atomRef index="188" />
<atomRef index="197" />
<atomRef index="196" />
<atomRef index="268" />
<atomRef index="269" />
<atomRef index="278" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="189" />
<atomRef index="190" />
<atomRef index="199" />
<atomRef index="198" />
<atomRef index="270" />
<atomRef index="271" />
<atomRef index="280" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="190" />
<atomRef index="191" />
<atomRef index="200" />
<atomRef index="199" />
<atomRef index="271" />
<atomRef index="272" />
<atomRef index="281" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="191" />
<atomRef index="192" />
<atomRef index="201" />
<atomRef index="200" />
<atomRef index="272" />
<atomRef index="273" />
<atomRef index="282" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="192" />
<atomRef index="193" />
<atomRef index="202" />
<atomRef index="201" />
<atomRef index="273" />
<atomRef index="274" />
<atomRef index="283" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="193" />
<atomRef index="194" />
<atomRef index="203" />
<atomRef index="202" />
<atomRef index="274" />
<atomRef index="275" />
<atomRef index="284" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="194" />
<atomRef index="195" />
<atomRef index="204" />
<atomRef index="203" />
<atomRef index="275" />
<atomRef index="276" />
<atomRef index="285" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="195" />
<atomRef index="196" />
<atomRef index="205" />
<atomRef index="204" />
<atomRef index="276" />
<atomRef index="277" />
<atomRef index="286" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="196" />
<atomRef index="197" />
<atomRef index="206" />
<atomRef index="205" />
<atomRef index="277" />
<atomRef index="278" />
<atomRef index="287" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="198" />
<atomRef index="199" />
<atomRef index="208" />
<atomRef index="207" />
<atomRef index="279" />
<atomRef index="280" />
<atomRef index="289" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="199" />
<atomRef index="200" />
<atomRef index="209" />
<atomRef index="208" />
<atomRef index="280" />
<atomRef index="281" />
<atomRef index="290" />
<atomRef index="289" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="200" />
<atomRef index="201" />
<atomRef index="210" />
<atomRef index="209" />
<atomRef index="281" />
<atomRef index="282" />
<atomRef index="291" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="201" />
<atomRef index="202" />
<atomRef index="211" />
<atomRef index="210" />
<atomRef index="282" />
<atomRef index="283" />
<atomRef index="292" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="212" />
<atomRef index="211" />
<atomRef index="283" />
<atomRef index="284" />
<atomRef index="293" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="213" />
<atomRef index="212" />
<atomRef index="284" />
<atomRef index="285" />
<atomRef index="294" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="204" />
<atomRef index="205" />
<atomRef index="214" />
<atomRef index="213" />
<atomRef index="285" />
<atomRef index="286" />
<atomRef index="295" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="205" />
<atomRef index="206" />
<atomRef index="215" />
<atomRef index="214" />
<atomRef index="286" />
<atomRef index="287" />
<atomRef index="296" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="207" />
<atomRef index="208" />
<atomRef index="217" />
<atomRef index="216" />
<atomRef index="288" />
<atomRef index="289" />
<atomRef index="298" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="208" />
<atomRef index="209" />
<atomRef index="218" />
<atomRef index="217" />
<atomRef index="289" />
<atomRef index="290" />
<atomRef index="299" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="209" />
<atomRef index="210" />
<atomRef index="219" />
<atomRef index="218" />
<atomRef index="290" />
<atomRef index="291" />
<atomRef index="300" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="210" />
<atomRef index="211" />
<atomRef index="220" />
<atomRef index="219" />
<atomRef index="291" />
<atomRef index="292" />
<atomRef index="301" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="211" />
<atomRef index="212" />
<atomRef index="221" />
<atomRef index="220" />
<atomRef index="292" />
<atomRef index="293" />
<atomRef index="302" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="212" />
<atomRef index="213" />
<atomRef index="222" />
<atomRef index="221" />
<atomRef index="293" />
<atomRef index="294" />
<atomRef index="303" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="223" />
<atomRef index="222" />
<atomRef index="294" />
<atomRef index="295" />
<atomRef index="304" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="214" />
<atomRef index="215" />
<atomRef index="224" />
<atomRef index="223" />
<atomRef index="295" />
<atomRef index="296" />
<atomRef index="305" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="216" />
<atomRef index="217" />
<atomRef index="226" />
<atomRef index="225" />
<atomRef index="297" />
<atomRef index="298" />
<atomRef index="307" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="217" />
<atomRef index="218" />
<atomRef index="227" />
<atomRef index="226" />
<atomRef index="298" />
<atomRef index="299" />
<atomRef index="308" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="218" />
<atomRef index="219" />
<atomRef index="228" />
<atomRef index="227" />
<atomRef index="299" />
<atomRef index="300" />
<atomRef index="309" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="219" />
<atomRef index="220" />
<atomRef index="229" />
<atomRef index="228" />
<atomRef index="300" />
<atomRef index="301" />
<atomRef index="310" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="220" />
<atomRef index="221" />
<atomRef index="230" />
<atomRef index="229" />
<atomRef index="301" />
<atomRef index="302" />
<atomRef index="311" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="221" />
<atomRef index="222" />
<atomRef index="231" />
<atomRef index="230" />
<atomRef index="302" />
<atomRef index="303" />
<atomRef index="312" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="222" />
<atomRef index="223" />
<atomRef index="232" />
<atomRef index="231" />
<atomRef index="303" />
<atomRef index="304" />
<atomRef index="313" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="223" />
<atomRef index="224" />
<atomRef index="233" />
<atomRef index="232" />
<atomRef index="304" />
<atomRef index="305" />
<atomRef index="314" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="225" />
<atomRef index="226" />
<atomRef index="235" />
<atomRef index="234" />
<atomRef index="306" />
<atomRef index="307" />
<atomRef index="316" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="226" />
<atomRef index="227" />
<atomRef index="236" />
<atomRef index="235" />
<atomRef index="307" />
<atomRef index="308" />
<atomRef index="317" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="227" />
<atomRef index="228" />
<atomRef index="237" />
<atomRef index="236" />
<atomRef index="308" />
<atomRef index="309" />
<atomRef index="318" />
<atomRef index="317" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="228" />
<atomRef index="229" />
<atomRef index="238" />
<atomRef index="237" />
<atomRef index="309" />
<atomRef index="310" />
<atomRef index="319" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="229" />
<atomRef index="230" />
<atomRef index="239" />
<atomRef index="238" />
<atomRef index="310" />
<atomRef index="311" />
<atomRef index="320" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="230" />
<atomRef index="231" />
<atomRef index="240" />
<atomRef index="239" />
<atomRef index="311" />
<atomRef index="312" />
<atomRef index="321" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="231" />
<atomRef index="232" />
<atomRef index="241" />
<atomRef index="240" />
<atomRef index="312" />
<atomRef index="313" />
<atomRef index="322" />
<atomRef index="321" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="232" />
<atomRef index="233" />
<atomRef index="242" />
<atomRef index="241" />
<atomRef index="313" />
<atomRef index="314" />
<atomRef index="323" />
<atomRef index="322" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="253" />
<atomRef index="252" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="334" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="254" />
<atomRef index="253" />
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="335" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="255" />
<atomRef index="254" />
<atomRef index="326" />
<atomRef index="327" />
<atomRef index="336" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="256" />
<atomRef index="255" />
<atomRef index="327" />
<atomRef index="328" />
<atomRef index="337" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="257" />
<atomRef index="256" />
<atomRef index="328" />
<atomRef index="329" />
<atomRef index="338" />
<atomRef index="337" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="258" />
<atomRef index="257" />
<atomRef index="329" />
<atomRef index="330" />
<atomRef index="339" />
<atomRef index="338" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="259" />
<atomRef index="258" />
<atomRef index="330" />
<atomRef index="331" />
<atomRef index="340" />
<atomRef index="339" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="250" />
<atomRef index="251" />
<atomRef index="260" />
<atomRef index="259" />
<atomRef index="331" />
<atomRef index="332" />
<atomRef index="341" />
<atomRef index="340" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="252" />
<atomRef index="253" />
<atomRef index="262" />
<atomRef index="261" />
<atomRef index="333" />
<atomRef index="334" />
<atomRef index="343" />
<atomRef index="342" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="253" />
<atomRef index="254" />
<atomRef index="263" />
<atomRef index="262" />
<atomRef index="334" />
<atomRef index="335" />
<atomRef index="344" />
<atomRef index="343" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="254" />
<atomRef index="255" />
<atomRef index="264" />
<atomRef index="263" />
<atomRef index="335" />
<atomRef index="336" />
<atomRef index="345" />
<atomRef index="344" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="255" />
<atomRef index="256" />
<atomRef index="265" />
<atomRef index="264" />
<atomRef index="336" />
<atomRef index="337" />
<atomRef index="346" />
<atomRef index="345" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="256" />
<atomRef index="257" />
<atomRef index="266" />
<atomRef index="265" />
<atomRef index="337" />
<atomRef index="338" />
<atomRef index="347" />
<atomRef index="346" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="257" />
<atomRef index="258" />
<atomRef index="267" />
<atomRef index="266" />
<atomRef index="338" />
<atomRef index="339" />
<atomRef index="348" />
<atomRef index="347" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="258" />
<atomRef index="259" />
<atomRef index="268" />
<atomRef index="267" />
<atomRef index="339" />
<atomRef index="340" />
<atomRef index="349" />
<atomRef index="348" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="259" />
<atomRef index="260" />
<atomRef index="269" />
<atomRef index="268" />
<atomRef index="340" />
<atomRef index="341" />
<atomRef index="350" />
<atomRef index="349" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="261" />
<atomRef index="262" />
<atomRef index="271" />
<atomRef index="270" />
<atomRef index="342" />
<atomRef index="343" />
<atomRef index="352" />
<atomRef index="351" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="262" />
<atomRef index="263" />
<atomRef index="272" />
<atomRef index="271" />
<atomRef index="343" />
<atomRef index="344" />
<atomRef index="353" />
<atomRef index="352" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="263" />
<atomRef index="264" />
<atomRef index="273" />
<atomRef index="272" />
<atomRef index="344" />
<atomRef index="345" />
<atomRef index="354" />
<atomRef index="353" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="264" />
<atomRef index="265" />
<atomRef index="274" />
<atomRef index="273" />
<atomRef index="345" />
<atomRef index="346" />
<atomRef index="355" />
<atomRef index="354" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="265" />
<atomRef index="266" />
<atomRef index="275" />
<atomRef index="274" />
<atomRef index="346" />
<atomRef index="347" />
<atomRef index="356" />
<atomRef index="355" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="266" />
<atomRef index="267" />
<atomRef index="276" />
<atomRef index="275" />
<atomRef index="347" />
<atomRef index="348" />
<atomRef index="357" />
<atomRef index="356" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="267" />
<atomRef index="268" />
<atomRef index="277" />
<atomRef index="276" />
<atomRef index="348" />
<atomRef index="349" />
<atomRef index="358" />
<atomRef index="357" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="268" />
<atomRef index="269" />
<atomRef index="278" />
<atomRef index="277" />
<atomRef index="349" />
<atomRef index="350" />
<atomRef index="359" />
<atomRef index="358" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="270" />
<atomRef index="271" />
<atomRef index="280" />
<atomRef index="279" />
<atomRef index="351" />
<atomRef index="352" />
<atomRef index="361" />
<atomRef index="360" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="271" />
<atomRef index="272" />
<atomRef index="281" />
<atomRef index="280" />
<atomRef index="352" />
<atomRef index="353" />
<atomRef index="362" />
<atomRef index="361" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="272" />
<atomRef index="273" />
<atomRef index="282" />
<atomRef index="281" />
<atomRef index="353" />
<atomRef index="354" />
<atomRef index="363" />
<atomRef index="362" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="273" />
<atomRef index="274" />
<atomRef index="283" />
<atomRef index="282" />
<atomRef index="354" />
<atomRef index="355" />
<atomRef index="364" />
<atomRef index="363" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="274" />
<atomRef index="275" />
<atomRef index="284" />
<atomRef index="283" />
<atomRef index="355" />
<atomRef index="356" />
<atomRef index="365" />
<atomRef index="364" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="275" />
<atomRef index="276" />
<atomRef index="285" />
<atomRef index="284" />
<atomRef index="356" />
<atomRef index="357" />
<atomRef index="366" />
<atomRef index="365" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="276" />
<atomRef index="277" />
<atomRef index="286" />
<atomRef index="285" />
<atomRef index="357" />
<atomRef index="358" />
<atomRef index="367" />
<atomRef index="366" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="277" />
<atomRef index="278" />
<atomRef index="287" />
<atomRef index="286" />
<atomRef index="358" />
<atomRef index="359" />
<atomRef index="368" />
<atomRef index="367" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="279" />
<atomRef index="280" />
<atomRef index="289" />
<atomRef index="288" />
<atomRef index="360" />
<atomRef index="361" />
<atomRef index="370" />
<atomRef index="369" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="280" />
<atomRef index="281" />
<atomRef index="290" />
<atomRef index="289" />
<atomRef index="361" />
<atomRef index="362" />
<atomRef index="371" />
<atomRef index="370" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="281" />
<atomRef index="282" />
<atomRef index="291" />
<atomRef index="290" />
<atomRef index="362" />
<atomRef index="363" />
<atomRef index="372" />
<atomRef index="371" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="282" />
<atomRef index="283" />
<atomRef index="292" />
<atomRef index="291" />
<atomRef index="363" />
<atomRef index="364" />
<atomRef index="373" />
<atomRef index="372" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="283" />
<atomRef index="284" />
<atomRef index="293" />
<atomRef index="292" />
<atomRef index="364" />
<atomRef index="365" />
<atomRef index="374" />
<atomRef index="373" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="284" />
<atomRef index="285" />
<atomRef index="294" />
<atomRef index="293" />
<atomRef index="365" />
<atomRef index="366" />
<atomRef index="375" />
<atomRef index="374" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="285" />
<atomRef index="286" />
<atomRef index="295" />
<atomRef index="294" />
<atomRef index="366" />
<atomRef index="367" />
<atomRef index="376" />
<atomRef index="375" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="286" />
<atomRef index="287" />
<atomRef index="296" />
<atomRef index="295" />
<atomRef index="367" />
<atomRef index="368" />
<atomRef index="377" />
<atomRef index="376" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="288" />
<atomRef index="289" />
<atomRef index="298" />
<atomRef index="297" />
<atomRef index="369" />
<atomRef index="370" />
<atomRef index="379" />
<atomRef index="378" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="289" />
<atomRef index="290" />
<atomRef index="299" />
<atomRef index="298" />
<atomRef index="370" />
<atomRef index="371" />
<atomRef index="380" />
<atomRef index="379" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="290" />
<atomRef index="291" />
<atomRef index="300" />
<atomRef index="299" />
<atomRef index="371" />
<atomRef index="372" />
<atomRef index="381" />
<atomRef index="380" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="291" />
<atomRef index="292" />
<atomRef index="301" />
<atomRef index="300" />
<atomRef index="372" />
<atomRef index="373" />
<atomRef index="382" />
<atomRef index="381" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="292" />
<atomRef index="293" />
<atomRef index="302" />
<atomRef index="301" />
<atomRef index="373" />
<atomRef index="374" />
<atomRef index="383" />
<atomRef index="382" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="293" />
<atomRef index="294" />
<atomRef index="303" />
<atomRef index="302" />
<atomRef index="374" />
<atomRef index="375" />
<atomRef index="384" />
<atomRef index="383" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="294" />
<atomRef index="295" />
<atomRef index="304" />
<atomRef index="303" />
<atomRef index="375" />
<atomRef index="376" />
<atomRef index="385" />
<atomRef index="384" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="295" />
<atomRef index="296" />
<atomRef index="305" />
<atomRef index="304" />
<atomRef index="376" />
<atomRef index="377" />
<atomRef index="386" />
<atomRef index="385" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="297" />
<atomRef index="298" />
<atomRef index="307" />
<atomRef index="306" />
<atomRef index="378" />
<atomRef index="379" />
<atomRef index="388" />
<atomRef index="387" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="298" />
<atomRef index="299" />
<atomRef index="308" />
<atomRef index="307" />
<atomRef index="379" />
<atomRef index="380" />
<atomRef index="389" />
<atomRef index="388" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="299" />
<atomRef index="300" />
<atomRef index="309" />
<atomRef index="308" />
<atomRef index="380" />
<atomRef index="381" />
<atomRef index="390" />
<atomRef index="389" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="300" />
<atomRef index="301" />
<atomRef index="310" />
<atomRef index="309" />
<atomRef index="381" />
<atomRef index="382" />
<atomRef index="391" />
<atomRef index="390" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="301" />
<atomRef index="302" />
<atomRef index="311" />
<atomRef index="310" />
<atomRef index="382" />
<atomRef index="383" />
<atomRef index="392" />
<atomRef index="391" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="302" />
<atomRef index="303" />
<atomRef index="312" />
<atomRef index="311" />
<atomRef index="383" />
<atomRef index="384" />
<atomRef index="393" />
<atomRef index="392" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="303" />
<atomRef index="304" />
<atomRef index="313" />
<atomRef index="312" />
<atomRef index="384" />
<atomRef index="385" />
<atomRef index="394" />
<atomRef index="393" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="304" />
<atomRef index="305" />
<atomRef index="314" />
<atomRef index="313" />
<atomRef index="385" />
<atomRef index="386" />
<atomRef index="395" />
<atomRef index="394" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="306" />
<atomRef index="307" />
<atomRef index="316" />
<atomRef index="315" />
<atomRef index="387" />
<atomRef index="388" />
<atomRef index="397" />
<atomRef index="396" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="307" />
<atomRef index="308" />
<atomRef index="317" />
<atomRef index="316" />
<atomRef index="388" />
<atomRef index="389" />
<atomRef index="398" />
<atomRef index="397" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="308" />
<atomRef index="309" />
<atomRef index="318" />
<atomRef index="317" />
<atomRef index="389" />
<atomRef index="390" />
<atomRef index="399" />
<atomRef index="398" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="309" />
<atomRef index="310" />
<atomRef index="319" />
<atomRef index="318" />
<atomRef index="390" />
<atomRef index="391" />
<atomRef index="400" />
<atomRef index="399" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="310" />
<atomRef index="311" />
<atomRef index="320" />
<atomRef index="319" />
<atomRef index="391" />
<atomRef index="392" />
<atomRef index="401" />
<atomRef index="400" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="311" />
<atomRef index="312" />
<atomRef index="321" />
<atomRef index="320" />
<atomRef index="392" />
<atomRef index="393" />
<atomRef index="402" />
<atomRef index="401" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="312" />
<atomRef index="313" />
<atomRef index="322" />
<atomRef index="321" />
<atomRef index="393" />
<atomRef index="394" />
<atomRef index="403" />
<atomRef index="402" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="313" />
<atomRef index="314" />
<atomRef index="323" />
<atomRef index="322" />
<atomRef index="394" />
<atomRef index="395" />
<atomRef index="404" />
<atomRef index="403" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="334" />
<atomRef index="333" />
<atomRef index="405" />
<atomRef index="406" />
<atomRef index="415" />
<atomRef index="414" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="335" />
<atomRef index="334" />
<atomRef index="406" />
<atomRef index="407" />
<atomRef index="416" />
<atomRef index="415" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="326" />
<atomRef index="327" />
<atomRef index="336" />
<atomRef index="335" />
<atomRef index="407" />
<atomRef index="408" />
<atomRef index="417" />
<atomRef index="416" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="327" />
<atomRef index="328" />
<atomRef index="337" />
<atomRef index="336" />
<atomRef index="408" />
<atomRef index="409" />
<atomRef index="418" />
<atomRef index="417" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="328" />
<atomRef index="329" />
<atomRef index="338" />
<atomRef index="337" />
<atomRef index="409" />
<atomRef index="410" />
<atomRef index="419" />
<atomRef index="418" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="329" />
<atomRef index="330" />
<atomRef index="339" />
<atomRef index="338" />
<atomRef index="410" />
<atomRef index="411" />
<atomRef index="420" />
<atomRef index="419" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="330" />
<atomRef index="331" />
<atomRef index="340" />
<atomRef index="339" />
<atomRef index="411" />
<atomRef index="412" />
<atomRef index="421" />
<atomRef index="420" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="331" />
<atomRef index="332" />
<atomRef index="341" />
<atomRef index="340" />
<atomRef index="412" />
<atomRef index="413" />
<atomRef index="422" />
<atomRef index="421" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="333" />
<atomRef index="334" />
<atomRef index="343" />
<atomRef index="342" />
<atomRef index="414" />
<atomRef index="415" />
<atomRef index="424" />
<atomRef index="423" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="334" />
<atomRef index="335" />
<atomRef index="344" />
<atomRef index="343" />
<atomRef index="415" />
<atomRef index="416" />
<atomRef index="425" />
<atomRef index="424" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="335" />
<atomRef index="336" />
<atomRef index="345" />
<atomRef index="344" />
<atomRef index="416" />
<atomRef index="417" />
<atomRef index="426" />
<atomRef index="425" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="336" />
<atomRef index="337" />
<atomRef index="346" />
<atomRef index="345" />
<atomRef index="417" />
<atomRef index="418" />
<atomRef index="427" />
<atomRef index="426" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="337" />
<atomRef index="338" />
<atomRef index="347" />
<atomRef index="346" />
<atomRef index="418" />
<atomRef index="419" />
<atomRef index="428" />
<atomRef index="427" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="338" />
<atomRef index="339" />
<atomRef index="348" />
<atomRef index="347" />
<atomRef index="419" />
<atomRef index="420" />
<atomRef index="429" />
<atomRef index="428" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="339" />
<atomRef index="340" />
<atomRef index="349" />
<atomRef index="348" />
<atomRef index="420" />
<atomRef index="421" />
<atomRef index="430" />
<atomRef index="429" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="340" />
<atomRef index="341" />
<atomRef index="350" />
<atomRef index="349" />
<atomRef index="421" />
<atomRef index="422" />
<atomRef index="431" />
<atomRef index="430" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="342" />
<atomRef index="343" />
<atomRef index="352" />
<atomRef index="351" />
<atomRef index="423" />
<atomRef index="424" />
<atomRef index="433" />
<atomRef index="432" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="343" />
<atomRef index="344" />
<atomRef index="353" />
<atomRef index="352" />
<atomRef index="424" />
<atomRef index="425" />
<atomRef index="434" />
<atomRef index="433" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="344" />
<atomRef index="345" />
<atomRef index="354" />
<atomRef index="353" />
<atomRef index="425" />
<atomRef index="426" />
<atomRef index="435" />
<atomRef index="434" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="345" />
<atomRef index="346" />
<atomRef index="355" />
<atomRef index="354" />
<atomRef index="426" />
<atomRef index="427" />
<atomRef index="436" />
<atomRef index="435" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="346" />
<atomRef index="347" />
<atomRef index="356" />
<atomRef index="355" />
<atomRef index="427" />
<atomRef index="428" />
<atomRef index="437" />
<atomRef index="436" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="347" />
<atomRef index="348" />
<atomRef index="357" />
<atomRef index="356" />
<atomRef index="428" />
<atomRef index="429" />
<atomRef index="438" />
<atomRef index="437" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="348" />
<atomRef index="349" />
<atomRef index="358" />
<atomRef index="357" />
<atomRef index="429" />
<atomRef index="430" />
<atomRef index="439" />
<atomRef index="438" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="349" />
<atomRef index="350" />
<atomRef index="359" />
<atomRef index="358" />
<atomRef index="430" />
<atomRef index="431" />
<atomRef index="440" />
<atomRef index="439" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="351" />
<atomRef index="352" />
<atomRef index="361" />
<atomRef index="360" />
<atomRef index="432" />
<atomRef index="433" />
<atomRef index="442" />
<atomRef index="441" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="352" />
<atomRef index="353" />
<atomRef index="362" />
<atomRef index="361" />
<atomRef index="433" />
<atomRef index="434" />
<atomRef index="443" />
<atomRef index="442" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="353" />
<atomRef index="354" />
<atomRef index="363" />
<atomRef index="362" />
<atomRef index="434" />
<atomRef index="435" />
<atomRef index="444" />
<atomRef index="443" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="354" />
<atomRef index="355" />
<atomRef index="364" />
<atomRef index="363" />
<atomRef index="435" />
<atomRef index="436" />
<atomRef index="445" />
<atomRef index="444" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="355" />
<atomRef index="356" />
<atomRef index="365" />
<atomRef index="364" />
<atomRef index="436" />
<atomRef index="437" />
<atomRef index="446" />
<atomRef index="445" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="356" />
<atomRef index="357" />
<atomRef index="366" />
<atomRef index="365" />
<atomRef index="437" />
<atomRef index="438" />
<atomRef index="447" />
<atomRef index="446" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="357" />
<atomRef index="358" />
<atomRef index="367" />
<atomRef index="366" />
<atomRef index="438" />
<atomRef index="439" />
<atomRef index="448" />
<atomRef index="447" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="358" />
<atomRef index="359" />
<atomRef index="368" />
<atomRef index="367" />
<atomRef index="439" />
<atomRef index="440" />
<atomRef index="449" />
<atomRef index="448" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="360" />
<atomRef index="361" />
<atomRef index="370" />
<atomRef index="369" />
<atomRef index="441" />
<atomRef index="442" />
<atomRef index="451" />
<atomRef index="450" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="361" />
<atomRef index="362" />
<atomRef index="371" />
<atomRef index="370" />
<atomRef index="442" />
<atomRef index="443" />
<atomRef index="452" />
<atomRef index="451" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="362" />
<atomRef index="363" />
<atomRef index="372" />
<atomRef index="371" />
<atomRef index="443" />
<atomRef index="444" />
<atomRef index="453" />
<atomRef index="452" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="363" />
<atomRef index="364" />
<atomRef index="373" />
<atomRef index="372" />
<atomRef index="444" />
<atomRef index="445" />
<atomRef index="454" />
<atomRef index="453" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="364" />
<atomRef index="365" />
<atomRef index="374" />
<atomRef index="373" />
<atomRef index="445" />
<atomRef index="446" />
<atomRef index="455" />
<atomRef index="454" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="365" />
<atomRef index="366" />
<atomRef index="375" />
<atomRef index="374" />
<atomRef index="446" />
<atomRef index="447" />
<atomRef index="456" />
<atomRef index="455" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="366" />
<atomRef index="367" />
<atomRef index="376" />
<atomRef index="375" />
<atomRef index="447" />
<atomRef index="448" />
<atomRef index="457" />
<atomRef index="456" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="367" />
<atomRef index="368" />
<atomRef index="377" />
<atomRef index="376" />
<atomRef index="448" />
<atomRef index="449" />
<atomRef index="458" />
<atomRef index="457" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="369" />
<atomRef index="370" />
<atomRef index="379" />
<atomRef index="378" />
<atomRef index="450" />
<atomRef index="451" />
<atomRef index="460" />
<atomRef index="459" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="370" />
<atomRef index="371" />
<atomRef index="380" />
<atomRef index="379" />
<atomRef index="451" />
<atomRef index="452" />
<atomRef index="461" />
<atomRef index="460" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="371" />
<atomRef index="372" />
<atomRef index="381" />
<atomRef index="380" />
<atomRef index="452" />
<atomRef index="453" />
<atomRef index="462" />
<atomRef index="461" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="372" />
<atomRef index="373" />
<atomRef index="382" />
<atomRef index="381" />
<atomRef index="453" />
<atomRef index="454" />
<atomRef index="463" />
<atomRef index="462" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="373" />
<atomRef index="374" />
<atomRef index="383" />
<atomRef index="382" />
<atomRef index="454" />
<atomRef index="455" />
<atomRef index="464" />
<atomRef index="463" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="374" />
<atomRef index="375" />
<atomRef index="384" />
<atomRef index="383" />
<atomRef index="455" />
<atomRef index="456" />
<atomRef index="465" />
<atomRef index="464" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="375" />
<atomRef index="376" />
<atomRef index="385" />
<atomRef index="384" />
<atomRef index="456" />
<atomRef index="457" />
<atomRef index="466" />
<atomRef index="465" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="376" />
<atomRef index="377" />
<atomRef index="386" />
<atomRef index="385" />
<atomRef index="457" />
<atomRef index="458" />
<atomRef index="467" />
<atomRef index="466" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="378" />
<atomRef index="379" />
<atomRef index="388" />
<atomRef index="387" />
<atomRef index="459" />
<atomRef index="460" />
<atomRef index="469" />
<atomRef index="468" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="379" />
<atomRef index="380" />
<atomRef index="389" />
<atomRef index="388" />
<atomRef index="460" />
<atomRef index="461" />
<atomRef index="470" />
<atomRef index="469" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="380" />
<atomRef index="381" />
<atomRef index="390" />
<atomRef index="389" />
<atomRef index="461" />
<atomRef index="462" />
<atomRef index="471" />
<atomRef index="470" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="381" />
<atomRef index="382" />
<atomRef index="391" />
<atomRef index="390" />
<atomRef index="462" />
<atomRef index="463" />
<atomRef index="472" />
<atomRef index="471" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="382" />
<atomRef index="383" />
<atomRef index="392" />
<atomRef index="391" />
<atomRef index="463" />
<atomRef index="464" />
<atomRef index="473" />
<atomRef index="472" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="383" />
<atomRef index="384" />
<atomRef index="393" />
<atomRef index="392" />
<atomRef index="464" />
<atomRef index="465" />
<atomRef index="474" />
<atomRef index="473" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="384" />
<atomRef index="385" />
<atomRef index="394" />
<atomRef index="393" />
<atomRef index="465" />
<atomRef index="466" />
<atomRef index="475" />
<atomRef index="474" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="385" />
<atomRef index="386" />
<atomRef index="395" />
<atomRef index="394" />
<atomRef index="466" />
<atomRef index="467" />
<atomRef index="476" />
<atomRef index="475" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="387" />
<atomRef index="388" />
<atomRef index="397" />
<atomRef index="396" />
<atomRef index="468" />
<atomRef index="469" />
<atomRef index="478" />
<atomRef index="477" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="388" />
<atomRef index="389" />
<atomRef index="398" />
<atomRef index="397" />
<atomRef index="469" />
<atomRef index="470" />
<atomRef index="479" />
<atomRef index="478" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="389" />
<atomRef index="390" />
<atomRef index="399" />
<atomRef index="398" />
<atomRef index="470" />
<atomRef index="471" />
<atomRef index="480" />
<atomRef index="479" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="390" />
<atomRef index="391" />
<atomRef index="400" />
<atomRef index="399" />
<atomRef index="471" />
<atomRef index="472" />
<atomRef index="481" />
<atomRef index="480" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="391" />
<atomRef index="392" />
<atomRef index="401" />
<atomRef index="400" />
<atomRef index="472" />
<atomRef index="473" />
<atomRef index="482" />
<atomRef index="481" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="392" />
<atomRef index="393" />
<atomRef index="402" />
<atomRef index="401" />
<atomRef index="473" />
<atomRef index="474" />
<atomRef index="483" />
<atomRef index="482" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="393" />
<atomRef index="394" />
<atomRef index="403" />
<atomRef index="402" />
<atomRef index="474" />
<atomRef index="475" />
<atomRef index="484" />
<atomRef index="483" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="394" />
<atomRef index="395" />
<atomRef index="404" />
<atomRef index="403" />
<atomRef index="475" />
<atomRef index="476" />
<atomRef index="485" />
<atomRef index="484" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="405" />
<atomRef index="406" />
<atomRef index="415" />
<atomRef index="414" />
<atomRef index="486" />
<atomRef index="487" />
<atomRef index="496" />
<atomRef index="495" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="406" />
<atomRef index="407" />
<atomRef index="416" />
<atomRef index="415" />
<atomRef index="487" />
<atomRef index="488" />
<atomRef index="497" />
<atomRef index="496" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="407" />
<atomRef index="408" />
<atomRef index="417" />
<atomRef index="416" />
<atomRef index="488" />
<atomRef index="489" />
<atomRef index="498" />
<atomRef index="497" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="408" />
<atomRef index="409" />
<atomRef index="418" />
<atomRef index="417" />
<atomRef index="489" />
<atomRef index="490" />
<atomRef index="499" />
<atomRef index="498" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="409" />
<atomRef index="410" />
<atomRef index="419" />
<atomRef index="418" />
<atomRef index="490" />
<atomRef index="491" />
<atomRef index="500" />
<atomRef index="499" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="410" />
<atomRef index="411" />
<atomRef index="420" />
<atomRef index="419" />
<atomRef index="491" />
<atomRef index="492" />
<atomRef index="501" />
<atomRef index="500" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="411" />
<atomRef index="412" />
<atomRef index="421" />
<atomRef index="420" />
<atomRef index="492" />
<atomRef index="493" />
<atomRef index="502" />
<atomRef index="501" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="412" />
<atomRef index="413" />
<atomRef index="422" />
<atomRef index="421" />
<atomRef index="493" />
<atomRef index="494" />
<atomRef index="503" />
<atomRef index="502" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="414" />
<atomRef index="415" />
<atomRef index="424" />
<atomRef index="423" />
<atomRef index="495" />
<atomRef index="496" />
<atomRef index="505" />
<atomRef index="504" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="415" />
<atomRef index="416" />
<atomRef index="425" />
<atomRef index="424" />
<atomRef index="496" />
<atomRef index="497" />
<atomRef index="506" />
<atomRef index="505" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="416" />
<atomRef index="417" />
<atomRef index="426" />
<atomRef index="425" />
<atomRef index="497" />
<atomRef index="498" />
<atomRef index="507" />
<atomRef index="506" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="417" />
<atomRef index="418" />
<atomRef index="427" />
<atomRef index="426" />
<atomRef index="498" />
<atomRef index="499" />
<atomRef index="508" />
<atomRef index="507" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="418" />
<atomRef index="419" />
<atomRef index="428" />
<atomRef index="427" />
<atomRef index="499" />
<atomRef index="500" />
<atomRef index="509" />
<atomRef index="508" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="419" />
<atomRef index="420" />
<atomRef index="429" />
<atomRef index="428" />
<atomRef index="500" />
<atomRef index="501" />
<atomRef index="510" />
<atomRef index="509" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="420" />
<atomRef index="421" />
<atomRef index="430" />
<atomRef index="429" />
<atomRef index="501" />
<atomRef index="502" />
<atomRef index="511" />
<atomRef index="510" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="421" />
<atomRef index="422" />
<atomRef index="431" />
<atomRef index="430" />
<atomRef index="502" />
<atomRef index="503" />
<atomRef index="512" />
<atomRef index="511" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="423" />
<atomRef index="424" />
<atomRef index="433" />
<atomRef index="432" />
<atomRef index="504" />
<atomRef index="505" />
<atomRef index="514" />
<atomRef index="513" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="424" />
<atomRef index="425" />
<atomRef index="434" />
<atomRef index="433" />
<atomRef index="505" />
<atomRef index="506" />
<atomRef index="515" />
<atomRef index="514" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="425" />
<atomRef index="426" />
<atomRef index="435" />
<atomRef index="434" />
<atomRef index="506" />
<atomRef index="507" />
<atomRef index="516" />
<atomRef index="515" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="426" />
<atomRef index="427" />
<atomRef index="436" />
<atomRef index="435" />
<atomRef index="507" />
<atomRef index="508" />
<atomRef index="517" />
<atomRef index="516" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="427" />
<atomRef index="428" />
<atomRef index="437" />
<atomRef index="436" />
<atomRef index="508" />
<atomRef index="509" />
<atomRef index="518" />
<atomRef index="517" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="428" />
<atomRef index="429" />
<atomRef index="438" />
<atomRef index="437" />
<atomRef index="509" />
<atomRef index="510" />
<atomRef index="519" />
<atomRef index="518" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="429" />
<atomRef index="430" />
<atomRef index="439" />
<atomRef index="438" />
<atomRef index="510" />
<atomRef index="511" />
<atomRef index="520" />
<atomRef index="519" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="430" />
<atomRef index="431" />
<atomRef index="440" />
<atomRef index="439" />
<atomRef index="511" />
<atomRef index="512" />
<atomRef index="521" />
<atomRef index="520" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="432" />
<atomRef index="433" />
<atomRef index="442" />
<atomRef index="441" />
<atomRef index="513" />
<atomRef index="514" />
<atomRef index="523" />
<atomRef index="522" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="433" />
<atomRef index="434" />
<atomRef index="443" />
<atomRef index="442" />
<atomRef index="514" />
<atomRef index="515" />
<atomRef index="524" />
<atomRef index="523" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="434" />
<atomRef index="435" />
<atomRef index="444" />
<atomRef index="443" />
<atomRef index="515" />
<atomRef index="516" />
<atomRef index="525" />
<atomRef index="524" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="435" />
<atomRef index="436" />
<atomRef index="445" />
<atomRef index="444" />
<atomRef index="516" />
<atomRef index="517" />
<atomRef index="526" />
<atomRef index="525" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="436" />
<atomRef index="437" />
<atomRef index="446" />
<atomRef index="445" />
<atomRef index="517" />
<atomRef index="518" />
<atomRef index="527" />
<atomRef index="526" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="437" />
<atomRef index="438" />
<atomRef index="447" />
<atomRef index="446" />
<atomRef index="518" />
<atomRef index="519" />
<atomRef index="528" />
<atomRef index="527" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="438" />
<atomRef index="439" />
<atomRef index="448" />
<atomRef index="447" />
<atomRef index="519" />
<atomRef index="520" />
<atomRef index="529" />
<atomRef index="528" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="439" />
<atomRef index="440" />
<atomRef index="449" />
<atomRef index="448" />
<atomRef index="520" />
<atomRef index="521" />
<atomRef index="530" />
<atomRef index="529" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="441" />
<atomRef index="442" />
<atomRef index="451" />
<atomRef index="450" />
<atomRef index="522" />
<atomRef index="523" />
<atomRef index="532" />
<atomRef index="531" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="442" />
<atomRef index="443" />
<atomRef index="452" />
<atomRef index="451" />
<atomRef index="523" />
<atomRef index="524" />
<atomRef index="533" />
<atomRef index="532" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="443" />
<atomRef index="444" />
<atomRef index="453" />
<atomRef index="452" />
<atomRef index="524" />
<atomRef index="525" />
<atomRef index="534" />
<atomRef index="533" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="444" />
<atomRef index="445" />
<atomRef index="454" />
<atomRef index="453" />
<atomRef index="525" />
<atomRef index="526" />
<atomRef index="535" />
<atomRef index="534" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="445" />
<atomRef index="446" />
<atomRef index="455" />
<atomRef index="454" />
<atomRef index="526" />
<atomRef index="527" />
<atomRef index="536" />
<atomRef index="535" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="446" />
<atomRef index="447" />
<atomRef index="456" />
<atomRef index="455" />
<atomRef index="527" />
<atomRef index="528" />
<atomRef index="537" />
<atomRef index="536" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="447" />
<atomRef index="448" />
<atomRef index="457" />
<atomRef index="456" />
<atomRef index="528" />
<atomRef index="529" />
<atomRef index="538" />
<atomRef index="537" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="448" />
<atomRef index="449" />
<atomRef index="458" />
<atomRef index="457" />
<atomRef index="529" />
<atomRef index="530" />
<atomRef index="539" />
<atomRef index="538" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="450" />
<atomRef index="451" />
<atomRef index="460" />
<atomRef index="459" />
<atomRef index="531" />
<atomRef index="532" />
<atomRef index="541" />
<atomRef index="540" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="451" />
<atomRef index="452" />
<atomRef index="461" />
<atomRef index="460" />
<atomRef index="532" />
<atomRef index="533" />
<atomRef index="542" />
<atomRef index="541" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="452" />
<atomRef index="453" />
<atomRef index="462" />
<atomRef index="461" />
<atomRef index="533" />
<atomRef index="534" />
<atomRef index="543" />
<atomRef index="542" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="453" />
<atomRef index="454" />
<atomRef index="463" />
<atomRef index="462" />
<atomRef index="534" />
<atomRef index="535" />
<atomRef index="544" />
<atomRef index="543" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="454" />
<atomRef index="455" />
<atomRef index="464" />
<atomRef index="463" />
<atomRef index="535" />
<atomRef index="536" />
<atomRef index="545" />
<atomRef index="544" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="455" />
<atomRef index="456" />
<atomRef index="465" />
<atomRef index="464" />
<atomRef index="536" />
<atomRef index="537" />
<atomRef index="546" />
<atomRef index="545" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="456" />
<atomRef index="457" />
<atomRef index="466" />
<atomRef index="465" />
<atomRef index="537" />
<atomRef index="538" />
<atomRef index="547" />
<atomRef index="546" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="457" />
<atomRef index="458" />
<atomRef index="467" />
<atomRef index="466" />
<atomRef index="538" />
<atomRef index="539" />
<atomRef index="548" />
<atomRef index="547" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="459" />
<atomRef index="460" />
<atomRef index="469" />
<atomRef index="468" />
<atomRef index="540" />
<atomRef index="541" />
<atomRef index="550" />
<atomRef index="549" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="460" />
<atomRef index="461" />
<atomRef index="470" />
<atomRef index="469" />
<atomRef index="541" />
<atomRef index="542" />
<atomRef index="551" />
<atomRef index="550" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="461" />
<atomRef index="462" />
<atomRef index="471" />
<atomRef index="470" />
<atomRef index="542" />
<atomRef index="543" />
<atomRef index="552" />
<atomRef index="551" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="462" />
<atomRef index="463" />
<atomRef index="472" />
<atomRef index="471" />
<atomRef index="543" />
<atomRef index="544" />
<atomRef index="553" />
<atomRef index="552" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="463" />
<atomRef index="464" />
<atomRef index="473" />
<atomRef index="472" />
<atomRef index="544" />
<atomRef index="545" />
<atomRef index="554" />
<atomRef index="553" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="464" />
<atomRef index="465" />
<atomRef index="474" />
<atomRef index="473" />
<atomRef index="545" />
<atomRef index="546" />
<atomRef index="555" />
<atomRef index="554" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="465" />
<atomRef index="466" />
<atomRef index="475" />
<atomRef index="474" />
<atomRef index="546" />
<atomRef index="547" />
<atomRef index="556" />
<atomRef index="555" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="466" />
<atomRef index="467" />
<atomRef index="476" />
<atomRef index="475" />
<atomRef index="547" />
<atomRef index="548" />
<atomRef index="557" />
<atomRef index="556" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="468" />
<atomRef index="469" />
<atomRef index="478" />
<atomRef index="477" />
<atomRef index="549" />
<atomRef index="550" />
<atomRef index="559" />
<atomRef index="558" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="469" />
<atomRef index="470" />
<atomRef index="479" />
<atomRef index="478" />
<atomRef index="550" />
<atomRef index="551" />
<atomRef index="560" />
<atomRef index="559" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="470" />
<atomRef index="471" />
<atomRef index="480" />
<atomRef index="479" />
<atomRef index="551" />
<atomRef index="552" />
<atomRef index="561" />
<atomRef index="560" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="471" />
<atomRef index="472" />
<atomRef index="481" />
<atomRef index="480" />
<atomRef index="552" />
<atomRef index="553" />
<atomRef index="562" />
<atomRef index="561" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="472" />
<atomRef index="473" />
<atomRef index="482" />
<atomRef index="481" />
<atomRef index="553" />
<atomRef index="554" />
<atomRef index="563" />
<atomRef index="562" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="473" />
<atomRef index="474" />
<atomRef index="483" />
<atomRef index="482" />
<atomRef index="554" />
<atomRef index="555" />
<atomRef index="564" />
<atomRef index="563" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="474" />
<atomRef index="475" />
<atomRef index="484" />
<atomRef index="483" />
<atomRef index="555" />
<atomRef index="556" />
<atomRef index="565" />
<atomRef index="564" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="475" />
<atomRef index="476" />
<atomRef index="485" />
<atomRef index="484" />
<atomRef index="556" />
<atomRef index="557" />
<atomRef index="566" />
<atomRef index="565" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="486" />
<atomRef index="487" />
<atomRef index="496" />
<atomRef index="495" />
<atomRef index="567" />
<atomRef index="568" />
<atomRef index="577" />
<atomRef index="576" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="487" />
<atomRef index="488" />
<atomRef index="497" />
<atomRef index="496" />
<atomRef index="568" />
<atomRef index="569" />
<atomRef index="578" />
<atomRef index="577" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="488" />
<atomRef index="489" />
<atomRef index="498" />
<atomRef index="497" />
<atomRef index="569" />
<atomRef index="570" />
<atomRef index="579" />
<atomRef index="578" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="489" />
<atomRef index="490" />
<atomRef index="499" />
<atomRef index="498" />
<atomRef index="570" />
<atomRef index="571" />
<atomRef index="580" />
<atomRef index="579" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="490" />
<atomRef index="491" />
<atomRef index="500" />
<atomRef index="499" />
<atomRef index="571" />
<atomRef index="572" />
<atomRef index="581" />
<atomRef index="580" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="491" />
<atomRef index="492" />
<atomRef index="501" />
<atomRef index="500" />
<atomRef index="572" />
<atomRef index="573" />
<atomRef index="582" />
<atomRef index="581" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="492" />
<atomRef index="493" />
<atomRef index="502" />
<atomRef index="501" />
<atomRef index="573" />
<atomRef index="574" />
<atomRef index="583" />
<atomRef index="582" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="493" />
<atomRef index="494" />
<atomRef index="503" />
<atomRef index="502" />
<atomRef index="574" />
<atomRef index="575" />
<atomRef index="584" />
<atomRef index="583" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="495" />
<atomRef index="496" />
<atomRef index="505" />
<atomRef index="504" />
<atomRef index="576" />
<atomRef index="577" />
<atomRef index="586" />
<atomRef index="585" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="496" />
<atomRef index="497" />
<atomRef index="506" />
<atomRef index="505" />
<atomRef index="577" />
<atomRef index="578" />
<atomRef index="587" />
<atomRef index="586" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="497" />
<atomRef index="498" />
<atomRef index="507" />
<atomRef index="506" />
<atomRef index="578" />
<atomRef index="579" />
<atomRef index="588" />
<atomRef index="587" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="498" />
<atomRef index="499" />
<atomRef index="508" />
<atomRef index="507" />
<atomRef index="579" />
<atomRef index="580" />
<atomRef index="589" />
<atomRef index="588" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="499" />
<atomRef index="500" />
<atomRef index="509" />
<atomRef index="508" />
<atomRef index="580" />
<atomRef index="581" />
<atomRef index="590" />
<atomRef index="589" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="500" />
<atomRef index="501" />
<atomRef index="510" />
<atomRef index="509" />
<atomRef index="581" />
<atomRef index="582" />
<atomRef index="591" />
<atomRef index="590" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="501" />
<atomRef index="502" />
<atomRef index="511" />
<atomRef index="510" />
<atomRef index="582" />
<atomRef index="583" />
<atomRef index="592" />
<atomRef index="591" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="502" />
<atomRef index="503" />
<atomRef index="512" />
<atomRef index="511" />
<atomRef index="583" />
<atomRef index="584" />
<atomRef index="593" />
<atomRef index="592" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="504" />
<atomRef index="505" />
<atomRef index="514" />
<atomRef index="513" />
<atomRef index="585" />
<atomRef index="586" />
<atomRef index="595" />
<atomRef index="594" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="505" />
<atomRef index="506" />
<atomRef index="515" />
<atomRef index="514" />
<atomRef index="586" />
<atomRef index="587" />
<atomRef index="596" />
<atomRef index="595" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="506" />
<atomRef index="507" />
<atomRef index="516" />
<atomRef index="515" />
<atomRef index="587" />
<atomRef index="588" />
<atomRef index="597" />
<atomRef index="596" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="507" />
<atomRef index="508" />
<atomRef index="517" />
<atomRef index="516" />
<atomRef index="588" />
<atomRef index="589" />
<atomRef index="598" />
<atomRef index="597" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="508" />
<atomRef index="509" />
<atomRef index="518" />
<atomRef index="517" />
<atomRef index="589" />
<atomRef index="590" />
<atomRef index="599" />
<atomRef index="598" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="509" />
<atomRef index="510" />
<atomRef index="519" />
<atomRef index="518" />
<atomRef index="590" />
<atomRef index="591" />
<atomRef index="600" />
<atomRef index="599" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="510" />
<atomRef index="511" />
<atomRef index="520" />
<atomRef index="519" />
<atomRef index="591" />
<atomRef index="592" />
<atomRef index="601" />
<atomRef index="600" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="511" />
<atomRef index="512" />
<atomRef index="521" />
<atomRef index="520" />
<atomRef index="592" />
<atomRef index="593" />
<atomRef index="602" />
<atomRef index="601" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="513" />
<atomRef index="514" />
<atomRef index="523" />
<atomRef index="522" />
<atomRef index="594" />
<atomRef index="595" />
<atomRef index="604" />
<atomRef index="603" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="514" />
<atomRef index="515" />
<atomRef index="524" />
<atomRef index="523" />
<atomRef index="595" />
<atomRef index="596" />
<atomRef index="605" />
<atomRef index="604" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="515" />
<atomRef index="516" />
<atomRef index="525" />
<atomRef index="524" />
<atomRef index="596" />
<atomRef index="597" />
<atomRef index="606" />
<atomRef index="605" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="516" />
<atomRef index="517" />
<atomRef index="526" />
<atomRef index="525" />
<atomRef index="597" />
<atomRef index="598" />
<atomRef index="607" />
<atomRef index="606" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="517" />
<atomRef index="518" />
<atomRef index="527" />
<atomRef index="526" />
<atomRef index="598" />
<atomRef index="599" />
<atomRef index="608" />
<atomRef index="607" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="518" />
<atomRef index="519" />
<atomRef index="528" />
<atomRef index="527" />
<atomRef index="599" />
<atomRef index="600" />
<atomRef index="609" />
<atomRef index="608" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="519" />
<atomRef index="520" />
<atomRef index="529" />
<atomRef index="528" />
<atomRef index="600" />
<atomRef index="601" />
<atomRef index="610" />
<atomRef index="609" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="520" />
<atomRef index="521" />
<atomRef index="530" />
<atomRef index="529" />
<atomRef index="601" />
<atomRef index="602" />
<atomRef index="611" />
<atomRef index="610" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="522" />
<atomRef index="523" />
<atomRef index="532" />
<atomRef index="531" />
<atomRef index="603" />
<atomRef index="604" />
<atomRef index="613" />
<atomRef index="612" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="523" />
<atomRef index="524" />
<atomRef index="533" />
<atomRef index="532" />
<atomRef index="604" />
<atomRef index="605" />
<atomRef index="614" />
<atomRef index="613" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="524" />
<atomRef index="525" />
<atomRef index="534" />
<atomRef index="533" />
<atomRef index="605" />
<atomRef index="606" />
<atomRef index="615" />
<atomRef index="614" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="525" />
<atomRef index="526" />
<atomRef index="535" />
<atomRef index="534" />
<atomRef index="606" />
<atomRef index="607" />
<atomRef index="616" />
<atomRef index="615" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="526" />
<atomRef index="527" />
<atomRef index="536" />
<atomRef index="535" />
<atomRef index="607" />
<atomRef index="608" />
<atomRef index="617" />
<atomRef index="616" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="527" />
<atomRef index="528" />
<atomRef index="537" />
<atomRef index="536" />
<atomRef index="608" />
<atomRef index="609" />
<atomRef index="618" />
<atomRef index="617" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="528" />
<atomRef index="529" />
<atomRef index="538" />
<atomRef index="537" />
<atomRef index="609" />
<atomRef index="610" />
<atomRef index="619" />
<atomRef index="618" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="529" />
<atomRef index="530" />
<atomRef index="539" />
<atomRef index="538" />
<atomRef index="610" />
<atomRef index="611" />
<atomRef index="620" />
<atomRef index="619" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="531" />
<atomRef index="532" />
<atomRef index="541" />
<atomRef index="540" />
<atomRef index="612" />
<atomRef index="613" />
<atomRef index="622" />
<atomRef index="621" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="532" />
<atomRef index="533" />
<atomRef index="542" />
<atomRef index="541" />
<atomRef index="613" />
<atomRef index="614" />
<atomRef index="623" />
<atomRef index="622" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="533" />
<atomRef index="534" />
<atomRef index="543" />
<atomRef index="542" />
<atomRef index="614" />
<atomRef index="615" />
<atomRef index="624" />
<atomRef index="623" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="534" />
<atomRef index="535" />
<atomRef index="544" />
<atomRef index="543" />
<atomRef index="615" />
<atomRef index="616" />
<atomRef index="625" />
<atomRef index="624" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="535" />
<atomRef index="536" />
<atomRef index="545" />
<atomRef index="544" />
<atomRef index="616" />
<atomRef index="617" />
<atomRef index="626" />
<atomRef index="625" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="536" />
<atomRef index="537" />
<atomRef index="546" />
<atomRef index="545" />
<atomRef index="617" />
<atomRef index="618" />
<atomRef index="627" />
<atomRef index="626" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="537" />
<atomRef index="538" />
<atomRef index="547" />
<atomRef index="546" />
<atomRef index="618" />
<atomRef index="619" />
<atomRef index="628" />
<atomRef index="627" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="538" />
<atomRef index="539" />
<atomRef index="548" />
<atomRef index="547" />
<atomRef index="619" />
<atomRef index="620" />
<atomRef index="629" />
<atomRef index="628" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="540" />
<atomRef index="541" />
<atomRef index="550" />
<atomRef index="549" />
<atomRef index="621" />
<atomRef index="622" />
<atomRef index="631" />
<atomRef index="630" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="541" />
<atomRef index="542" />
<atomRef index="551" />
<atomRef index="550" />
<atomRef index="622" />
<atomRef index="623" />
<atomRef index="632" />
<atomRef index="631" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="542" />
<atomRef index="543" />
<atomRef index="552" />
<atomRef index="551" />
<atomRef index="623" />
<atomRef index="624" />
<atomRef index="633" />
<atomRef index="632" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="543" />
<atomRef index="544" />
<atomRef index="553" />
<atomRef index="552" />
<atomRef index="624" />
<atomRef index="625" />
<atomRef index="634" />
<atomRef index="633" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="544" />
<atomRef index="545" />
<atomRef index="554" />
<atomRef index="553" />
<atomRef index="625" />
<atomRef index="626" />
<atomRef index="635" />
<atomRef index="634" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="545" />
<atomRef index="546" />
<atomRef index="555" />
<atomRef index="554" />
<atomRef index="626" />
<atomRef index="627" />
<atomRef index="636" />
<atomRef index="635" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="546" />
<atomRef index="547" />
<atomRef index="556" />
<atomRef index="555" />
<atomRef index="627" />
<atomRef index="628" />
<atomRef index="637" />
<atomRef index="636" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="547" />
<atomRef index="548" />
<atomRef index="557" />
<atomRef index="556" />
<atomRef index="628" />
<atomRef index="629" />
<atomRef index="638" />
<atomRef index="637" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="549" />
<atomRef index="550" />
<atomRef index="559" />
<atomRef index="558" />
<atomRef index="630" />
<atomRef index="631" />
<atomRef index="640" />
<atomRef index="639" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="550" />
<atomRef index="551" />
<atomRef index="560" />
<atomRef index="559" />
<atomRef index="631" />
<atomRef index="632" />
<atomRef index="641" />
<atomRef index="640" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="551" />
<atomRef index="552" />
<atomRef index="561" />
<atomRef index="560" />
<atomRef index="632" />
<atomRef index="633" />
<atomRef index="642" />
<atomRef index="641" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="552" />
<atomRef index="553" />
<atomRef index="562" />
<atomRef index="561" />
<atomRef index="633" />
<atomRef index="634" />
<atomRef index="643" />
<atomRef index="642" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="553" />
<atomRef index="554" />
<atomRef index="563" />
<atomRef index="562" />
<atomRef index="634" />
<atomRef index="635" />
<atomRef index="644" />
<atomRef index="643" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="554" />
<atomRef index="555" />
<atomRef index="564" />
<atomRef index="563" />
<atomRef index="635" />
<atomRef index="636" />
<atomRef index="645" />
<atomRef index="644" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="555" />
<atomRef index="556" />
<atomRef index="565" />
<atomRef index="564" />
<atomRef index="636" />
<atomRef index="637" />
<atomRef index="646" />
<atomRef index="645" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="556" />
<atomRef index="557" />
<atomRef index="566" />
<atomRef index="565" />
<atomRef index="637" />
<atomRef index="638" />
<atomRef index="647" />
<atomRef index="646" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="567" />
<atomRef index="568" />
<atomRef index="577" />
<atomRef index="576" />
<atomRef index="648" />
<atomRef index="649" />
<atomRef index="658" />
<atomRef index="657" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="568" />
<atomRef index="569" />
<atomRef index="578" />
<atomRef index="577" />
<atomRef index="649" />
<atomRef index="650" />
<atomRef index="659" />
<atomRef index="658" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="569" />
<atomRef index="570" />
<atomRef index="579" />
<atomRef index="578" />
<atomRef index="650" />
<atomRef index="651" />
<atomRef index="660" />
<atomRef index="659" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="570" />
<atomRef index="571" />
<atomRef index="580" />
<atomRef index="579" />
<atomRef index="651" />
<atomRef index="652" />
<atomRef index="661" />
<atomRef index="660" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="571" />
<atomRef index="572" />
<atomRef index="581" />
<atomRef index="580" />
<atomRef index="652" />
<atomRef index="653" />
<atomRef index="662" />
<atomRef index="661" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="572" />
<atomRef index="573" />
<atomRef index="582" />
<atomRef index="581" />
<atomRef index="653" />
<atomRef index="654" />
<atomRef index="663" />
<atomRef index="662" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="573" />
<atomRef index="574" />
<atomRef index="583" />
<atomRef index="582" />
<atomRef index="654" />
<atomRef index="655" />
<atomRef index="664" />
<atomRef index="663" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="574" />
<atomRef index="575" />
<atomRef index="584" />
<atomRef index="583" />
<atomRef index="655" />
<atomRef index="656" />
<atomRef index="665" />
<atomRef index="664" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="576" />
<atomRef index="577" />
<atomRef index="586" />
<atomRef index="585" />
<atomRef index="657" />
<atomRef index="658" />
<atomRef index="667" />
<atomRef index="666" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="577" />
<atomRef index="578" />
<atomRef index="587" />
<atomRef index="586" />
<atomRef index="658" />
<atomRef index="659" />
<atomRef index="668" />
<atomRef index="667" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="578" />
<atomRef index="579" />
<atomRef index="588" />
<atomRef index="587" />
<atomRef index="659" />
<atomRef index="660" />
<atomRef index="669" />
<atomRef index="668" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="579" />
<atomRef index="580" />
<atomRef index="589" />
<atomRef index="588" />
<atomRef index="660" />
<atomRef index="661" />
<atomRef index="670" />
<atomRef index="669" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="580" />
<atomRef index="581" />
<atomRef index="590" />
<atomRef index="589" />
<atomRef index="661" />
<atomRef index="662" />
<atomRef index="671" />
<atomRef index="670" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="581" />
<atomRef index="582" />
<atomRef index="591" />
<atomRef index="590" />
<atomRef index="662" />
<atomRef index="663" />
<atomRef index="672" />
<atomRef index="671" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="582" />
<atomRef index="583" />
<atomRef index="592" />
<atomRef index="591" />
<atomRef index="663" />
<atomRef index="664" />
<atomRef index="673" />
<atomRef index="672" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="583" />
<atomRef index="584" />
<atomRef index="593" />
<atomRef index="592" />
<atomRef index="664" />
<atomRef index="665" />
<atomRef index="674" />
<atomRef index="673" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="585" />
<atomRef index="586" />
<atomRef index="595" />
<atomRef index="594" />
<atomRef index="666" />
<atomRef index="667" />
<atomRef index="676" />
<atomRef index="675" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="586" />
<atomRef index="587" />
<atomRef index="596" />
<atomRef index="595" />
<atomRef index="667" />
<atomRef index="668" />
<atomRef index="677" />
<atomRef index="676" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="587" />
<atomRef index="588" />
<atomRef index="597" />
<atomRef index="596" />
<atomRef index="668" />
<atomRef index="669" />
<atomRef index="678" />
<atomRef index="677" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="588" />
<atomRef index="589" />
<atomRef index="598" />
<atomRef index="597" />
<atomRef index="669" />
<atomRef index="670" />
<atomRef index="679" />
<atomRef index="678" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="589" />
<atomRef index="590" />
<atomRef index="599" />
<atomRef index="598" />
<atomRef index="670" />
<atomRef index="671" />
<atomRef index="680" />
<atomRef index="679" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="590" />
<atomRef index="591" />
<atomRef index="600" />
<atomRef index="599" />
<atomRef index="671" />
<atomRef index="672" />
<atomRef index="681" />
<atomRef index="680" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="591" />
<atomRef index="592" />
<atomRef index="601" />
<atomRef index="600" />
<atomRef index="672" />
<atomRef index="673" />
<atomRef index="682" />
<atomRef index="681" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="592" />
<atomRef index="593" />
<atomRef index="602" />
<atomRef index="601" />
<atomRef index="673" />
<atomRef index="674" />
<atomRef index="683" />
<atomRef index="682" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="594" />
<atomRef index="595" />
<atomRef index="604" />
<atomRef index="603" />
<atomRef index="675" />
<atomRef index="676" />
<atomRef index="685" />
<atomRef index="684" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="595" />
<atomRef index="596" />
<atomRef index="605" />
<atomRef index="604" />
<atomRef index="676" />
<atomRef index="677" />
<atomRef index="686" />
<atomRef index="685" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="596" />
<atomRef index="597" />
<atomRef index="606" />
<atomRef index="605" />
<atomRef index="677" />
<atomRef index="678" />
<atomRef index="687" />
<atomRef index="686" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="597" />
<atomRef index="598" />
<atomRef index="607" />
<atomRef index="606" />
<atomRef index="678" />
<atomRef index="679" />
<atomRef index="688" />
<atomRef index="687" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="598" />
<atomRef index="599" />
<atomRef index="608" />
<atomRef index="607" />
<atomRef index="679" />
<atomRef index="680" />
<atomRef index="689" />
<atomRef index="688" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="599" />
<atomRef index="600" />
<atomRef index="609" />
<atomRef index="608" />
<atomRef index="680" />
<atomRef index="681" />
<atomRef index="690" />
<atomRef index="689" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="600" />
<atomRef index="601" />
<atomRef index="610" />
<atomRef index="609" />
<atomRef index="681" />
<atomRef index="682" />
<atomRef index="691" />
<atomRef index="690" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="601" />
<atomRef index="602" />
<atomRef index="611" />
<atomRef index="610" />
<atomRef index="682" />
<atomRef index="683" />
<atomRef index="692" />
<atomRef index="691" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="603" />
<atomRef index="604" />
<atomRef index="613" />
<atomRef index="612" />
<atomRef index="684" />
<atomRef index="685" />
<atomRef index="694" />
<atomRef index="693" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="604" />
<atomRef index="605" />
<atomRef index="614" />
<atomRef index="613" />
<atomRef index="685" />
<atomRef index="686" />
<atomRef index="695" />
<atomRef index="694" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="605" />
<atomRef index="606" />
<atomRef index="615" />
<atomRef index="614" />
<atomRef index="686" />
<atomRef index="687" />
<atomRef index="696" />
<atomRef index="695" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="606" />
<atomRef index="607" />
<atomRef index="616" />
<atomRef index="615" />
<atomRef index="687" />
<atomRef index="688" />
<atomRef index="697" />
<atomRef index="696" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="607" />
<atomRef index="608" />
<atomRef index="617" />
<atomRef index="616" />
<atomRef index="688" />
<atomRef index="689" />
<atomRef index="698" />
<atomRef index="697" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="608" />
<atomRef index="609" />
<atomRef index="618" />
<atomRef index="617" />
<atomRef index="689" />
<atomRef index="690" />
<atomRef index="699" />
<atomRef index="698" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="609" />
<atomRef index="610" />
<atomRef index="619" />
<atomRef index="618" />
<atomRef index="690" />
<atomRef index="691" />
<atomRef index="700" />
<atomRef index="699" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="610" />
<atomRef index="611" />
<atomRef index="620" />
<atomRef index="619" />
<atomRef index="691" />
<atomRef index="692" />
<atomRef index="701" />
<atomRef index="700" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="612" />
<atomRef index="613" />
<atomRef index="622" />
<atomRef index="621" />
<atomRef index="693" />
<atomRef index="694" />
<atomRef index="703" />
<atomRef index="702" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="613" />
<atomRef index="614" />
<atomRef index="623" />
<atomRef index="622" />
<atomRef index="694" />
<atomRef index="695" />
<atomRef index="704" />
<atomRef index="703" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="614" />
<atomRef index="615" />
<atomRef index="624" />
<atomRef index="623" />
<atomRef index="695" />
<atomRef index="696" />
<atomRef index="705" />
<atomRef index="704" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="615" />
<atomRef index="616" />
<atomRef index="625" />
<atomRef index="624" />
<atomRef index="696" />
<atomRef index="697" />
<atomRef index="706" />
<atomRef index="705" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="616" />
<atomRef index="617" />
<atomRef index="626" />
<atomRef index="625" />
<atomRef index="697" />
<atomRef index="698" />
<atomRef index="707" />
<atomRef index="706" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="617" />
<atomRef index="618" />
<atomRef index="627" />
<atomRef index="626" />
<atomRef index="698" />
<atomRef index="699" />
<atomRef index="708" />
<atomRef index="707" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="618" />
<atomRef index="619" />
<atomRef index="628" />
<atomRef index="627" />
<atomRef index="699" />
<atomRef index="700" />
<atomRef index="709" />
<atomRef index="708" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="619" />
<atomRef index="620" />
<atomRef index="629" />
<atomRef index="628" />
<atomRef index="700" />
<atomRef index="701" />
<atomRef index="710" />
<atomRef index="709" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="621" />
<atomRef index="622" />
<atomRef index="631" />
<atomRef index="630" />
<atomRef index="702" />
<atomRef index="703" />
<atomRef index="712" />
<atomRef index="711" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="622" />
<atomRef index="623" />
<atomRef index="632" />
<atomRef index="631" />
<atomRef index="703" />
<atomRef index="704" />
<atomRef index="713" />
<atomRef index="712" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="623" />
<atomRef index="624" />
<atomRef index="633" />
<atomRef index="632" />
<atomRef index="704" />
<atomRef index="705" />
<atomRef index="714" />
<atomRef index="713" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="624" />
<atomRef index="625" />
<atomRef index="634" />
<atomRef index="633" />
<atomRef index="705" />
<atomRef index="706" />
<atomRef index="715" />
<atomRef index="714" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="625" />
<atomRef index="626" />
<atomRef index="635" />
<atomRef index="634" />
<atomRef index="706" />
<atomRef index="707" />
<atomRef index="716" />
<atomRef index="715" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="626" />
<atomRef index="627" />
<atomRef index="636" />
<atomRef index="635" />
<atomRef index="707" />
<atomRef index="708" />
<atomRef index="717" />
<atomRef index="716" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="627" />
<atomRef index="628" />
<atomRef index="637" />
<atomRef index="636" />
<atomRef index="708" />
<atomRef index="709" />
<atomRef index="718" />
<atomRef index="717" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="628" />
<atomRef index="629" />
<atomRef index="638" />
<atomRef index="637" />
<atomRef index="709" />
<atomRef index="710" />
<atomRef index="719" />
<atomRef index="718" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="630" />
<atomRef index="631" />
<atomRef index="640" />
<atomRef index="639" />
<atomRef index="711" />
<atomRef index="712" />
<atomRef index="721" />
<atomRef index="720" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="631" />
<atomRef index="632" />
<atomRef index="641" />
<atomRef index="640" />
<atomRef index="712" />
<atomRef index="713" />
<atomRef index="722" />
<atomRef index="721" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="632" />
<atomRef index="633" />
<atomRef index="642" />
<atomRef index="641" />
<atomRef index="713" />
<atomRef index="714" />
<atomRef index="723" />
<atomRef index="722" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="633" />
<atomRef index="634" />
<atomRef index="643" />
<atomRef index="642" />
<atomRef index="714" />
<atomRef index="715" />
<atomRef index="724" />
<atomRef index="723" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="634" />
<atomRef index="635" />
<atomRef index="644" />
<atomRef index="643" />
<atomRef index="715" />
<atomRef index="716" />
<atomRef index="725" />
<atomRef index="724" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="635" />
<atomRef index="636" />
<atomRef index="645" />
<atomRef index="644" />
<atomRef index="716" />
<atomRef index="717" />
<atomRef index="726" />
<atomRef index="725" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="636" />
<atomRef index="637" />
<atomRef index="646" />
<atomRef index="645" />
<atomRef index="717" />
<atomRef index="718" />
<atomRef index="727" />
<atomRef index="726" />
</cell>
<cell>
<cellProperties   type="HEXAHEDRON"  />
<nrOfStructures value="8"/>
<atomRef index="637" />
<atomRef index="638" />
<atomRef index="647" />
<atomRef index="646" />
<atomRef index="718" />
<atomRef index="719" />
<atomRef index="728" />
<atomRef index="727" />
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Informative Components " >
<structuralComponent  name="border" >
<nrOfStructures value="1"/>
<cell>
<cellProperties   type="POLY_VERTEX"  name="border" />
<color r="1" g="0" b="0" a="0" />
<nrOfStructures value="386"/>
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="9" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="27" />
<atomRef index="28" />
<atomRef index="29" />
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="73" />
<atomRef index="74" />
<atomRef index="75" />
<atomRef index="76" />
<atomRef index="77" />
<atomRef index="78" />
<atomRef index="79" />
<atomRef index="80" />
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="90" />
<atomRef index="98" />
<atomRef index="99" />
<atomRef index="107" />
<atomRef index="108" />
<atomRef index="116" />
<atomRef index="117" />
<atomRef index="125" />
<atomRef index="126" />
<atomRef index="134" />
<atomRef index="135" />
<atomRef index="143" />
<atomRef index="144" />
<atomRef index="152" />
<atomRef index="153" />
<atomRef index="154" />
<atomRef index="155" />
<atomRef index="156" />
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="161" />
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="171" />
<atomRef index="179" />
<atomRef index="180" />
<atomRef index="188" />
<atomRef index="189" />
<atomRef index="197" />
<atomRef index="198" />
<atomRef index="206" />
<atomRef index="207" />
<atomRef index="215" />
<atomRef index="216" />
<atomRef index="224" />
<atomRef index="225" />
<atomRef index="233" />
<atomRef index="234" />
<atomRef index="235" />
<atomRef index="236" />
<atomRef index="237" />
<atomRef index="238" />
<atomRef index="239" />
<atomRef index="240" />
<atomRef index="241" />
<atomRef index="242" />
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="251" />
<atomRef index="252" />
<atomRef index="260" />
<atomRef index="261" />
<atomRef index="269" />
<atomRef index="270" />
<atomRef index="278" />
<atomRef index="279" />
<atomRef index="287" />
<atomRef index="288" />
<atomRef index="296" />
<atomRef index="297" />
<atomRef index="305" />
<atomRef index="306" />
<atomRef index="314" />
<atomRef index="315" />
<atomRef index="316" />
<atomRef index="317" />
<atomRef index="318" />
<atomRef index="319" />
<atomRef index="320" />
<atomRef index="321" />
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="327" />
<atomRef index="328" />
<atomRef index="329" />
<atomRef index="330" />
<atomRef index="331" />
<atomRef index="332" />
<atomRef index="333" />
<atomRef index="341" />
<atomRef index="342" />
<atomRef index="350" />
<atomRef index="351" />
<atomRef index="359" />
<atomRef index="360" />
<atomRef index="368" />
<atomRef index="369" />
<atomRef index="377" />
<atomRef index="378" />
<atomRef index="386" />
<atomRef index="387" />
<atomRef index="395" />
<atomRef index="396" />
<atomRef index="397" />
<atomRef index="398" />
<atomRef index="399" />
<atomRef index="400" />
<atomRef index="401" />
<atomRef index="402" />
<atomRef index="403" />
<atomRef index="404" />
<atomRef index="405" />
<atomRef index="406" />
<atomRef index="407" />
<atomRef index="408" />
<atomRef index="409" />
<atomRef index="410" />
<atomRef index="411" />
<atomRef index="412" />
<atomRef index="413" />
<atomRef index="414" />
<atomRef index="422" />
<atomRef index="423" />
<atomRef index="431" />
<atomRef index="432" />
<atomRef index="440" />
<atomRef index="441" />
<atomRef index="449" />
<atomRef index="450" />
<atomRef index="458" />
<atomRef index="459" />
<atomRef index="467" />
<atomRef index="468" />
<atomRef index="476" />
<atomRef index="477" />
<atomRef index="478" />
<atomRef index="479" />
<atomRef index="480" />
<atomRef index="481" />
<atomRef index="482" />
<atomRef index="483" />
<atomRef index="484" />
<atomRef index="485" />
<atomRef index="486" />
<atomRef index="487" />
<atomRef index="488" />
<atomRef index="489" />
<atomRef index="490" />
<atomRef index="491" />
<atomRef index="492" />
<atomRef index="493" />
<atomRef index="494" />
<atomRef index="495" />
<atomRef index="503" />
<atomRef index="504" />
<atomRef index="512" />
<atomRef index="513" />
<atomRef index="521" />
<atomRef index="522" />
<atomRef index="530" />
<atomRef index="531" />
<atomRef index="539" />
<atomRef index="540" />
<atomRef index="548" />
<atomRef index="549" />
<atomRef index="557" />
<atomRef index="558" />
<atomRef index="559" />
<atomRef index="560" />
<atomRef index="561" />
<atomRef index="562" />
<atomRef index="563" />
<atomRef index="564" />
<atomRef index="565" />
<atomRef index="566" />
<atomRef index="567" />
<atomRef index="568" />
<atomRef index="569" />
<atomRef index="570" />
<atomRef index="571" />
<atomRef index="572" />
<atomRef index="573" />
<atomRef index="574" />
<atomRef index="575" />
<atomRef index="576" />
<atomRef index="584" />
<atomRef index="585" />
<atomRef index="593" />
<atomRef index="594" />
<atomRef index="602" />
<atomRef index="603" />
<atomRef index="611" />
<atomRef index="612" />
<atomRef index="620" />
<atomRef index="621" />
<atomRef index="629" />
<atomRef index="630" />
<atomRef index="638" />
<atomRef index="639" />
<atomRef index="640" />
<atomRef index="641" />
<atomRef index="642" />
<atomRef index="643" />
<atomRef index="644" />
<atomRef index="645" />
<atomRef index="646" />
<atomRef index="647" />
<atomRef index="648" />
<atomRef index="649" />
<atomRef index="650" />
<atomRef index="651" />
<atomRef index="652" />
<atomRef index="653" />
<atomRef index="654" />
<atomRef index="655" />
<atomRef index="656" />
<atomRef index="657" />
<atomRef index="658" />
<atomRef index="659" />
<atomRef index="660" />
<atomRef index="661" />
<atomRef index="662" />
<atomRef index="663" />
<atomRef index="664" />
<atomRef index="665" />
<atomRef index="666" />
<atomRef index="667" />
<atomRef index="668" />
<atomRef index="669" />
<atomRef index="670" />
<atomRef index="671" />
<atomRef index="672" />
<atomRef index="673" />
<atomRef index="674" />
<atomRef index="675" />
<atomRef index="676" />
<atomRef index="677" />
<atomRef index="678" />
<atomRef index="679" />
<atomRef index="680" />
<atomRef index="681" />
<atomRef index="682" />
<atomRef index="683" />
<atomRef index="684" />
<atomRef index="685" />
<atomRef index="686" />
<atomRef index="687" />
<atomRef index="688" />
<atomRef index="689" />
<atomRef index="690" />
<atomRef index="691" />
<atomRef index="692" />
<atomRef index="693" />
<atomRef index="694" />
<atomRef index="695" />
<atomRef index="696" />
<atomRef index="697" />
<atomRef index="698" />
<atomRef index="699" />
<atomRef index="700" />
<atomRef index="701" />
<atomRef index="702" />
<atomRef index="703" />
<atomRef index="704" />
<atomRef index="705" />
<atomRef index="706" />
<atomRef index="707" />
<atomRef index="708" />
<atomRef index="709" />
<atomRef index="710" />
<atomRef index="711" />
<atomRef index="712" />
<atomRef index="713" />
<atomRef index="714" />
<atomRef index="715" />
<atomRef index="716" />
<atomRef index="717" />
<atomRef index="718" />
<atomRef index="719" />
<atomRef index="720" />
<atomRef index="721" />
<atomRef index="722" />
<atomRef index="723" />
<atomRef index="724" />
<atomRef index="725" />
<atomRef index="726" />
<atomRef index="727" />
<atomRef index="728" />
</cell>
</structuralComponent>
<structuralComponent  name="beads" mode="POINTS">


<cell>
  <cellProperties   type="POLY_VERTEX"  name="beads" />
  <color r="1" g="0" b="0" a="1" />
  <nrOfStructures value="343"/>
  <atomRef index="91" />
  <atomRef index="92" />
  <atomRef index="93" />
  <atomRef index="94" />
  <atomRef index="95" />
  <atomRef index="96" />
  <atomRef index="97" />
  <atomRef index="100" />
  <atomRef index="101" />
  <atomRef index="102" />
  <atomRef index="103" />
  <atomRef index="104" />
  <atomRef index="105" />
  <atomRef index="106" />
  <atomRef index="109" />
  <atomRef index="110" />
  <atomRef index="111" />
  <atomRef index="112" />
  <atomRef index="113" />
  <atomRef index="114" />
  <atomRef index="115" />
  <atomRef index="118" />
  <atomRef index="119" />
  <atomRef index="120" />
  <atomRef index="121" />
  <atomRef index="122" />
  <atomRef index="123" />
  <atomRef index="124" />
  <atomRef index="127" />
  <atomRef index="128" />
  <atomRef index="129" />
  <atomRef index="130" />
  <atomRef index="131" />
  <atomRef index="132" />
  <atomRef index="133" />
  <atomRef index="136" />
  <atomRef index="137" />
  <atomRef index="138" />
  <atomRef index="139" />
  <atomRef index="140" />
  <atomRef index="141" />
  <atomRef index="142" />
  <atomRef index="145" />
  <atomRef index="146" />
  <atomRef index="147" />
  <atomRef index="148" />
  <atomRef index="149" />
  <atomRef index="150" />
  <atomRef index="151" />
  <atomRef index="172" />
  <atomRef index="173" />
  <atomRef index="174" />
  <atomRef index="175" />
  <atomRef index="176" />
  <atomRef index="177" />
  <atomRef index="178" />
  <atomRef index="181" />
  <atomRef index="182" />
  <atomRef index="183" />
  <atomRef index="184" />
  <atomRef index="185" />
  <atomRef index="186" />
  <atomRef index="187" />
  <atomRef index="190" />
  <atomRef index="191" />
  <atomRef index="192" />
  <atomRef index="193" />
  <atomRef index="194" />
  <atomRef index="195" />
  <atomRef index="196" />
  <atomRef index="199" />
  <atomRef index="200" />
  <atomRef index="201" />
  <atomRef index="202" />
  <atomRef index="203" />
  <atomRef index="204" />
  <atomRef index="205" />
  <atomRef index="208" />
  <atomRef index="209" />
  <atomRef index="210" />
  <atomRef index="211" />
  <atomRef index="212" />
  <atomRef index="213" />
  <atomRef index="214" />
  <atomRef index="217" />
  <atomRef index="218" />
  <atomRef index="219" />
  <atomRef index="220" />
  <atomRef index="221" />
  <atomRef index="222" />
  <atomRef index="223" />
  <atomRef index="226" />
  <atomRef index="227" />
  <atomRef index="228" />
  <atomRef index="229" />
  <atomRef index="230" />
  <atomRef index="231" />
  <atomRef index="232" />
  <atomRef index="253" />
  <atomRef index="254" />
  <atomRef index="255" />
  <atomRef index="256" />
  <atomRef index="257" />
  <atomRef index="258" />
  <atomRef index="259" />
  <atomRef index="262" />
  <atomRef index="263" />
  <atomRef index="264" />
  <atomRef index="265" />
  <atomRef index="266" />
  <atomRef index="267" />
  <atomRef index="268" />
  <atomRef index="271" />
  <atomRef index="272" />
  <atomRef index="273" />
  <atomRef index="274" />
  <atomRef index="275" />
  <atomRef index="276" />
  <atomRef index="277" />
  <atomRef index="280" />
  <atomRef index="281" />
  <atomRef index="282" />
  <atomRef index="283" />
  <atomRef index="284" />
  <atomRef index="285" />
  <atomRef index="286" />
  <atomRef index="289" />
  <atomRef index="290" />
  <atomRef index="291" />
  <atomRef index="292" />
  <atomRef index="293" />
  <atomRef index="294" />
  <atomRef index="295" />
  <atomRef index="298" />
  <atomRef index="299" />
  <atomRef index="300" />
  <atomRef index="301" />
  <atomRef index="302" />
  <atomRef index="303" />
  <atomRef index="304" />
  <atomRef index="307" />
  <atomRef index="308" />
  <atomRef index="309" />
  <atomRef index="310" />
  <atomRef index="311" />
  <atomRef index="312" />
  <atomRef index="313" />
  <atomRef index="334" />
  <atomRef index="335" />
  <atomRef index="336" />
  <atomRef index="337" />
  <atomRef index="338" />
  <atomRef index="339" />
  <atomRef index="340" />
  <atomRef index="343" />
  <atomRef index="344" />
  <atomRef index="345" />
  <atomRef index="346" />
  <atomRef index="347" />
  <atomRef index="348" />
  <atomRef index="349" />
  <atomRef index="352" />
  <atomRef index="353" />
  <atomRef index="354" />
  <atomRef index="355" />
  <atomRef index="356" />
  <atomRef index="357" />
  <atomRef index="358" />
  <atomRef index="361" />
  <atomRef index="362" />
  <atomRef index="363" />
  <atomRef index="364" />
  <atomRef index="365" />
  <atomRef index="366" />
  <atomRef index="367" />
  <atomRef index="370" />
  <atomRef index="371" />
  <atomRef index="372" />
  <atomRef index="373" />
  <atomRef index="374" />
  <atomRef index="375" />
  <atomRef index="376" />
  <atomRef index="379" />
  <atomRef index="380" />
  <atomRef index="381" />
  <atomRef index="382" />
  <atomRef index="383" />
  <atomRef index="384" />
  <atomRef index="385" />
  <atomRef index="388" />
  <atomRef index="389" />
  <atomRef index="390" />
  <atomRef index="391" />
  <atomRef index="392" />
  <atomRef index="393" />
  <atomRef index="394" />
  <atomRef index="415" />
  <atomRef index="416" />
  <atomRef index="417" />
  <atomRef index="418" />
  <atomRef index="419" />
  <atomRef index="420" />
  <atomRef index="421" />
  <atomRef index="424" />
  <atomRef index="425" />
  <atomRef index="426" />
  <atomRef index="427" />
  <atomRef index="428" />
  <atomRef index="429" />
  <atomRef index="430" />
  <atomRef index="433" />
  <atomRef index="434" />
  <atomRef index="435" />
  <atomRef index="436" />
  <atomRef index="437" />
  <atomRef index="438" />
  <atomRef index="439" />
  <atomRef index="442" />
  <atomRef index="443" />
  <atomRef index="444" />
  <atomRef index="445" />
  <atomRef index="446" />
  <atomRef index="447" />
  <atomRef index="448" />
  <atomRef index="451" />
  <atomRef index="452" />
  <atomRef index="453" />
  <atomRef index="454" />
  <atomRef index="455" />
  <atomRef index="456" />
  <atomRef index="457" />
  <atomRef index="460" />
  <atomRef index="461" />
  <atomRef index="462" />
  <atomRef index="463" />
  <atomRef index="464" />
  <atomRef index="465" />
  <atomRef index="466" />
  <atomRef index="469" />
  <atomRef index="470" />
  <atomRef index="471" />
  <atomRef index="472" />
  <atomRef index="473" />
  <atomRef index="474" />
  <atomRef index="475" />
  <atomRef index="496" />
  <atomRef index="497" />
  <atomRef index="498" />
  <atomRef index="499" />
  <atomRef index="500" />
  <atomRef index="501" />
  <atomRef index="502" />
  <atomRef index="505" />
  <atomRef index="506" />
  <atomRef index="507" />
  <atomRef index="508" />
  <atomRef index="509" />
  <atomRef index="510" />
  <atomRef index="511" />
  <atomRef index="514" />
  <atomRef index="515" />
  <atomRef index="516" />
  <atomRef index="517" />
  <atomRef index="518" />
  <atomRef index="519" />
  <atomRef index="520" />
  <atomRef index="523" />
  <atomRef index="524" />
  <atomRef index="525" />
  <atomRef index="526" />
  <atomRef index="527" />
  <atomRef index="528" />
  <atomRef index="529" />
  <atomRef index="532" />
  <atomRef index="533" />
  <atomRef index="534" />
  <atomRef index="535" />
  <atomRef index="536" />
  <atomRef index="537" />
  <atomRef index="538" />
  <atomRef index="541" />
  <atomRef index="542" />
  <atomRef index="543" />
  <atomRef index="544" />
  <atomRef index="545" />
  <atomRef index="546" />
  <atomRef index="547" />
  <atomRef index="550" />
  <atomRef index="551" />
  <atomRef index="552" />
  <atomRef index="553" />
  <atomRef index="554" />
  <atomRef index="555" />
  <atomRef index="556" />
  <atomRef index="577" />
  <atomRef index="578" />
  <atomRef index="579" />
  <atomRef index="580" />
  <atomRef index="581" />
  <atomRef index="582" />
  <atomRef index="583" />
  <atomRef index="586" />
  <atomRef index="587" />
  <atomRef index="588" />
  <atomRef index="589" />
  <atomRef index="590" />
  <atomRef index="591" />
  <atomRef index="592" />
  <atomRef index="595" />
  <atomRef index="596" />
  <atomRef index="597" />
  <atomRef index="598" />
  <atomRef index="599" />
  <atomRef index="600" />
  <atomRef index="601" />
  <atomRef index="604" />
  <atomRef index="605" />
  <atomRef index="606" />
  <atomRef index="607" />
  <atomRef index="608" />
  <atomRef index="609" />
  <atomRef index="610" />
  <atomRef index="613" />
  <atomRef index="614" />
  <atomRef index="615" />
  <atomRef index="616" />
  <atomRef index="617" />
  <atomRef index="618" />
  <atomRef index="619" />
  <atomRef index="622" />
  <atomRef index="623" />
  <atomRef index="624" />
  <atomRef index="625" />
  <atomRef index="626" />
  <atomRef index="627" />
  <atomRef index="628" />
  <atomRef index="631" />
  <atomRef index="632" />
  <atomRef index="633" />
  <atomRef index="634" />
  <atomRef index="635" />
  <atomRef index="636" />
  <atomRef index="637" />
</cell>
</structuralComponent>

<structuralComponent  name="mid slice axial"  mode="WIREFRAME" >
<color r="1" g="0" b="0" a="1" />
<nrOfStructures value="49"/>
<atomRef index="334" />
<atomRef index="335" />
<atomRef index="336" />
<atomRef index="337" />
<atomRef index="338" />
<atomRef index="339" />
<atomRef index="340" />
<atomRef index="343" />
<atomRef index="344" />
<atomRef index="345" />
<atomRef index="346" />
<atomRef index="347" />
<atomRef index="348" />
<atomRef index="349" />
<atomRef index="352" />
<atomRef index="353" />
<atomRef index="354" />
<atomRef index="355" />
<atomRef index="356" />
<atomRef index="357" />
<atomRef index="358" />
<atomRef index="361" />
<atomRef index="362" />
<atomRef index="363" />
<atomRef index="364" />
<atomRef index="365" />
<atomRef index="366" />
<atomRef index="367" />
<atomRef index="370" />
<atomRef index="371" />
<atomRef index="372" />
<atomRef index="373" />
<atomRef index="374" />
<atomRef index="375" />
<atomRef index="376" />
<atomRef index="379" />
<atomRef index="380" />
<atomRef index="381" />
<atomRef index="382" />
<atomRef index="383" />
<atomRef index="384" />
<atomRef index="385" />
<atomRef index="388" />
<atomRef index="389" />
<atomRef index="390" />
<atomRef index="391" />
<atomRef index="392" />
<atomRef index="393" />
<atomRef index="394" />
</structuralComponent>
<structuralComponent  name="inner surface"  mode="WIREFRAME_AND_SURFACE" >
<nrOfStructures value="216"/>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="173" />
<atomRef index="172" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="174" />
<atomRef index="173" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="175" />
<atomRef index="174" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="176" />
<atomRef index="175" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="95" />
<atomRef index="96" />
<atomRef index="177" />
<atomRef index="176" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="96" />
<atomRef index="97" />
<atomRef index="178" />
<atomRef index="177" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="172" />
<atomRef index="173" />
<atomRef index="254" />
<atomRef index="253" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="173" />
<atomRef index="174" />
<atomRef index="255" />
<atomRef index="254" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="174" />
<atomRef index="175" />
<atomRef index="256" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="175" />
<atomRef index="176" />
<atomRef index="257" />
<atomRef index="256" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="176" />
<atomRef index="177" />
<atomRef index="258" />
<atomRef index="257" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="177" />
<atomRef index="178" />
<atomRef index="259" />
<atomRef index="258" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="254" />
<atomRef index="335" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="254" />
<atomRef index="255" />
<atomRef index="336" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="256" />
<atomRef index="337" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="257" />
<atomRef index="338" />
<atomRef index="337" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="257" />
<atomRef index="258" />
<atomRef index="339" />
<atomRef index="338" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="258" />
<atomRef index="259" />
<atomRef index="340" />
<atomRef index="339" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="335" />
<atomRef index="416" />
<atomRef index="415" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="335" />
<atomRef index="336" />
<atomRef index="417" />
<atomRef index="416" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="336" />
<atomRef index="337" />
<atomRef index="418" />
<atomRef index="417" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="337" />
<atomRef index="338" />
<atomRef index="419" />
<atomRef index="418" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="338" />
<atomRef index="339" />
<atomRef index="420" />
<atomRef index="419" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="339" />
<atomRef index="340" />
<atomRef index="421" />
<atomRef index="420" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="415" />
<atomRef index="416" />
<atomRef index="497" />
<atomRef index="496" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="416" />
<atomRef index="417" />
<atomRef index="498" />
<atomRef index="497" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="417" />
<atomRef index="418" />
<atomRef index="499" />
<atomRef index="498" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="418" />
<atomRef index="419" />
<atomRef index="500" />
<atomRef index="499" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="419" />
<atomRef index="420" />
<atomRef index="501" />
<atomRef index="500" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="420" />
<atomRef index="421" />
<atomRef index="502" />
<atomRef index="501" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="496" />
<atomRef index="497" />
<atomRef index="578" />
<atomRef index="577" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="497" />
<atomRef index="498" />
<atomRef index="579" />
<atomRef index="578" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="498" />
<atomRef index="499" />
<atomRef index="580" />
<atomRef index="579" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="499" />
<atomRef index="500" />
<atomRef index="581" />
<atomRef index="580" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="500" />
<atomRef index="501" />
<atomRef index="582" />
<atomRef index="581" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="501" />
<atomRef index="502" />
<atomRef index="583" />
<atomRef index="582" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="100" />
<atomRef index="101" />
<atomRef index="92" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="92" />
<atomRef index="101" />
<atomRef index="102" />
<atomRef index="93" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="93" />
<atomRef index="102" />
<atomRef index="103" />
<atomRef index="94" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="94" />
<atomRef index="103" />
<atomRef index="104" />
<atomRef index="95" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="95" />
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="96" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="96" />
<atomRef index="105" />
<atomRef index="106" />
<atomRef index="97" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="100" />
<atomRef index="109" />
<atomRef index="110" />
<atomRef index="101" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="101" />
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="102" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="103" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="103" />
<atomRef index="112" />
<atomRef index="113" />
<atomRef index="104" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="104" />
<atomRef index="113" />
<atomRef index="114" />
<atomRef index="105" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="105" />
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="106" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="109" />
<atomRef index="118" />
<atomRef index="119" />
<atomRef index="110" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="110" />
<atomRef index="119" />
<atomRef index="120" />
<atomRef index="111" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="111" />
<atomRef index="120" />
<atomRef index="121" />
<atomRef index="112" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="112" />
<atomRef index="121" />
<atomRef index="122" />
<atomRef index="113" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="113" />
<atomRef index="122" />
<atomRef index="123" />
<atomRef index="114" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="114" />
<atomRef index="123" />
<atomRef index="124" />
<atomRef index="115" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="118" />
<atomRef index="127" />
<atomRef index="128" />
<atomRef index="119" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="119" />
<atomRef index="128" />
<atomRef index="129" />
<atomRef index="120" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="120" />
<atomRef index="129" />
<atomRef index="130" />
<atomRef index="121" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="121" />
<atomRef index="130" />
<atomRef index="131" />
<atomRef index="122" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="122" />
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="123" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="123" />
<atomRef index="132" />
<atomRef index="133" />
<atomRef index="124" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="127" />
<atomRef index="136" />
<atomRef index="137" />
<atomRef index="128" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="128" />
<atomRef index="137" />
<atomRef index="138" />
<atomRef index="129" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="129" />
<atomRef index="138" />
<atomRef index="139" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="130" />
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="131" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="131" />
<atomRef index="140" />
<atomRef index="141" />
<atomRef index="132" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="132" />
<atomRef index="141" />
<atomRef index="142" />
<atomRef index="133" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="136" />
<atomRef index="145" />
<atomRef index="146" />
<atomRef index="137" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="137" />
<atomRef index="146" />
<atomRef index="147" />
<atomRef index="138" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="138" />
<atomRef index="147" />
<atomRef index="148" />
<atomRef index="139" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="139" />
<atomRef index="148" />
<atomRef index="149" />
<atomRef index="140" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="140" />
<atomRef index="149" />
<atomRef index="150" />
<atomRef index="141" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="141" />
<atomRef index="150" />
<atomRef index="151" />
<atomRef index="142" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="172" />
<atomRef index="181" />
<atomRef index="100" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="100" />
<atomRef index="181" />
<atomRef index="190" />
<atomRef index="109" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="109" />
<atomRef index="190" />
<atomRef index="199" />
<atomRef index="118" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="118" />
<atomRef index="199" />
<atomRef index="208" />
<atomRef index="127" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="127" />
<atomRef index="208" />
<atomRef index="217" />
<atomRef index="136" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="136" />
<atomRef index="217" />
<atomRef index="226" />
<atomRef index="145" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="172" />
<atomRef index="253" />
<atomRef index="262" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="181" />
<atomRef index="262" />
<atomRef index="271" />
<atomRef index="190" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="190" />
<atomRef index="271" />
<atomRef index="280" />
<atomRef index="199" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="199" />
<atomRef index="280" />
<atomRef index="289" />
<atomRef index="208" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="208" />
<atomRef index="289" />
<atomRef index="298" />
<atomRef index="217" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="217" />
<atomRef index="298" />
<atomRef index="307" />
<atomRef index="226" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="334" />
<atomRef index="343" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="343" />
<atomRef index="352" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="352" />
<atomRef index="361" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="280" />
<atomRef index="361" />
<atomRef index="370" />
<atomRef index="289" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="370" />
<atomRef index="379" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="379" />
<atomRef index="388" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="415" />
<atomRef index="424" />
<atomRef index="343" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="343" />
<atomRef index="424" />
<atomRef index="433" />
<atomRef index="352" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="352" />
<atomRef index="433" />
<atomRef index="442" />
<atomRef index="361" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="361" />
<atomRef index="442" />
<atomRef index="451" />
<atomRef index="370" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="370" />
<atomRef index="451" />
<atomRef index="460" />
<atomRef index="379" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="379" />
<atomRef index="460" />
<atomRef index="469" />
<atomRef index="388" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="415" />
<atomRef index="496" />
<atomRef index="505" />
<atomRef index="424" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="424" />
<atomRef index="505" />
<atomRef index="514" />
<atomRef index="433" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="433" />
<atomRef index="514" />
<atomRef index="523" />
<atomRef index="442" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="442" />
<atomRef index="523" />
<atomRef index="532" />
<atomRef index="451" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="451" />
<atomRef index="532" />
<atomRef index="541" />
<atomRef index="460" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="460" />
<atomRef index="541" />
<atomRef index="550" />
<atomRef index="469" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="496" />
<atomRef index="577" />
<atomRef index="586" />
<atomRef index="505" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="505" />
<atomRef index="586" />
<atomRef index="595" />
<atomRef index="514" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="514" />
<atomRef index="595" />
<atomRef index="604" />
<atomRef index="523" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="523" />
<atomRef index="604" />
<atomRef index="613" />
<atomRef index="532" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="532" />
<atomRef index="613" />
<atomRef index="622" />
<atomRef index="541" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="541" />
<atomRef index="622" />
<atomRef index="631" />
<atomRef index="550" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="97" />
<atomRef index="106" />
<atomRef index="187" />
<atomRef index="178" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="106" />
<atomRef index="115" />
<atomRef index="196" />
<atomRef index="187" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="115" />
<atomRef index="124" />
<atomRef index="205" />
<atomRef index="196" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="124" />
<atomRef index="133" />
<atomRef index="214" />
<atomRef index="205" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="133" />
<atomRef index="142" />
<atomRef index="223" />
<atomRef index="214" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="142" />
<atomRef index="151" />
<atomRef index="232" />
<atomRef index="223" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="178" />
<atomRef index="187" />
<atomRef index="268" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="187" />
<atomRef index="196" />
<atomRef index="277" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="196" />
<atomRef index="205" />
<atomRef index="286" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="205" />
<atomRef index="214" />
<atomRef index="295" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="214" />
<atomRef index="223" />
<atomRef index="304" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="223" />
<atomRef index="232" />
<atomRef index="313" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="268" />
<atomRef index="349" />
<atomRef index="340" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="268" />
<atomRef index="277" />
<atomRef index="358" />
<atomRef index="349" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="286" />
<atomRef index="367" />
<atomRef index="358" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="295" />
<atomRef index="376" />
<atomRef index="367" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="304" />
<atomRef index="385" />
<atomRef index="376" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="313" />
<atomRef index="394" />
<atomRef index="385" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="340" />
<atomRef index="349" />
<atomRef index="430" />
<atomRef index="421" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="349" />
<atomRef index="358" />
<atomRef index="439" />
<atomRef index="430" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="358" />
<atomRef index="367" />
<atomRef index="448" />
<atomRef index="439" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="367" />
<atomRef index="376" />
<atomRef index="457" />
<atomRef index="448" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="376" />
<atomRef index="385" />
<atomRef index="466" />
<atomRef index="457" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="385" />
<atomRef index="394" />
<atomRef index="475" />
<atomRef index="466" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="421" />
<atomRef index="430" />
<atomRef index="511" />
<atomRef index="502" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="430" />
<atomRef index="439" />
<atomRef index="520" />
<atomRef index="511" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="439" />
<atomRef index="448" />
<atomRef index="529" />
<atomRef index="520" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="448" />
<atomRef index="457" />
<atomRef index="538" />
<atomRef index="529" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="457" />
<atomRef index="466" />
<atomRef index="547" />
<atomRef index="538" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="466" />
<atomRef index="475" />
<atomRef index="556" />
<atomRef index="547" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="502" />
<atomRef index="511" />
<atomRef index="592" />
<atomRef index="583" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="511" />
<atomRef index="520" />
<atomRef index="601" />
<atomRef index="592" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="520" />
<atomRef index="529" />
<atomRef index="610" />
<atomRef index="601" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="529" />
<atomRef index="538" />
<atomRef index="619" />
<atomRef index="610" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="538" />
<atomRef index="547" />
<atomRef index="628" />
<atomRef index="619" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="547" />
<atomRef index="556" />
<atomRef index="637" />
<atomRef index="628" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="151" />
<atomRef index="150" />
<atomRef index="231" />
<atomRef index="232" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="150" />
<atomRef index="149" />
<atomRef index="230" />
<atomRef index="231" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="149" />
<atomRef index="148" />
<atomRef index="229" />
<atomRef index="230" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="148" />
<atomRef index="147" />
<atomRef index="228" />
<atomRef index="229" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="147" />
<atomRef index="146" />
<atomRef index="227" />
<atomRef index="228" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="146" />
<atomRef index="145" />
<atomRef index="226" />
<atomRef index="227" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="232" />
<atomRef index="231" />
<atomRef index="312" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="231" />
<atomRef index="230" />
<atomRef index="311" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="230" />
<atomRef index="229" />
<atomRef index="310" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="229" />
<atomRef index="228" />
<atomRef index="309" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="228" />
<atomRef index="227" />
<atomRef index="308" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="227" />
<atomRef index="226" />
<atomRef index="307" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="312" />
<atomRef index="393" />
<atomRef index="394" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="312" />
<atomRef index="311" />
<atomRef index="392" />
<atomRef index="393" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="311" />
<atomRef index="310" />
<atomRef index="391" />
<atomRef index="392" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="310" />
<atomRef index="309" />
<atomRef index="390" />
<atomRef index="391" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="309" />
<atomRef index="308" />
<atomRef index="389" />
<atomRef index="390" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="308" />
<atomRef index="307" />
<atomRef index="388" />
<atomRef index="389" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="394" />
<atomRef index="393" />
<atomRef index="474" />
<atomRef index="475" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="393" />
<atomRef index="392" />
<atomRef index="473" />
<atomRef index="474" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="392" />
<atomRef index="391" />
<atomRef index="472" />
<atomRef index="473" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="391" />
<atomRef index="390" />
<atomRef index="471" />
<atomRef index="472" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="390" />
<atomRef index="389" />
<atomRef index="470" />
<atomRef index="471" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="389" />
<atomRef index="388" />
<atomRef index="469" />
<atomRef index="470" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="475" />
<atomRef index="474" />
<atomRef index="555" />
<atomRef index="556" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="474" />
<atomRef index="473" />
<atomRef index="554" />
<atomRef index="555" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="473" />
<atomRef index="472" />
<atomRef index="553" />
<atomRef index="554" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="472" />
<atomRef index="471" />
<atomRef index="552" />
<atomRef index="553" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="471" />
<atomRef index="470" />
<atomRef index="551" />
<atomRef index="552" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="470" />
<atomRef index="469" />
<atomRef index="550" />
<atomRef index="551" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="556" />
<atomRef index="555" />
<atomRef index="636" />
<atomRef index="637" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="555" />
<atomRef index="554" />
<atomRef index="635" />
<atomRef index="636" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="554" />
<atomRef index="553" />
<atomRef index="634" />
<atomRef index="635" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="553" />
<atomRef index="552" />
<atomRef index="633" />
<atomRef index="634" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="552" />
<atomRef index="551" />
<atomRef index="632" />
<atomRef index="633" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="551" />
<atomRef index="550" />
<atomRef index="631" />
<atomRef index="632" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="577" />
<atomRef index="578" />
<atomRef index="587" />
<atomRef index="586" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="578" />
<atomRef index="579" />
<atomRef index="588" />
<atomRef index="587" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="579" />
<atomRef index="580" />
<atomRef index="589" />
<atomRef index="588" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="580" />
<atomRef index="581" />
<atomRef index="590" />
<atomRef index="589" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="581" />
<atomRef index="582" />
<atomRef index="591" />
<atomRef index="590" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="582" />
<atomRef index="583" />
<atomRef index="592" />
<atomRef index="591" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="586" />
<atomRef index="587" />
<atomRef index="596" />
<atomRef index="595" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="587" />
<atomRef index="588" />
<atomRef index="597" />
<atomRef index="596" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="588" />
<atomRef index="589" />
<atomRef index="598" />
<atomRef index="597" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="589" />
<atomRef index="590" />
<atomRef index="599" />
<atomRef index="598" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="590" />
<atomRef index="591" />
<atomRef index="600" />
<atomRef index="599" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="591" />
<atomRef index="592" />
<atomRef index="601" />
<atomRef index="600" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="595" />
<atomRef index="596" />
<atomRef index="605" />
<atomRef index="604" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="596" />
<atomRef index="597" />
<atomRef index="606" />
<atomRef index="605" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="597" />
<atomRef index="598" />
<atomRef index="607" />
<atomRef index="606" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="598" />
<atomRef index="599" />
<atomRef index="608" />
<atomRef index="607" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="599" />
<atomRef index="600" />
<atomRef index="609" />
<atomRef index="608" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="600" />
<atomRef index="601" />
<atomRef index="610" />
<atomRef index="609" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="604" />
<atomRef index="605" />
<atomRef index="614" />
<atomRef index="613" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="605" />
<atomRef index="606" />
<atomRef index="615" />
<atomRef index="614" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="606" />
<atomRef index="607" />
<atomRef index="616" />
<atomRef index="615" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="607" />
<atomRef index="608" />
<atomRef index="617" />
<atomRef index="616" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="608" />
<atomRef index="609" />
<atomRef index="618" />
<atomRef index="617" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="609" />
<atomRef index="610" />
<atomRef index="619" />
<atomRef index="618" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="613" />
<atomRef index="614" />
<atomRef index="623" />
<atomRef index="622" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="614" />
<atomRef index="615" />
<atomRef index="624" />
<atomRef index="623" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="615" />
<atomRef index="616" />
<atomRef index="625" />
<atomRef index="624" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="616" />
<atomRef index="617" />
<atomRef index="626" />
<atomRef index="625" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="617" />
<atomRef index="618" />
<atomRef index="627" />
<atomRef index="626" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="618" />
<atomRef index="619" />
<atomRef index="628" />
<atomRef index="627" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="622" />
<atomRef index="623" />
<atomRef index="632" />
<atomRef index="631" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="623" />
<atomRef index="624" />
<atomRef index="633" />
<atomRef index="632" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="624" />
<atomRef index="625" />
<atomRef index="634" />
<atomRef index="633" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="625" />
<atomRef index="626" />
<atomRef index="635" />
<atomRef index="634" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="626" />
<atomRef index="627" />
<atomRef index="636" />
<atomRef index="635" />
</cell>
<cell>
<cellProperties   type="QUAD"  />
<nrOfStructures value="4"/>
<atomRef index="627" />
<atomRef index="628" />
<atomRef index="637" />
<atomRef index="636" />
</cell>
</structuralComponent>
<structuralComponent  name="mid slice coronal"  mode="WIREFRAME_AND_SURFACE" >
<nrOfStructures value="81"/>
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="117" />
<atomRef index="125" />
<atomRef index="198" />
<atomRef index="206" />
<atomRef index="279" />
<atomRef index="287" />
<atomRef index="360" />
<atomRef index="368" />
<atomRef index="441" />
<atomRef index="449" />
<atomRef index="522" />
<atomRef index="530" />
<atomRef index="603" />
<atomRef index="611" />
<atomRef index="684" />
<atomRef index="685" />
<atomRef index="686" />
<atomRef index="687" />
<atomRef index="688" />
<atomRef index="689" />
<atomRef index="690" />
<atomRef index="691" />
<atomRef index="692" />
<atomRef index="604" />
<atomRef index="523" />
<atomRef index="442" />
<atomRef index="361" />
<atomRef index="280" />
<atomRef index="199" />
<atomRef index="118" />
<atomRef index="605" />
<atomRef index="524" />
<atomRef index="443" />
<atomRef index="362" />
<atomRef index="281" />
<atomRef index="200" />
<atomRef index="119" />
<atomRef index="606" />
<atomRef index="525" />
<atomRef index="444" />
<atomRef index="363" />
<atomRef index="282" />
<atomRef index="201" />
<atomRef index="120" />
<atomRef index="607" />
<atomRef index="526" />
<atomRef index="445" />
<atomRef index="364" />
<atomRef index="283" />
<atomRef index="202" />
<atomRef index="121" />
<atomRef index="608" />
<atomRef index="527" />
<atomRef index="446" />
<atomRef index="365" />
<atomRef index="284" />
<atomRef index="203" />
<atomRef index="122" />
<atomRef index="609" />
<atomRef index="528" />
<atomRef index="447" />
<atomRef index="366" />
<atomRef index="285" />
<atomRef index="204" />
<atomRef index="123" />
<atomRef index="610" />
<atomRef index="529" />
<atomRef index="448" />
<atomRef index="367" />
<atomRef index="286" />
<atomRef index="205" />
<atomRef index="124" />
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>
