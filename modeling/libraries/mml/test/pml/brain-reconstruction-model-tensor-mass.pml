<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model is a generic representation for 3D physical model (FEM, spring mass network, phymulob...) --> 
<physicalModel name="brain-reconstruction-model" nrOfAtoms="337"
 nrOfExclusiveComponents="2"
 nrOfInformativeComponents="2"
 nrOfCells="1701"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent  name="DOF"  mode="WIREFRAME_AND_SURFACE" >
<nrOfStructures value="337"/>
<atom>
<atomProperties index="0" x="131.251" y="93.7504" z="12.5384"  />
</atom>
<atom>
<atomProperties index="1" x="131.251" y="75.0739" z="24.9788"  />
</atom>
<atom>
<atomProperties index="2" x="112.574" y="93.7504" z="24.9788"  />
</atom>
<atom>
<atomProperties index="3" x="150.001" y="93.7504" z="12.5384"  />
</atom>
<atom>
<atomProperties index="4" x="150.001" y="75.0739" z="24.9788"  />
</atom>
<atom>
<atomProperties index="5" x="168.751" y="93.7504" z="12.5384"  />
</atom>
<atom>
<atomProperties index="6" x="168.751" y="75.0739" z="24.9788"  />
</atom>
<atom>
<atomProperties index="7" x="187.427" y="93.7504" z="24.9788"  />
</atom>
<atom>
<atomProperties index="8" x="112.5" y="112.5" z="12.5384"  />
</atom>
<atom>
<atomProperties index="9" x="93.8239" y="112.5" z="24.9788"  />
</atom>
<atom>
<atomProperties index="10" x="131.251" y="112.5" z="12.5384"  />
</atom>
<atom>
<atomProperties index="11" x="150.001" y="112.5" z="12.5384"  />
</atom>
<atom>
<atomProperties index="12" x="168.751" y="112.5" z="12.5384"  />
</atom>
<atom>
<atomProperties index="13" x="187.501" y="112.5" z="12.5384"  />
</atom>
<atom>
<atomProperties index="14" x="206.177" y="112.5" z="24.9788"  />
</atom>
<atom>
<atomProperties index="15" x="112.5" y="131.251" z="12.5384"  />
</atom>
<atom>
<atomProperties index="16" x="93.8239" y="131.251" z="24.9788"  />
</atom>
<atom>
<atomProperties index="17" x="131.251" y="131.251" z="12.5384"  />
</atom>
<atom>
<atomProperties index="18" x="150.001" y="131.251" z="12.5384"  />
</atom>
<atom>
<atomProperties index="19" x="168.751" y="131.251" z="12.5384"  />
</atom>
<atom>
<atomProperties index="20" x="187.501" y="131.251" z="12.5384"  />
</atom>
<atom>
<atomProperties index="21" x="206.177" y="131.251" z="24.9788"  />
</atom>
<atom>
<atomProperties index="22" x="112.5" y="149.927" z="24.9788"  />
</atom>
<atom>
<atomProperties index="23" x="131.251" y="149.927" z="24.9788"  />
</atom>
<atom>
<atomProperties index="24" x="150.001" y="149.927" z="24.9788"  />
</atom>
<atom>
<atomProperties index="25" x="168.751" y="149.927" z="24.9788"  />
</atom>
<atom>
<atomProperties index="26" x="187.501" y="149.927" z="24.9788"  />
</atom>
<atom>
<atomProperties index="27" x="131.251" y="56.3238" z="37.4683"  />
</atom>
<atom>
<atomProperties index="28" x="112.574" y="75.0003" z="37.4683"  />
</atom>
<atom>
<atomProperties index="29" x="150.001" y="56.3238" z="37.4683"  />
</atom>
<atom>
<atomProperties index="30" x="168.751" y="56.3238" z="37.4683"  />
</atom>
<atom>
<atomProperties index="31" x="187.427" y="75.0003" z="37.4683"  />
</atom>
<atom>
<atomProperties index="32" x="93.8239" y="93.7504" z="37.4683"  />
</atom>
<atom>
<atomProperties index="33" x="206.177" y="93.7504" z="37.4683"  />
</atom>
<atom>
<atomProperties index="34" x="93.8239" y="112.5" z="37.4683"  />
</atom>
<atom>
<atomProperties index="35" x="206.177" y="112.5" z="37.4683"  />
</atom>
<atom>
<atomProperties index="36" x="93.8239" y="131.251" z="37.4683"  />
</atom>
<atom>
<atomProperties index="37" x="206.177" y="131.251" z="37.4683"  />
</atom>
<atom>
<atomProperties index="38" x="112.5" y="149.927" z="37.4683"  />
</atom>
<atom>
<atomProperties index="39" x="131.251" y="149.927" z="37.4683"  />
</atom>
<atom>
<atomProperties index="40" x="150.001" y="149.927" z="37.4683"  />
</atom>
<atom>
<atomProperties index="41" x="168.751" y="149.927" z="37.4683"  />
</atom>
<atom>
<atomProperties index="42" x="187.501" y="149.927" z="37.4683"  />
</atom>
<atom>
<atomProperties index="43" x="131.251" y="56.3238" z="49.9577"  />
</atom>
<atom>
<atomProperties index="44" x="112.574" y="75.0003" z="49.9577"  />
</atom>
<atom>
<atomProperties index="45" x="150.001" y="56.3238" z="49.9577"  />
</atom>
<atom>
<atomProperties index="46" x="168.751" y="56.3238" z="49.9577"  />
</atom>
<atom>
<atomProperties index="47" x="187.501" y="56.3238" z="49.9577"  />
</atom>
<atom>
<atomProperties index="48" x="206.177" y="75.0003" z="49.9577"  />
</atom>
<atom>
<atomProperties index="49" x="93.8239" y="93.7504" z="49.9577"  />
</atom>
<atom>
<atomProperties index="50" x="206.177" y="93.7504" z="49.9577"  />
</atom>
<atom>
<atomProperties index="51" x="93.8239" y="112.5" z="49.9577"  />
</atom>
<atom>
<atomProperties index="52" x="224.927" y="112.5" z="49.9577"  />
</atom>
<atom>
<atomProperties index="53" x="75.0739" y="131.251" z="49.9577"  />
</atom>
<atom>
<atomProperties index="54" x="224.927" y="131.251" z="49.9577"  />
</atom>
<atom>
<atomProperties index="55" x="93.7504" y="149.927" z="49.9577"  />
</atom>
<atom>
<atomProperties index="56" x="206.177" y="150.001" z="49.9577"  />
</atom>
<atom>
<atomProperties index="57" x="112.5" y="168.751" z="37.5172"  />
</atom>
<atom>
<atomProperties index="58" x="93.8239" y="168.751" z="49.9577"  />
</atom>
<atom>
<atomProperties index="59" x="131.177" y="168.751" z="49.9577"  />
</atom>
<atom>
<atomProperties index="60" x="150.001" y="168.677" z="49.9577"  />
</atom>
<atom>
<atomProperties index="61" x="168.751" y="168.677" z="49.9577"  />
</atom>
<atom>
<atomProperties index="62" x="187.501" y="168.751" z="37.5172"  />
</atom>
<atom>
<atomProperties index="63" x="206.177" y="168.751" z="49.9577"  />
</atom>
<atom>
<atomProperties index="64" x="112.5" y="187.427" z="49.9577"  />
</atom>
<atom>
<atomProperties index="65" x="187.501" y="187.427" z="49.9577"  />
</atom>
<atom>
<atomProperties index="66" x="112.5" y="56.3238" z="62.4471"  />
</atom>
<atom>
<atomProperties index="67" x="93.8239" y="75.0003" z="62.4471"  />
</atom>
<atom>
<atomProperties index="68" x="131.251" y="56.3238" z="62.4471"  />
</atom>
<atom>
<atomProperties index="69" x="150.001" y="56.3238" z="62.4471"  />
</atom>
<atom>
<atomProperties index="70" x="168.751" y="56.3238" z="62.4471"  />
</atom>
<atom>
<atomProperties index="71" x="187.501" y="56.3238" z="62.4471"  />
</atom>
<atom>
<atomProperties index="72" x="206.177" y="75.0003" z="62.4471"  />
</atom>
<atom>
<atomProperties index="73" x="93.8239" y="93.7504" z="62.4471"  />
</atom>
<atom>
<atomProperties index="74" x="206.177" y="93.7504" z="62.4471"  />
</atom>
<atom>
<atomProperties index="75" x="75.0739" y="112.5" z="62.4471"  />
</atom>
<atom>
<atomProperties index="76" x="224.927" y="112.5" z="62.4471"  />
</atom>
<atom>
<atomProperties index="77" x="75.0739" y="131.251" z="62.4471"  />
</atom>
<atom>
<atomProperties index="78" x="224.927" y="131.251" z="62.4471"  />
</atom>
<atom>
<atomProperties index="79" x="75.0739" y="150.001" z="62.4471"  />
</atom>
<atom>
<atomProperties index="80" x="224.927" y="150.001" z="62.4471"  />
</atom>
<atom>
<atomProperties index="81" x="93.7504" y="168.677" z="62.4471"  />
</atom>
<atom>
<atomProperties index="82" x="224.927" y="168.751" z="62.4471"  />
</atom>
<atom>
<atomProperties index="83" x="112.5" y="187.427" z="62.4471"  />
</atom>
<atom>
<atomProperties index="84" x="131.251" y="187.427" z="62.4471"  />
</atom>
<atom>
<atomProperties index="85" x="150.001" y="187.427" z="62.4471"  />
</atom>
<atom>
<atomProperties index="86" x="168.751" y="187.427" z="62.4471"  />
</atom>
<atom>
<atomProperties index="87" x="206.177" y="187.501" z="62.4471"  />
</atom>
<atom>
<atomProperties index="88" x="187.501" y="206.177" z="62.4471"  />
</atom>
<atom>
<atomProperties index="89" x="112.5" y="56.3238" z="74.9365"  />
</atom>
<atom>
<atomProperties index="90" x="93.8239" y="75.0003" z="74.9365"  />
</atom>
<atom>
<atomProperties index="91" x="131.251" y="56.3238" z="74.9365"  />
</atom>
<atom>
<atomProperties index="92" x="150.001" y="56.3238" z="74.9365"  />
</atom>
<atom>
<atomProperties index="93" x="168.751" y="56.3238" z="74.9365"  />
</atom>
<atom>
<atomProperties index="94" x="187.501" y="56.3238" z="74.9365"  />
</atom>
<atom>
<atomProperties index="95" x="206.177" y="75.0003" z="74.9365"  />
</atom>
<atom>
<atomProperties index="96" x="93.8239" y="93.7504" z="74.9365"  />
</atom>
<atom>
<atomProperties index="97" x="224.927" y="93.7504" z="74.9365"  />
</atom>
<atom>
<atomProperties index="98" x="75.0739" y="112.5" z="74.9365"  />
</atom>
<atom>
<atomProperties index="99" x="224.927" y="112.5" z="74.9365"  />
</atom>
<atom>
<atomProperties index="100" x="75.0739" y="131.251" z="74.9365"  />
</atom>
<atom>
<atomProperties index="101" x="224.927" y="131.251" z="74.9365"  />
</atom>
<atom>
<atomProperties index="102" x="75.0739" y="150.001" z="74.9365"  />
</atom>
<atom>
<atomProperties index="103" x="224.927" y="150.001" z="74.9365"  />
</atom>
<atom>
<atomProperties index="104" x="75.0739" y="168.751" z="74.9365"  />
</atom>
<atom>
<atomProperties index="105" x="224.927" y="168.751" z="74.9365"  />
</atom>
<atom>
<atomProperties index="106" x="93.7504" y="187.427" z="74.9365"  />
</atom>
<atom>
<atomProperties index="107" x="206.177" y="187.501" z="74.9365"  />
</atom>
<atom>
<atomProperties index="108" x="112.5" y="206.177" z="74.9365"  />
</atom>
<atom>
<atomProperties index="109" x="131.251" y="206.177" z="74.9365"  />
</atom>
<atom>
<atomProperties index="110" x="150.001" y="206.177" z="74.9365"  />
</atom>
<atom>
<atomProperties index="111" x="168.751" y="206.177" z="74.9365"  />
</atom>
<atom>
<atomProperties index="112" x="187.501" y="206.177" z="74.9365"  />
</atom>
<atom>
<atomProperties index="113" x="112.5" y="75.0003" z="87.377"  />
</atom>
<atom>
<atomProperties index="114" x="131.251" y="56.3238" z="87.4259"  />
</atom>
<atom>
<atomProperties index="115" x="150.001" y="56.3238" z="87.4259"  />
</atom>
<atom>
<atomProperties index="116" x="168.751" y="56.3238" z="87.4259"  />
</atom>
<atom>
<atomProperties index="117" x="187.501" y="56.3238" z="87.4259"  />
</atom>
<atom>
<atomProperties index="118" x="206.177" y="75.0003" z="87.4259"  />
</atom>
<atom>
<atomProperties index="119" x="93.8239" y="93.7504" z="87.4259"  />
</atom>
<atom>
<atomProperties index="120" x="224.927" y="93.7504" z="87.4259"  />
</atom>
<atom>
<atomProperties index="121" x="75.0739" y="112.5" z="87.4259"  />
</atom>
<atom>
<atomProperties index="122" x="224.927" y="112.5" z="87.4259"  />
</atom>
<atom>
<atomProperties index="123" x="75.0739" y="131.251" z="87.4259"  />
</atom>
<atom>
<atomProperties index="124" x="224.927" y="131.251" z="87.4259"  />
</atom>
<atom>
<atomProperties index="125" x="75.0739" y="150.001" z="87.4259"  />
</atom>
<atom>
<atomProperties index="126" x="224.927" y="150.001" z="87.4259"  />
</atom>
<atom>
<atomProperties index="127" x="93.7504" y="168.677" z="87.4259"  />
</atom>
<atom>
<atomProperties index="128" x="224.927" y="168.751" z="87.4259"  />
</atom>
<atom>
<atomProperties index="129" x="93.8239" y="187.501" z="87.4259"  />
</atom>
<atom>
<atomProperties index="130" x="206.177" y="187.501" z="87.4259"  />
</atom>
<atom>
<atomProperties index="131" x="112.5" y="206.177" z="87.4259"  />
</atom>
<atom>
<atomProperties index="132" x="131.251" y="206.177" z="87.4259"  />
</atom>
<atom>
<atomProperties index="133" x="168.677" y="206.251" z="87.4259"  />
</atom>
<atom>
<atomProperties index="134" x="187.501" y="206.177" z="87.4259"  />
</atom>
<atom>
<atomProperties index="135" x="150.001" y="224.927" z="87.4259"  />
</atom>
<atom>
<atomProperties index="136" x="131.251" y="56.3238" z="99.9154"  />
</atom>
<atom>
<atomProperties index="137" x="112.574" y="75.0003" z="99.9154"  />
</atom>
<atom>
<atomProperties index="138" x="150.001" y="56.3238" z="99.9154"  />
</atom>
<atom>
<atomProperties index="139" x="168.751" y="56.3238" z="99.9154"  />
</atom>
<atom>
<atomProperties index="140" x="187.501" y="75.0003" z="99.8664"  />
</atom>
<atom>
<atomProperties index="141" x="93.8239" y="93.7504" z="99.9154"  />
</atom>
<atom>
<atomProperties index="142" x="206.251" y="93.7504" z="99.8664"  />
</atom>
<atom>
<atomProperties index="143" x="75.0739" y="112.5" z="99.9154"  />
</atom>
<atom>
<atomProperties index="144" x="224.927" y="112.5" z="99.9154"  />
</atom>
<atom>
<atomProperties index="145" x="75.0739" y="131.251" z="99.9154"  />
</atom>
<atom>
<atomProperties index="146" x="224.927" y="131.251" z="99.9154"  />
</atom>
<atom>
<atomProperties index="147" x="75.0739" y="150.001" z="99.9154"  />
</atom>
<atom>
<atomProperties index="148" x="224.927" y="150.001" z="99.9154"  />
</atom>
<atom>
<atomProperties index="149" x="93.7504" y="168.677" z="99.9154"  />
</atom>
<atom>
<atomProperties index="150" x="224.927" y="168.751" z="99.9154"  />
</atom>
<atom>
<atomProperties index="151" x="93.8239" y="187.501" z="99.9154"  />
</atom>
<atom>
<atomProperties index="152" x="206.177" y="187.501" z="99.9154"  />
</atom>
<atom>
<atomProperties index="153" x="112.5" y="206.177" z="99.9154"  />
</atom>
<atom>
<atomProperties index="154" x="206.177" y="206.251" z="99.9154"  />
</atom>
<atom>
<atomProperties index="155" x="131.251" y="224.927" z="99.9154"  />
</atom>
<atom>
<atomProperties index="156" x="150.001" y="224.927" z="99.9154"  />
</atom>
<atom>
<atomProperties index="157" x="168.751" y="224.927" z="99.9154"  />
</atom>
<atom>
<atomProperties index="158" x="187.501" y="224.927" z="99.9154"  />
</atom>
<atom>
<atomProperties index="159" x="131.251" y="75.0003" z="112.356"  />
</atom>
<atom>
<atomProperties index="160" x="150.001" y="75.0003" z="112.356"  />
</atom>
<atom>
<atomProperties index="161" x="168.751" y="75.0003" z="112.356"  />
</atom>
<atom>
<atomProperties index="162" x="112.5" y="93.7504" z="112.356"  />
</atom>
<atom>
<atomProperties index="163" x="187.501" y="93.7504" z="112.356"  />
</atom>
<atom>
<atomProperties index="164" x="93.7504" y="112.5" z="112.356"  />
</atom>
<atom>
<atomProperties index="165" x="206.251" y="112.5" z="112.356"  />
</atom>
<atom>
<atomProperties index="166" x="93.7504" y="131.251" z="112.356"  />
</atom>
<atom>
<atomProperties index="167" x="224.927" y="131.251" z="112.405"  />
</atom>
<atom>
<atomProperties index="168" x="93.7504" y="150.001" z="112.356"  />
</atom>
<atom>
<atomProperties index="169" x="224.927" y="150.001" z="112.405"  />
</atom>
<atom>
<atomProperties index="170" x="93.8239" y="168.751" z="112.405"  />
</atom>
<atom>
<atomProperties index="171" x="224.927" y="168.751" z="112.405"  />
</atom>
<atom>
<atomProperties index="172" x="93.8239" y="187.501" z="112.405"  />
</atom>
<atom>
<atomProperties index="173" x="206.177" y="187.501" z="112.405"  />
</atom>
<atom>
<atomProperties index="174" x="112.5" y="206.177" z="112.405"  />
</atom>
<atom>
<atomProperties index="175" x="187.501" y="206.251" z="112.356"  />
</atom>
<atom>
<atomProperties index="176" x="131.251" y="224.927" z="112.405"  />
</atom>
<atom>
<atomProperties index="177" x="150.001" y="224.927" z="112.405"  />
</atom>
<atom>
<atomProperties index="178" x="168.751" y="224.927" z="112.405"  />
</atom>
<atom>
<atomProperties index="179" x="131.251" y="93.7504" z="124.845"  />
</atom>
<atom>
<atomProperties index="180" x="150.001" y="75.0739" z="124.894"  />
</atom>
<atom>
<atomProperties index="181" x="168.751" y="75.0739" z="124.894"  />
</atom>
<atom>
<atomProperties index="182" x="187.427" y="93.7504" z="124.894"  />
</atom>
<atom>
<atomProperties index="183" x="112.5" y="93.8239" z="124.894"  />
</atom>
<atom>
<atomProperties index="184" x="93.8239" y="112.5" z="124.894"  />
</atom>
<atom>
<atomProperties index="185" x="206.177" y="112.5" z="124.894"  />
</atom>
<atom>
<atomProperties index="186" x="93.8239" y="131.251" z="124.894"  />
</atom>
<atom>
<atomProperties index="187" x="206.251" y="131.251" z="124.845"  />
</atom>
<atom>
<atomProperties index="188" x="93.8239" y="150.001" z="124.894"  />
</atom>
<atom>
<atomProperties index="189" x="206.251" y="150.001" z="124.845"  />
</atom>
<atom>
<atomProperties index="190" x="93.8239" y="168.751" z="124.894"  />
</atom>
<atom>
<atomProperties index="191" x="206.251" y="168.751" z="124.845"  />
</atom>
<atom>
<atomProperties index="192" x="112.5" y="187.427" z="124.894"  />
</atom>
<atom>
<atomProperties index="193" x="206.177" y="187.501" z="124.894"  />
</atom>
<atom>
<atomProperties index="194" x="131.251" y="206.177" z="124.894"  />
</atom>
<atom>
<atomProperties index="195" x="187.427" y="206.251" z="124.894"  />
</atom>
<atom>
<atomProperties index="196" x="150.001" y="224.927" z="124.894"  />
</atom>
<atom>
<atomProperties index="197" x="168.751" y="224.927" z="124.894"  />
</atom>
<atom>
<atomProperties index="198" x="150.001" y="93.7504" z="137.335"  />
</atom>
<atom>
<atomProperties index="199" x="168.751" y="93.7504" z="137.335"  />
</atom>
<atom>
<atomProperties index="200" x="112.5" y="112.5" z="137.335"  />
</atom>
<atom>
<atomProperties index="201" x="131.251" y="112.5" z="137.335"  />
</atom>
<atom>
<atomProperties index="202" x="150.001" y="112.5" z="137.335"  />
</atom>
<atom>
<atomProperties index="203" x="168.751" y="112.5" z="137.335"  />
</atom>
<atom>
<atomProperties index="204" x="187.501" y="112.5" z="137.335"  />
</atom>
<atom>
<atomProperties index="205" x="112.5" y="131.251" z="137.335"  />
</atom>
<atom>
<atomProperties index="206" x="187.501" y="131.251" z="137.335"  />
</atom>
<atom>
<atomProperties index="207" x="112.5" y="150.001" z="137.335"  />
</atom>
<atom>
<atomProperties index="208" x="187.501" y="150.001" z="137.335"  />
</atom>
<atom>
<atomProperties index="209" x="112.5" y="168.751" z="137.335"  />
</atom>
<atom>
<atomProperties index="210" x="187.501" y="168.751" z="137.335"  />
</atom>
<atom>
<atomProperties index="211" x="131.251" y="187.427" z="137.384"  />
</atom>
<atom>
<atomProperties index="212" x="187.501" y="187.501" z="137.335"  />
</atom>
<atom>
<atomProperties index="213" x="150.001" y="206.177" z="137.384"  />
</atom>
<atom>
<atomProperties index="214" x="168.751" y="206.177" z="137.384"  />
</atom>
<atom>
<atomProperties index="215" x="131.251" y="131.251" z="149.824"  />
</atom>
<atom>
<atomProperties index="216" x="150.001" y="131.251" z="149.824"  />
</atom>
<atom>
<atomProperties index="217" x="168.751" y="131.251" z="149.824"  />
</atom>
<atom>
<atomProperties index="218" x="131.251" y="150.001" z="149.824"  />
</atom>
<atom>
<atomProperties index="219" x="150.001" y="150.001" z="149.824"  />
</atom>
<atom>
<atomProperties index="220" x="168.751" y="150.001" z="149.824"  />
</atom>
<atom>
<atomProperties index="221" x="131.251" y="168.751" z="149.824"  />
</atom>
<atom>
<atomProperties index="222" x="150.001" y="168.751" z="149.824"  />
</atom>
<atom>
<atomProperties index="223" x="168.751" y="168.751" z="149.824"  />
</atom>
<atom>
<atomProperties index="224" x="150.001" y="187.501" z="149.824"  />
</atom>
<atom>
<atomProperties index="225" x="168.751" y="187.501" z="149.824"  />
</atom>
<atom>
<atomProperties index="226" x="101.74" y="195.417" z="80.2585"  />
</atom>
<atom>
<atomProperties index="227" x="196.839" y="159.339" z="43.713"  />
</atom>
<atom>
<atomProperties index="228" x="179.473" y="85.754" z="117.724"  />
</atom>
<atom>
<atomProperties index="229" x="140.589" y="159.339" z="43.713"  />
</atom>
<atom>
<atomProperties index="230" x="103.125" y="159.339" z="43.7374"  />
</atom>
<atom>
<atomProperties index="231" x="103.162" y="178.089" z="56.2024"  />
</atom>
<atom>
<atomProperties index="232" x="103.125" y="196.802" z="74.9365"  />
</atom>
<atom>
<atomProperties index="233" x="103.162" y="196.839" z="87.4259"  />
</atom>
<atom>
<atomProperties index="234" x="121.913" y="65.662" z="93.6707"  />
</atom>
<atom>
<atomProperties index="235" x="103.162" y="84.3754" z="93.6462"  />
</atom>
<atom>
<atomProperties index="236" x="178.126" y="215.552" z="93.6707"  />
</atom>
<atom>
<atomProperties index="237" x="178.126" y="84.3754" z="112.356"  />
</atom>
<atom>
<atomProperties index="238" x="178.089" y="84.4121" z="124.894"  />
</atom>
<atom>
<atomProperties index="239" x="196.839" y="196.876" z="118.625"  />
</atom>
<atom>
<atomProperties index="240" x="178.089" y="215.589" z="118.649"  />
</atom>
<atom>
<atomProperties index="241" x="103.162" y="178.089" z="49.9577"  />
</atom>
<atom>
<atomProperties index="242" x="103.125" y="178.052" z="62.4471"  />
</atom>
<atom>
<atomProperties index="243" x="121.876" y="65.662" z="87.4015"  />
</atom>
<atom>
<atomProperties index="244" x="121.913" y="65.662" z="99.9154"  />
</atom>
<atom>
<atomProperties index="245" x="103.162" y="84.3754" z="87.4015"  />
</atom>
<atom>
<atomProperties index="246" x="103.199" y="84.3754" z="99.9154"  />
</atom>
<atom>
<atomProperties index="247" x="196.802" y="196.876" z="124.894"  />
</atom>
<atom>
<atomProperties index="248" x="196.839" y="196.876" z="112.381"  />
</atom>
<atom>
<atomProperties index="249" x="178.126" y="215.589" z="112.381"  />
</atom>
<atom>
<atomProperties index="250" x="178.089" y="215.589" z="124.894"  />
</atom>
<atom>
<atomProperties index="251" x="125.041" y="96.8217" z="90.5238"  />
</atom>
<atom>
<atomProperties index="252" x="165.59" y="193.62" z="115.441"  />
</atom>
<atom>
<atomProperties index="253" x="160.951" y="103.125" z="114.881"  />
</atom>
<atom>
<atomProperties index="254" x="137.505" y="151.036" z="93.6707"  />
</atom>
<atom>
<atomProperties index="255" x="185.858" y="176.609" z="115.503"  />
</atom>
<atom>
<atomProperties index="256" x="141.684" y="78.3052" z="90.4193"  />
</atom>
<atom>
<atomProperties index="257" x="181.21" y="131.242" z="81.1812"  />
</atom>
<atom>
<atomProperties index="258" x="117.96" y="176.24" z="84.7117"  />
</atom>
<atom>
<atomProperties index="259" x="114.912" y="162.487" z="59.3017"  />
</atom>
<atom>
<atomProperties index="260" x="168.715" y="93.786" z="68.6918"  />
</atom>
<atom>
<atomProperties index="261" x="109.418" y="99.9694" z="96.744"  />
</atom>
<atom>
<atomProperties index="262" x="164.522" y="88.6039" z="117.714"  />
</atom>
<atom>
<atomProperties index="263" x="109.387" y="134.327" z="81.1812"  />
</atom>
<atom>
<atomProperties index="264" x="125" y="116.607" z="43.713"  />
</atom>
<atom>
<atomProperties index="265" x="163.064" y="209.834" z="115.426"  />
</atom>
<atom>
<atomProperties index="266" x="183.342" y="164.312" z="81.1812"  />
</atom>
<atom>
<atomProperties index="267" x="140.626" y="84.3761" z="56.2024"  />
</atom>
<atom>
<atomProperties index="268" x="182.394" y="191.834" z="115.503"  />
</atom>
<atom>
<atomProperties index="269" x="126.572" y="79.761" z="90.5084"  />
</atom>
<atom>
<atomProperties index="270" x="174.034" y="97.8426" z="112.954"  />
</atom>
<atom>
<atomProperties index="271" x="141.834" y="134.675" z="65.4546"  />
</atom>
<atom>
<atomProperties index="272" x="185.901" y="111.445" z="56.2024"  />
</atom>
<atom>
<atomProperties index="273" x="114.959" y="110.359" z="68.6918"  />
</atom>
<atom>
<atomProperties index="274" x="199.126" y="146.69" z="93.6707"  />
</atom>
<atom>
<atomProperties index="275" x="115.656" y="87.4739" z="90.5238"  />
</atom>
<atom>
<atomProperties index="276" x="174.978" y="203.045" z="115.497"  />
</atom>
<atom>
<atomProperties index="277" x="199.012" y="111.367" z="81.1812"  />
</atom>
<atom>
<atomProperties index="278" x="130.194" y="188.483" z="106.16"  />
</atom>
<atom>
<atomProperties index="279" x="116.73" y="159.303" z="106.081"  />
</atom>
<atom>
<atomProperties index="280" x="133.87" y="68.337" z="90.4654"  />
</atom>
<atom>
<atomProperties index="281" x="177.574" y="126.873" z="110.019"  />
</atom>
<atom>
<atomProperties index="282" x="201.73" y="135.123" z="68.6918"  />
</atom>
<atom>
<atomProperties index="283" x="188.536" y="89.6805" z="81.1812"  />
</atom>
<atom>
<atomProperties index="284" x="184.134" y="184.134" z="93.6707"  />
</atom>
<atom>
<atomProperties index="285" x="127.89" y="123.809" z="107.55"  />
</atom>
<atom>
<atomProperties index="286" x="167.768" y="109.903" z="89.9653"  />
</atom>
<atom>
<atomProperties index="287" x="106.764" y="183.898" z="85.4131"  />
</atom>
<atom>
<atomProperties index="288" x="115.347" y="193.992" z="81.7077"  />
</atom>
<atom>
<atomProperties index="289" x="161.625" y="76.6095" z="56.2024"  />
</atom>
<atom>
<atomProperties index="290" x="96.6947" y="115.371" z="81.1812"  />
</atom>
<atom>
<atomProperties index="291" x="167.183" y="85.943" z="91.192"  />
</atom>
<atom>
<atomProperties index="292" x="157.603" y="159.376" z="118.829"  />
</atom>
<atom>
<atomProperties index="293" x="113.984" y="87.9034" z="68.6918"  />
</atom>
<atom>
<atomProperties index="294" x="204.541" y="167.115" z="81.1812"  />
</atom>
<atom>
<atomProperties index="295" x="185.881" y="89.855" z="56.2024"  />
</atom>
<atom>
<atomProperties index="296" x="144.097" y="205.066" z="106.16"  />
</atom>
<atom>
<atomProperties index="297" x="140.626" y="132.286" z="31.2235"  />
</atom>
<atom>
<atomProperties index="298" x="92.7152" y="140.626" z="93.6707"  />
</atom>
<atom>
<atomProperties index="299" x="111.551" y="140.626" z="118.521"  />
</atom>
<atom>
<atomProperties index="300" x="92.7271" y="140.626" z="68.6918"  />
</atom>
<atom>
<atomProperties index="301" x="188.536" y="121.876" z="31.2235"  />
</atom>
<atom>
<atomProperties index="302" x="140.626" y="188.483" z="81.1812"  />
</atom>
<atom>
<atomProperties index="303" x="205.499" y="126.8" z="93.6707"  />
</atom>
<atom>
<atomProperties index="304" x="183.231" y="149.56" z="61.6038"  />
</atom>
<atom>
<atomProperties index="305" x="178.126" y="73.895" z="68.6918"  />
</atom>
<atom>
<atomProperties index="306" x="159.376" y="103.125" z="40.4087"  />
</atom>
<atom>
<atomProperties index="307" x="207.286" y="160.613" z="106.16"  />
</atom>
<atom>
<atomProperties index="308" x="207.233" y="120.147" z="56.2024"  />
</atom>
<atom>
<atomProperties index="309" x="111.551" y="119.59" z="118.521"  />
</atom>
<atom>
<atomProperties index="310" x="160.914" y="132.286" z="31.2235"  />
</atom>
<atom>
<atomProperties index="311" x="120.337" y="132.286" z="31.2235"  />
</atom>
<atom>
<atomProperties index="312" x="160.986" y="188.443" z="81.2407"  />
</atom>
<atom>
<atomProperties index="313" x="128.737" y="74.842" z="68.6918"  />
</atom>
<atom>
<atomProperties index="314" x="159.376" y="71.7121" z="81.1812"  />
</atom>
<atom>
<atomProperties index="315" x="192.653" y="175.458" z="68.6918"  />
</atom>
<atom>
<atomProperties index="316" x="207.355" y="153.186" z="68.6918"  />
</atom>
<atom>
<atomProperties index="317" x="103.199" y="84.3754" z="49.9577"  />
</atom>
<atom>
<atomProperties index="318" x="103.199" y="84.3754" z="37.4683"  />
</atom>
<atom>
<atomProperties index="319" x="122.865" y="94.6662" z="47.3708"  />
</atom>
<atom>
<atomProperties index="320" x="121.913" y="65.662" z="49.9577"  />
</atom>
<atom>
<atomProperties index="321" x="121.913" y="65.662" z="37.4683"  />
</atom>
<atom>
<atomProperties index="322" x="215.552" y="84.3754" z="74.9365"  />
</atom>
<atom>
<atomProperties index="323" x="215.552" y="84.3754" z="87.4259"  />
</atom>
<atom>
<atomProperties index="324" x="196.876" y="103.125" z="112.356"  />
</atom>
<atom>
<atomProperties index="325" x="196.802" y="103.125" z="124.894"  />
</atom>
<atom>
<atomProperties index="326" x="186.391" y="104.235" z="95.1812"  />
</atom>
<atom>
<atomProperties index="327" x="140.541" y="156.438" z="65.3207"  />
</atom>
<atom>
<atomProperties index="328" x="103.162" y="196.839" z="112.405"  />
</atom>
<atom>
<atomProperties index="329" x="111.094" y="116.03" z="31.2235"  />
</atom>
<atom>
<atomProperties index="330" x="112.715" y="178.858" z="103.917"  />
</atom>
<atom>
<atomProperties index="331" x="148.545" y="73.515" z="43.713"  />
</atom>
<atom>
<atomProperties index="332" x="104.054" y="154.947" z="93.6707"  />
</atom>
<atom>
<atomProperties index="333" x="139.186" y="140.626" z="125.617"  />
</atom>
<atom>
<atomProperties index="334" x="146.551" y="72.7769" z="68.6918"  />
</atom>
<atom>
<atomProperties index="335" x="182.898" y="112.206" z="118.543"  />
</atom>
<atom>
<atomProperties index="336" x="149.332" y="68.327" z="56.2024"  />
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Exclusive Components" >
<multiComponent name="Mechanical Objects" >
<structuralComponent  name="CamiTKModel"  mode="WIREFRAME_AND_SURFACE" >
<nrOfStructures value="337"/>
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="9" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="27" />
<atomRef index="28" />
<atomRef index="29" />
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="73" />
<atomRef index="74" />
<atomRef index="75" />
<atomRef index="76" />
<atomRef index="77" />
<atomRef index="78" />
<atomRef index="79" />
<atomRef index="80" />
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="90" />
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="96" />
<atomRef index="97" />
<atomRef index="98" />
<atomRef index="99" />
<atomRef index="100" />
<atomRef index="101" />
<atomRef index="102" />
<atomRef index="103" />
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="106" />
<atomRef index="107" />
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="113" />
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="116" />
<atomRef index="117" />
<atomRef index="118" />
<atomRef index="119" />
<atomRef index="120" />
<atomRef index="121" />
<atomRef index="122" />
<atomRef index="123" />
<atomRef index="124" />
<atomRef index="125" />
<atomRef index="126" />
<atomRef index="127" />
<atomRef index="128" />
<atomRef index="129" />
<atomRef index="130" />
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="135" />
<atomRef index="136" />
<atomRef index="137" />
<atomRef index="138" />
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="141" />
<atomRef index="142" />
<atomRef index="143" />
<atomRef index="144" />
<atomRef index="145" />
<atomRef index="146" />
<atomRef index="147" />
<atomRef index="148" />
<atomRef index="149" />
<atomRef index="150" />
<atomRef index="151" />
<atomRef index="152" />
<atomRef index="153" />
<atomRef index="154" />
<atomRef index="155" />
<atomRef index="156" />
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="161" />
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="171" />
<atomRef index="172" />
<atomRef index="173" />
<atomRef index="174" />
<atomRef index="175" />
<atomRef index="176" />
<atomRef index="177" />
<atomRef index="178" />
<atomRef index="179" />
<atomRef index="180" />
<atomRef index="181" />
<atomRef index="182" />
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="185" />
<atomRef index="186" />
<atomRef index="187" />
<atomRef index="188" />
<atomRef index="189" />
<atomRef index="190" />
<atomRef index="191" />
<atomRef index="192" />
<atomRef index="193" />
<atomRef index="194" />
<atomRef index="195" />
<atomRef index="196" />
<atomRef index="197" />
<atomRef index="198" />
<atomRef index="199" />
<atomRef index="200" />
<atomRef index="201" />
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="205" />
<atomRef index="206" />
<atomRef index="207" />
<atomRef index="208" />
<atomRef index="209" />
<atomRef index="210" />
<atomRef index="211" />
<atomRef index="212" />
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="215" />
<atomRef index="216" />
<atomRef index="217" />
<atomRef index="218" />
<atomRef index="219" />
<atomRef index="220" />
<atomRef index="221" />
<atomRef index="222" />
<atomRef index="223" />
<atomRef index="224" />
<atomRef index="225" />
<atomRef index="226" />
<atomRef index="227" />
<atomRef index="228" />
<atomRef index="229" />
<atomRef index="230" />
<atomRef index="231" />
<atomRef index="232" />
<atomRef index="233" />
<atomRef index="234" />
<atomRef index="235" />
<atomRef index="236" />
<atomRef index="237" />
<atomRef index="238" />
<atomRef index="239" />
<atomRef index="240" />
<atomRef index="241" />
<atomRef index="242" />
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="251" />
<atomRef index="252" />
<atomRef index="253" />
<atomRef index="254" />
<atomRef index="255" />
<atomRef index="256" />
<atomRef index="257" />
<atomRef index="258" />
<atomRef index="259" />
<atomRef index="260" />
<atomRef index="261" />
<atomRef index="262" />
<atomRef index="263" />
<atomRef index="264" />
<atomRef index="265" />
<atomRef index="266" />
<atomRef index="267" />
<atomRef index="268" />
<atomRef index="269" />
<atomRef index="270" />
<atomRef index="271" />
<atomRef index="272" />
<atomRef index="273" />
<atomRef index="274" />
<atomRef index="275" />
<atomRef index="276" />
<atomRef index="277" />
<atomRef index="278" />
<atomRef index="279" />
<atomRef index="280" />
<atomRef index="281" />
<atomRef index="282" />
<atomRef index="283" />
<atomRef index="284" />
<atomRef index="285" />
<atomRef index="286" />
<atomRef index="287" />
<atomRef index="288" />
<atomRef index="289" />
<atomRef index="290" />
<atomRef index="291" />
<atomRef index="292" />
<atomRef index="293" />
<atomRef index="294" />
<atomRef index="295" />
<atomRef index="296" />
<atomRef index="297" />
<atomRef index="298" />
<atomRef index="299" />
<atomRef index="300" />
<atomRef index="301" />
<atomRef index="302" />
<atomRef index="303" />
<atomRef index="304" />
<atomRef index="305" />
<atomRef index="306" />
<atomRef index="307" />
<atomRef index="308" />
<atomRef index="309" />
<atomRef index="310" />
<atomRef index="311" />
<atomRef index="312" />
<atomRef index="313" />
<atomRef index="314" />
<atomRef index="315" />
<atomRef index="316" />
<atomRef index="317" />
<atomRef index="318" />
<atomRef index="319" />
<atomRef index="320" />
<atomRef index="321" />
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="327" />
<atomRef index="328" />
<atomRef index="329" />
<atomRef index="330" />
<atomRef index="331" />
<atomRef index="332" />
<atomRef index="333" />
<atomRef index="334" />
<atomRef index="335" />
<atomRef index="336" />
</structuralComponent>
</multiComponent>
<multiComponent name="Elements" >
<structuralComponent  name="Tetras-CamiTKModel"  mode="WIREFRAME_AND_SURFACE" >
<nrOfStructures value="1185"/>
<cell>
<cellProperties index="0" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="38" />
<atomRef index="36" />
<atomRef index="16" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="1" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="266" />
<atomRef index="61" />
<atomRef index="86" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="2" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="136" />
<atomRef index="114" />
<atomRef index="234" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="3" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="96" />
<atomRef index="121" />
<atomRef index="119" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="4" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="252" />
<atomRef index="284" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="5" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="223" />
<atomRef index="252" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="6" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="311" />
<atomRef index="36" />
<atomRef index="16" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="7" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="6" />
<atomRef index="30" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="8" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="251" />
<atomRef index="162" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="9" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="82" />
<atomRef index="56" />
<atomRef index="80" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="10" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="142" />
<atomRef index="118" />
<atomRef index="140" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="11" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="8" />
<atomRef index="2" />
<atomRef index="0" />
<atomRef index="10" />
</cell>
<cell>
<cellProperties index="12" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="226" />
<atomRef index="106" />
<atomRef index="232" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="13" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="192" />
<atomRef index="174" />
<atomRef index="328" />
</cell>
<cell>
<cellProperties index="14" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="7" />
<atomRef index="5" />
<atomRef index="12" />
<atomRef index="13" />
</cell>
<cell>
<cellProperties index="15" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="250" />
<atomRef index="195" />
<atomRef index="214" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="16" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="107" />
<atomRef index="87" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="17" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="236" />
<atomRef index="134" />
<atomRef index="175" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="18" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="66" />
<atomRef index="313" />
<atomRef index="320" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties index="19" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="263" />
<atomRef index="254" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="20" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="48" />
<atomRef index="72" />
<atomRef index="74" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="21" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="75" />
<atomRef index="51" />
<atomRef index="53" />
<atomRef index="77" />
</cell>
<cell>
<cellProperties index="22" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="251" />
<atomRef index="179" />
<atomRef index="159" />
<atomRef index="256" />
</cell>
<cell>
<cellProperties index="23" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="39" />
<atomRef index="23" />
<atomRef index="40" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="24" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="37" />
<atomRef index="14" />
<atomRef index="35" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="25" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="178" />
<atomRef index="197" />
<atomRef index="177" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="26" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="77" />
<atomRef index="53" />
<atomRef index="55" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="27" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="29" />
<atomRef index="4" />
<atomRef index="1" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="28" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="170" />
<atomRef index="190" />
<atomRef index="188" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="29" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="73" />
<atomRef index="67" />
<atomRef index="49" />
<atomRef index="317" />
</cell>
<cell>
<cellProperties index="30" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="136" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="31" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="246" />
<atomRef index="162" />
<atomRef index="137" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="32" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="32" />
<atomRef index="2" />
<atomRef index="9" />
<atomRef index="34" />
</cell>
<cell>
<cellProperties index="33" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="259" />
<atomRef index="263" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="34" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="105" />
<atomRef index="82" />
<atomRef index="80" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="35" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="13" />
<atomRef index="12" />
<atomRef index="7" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="36" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="110" />
<atomRef index="135" />
<atomRef index="132" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="37" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="180" />
<atomRef index="159" />
<atomRef index="179" />
<atomRef index="160" />
</cell>
<cell>
<cellProperties index="38" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="173" />
<atomRef index="193" />
<atomRef index="239" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="39" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="87" />
<atomRef index="63" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="40" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="263" />
<atomRef index="166" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="41" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="86" />
<atomRef index="85" />
<atomRef index="60" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="42" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="175" />
<atomRef index="158" />
<atomRef index="134" />
<atomRef index="236" />
</cell>
<cell>
<cellProperties index="43" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="90" />
<atomRef index="67" />
<atomRef index="96" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="44" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="57" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="59" />
</cell>
<cell>
<cellProperties index="45" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="104" />
<atomRef index="79" />
<atomRef index="81" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties index="46" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="33" />
<atomRef index="31" />
<atomRef index="50" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="47" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="251" />
<atomRef index="271" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="48" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="256" />
<atomRef index="260" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="49" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="106" />
<atomRef index="83" />
<atomRef index="232" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="50" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="236" />
<atomRef index="175" />
<atomRef index="249" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="51" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="266" />
<atomRef index="274" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="52" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="240" />
<atomRef index="250" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="53" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="104" />
<atomRef index="127" />
<atomRef index="125" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties index="54" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="80" />
<atomRef index="56" />
<atomRef index="78" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="55" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="89" />
<atomRef index="243" />
<atomRef index="114" />
<atomRef index="91" />
</cell>
<cell>
<cellProperties index="56" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="242" />
<atomRef index="231" />
<atomRef index="83" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="57" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="271" />
<atomRef index="263" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="58" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="151" />
<atomRef index="172" />
<atomRef index="170" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="59" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="114" />
<atomRef index="138" />
<atomRef index="115" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="60" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="160" />
<atomRef index="159" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="61" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="146" />
<atomRef index="124" />
<atomRef index="122" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="62" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="228" />
<atomRef index="163" />
<atomRef index="237" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="63" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="178" />
<atomRef index="156" />
<atomRef index="157" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="64" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="68" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="65" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="32" />
<atomRef index="317" />
<atomRef index="318" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="66" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="277" />
<atomRef index="101" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="67" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="240" />
<atomRef index="175" />
<atomRef index="195" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="68" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="179" />
<atomRef index="160" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="69" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="208" />
<atomRef index="187" />
<atomRef index="206" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="70" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="268" />
<atomRef index="175" />
<atomRef index="248" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="71" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="100" />
<atomRef index="125" />
<atomRef index="123" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="72" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="164" />
<atomRef index="166" />
<atomRef index="186" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="73" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="246" />
<atomRef index="137" />
<atomRef index="235" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="74" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="248" />
<atomRef index="175" />
<atomRef index="134" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="75" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="254" />
<atomRef index="257" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="76" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="240" />
<atomRef index="178" />
<atomRef index="249" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="77" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="276" />
<atomRef index="252" />
<atomRef index="133" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="78" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="87" />
<atomRef index="63" />
<atomRef index="82" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="79" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="152" />
<atomRef index="248" />
<atomRef index="134" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="80" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="206" />
<atomRef index="203" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="81" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="270" />
<atomRef index="253" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="82" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="267" />
<atomRef index="256" />
<atomRef index="251" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="83" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="229" />
<atomRef index="40" />
<atomRef index="60" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="84" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="203" />
<atomRef index="199" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="85" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="223" />
<atomRef index="220" />
<atomRef index="219" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="86" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="178" />
<atomRef index="177" />
<atomRef index="156" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="87" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="140" />
<atomRef index="161" />
<atomRef index="237" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="88" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="140" />
<atomRef index="142" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="89" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="274" />
<atomRef index="187" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="90" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="47" />
<atomRef index="72" />
<atomRef index="48" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="91" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="267" />
<atomRef index="260" />
<atomRef index="256" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="92" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="36" />
<atomRef index="38" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="93" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="252" />
<atomRef index="211" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="94" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="208" />
<atomRef index="255" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="95" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="133" />
<atomRef index="111" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="96" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="260" />
<atomRef index="256" />
<atomRef index="251" />
<atomRef index="267" />
</cell>
<cell>
<cellProperties index="97" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="172" />
<atomRef index="192" />
<atomRef index="190" />
<atomRef index="170" />
</cell>
<cell>
<cellProperties index="98" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="138" />
<atomRef index="115" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="99" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="29" />
<atomRef index="1" />
<atomRef index="27" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="100" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="220" />
<atomRef index="208" />
<atomRef index="206" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="101" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="292" />
<atomRef index="254" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="102" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="214" />
<atomRef index="197" />
<atomRef index="250" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="103" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="251" />
<atomRef index="261" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="104" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="133" />
<atomRef index="156" />
<atomRef index="135" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="105" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="210" />
<atomRef index="223" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="106" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="188" />
<atomRef index="209" />
<atomRef index="207" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="107" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="38" />
<atomRef index="16" />
<atomRef index="22" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="108" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="219" />
<atomRef index="215" />
<atomRef index="218" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="109" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="115" />
<atomRef index="92" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="110" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="111" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="267" />
<atomRef index="251" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="112" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="272" />
<atomRef index="74" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="113" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="116" />
<atomRef index="92" />
<atomRef index="115" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="114" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="170" />
<atomRef index="192" />
<atomRef index="190" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="115" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="164" />
<atomRef index="261" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="116" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="102" />
<atomRef index="77" />
<atomRef index="79" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="117" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="170" />
<atomRef index="172" />
<atomRef index="192" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="118" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="226" />
<atomRef index="129" />
<atomRef index="106" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="119" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="214" />
<atomRef index="196" />
<atomRef index="197" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="120" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="236" />
<atomRef index="249" />
<atomRef index="157" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="121" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="113" />
<atomRef index="137" />
<atomRef index="234" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="122" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="323" />
<atomRef index="283" />
<atomRef index="95" />
<atomRef index="118" />
</cell>
<cell>
<cellProperties index="123" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="125" />
<atomRef index="149" />
<atomRef index="147" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="124" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="305" />
<atomRef index="93" />
<atomRef index="116" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="125" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="301" />
<atomRef index="19" />
<atomRef index="12" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="126" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="24" />
<atomRef index="41" />
<atomRef index="40" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="127" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="267" />
<atomRef index="251" />
<atomRef index="260" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties index="128" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="284" />
<atomRef index="266" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="129" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="240" />
<atomRef index="250" />
<atomRef index="197" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="130" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="272" />
<atomRef index="301" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="131" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="128" />
<atomRef index="150" />
<atomRef index="152" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="132" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="266" />
<atomRef index="257" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="133" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="198" />
<atomRef index="179" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="134" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="266" />
<atomRef index="274" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="135" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="271" />
<atomRef index="260" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="136" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="104" />
<atomRef index="81" />
<atomRef index="127" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties index="137" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="272" />
<atomRef index="260" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="138" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="166" />
<atomRef index="285" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="139" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="238" />
<atomRef index="199" />
<atomRef index="182" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="140" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="106" />
<atomRef index="129" />
<atomRef index="127" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="141" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="160" />
<atomRef index="179" />
<atomRef index="180" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="142" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="184" />
<atomRef index="205" />
<atomRef index="200" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="143" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="275" />
<atomRef index="261" />
<atomRef index="273" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="144" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="260" />
<atomRef index="283" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="145" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="312" />
<atomRef index="61" />
<atomRef index="266" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="146" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="42" />
<atomRef index="37" />
<atomRef index="272" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="147" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="11" />
<atomRef index="10" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="148" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="251" />
<atomRef index="260" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="149" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="150" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="323" />
<atomRef index="283" />
<atomRef index="277" />
<atomRef index="322" />
</cell>
<cell>
<cellProperties index="151" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="178" />
<atomRef index="240" />
<atomRef index="197" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="152" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="197" />
<atomRef index="196" />
<atomRef index="177" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="153" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="224" />
<atomRef index="213" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="154" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="102" />
<atomRef index="81" />
<atomRef index="127" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="155" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="37" />
<atomRef index="35" />
<atomRef index="54" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="156" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="55" />
<atomRef index="38" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="157" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="96" />
<atomRef index="261" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="158" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="214" />
<atomRef index="213" />
<atomRef index="196" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="159" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="102" />
<atomRef index="127" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="160" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="207" />
<atomRef index="205" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="161" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="90" />
<atomRef index="89" />
<atomRef index="66" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="162" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="113" />
<atomRef index="90" />
<atomRef index="245" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="163" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="82" />
<atomRef index="63" />
<atomRef index="56" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="164" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="283" />
<atomRef index="277" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="165" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="141" />
<atomRef index="164" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="166" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="12" />
<atomRef index="11" />
<atomRef index="3" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="167" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="68" />
<atomRef index="313" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="168" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="251" />
<atomRef index="162" />
<atomRef index="179" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="169" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="150" />
<atomRef index="173" />
<atomRef index="152" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="170" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="320" />
<atomRef index="267" />
<atomRef index="319" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties index="171" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="127" />
<atomRef index="149" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="172" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="140" />
<atomRef index="283" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="173" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="195" />
<atomRef index="175" />
<atomRef index="239" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="174" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="210" />
<atomRef index="191" />
<atomRef index="189" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="175" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="266" />
<atomRef index="254" />
<atomRef index="257" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="176" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="188" />
<atomRef index="190" />
<atomRef index="209" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="177" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="263" />
<atomRef index="290" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="178" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="267" />
<atomRef index="251" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="179" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="74" />
<atomRef index="50" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="180" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="207" />
<atomRef index="188" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="181" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="59" />
<atomRef index="84" />
<atomRef index="83" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="182" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="273" />
<atomRef index="51" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="183" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="100" />
<atomRef index="77" />
<atomRef index="102" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="184" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="69" />
<atomRef index="93" />
<atomRef index="70" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="185" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="270" />
<atomRef index="286" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="186" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="233" />
<atomRef index="328" />
<atomRef index="151" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="187" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="258" />
<atomRef index="127" />
<atomRef index="242" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="188" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="278" />
<atomRef index="328" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="189" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="59" />
<atomRef index="57" />
<atomRef index="38" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="190" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="244" />
<atomRef index="234" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="191" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="212" />
<atomRef index="210" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="192" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="317" />
<atomRef index="293" />
<atomRef index="67" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties index="193" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="100" />
<atomRef index="102" />
<atomRef index="125" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="194" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="301" />
<atomRef index="37" />
<atomRef index="272" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="195" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="137" />
<atomRef index="162" />
<atomRef index="159" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="196" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="47" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="197" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="174" />
<atomRef index="194" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="198" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="74" />
<atomRef index="72" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="199" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="103" />
<atomRef index="128" />
<atomRef index="105" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="200" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="331" />
<atomRef index="43" />
<atomRef index="45" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="201" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="14" />
<atomRef index="13" />
<atomRef index="7" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="202" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="122" />
<atomRef index="97" />
<atomRef index="120" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties index="203" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="199" />
<atomRef index="238" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="204" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="74" />
<atomRef index="272" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="205" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="81" />
<atomRef index="79" />
<atomRef index="55" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="206" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="238" />
<atomRef index="181" />
<atomRef index="199" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="207" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="331" />
<atomRef index="320" />
<atomRef index="43" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="208" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="37" />
<atomRef index="21" />
<atomRef index="14" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="209" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="210" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="274" />
<atomRef index="282" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="211" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="257" />
<atomRef index="274" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="212" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="272" />
<atomRef index="257" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="213" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="202" />
<atomRef index="201" />
<atomRef index="179" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties index="214" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="254" />
<atomRef index="258" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="215" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="42" />
<atomRef index="21" />
<atomRef index="37" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="216" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="290" />
<atomRef index="100" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="217" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="141" />
<atomRef index="164" />
<atomRef index="162" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties index="218" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="226" />
<atomRef index="233" />
<atomRef index="129" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="219" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="213" />
<atomRef index="196" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="220" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="35" />
<atomRef index="52" />
<atomRef index="54" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="221" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="331" />
<atomRef index="289" />
<atomRef index="267" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="222" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="324" />
<atomRef index="185" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="223" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="19" />
<atomRef index="18" />
<atomRef index="11" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="224" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="289" />
<atomRef index="70" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="225" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="180" />
<atomRef index="179" />
<atomRef index="198" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="226" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="187" />
<atomRef index="189" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="227" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="251" />
<atomRef index="269" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="228" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="276" />
<atomRef index="268" />
<atomRef index="252" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="229" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="189" />
<atomRef index="208" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="230" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="161" />
<atomRef index="160" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="231" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="208" />
<atomRef index="189" />
<atomRef index="187" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="232" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="112" />
<atomRef index="107" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="233" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="210" />
<atomRef index="223" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="234" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="62" />
<atomRef index="61" />
<atomRef index="42" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="235" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="94" />
<atomRef index="117" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="236" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="46" />
<atomRef index="30" />
<atomRef index="29" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="237" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="26" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="238" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="326" />
<atomRef index="281" />
<atomRef index="286" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="239" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="126" />
<atomRef index="103" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="240" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="112" />
<atomRef index="86" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="241" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="33" />
<atomRef index="7" />
<atomRef index="31" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="242" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="180" />
<atomRef index="199" />
<atomRef index="181" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="243" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="16" />
<atomRef index="9" />
<atomRef index="8" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="244" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="91" />
<atomRef index="68" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="245" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="326" />
<atomRef index="270" />
<atomRef index="163" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="246" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="311" />
<atomRef index="8" />
<atomRef index="10" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="247" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="86" />
<atomRef index="112" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="248" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="89" />
<atomRef index="243" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="249" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="130" />
<atomRef index="107" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="250" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="174" />
<atomRef index="194" />
<atomRef index="192" />
<atomRef index="278" />
</cell>
<cell>
<cellProperties index="251" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="317" />
<atomRef index="44" />
<atomRef index="28" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="252" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="29" />
<atomRef index="27" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="253" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="50" />
<atomRef index="48" />
<atomRef index="33" />
</cell>
<cell>
<cellProperties index="254" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="212" />
<atomRef index="191" />
<atomRef index="193" />
</cell>
<cell>
<cellProperties index="255" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="301" />
<atomRef index="12" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="256" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="46" />
<atomRef index="70" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="257" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="246" />
<atomRef index="141" />
<atomRef index="162" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties index="258" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="126" />
<atomRef index="128" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="259" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="220" />
<atomRef index="216" />
<atomRef index="219" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="260" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="260" />
<atomRef index="267" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="261" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="102" />
<atomRef index="79" />
<atomRef index="81" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="262" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="23" />
<atomRef index="17" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="263" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="283" />
<atomRef index="95" />
</cell>
<cell>
<cellProperties index="264" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="160" />
<atomRef index="159" />
<atomRef index="179" />
<atomRef index="256" />
</cell>
<cell>
<cellProperties index="265" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="66" />
<atomRef index="43" />
<atomRef index="320" />
<atomRef index="68" />
</cell>
<cell>
<cellProperties index="266" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="270" />
<atomRef index="237" />
<atomRef index="262" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="267" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="65" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="268" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="159" />
<atomRef index="244" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="269" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="72" />
<atomRef index="283" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="270" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="309" />
<atomRef index="299" />
<atomRef index="205" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="271" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="155" />
<atomRef index="176" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="272" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="173" />
<atomRef index="191" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="273" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="266" />
<atomRef index="304" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="274" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="242" />
<atomRef index="127" />
<atomRef index="81" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="275" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="132" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="276" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="83" />
<atomRef index="232" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="277" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="213" />
<atomRef index="211" />
<atomRef index="194" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="278" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="50" />
<atomRef index="35" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="279" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="10" />
<atomRef index="2" />
<atomRef index="0" />
<atomRef index="264" />
</cell>
<cell>
<cellProperties index="280" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="276" />
<atomRef index="134" />
<atomRef index="175" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="281" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="254" />
<atomRef index="278" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="282" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="258" />
<atomRef index="254" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="283" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="107" />
<atomRef index="134" />
<atomRef index="112" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="284" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="203" />
<atomRef index="253" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="285" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="186" />
<atomRef index="188" />
<atomRef index="207" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="286" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="201" />
<atomRef index="205" />
<atomRef index="215" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="287" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="270" />
<atomRef index="199" />
<atomRef index="182" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="288" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="132" />
<atomRef index="278" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="289" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="26" />
<atomRef index="21" />
<atomRef index="42" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="290" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="153" />
<atomRef index="174" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="291" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="90" />
<atomRef index="113" />
<atomRef index="89" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="292" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="27" />
<atomRef index="1" />
<atomRef index="321" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="293" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="301" />
<atomRef index="7" />
<atomRef index="272" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="294" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="268" />
<atomRef index="248" />
<atomRef index="255" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="295" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="98" />
<atomRef index="100" />
<atomRef index="123" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="296" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="297" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="263" />
<atomRef index="259" />
<atomRef index="38" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="298" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="105" />
<atomRef index="128" />
<atomRef index="130" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="299" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="105" />
<atomRef index="130" />
<atomRef index="107" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="300" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="30" />
<atomRef index="46" />
<atomRef index="289" />
</cell>
<cell>
<cellProperties index="301" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="83" />
<atomRef index="59" />
<atomRef index="64" />
<atomRef index="84" />
</cell>
<cell>
<cellProperties index="302" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="321" />
<atomRef index="1" />
<atomRef index="267" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="303" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="30" />
<atomRef index="46" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="304" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="203" />
<atomRef index="217" />
<atomRef index="206" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="305" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="172" />
<atomRef index="328" />
<atomRef index="192" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="306" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="145" />
<atomRef index="166" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="307" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="256" />
<atomRef index="260" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="308" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="255" />
<atomRef index="266" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="309" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="87" />
<atomRef index="65" />
<atomRef index="63" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="310" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="56" />
<atomRef index="227" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="311" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="77" />
<atomRef index="55" />
<atomRef index="53" />
<atomRef index="79" />
</cell>
<cell>
<cellProperties index="312" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="164" />
<atomRef index="162" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="313" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="136" />
<atomRef index="244" />
<atomRef index="159" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="314" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="268" />
<atomRef index="195" />
<atomRef index="175" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="315" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="223" />
<atomRef index="219" />
<atomRef index="222" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="316" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="234" />
<atomRef index="114" />
<atomRef index="243" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="317" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="260" />
<atomRef index="289" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="318" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="243" />
<atomRef index="114" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="319" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="222" />
<atomRef index="221" />
<atomRef index="211" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="320" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="231" />
<atomRef index="81" />
<atomRef index="58" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="321" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="210" />
<atomRef index="189" />
<atomRef index="208" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="322" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="268" />
<atomRef index="214" />
<atomRef index="195" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="323" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="186" />
<atomRef index="166" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="324" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="148" />
<atomRef index="169" />
<atomRef index="171" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="325" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="235" />
<atomRef index="245" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="326" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="118" />
<atomRef index="94" />
<atomRef index="117" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="327" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="105" />
<atomRef index="107" />
<atomRef index="82" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="328" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="146" />
<atomRef index="122" />
<atomRef index="144" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="329" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="107" />
<atomRef index="130" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="330" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="280" />
<atomRef index="243" />
<atomRef index="269" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="331" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="144" />
<atomRef index="122" />
<atomRef index="142" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties index="332" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="260" />
<atomRef index="283" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="333" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="239" />
<atomRef index="175" />
<atomRef index="248" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="334" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="268" />
<atomRef index="252" />
<atomRef index="214" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="335" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="21" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="336" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="258" />
<atomRef index="83" />
<atomRef index="242" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="337" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="258" />
<atomRef index="242" />
<atomRef index="127" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="338" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="286" />
<atomRef index="270" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="339" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="56" />
<atomRef index="78" />
<atomRef index="54" />
<atomRef index="80" />
</cell>
<cell>
<cellProperties index="340" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="160" />
<atomRef index="138" />
<atomRef index="136" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="341" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="106" />
<atomRef index="127" />
<atomRef index="242" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="342" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="236" />
<atomRef index="157" />
<atomRef index="133" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="343" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="226" />
<atomRef index="131" />
<atomRef index="233" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="344" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="254" />
<atomRef index="299" />
<atomRef index="263" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="345" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="189" />
<atomRef index="208" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="346" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="305" />
<atomRef index="289" />
<atomRef index="70" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="347" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="46" />
<atomRef index="71" />
<atomRef index="47" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="348" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="228" />
<atomRef index="237" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="349" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="151" />
<atomRef index="129" />
<atomRef index="233" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="350" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="311" />
<atomRef index="16" />
<atomRef index="15" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="351" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="280" />
<atomRef index="91" />
<atomRef index="243" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="352" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="247" />
<atomRef index="239" />
<atomRef index="193" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="353" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="219" />
<atomRef index="222" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="354" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="165" />
<atomRef index="142" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="355" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="238" />
<atomRef index="228" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="356" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="254" />
<atomRef index="281" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="357" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="272" />
<atomRef index="257" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="358" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="2" />
<atomRef index="1" />
<atomRef index="0" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="359" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="242" />
<atomRef index="81" />
<atomRef index="127" />
<atomRef index="106" />
</cell>
<cell>
<cellProperties index="360" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="227" />
<atomRef index="56" />
<atomRef index="63" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="361" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="113" />
<atomRef index="234" />
<atomRef index="243" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="362" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="226" />
<atomRef index="108" />
<atomRef index="131" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="363" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="86" />
<atomRef index="111" />
<atomRef index="85" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="364" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="283" />
<atomRef index="323" />
<atomRef index="142" />
</cell>
<cell>
<cellProperties index="365" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="106" />
<atomRef index="81" />
<atomRef index="127" />
<atomRef index="104" />
</cell>
<cell>
<cellProperties index="366" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="24" />
<atomRef index="40" />
<atomRef index="23" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="367" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="26" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="368" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="263" />
<atomRef index="273" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="369" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="24" />
<atomRef index="17" />
<atomRef index="18" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="370" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="50" />
<atomRef index="76" />
<atomRef index="52" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="371" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="236" />
<atomRef index="249" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="372" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="276" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="373" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="284" />
<atomRef index="130" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="374" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="140" />
<atomRef index="116" />
<atomRef index="139" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="375" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="44" />
<atomRef index="317" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="376" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="130" />
<atomRef index="152" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="377" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="89" />
<atomRef index="113" />
<atomRef index="243" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="378" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="223" />
<atomRef index="222" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="379" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="270" />
<atomRef index="262" />
<atomRef index="253" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="380" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="6" />
<atomRef index="30" />
<atomRef index="289" />
</cell>
<cell>
<cellProperties index="381" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="179" />
<atomRef index="202" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="382" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="238" />
<atomRef index="228" />
<atomRef index="181" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="383" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="229" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties index="384" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="237" />
<atomRef index="161" />
<atomRef index="228" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="385" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="25" />
<atomRef index="42" />
<atomRef index="41" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="386" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="35" />
<atomRef index="7" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="387" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="322" />
<atomRef index="95" />
<atomRef index="283" />
<atomRef index="74" />
</cell>
<cell>
<cellProperties index="388" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="196" />
<atomRef index="194" />
<atomRef index="177" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="389" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="283" />
<atomRef index="286" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="390" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="230" />
<atomRef index="38" />
<atomRef index="57" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="391" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="251" />
<atomRef index="271" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="392" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="123" />
<atomRef index="145" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="393" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="250" />
<atomRef index="214" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="394" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="148" />
<atomRef index="146" />
<atomRef index="169" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="395" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="203" />
<atomRef index="206" />
<atomRef index="204" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="396" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="12" />
<atomRef index="3" />
<atomRef index="5" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="397" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="248" />
<atomRef index="152" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="398" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="264" />
<atomRef index="11" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="399" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="119" />
<atomRef index="141" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="400" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="137" />
<atomRef index="113" />
<atomRef index="235" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="401" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="176" />
<atomRef index="174" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="402" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="289" />
<atomRef index="267" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="403" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="18" />
<atomRef index="17" />
<atomRef index="10" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="404" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="280" />
<atomRef index="115" />
<atomRef index="92" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="405" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="331" />
<atomRef index="267" />
<atomRef index="320" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="406" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="271" />
<atomRef index="272" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="407" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="174" />
<atomRef index="328" />
<atomRef index="278" />
</cell>
<cell>
<cellProperties index="408" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="152" />
<atomRef index="284" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="409" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="100" />
<atomRef index="75" />
<atomRef index="77" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="410" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="252" />
<atomRef index="133" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="411" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="254" />
<atomRef index="263" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="412" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="257" />
<atomRef index="254" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="413" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="76" />
<atomRef index="97" />
<atomRef index="99" />
<atomRef index="74" />
</cell>
<cell>
<cellProperties index="414" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="17" />
<atomRef index="15" />
<atomRef index="8" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="415" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="105" />
<atomRef index="103" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="416" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="214" />
<atomRef index="252" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="417" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="276" />
<atomRef index="175" />
<atomRef index="268" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="418" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="142" />
<atomRef index="120" />
<atomRef index="122" />
<atomRef index="144" />
</cell>
<cell>
<cellProperties index="419" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="420" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="247" />
<atomRef index="239" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="421" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="266" />
<atomRef index="257" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="422" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="191" />
<atomRef index="171" />
<atomRef index="169" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="423" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="258" />
<atomRef index="84" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="424" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="270" />
<atomRef index="253" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="425" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="312" />
<atomRef index="85" />
<atomRef index="60" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="426" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="234" />
<atomRef index="243" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="427" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="201" />
<atomRef index="202" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="428" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="278" />
<atomRef index="153" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="429" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="250" />
<atomRef index="240" />
<atomRef index="195" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="430" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="11" />
<atomRef index="10" />
<atomRef index="0" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="431" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="267" />
<atomRef index="289" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="432" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="245" />
<atomRef index="96" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="433" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="180" />
<atomRef index="198" />
<atomRef index="199" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="434" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="152" />
<atomRef index="173" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="435" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="179" />
<atomRef index="201" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="436" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="139" />
<atomRef index="117" />
<atomRef index="116" />
<atomRef index="140" />
</cell>
<cell>
<cellProperties index="437" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="323" />
<atomRef index="120" />
<atomRef index="277" />
<atomRef index="142" />
</cell>
<cell>
<cellProperties index="438" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="249" />
<atomRef index="240" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="439" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="110" />
<atomRef index="109" />
<atomRef index="84" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="440" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="202" />
<atomRef index="179" />
<atomRef index="201" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="441" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="298" />
<atomRef index="168" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="442" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="135" />
<atomRef index="156" />
<atomRef index="132" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="443" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="68" />
<atomRef index="43" />
<atomRef index="320" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="444" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="214" />
<atomRef index="195" />
<atomRef index="212" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="445" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="159" />
<atomRef index="251" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="446" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="43" />
<atomRef index="69" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="447" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="296" />
<atomRef index="133" />
<atomRef index="252" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="448" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="156" />
<atomRef index="133" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="449" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="149" />
<atomRef index="168" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="450" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="117" />
<atomRef index="94" />
<atomRef index="93" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="451" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="25" />
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="452" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="98" />
<atomRef index="123" />
<atomRef index="121" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="453" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="148" />
<atomRef index="124" />
<atomRef index="146" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="454" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="252" />
<atomRef index="133" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="455" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="260" />
<atomRef index="272" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="456" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="74" />
<atomRef index="99" />
<atomRef index="76" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties index="457" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="296" />
<atomRef index="132" />
<atomRef index="110" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="458" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="9" />
<atomRef index="2" />
<atomRef index="8" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="459" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="72" />
<atomRef index="95" />
<atomRef index="74" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="460" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="260" />
<atomRef index="272" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="461" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="196" />
<atomRef index="177" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="462" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="245" />
<atomRef index="235" />
<atomRef index="113" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="463" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="83" />
<atomRef index="64" />
<atomRef index="59" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="464" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="69" />
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="465" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="244" />
<atomRef index="234" />
<atomRef index="137" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="466" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="232" />
<atomRef index="108" />
<atomRef index="226" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="467" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="10" />
<atomRef index="8" />
<atomRef index="2" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="468" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="258" />
<atomRef index="127" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="469" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="126" />
<atomRef index="124" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="470" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="144" />
<atomRef index="142" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="471" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="241" />
<atomRef index="58" />
<atomRef index="230" />
<atomRef index="231" />
</cell>
<cell>
<cellProperties index="472" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="247" />
<atomRef index="195" />
<atomRef index="239" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="473" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="92" />
<atomRef index="115" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="474" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="48" />
<atomRef index="50" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="475" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="83" />
<atomRef index="106" />
<atomRef index="242" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="476" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="11" />
<atomRef index="0" />
<atomRef index="3" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="477" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="257" />
<atomRef index="266" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="478" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="266" />
<atomRef index="255" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="479" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="258" />
<atomRef index="254" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="480" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="156" />
<atomRef index="132" />
<atomRef index="155" />
<atomRef index="135" />
</cell>
<cell>
<cellProperties index="481" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="87" />
<atomRef index="107" />
<atomRef index="112" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="482" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="283" />
<atomRef index="142" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="483" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="119" />
<atomRef index="121" />
<atomRef index="143" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="484" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="108" />
<atomRef index="132" />
<atomRef index="131" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="485" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="124" />
<atomRef index="274" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="486" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="189" />
<atomRef index="169" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="487" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="205" />
<atomRef index="186" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="488" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="31" />
<atomRef index="47" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="489" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="162" />
<atomRef index="183" />
<atomRef index="179" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="490" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="33" />
<atomRef index="50" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="491" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="267" />
<atomRef index="313" />
<atomRef index="44" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties index="492" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="4" />
<atomRef index="3" />
<atomRef index="0" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="493" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="222" />
<atomRef index="219" />
<atomRef index="218" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="494" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="198" />
<atomRef index="179" />
<atomRef index="202" />
<atomRef index="253" />
</cell>
<cell>
<cellProperties index="495" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="205" />
<atomRef index="218" />
<atomRef index="215" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="496" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="191" />
<atomRef index="189" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="497" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="301" />
<atomRef index="12" />
<atomRef index="7" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="498" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="110" />
<atomRef index="132" />
<atomRef index="109" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="499" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="28" />
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="500" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="289" />
<atomRef index="260" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="501" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="97" />
<atomRef index="323" />
<atomRef index="322" />
</cell>
<cell>
<cellProperties index="502" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="120" />
<atomRef index="323" />
<atomRef index="277" />
<atomRef index="97" />
</cell>
<cell>
<cellProperties index="503" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="150" />
<atomRef index="171" />
<atomRef index="173" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="504" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="249" />
<atomRef index="236" />
<atomRef index="157" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties index="505" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="287" />
<atomRef index="258" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="506" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="263" />
<atomRef index="264" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties index="507" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="74" />
<atomRef index="97" />
<atomRef index="99" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties index="508" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="187" />
<atomRef index="167" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="509" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="263" />
<atomRef index="261" />
<atomRef index="251" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="510" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="280" />
<atomRef index="256" />
<atomRef index="115" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="511" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="6" />
<atomRef index="289" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="512" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="52" />
<atomRef index="76" />
<atomRef index="78" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="513" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="331" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="514" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="103" />
<atomRef index="78" />
<atomRef index="101" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="515" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="160" />
<atomRef index="253" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="516" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="275" />
<atomRef index="273" />
<atomRef index="251" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="517" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="162" />
<atomRef index="184" />
<atomRef index="183" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="518" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="103" />
<atomRef index="80" />
<atomRef index="78" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="519" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="37" />
<atomRef index="56" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="520" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="326" />
<atomRef index="324" />
<atomRef index="165" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="521" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="278" />
<atomRef index="292" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="522" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="248" />
<atomRef index="173" />
<atomRef index="239" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="523" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="115" />
<atomRef index="256" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="524" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="61" />
<atomRef index="42" />
<atomRef index="41" />
<atomRef index="62" />
</cell>
<cell>
<cellProperties index="525" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="148" />
<atomRef index="171" />
<atomRef index="150" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="526" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="56" />
<atomRef index="54" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="527" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="142" />
<atomRef index="122" />
<atomRef index="120" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties index="528" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="34" />
<atomRef index="32" />
<atomRef index="2" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="529" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="266" />
<atomRef index="257" />
<atomRef index="254" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="530" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="46" />
<atomRef index="31" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="531" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="257" />
<atomRef index="254" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="532" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="177" />
<atomRef index="176" />
<atomRef index="155" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="533" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="257" />
<atomRef index="272" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="534" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="260" />
<atomRef index="267" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="535" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="188" />
<atomRef index="168" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="536" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="187" />
<atomRef index="165" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="537" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="109" />
<atomRef index="132" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="538" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="312" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="539" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="270" />
<atomRef index="237" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="540" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="148" />
<atomRef index="126" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="541" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="83" />
<atomRef index="231" />
<atomRef index="64" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="542" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="246" />
<atomRef index="235" />
<atomRef index="141" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties index="543" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="162" />
<atomRef index="164" />
<atomRef index="184" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="544" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="280" />
<atomRef index="91" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="545" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="320" />
<atomRef index="313" />
<atomRef index="66" />
<atomRef index="68" />
</cell>
<cell>
<cellProperties index="546" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="263" />
<atomRef index="261" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="547" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="88" />
<atomRef index="87" />
<atomRef index="112" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="548" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="549" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="16" />
<atomRef index="8" />
<atomRef index="15" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="550" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="126" />
<atomRef index="101" />
<atomRef index="124" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="551" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="330" />
<atomRef index="258" />
<atomRef index="279" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="552" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="241" />
<atomRef index="231" />
<atomRef index="230" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="553" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="128" />
<atomRef index="152" />
<atomRef index="130" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="554" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="134" />
<atomRef index="154" />
<atomRef index="152" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties index="555" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="21" />
<atomRef index="20" />
<atomRef index="13" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="556" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="146" />
<atomRef index="144" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="557" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="82" />
<atomRef index="105" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="558" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="96" />
<atomRef index="119" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="559" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="140" />
<atomRef index="118" />
<atomRef index="117" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="560" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="18" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="561" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="312" />
<atomRef index="266" />
<atomRef index="254" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="562" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="200" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="563" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="227" />
<atomRef index="62" />
<atomRef index="42" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="564" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="305" />
<atomRef index="116" />
<atomRef index="291" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="565" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="92" />
<atomRef index="69" />
<atomRef index="68" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="566" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="182" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="567" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="264" />
<atomRef index="0" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="568" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="251" />
<atomRef index="159" />
<atomRef index="162" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="569" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="130" />
<atomRef index="152" />
<atomRef index="134" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="570" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="177" />
<atomRef index="176" />
<atomRef index="194" />
<atomRef index="196" />
</cell>
<cell>
<cellProperties index="571" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="186" />
<atomRef index="207" />
<atomRef index="205" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="572" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="271" />
<atomRef index="257" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="573" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="199" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="182" />
</cell>
<cell>
<cellProperties index="574" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="331" />
<atomRef index="46" />
<atomRef index="289" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="575" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="146" />
<atomRef index="167" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="576" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="57" />
<atomRef index="59" />
<atomRef index="64" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="577" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="75" />
<atomRef index="73" />
<atomRef index="51" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="578" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="258" />
<atomRef index="84" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="579" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="223" />
<atomRef index="208" />
<atomRef index="220" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="580" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="162" />
<atomRef index="251" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="581" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="30" />
<atomRef index="6" />
<atomRef index="4" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="582" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="184" />
<atomRef index="186" />
<atomRef index="205" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="583" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="253" />
<atomRef index="251" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="584" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="267" />
<atomRef index="251" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties index="585" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="263" />
<atomRef index="299" />
<atomRef index="254" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="586" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="320" />
<atomRef index="44" />
<atomRef index="319" />
<atomRef index="28" />
</cell>
<cell>
<cellProperties index="587" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="96" />
<atomRef index="73" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="588" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="159" />
<atomRef index="162" />
<atomRef index="179" />
<atomRef index="251" />
</cell>
<cell>
<cellProperties index="589" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="73" />
<atomRef index="49" />
<atomRef index="51" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties index="590" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="272" />
<atomRef index="74" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="591" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="178" />
<atomRef index="157" />
<atomRef index="249" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="592" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="56" />
<atomRef index="37" />
<atomRef index="54" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="593" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="191" />
<atomRef index="169" />
<atomRef index="189" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="594" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="30" />
<atomRef index="31" />
</cell>
<cell>
<cellProperties index="595" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="99" />
<atomRef index="76" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="596" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="156" />
<atomRef index="155" />
<atomRef index="132" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="597" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="194" />
<atomRef index="213" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="598" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="213" />
<atomRef index="194" />
<atomRef index="196" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="599" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="300" />
<atomRef index="127" />
<atomRef index="298" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="600" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="84" />
<atomRef index="109" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="601" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="253" />
<atomRef index="160" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="602" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="77" />
<atomRef index="75" />
<atomRef index="51" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="603" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="271" />
<atomRef index="254" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="604" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="121" />
<atomRef index="123" />
<atomRef index="145" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="605" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="39" />
<atomRef index="38" />
<atomRef index="22" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="606" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="191" />
<atomRef index="173" />
<atomRef index="171" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="607" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="121" />
<atomRef index="145" />
<atomRef index="143" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="608" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="311" />
<atomRef index="264" />
<atomRef index="36" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="609" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="34" />
<atomRef index="2" />
<atomRef index="9" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="610" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="2" />
<atomRef index="0" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="611" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="60" />
<atomRef index="84" />
<atomRef index="59" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="612" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="169" />
<atomRef index="146" />
<atomRef index="167" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="613" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="228" />
<atomRef index="182" />
<atomRef index="163" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="614" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="103" />
<atomRef index="126" />
<atomRef index="128" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="615" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="263" />
<atomRef index="251" />
<atomRef index="261" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties index="616" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="33" />
<atomRef index="14" />
<atomRef index="7" />
<atomRef index="35" />
</cell>
<cell>
<cellProperties index="617" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="289" />
<atomRef index="31" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="618" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="141" />
<atomRef index="143" />
<atomRef index="164" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="619" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="126" />
<atomRef index="124" />
<atomRef index="148" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="620" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="177" />
<atomRef index="155" />
<atomRef index="156" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="621" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="253" />
<atomRef index="199" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="622" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="35" />
<atomRef index="14" />
<atomRef index="7" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="623" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="271" />
<atomRef index="264" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="624" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="183" />
<atomRef index="200" />
<atomRef index="201" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="625" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="2" />
<atomRef index="32" />
<atomRef index="318" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="626" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="275" />
<atomRef index="269" />
<atomRef index="113" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="627" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="55" />
<atomRef index="53" />
<atomRef index="36" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="628" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="99" />
<atomRef index="277" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="629" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="4" />
<atomRef index="267" />
<atomRef index="1" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="630" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="272" />
<atomRef index="271" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="631" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="96" />
<atomRef index="98" />
<atomRef index="121" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="632" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="118" />
<atomRef index="95" />
<atomRef index="94" />
<atomRef index="283" />
</cell>
<cell>
<cellProperties index="633" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="164" />
<atomRef index="186" />
<atomRef index="184" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="634" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="142" />
<atomRef index="144" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="635" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="261" />
<atomRef index="162" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="636" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="280" />
<atomRef index="269" />
<atomRef index="256" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="637" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="252" />
<atomRef index="213" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="638" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="253" />
<atomRef index="256" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="639" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="154" />
<atomRef index="152" />
<atomRef index="173" />
<atomRef index="248" />
</cell>
<cell>
<cellProperties index="640" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="296" />
<atomRef index="278" />
<atomRef index="132" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="641" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="319" />
<atomRef index="51" />
<atomRef index="264" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="642" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="39" />
<atomRef index="23" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="643" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="167" />
<atomRef index="146" />
<atomRef index="144" />
<atomRef index="165" />
</cell>
<cell>
<cellProperties index="644" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="4" />
<atomRef index="6" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="645" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="57" />
<atomRef index="241" />
<atomRef index="230" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="646" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="81" />
<atomRef index="230" />
<atomRef index="58" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="647" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="230" />
<atomRef index="81" />
<atomRef index="55" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="648" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="297" />
<atomRef index="271" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="649" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="132" />
<atomRef index="155" />
<atomRef index="153" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="650" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="76" />
<atomRef index="74" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="651" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="103" />
<atomRef index="80" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="652" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="189" />
<atomRef index="167" />
<atomRef index="187" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="653" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="122" />
<atomRef index="99" />
<atomRef index="97" />
<atomRef index="277" />
</cell>
<cell>
<cellProperties index="654" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="270" />
<atomRef index="163" />
<atomRef index="237" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="655" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="101" />
<atomRef index="124" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="656" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="323" />
<atomRef index="142" />
<atomRef index="283" />
<atomRef index="118" />
</cell>
<cell>
<cellProperties index="657" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="168" />
<atomRef index="170" />
<atomRef index="188" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="658" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="263" />
<atomRef index="166" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="659" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="261" />
<atomRef index="96" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="660" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="51" />
<atomRef index="34" />
<atomRef index="36" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="661" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="199" />
<atomRef index="198" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="662" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="101" />
<atomRef index="99" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="663" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="154" />
<atomRef index="175" />
<atomRef index="158" />
<atomRef index="134" />
</cell>
<cell>
<cellProperties index="664" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="69" />
<atomRef index="68" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="665" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="271" />
<atomRef index="257" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="666" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="108" />
<atomRef index="83" />
<atomRef index="109" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="667" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="95" />
<atomRef index="94" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="668" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="309" />
<atomRef index="285" />
<atomRef index="299" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="669" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="234" />
<atomRef index="244" />
<atomRef index="136" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="670" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="27" />
<atomRef index="43" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="671" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="232" />
<atomRef index="83" />
<atomRef index="108" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="672" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="61" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="673" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="261" />
<atomRef index="263" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="674" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="166" />
<atomRef index="263" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="675" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="254" />
<atomRef index="258" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="676" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="136" />
<atomRef index="138" />
<atomRef index="114" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="677" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="142" />
<atomRef index="165" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="678" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="81" />
<atomRef index="55" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="679" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="26" />
<atomRef index="310" />
<atomRef index="19" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="680" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="220" />
<atomRef index="219" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="681" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="321" />
<atomRef index="28" />
<atomRef index="319" />
<atomRef index="1" />
</cell>
<cell>
<cellProperties index="682" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="204" />
<atomRef index="185" />
<atomRef index="325" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="683" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="85" />
<atomRef index="110" />
<atomRef index="84" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="684" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="143" />
<atomRef index="145" />
<atomRef index="166" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="685" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="126" />
<atomRef index="150" />
<atomRef index="128" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="686" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="145" />
<atomRef index="168" />
<atomRef index="166" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="687" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="143" />
<atomRef index="166" />
<atomRef index="164" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="688" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="102" />
<atomRef index="127" />
<atomRef index="125" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="689" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="51" />
<atomRef index="77" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="690" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="209" />
<atomRef index="211" />
<atomRef index="278" />
<atomRef index="192" />
</cell>
<cell>
<cellProperties index="691" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="162" />
<atomRef index="179" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="692" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="124" />
<atomRef index="99" />
<atomRef index="122" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="693" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="119" />
<atomRef index="141" />
<atomRef index="235" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties index="694" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="29" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="695" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="279" />
<atomRef index="263" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="696" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="251" />
<atomRef index="179" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="697" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="283" />
<atomRef index="260" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="698" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="124" />
<atomRef index="101" />
<atomRef index="99" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="699" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="51" />
<atomRef index="73" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="700" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="277" />
<atomRef index="272" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="701" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="274" />
<atomRef index="126" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="702" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="254" />
<atomRef index="257" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="703" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="160" />
<atomRef index="179" />
<atomRef index="256" />
</cell>
<cell>
<cellProperties index="704" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="123" />
<atomRef index="147" />
<atomRef index="145" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="705" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="107" />
<atomRef index="130" />
<atomRef index="134" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="706" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="135" />
<atomRef index="157" />
<atomRef index="156" />
<atomRef index="133" />
</cell>
<cell>
<cellProperties index="707" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="239" />
<atomRef index="248" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="708" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="71" />
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="709" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="274" />
<atomRef index="266" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="710" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="111" />
<atomRef index="134" />
<atomRef index="133" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="711" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="232" />
<atomRef index="226" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="712" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="87" />
<atomRef index="82" />
<atomRef index="107" />
<atomRef index="294" />
</cell>
<cell>
<cellProperties index="713" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="35" />
<atomRef index="50" />
<atomRef index="52" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="714" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="6" />
<atomRef index="5" />
<atomRef index="3" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="715" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="86" />
<atomRef index="266" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="716" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="248" />
<atomRef index="154" />
<atomRef index="134" />
<atomRef index="175" />
</cell>
<cell>
<cellProperties index="717" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="144" />
<atomRef index="122" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="718" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="137" />
<atomRef index="162" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="719" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="162" />
<atomRef index="251" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="720" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="247" />
<atomRef index="212" />
<atomRef index="195" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="721" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="66" />
<atomRef index="89" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="722" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="115" />
<atomRef index="138" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="723" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="247" />
<atomRef index="193" />
<atomRef index="212" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="724" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="79" />
<atomRef index="77" />
<atomRef index="55" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="725" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="228" />
<atomRef index="161" />
<atomRef index="181" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="726" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="142" />
<atomRef index="324" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="727" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="300" />
<atomRef index="298" />
<atomRef index="263" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="728" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="205" />
<atomRef index="207" />
<atomRef index="218" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="729" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="319" />
<atomRef index="264" />
<atomRef index="2" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="730" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="128" />
<atomRef index="150" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="731" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="227" />
<atomRef index="42" />
<atomRef index="56" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="732" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="222" />
<atomRef index="211" />
<atomRef index="221" />
<atomRef index="224" />
</cell>
<cell>
<cellProperties index="733" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="230" />
<atomRef index="55" />
<atomRef index="38" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="734" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="735" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="275" />
<atomRef index="96" />
<atomRef index="261" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="736" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="226" />
<atomRef index="233" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="737" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="133" />
<atomRef index="157" />
<atomRef index="156" />
<atomRef index="265" />
</cell>
<cell>
<cellProperties index="738" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="739" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="158" />
<atomRef index="236" />
<atomRef index="175" />
<atomRef index="249" />
</cell>
<cell>
<cellProperties index="740" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="319" />
<atomRef index="32" />
<atomRef index="34" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="741" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="125" />
<atomRef index="127" />
<atomRef index="149" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="742" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="145" />
<atomRef index="147" />
<atomRef index="168" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="743" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="19" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="744" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="272" />
<atomRef index="37" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="745" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="17" />
<atomRef index="8" />
<atomRef index="10" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="746" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="133" />
<atomRef index="236" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="747" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="236" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="748" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="228" />
<atomRef index="238" />
<atomRef index="182" />
<atomRef index="270" />
</cell>
<cell>
<cellProperties index="749" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="50" />
<atomRef index="74" />
<atomRef index="76" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="750" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="134" />
<atomRef index="154" />
<atomRef index="248" />
<atomRef index="152" />
</cell>
<cell>
<cellProperties index="751" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="267" />
<atomRef index="306" />
<atomRef index="4" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="752" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="168" />
<atomRef index="279" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="753" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="168" />
<atomRef index="166" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="754" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="211" />
<atomRef index="209" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="755" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="255" />
<atomRef index="274" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="756" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="202" />
<atomRef index="201" />
<atomRef index="216" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="757" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="251" />
<atomRef index="253" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="758" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="285" />
<atomRef index="254" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="759" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="189" />
<atomRef index="169" />
<atomRef index="167" />
<atomRef index="274" />
</cell>
<cell>
<cellProperties index="760" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="132" />
<atomRef index="153" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="761" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="81" />
<atomRef index="55" />
<atomRef index="58" />
<atomRef index="230" />
</cell>
<cell>
<cellProperties index="762" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="76" />
<atomRef index="99" />
<atomRef index="101" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="763" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="200" />
<atomRef index="205" />
<atomRef index="201" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="764" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="40" />
<atomRef index="271" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="765" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="194" />
<atomRef index="211" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="766" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="11" />
<atomRef index="297" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="767" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="258" />
<atomRef index="84" />
<atomRef index="83" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="768" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="257" />
<atomRef index="274" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="769" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="133" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="770" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="35" />
<atomRef index="7" />
<atomRef index="33" />
<atomRef index="272" />
</cell>
<cell>
<cellProperties index="771" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="23" />
<atomRef index="22" />
<atomRef index="15" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="772" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="224" />
<atomRef index="211" />
<atomRef index="213" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="773" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="268" />
<atomRef index="255" />
<atomRef index="252" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="774" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="86" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="88" />
</cell>
<cell>
<cellProperties index="775" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="153" />
<atomRef index="233" />
<atomRef index="131" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="776" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="330" />
<atomRef index="149" />
<atomRef index="127" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="777" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="214" />
<atomRef index="212" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="778" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="289" />
<atomRef index="70" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="779" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="166" />
<atomRef index="263" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="780" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="781" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="122" />
<atomRef index="99" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="782" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="264" />
<atomRef index="39" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="783" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="30" />
<atomRef index="4" />
<atomRef index="29" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="784" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="212" />
<atomRef index="210" />
<atomRef index="252" />
</cell>
<cell>
<cellProperties index="785" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="160" />
<atomRef index="180" />
<atomRef index="181" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="786" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="296" />
<atomRef index="278" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="787" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="282" />
<atomRef index="56" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="788" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="63" />
<atomRef index="82" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="789" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="182" />
<atomRef index="325" />
<atomRef index="163" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="790" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="101" />
<atomRef index="99" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="791" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="258" />
<atomRef index="83" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="792" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="147" />
<atomRef index="149" />
<atomRef index="168" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="793" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="169" />
<atomRef index="148" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="794" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="795" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="149" />
<atomRef index="151" />
<atomRef index="170" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="796" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="177" />
<atomRef index="194" />
<atomRef index="176" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="797" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="203" />
<atomRef index="199" />
<atomRef index="198" />
<atomRef index="253" />
</cell>
<cell>
<cellProperties index="798" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="161" />
<atomRef index="140" />
<atomRef index="139" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="799" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="179" />
<atomRef index="183" />
<atomRef index="201" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="800" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="35" />
<atomRef index="33" />
<atomRef index="50" />
<atomRef index="272" />
</cell>
<cell>
<cellProperties index="801" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="252" />
<atomRef index="212" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="802" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="182" />
<atomRef index="163" />
<atomRef index="270" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="803" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="96" />
<atomRef index="73" />
<atomRef index="98" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="804" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="81" />
<atomRef index="231" />
<atomRef index="242" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="805" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="7" />
<atomRef index="33" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="806" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="45" />
<atomRef index="70" />
<atomRef index="46" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="807" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="70" />
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="808" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="150" />
<atomRef index="152" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="809" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="231" />
<atomRef index="58" />
<atomRef index="230" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="810" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="229" />
<atomRef index="60" />
<atomRef index="59" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="811" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="84" />
<atomRef index="109" />
<atomRef index="83" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="812" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="91" />
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="813" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="214" />
<atomRef index="212" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="814" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="173" />
<atomRef index="191" />
<atomRef index="193" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="815" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="48" />
<atomRef index="74" />
<atomRef index="50" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="816" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="249" />
<atomRef index="158" />
<atomRef index="157" />
<atomRef index="178" />
</cell>
<cell>
<cellProperties index="817" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="12" />
<atomRef index="11" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="818" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="313" />
<atomRef index="267" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="819" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="179" />
<atomRef index="251" />
<atomRef index="256" />
</cell>
<cell>
<cellProperties index="820" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="198" />
<atomRef index="253" />
</cell>
<cell>
<cellProperties index="821" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="166" />
<atomRef index="188" />
<atomRef index="186" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="822" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="142" />
<atomRef index="163" />
<atomRef index="324" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="823" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="57" />
<atomRef index="64" />
<atomRef index="241" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="824" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="174" />
<atomRef index="176" />
<atomRef index="194" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="825" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="52" />
<atomRef index="78" />
<atomRef index="54" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="826" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="152" />
<atomRef index="255" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="827" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="255" />
<atomRef index="266" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="828" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="223" />
<atomRef index="225" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="829" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="261" />
<atomRef index="263" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="830" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="90" />
<atomRef index="96" />
<atomRef index="119" />
<atomRef index="245" />
</cell>
<cell>
<cellProperties index="831" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="262" />
<atomRef index="237" />
<atomRef index="161" />
<atomRef index="291" />
</cell>
<cell>
<cellProperties index="832" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="301" />
<atomRef index="272" />
<atomRef index="35" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="833" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="166" />
<atomRef index="263" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="834" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="256" />
<atomRef index="159" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="835" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="153" />
<atomRef index="132" />
<atomRef index="288" />
</cell>
<cell>
<cellProperties index="836" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="326" />
<atomRef index="165" />
<atomRef index="281" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="837" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="241" />
<atomRef index="57" />
<atomRef index="230" />
<atomRef index="58" />
</cell>
<cell>
<cellProperties index="838" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="233" />
<atomRef index="151" />
<atomRef index="287" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="839" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="264" />
<atomRef index="39" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="840" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="278" />
<atomRef index="258" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="841" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="39" />
<atomRef index="38" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties index="842" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="266" />
<atomRef index="255" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="843" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="119" />
<atomRef index="143" />
<atomRef index="141" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="844" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="266" />
<atomRef index="284" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="845" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties index="846" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="38" />
<atomRef index="263" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties index="847" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="263" />
<atomRef index="38" />
<atomRef index="259" />
<atomRef index="271" />
</cell>
<cell>
<cellProperties index="848" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="185" />
<atomRef index="206" />
<atomRef index="187" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="849" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="103" />
<atomRef index="126" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="850" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="311" />
<atomRef index="10" />
<atomRef index="264" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="851" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="98" />
<atomRef index="73" />
<atomRef index="75" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="852" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="277" />
<atomRef index="260" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="853" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="36" />
<atomRef index="9" />
<atomRef index="16" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="854" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="149" />
<atomRef index="170" />
<atomRef index="168" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="855" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="264" />
<atomRef index="267" />
<atomRef index="273" />
</cell>
<cell>
<cellProperties index="856" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="64" />
<atomRef index="231" />
<atomRef index="241" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="857" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="209" />
<atomRef index="330" />
<atomRef index="279" />
</cell>
<cell>
<cellProperties index="858" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="163" />
<atomRef index="142" />
<atomRef index="140" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="859" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="85" />
<atomRef index="84" />
<atomRef index="60" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="860" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="50" />
<atomRef index="74" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="861" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="63" />
<atomRef index="294" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="862" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="260" />
<atomRef index="251" />
<atomRef index="256" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="863" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="73" />
<atomRef index="96" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="864" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="256" />
<atomRef index="280" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="865" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="59" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="866" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="295" />
</cell>
<cell>
<cellProperties index="867" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="42" />
<atomRef index="310" />
<atomRef index="41" />
</cell>
<cell>
<cellProperties index="868" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="278" />
<atomRef index="211" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="869" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="151" />
<atomRef index="127" />
<atomRef index="129" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="870" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="321" />
<atomRef index="319" />
<atomRef index="267" />
<atomRef index="1" />
</cell>
<cell>
<cellProperties index="871" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="301" />
<atomRef index="35" />
<atomRef index="37" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="872" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="260" />
<atomRef index="289" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="873" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="311" />
<atomRef index="15" />
<atomRef index="8" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="874" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="162" />
<atomRef index="246" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="875" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="319" />
<atomRef index="2" />
<atomRef index="32" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="876" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="245" />
<atomRef index="96" />
<atomRef index="119" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties index="877" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="305" />
<atomRef index="70" />
<atomRef index="93" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="878" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="245" />
<atomRef index="119" />
<atomRef index="235" />
<atomRef index="261" />
</cell>
<cell>
<cellProperties index="879" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="277" />
<atomRef index="99" />
<atomRef index="101" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="880" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="20" />
<atomRef index="19" />
<atomRef index="12" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="881" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="123" />
<atomRef index="125" />
<atomRef index="147" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="882" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="166" />
<atomRef index="168" />
<atomRef index="188" />
<atomRef index="299" />
</cell>
<cell>
<cellProperties index="883" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="305" />
<atomRef index="291" />
<atomRef index="260" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="884" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="78" />
<atomRef index="101" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="885" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="330" />
<atomRef index="127" />
<atomRef index="258" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="886" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="191" />
<atomRef index="212" />
<atomRef index="255" />
<atomRef index="210" />
</cell>
<cell>
<cellProperties index="887" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="305" />
<atomRef index="260" />
<atomRef index="289" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="888" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="56" />
<atomRef index="54" />
<atomRef index="78" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="889" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="124" />
<atomRef index="146" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="890" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="274" />
<atomRef index="257" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="891" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="126" />
<atomRef index="148" />
<atomRef index="150" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="892" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="41" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="893" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="103" />
<atomRef index="101" />
<atomRef index="126" />
<atomRef index="282" />
</cell>
<cell>
<cellProperties index="894" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="70" />
<atomRef index="46" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="895" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="257" />
<atomRef index="271" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="896" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="90" />
<atomRef index="96" />
<atomRef index="245" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="897" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="163" />
<atomRef index="325" />
<atomRef index="324" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="898" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="37" />
<atomRef index="42" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="899" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="68" />
<atomRef index="320" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="900" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="70" />
<atomRef index="94" />
<atomRef index="71" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="901" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="267" />
<atomRef index="319" />
<atomRef index="321" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties index="902" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="330" />
<atomRef index="209" />
<atomRef index="278" />
<atomRef index="192" />
</cell>
<cell>
<cellProperties index="903" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="263" />
<atomRef index="298" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="904" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="98" />
<atomRef index="75" />
<atomRef index="100" />
<atomRef index="290" />
</cell>
<cell>
<cellProperties index="905" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="149" />
<atomRef index="127" />
<atomRef index="151" />
<atomRef index="287" />
</cell>
<cell>
<cellProperties index="906" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="212" />
<atomRef index="247" />
<atomRef index="268" />
</cell>
<cell>
<cellProperties index="907" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="211" />
<atomRef index="194" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="908" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="281" />
<atomRef index="253" />
<atomRef index="286" />
</cell>
<cell>
<cellProperties index="909" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="322" />
<atomRef index="283" />
<atomRef index="277" />
<atomRef index="74" />
</cell>
<cell>
<cellProperties index="910" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="100" />
<atomRef index="123" />
<atomRef index="298" />
</cell>
<cell>
<cellProperties index="911" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="256" />
<atomRef index="260" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="912" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="261" />
<atomRef index="246" />
<atomRef index="235" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="913" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="46" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="914" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="192" />
<atomRef index="194" />
<atomRef index="211" />
<atomRef index="278" />
</cell>
<cell>
<cellProperties index="915" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="255" />
<atomRef index="189" />
<atomRef index="307" />
</cell>
<cell>
<cellProperties index="916" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="20" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="917" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="149" />
<atomRef index="170" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="918" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="113" />
<atomRef index="137" />
<atomRef index="275" />
</cell>
<cell>
<cellProperties index="919" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="258" />
<atomRef index="242" />
<atomRef index="83" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="920" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="248" />
<atomRef index="152" />
<atomRef index="173" />
<atomRef index="255" />
</cell>
<cell>
<cellProperties index="921" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="258" />
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="259" />
</cell>
<cell>
<cellProperties index="922" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="103" />
<atomRef index="105" />
<atomRef index="80" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="923" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="80" />
<atomRef index="56" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="924" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="275" />
<atomRef index="251" />
<atomRef index="269" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="925" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="69" />
<atomRef index="43" />
<atomRef index="68" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="926" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="73" />
<atomRef index="96" />
<atomRef index="67" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="927" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="208" />
<atomRef index="255" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="928" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="263" />
<atomRef index="251" />
<atomRef index="285" />
</cell>
<cell>
<cellProperties index="929" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="67" />
<atomRef index="293" />
<atomRef index="317" />
<atomRef index="73" />
</cell>
<cell>
<cellProperties index="930" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="138" />
<atomRef index="160" />
<atomRef index="280" />
</cell>
<cell>
<cellProperties index="931" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="86" />
<atomRef index="112" />
<atomRef index="111" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="932" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="253" />
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="281" />
</cell>
<cell>
<cellProperties index="933" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="244" />
<atomRef index="137" />
<atomRef index="159" />
<atomRef index="269" />
</cell>
<cell>
<cellProperties index="934" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="24" />
<atomRef index="23" />
<atomRef index="17" />
<atomRef index="297" />
</cell>
<cell>
<cellProperties index="935" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="274" />
<atomRef index="167" />
<atomRef index="187" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="936" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="161" />
<atomRef index="160" />
<atomRef index="181" />
<atomRef index="262" />
</cell>
<cell>
<cellProperties index="937" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="90" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="938" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="254" />
<atomRef index="263" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="939" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="134" />
<atomRef index="284" />
</cell>
<cell>
<cellProperties index="940" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="175" />
<atomRef index="240" />
<atomRef index="249" />
<atomRef index="276" />
</cell>
<cell>
<cellProperties index="941" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="70" />
<atomRef index="69" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="942" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="67" />
<atomRef index="66" />
<atomRef index="44" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="943" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="275" />
<atomRef index="245" />
<atomRef index="96" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="944" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="116" />
<atomRef index="93" />
<atomRef index="92" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="945" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="275" />
<atomRef index="113" />
<atomRef index="245" />
<atomRef index="293" />
</cell>
<cell>
<cellProperties index="946" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="77" />
<atomRef index="100" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="947" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="298" />
<atomRef index="100" />
<atomRef index="102" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="948" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="330" />
<atomRef index="279" />
<atomRef index="149" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="949" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="170" />
<atomRef index="149" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="950" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="22" />
<atomRef index="16" />
<atomRef index="15" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="951" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="272" />
<atomRef index="260" />
<atomRef index="271" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="952" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="177" />
<atomRef index="156" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="953" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="110" />
<atomRef index="133" />
<atomRef index="135" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="954" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="211" />
<atomRef index="252" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="955" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="257" />
<atomRef index="277" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="956" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="17" />
<atomRef index="10" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="957" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="265" />
<atomRef index="133" />
<atomRef index="252" />
<atomRef index="296" />
</cell>
<cell>
<cellProperties index="958" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="266" />
<atomRef index="86" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="959" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="10" />
<atomRef index="264" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="960" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="0" />
<atomRef index="10" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="961" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="28" />
<atomRef index="318" />
<atomRef index="317" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="962" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="227" />
<atomRef index="63" />
<atomRef index="62" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="963" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="284" />
<atomRef index="255" />
<atomRef index="252" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="964" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="167" />
<atomRef index="146" />
<atomRef index="303" />
</cell>
<cell>
<cellProperties index="965" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="267" />
<atomRef index="260" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="966" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="23" />
<atomRef index="15" />
<atomRef index="17" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="967" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="312" />
<atomRef index="254" />
<atomRef index="302" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="968" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="263" />
<atomRef index="259" />
<atomRef index="254" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="969" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="258" />
<atomRef index="254" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="970" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="312" />
<atomRef index="302" />
<atomRef index="85" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="971" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="12" />
<atomRef index="5" />
<atomRef index="7" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="972" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="267" />
<atomRef index="264" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="973" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="260" />
<atomRef index="272" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="974" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="206" />
<atomRef index="208" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="975" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="223" />
<atomRef index="210" />
<atomRef index="208" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="976" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="210" />
<atomRef index="223" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="977" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="255" />
<atomRef index="208" />
<atomRef index="210" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="978" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="224" />
<atomRef index="222" />
<atomRef index="211" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="979" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="224" />
<atomRef index="211" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="980" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="225" />
<atomRef index="222" />
<atomRef index="224" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="981" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="252" />
<atomRef index="225" />
<atomRef index="224" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="982" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="209" />
<atomRef index="211" />
<atomRef index="221" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="983" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="211" />
<atomRef index="209" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="984" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="187" />
<atomRef index="206" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="985" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="314" />
<atomRef index="69" />
<atomRef index="92" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="986" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="267" />
<atomRef index="256" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="987" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="41" />
<atomRef index="272" />
<atomRef index="42" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="988" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="280" />
<atomRef index="92" />
<atomRef index="91" />
<atomRef index="334" />
</cell>
<cell>
<cellProperties index="989" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="220" />
<atomRef index="206" />
<atomRef index="217" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="990" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="217" />
<atomRef index="206" />
<atomRef index="292" />
</cell>
<cell>
<cellProperties index="991" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="266" />
<atomRef index="254" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="992" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="296" />
<atomRef index="252" />
<atomRef index="278" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="993" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="284" />
<atomRef index="266" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="994" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="54" />
<atomRef index="78" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="995" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="76" />
<atomRef index="101" />
<atomRef index="78" />
<atomRef index="308" />
</cell>
<cell>
<cellProperties index="996" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="290" />
<atomRef index="166" />
<atomRef index="164" />
<atomRef index="309" />
</cell>
<cell>
<cellProperties index="997" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="278" />
<atomRef index="252" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="998" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="296" />
<atomRef index="110" />
<atomRef index="133" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="999" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="182" />
<atomRef index="199" />
<atomRef index="203" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1000" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="24" />
<atomRef index="18" />
<atomRef index="25" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1001" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="24" />
<atomRef index="40" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1002" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="11" />
<atomRef index="18" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1003" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="297" />
<atomRef index="18" />
<atomRef index="24" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1004" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="53" />
<atomRef index="51" />
<atomRef index="36" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1005" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="77" />
<atomRef index="51" />
<atomRef index="53" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1006" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="263" />
<atomRef index="38" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1007" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="263" />
<atomRef index="264" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1008" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="55" />
<atomRef index="36" />
<atomRef index="38" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1009" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="38" />
<atomRef index="36" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1010" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="36" />
<atomRef index="51" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1011" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="264" />
<atomRef index="51" />
<atomRef index="300" />
</cell>
<cell>
<cellProperties index="1012" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="61" />
<atomRef index="266" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="1013" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="269" />
<atomRef index="113" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="1014" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="254" />
<atomRef index="278" />
<atomRef index="302" />
</cell>
<cell>
<cellProperties index="1015" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="88" />
<atomRef index="65" />
<atomRef index="87" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="1016" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="182" />
<atomRef index="204" />
<atomRef index="325" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1017" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="251" />
<atomRef index="256" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="1018" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="185" />
<atomRef index="204" />
<atomRef index="206" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1019" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="89" />
<atomRef index="66" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="1020" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1021" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="113" />
<atomRef index="89" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="1022" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="269" />
<atomRef index="243" />
<atomRef index="113" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="1023" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="68" />
<atomRef index="66" />
<atomRef index="91" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="1024" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="37" />
<atomRef index="272" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="1025" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="294" />
<atomRef index="266" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="1026" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="126" />
<atomRef index="274" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="1027" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="304" />
<atomRef index="62" />
<atomRef index="227" />
</cell>
<cell>
<cellProperties index="1028" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="62" />
<atomRef index="304" />
<atomRef index="315" />
<atomRef index="61" />
</cell>
<cell>
<cellProperties index="1029" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="320" />
<atomRef index="267" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="1030" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="28" />
<atomRef index="2" />
<atomRef index="318" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1031" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="267" />
<atomRef index="264" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1032" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="56" />
<atomRef index="42" />
<atomRef index="37" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="1033" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="56" />
<atomRef index="37" />
<atomRef index="304" />
</cell>
<cell>
<cellProperties index="1034" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="282" />
<atomRef index="274" />
<atomRef index="126" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="1035" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="294" />
<atomRef index="274" />
<atomRef index="266" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="1036" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1037" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="48" />
<atomRef index="72" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1038" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="71" />
<atomRef index="95" />
<atomRef index="72" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1039" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="72" />
<atomRef index="95" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1040" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="283" />
<atomRef index="117" />
<atomRef index="140" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1041" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="283" />
<atomRef index="140" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1042" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="117" />
<atomRef index="93" />
<atomRef index="116" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1043" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="86" />
<atomRef index="61" />
<atomRef index="65" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="1044" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="140" />
<atomRef index="117" />
<atomRef index="116" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1045" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="140" />
<atomRef index="116" />
<atomRef index="305" />
</cell>
<cell>
<cellProperties index="1046" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="272" />
<atomRef index="7" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1047" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="7" />
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1048" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="278" />
<atomRef index="258" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1049" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="39" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="311" />
</cell>
<cell>
<cellProperties index="1050" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="6" />
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1051" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="254" />
<atomRef index="258" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="1052" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="4" />
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1053" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="322" />
<atomRef index="74" />
<atomRef index="277" />
<atomRef index="97" />
</cell>
<cell>
<cellProperties index="1054" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="40" />
<atomRef index="229" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1055" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="289" />
<atomRef index="31" />
<atomRef index="6" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1056" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="31" />
<atomRef index="289" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1057" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="31" />
<atomRef index="7" />
<atomRef index="6" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1058" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="295" />
<atomRef index="7" />
<atomRef index="31" />
<atomRef index="306" />
</cell>
<cell>
<cellProperties index="1059" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="41" />
<atomRef index="272" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1060" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="271" />
<atomRef index="41" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1061" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="42" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1062" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="301" />
<atomRef index="42" />
<atomRef index="26" />
<atomRef index="310" />
</cell>
<cell>
<cellProperties index="1063" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="151" />
<atomRef index="149" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1064" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="216" />
<atomRef index="201" />
<atomRef index="215" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1065" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="328" />
<atomRef index="233" />
<atomRef index="153" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1066" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="258" />
<atomRef index="278" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1067" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="110" />
<atomRef index="296" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="1068" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="85" />
<atomRef index="111" />
<atomRef index="110" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="1069" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="302" />
<atomRef index="85" />
<atomRef index="110" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="1070" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="86" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="1071" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="266" />
<atomRef index="86" />
<atomRef index="61" />
<atomRef index="312" />
</cell>
<cell>
<cellProperties index="1072" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="36" />
<atomRef index="34" />
<atomRef index="9" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="1073" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="51" />
<atomRef index="36" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="1074" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="319" />
<atomRef index="34" />
<atomRef index="51" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="1075" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="264" />
<atomRef index="10" />
<atomRef index="2" />
<atomRef index="329" />
</cell>
<cell>
<cellProperties index="1076" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="326" />
<atomRef index="286" />
<atomRef index="270" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1077" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="4" />
<atomRef index="306" />
<atomRef index="267" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="1078" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="310" />
<atomRef index="42" />
<atomRef index="272" />
<atomRef index="301" />
</cell>
<cell>
<cellProperties index="1079" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="321" />
<atomRef index="320" />
<atomRef index="319" />
<atomRef index="28" />
</cell>
<cell>
<cellProperties index="1080" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="66" />
<atomRef index="44" />
<atomRef index="313" />
</cell>
<cell>
<cellProperties index="1081" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="256" />
<atomRef index="160" />
<atomRef index="138" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1082" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="160" />
<atomRef index="256" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1083" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="139" />
<atomRef index="115" />
<atomRef index="138" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1084" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="139" />
<atomRef index="116" />
<atomRef index="115" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1085" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="116" />
<atomRef index="139" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1086" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="161" />
<atomRef index="138" />
<atomRef index="160" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1087" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="161" />
<atomRef index="160" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1088" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="161" />
<atomRef index="139" />
<atomRef index="138" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1089" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="139" />
<atomRef index="161" />
<atomRef index="314" />
</cell>
<cell>
<cellProperties index="1090" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="88" />
<atomRef index="86" />
<atomRef index="65" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="1091" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="88" />
<atomRef index="112" />
<atomRef index="86" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="1092" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="65" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="315" />
</cell>
<cell>
<cellProperties index="1093" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="19" />
<atomRef index="310" />
<atomRef index="26" />
<atomRef index="25" />
</cell>
<cell>
<cellProperties index="1094" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="304" />
<atomRef index="227" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="1095" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="315" />
<atomRef index="227" />
<atomRef index="63" />
<atomRef index="316" />
</cell>
<cell>
<cellProperties index="1096" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1097" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="264" />
<atomRef index="267" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1098" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="273" />
<atomRef index="267" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1099" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="293" />
<atomRef index="267" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1100" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="44" />
<atomRef index="293" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1101" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="313" />
<atomRef index="267" />
<atomRef index="44" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1102" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="278" />
<atomRef index="192" />
<atomRef index="328" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1103" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="172" />
<atomRef index="151" />
<atomRef index="328" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1104" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="49" />
<atomRef index="317" />
<atomRef index="32" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1105" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="263" />
<atomRef index="254" />
<atomRef index="259" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="1106" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="300" />
<atomRef index="263" />
<atomRef index="259" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="1107" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="306" />
<atomRef index="1" />
<atomRef index="4" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1108" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="73" />
<atomRef index="273" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1109" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="293" />
<atomRef index="317" />
<atomRef index="73" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1110" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="51" />
<atomRef index="32" />
<atomRef index="34" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1111" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="51" />
<atomRef index="49" />
<atomRef index="32" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1112" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="49" />
<atomRef index="73" />
<atomRef index="317" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1113" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="49" />
<atomRef index="273" />
<atomRef index="73" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1114" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="51" />
<atomRef index="264" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1115" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="273" />
<atomRef index="49" />
<atomRef index="51" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1116" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="257" />
<atomRef index="281" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1117" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="286" />
<atomRef index="277" />
<atomRef index="257" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1118" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="303" />
<atomRef index="277" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1119" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="163" />
<atomRef index="140" />
<atomRef index="237" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1120" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="291" />
<atomRef index="237" />
<atomRef index="140" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1121" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="334" />
<atomRef index="70" />
<atomRef index="69" />
<atomRef index="336" />
</cell>
<cell>
<cellProperties index="1122" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="303" />
<atomRef index="165" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1123" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="303" />
<atomRef index="281" />
<atomRef index="257" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1124" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="303" />
<atomRef index="257" />
<atomRef index="277" />
<atomRef index="326" />
</cell>
<cell>
<cellProperties index="1125" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="257" />
<atomRef index="266" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1126" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="266" />
<atomRef index="61" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1127" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="84" />
<atomRef index="258" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1128" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="59" />
<atomRef index="84" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1129" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="229" />
<atomRef index="59" />
<atomRef index="39" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1130" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="39" />
<atomRef index="59" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1131" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="39" />
<atomRef index="259" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1132" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="229" />
<atomRef index="39" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1133" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="61" />
<atomRef index="60" />
<atomRef index="40" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1134" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="41" />
<atomRef index="271" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1135" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="304" />
<atomRef index="61" />
<atomRef index="41" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1136" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="61" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1137" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="271" />
<atomRef index="41" />
<atomRef index="40" />
<atomRef index="327" />
</cell>
<cell>
<cellProperties index="1138" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="170" />
<atomRef index="190" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1139" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="233" />
<atomRef index="287" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1140" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="288" />
<atomRef index="153" />
<atomRef index="233" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1141" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="219" />
<atomRef index="216" />
<atomRef index="215" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1142" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="127" />
<atomRef index="258" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1143" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="287" />
<atomRef index="149" />
<atomRef index="127" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1144" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="190" />
<atomRef index="192" />
<atomRef index="209" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1145" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="190" />
<atomRef index="209" />
<atomRef index="330" />
</cell>
<cell>
<cellProperties index="1146" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="185" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1147" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="320" />
<atomRef index="321" />
<atomRef index="267" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="1148" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="320" />
<atomRef index="27" />
<atomRef index="321" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="1149" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="1" />
<atomRef index="267" />
<atomRef index="4" />
<atomRef index="319" />
</cell>
<cell>
<cellProperties index="1150" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="43" />
<atomRef index="27" />
<atomRef index="320" />
<atomRef index="331" />
</cell>
<cell>
<cellProperties index="1151" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="270" />
<atomRef index="253" />
<atomRef index="203" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1152" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="326" />
<atomRef index="163" />
<atomRef index="324" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1153" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="253" />
<atomRef index="270" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1154" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="168" />
<atomRef index="170" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="1155" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="259" />
<atomRef index="127" />
<atomRef index="81" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="1156" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="300" />
<atomRef index="259" />
<atomRef index="81" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="1157" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="300" />
<atomRef index="81" />
<atomRef index="127" />
<atomRef index="332" />
</cell>
<cell>
<cellProperties index="1158" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="207" />
<atomRef index="221" />
<atomRef index="218" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1159" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="279" />
<atomRef index="207" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1160" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="281" />
<atomRef index="254" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1161" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="254" />
<atomRef index="281" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1162" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="279" />
<atomRef index="254" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1163" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="299" />
<atomRef index="254" />
<atomRef index="279" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1164" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="222" />
<atomRef index="218" />
<atomRef index="221" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1165" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="222" />
<atomRef index="221" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1166" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="309" />
<atomRef index="201" />
<atomRef index="285" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1167" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="309" />
<atomRef index="205" />
<atomRef index="201" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1168" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="253" />
<atomRef index="202" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1169" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="253" />
<atomRef index="281" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1170" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="285" />
<atomRef index="202" />
<atomRef index="253" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1171" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="202" />
<atomRef index="216" />
<atomRef index="217" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1172" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="281" />
<atomRef index="217" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1173" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="220" />
<atomRef index="217" />
<atomRef index="216" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1174" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="217" />
<atomRef index="220" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1175" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="207" />
<atomRef index="209" />
<atomRef index="221" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1176" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="221" />
<atomRef index="209" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1177" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="279" />
<atomRef index="209" />
<atomRef index="207" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1178" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="292" />
<atomRef index="209" />
<atomRef index="279" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1179" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="202" />
<atomRef index="217" />
<atomRef index="203" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1180" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1181" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="203" />
<atomRef index="217" />
<atomRef index="333" />
</cell>
<cell>
<cellProperties index="1182" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="270" />
<atomRef index="203" />
<atomRef index="199" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1183" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="165" />
<atomRef index="185" />
<atomRef index="187" />
<atomRef index="335" />
</cell>
<cell>
<cellProperties index="1184" type="TETRAHEDRON"  />
<nrOfStructures value="4"/>
<atomRef index="281" />
<atomRef index="165" />
<atomRef index="187" />
<atomRef index="335" />
</cell>
</structuralComponent>
</multiComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Informative Components" >
<multiComponent name="surface cells" >
<structuralComponent  name="surfaceCells-CamiTKModel"  mode="WIREFRAME_AND_SURFACE" >
<nrOfStructures value="516"/>
<cell>
<cellProperties index="1185" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="38" />
<atomRef index="36" />
<atomRef index="16" />
</cell>
<cell>
<cellProperties index="1186" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="136" />
<atomRef index="114" />
<atomRef index="234" />
</cell>
<cell>
<cellProperties index="1187" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="96" />
<atomRef index="121" />
<atomRef index="119" />
</cell>
<cell>
<cellProperties index="1188" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="82" />
<atomRef index="56" />
<atomRef index="80" />
</cell>
<cell>
<cellProperties index="1189" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="142" />
<atomRef index="118" />
<atomRef index="140" />
</cell>
<cell>
<cellProperties index="1190" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="8" />
<atomRef index="2" />
<atomRef index="0" />
</cell>
<cell>
<cellProperties index="1191" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="8" />
<atomRef index="0" />
<atomRef index="10" />
</cell>
<cell>
<cellProperties index="1192" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="226" />
<atomRef index="106" />
<atomRef index="232" />
</cell>
<cell>
<cellProperties index="1193" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="174" />
<atomRef index="192" />
<atomRef index="328" />
</cell>
<cell>
<cellProperties index="1194" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="7" />
<atomRef index="13" />
<atomRef index="5" />
</cell>
<cell>
<cellProperties index="1195" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="12" />
<atomRef index="5" />
<atomRef index="13" />
</cell>
<cell>
<cellProperties index="1196" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="250" />
<atomRef index="195" />
<atomRef index="214" />
</cell>
<cell>
<cellProperties index="1197" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="66" />
<atomRef index="320" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties index="1198" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="48" />
<atomRef index="72" />
<atomRef index="74" />
</cell>
<cell>
<cellProperties index="1199" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="75" />
<atomRef index="51" />
<atomRef index="53" />
</cell>
<cell>
<cellProperties index="1200" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="75" />
<atomRef index="53" />
<atomRef index="77" />
</cell>
<cell>
<cellProperties index="1201" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="39" />
<atomRef index="23" />
<atomRef index="40" />
</cell>
<cell>
<cellProperties index="1202" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="37" />
<atomRef index="14" />
<atomRef index="35" />
</cell>
<cell>
<cellProperties index="1203" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="178" />
<atomRef index="197" />
<atomRef index="177" />
</cell>
<cell>
<cellProperties index="1204" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="29" />
<atomRef index="4" />
<atomRef index="1" />
</cell>
<cell>
<cellProperties index="1205" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="170" />
<atomRef index="190" />
<atomRef index="188" />
</cell>
<cell>
<cellProperties index="1206" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="73" />
<atomRef index="67" />
<atomRef index="49" />
</cell>
<cell>
<cellProperties index="1207" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="49" />
<atomRef index="67" />
<atomRef index="317" />
</cell>
<cell>
<cellProperties index="1208" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="136" />
</cell>
<cell>
<cellProperties index="1209" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="246" />
<atomRef index="162" />
<atomRef index="137" />
</cell>
<cell>
<cellProperties index="1210" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="32" />
<atomRef index="2" />
<atomRef index="9" />
</cell>
<cell>
<cellProperties index="1211" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="32" />
<atomRef index="9" />
<atomRef index="34" />
</cell>
<cell>
<cellProperties index="1212" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="105" />
<atomRef index="82" />
<atomRef index="80" />
</cell>
<cell>
<cellProperties index="1213" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="110" />
<atomRef index="135" />
<atomRef index="132" />
</cell>
<cell>
<cellProperties index="1214" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="180" />
<atomRef index="159" />
<atomRef index="179" />
</cell>
<cell>
<cellProperties index="1215" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="180" />
<atomRef index="160" />
<atomRef index="159" />
</cell>
<cell>
<cellProperties index="1216" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="173" />
<atomRef index="193" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties index="1217" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="86" />
<atomRef index="85" />
<atomRef index="60" />
</cell>
<cell>
<cellProperties index="1218" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="134" />
<atomRef index="158" />
<atomRef index="236" />
</cell>
<cell>
<cellProperties index="1219" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="90" />
<atomRef index="67" />
<atomRef index="96" />
</cell>
<cell>
<cellProperties index="1220" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="57" />
<atomRef index="38" />
<atomRef index="39" />
</cell>
<cell>
<cellProperties index="1221" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="57" />
<atomRef index="39" />
<atomRef index="59" />
</cell>
<cell>
<cellProperties index="1222" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="104" />
<atomRef index="79" />
<atomRef index="81" />
</cell>
<cell>
<cellProperties index="1223" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="104" />
<atomRef index="102" />
<atomRef index="79" />
</cell>
<cell>
<cellProperties index="1224" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="106" />
<atomRef index="83" />
<atomRef index="232" />
</cell>
<cell>
<cellProperties index="1225" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="104" />
<atomRef index="127" />
<atomRef index="125" />
</cell>
<cell>
<cellProperties index="1226" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="104" />
<atomRef index="125" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties index="1227" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="89" />
<atomRef index="243" />
<atomRef index="114" />
</cell>
<cell>
<cellProperties index="1228" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="89" />
<atomRef index="114" />
<atomRef index="91" />
</cell>
<cell>
<cellProperties index="1229" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="242" />
<atomRef index="231" />
<atomRef index="83" />
</cell>
<cell>
<cellProperties index="1230" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="151" />
<atomRef index="172" />
<atomRef index="170" />
</cell>
<cell>
<cellProperties index="1231" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="114" />
<atomRef index="138" />
<atomRef index="115" />
</cell>
<cell>
<cellProperties index="1232" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="146" />
<atomRef index="124" />
<atomRef index="122" />
</cell>
<cell>
<cellProperties index="1233" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="228" />
<atomRef index="163" />
<atomRef index="237" />
</cell>
<cell>
<cellProperties index="1234" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="178" />
<atomRef index="156" />
<atomRef index="157" />
</cell>
<cell>
<cellProperties index="1235" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="68" />
</cell>
<cell>
<cellProperties index="1236" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="32" />
<atomRef index="317" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties index="1237" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="240" />
<atomRef index="175" />
<atomRef index="195" />
</cell>
<cell>
<cellProperties index="1238" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="208" />
<atomRef index="187" />
<atomRef index="206" />
</cell>
<cell>
<cellProperties index="1239" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="100" />
<atomRef index="125" />
<atomRef index="123" />
</cell>
<cell>
<cellProperties index="1240" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="164" />
<atomRef index="166" />
<atomRef index="186" />
</cell>
<cell>
<cellProperties index="1241" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="246" />
<atomRef index="137" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties index="1242" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="240" />
<atomRef index="178" />
<atomRef index="249" />
</cell>
<cell>
<cellProperties index="1243" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="87" />
<atomRef index="63" />
<atomRef index="82" />
</cell>
<cell>
<cellProperties index="1244" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="229" />
<atomRef index="40" />
<atomRef index="60" />
</cell>
<cell>
<cellProperties index="1245" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="223" />
<atomRef index="220" />
<atomRef index="219" />
</cell>
<cell>
<cellProperties index="1246" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="178" />
<atomRef index="177" />
<atomRef index="156" />
</cell>
<cell>
<cellProperties index="1247" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="140" />
<atomRef index="161" />
<atomRef index="237" />
</cell>
<cell>
<cellProperties index="1248" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="47" />
<atomRef index="72" />
<atomRef index="48" />
</cell>
<cell>
<cellProperties index="1249" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="172" />
<atomRef index="192" />
<atomRef index="190" />
</cell>
<cell>
<cellProperties index="1250" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="172" />
<atomRef index="190" />
<atomRef index="170" />
</cell>
<cell>
<cellProperties index="1251" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="29" />
<atomRef index="1" />
<atomRef index="27" />
</cell>
<cell>
<cellProperties index="1252" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="220" />
<atomRef index="208" />
<atomRef index="206" />
</cell>
<cell>
<cellProperties index="1253" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="214" />
<atomRef index="197" />
<atomRef index="250" />
</cell>
<cell>
<cellProperties index="1254" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="225" />
<atomRef index="210" />
<atomRef index="223" />
</cell>
<cell>
<cellProperties index="1255" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="188" />
<atomRef index="209" />
<atomRef index="207" />
</cell>
<cell>
<cellProperties index="1256" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="38" />
<atomRef index="16" />
<atomRef index="22" />
</cell>
<cell>
<cellProperties index="1257" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="219" />
<atomRef index="215" />
<atomRef index="218" />
</cell>
<cell>
<cellProperties index="1258" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="91" />
<atomRef index="115" />
<atomRef index="92" />
</cell>
<cell>
<cellProperties index="1259" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="116" />
<atomRef index="92" />
<atomRef index="115" />
</cell>
<cell>
<cellProperties index="1260" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="102" />
<atomRef index="77" />
<atomRef index="79" />
</cell>
<cell>
<cellProperties index="1261" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="226" />
<atomRef index="129" />
<atomRef index="106" />
</cell>
<cell>
<cellProperties index="1262" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="214" />
<atomRef index="196" />
<atomRef index="197" />
</cell>
<cell>
<cellProperties index="1263" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="113" />
<atomRef index="137" />
<atomRef index="234" />
</cell>
<cell>
<cellProperties index="1264" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="323" />
<atomRef index="95" />
<atomRef index="118" />
</cell>
<cell>
<cellProperties index="1265" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="125" />
<atomRef index="149" />
<atomRef index="147" />
</cell>
<cell>
<cellProperties index="1266" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="24" />
<atomRef index="41" />
<atomRef index="40" />
</cell>
<cell>
<cellProperties index="1267" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="240" />
<atomRef index="250" />
<atomRef index="197" />
</cell>
<cell>
<cellProperties index="1268" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="128" />
<atomRef index="150" />
<atomRef index="152" />
</cell>
<cell>
<cellProperties index="1269" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="238" />
<atomRef index="199" />
<atomRef index="182" />
</cell>
<cell>
<cellProperties index="1270" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="106" />
<atomRef index="129" />
<atomRef index="127" />
</cell>
<cell>
<cellProperties index="1271" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="184" />
<atomRef index="205" />
<atomRef index="200" />
</cell>
<cell>
<cellProperties index="1272" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="178" />
<atomRef index="240" />
<atomRef index="197" />
</cell>
<cell>
<cellProperties index="1273" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="197" />
<atomRef index="196" />
<atomRef index="177" />
</cell>
<cell>
<cellProperties index="1274" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="225" />
<atomRef index="224" />
<atomRef index="213" />
</cell>
<cell>
<cellProperties index="1275" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="37" />
<atomRef index="35" />
<atomRef index="54" />
</cell>
<cell>
<cellProperties index="1276" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="214" />
<atomRef index="213" />
<atomRef index="196" />
</cell>
<cell>
<cellProperties index="1277" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="90" />
<atomRef index="89" />
<atomRef index="66" />
</cell>
<cell>
<cellProperties index="1278" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="113" />
<atomRef index="90" />
<atomRef index="245" />
</cell>
<cell>
<cellProperties index="1279" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="82" />
<atomRef index="63" />
<atomRef index="56" />
</cell>
<cell>
<cellProperties index="1280" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="12" />
<atomRef index="11" />
<atomRef index="3" />
</cell>
<cell>
<cellProperties index="1281" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="150" />
<atomRef index="173" />
<atomRef index="152" />
</cell>
<cell>
<cellProperties index="1282" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="195" />
<atomRef index="175" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties index="1283" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="210" />
<atomRef index="191" />
<atomRef index="189" />
</cell>
<cell>
<cellProperties index="1284" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="188" />
<atomRef index="190" />
<atomRef index="209" />
</cell>
<cell>
<cellProperties index="1285" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="100" />
<atomRef index="77" />
<atomRef index="102" />
</cell>
<cell>
<cellProperties index="1286" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="69" />
<atomRef index="93" />
<atomRef index="70" />
</cell>
<cell>
<cellProperties index="1287" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="233" />
<atomRef index="328" />
<atomRef index="151" />
</cell>
<cell>
<cellProperties index="1288" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="317" />
<atomRef index="67" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties index="1289" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="100" />
<atomRef index="102" />
<atomRef index="125" />
</cell>
<cell>
<cellProperties index="1290" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="137" />
<atomRef index="162" />
<atomRef index="159" />
</cell>
<cell>
<cellProperties index="1291" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="47" />
<atomRef index="71" />
<atomRef index="72" />
</cell>
<cell>
<cellProperties index="1292" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="103" />
<atomRef index="128" />
<atomRef index="105" />
</cell>
<cell>
<cellProperties index="1293" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="14" />
<atomRef index="13" />
<atomRef index="7" />
</cell>
<cell>
<cellProperties index="1294" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="122" />
<atomRef index="97" />
<atomRef index="120" />
</cell>
<cell>
<cellProperties index="1295" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="81" />
<atomRef index="79" />
<atomRef index="55" />
</cell>
<cell>
<cellProperties index="1296" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="238" />
<atomRef index="181" />
<atomRef index="199" />
</cell>
<cell>
<cellProperties index="1297" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="37" />
<atomRef index="21" />
<atomRef index="14" />
</cell>
<cell>
<cellProperties index="1298" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="153" />
<atomRef index="131" />
<atomRef index="132" />
</cell>
<cell>
<cellProperties index="1299" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="202" />
<atomRef index="198" />
<atomRef index="201" />
</cell>
<cell>
<cellProperties index="1300" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="179" />
<atomRef index="201" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties index="1301" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="42" />
<atomRef index="21" />
<atomRef index="37" />
</cell>
<cell>
<cellProperties index="1302" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="141" />
<atomRef index="164" />
<atomRef index="162" />
</cell>
<cell>
<cellProperties index="1303" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="226" />
<atomRef index="233" />
<atomRef index="129" />
</cell>
<cell>
<cellProperties index="1304" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="35" />
<atomRef index="52" />
<atomRef index="54" />
</cell>
<cell>
<cellProperties index="1305" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="165" />
<atomRef index="324" />
<atomRef index="185" />
</cell>
<cell>
<cellProperties index="1306" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="19" />
<atomRef index="18" />
<atomRef index="11" />
</cell>
<cell>
<cellProperties index="1307" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="180" />
<atomRef index="179" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties index="1308" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="208" />
<atomRef index="189" />
<atomRef index="187" />
</cell>
<cell>
<cellProperties index="1309" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="46" />
<atomRef index="30" />
<atomRef index="29" />
</cell>
<cell>
<cellProperties index="1310" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="26" />
<atomRef index="19" />
<atomRef index="20" />
</cell>
<cell>
<cellProperties index="1311" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="33" />
<atomRef index="7" />
<atomRef index="31" />
</cell>
<cell>
<cellProperties index="1312" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="180" />
<atomRef index="199" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties index="1313" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="16" />
<atomRef index="9" />
<atomRef index="8" />
</cell>
<cell>
<cellProperties index="1314" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="174" />
<atomRef index="194" />
<atomRef index="192" />
</cell>
<cell>
<cellProperties index="1315" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="317" />
<atomRef index="44" />
<atomRef index="28" />
</cell>
<cell>
<cellProperties index="1316" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="45" />
<atomRef index="29" />
<atomRef index="27" />
</cell>
<cell>
<cellProperties index="1317" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="31" />
<atomRef index="48" />
<atomRef index="33" />
</cell>
<cell>
<cellProperties index="1318" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="48" />
<atomRef index="50" />
<atomRef index="33" />
</cell>
<cell>
<cellProperties index="1319" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="191" />
<atomRef index="212" />
<atomRef index="193" />
</cell>
<cell>
<cellProperties index="1320" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="246" />
<atomRef index="141" />
<atomRef index="162" />
</cell>
<cell>
<cellProperties index="1321" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="220" />
<atomRef index="216" />
<atomRef index="219" />
</cell>
<cell>
<cellProperties index="1322" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="322" />
<atomRef index="95" />
<atomRef index="323" />
</cell>
<cell>
<cellProperties index="1323" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="66" />
<atomRef index="43" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties index="1324" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="66" />
<atomRef index="68" />
<atomRef index="43" />
</cell>
<cell>
<cellProperties index="1325" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="65" />
<atomRef index="62" />
<atomRef index="63" />
</cell>
<cell>
<cellProperties index="1326" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="153" />
<atomRef index="155" />
<atomRef index="176" />
</cell>
<cell>
<cellProperties index="1327" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="132" />
</cell>
<cell>
<cellProperties index="1328" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="213" />
<atomRef index="211" />
<atomRef index="194" />
</cell>
<cell>
<cellProperties index="1329" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="107" />
<atomRef index="134" />
<atomRef index="112" />
</cell>
<cell>
<cellProperties index="1330" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="186" />
<atomRef index="188" />
<atomRef index="207" />
</cell>
<cell>
<cellProperties index="1331" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="201" />
<atomRef index="205" />
<atomRef index="215" />
</cell>
<cell>
<cellProperties index="1332" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="26" />
<atomRef index="21" />
<atomRef index="42" />
</cell>
<cell>
<cellProperties index="1333" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="90" />
<atomRef index="113" />
<atomRef index="89" />
</cell>
<cell>
<cellProperties index="1334" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="27" />
<atomRef index="1" />
<atomRef index="321" />
</cell>
<cell>
<cellProperties index="1335" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="98" />
<atomRef index="100" />
<atomRef index="123" />
</cell>
<cell>
<cellProperties index="1336" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="105" />
<atomRef index="128" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties index="1337" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="105" />
<atomRef index="130" />
<atomRef index="107" />
</cell>
<cell>
<cellProperties index="1338" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="83" />
<atomRef index="64" />
<atomRef index="84" />
</cell>
<cell>
<cellProperties index="1339" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="64" />
<atomRef index="59" />
<atomRef index="84" />
</cell>
<cell>
<cellProperties index="1340" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="203" />
<atomRef index="217" />
<atomRef index="206" />
</cell>
<cell>
<cellProperties index="1341" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="172" />
<atomRef index="328" />
<atomRef index="192" />
</cell>
<cell>
<cellProperties index="1342" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="87" />
<atomRef index="65" />
<atomRef index="63" />
</cell>
<cell>
<cellProperties index="1343" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="77" />
<atomRef index="53" />
<atomRef index="79" />
</cell>
<cell>
<cellProperties index="1344" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="53" />
<atomRef index="55" />
<atomRef index="79" />
</cell>
<cell>
<cellProperties index="1345" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="136" />
<atomRef index="244" />
<atomRef index="159" />
</cell>
<cell>
<cellProperties index="1346" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="223" />
<atomRef index="219" />
<atomRef index="222" />
</cell>
<cell>
<cellProperties index="1347" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="234" />
<atomRef index="114" />
<atomRef index="243" />
</cell>
<cell>
<cellProperties index="1348" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="231" />
<atomRef index="81" />
<atomRef index="58" />
</cell>
<cell>
<cellProperties index="1349" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="210" />
<atomRef index="189" />
<atomRef index="208" />
</cell>
<cell>
<cellProperties index="1350" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="148" />
<atomRef index="169" />
<atomRef index="171" />
</cell>
<cell>
<cellProperties index="1351" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="118" />
<atomRef index="94" />
<atomRef index="117" />
</cell>
<cell>
<cellProperties index="1352" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="105" />
<atomRef index="107" />
<atomRef index="82" />
</cell>
<cell>
<cellProperties index="1353" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="146" />
<atomRef index="122" />
<atomRef index="144" />
</cell>
<cell>
<cellProperties index="1354" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="239" />
<atomRef index="175" />
<atomRef index="248" />
</cell>
<cell>
<cellProperties index="1355" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="21" />
<atomRef index="13" />
<atomRef index="14" />
</cell>
<cell>
<cellProperties index="1356" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="56" />
<atomRef index="54" />
<atomRef index="80" />
</cell>
<cell>
<cellProperties index="1357" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="54" />
<atomRef index="78" />
<atomRef index="80" />
</cell>
<cell>
<cellProperties index="1358" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="160" />
<atomRef index="138" />
<atomRef index="136" />
</cell>
<cell>
<cellProperties index="1359" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="236" />
<atomRef index="157" />
<atomRef index="133" />
</cell>
<cell>
<cellProperties index="1360" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="226" />
<atomRef index="131" />
<atomRef index="233" />
</cell>
<cell>
<cellProperties index="1361" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="46" />
<atomRef index="71" />
<atomRef index="47" />
</cell>
<cell>
<cellProperties index="1362" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="151" />
<atomRef index="129" />
<atomRef index="233" />
</cell>
<cell>
<cellProperties index="1363" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="247" />
<atomRef index="239" />
<atomRef index="193" />
</cell>
<cell>
<cellProperties index="1364" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="2" />
<atomRef index="1" />
<atomRef index="0" />
</cell>
<cell>
<cellProperties index="1365" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="242" />
<atomRef index="106" />
<atomRef index="81" />
</cell>
<cell>
<cellProperties index="1366" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="227" />
<atomRef index="56" />
<atomRef index="63" />
</cell>
<cell>
<cellProperties index="1367" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="113" />
<atomRef index="234" />
<atomRef index="243" />
</cell>
<cell>
<cellProperties index="1368" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="226" />
<atomRef index="108" />
<atomRef index="131" />
</cell>
<cell>
<cellProperties index="1369" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="86" />
<atomRef index="111" />
<atomRef index="85" />
</cell>
<cell>
<cellProperties index="1370" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="106" />
<atomRef index="127" />
<atomRef index="104" />
</cell>
<cell>
<cellProperties index="1371" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="106" />
<atomRef index="104" />
<atomRef index="81" />
</cell>
<cell>
<cellProperties index="1372" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="24" />
<atomRef index="40" />
<atomRef index="23" />
</cell>
<cell>
<cellProperties index="1373" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="26" />
<atomRef index="20" />
<atomRef index="21" />
</cell>
<cell>
<cellProperties index="1374" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="24" />
<atomRef index="17" />
<atomRef index="18" />
</cell>
<cell>
<cellProperties index="1375" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="50" />
<atomRef index="76" />
<atomRef index="52" />
</cell>
<cell>
<cellProperties index="1376" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="89" />
<atomRef index="113" />
<atomRef index="243" />
</cell>
<cell>
<cellProperties index="1377" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="225" />
<atomRef index="223" />
<atomRef index="222" />
</cell>
<cell>
<cellProperties index="1378" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="31" />
<atomRef index="6" />
<atomRef index="30" />
</cell>
<cell>
<cellProperties index="1379" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="238" />
<atomRef index="228" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties index="1380" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="229" />
<atomRef index="39" />
<atomRef index="40" />
</cell>
<cell>
<cellProperties index="1381" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="237" />
<atomRef index="161" />
<atomRef index="228" />
</cell>
<cell>
<cellProperties index="1382" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="25" />
<atomRef index="42" />
<atomRef index="41" />
</cell>
<cell>
<cellProperties index="1383" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="322" />
<atomRef index="74" />
<atomRef index="95" />
</cell>
<cell>
<cellProperties index="1384" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="230" />
<atomRef index="38" />
<atomRef index="57" />
</cell>
<cell>
<cellProperties index="1385" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="148" />
<atomRef index="146" />
<atomRef index="169" />
</cell>
<cell>
<cellProperties index="1386" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="203" />
<atomRef index="206" />
<atomRef index="204" />
</cell>
<cell>
<cellProperties index="1387" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="12" />
<atomRef index="3" />
<atomRef index="5" />
</cell>
<cell>
<cellProperties index="1388" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="137" />
<atomRef index="113" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties index="1389" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="153" />
<atomRef index="176" />
<atomRef index="174" />
</cell>
<cell>
<cellProperties index="1390" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="18" />
<atomRef index="17" />
<atomRef index="10" />
</cell>
<cell>
<cellProperties index="1391" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="153" />
<atomRef index="174" />
<atomRef index="328" />
</cell>
<cell>
<cellProperties index="1392" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="100" />
<atomRef index="75" />
<atomRef index="77" />
</cell>
<cell>
<cellProperties index="1393" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="76" />
<atomRef index="97" />
<atomRef index="99" />
</cell>
<cell>
<cellProperties index="1394" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="76" />
<atomRef index="74" />
<atomRef index="97" />
</cell>
<cell>
<cellProperties index="1395" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="17" />
<atomRef index="15" />
<atomRef index="8" />
</cell>
<cell>
<cellProperties index="1396" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="142" />
<atomRef index="144" />
<atomRef index="120" />
</cell>
<cell>
<cellProperties index="1397" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="122" />
<atomRef index="120" />
<atomRef index="144" />
</cell>
<cell>
<cellProperties index="1398" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="191" />
<atomRef index="171" />
<atomRef index="169" />
</cell>
<cell>
<cellProperties index="1399" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="250" />
<atomRef index="240" />
<atomRef index="195" />
</cell>
<cell>
<cellProperties index="1400" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="11" />
<atomRef index="10" />
<atomRef index="0" />
</cell>
<cell>
<cellProperties index="1401" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="180" />
<atomRef index="198" />
<atomRef index="199" />
</cell>
<cell>
<cellProperties index="1402" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="139" />
<atomRef index="117" />
<atomRef index="116" />
</cell>
<cell>
<cellProperties index="1403" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="117" />
</cell>
<cell>
<cellProperties index="1404" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="323" />
<atomRef index="142" />
<atomRef index="120" />
</cell>
<cell>
<cellProperties index="1405" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="110" />
<atomRef index="109" />
<atomRef index="84" />
</cell>
<cell>
<cellProperties index="1406" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="214" />
<atomRef index="195" />
<atomRef index="212" />
</cell>
<cell>
<cellProperties index="1407" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="45" />
<atomRef index="43" />
<atomRef index="69" />
</cell>
<cell>
<cellProperties index="1408" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="117" />
<atomRef index="94" />
<atomRef index="93" />
</cell>
<cell>
<cellProperties index="1409" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="25" />
<atomRef index="18" />
<atomRef index="19" />
</cell>
<cell>
<cellProperties index="1410" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="98" />
<atomRef index="123" />
<atomRef index="121" />
</cell>
<cell>
<cellProperties index="1411" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="148" />
<atomRef index="124" />
<atomRef index="146" />
</cell>
<cell>
<cellProperties index="1412" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="9" />
<atomRef index="2" />
<atomRef index="8" />
</cell>
<cell>
<cellProperties index="1413" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="72" />
<atomRef index="95" />
<atomRef index="74" />
</cell>
<cell>
<cellProperties index="1414" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="245" />
<atomRef index="235" />
<atomRef index="113" />
</cell>
<cell>
<cellProperties index="1415" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="69" />
<atomRef index="92" />
<atomRef index="93" />
</cell>
<cell>
<cellProperties index="1416" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="244" />
<atomRef index="234" />
<atomRef index="137" />
</cell>
<cell>
<cellProperties index="1417" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="232" />
<atomRef index="108" />
<atomRef index="226" />
</cell>
<cell>
<cellProperties index="1418" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="165" />
<atomRef index="144" />
<atomRef index="142" />
</cell>
<cell>
<cellProperties index="1419" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="241" />
<atomRef index="231" />
<atomRef index="58" />
</cell>
<cell>
<cellProperties index="1420" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="247" />
<atomRef index="195" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties index="1421" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="83" />
<atomRef index="106" />
<atomRef index="242" />
</cell>
<cell>
<cellProperties index="1422" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="11" />
<atomRef index="0" />
<atomRef index="3" />
</cell>
<cell>
<cellProperties index="1423" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="156" />
<atomRef index="155" />
<atomRef index="135" />
</cell>
<cell>
<cellProperties index="1424" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="155" />
<atomRef index="132" />
<atomRef index="135" />
</cell>
<cell>
<cellProperties index="1425" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="87" />
<atomRef index="107" />
<atomRef index="112" />
</cell>
<cell>
<cellProperties index="1426" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="119" />
<atomRef index="121" />
<atomRef index="143" />
</cell>
<cell>
<cellProperties index="1427" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="108" />
<atomRef index="132" />
<atomRef index="131" />
</cell>
<cell>
<cellProperties index="1428" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="162" />
<atomRef index="183" />
<atomRef index="179" />
</cell>
<cell>
<cellProperties index="1429" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="4" />
<atomRef index="3" />
<atomRef index="0" />
</cell>
<cell>
<cellProperties index="1430" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="222" />
<atomRef index="219" />
<atomRef index="218" />
</cell>
<cell>
<cellProperties index="1431" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="205" />
<atomRef index="218" />
<atomRef index="215" />
</cell>
<cell>
<cellProperties index="1432" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="110" />
<atomRef index="132" />
<atomRef index="109" />
</cell>
<cell>
<cellProperties index="1433" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="28" />
<atomRef index="1" />
<atomRef index="2" />
</cell>
<cell>
<cellProperties index="1434" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="323" />
<atomRef index="97" />
<atomRef index="322" />
</cell>
<cell>
<cellProperties index="1435" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="120" />
<atomRef index="97" />
<atomRef index="323" />
</cell>
<cell>
<cellProperties index="1436" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="150" />
<atomRef index="171" />
<atomRef index="173" />
</cell>
<cell>
<cellProperties index="1437" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="157" />
<atomRef index="236" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties index="1438" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="165" />
<atomRef index="187" />
<atomRef index="167" />
</cell>
<cell>
<cellProperties index="1439" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="52" />
<atomRef index="76" />
<atomRef index="78" />
</cell>
<cell>
<cellProperties index="1440" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="103" />
<atomRef index="78" />
<atomRef index="101" />
</cell>
<cell>
<cellProperties index="1441" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="162" />
<atomRef index="184" />
<atomRef index="183" />
</cell>
<cell>
<cellProperties index="1442" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="103" />
<atomRef index="80" />
<atomRef index="78" />
</cell>
<cell>
<cellProperties index="1443" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="248" />
<atomRef index="173" />
<atomRef index="239" />
</cell>
<cell>
<cellProperties index="1444" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="61" />
<atomRef index="41" />
<atomRef index="62" />
</cell>
<cell>
<cellProperties index="1445" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="62" />
</cell>
<cell>
<cellProperties index="1446" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="148" />
<atomRef index="171" />
<atomRef index="150" />
</cell>
<cell>
<cellProperties index="1447" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="177" />
<atomRef index="176" />
<atomRef index="155" />
</cell>
<cell>
<cellProperties index="1448" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="83" />
<atomRef index="231" />
<atomRef index="64" />
</cell>
<cell>
<cellProperties index="1449" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="246" />
<atomRef index="235" />
<atomRef index="141" />
</cell>
<cell>
<cellProperties index="1450" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="162" />
<atomRef index="164" />
<atomRef index="184" />
</cell>
<cell>
<cellProperties index="1451" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="88" />
<atomRef index="87" />
<atomRef index="112" />
</cell>
<cell>
<cellProperties index="1452" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="16" />
<atomRef index="8" />
<atomRef index="15" />
</cell>
<cell>
<cellProperties index="1453" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="126" />
<atomRef index="101" />
<atomRef index="124" />
</cell>
<cell>
<cellProperties index="1454" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="128" />
<atomRef index="152" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties index="1455" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="134" />
<atomRef index="130" />
<atomRef index="154" />
</cell>
<cell>
<cellProperties index="1456" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="152" />
<atomRef index="154" />
<atomRef index="130" />
</cell>
<cell>
<cellProperties index="1457" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="21" />
<atomRef index="20" />
<atomRef index="13" />
</cell>
<cell>
<cellProperties index="1458" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="140" />
<atomRef index="118" />
<atomRef index="117" />
</cell>
<cell>
<cellProperties index="1459" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="18" />
<atomRef index="10" />
<atomRef index="11" />
</cell>
<cell>
<cellProperties index="1460" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="200" />
</cell>
<cell>
<cellProperties index="1461" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="227" />
<atomRef index="62" />
<atomRef index="42" />
</cell>
<cell>
<cellProperties index="1462" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="92" />
<atomRef index="69" />
<atomRef index="68" />
</cell>
<cell>
<cellProperties index="1463" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="177" />
<atomRef index="196" />
<atomRef index="176" />
</cell>
<cell>
<cellProperties index="1464" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="194" />
<atomRef index="176" />
<atomRef index="196" />
</cell>
<cell>
<cellProperties index="1465" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="186" />
<atomRef index="207" />
<atomRef index="205" />
</cell>
<cell>
<cellProperties index="1466" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="199" />
<atomRef index="203" />
<atomRef index="204" />
</cell>
<cell>
<cellProperties index="1467" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="199" />
<atomRef index="204" />
<atomRef index="182" />
</cell>
<cell>
<cellProperties index="1468" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="57" />
<atomRef index="59" />
<atomRef index="64" />
</cell>
<cell>
<cellProperties index="1469" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="75" />
<atomRef index="73" />
<atomRef index="51" />
</cell>
<cell>
<cellProperties index="1470" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="223" />
<atomRef index="208" />
<atomRef index="220" />
</cell>
<cell>
<cellProperties index="1471" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="30" />
<atomRef index="6" />
<atomRef index="4" />
</cell>
<cell>
<cellProperties index="1472" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="184" />
<atomRef index="186" />
<atomRef index="205" />
</cell>
<cell>
<cellProperties index="1473" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="320" />
<atomRef index="28" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties index="1474" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="159" />
<atomRef index="162" />
<atomRef index="179" />
</cell>
<cell>
<cellProperties index="1475" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="73" />
<atomRef index="49" />
<atomRef index="51" />
</cell>
<cell>
<cellProperties index="1476" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="56" />
<atomRef index="37" />
<atomRef index="54" />
</cell>
<cell>
<cellProperties index="1477" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="191" />
<atomRef index="169" />
<atomRef index="189" />
</cell>
<cell>
<cellProperties index="1478" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="30" />
</cell>
<cell>
<cellProperties index="1479" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="30" />
<atomRef index="47" />
<atomRef index="31" />
</cell>
<cell>
<cellProperties index="1480" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="213" />
<atomRef index="194" />
<atomRef index="196" />
</cell>
<cell>
<cellProperties index="1481" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="121" />
<atomRef index="123" />
<atomRef index="145" />
</cell>
<cell>
<cellProperties index="1482" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="39" />
<atomRef index="38" />
<atomRef index="22" />
</cell>
<cell>
<cellProperties index="1483" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="191" />
<atomRef index="173" />
<atomRef index="171" />
</cell>
<cell>
<cellProperties index="1484" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="121" />
<atomRef index="145" />
<atomRef index="143" />
</cell>
<cell>
<cellProperties index="1485" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="60" />
<atomRef index="84" />
<atomRef index="59" />
</cell>
<cell>
<cellProperties index="1486" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="169" />
<atomRef index="146" />
<atomRef index="167" />
</cell>
<cell>
<cellProperties index="1487" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="228" />
<atomRef index="182" />
<atomRef index="163" />
</cell>
<cell>
<cellProperties index="1488" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="103" />
<atomRef index="126" />
<atomRef index="128" />
</cell>
<cell>
<cellProperties index="1489" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="33" />
<atomRef index="14" />
<atomRef index="7" />
</cell>
<cell>
<cellProperties index="1490" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="33" />
<atomRef index="35" />
<atomRef index="14" />
</cell>
<cell>
<cellProperties index="1491" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="141" />
<atomRef index="143" />
<atomRef index="164" />
</cell>
<cell>
<cellProperties index="1492" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="126" />
<atomRef index="124" />
<atomRef index="148" />
</cell>
<cell>
<cellProperties index="1493" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="177" />
<atomRef index="155" />
<atomRef index="156" />
</cell>
<cell>
<cellProperties index="1494" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="183" />
<atomRef index="200" />
<atomRef index="201" />
</cell>
<cell>
<cellProperties index="1495" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="2" />
<atomRef index="32" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties index="1496" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="55" />
<atomRef index="53" />
<atomRef index="36" />
</cell>
<cell>
<cellProperties index="1497" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="96" />
<atomRef index="98" />
<atomRef index="121" />
</cell>
<cell>
<cellProperties index="1498" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="118" />
<atomRef index="95" />
<atomRef index="94" />
</cell>
<cell>
<cellProperties index="1499" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="164" />
<atomRef index="186" />
<atomRef index="184" />
</cell>
<cell>
<cellProperties index="1500" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="154" />
<atomRef index="152" />
<atomRef index="173" />
</cell>
<cell>
<cellProperties index="1501" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="154" />
<atomRef index="173" />
<atomRef index="248" />
</cell>
<cell>
<cellProperties index="1502" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="167" />
<atomRef index="146" />
<atomRef index="144" />
</cell>
<cell>
<cellProperties index="1503" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="167" />
<atomRef index="144" />
<atomRef index="165" />
</cell>
<cell>
<cellProperties index="1504" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="132" />
<atomRef index="155" />
<atomRef index="153" />
</cell>
<cell>
<cellProperties index="1505" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="189" />
<atomRef index="167" />
<atomRef index="187" />
</cell>
<cell>
<cellProperties index="1506" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="122" />
<atomRef index="99" />
<atomRef index="97" />
</cell>
<cell>
<cellProperties index="1507" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="323" />
<atomRef index="118" />
<atomRef index="142" />
</cell>
<cell>
<cellProperties index="1508" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="168" />
<atomRef index="170" />
<atomRef index="188" />
</cell>
<cell>
<cellProperties index="1509" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="51" />
<atomRef index="34" />
<atomRef index="36" />
</cell>
<cell>
<cellProperties index="1510" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="154" />
<atomRef index="175" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties index="1511" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="154" />
<atomRef index="158" />
<atomRef index="134" />
</cell>
<cell>
<cellProperties index="1512" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="108" />
<atomRef index="83" />
<atomRef index="109" />
</cell>
<cell>
<cellProperties index="1513" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="234" />
<atomRef index="244" />
<atomRef index="136" />
</cell>
<cell>
<cellProperties index="1514" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="45" />
<atomRef index="27" />
<atomRef index="43" />
</cell>
<cell>
<cellProperties index="1515" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="232" />
<atomRef index="83" />
<atomRef index="108" />
</cell>
<cell>
<cellProperties index="1516" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="136" />
<atomRef index="138" />
<atomRef index="114" />
</cell>
<cell>
<cellProperties index="1517" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="321" />
<atomRef index="1" />
<atomRef index="28" />
</cell>
<cell>
<cellProperties index="1518" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="204" />
<atomRef index="185" />
<atomRef index="325" />
</cell>
<cell>
<cellProperties index="1519" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="85" />
<atomRef index="110" />
<atomRef index="84" />
</cell>
<cell>
<cellProperties index="1520" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="143" />
<atomRef index="145" />
<atomRef index="166" />
</cell>
<cell>
<cellProperties index="1521" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="126" />
<atomRef index="150" />
<atomRef index="128" />
</cell>
<cell>
<cellProperties index="1522" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="145" />
<atomRef index="168" />
<atomRef index="166" />
</cell>
<cell>
<cellProperties index="1523" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="143" />
<atomRef index="166" />
<atomRef index="164" />
</cell>
<cell>
<cellProperties index="1524" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="209" />
<atomRef index="192" />
<atomRef index="211" />
</cell>
<cell>
<cellProperties index="1525" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="124" />
<atomRef index="99" />
<atomRef index="122" />
</cell>
<cell>
<cellProperties index="1526" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="119" />
<atomRef index="141" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties index="1527" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="29" />
</cell>
<cell>
<cellProperties index="1528" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="124" />
<atomRef index="101" />
<atomRef index="99" />
</cell>
<cell>
<cellProperties index="1529" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="123" />
<atomRef index="147" />
<atomRef index="145" />
</cell>
<cell>
<cellProperties index="1530" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="107" />
<atomRef index="130" />
<atomRef index="134" />
</cell>
<cell>
<cellProperties index="1531" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="135" />
<atomRef index="157" />
<atomRef index="156" />
</cell>
<cell>
<cellProperties index="1532" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="135" />
<atomRef index="133" />
<atomRef index="157" />
</cell>
<cell>
<cellProperties index="1533" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="71" />
<atomRef index="94" />
<atomRef index="95" />
</cell>
<cell>
<cellProperties index="1534" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="111" />
<atomRef index="134" />
<atomRef index="133" />
</cell>
<cell>
<cellProperties index="1535" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="87" />
<atomRef index="82" />
<atomRef index="107" />
</cell>
<cell>
<cellProperties index="1536" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="35" />
<atomRef index="50" />
<atomRef index="52" />
</cell>
<cell>
<cellProperties index="1537" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="6" />
<atomRef index="5" />
<atomRef index="3" />
</cell>
<cell>
<cellProperties index="1538" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="248" />
<atomRef index="175" />
<atomRef index="154" />
</cell>
<cell>
<cellProperties index="1539" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="247" />
<atomRef index="212" />
<atomRef index="195" />
</cell>
<cell>
<cellProperties index="1540" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="91" />
<atomRef index="66" />
<atomRef index="89" />
</cell>
<cell>
<cellProperties index="1541" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="247" />
<atomRef index="193" />
<atomRef index="212" />
</cell>
<cell>
<cellProperties index="1542" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="228" />
<atomRef index="161" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties index="1543" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="165" />
<atomRef index="142" />
<atomRef index="324" />
</cell>
<cell>
<cellProperties index="1544" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="205" />
<atomRef index="207" />
<atomRef index="218" />
</cell>
<cell>
<cellProperties index="1545" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="227" />
<atomRef index="42" />
<atomRef index="56" />
</cell>
<cell>
<cellProperties index="1546" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="222" />
<atomRef index="221" />
<atomRef index="224" />
</cell>
<cell>
<cellProperties index="1547" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="221" />
<atomRef index="211" />
<atomRef index="224" />
</cell>
<cell>
<cellProperties index="1548" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="230" />
<atomRef index="55" />
<atomRef index="38" />
</cell>
<cell>
<cellProperties index="1549" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="225" />
<atomRef index="213" />
<atomRef index="214" />
</cell>
<cell>
<cellProperties index="1550" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="158" />
<atomRef index="175" />
<atomRef index="249" />
</cell>
<cell>
<cellProperties index="1551" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="125" />
<atomRef index="127" />
<atomRef index="149" />
</cell>
<cell>
<cellProperties index="1552" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="145" />
<atomRef index="147" />
<atomRef index="168" />
</cell>
<cell>
<cellProperties index="1553" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="19" />
<atomRef index="11" />
<atomRef index="12" />
</cell>
<cell>
<cellProperties index="1554" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="17" />
<atomRef index="8" />
<atomRef index="10" />
</cell>
<cell>
<cellProperties index="1555" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="236" />
<atomRef index="133" />
<atomRef index="134" />
</cell>
<cell>
<cellProperties index="1556" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="228" />
<atomRef index="238" />
<atomRef index="182" />
</cell>
<cell>
<cellProperties index="1557" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="50" />
<atomRef index="74" />
<atomRef index="76" />
</cell>
<cell>
<cellProperties index="1558" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="202" />
<atomRef index="201" />
<atomRef index="216" />
</cell>
<cell>
<cellProperties index="1559" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="189" />
<atomRef index="169" />
<atomRef index="167" />
</cell>
<cell>
<cellProperties index="1560" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="81" />
<atomRef index="55" />
<atomRef index="58" />
</cell>
<cell>
<cellProperties index="1561" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="58" />
<atomRef index="55" />
<atomRef index="230" />
</cell>
<cell>
<cellProperties index="1562" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="76" />
<atomRef index="99" />
<atomRef index="101" />
</cell>
<cell>
<cellProperties index="1563" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="200" />
<atomRef index="205" />
<atomRef index="201" />
</cell>
<cell>
<cellProperties index="1564" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="133" />
</cell>
<cell>
<cellProperties index="1565" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="23" />
<atomRef index="22" />
<atomRef index="15" />
</cell>
<cell>
<cellProperties index="1566" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="224" />
<atomRef index="211" />
<atomRef index="213" />
</cell>
<cell>
<cellProperties index="1567" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="86" />
<atomRef index="88" />
<atomRef index="111" />
</cell>
<cell>
<cellProperties index="1568" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="112" />
<atomRef index="111" />
<atomRef index="88" />
</cell>
<cell>
<cellProperties index="1569" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="153" />
<atomRef index="233" />
<atomRef index="131" />
</cell>
<cell>
<cellProperties index="1570" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="225" />
<atomRef index="214" />
<atomRef index="212" />
</cell>
<cell>
<cellProperties index="1571" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="30" />
<atomRef index="4" />
<atomRef index="29" />
</cell>
<cell>
<cellProperties index="1572" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="225" />
<atomRef index="212" />
<atomRef index="210" />
</cell>
<cell>
<cellProperties index="1573" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="160" />
<atomRef index="180" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties index="1574" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="182" />
<atomRef index="325" />
<atomRef index="163" />
</cell>
<cell>
<cellProperties index="1575" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="147" />
<atomRef index="149" />
<atomRef index="168" />
</cell>
<cell>
<cellProperties index="1576" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="45" />
<atomRef index="69" />
<atomRef index="70" />
</cell>
<cell>
<cellProperties index="1577" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="149" />
<atomRef index="151" />
<atomRef index="170" />
</cell>
<cell>
<cellProperties index="1578" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="203" />
<atomRef index="199" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties index="1579" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="161" />
<atomRef index="140" />
<atomRef index="139" />
</cell>
<cell>
<cellProperties index="1580" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="179" />
<atomRef index="183" />
<atomRef index="201" />
</cell>
<cell>
<cellProperties index="1581" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="35" />
<atomRef index="33" />
<atomRef index="50" />
</cell>
<cell>
<cellProperties index="1582" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="96" />
<atomRef index="73" />
<atomRef index="98" />
</cell>
<cell>
<cellProperties index="1583" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="81" />
<atomRef index="231" />
<atomRef index="242" />
</cell>
<cell>
<cellProperties index="1584" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="45" />
<atomRef index="70" />
<atomRef index="46" />
</cell>
<cell>
<cellProperties index="1585" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="70" />
<atomRef index="93" />
<atomRef index="94" />
</cell>
<cell>
<cellProperties index="1586" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="229" />
<atomRef index="60" />
<atomRef index="59" />
</cell>
<cell>
<cellProperties index="1587" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="84" />
<atomRef index="109" />
<atomRef index="83" />
</cell>
<cell>
<cellProperties index="1588" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="91" />
<atomRef index="114" />
<atomRef index="115" />
</cell>
<cell>
<cellProperties index="1589" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="173" />
<atomRef index="191" />
<atomRef index="193" />
</cell>
<cell>
<cellProperties index="1590" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="48" />
<atomRef index="74" />
<atomRef index="50" />
</cell>
<cell>
<cellProperties index="1591" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="249" />
<atomRef index="178" />
<atomRef index="158" />
</cell>
<cell>
<cellProperties index="1592" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="178" />
</cell>
<cell>
<cellProperties index="1593" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="198" />
</cell>
<cell>
<cellProperties index="1594" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="166" />
<atomRef index="188" />
<atomRef index="186" />
</cell>
<cell>
<cellProperties index="1595" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="142" />
<atomRef index="163" />
<atomRef index="324" />
</cell>
<cell>
<cellProperties index="1596" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="57" />
<atomRef index="64" />
<atomRef index="241" />
</cell>
<cell>
<cellProperties index="1597" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="174" />
<atomRef index="176" />
<atomRef index="194" />
</cell>
<cell>
<cellProperties index="1598" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="52" />
<atomRef index="78" />
<atomRef index="54" />
</cell>
<cell>
<cellProperties index="1599" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="90" />
<atomRef index="96" />
<atomRef index="119" />
</cell>
<cell>
<cellProperties index="1600" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="90" />
<atomRef index="119" />
<atomRef index="245" />
</cell>
<cell>
<cellProperties index="1601" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="241" />
<atomRef index="58" />
<atomRef index="57" />
</cell>
<cell>
<cellProperties index="1602" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="230" />
<atomRef index="57" />
<atomRef index="58" />
</cell>
<cell>
<cellProperties index="1603" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="119" />
<atomRef index="143" />
<atomRef index="141" />
</cell>
<cell>
<cellProperties index="1604" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="185" />
<atomRef index="206" />
<atomRef index="187" />
</cell>
<cell>
<cellProperties index="1605" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="98" />
<atomRef index="73" />
<atomRef index="75" />
</cell>
<cell>
<cellProperties index="1606" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="36" />
<atomRef index="9" />
<atomRef index="16" />
</cell>
<cell>
<cellProperties index="1607" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="149" />
<atomRef index="170" />
<atomRef index="168" />
</cell>
<cell>
<cellProperties index="1608" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="64" />
<atomRef index="231" />
<atomRef index="241" />
</cell>
<cell>
<cellProperties index="1609" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="163" />
<atomRef index="142" />
<atomRef index="140" />
</cell>
<cell>
<cellProperties index="1610" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="85" />
<atomRef index="84" />
<atomRef index="60" />
</cell>
<cell>
<cellProperties index="1611" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="31" />
<atomRef index="47" />
<atomRef index="48" />
</cell>
<cell>
<cellProperties index="1612" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="151" />
<atomRef index="127" />
<atomRef index="129" />
</cell>
<cell>
<cellProperties index="1613" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="245" />
<atomRef index="119" />
<atomRef index="235" />
</cell>
<cell>
<cellProperties index="1614" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="20" />
<atomRef index="19" />
<atomRef index="12" />
</cell>
<cell>
<cellProperties index="1615" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="123" />
<atomRef index="125" />
<atomRef index="147" />
</cell>
<cell>
<cellProperties index="1616" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="166" />
<atomRef index="168" />
<atomRef index="188" />
</cell>
<cell>
<cellProperties index="1617" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="191" />
<atomRef index="210" />
<atomRef index="212" />
</cell>
<cell>
<cellProperties index="1618" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="126" />
<atomRef index="148" />
<atomRef index="150" />
</cell>
<cell>
<cellProperties index="1619" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="41" />
</cell>
<cell>
<cellProperties index="1620" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="103" />
<atomRef index="101" />
<atomRef index="126" />
</cell>
<cell>
<cellProperties index="1621" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="163" />
<atomRef index="325" />
<atomRef index="324" />
</cell>
<cell>
<cellProperties index="1622" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="70" />
<atomRef index="94" />
<atomRef index="71" />
</cell>
<cell>
<cellProperties index="1623" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="98" />
<atomRef index="75" />
<atomRef index="100" />
</cell>
<cell>
<cellProperties index="1624" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="149" />
<atomRef index="127" />
<atomRef index="151" />
</cell>
<cell>
<cellProperties index="1625" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="46" />
<atomRef index="70" />
<atomRef index="71" />
</cell>
<cell>
<cellProperties index="1626" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="192" />
<atomRef index="194" />
<atomRef index="211" />
</cell>
<cell>
<cellProperties index="1627" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="20" />
<atomRef index="12" />
<atomRef index="13" />
</cell>
<cell>
<cellProperties index="1628" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="103" />
<atomRef index="105" />
<atomRef index="80" />
</cell>
<cell>
<cellProperties index="1629" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="69" />
<atomRef index="43" />
<atomRef index="68" />
</cell>
<cell>
<cellProperties index="1630" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="73" />
<atomRef index="96" />
<atomRef index="67" />
</cell>
<cell>
<cellProperties index="1631" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="244" />
<atomRef index="137" />
<atomRef index="159" />
</cell>
<cell>
<cellProperties index="1632" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="24" />
<atomRef index="23" />
<atomRef index="17" />
</cell>
<cell>
<cellProperties index="1633" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="161" />
<atomRef index="160" />
<atomRef index="181" />
</cell>
<cell>
<cellProperties index="1634" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="90" />
<atomRef index="66" />
<atomRef index="67" />
</cell>
<cell>
<cellProperties index="1635" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="134" />
</cell>
<cell>
<cellProperties index="1636" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="175" />
<atomRef index="240" />
<atomRef index="249" />
</cell>
<cell>
<cellProperties index="1637" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="67" />
<atomRef index="66" />
<atomRef index="44" />
</cell>
<cell>
<cellProperties index="1638" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="116" />
<atomRef index="93" />
<atomRef index="92" />
</cell>
<cell>
<cellProperties index="1639" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="22" />
<atomRef index="16" />
<atomRef index="15" />
</cell>
<cell>
<cellProperties index="1640" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="110" />
<atomRef index="133" />
<atomRef index="135" />
</cell>
<cell>
<cellProperties index="1641" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="28" />
<atomRef index="318" />
<atomRef index="317" />
</cell>
<cell>
<cellProperties index="1642" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="227" />
<atomRef index="63" />
<atomRef index="62" />
</cell>
<cell>
<cellProperties index="1643" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="23" />
<atomRef index="15" />
<atomRef index="17" />
</cell>
<cell>
<cellProperties index="1644" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="223" />
<atomRef index="210" />
<atomRef index="208" />
</cell>
<cell>
<cellProperties index="1645" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="225" />
<atomRef index="222" />
<atomRef index="224" />
</cell>
<cell>
<cellProperties index="1646" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="209" />
<atomRef index="211" />
<atomRef index="221" />
</cell>
<cell>
<cellProperties index="1647" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="220" />
<atomRef index="206" />
<atomRef index="217" />
</cell>
<cell>
<cellProperties index="1648" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="76" />
<atomRef index="101" />
<atomRef index="78" />
</cell>
<cell>
<cellProperties index="1649" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="24" />
<atomRef index="18" />
<atomRef index="25" />
</cell>
<cell>
<cellProperties index="1650" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="53" />
<atomRef index="51" />
<atomRef index="36" />
</cell>
<cell>
<cellProperties index="1651" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="55" />
<atomRef index="36" />
<atomRef index="38" />
</cell>
<cell>
<cellProperties index="1652" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="88" />
<atomRef index="65" />
<atomRef index="87" />
</cell>
<cell>
<cellProperties index="1653" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="182" />
<atomRef index="204" />
<atomRef index="325" />
</cell>
<cell>
<cellProperties index="1654" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="185" />
<atomRef index="204" />
<atomRef index="206" />
</cell>
<cell>
<cellProperties index="1655" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="68" />
<atomRef index="66" />
<atomRef index="91" />
</cell>
<cell>
<cellProperties index="1656" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="28" />
<atomRef index="2" />
<atomRef index="318" />
</cell>
<cell>
<cellProperties index="1657" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="56" />
<atomRef index="42" />
<atomRef index="37" />
</cell>
<cell>
<cellProperties index="1658" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="71" />
<atomRef index="95" />
<atomRef index="72" />
</cell>
<cell>
<cellProperties index="1659" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="117" />
<atomRef index="93" />
<atomRef index="116" />
</cell>
<cell>
<cellProperties index="1660" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="86" />
<atomRef index="61" />
<atomRef index="65" />
</cell>
<cell>
<cellProperties index="1661" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="7" />
<atomRef index="5" />
<atomRef index="6" />
</cell>
<cell>
<cellProperties index="1662" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="39" />
<atomRef index="22" />
<atomRef index="23" />
</cell>
<cell>
<cellProperties index="1663" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="6" />
<atomRef index="3" />
<atomRef index="4" />
</cell>
<cell>
<cellProperties index="1664" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="4" />
<atomRef index="0" />
<atomRef index="1" />
</cell>
<cell>
<cellProperties index="1665" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="322" />
<atomRef index="97" />
<atomRef index="74" />
</cell>
<cell>
<cellProperties index="1666" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="31" />
<atomRef index="7" />
<atomRef index="6" />
</cell>
<cell>
<cellProperties index="1667" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="42" />
</cell>
<cell>
<cellProperties index="1668" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="216" />
<atomRef index="201" />
<atomRef index="215" />
</cell>
<cell>
<cellProperties index="1669" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="328" />
<atomRef index="233" />
<atomRef index="153" />
</cell>
<cell>
<cellProperties index="1670" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="85" />
<atomRef index="111" />
<atomRef index="110" />
</cell>
<cell>
<cellProperties index="1671" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="86" />
<atomRef index="60" />
<atomRef index="61" />
</cell>
<cell>
<cellProperties index="1672" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="36" />
<atomRef index="34" />
<atomRef index="9" />
</cell>
<cell>
<cellProperties index="1673" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="321" />
<atomRef index="28" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties index="1674" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="139" />
<atomRef index="115" />
<atomRef index="138" />
</cell>
<cell>
<cellProperties index="1675" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="139" />
<atomRef index="116" />
<atomRef index="115" />
</cell>
<cell>
<cellProperties index="1676" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="161" />
<atomRef index="138" />
<atomRef index="160" />
</cell>
<cell>
<cellProperties index="1677" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="161" />
<atomRef index="139" />
<atomRef index="138" />
</cell>
<cell>
<cellProperties index="1678" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="88" />
<atomRef index="86" />
<atomRef index="65" />
</cell>
<cell>
<cellProperties index="1679" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="65" />
<atomRef index="61" />
<atomRef index="62" />
</cell>
<cell>
<cellProperties index="1680" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="19" />
<atomRef index="26" />
<atomRef index="25" />
</cell>
<cell>
<cellProperties index="1681" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="172" />
<atomRef index="151" />
<atomRef index="328" />
</cell>
<cell>
<cellProperties index="1682" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="49" />
<atomRef index="317" />
<atomRef index="32" />
</cell>
<cell>
<cellProperties index="1683" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="51" />
<atomRef index="32" />
<atomRef index="34" />
</cell>
<cell>
<cellProperties index="1684" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="51" />
<atomRef index="49" />
<atomRef index="32" />
</cell>
<cell>
<cellProperties index="1685" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="163" />
<atomRef index="140" />
<atomRef index="237" />
</cell>
<cell>
<cellProperties index="1686" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="229" />
<atomRef index="59" />
<atomRef index="39" />
</cell>
<cell>
<cellProperties index="1687" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="61" />
<atomRef index="60" />
<atomRef index="40" />
</cell>
<cell>
<cellProperties index="1688" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="61" />
<atomRef index="40" />
<atomRef index="41" />
</cell>
<cell>
<cellProperties index="1689" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="219" />
<atomRef index="216" />
<atomRef index="215" />
</cell>
<cell>
<cellProperties index="1690" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="190" />
<atomRef index="192" />
<atomRef index="209" />
</cell>
<cell>
<cellProperties index="1691" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="185" />
<atomRef index="324" />
<atomRef index="325" />
</cell>
<cell>
<cellProperties index="1692" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="320" />
<atomRef index="27" />
<atomRef index="321" />
</cell>
<cell>
<cellProperties index="1693" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="43" />
<atomRef index="27" />
<atomRef index="320" />
</cell>
<cell>
<cellProperties index="1694" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="207" />
<atomRef index="221" />
<atomRef index="218" />
</cell>
<cell>
<cellProperties index="1695" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="222" />
<atomRef index="218" />
<atomRef index="221" />
</cell>
<cell>
<cellProperties index="1696" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="202" />
<atomRef index="216" />
<atomRef index="217" />
</cell>
<cell>
<cellProperties index="1697" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="220" />
<atomRef index="217" />
<atomRef index="216" />
</cell>
<cell>
<cellProperties index="1698" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="207" />
<atomRef index="209" />
<atomRef index="221" />
</cell>
<cell>
<cellProperties index="1699" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="202" />
<atomRef index="217" />
<atomRef index="203" />
</cell>
<cell>
<cellProperties index="1700" type="TRIANGLE"  />
<nrOfStructures value="3"/>
<atomRef index="165" />
<atomRef index="185" />
<atomRef index="187" />
</cell>
</structuralComponent>
</multiComponent>
<multiComponent name="surface points" >
<structuralComponent  name="surfacePoints-CamiTKModel"  mode="WIREFRAME_AND_SURFACE" >
<nrOfStructures value="260"/>
<atomRef index="0" />
<atomRef index="1" />
<atomRef index="2" />
<atomRef index="3" />
<atomRef index="4" />
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="8" />
<atomRef index="9" />
<atomRef index="10" />
<atomRef index="11" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="15" />
<atomRef index="16" />
<atomRef index="17" />
<atomRef index="18" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="22" />
<atomRef index="23" />
<atomRef index="24" />
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="27" />
<atomRef index="28" />
<atomRef index="29" />
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="32" />
<atomRef index="33" />
<atomRef index="34" />
<atomRef index="35" />
<atomRef index="36" />
<atomRef index="37" />
<atomRef index="38" />
<atomRef index="39" />
<atomRef index="40" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="43" />
<atomRef index="44" />
<atomRef index="45" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="49" />
<atomRef index="50" />
<atomRef index="51" />
<atomRef index="52" />
<atomRef index="53" />
<atomRef index="54" />
<atomRef index="55" />
<atomRef index="56" />
<atomRef index="57" />
<atomRef index="58" />
<atomRef index="59" />
<atomRef index="60" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="64" />
<atomRef index="65" />
<atomRef index="66" />
<atomRef index="67" />
<atomRef index="68" />
<atomRef index="69" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="73" />
<atomRef index="74" />
<atomRef index="75" />
<atomRef index="76" />
<atomRef index="77" />
<atomRef index="78" />
<atomRef index="79" />
<atomRef index="80" />
<atomRef index="81" />
<atomRef index="82" />
<atomRef index="83" />
<atomRef index="84" />
<atomRef index="85" />
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="89" />
<atomRef index="90" />
<atomRef index="91" />
<atomRef index="92" />
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="96" />
<atomRef index="97" />
<atomRef index="98" />
<atomRef index="99" />
<atomRef index="100" />
<atomRef index="101" />
<atomRef index="102" />
<atomRef index="103" />
<atomRef index="104" />
<atomRef index="105" />
<atomRef index="106" />
<atomRef index="107" />
<atomRef index="108" />
<atomRef index="109" />
<atomRef index="110" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="113" />
<atomRef index="114" />
<atomRef index="115" />
<atomRef index="116" />
<atomRef index="117" />
<atomRef index="118" />
<atomRef index="119" />
<atomRef index="120" />
<atomRef index="121" />
<atomRef index="122" />
<atomRef index="123" />
<atomRef index="124" />
<atomRef index="125" />
<atomRef index="126" />
<atomRef index="127" />
<atomRef index="128" />
<atomRef index="129" />
<atomRef index="130" />
<atomRef index="131" />
<atomRef index="132" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="135" />
<atomRef index="136" />
<atomRef index="137" />
<atomRef index="138" />
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="141" />
<atomRef index="142" />
<atomRef index="143" />
<atomRef index="144" />
<atomRef index="145" />
<atomRef index="146" />
<atomRef index="147" />
<atomRef index="148" />
<atomRef index="149" />
<atomRef index="150" />
<atomRef index="151" />
<atomRef index="152" />
<atomRef index="153" />
<atomRef index="154" />
<atomRef index="155" />
<atomRef index="156" />
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="159" />
<atomRef index="160" />
<atomRef index="161" />
<atomRef index="162" />
<atomRef index="163" />
<atomRef index="164" />
<atomRef index="165" />
<atomRef index="166" />
<atomRef index="167" />
<atomRef index="168" />
<atomRef index="169" />
<atomRef index="170" />
<atomRef index="171" />
<atomRef index="172" />
<atomRef index="173" />
<atomRef index="174" />
<atomRef index="175" />
<atomRef index="176" />
<atomRef index="177" />
<atomRef index="178" />
<atomRef index="179" />
<atomRef index="180" />
<atomRef index="181" />
<atomRef index="182" />
<atomRef index="183" />
<atomRef index="184" />
<atomRef index="185" />
<atomRef index="186" />
<atomRef index="187" />
<atomRef index="188" />
<atomRef index="189" />
<atomRef index="190" />
<atomRef index="191" />
<atomRef index="192" />
<atomRef index="193" />
<atomRef index="194" />
<atomRef index="195" />
<atomRef index="196" />
<atomRef index="197" />
<atomRef index="198" />
<atomRef index="199" />
<atomRef index="200" />
<atomRef index="201" />
<atomRef index="202" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="205" />
<atomRef index="206" />
<atomRef index="207" />
<atomRef index="208" />
<atomRef index="209" />
<atomRef index="210" />
<atomRef index="211" />
<atomRef index="212" />
<atomRef index="213" />
<atomRef index="214" />
<atomRef index="215" />
<atomRef index="216" />
<atomRef index="217" />
<atomRef index="218" />
<atomRef index="219" />
<atomRef index="220" />
<atomRef index="221" />
<atomRef index="222" />
<atomRef index="223" />
<atomRef index="224" />
<atomRef index="225" />
<atomRef index="226" />
<atomRef index="227" />
<atomRef index="228" />
<atomRef index="229" />
<atomRef index="230" />
<atomRef index="231" />
<atomRef index="232" />
<atomRef index="233" />
<atomRef index="234" />
<atomRef index="235" />
<atomRef index="236" />
<atomRef index="237" />
<atomRef index="238" />
<atomRef index="239" />
<atomRef index="240" />
<atomRef index="241" />
<atomRef index="242" />
<atomRef index="243" />
<atomRef index="244" />
<atomRef index="245" />
<atomRef index="246" />
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="317" />
<atomRef index="318" />
<atomRef index="320" />
<atomRef index="321" />
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="328" />
</structuralComponent>
</multiComponent>
<structuralComponent  name="moving"  mode="WIREFRAME_AND_SURFACE" >
<atomRef index="5" />
<atomRef index="6" />
<atomRef index="7" />
<atomRef index="12" />
<atomRef index="13" />
<atomRef index="14" />
<atomRef index="19" />
<atomRef index="20" />
<atomRef index="21" />
<atomRef index="25" />
<atomRef index="26" />
<atomRef index="30" />
<atomRef index="31" />
<atomRef index="33" />
<atomRef index="35" />
<atomRef index="37" />
<atomRef index="41" />
<atomRef index="42" />
<atomRef index="46" />
<atomRef index="47" />
<atomRef index="48" />
<atomRef index="50" />
<atomRef index="52" />
<atomRef index="54" />
<atomRef index="56" />
<atomRef index="61" />
<atomRef index="62" />
<atomRef index="63" />
<atomRef index="65" />
<atomRef index="70" />
<atomRef index="71" />
<atomRef index="72" />
<atomRef index="74" />
<atomRef index="76" />
<atomRef index="78" />
<atomRef index="80" />
<atomRef index="82" />
<atomRef index="86" />
<atomRef index="87" />
<atomRef index="88" />
<atomRef index="93" />
<atomRef index="94" />
<atomRef index="95" />
<atomRef index="97" />
<atomRef index="99" />
<atomRef index="101" />
<atomRef index="103" />
<atomRef index="105" />
<atomRef index="107" />
<atomRef index="111" />
<atomRef index="112" />
<atomRef index="116" />
<atomRef index="117" />
<atomRef index="118" />
<atomRef index="120" />
<atomRef index="122" />
<atomRef index="124" />
<atomRef index="126" />
<atomRef index="128" />
<atomRef index="130" />
<atomRef index="133" />
<atomRef index="134" />
<atomRef index="139" />
<atomRef index="140" />
<atomRef index="142" />
<atomRef index="144" />
<atomRef index="146" />
<atomRef index="148" />
<atomRef index="150" />
<atomRef index="152" />
<atomRef index="154" />
<atomRef index="157" />
<atomRef index="158" />
<atomRef index="161" />
<atomRef index="163" />
<atomRef index="165" />
<atomRef index="167" />
<atomRef index="169" />
<atomRef index="171" />
<atomRef index="173" />
<atomRef index="175" />
<atomRef index="178" />
<atomRef index="181" />
<atomRef index="182" />
<atomRef index="185" />
<atomRef index="187" />
<atomRef index="189" />
<atomRef index="191" />
<atomRef index="193" />
<atomRef index="195" />
<atomRef index="197" />
<atomRef index="199" />
<atomRef index="203" />
<atomRef index="204" />
<atomRef index="206" />
<atomRef index="208" />
<atomRef index="210" />
<atomRef index="212" />
<atomRef index="214" />
<atomRef index="217" />
<atomRef index="220" />
<atomRef index="223" />
<atomRef index="225" />
<atomRef index="227" />
<atomRef index="228" />
<atomRef index="236" />
<atomRef index="237" />
<atomRef index="238" />
<atomRef index="239" />
<atomRef index="240" />
<atomRef index="247" />
<atomRef index="248" />
<atomRef index="249" />
<atomRef index="250" />
<atomRef index="252" />
<atomRef index="253" />
<atomRef index="255" />
<atomRef index="257" />
<atomRef index="260" />
<atomRef index="262" />
<atomRef index="265" />
<atomRef index="266" />
<atomRef index="268" />
<atomRef index="270" />
<atomRef index="272" />
<atomRef index="274" />
<atomRef index="276" />
<atomRef index="277" />
<atomRef index="281" />
<atomRef index="282" />
<atomRef index="283" />
<atomRef index="284" />
<atomRef index="286" />
<atomRef index="289" />
<atomRef index="291" />
<atomRef index="292" />
<atomRef index="294" />
<atomRef index="295" />
<atomRef index="301" />
<atomRef index="303" />
<atomRef index="304" />
<atomRef index="305" />
<atomRef index="306" />
<atomRef index="307" />
<atomRef index="308" />
<atomRef index="310" />
<atomRef index="312" />
<atomRef index="314" />
<atomRef index="315" />
<atomRef index="316" />
<atomRef index="322" />
<atomRef index="323" />
<atomRef index="324" />
<atomRef index="325" />
<atomRef index="326" />
<atomRef index="335" />
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>
