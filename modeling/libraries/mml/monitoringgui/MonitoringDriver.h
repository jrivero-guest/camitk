/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MONITORINGSIMULATIONDRIVER_H
#define MONITORINGSIMULATIONDRIVER_H

#include <QObject>
#include <qtimer.h>

#include "MonitoringGuiManager.h"

/**
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * TODO Comment class here
 *
 **/
class MonitoringDriver : public QObject {

    Q_OBJECT

public:

    /// Constructor
    MonitoringDriver(MonitoringGuiManager* guiManager);

    /// Destructor
    ~MonitoringDriver() override;

    void init();

    /// starts the timer that will repeatedly call play and emit doOneStep
    void startTimer();

    /// stop the timer
    void stopTimer();

    /// is the timer currently running
    bool isTimerActive();

signals:

    /// signal call when one step is to be performed
    void doOneStep();

public slots:

    /// Slot called when at each timer tick (i.e. after the same interval of time)
    void play();

private:
    /// the monitoring manager
    MonitoringGuiManager* guiManager;

    /// the simulation timer
    QTimer* timer;



};

#endif