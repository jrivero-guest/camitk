/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MONITORINGDIALOG_H
#define MONITORINGDIALOG_H

#include "MMLMonitoringGUIAPI.h"

#include <QDialog>
class QTableWidget;
class QTreeWidgetItem;

namespace Ui {
class MonitoringDialog;
}

class MonitoringGuiManager;

class StoppingCriterion;

/**
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * TODO Comment class here
 *
 **/
class /* needed if you monitoringgui is compiled as shared: MML_MONITORING_GUI_API*/ MonitoringDialog : public QDialog {
    Q_OBJECT

public:
    /**Default Constructor*/
    MonitoringDialog(MonitoringGuiManager* guiManager,  QWidget* parent = nullptr);
    /**Destructor*/
    ~MonitoringDialog() override;

    /// initialize dialog
    void init();

    /// update all tabs
    void updateAll();
    /// update monitors tab
    void updateMonitors();
    /// update stopping criteria tab
    void updateStoppingCriteria();
    /// update simulation tab
    void updateSimulation();
    /// update simulator specific widget
    void updateSimulatorWidget();
    /// get MmlIn file name
    QString getMmlInFileName();
    /// get the monitors table
    QTableWidget* getMonitorsTableWidget();

public slots:
    /// Slot called when Browse MmlIn file is clicked
    void browseMmlIn();
    /// Slot called when save MmlOut file is clicked
    void saveMmlOut();
    /// Slot called when save as csv file is clicked
    void saveCsv();
    /// Slot called when save MmlIn file is clicked
    void saveMmlIn();
    /// Slot called when Browse Pml file is clicked
    void browsePml();
    /// Slot called when Browse Lml file is clicked
    void browseLml();
    /// Slot called after dt editing
    void dtModified();
    /// Slot called after refresh editing
    void refreshModified();

    /** do one step of simulation
      * @return true only if the step was done
      */
    /// Slot called when the button Pause is clicked
    void pause();
    /// Slot called when the button Play is clicked
    void simulate();
    /// Slot called when the next step is clicked
    void simulateOneStep();
    /// Slot called when the button Rewind is clicked
    void rewind();

signals:
    /// emitted
    void monitorsChanged();


private:
    /// the ui dialog designed in qtdesigner
    Ui::MonitoringDialog* ui;

    /// the monitoring manager
    MonitoringGuiManager* guiManager;

    /// the initial bg color
    QColor bgColor;

    /// set the rew,play,pause,step buttons enable or not
    void enableButtons(bool enable);

    ///
    QTreeWidgetItem* createItem(StoppingCriterion* sc, QTreeWidgetItem* parent);
};

#endif // MONITORINGDIALOG_H