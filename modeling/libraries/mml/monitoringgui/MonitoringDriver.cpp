/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "MonitoringDriver.h"

#include <qapplication.h>
#include <qcursor.h>
#include <limits>

//--------------- Constructor ---------------------------------
MonitoringDriver::MonitoringDriver(MonitoringGuiManager* guiManager): QObject() {
    this->guiManager = guiManager;
    timer = nullptr; // not started yet
}

//--------------- Constructor ---------------------------------
MonitoringDriver::~MonitoringDriver() {
    stopTimer();
    delete timer;
    timer = nullptr;
}

//--------------- Constructor ---------------------------------
void MonitoringDriver::init() {
    // start the timer
    timer = new QTimer(this);

    // connect the timer tick signal to play()
    connect(timer, SIGNAL(timeout()), this, SLOT(play()));

}

//--------------- Constructor ---------------------------------
void MonitoringDriver::play() {
    if (!timer->isActive()) {
        // timer wasn't active, action to be done = launch the timer
        startTimer();
    }
    else {
        // timer is active, action to be done = play one step
        emit doOneStep();
    }
}

//--------------- Constructor ---------------------------------
void MonitoringDriver::startTimer() {
    timer->start();
}

//--------------- Constructor ---------------------------------
void MonitoringDriver::stopTimer() {
    timer->stop();
}

//--------------- Constructor ---------------------------------
bool MonitoringDriver::isTimerActive() {
    return timer->isActive();
}



