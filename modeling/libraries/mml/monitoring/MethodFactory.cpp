/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Stopping criteria includes
#include "MethodFactory.h"

#include "iostream"

// -------------------- constructor --------------------
MethodFactory::MethodFactory() {}

// -------------------- destructor --------------------
MethodFactory::~MethodFactory() {}

// -------------------- createMethod --------------------
Method* MethodFactory::createMethod(mml::Method& m) {
    if (dynamic_cast<mml::Threshold*>(&m)) {
        return (new Threshold((mml::Threshold*)&m));
    }
    else if (dynamic_cast<mml::MinThreshold*>(&m)) {
        return (new MinThreshold((mml::MinThreshold*)&m));
    }
    else if (dynamic_cast<mml::TimePeriodThreshold*>(&m)) {
        return (new TimePeriodThreshold((mml::TimePeriodThreshold*)&m));
    }
    else {
        std::cerr << "Method type error: unknown method" << std::endl;
        return NULL;
    }
}

