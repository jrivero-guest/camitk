/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef STOPPINGCRITERION_STOPPINGCRITERIA_METHOD_H
#define STOPPINGCRITERION_STOPPINGCRITERIA_METHOD_H

#include <vector>

#include <MonitorIn.hxx>
#include <MonitoringModel.hxx>

/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A Method represent how to make the boolean assessment with criteria data (a vector of double)
 */
class Method {
public:
    /**
    * constructor
    * @param the xsdcxx generated method class
    */
    Method(mml::Method* m);
    /// destructor
    virtual ~Method() = default;

    /// the scope of the method
    enum ScopeType {
        Any,
        Average,
        Sum,
        None
    };
    /**
    * return true if the vector of fouble values passed the test (see scope)
    */
    virtual bool test(std::vector<double>& values);
    /**
    * return true if the double passed the test
    */
    virtual bool individualTest(double tested) = 0;
    /// get Method name
    virtual std::string toString() = 0;
    /// get a String of the scope
    std::string scopeTosString();


protected:
    /**
    * scope of the method
    * Any: test is true if all indivitual test are true in the values vector
    * Average: test is true if the indivudal test of values vector's average is true
    * Sum:  test is true if the indivudal test of the sum of all double of the values vector is true
    * None: used for monitor wich are not applied to a structural component (ex: time)
    */
    ScopeType scope;
};

#endif // STOPPINGCRITERION_STOPPINGCRITERIA_METHOD_H
