/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_NONINTERACTIVESIMULATOR_H
#define SIMULATOR_NONINTERACTIVESIMULATOR_H
// Simulator includes
#include "Simulator.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * An interactive simulator is a simulator that we cannot control step by step (ex: Ansys)
 * Entire simulation is performed and then post-processed
 */
class NonInteractiveSimulator: public Simulator {
public:
    /// constructor
    NonInteractiveSimulator(MonitoringManager* monitoringManager);
    /// constructor
    NonInteractiveSimulator(MonitoringManager* monitoringManager, const char* file);
    /// destructor
    ~NonInteractiveSimulator() override = default;

    /// initialize simulator
    void init() = 0;
    /// end simultor
    void end() = 0;
    /// get current position for one atom (use parameters)
    void getPosition(int index, double position[3]) = 0;

    /** make entire simulation ans store results of all step
     * @return true if calculation succeded
     */
    virtual bool doCalc() = 0;
    /// get the time of simulation of a given step
    virtual double getTime(int step) = 0;
    /// get the number of steps after simulation
    virtual int getMaxStep() = 0;

    /// Create a pml file from an imput file
    void createPml(const char* inputFile, const char* pmlFile) = 0;



};

#endif // SIMULATOR_NONINTERACTIVESIMULATOR_H
