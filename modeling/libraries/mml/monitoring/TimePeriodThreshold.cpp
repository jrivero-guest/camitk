/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Stopping criteria includes
#include "TimePeriodThreshold.h"

// -------------------- constructor --------------------
TimePeriodThreshold::TimePeriodThreshold(mml::TimePeriodThreshold* m): Method(m), mappedObject(m) {
    value = m->minValue();
    iterations = m->iterations();
    increment = iterations;
}

// -------------------- individualTest --------------------
bool  TimePeriodThreshold::individualTest(double tested) {
    if (tested <= value) {
        increment--;
    }
    else {
        increment = iterations;
    }
    return (increment == 0);
}

// -------------------- toString --------------------
std::string TimePeriodThreshold::toString() {
    std::ostringstream strs;
    strs << value;
    std::string str = strs.str();

    return "< " + str;
}


