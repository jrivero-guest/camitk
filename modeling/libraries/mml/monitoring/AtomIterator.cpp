/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "AtomIterator.h"


// -------------------- constructor --------------------
AtomIterator::AtomIterator(PhysicalModel* pml, std::string targetList) {
    TargetList tlist = TargetList(targetList);
    if (tlist.indexedTargets()) {
        for (unsigned int i = 0; i < tlist.getNumberOfTargets(); i++) {
            atoms.push_back(pml->getAtom(tlist.getIndexedTarget(i)));
        }
    }
    else {
        for (unsigned int i = 0; i < tlist.getNumberOfTargets(); i++) {
            StructuralComponent* sc = dynamic_cast<StructuralComponent*>(pml->getComponentByName(tlist.getNamedTarget(i)));
            if (sc) {
                for (unsigned int j = 0; j < sc->getNumberOfStructures(); j++) {
                    Cell* c = dynamic_cast<Cell*>(sc->getStructure(j));
                    if (c) {
                        for (unsigned int k = 0; k < c->getNumberOfStructures(); k++) {
                            atoms.push_back(dynamic_cast<Atom*>(c->getStructure(k)));
                        }
                    }
                    else {
                        Atom* a = dynamic_cast<Atom*>(sc->getStructure(j));
                        if (a) {
                            atoms.push_back(a);
                        }
                    }
                }
            }
        }
    }

    index = 0;
}

// -------------------- begin --------------------
void AtomIterator::begin() {
    index = 0;
}

// -------------------- end --------------------
bool AtomIterator::end() {
    return index >= atoms.size();
}

// -------------------- next --------------------
void AtomIterator::next() {
    if (!end()) {
        index++;
    }
}

// -------------------- currentAtom --------------------
Atom* AtomIterator::currentAtom() {
    return atoms[index];
}

// -------------------- ++ --------------------
void AtomIterator::operator++() {
    if (!end()) {
        index++;
    }
}


