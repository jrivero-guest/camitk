/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Stopping criteria includes
#include "TimeMonitoring.h"

// -------------------- constructor --------------------
Time::Time(mml::Time* c, MonitoringManager* monitoringManager, MultipleCriterion* parent):  Criterion(c, monitoringManager, parent) {
    switch (c->unit()) {
        case mml::TimeUnit::ms:
            factor = 1000;
            unit = "ms";
            break;
        case mml::TimeUnit::s:
            factor = 1;
            unit = "s";
            break;
        case mml::TimeUnit::min:
            factor = ((double)1) / ((double)60);
            unit = "min";
            break;
        default:
            std::cerr << "Time unit error" << std::endl;
    }
}

// -------------------- calculate --------------------
void Time::calculate() {
    values.clear();
    values.push_back(monitoringManager->getCurrentTime());
}

// -------------------- getName --------------------
std::string Time::getName() {
    return "Time";
}
