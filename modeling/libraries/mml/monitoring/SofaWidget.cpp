/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "SofaWidget.h"

#include <QFile>
#include <QProcess>
#include <QTextStream>
#include <QMessageBox>

using namespace std;

//--------------- constructor ---------------------------------
SofaWidget::SofaWidget(QWidget* parent, SofaSimulator* sofaSimulator): SimulatorWidget(parent) {
    this->sofaSimulator = sofaSimulator;
    this->scnFile = sofaSimulator->getScnFile();
    this->scnFileTemp = scnFile;
    string::size_type pLast = scnFileTemp.rfind(".");
    if (pLast != string::npos) {
        scnFileTemp.erase(pLast);
        scnFileTemp += "-temp.scn";
    }
    ui.setupUi(this);

    connect(ui.applyChanges, SIGNAL(clicked()), this, SLOT(applyChanges()));
    connect(ui.runSofa, SIGNAL(clicked()), this, SLOT(runSofa()));

    xmlHighlighter = new XmlHighlighter(ui.text->document());

    QFile file(tr(scnFile.c_str()));
    if (file.open(QIODevice::ReadOnly)) {
        ui.text->document()->setPlainText(file.readAll());
    }


}

//--------------- SofaWidget ---------------------------------
SofaWidget::~SofaWidget() {
    if (xmlHighlighter) {
        delete xmlHighlighter;
    }
}

//--------------- applyChanges ---------------------------------
void SofaWidget::applyChanges() {
    // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING as the user interaction is required
    int r = QMessageBox::warning(this, tr("Apply scene changes"), tr("This will save current changes in scn file and reload simulation.\n Not saved data will be lost."), QMessageBox::Ok, QMessageBox::Cancel);
    if (r == QMessageBox::Ok) {
        writeScn(tr(scnFile.c_str()));
        emit reload();
    }
}

//--------------- runSofa ---------------------------------
void SofaWidget::runSofa() {
    //save file into temporary file
    QString temp = tr(scnFileTemp.c_str()); //TODO use a temp directory?

    writeScn(temp);

    //run Sofa
    QProcess* p = new QProcess(this);
    string folder = scnFileTemp;
    size_t slashPlace = scnFileTemp.find_last_of("/");
    if (slashPlace) {
        folder = scnFileTemp.substr(0, slashPlace + 1);
    }
    else {
        folder = ".";
    }
    p->setWorkingDirectory(tr(SOFA_BUILD_DIR) + tr("/bin/"));
    connect(p, SIGNAL(finished(int)), this, SLOT(sofaExited()));
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("LD_LIBRARY_PATH", env.value("LD_LIBRARY_PATH") + ":" + SOFA_BUILD_DIR + "/lib/linux/"); //TODO if win32...
    p->setProcessEnvironment(env);

    p->start(tr(SOFA_BUILD_DIR) + tr("/bin/runSofa"), QStringList() << temp);

}

//--------------- WriteScn ---------------------------------
void SofaWidget::writeScn(QString path) {
    QFile file(path) ;
    if (file.open(QFile::WriteOnly)) {
        QTextStream out(&file) ;
        QString text = ui.text->toPlainText();
        out << text ;
    }

}

//--------------- sofaExited ---------------------------------
void SofaWidget::sofaExited() {
    remove(scnFileTemp.c_str());
}




