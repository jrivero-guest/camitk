/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorPointSetDistance.h"

// Tools includes
#include "Tools.h"
#include "AtomIterator.h"

// -------------------- constructor --------------------
MonitorPointSetDistance::MonitorPointSetDistance(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, SCALARSET) {}

// -------------------- destructor --------------------
void MonitorPointSetDistance::calculate() {
    values.clear();
    double posSimul[3];
    double posRef[3];

    //TODO indexes are in index vector: do not iterate and use indexes vector directly?
    AtomIterator it = AtomIterator(monitoringManager->getPml(), target);
    for (it.begin(); !it.end(); it.next()) {
        it.currentAtom()->getPosition(posSimul);
        posSimul[0] = posSimul[0] + dx;
        posSimul[1] = posSimul[1] + dy;
        posSimul[2] = posSimul[2] + dz;
        if (references[0]->getNearest(posSimul, monitoringManager->getCurrentTime(), posRef)) {
            values.push_back(distance(posRef, posSimul));
        }
        else {
            std::cerr << "not atoms found to calculate the nearest" << std::endl;
        }
    }

    write();
}

// -------------------- destructor --------------------
std::string MonitorPointSetDistance::getTypeName() {
    return "Point set distance";
}
