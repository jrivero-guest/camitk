/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorSurface.h"

// -------------------- constructor --------------------
MonitorSurface::MonitorSurface(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, SCALAR) {}

// -------------------- calculate --------------------
void MonitorSurface::calculate() {
    values.clear();
    double surface = 0;
    StructuralComponent* scSimul = dynamic_cast<StructuralComponent*>(monitoringManager->getPml()->getComponentByName(target));
    if (scSimul) {
        // loop on all structure
        for (unsigned int i = 0; i < scSimul->getNumberOfStructures(); i++) {
            Cell* cSimul = dynamic_cast<Cell*>(scSimul->getStructure(i));
            if (cSimul) {
                surface += cSimul->surface();
            }
        }
    }
    values.push_back(surface);
    write();
}

// -------------------- getType --------------------
std::string MonitorSurface::getTypeName() {
    return "Surface";
}

