/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the COMPILE_MML_TOOL
// flag defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// MML_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#if defined(_WIN32) // MSVC and mingw
#ifdef COMPILE_MML_TOOL
#define MML_API __declspec(dllexport)
//#define MML_STL_TEMPLATE
#else
#define MML_API __declspec(dllimport)
//#define MML_STL_TEMPLATE export
#endif
#else
// for all other platforms MML_API is defined to be "nothing"
#ifndef MML_API
#define MML_API
#endif
#endif // MSVC and mingw

// tentative to understand http://support.microsoft.com/kb/168958/en-us
// failed...
/*#if defined(_WIN32) // MSVC and mingw

    //disable warnings on 255 char debug symbols
    #pragma warning (disable : 4786)
    //disable warnings on extern before template instantiation
    #pragma warning (disable : 4231)

    // Instanciate classes for stl templates
    // This does not create an object. It only forces the generation of all
    // of the members of std::auto_ptr<mml::MonitoringIn>, std::vector<Monitor*>
    // and std::vector<mml::TimeStep*>. It exports
    // them from the DLL and imports them into the .exe file.
    MML_STL_TEMPLATE template class MML_API std::auto_ptr<mml::MonitoringIn>;
    MML_STL_TEMPLATE template class MML_API std::vector<Monitor*>;
    MML_STL_TEMPLATE template class MML_API std::vector<mml::TimeStep*>;
#endif
    */
//... disabling the warning on windows msvc
#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
#pragma warning( disable : 4251 )
#endif // MSVC only
