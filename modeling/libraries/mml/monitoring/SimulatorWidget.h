/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATORS_SIMULATORWIDGET_H
#define SIMULATOR_SIMULATORS_SIMULATORWIDGET_H

#include <QWidget>

#include "MMLAPI.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A widget specific of the simulator to add in the gui
 * all simulator widget have to derive from this class
 */
class MML_API SimulatorWidget: public QWidget {
    Q_OBJECT

public:
    /// constructor
    SimulatorWidget(QWidget* parent = nullptr);
    /// destructor
    ~SimulatorWidget() override = default;

signals:
    void reload();

};


#endif // SIMULATOR_SIMULATORS_SIMULATORWIDGET_H
