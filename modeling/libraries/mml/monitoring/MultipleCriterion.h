/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERION_H
#define STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERION_H

#include <vector>

#include <MonitorIn.hxx>
// Stopping criteria includes
#include "StoppingCriterion.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * class which represents a multiple criterion
 * a multiple criterion is either an OrMUltipleCriterion or an AndMultipleCriterion
 * A MultipleCriterion can be checked using checkCriterion method to know if the MultipleCriterion is reach
 * it contains a set of criterion (criteria vector) wich are all checked to make the boolena assessment of checkCriterion method
 */
class MultipleCriterion: public StoppingCriterion {
public:
    /**
     * constructor
     * @param m the xsdcxx generated MultipleCriterion
     */
    MultipleCriterion(mml::MultipleCriteria* m, MonitoringManager* monitoringManager, MultipleCriterion* parent = nullptr);
    /// destructor
    ~MultipleCriterion() override;

    /// return true if the MultipleCriterion is reach
    bool checkCriterion() = 0;

    /// get number of childre; return -1 for non multiple criteria
    int getNumberOfChildren() override;

    /// get the child repered by index, return null if no child
    StoppingCriterion* getChild(const unsigned int i) override;

    /// get stopping criterion name
    std::string getName() = 0;

    /// add child
    void addChild(StoppingCriterion* sc);

    /// remove child by index
    void removeChild(const unsigned int i);

protected:
    /// the set of criteria to check
    std::vector<StoppingCriterion*> criterias;
    /// the xsdcxx object representing MultipleCriterion, used for serialization
    mml::MultipleCriteria* mappedObject;
};

#endif // STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERION_H
