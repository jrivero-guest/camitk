/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATORS_ANSYS_ANSYSSIMULATOR_H
#define SIMULATOR_SIMULATORS_ANSYS_ANSYSSIMULATOR_H

#include <string>

#include "NonInteractiveSimulator.h"
#include "AnsysBatch.h"

/**
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * Simulator class to use Ansys as simulation engine
 */
class AnsysSimulator : public NonInteractiveSimulator {

public:
    /// constructor
    AnsysSimulator(MonitoringManager* monitoringManager);
    /// constructor
    AnsysSimulator(MonitoringManager* monitoringManager, const char* file);

    /// destructor
    ~AnsysSimulator();

    void init();
    void end();
    void getPosition(int index, double position[3]);
    void getForce(int index, double force[3]);

    bool doCalc();
    int getMaxStep();
    double getTime(int step);

    /// Create a pml file from an imput file
    virtual void createPml(const char* inputFile, const char* pmlFile);
    /// get path to batch file
    std::string getBatchFile();

private:
    /// Ansys binary
    std::string ansysPath;
    /// Ansys working dir
    std::string workingDir;
    /// Ansys batch file creator
    AnsysBatch* batch;

    /// start ansys with created batch file
    void runAnsys();

};

#endif // SIMULATOR_SIMULATORS_ANSYS_ANSYSSIMULATOR_H 
