/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MANAGER_INTERACTIVEMANAGER_H
#define MANAGER_INTERACTIVEMANAGER_H

#include "MMLAPI.h"

#include <memory>   // std::auto_ptr
#include <vector>

// Monitor includes
#include "MonitoringManager.h"
#include <MonitorIn.hxx>
#include <MonitorOut.hxx>

// Criterion includes
#include "StoppingCriterion.h"

// Simulator includes
#include "InteractiveSimulator.h"
/**
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * Interactice managers are managers linked with an interactive simulator
 *
 **/
class MML_API InteractiveMonitoringManager: public MonitoringManager {
public:
    /**
     * constructor
     *@param mml mml file name
     *@param sim simulator name (Sofa, Ansys...)
     */
    InteractiveMonitoringManager(const char* mml);

    /// destructor
    ~InteractiveMonitoringManager() override;

    /// Initialize manager
    bool init() override;
    /// End manager
    void end() override;
    /// make a simulation move (one step)
    void doMove() override;
    /// check if the stopping criterion is reached
    bool checkStop() override;

    /// get Stopping Criterion
    StoppingCriterion* getStoppingCriterion();

private:
    /// the stopping criteria to check in order to know when simulation loop should be stopped
    StoppingCriterion* stop;
    /// the simulator used for simualtion
    InteractiveSimulator* simul;



};

#endif // MANAGER_INTERACTIVEMANAGER_H
