/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorPosition.h"
// Tools includes
#include "AtomIterator.h"

// -------------------- constructor --------------------
MonitorPosition::MonitorPosition(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, VECTORSET) {}

// -------------------- calculate --------------------
void MonitorPosition::calculate() {
    values.clear();
    double pos[3];
    //TODO indexes are in index vector: do not iterate and use indexes vector directly?
    AtomIterator it = AtomIterator(monitoringManager->getPml(), target);
    for (it.begin(); !it.end(); it.next()) {
        it.currentAtom()->getPosition(pos);
        pos[0] = pos[0] + dx;
        pos[1] = pos[1] + dy;
        pos[2] = pos[2] + dz;
        values.push_back(pos[0]);
        values.push_back(pos[1]);
        values.push_back(pos[2]);
    }

    write();
}

// -------------------- getType --------------------
std::string MonitorPosition::getTypeName() {
    return "Position";
}
