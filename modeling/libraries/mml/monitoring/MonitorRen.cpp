/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorRen.h"

// Tools includes
#include "Tools.h"
#include "AtomIterator.h"

// -------------------- constructor --------------------
MonitorRen::MonitorRen(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, SCALARSET) {}

// -------------------- calculate --------------------
void MonitorRen::calculate() {
    values.clear();
    double posSimul[3];
    double posInit[3];
    double posRef[3];
    double realTime;

    //TODO indexes are in index vector: do not iterate and use indexes vector directly?
    AtomIterator it = AtomIterator(monitoringManager->getPml(), target);
    for (it.begin(); !it.end(); it.next()) {
        it.currentAtom()->getPosition(posSimul);
        posSimul[0] = posSimul[0] + dx;
        posSimul[1] = posSimul[1] + dy;
        posSimul[2] = posSimul[2] + dz;
        monitoringManager->getInitPml()->getAtom(it.currentAtom()->getIndex())->getPosition(posInit);
        if (references[0]->getMonitoredData("Position", monitoringManager->getCurrentTime(), it.currentAtom()->getIndex(), realTime, posRef)) {
            if (distance(posRef, posSimul) == 0 || distance(posRef, posInit) == 0) {
                values.push_back(0);
            }
            else {
                values.push_back(distance(posRef, posSimul) / distance(posRef, posInit));
            }
        }
        else {
            std::cerr << "getPosition non implemented" << std::endl;
        }
    }

    write();
}

// -------------------- getType --------------------
std::string MonitorRen::getTypeName() {
    return "REN";
}
