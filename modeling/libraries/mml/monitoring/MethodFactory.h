/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef STOPPINGCRITERION_STOPPINGCRITERIA_METHODFACTORY_H
#define STOPPINGCRITERION_STOPPINGCRITERIA_METHODFACTORY_H

#include <MonitorIn.hxx>

// Stopping criteria includes
#include "Method.h"
#include "Threshold.h"
#include "MinThreshold.h"
#include "TimePeriodThreshold.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 *  A factory to create method
 */
class MethodFactory {
public:
    /// destructor
    ~MethodFactory();

    /// create a method according to the xsdcxx object method
    static Method* createMethod(mml::Method& m);

private:
    /// constructor
    MethodFactory();

};

#endif // STOPPINGCRITERION_STOPPINGCRITERIA_METHODFACTORY_H
