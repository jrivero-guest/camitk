/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERIA_ORMULTIPLECRITERIA_H
#define STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERIA_ORMULTIPLECRITERIA_H

#include <vector>

#include <MonitorIn.hxx>
// Stopping criteria includes
#include "MultipleCriterion.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A OrMultipleCriterion is a MultipleCriterion where a call of checkCriteria methof is true
 * if at least one of Criterion of the Criteria vector is reached
 */
class OrMultipleCriterion: public MultipleCriterion {
public:
    /**
     * constructor
     * @param m the xsdcxx generated MultipleCriterion
     */
    OrMultipleCriterion(mml::MultipleCriteria* m, MonitoringManager* monitoringManager, MultipleCriterion* parent = nullptr);
    /// destructor
    ~OrMultipleCriterion() override = default;

    /// return true if at least one of the Criterion is reached
    bool checkCriterion() override;

    /// get stopping criterion name
    std::string getName() override;
};

#endif // STOPPINGCRITERION_STOPPINGCRITERIA_MULTIPLECRITERIA_ORMULTIPLECRITERIA_H
