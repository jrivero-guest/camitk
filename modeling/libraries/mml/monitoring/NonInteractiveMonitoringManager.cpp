/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <iostream>
#include <string>

// Monitor includes
#include "NonInteractiveMonitoringManager.h"

// Tools includes
#include "Chrono.h"

// -------------------- constructor --------------------
NonInteractiveMonitoringManager::NonInteractiveMonitoringManager(const char* mml): MonitoringManager(mml) {
}

// -------------------- destructor --------------------

NonInteractiveMonitoringManager::~NonInteractiveMonitoringManager() {
    if (simul) {
        delete simul;
    }
}


// -------------------- simulate --------------------
/*
void NonInteractiveMonitoringManager::simulate(){
  init();
  saveMonitors();
  while(!checkStop()){
  doMove();
  }
  writeOutput("Output.mml");
}*/

// -------------------- init --------------------
bool NonInteractiveMonitoringManager::init() {
    simul = (NonInteractiveSimulator*)(MonitoringManager::simul);
    simul->init();
    if (doCalc()) {
        simul->updatePositions();
        maxStep = simul->getMaxStep();
        return true;
    }
    else {
        return false;
    }

}

// -------------------- end --------------------
void NonInteractiveMonitoringManager::end() {
    simul->end();
}

// -------------------- doCalcl --------------------
bool NonInteractiveMonitoringManager::doCalc() {
    return (simul->doCalc());
}

// -------------------- doMove --------------------
void NonInteractiveMonitoringManager::doMove() {
    currentTime = (simul->getTime(step));
    simul->updatePositions();
    saveMonitors();
    incStep();
}

bool NonInteractiveMonitoringManager::checkStop() {
    return (step > maxStep);
}

