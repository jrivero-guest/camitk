/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "AnsysBatch.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

// -------------------- constructor --------------------
AnsysBatch::AnsysBatch(string wd, MonitoringManager* monitoringManager): workingDir(wd) {
    fileName = "MMLBatchAnsys.mac";
    this->monitoringManager = monitoringManager;
    prepw = new PrepWriter(workingDir, monitoringManager);
    soluw = new SoluWriter(monitoringManager);
    postw = new PostWriter(monitoringManager);
    paramw = new ParametersWriter(monitoringManager);
}

// -------------------- destructor --------------------

AnsysBatch::~AnsysBatch() {
    if (prepw) {
        delete prepw;
    }
    if (soluw) {
        delete soluw;
    }
    if (postw) {
        delete postw;
    }
    if (paramw) {
        delete paramw;
    }
}


// -------------------- write --------------------
void AnsysBatch::write() {
    ofstream fileComplete, fileParams, fileExport;
    string fileCompleteName = workingDir + fileName;
    string fileNameTronk = fileName;
    string::size_type pLast = fileName.rfind(".");
    if (pLast != string::npos) {
        fileNameTronk.erase(pLast);
    }
    string paramsName = fileNameTronk + "-params";
    string exportName = fileNameTronk + "-export";
    string fileParamsName = workingDir + paramsName + ".mac";
    string fileExportName = workingDir + exportName + ".mac";

    fileParams.open(fileParamsName.c_str(), ios::out);
    fileExport.open(fileExportName.c_str(), ios::out);
    if (fileParams.bad() || fileExport.bad()) {
        cerr << " Ansys batch file error" << endl;
    }

    //parameters and export macro have to be changed
    fileParams << paramw->write();
    fileExport << postw->write();
    fileParams.close();
    fileExport.close();

    // if file exists do not rewrite
    ifstream istr(fileCompleteName.c_str());
    if (!istr) {
        fileComplete.open(fileCompleteName.c_str(), ios::out);
        if (fileComplete.bad()) {
            cerr << " Ansys batch file error" << endl;
        }
        fileComplete << "/batch" << endl;
        fileComplete << "FINISH" << endl;
        fileComplete << "/CLEAR" << endl;
        fileComplete << "/UNIT,SI" << endl;
        fileComplete << "/input," << paramsName << ",mac" << endl;
        fileComplete << prepw->write();
        fileComplete << soluw->write();
        fileComplete << "/input," << exportName << ",mac" << endl;
        fileComplete.close();
    }
}

// -------------------- getFileName --------------------
string AnsysBatch::getFileName() {
    return fileName;
}
