/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "AnsysWidget.h"

#include <QFile>
#include <QProcess>
#include <QTextStream>
#include <QMessageBox>

//--------------- constructor ---------------------------------
AnsysWidget::AnsysWidget(QWidget* parent, AnsysSimulator* ansysSimulator): SimulatorWidget(parent) {
    this->ansysSimulator = ansysSimulator;
    this->batchFile = ansysSimulator->getBatchFile();
    ui.setupUi(this);

    connect(ui.applyChanges, SIGNAL(clicked()), this, SLOT(applyChanges()));

    QFile file(tr(batchFile.c_str()));
    if (file.open(QIODevice::ReadOnly)) {
        ui.text->document()->setPlainText(file.readAll());
    }

}

//--------------- SofaWidget ---------------------------------
AnsysWidget::~AnsysWidget() {}

//--------------- applyChanges ---------------------------------
void AnsysWidget::applyChanges() {
    // CCC Exception: Use a QMessageBox::warning instead of CAMITK_WARNING as the user interaction is required
    int r = QMessageBox::warning(this, tr("Apply scene changes"), tr("This will save current changes batch file and remake calculation.\n Not saved data will be lost."), QMessageBox::Ok, QMessageBox::Cancel);
    if (r == QMessageBox::Ok) {
        writeBatch(tr(batchFile.c_str()));
        emit reload();
    }
}

//--------------- WriteBatch ---------------------------------
void AnsysWidget::writeBatch(QString path) {
    QFile file(path) ;
    if (file.open(QFile::WriteOnly)) {
        QTextStream out(&file) ;
        QString text = ui.text->toPlainText();
        out << text ;
    }

}




