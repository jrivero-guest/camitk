/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Stopping criteria includes
#include "Force.h"

#include <iostream>

// -------------------- constructor --------------------
ForceCriterion::ForceCriterion(mml::Force* c, MonitoringManager* monitoringManager, MultipleCriterion* parent): Criterion(c, monitoringManager, parent) {
    switch (c->unit()) {
        case mml::ForceUnit::mN:
            factor = 1000;
            unit = "mN";
            break;
        case mml::ForceUnit::N:
            factor = 1;
            unit = "N";
            break;
        default:
            std::cerr << "Force unit error" << std::endl;
    }
}

// -------------------- destructor --------------------
ForceCriterion::~ForceCriterion() {}

// -------------------- calculate --------------------
void ForceCriterion::calculate() {}

// -------------------- getName --------------------
std::string ForceCriterion::getName() {
    return "Force";
}
