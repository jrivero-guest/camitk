/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Stopping criteria includes
#include "Simulator.h"

// -------------------- construcor --------------------
Simulator::Simulator(MonitoringManager* monitoringManager) {
    this->monitoringManager = monitoringManager;
#ifdef MML_GENERATE_GUI
    this->widget = nullptr;
#endif
}

Simulator::Simulator(MonitoringManager* monitoringManager, const char* file) {
    this->monitoringManager = monitoringManager;
#ifdef MML_GENERATE_GUI
    this->widget = nullptr;
#endif
}

// -------------------- updatePositions --------------------
void Simulator::updatePositions() {
    monitoringManager->storeOldPositions();
    for (unsigned int i = 0; i < monitoringManager->getPml()->getAtoms()->getNumberOfStructures(); i++) {
        double pos[3];
        int index = monitoringManager->getPml()->getAtoms()->getStructure(i)->getIndex();
        getPosition(index, pos);
        monitoringManager->getPml()->getAtom(index)->setPosition(pos);
    }
}

#ifdef MML_GENERATE_GUI
// -------------------- getWidget --------------------
QWidget* Simulator::getWidget() {
    return widget;
}
#endif
