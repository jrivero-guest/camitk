/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorFactory.h"

#include <iostream>

// -------------------- constructor --------------------
MonitorFactory::MonitorFactory() {}

// -------------------- destructor --------------------
MonitorFactory::~MonitorFactory() {}

// -------------------- createMonitor --------------------
Monitor* MonitorFactory::createMonitor(mml::Monitor* m, MonitoringManager* monitoringManager) {
    switch (m->type()) {
        case mml::MonitorType::GeomDeviation:
            return (new MonitorGeometricDeviation(m, monitoringManager));
            break;
        case mml::MonitorType::Displacement:
            return (new MonitorDisplacement(m, monitoringManager));
            break;
        case mml::MonitorType::NormDisplacement:
            return (new MonitorNormDisplacement(m, monitoringManager));
            break;
        case mml::MonitorType::Position:
            return (new MonitorPosition(m, monitoringManager));
            break;
        case mml::MonitorType::REN:
            return (new MonitorRen(m, monitoringManager));
            break;
        case mml::MonitorType::Force:
            return (new MonitorForce(m, monitoringManager));
            break;
        case mml::MonitorType::PointSetDistance:
            return (new MonitorPointSetDistance(m, monitoringManager));
            break;
        case mml::MonitorType::PointFinalSetDistance:
            return (new MonitorPointFinalSetDistance(m, monitoringManager));
            break;
        case mml::MonitorType::Volume:
            return (new MonitorVolume(m, monitoringManager));
            break;
        case mml::MonitorType::Surface:
            return (new MonitorSurface(m, monitoringManager));
            break;
        case mml::MonitorType::DistanceToTriangularMeshFinal:
            return (new MonitorPointToTriangleMeshDistanceFinal(m, monitoringManager));
            break;
        case mml::MonitorType::ComputingTime:
            return (new MonitorComputingTime(m, monitoringManager));
            break;
        case mml::MonitorType::DistanceX:
            return (new MonitorDistanceX(m, monitoringManager));
            break;
        case mml::MonitorType::DistanceY:
            return (new MonitorDistanceY(m, monitoringManager));
            break;
        case mml::MonitorType::DistanceZ:
            return (new MonitorDistanceZ(m, monitoringManager));
            break;
        default:
            std::cout << "Monitor type error" << std::endl;
    }
    return NULL;
}



