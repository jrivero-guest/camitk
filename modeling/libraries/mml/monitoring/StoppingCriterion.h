/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef STOPPINGCRITERION_STOPPINGCRITERION_H
#define STOPPINGCRITERION_STOPPINGCRITERION_H

#include "MMLAPI.h"

#include <vector>

#include <MonitorIn.hxx>

#include "MonitoringManager.h"

class MultipleCriterion;
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * class which represent the stopping criterion to check to stop simulation
 * a StoppingCriterion is either a Criterion or a MultipleCriterion
 * a StoppingCriterion can be checked using checkCriterion method to know if the StoppingCriterion is reach
 */
class MML_API StoppingCriterion {
public:

    /// constructor
    StoppingCriterion(MonitoringManager* monitoringManager, MultipleCriterion* parent = nullptr);
    /// destructor
    virtual ~StoppingCriterion() = default;

    /// return true if the stopping criterion is reach
    virtual bool checkCriterion() = 0;

    /// get number of childre; return -1 for non multiple criteria
    virtual int getNumberOfChildren() = 0;

    /// get the child repered by index, return null if no child
    virtual StoppingCriterion* getChild(const unsigned int i) = 0;

    /// get parent multiple criteria
    MultipleCriterion* getParent();

    /// get stopping criterion name
    virtual std::string getName() = 0;

protected:

    /// monitoring manager
    MonitoringManager* monitoringManager;
    /// parent stopping Criterion
    MultipleCriterion* parent;

};

#endif // STOPPINGCRITERION_STOPPINGCRITERION_H
