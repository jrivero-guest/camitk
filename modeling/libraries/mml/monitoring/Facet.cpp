/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#include "Facet.h"

#include <MultiComponent.h>
#include <StructuralComponent.h>
#include <Atom.h>

// -------------------- constructor ------------------------
Facet::Facet(unsigned int size, unsigned int id[]) {
    this->size = size;
    this->id = new unsigned int[size];
    for (unsigned int i = 0; i < size; i++) {
        this->id[i] = id[i];
    }
    used = 1;
}

// -------------------- destructor ------------------------
Facet::~Facet() {
    delete [] id;
}

// -------------------- debug ------------------------
void Facet::debug() {
    switch (size) {
        case 3:
            std::cout << "triangle <";
            break;
        case 4:
            std::cout << "quad <";
            break;
        default:
            std::cout << "unknown facet <";
            break;
    }
    unsigned int i;
    for (i = 0; i < size - 1; i++) {
        std::cout << id[i] << ",";
    }
    std::cout << id[i] << "> used " << used << " times" << std::endl;
}


// -------------------- testEquivalence ------------------------
bool Facet::testEquivalence(unsigned int size, unsigned int id[]) {
    if (this->size != size) {
        return false;
    }
    else {
        unsigned int i = 0;
        while (i < size && isIn(id[i])) {
            i++;
        }

        if (i == size) {
            used++;
        }

        return (i == size);
    }
}

// -------------------- isIn ------------------------
bool Facet::isIn(unsigned int index) const {
    unsigned int i = 0;
    while (i < size && id[i] != index) {
        i++;
    }
    return (i != size);
}

// -------------------- getCell ------------------------
Cell* Facet::getCell(PhysicalModel* pm) const {
    Cell* c;
    // create the correct geometric type cell
    switch (size) {
        case 3:
            c = new Cell(NULL, StructureProperties::TRIANGLE);
            break;
        case 4:
            c = new Cell(NULL, StructureProperties::QUAD);
            break;
        default:
            c = NULL;
    }
    // get the atom corresponding to the index stored in id
    // and insert them in the cell
    for (unsigned int i = 0; i < size; i++) {
        Atom* a = pm->getAtom(id[i]);
        if (a == NULL) {
            std::cout << "Argh! Cannot find atom #" << id[i] << std::endl;
        }
        else {
            c->addStructureIfNotIn(a);
        }
    }
    return c;
}

// -------------------- getUsed ------------------------
unsigned int Facet::getUsed() const {
    return used;
}
