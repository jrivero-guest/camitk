/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MONITOR_MONITORS_MONITORPOINTFINALSETDISTANCE_H
#define MONITOR_MONITORS_MONITORPOINTFINALSETDISTANCE_H

#include  <string>

// Monitor includes
#include <MonitorIn.hxx>
#include "Monitor.h"

/**
* A monitor that calculate the distance between a point and a set of points.
*
* This is a fast method that gives the point set distance relatively to the positions in the PML Reference.
*/
class MonitorPointFinalSetDistance: public Monitor {

public:
    /**
     * constructor
     * @param m the xsdcxx generated monitor
     */
    MonitorPointFinalSetDistance(mml::Monitor* m, MonitoringManager* monitoringManager);
    /// destructor
    ~MonitorPointFinalSetDistance() override = default;

    /// calculate current followed data and store them in values vector
    void calculate() override;

    /// return a string relative to monitor type
    std::string getTypeName() override;
};

#endif // MONITOR_MONITORS_MONITORPOINTFINALSETDISTANCE_H
