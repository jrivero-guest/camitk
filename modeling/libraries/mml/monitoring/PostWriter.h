/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITERS_POSTPWRITER_H
#define SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITERS_POSTPWRITER_H

#include <string>

#include "AnsysBatchWriter.h"

/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * TODO Comment class here.
 */
class PostWriter: public AnsysBatchWriter {

public:
    /// constructor
    PostWriter(MonitoringManager* monitoringManager);
    /// destructor
    ~PostWriter();

    /// write the POST part of batch file into a string
    std::string write();

private:
    /// set to true of positions have to be stored
    bool positions;//TODO ajouter les autres
    ///write a part of batch file (into a string) to make Ansys write the number of steps into a file
    std::string writeMaxStep();
    ///write a part of batch file (into a string) to make Ansys write the times associated to simulation steps into a file
    std::string writeTimes();
    ///write a part of batch file (into a string) to make Ansys write the positions of all atoms into a file (one file per simulation step)
    std::string writePositions();
};

#endif // SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITERS_POSTWRITER_H