/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "simulator/simulators/Ansys/AnsysSimulator.h"

#include "simulator/SimulatorFactory.h"

#include <iostream>
#include <fstream>
#include <sstream>

#ifdef MML_GENERATE_GUI
#include "AnsysWidget.h"
#endif

using namespace std;

MML_DECL_CLASS(AnsysSimulator)

bool ansysRegistered = SimulatorFactory::getInstance()->registerClass<AnsysSimulator>("ansys", false);

// -------------------- constructor --------------------
AnsysSimulator::AnsysSimulator(MonitoringManager* monitoringManager): NonInteractiveSimulator(monitoringManager) {
    ansysPath = ANSYS_BIN;
    workingDir = ANSYS_WORKING_DIR;
    if (workingDir[workingDir.length() - 1] != '/') {
        workingDir = workingDir + "/";
    }
    batch = new AnsysBatch(workingDir, monitoringManager);
    batch->write();

#ifdef MML_GENERATE_GUI
    widget = new AnsysWidget(NULL, this);
#endif

}

// -------------------- constructor --------------------
AnsysSimulator::AnsysSimulator(MonitoringManager* monitoringManager, const char* file): NonInteractiveSimulator(monitoringManager, file) {
    //TODO
    std::cerr << "not possible yet for ansys" << std::endl;
}


// -------------------- destructor --------------------
AnsysSimulator::~AnsysSimulator() {
    if (batch) {
        delete batch;
    }
}

// -------------------- init --------------------
void AnsysSimulator::init() {
    //create working directory if [ -d "$dir" ]; then  fi
    string str = "mkdir -p " + workingDir;
    system(str.c_str());
    batch->write(); //TODO this sould not be in both constructor and here but need to be done in constructor for batch edit in gui and here for custom parameters update from pml (perhaps need to cu in 3 part: mainBatch, paramBatch and exportBatch)
}

// -------------------- end --------------------
void AnsysSimulator::end() {
    // delete temporary files
    //string str="rm -rf " + workingDir + "*";
    //system(str.c_str());
    string str = "rm -f " + workingDir;
    string str1 = str + "*.tmp";
    string str2 = str + "job*";
    string str3 = str + "*.pos";
    //system("rm -f ansys.out");
    //system("rm -f MMLBatchAnsys.*");
    system(str1.c_str());
    system(str2.c_str());
    system(str3.c_str());
}

void AnsysSimulator::createPml(const char* inputFile, const char* pmlFile) {
    //TODO implement
    std::cerr << "not possible yet for ansys" << std::endl;
}


// -------------------- doCalc --------------------
bool AnsysSimulator::doCalc() {
    runAnsys();
    std::string str = workingDir + "times.tmp";
    ifstream istr(str.c_str());
    return istr;
}

// -------------------- runAnsys
void AnsysSimulator::runAnsys() { //TODO workingdir pointe sur un dossier temporaire et end() le supprime
    string s = ansysPath + " -p AA_T_ME -dir """ + workingDir + """ -j ""job"" -s read -l en-us -t < " + workingDir + batch->getFileName() + " > ansys.out";
    system(s.c_str());

}
// -------------------- getPosition --------------------
void AnsysSimulator::getPosition(int index, double position[3]) {
    int step = monitoringManager->getCurrentStep();

    stringstream ss;
    string str;
    ss << step;
    ss >> str;
    str = workingDir + str + ".pos";
    ifstream fichier;
    fichier.open(str.c_str(), ios::in);
    int i;
    double x;
    double y;
    double z;
    while (!fichier.eof()) {
        fichier >> i;
        fichier >> x;
        fichier >> y;
        fichier >> z;
        // NOTE Ansys node index starts at 1 not 0 (fortran heritage!)
        if (i - 1 == index) {
            position[0] = x;
            position[1] = y;
            position[2] = z;
            break;
        }
    }
    fichier.close();
}

// -------------------- getPosition --------------------
void AnsysSimulator::getForce(int index, double force[3]) {
    //TODO (should return booleen when non implemented?)
}


// -------------------- getMaxStep --------------------
int AnsysSimulator::getMaxStep() {
    ifstream fichier;
    string str = workingDir + "max.tmp";
    fichier.open(str.c_str(), ios::in);
    int i;
    fichier >> i;
    return i;
    fichier.close();
}

// -------------------- getTime --------------------
double AnsysSimulator::getTime(int step) {
    ifstream fichier;
    string str = workingDir + "times.tmp";
    fichier.open(str.c_str(), ios::in);
    double i;
    for (int j = 1; j <= step; j++) {
        fichier >> i;
    }
    return i;
    fichier.close();
}

// -------------------- getBatchFile --------------------
std::string AnsysSimulator::getBatchFile() {
    return workingDir + batch->getFileName();
}
