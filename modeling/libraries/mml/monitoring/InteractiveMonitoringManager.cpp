/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <iostream>
#include <string>

// Monitor includes
#include "InteractiveMonitoringManager.h"

// Tools includes
#include "Chrono.h"

// Criterion includes
#include "StoppingCriterionFactory.h"


// -------------------- constructor --------------------
InteractiveMonitoringManager::InteractiveMonitoringManager(const char* mml): MonitoringManager(mml) {
    if (mmlIn->stoppingCriteria().present()) {
        mml::StoppingCriteria* s = &(mmlIn->stoppingCriteria().get());
        stop = StoppingCriterionFactory::createStoppingCriterion(s, this);
    }
    else {
        stop = nullptr;
    }
}

// -------------------- destructor --------------------

InteractiveMonitoringManager::~InteractiveMonitoringManager() {
    end();
    if (stop) {
        delete stop;
    }
    if (simul) {
        delete simul;
    }
}


// -------------------- init --------------------
bool InteractiveMonitoringManager::init() {
    simul = (InteractiveSimulator*)(MonitoringManager::simul);
    simul->init();
    simul->updatePositions();
    return true;
}

// -------------------- doMove --------------------
void InteractiveMonitoringManager::doMove() {
    saveMonitors();
    Chrono c;
    c.start();
    simul->doMove(dt);
    stepComputingTime = c.stop();
    updateComputingTime();
    updateCurrentTime();
    simul->updatePositions();
    incStep();
}

// -------------------- end --------------------
void InteractiveMonitoringManager::end() {
    simul->end();
}

// -------------------- checkStop --------------------
bool InteractiveMonitoringManager::checkStop() {
    if (stop) {
        return stop->checkCriterion();
    }
    else {
        return false;
    }
}

// -------------------- getStoppingCriterion --------------------
StoppingCriterion* InteractiveMonitoringManager::getStoppingCriterion() {
    return stop;
}

