/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "PostWriter.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

// -------------------- constructor --------------------
PostWriter::PostWriter(MonitoringManager* monitoringManager): AnsysBatchWriter(monitoringManager) {
    //TODO positions en fonctions des params
    positions = true;
}

// -------------------- destructor --------------------
PostWriter::~PostWriter() {}


// -------------------- write --------------------
std::string PostWriter::write() {
    std::ostringstream os;

    os << "/post1" << std::endl;
    os << "allsel" << std::endl;
    os << writeMaxStep();
    os << writeTimes();
    if (positions) {
        os << writePositions();
    }

    return os.str();
}

// -------------------- writeMaxStep --------------------
std::string PostWriter::writeMaxStep() {
    std::ostringstream os;

    // number of sets = nbrset
    os << "*get,nbrset,active,0,set,nset" << std::endl;
    // write max step in max.tmp
    os << "/OUT,max,tmp,,append" << std::endl;
    os << "*vwrite,nbrset, , , , , , , , ," << std::endl;
    os << "(F6.0,TL1)" << std::endl;
    os << "/OUT" << std::endl;

    return os.str();
}

// -------------------- writeTimes --------------------
std::string PostWriter::writeTimes() {
    std::ostringstream os;

    // number of sets = nbrset
    os << "*get,nbrset,active,0,set,nset" << std::endl;
    //write times in times.tmp
    os << "*do,l,1,nbrset" << std::endl;
    os << "set,,, ,,, ,l" << std::endl;
    os << "*get,t,active,0,set,time" << std::endl;
    os << "/OUT,times,tmp,,append" << std::endl;
    os << "*vwrite,t, , , , , , , , ," << std::endl;
    os << "(G15.7)" << std::endl;
    os << "/OUT" << std::endl;
    os << "*enddo" << std::endl;

    return os.str();
}

// -------------------- writePositions --------------------
std::string PostWriter::writePositions() {
    std::ostringstream os;

    //write positions
    os << "nsel,all" << std::endl;
    // number of selected nodes=nbto2
    os << "*get,nbto2,node,0,count" << std::endl;
    // number of first selected node
    os << "*get,nmin2,node,0,num,min" << std::endl;
    // loop on sets
    os << "*do,k,1,nbrset" << std::endl;
    os << "set,,, ,,, ,k" << std::endl;
    // print node file
    //os << "nwrite,%k%,node,0" << std::endl;
    // loop on nodes
    os << "j=nmin2" << std::endl;
    os << "*do,i,1,nbto2" << std::endl;
    // write in i.pos file
    os << "/OUT,%k%,pos,,append" << std::endl;
    os << "*vwrite,j,(nx(j)+ux(j)),(ny(j)+uy(j)),(nz(j)+uz(j))" << std::endl; //TODO what to do  if 2d simulation (no z)
    os << "(F6.0,TL1,G15.7,G15.7,G15.7)" << std::endl;
    os << "/OUT" << std::endl;
    os << "j=ndnext(j)" << std::endl;
    os << "*enddo" << std::endl;
    os << "*enddo" << std::endl;


    return os.str();
}
