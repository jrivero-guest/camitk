/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitoringManagerFactory.h"

// Simulator includes
#include "SimulatorFactory.h"

// -------------------- createContext --------------------
MonitoringManager* MonitoringManagerFactory::createManager(const char* mml) {
    std::unique_ptr<mml::MonitoringIn> mmlIn;
    // TODO use the CamiTK Root Dir to determine where is the schema
    // in the meantime: do not validate!
    //xml_schema::properties props;
    //props.no_namespace_schema_location(MONITORIN_XSD);
    //mmlIn = mml::monitoringIn(mml,0,props);
    mmlIn = mml::monitoringIn(mml, xml_schema::flags::dont_validate);
    std::string sim = mmlIn->simulator();
    SimulatorFactory* sf = SimulatorFactory::getInstance();
    if (sf->isRegistered(sim)) {
        if (sf->isInteractive(sim)) {
            return new InteractiveMonitoringManager(mml);
        }
        else {
            return new NonInteractiveMonitoringManager(mml);
        }
    }
    else {
        std::cerr << "MonitoringManagerFactory: Simulator " << sim << " not registered"  << std::endl;
    }
    return nullptr;
}
