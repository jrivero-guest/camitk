# External Simulator supports
# Define all variables and settings for supporting external simulators:
# - Sofa
# - Ansys
# - Artisynth

# +----------------+
# |  SOFA SUPPORT  |
# +----------------+
if (SOFA_SUPPORT)
    # Defines the following variables 
    # - MML_SOFA_HEADERS
    # - MML_SOFA_SRC
    
    # add the sofa widget
    if(MML_GENERATE_GUI)
        set(SOFA_MOC_SRCS SofaWidget.h)
        set(SOFA_UIS SofaWidget.ui)
        qt4_wrap_ui(SOFA_UIS_H ${SOFA_UIS})
        # qt4_wrap_cpp(SOFA_MOCS ${SOFA_MOC_SRCS}) Not needed if AUTOMOC is set to ON (CMake therefore handle moc automatically)
        set(SOFA_GUI_SRC SofaWidget.cpp)
    endif()

    # define all the sources needed to support Sofa
    set(MML_SOFA_HEADERS SofaSimulator.h
                         TranslationConstraint.h
                         ${SOFA_UIS_H}
                         ${SOFA_MOC_SRCS}
    )
    set(MML_SOFA_SRC SofaSimulator.cpp
                     TranslationConstraint.cpp
                     TranslationConstraint.inl
                     ${SOFA_MOCS}
                     ${SOFA_MOC_UI}
                     ${SOFA_GUI_SRC}
    )

    # specific flags needed (C++ code)
    add_definitions(-DSOFA_SUPPORT)
    add_definitions(-DSOFA_BUILD_DIR="${SOFA_BUILD_DIR}")
    add_definitions(-DSOFA_SOURCE_DIR="${SOFA_SOURCE_DIR}")
    add_definitions(-DSOFA_STABLE)
endif()

# +-----------------+
# |  ANSYS SUPPORT  |
# +-----------------+
set(MML_ANSYS_BIN $ENV{MML_ANSYS_BIN} CACHE FILE "Where to find Ansys binary")
set(MML_ANSYS_WORKING_DIR $ENV{MML_ANSYS_WORKING_DIR} CACHE PATH "Specify a folder for ansys working files")
if((EXISTS ${MML_ANSYS_BIN}) AND (EXISTS ${MML_ANSYS_WORKING_DIR}))
    # Compile Ansys simulator files (ON or OFF)
    option(MML_ANSYS_SUPPORT "Compile Ansys simulator?" ON)

    # ansys settings
    if(MML_ANSYS_SUPPORT)
        # compilation flag
        add_definitions(-DANSYS_SUPPORT)
        add_definitions(-DANSYS_BIN="${MML_ANSYS_BIN}" -DANSYS_WORKING_DIR="${MML_ANSYS_WORKING_DIR}")

        if(MML_GENERATE_GUI)
            set(ansys_MOC_SRCS  AnsysWidget.h)
            set(ansys_UIS       AnsysWidget.ui)
            qt4_wrap_ui(ansys_UIS_H ${ansys_UIS})
            qt4_wrap_cpp(ansys_MOC_UI ${ansys_UIS_H})
            qt4_wrap_cpp(ansys_MOCS ${ansys_MOC_SRCS})
            set(ansys_QT_SRC AnsysWidget.cpp)
        endif(MML_GENERATE_GUI)

        set(MML_ANSYS_HEADERS       AnsysBatch.h  
                                    AnsysBatchWriter.h
                                    PrepWriter.h
                                    SoluWriter.h
                                    PostWriter.h
                                    ParametersWriter.h
                                    AnsysSimulator.h
                                    ${ansys_UIS_H}
                                    ${ansys_MOC_SRCS}
        )

        set(MML_ANSYS_SRC           AnsysBatch.cpp
                                    AnsysBatchWriter.cpp
                                    PrepWriter.cpp
                                    SoluWriter.cpp
                                    PostWriter.cpp
                                    ParametersWriter.cpp
                                    AnsysSimulator.cpp
                                    ${ansys_MOCS}
                                    ${ansys_MOC_UI}
                                    ${ansys_QT_SRC}
        )
    endif()
endif()

# +---------------------+
# |  ARTISYNTH SUPPORT  |
# +---------------------+
set(MML_ARTISYNTH_BIN $ENV{MML_ARTISYNTH_BIN} CACHE FILE "Where to find artisynth binary")
set(MML_ARTISYNTH_WORKING_DIR $ENV{MML_ARTISYNTH_WORKING_DIR} CACHE PATH "Specify a folder for artisynth working files")
if((EXISTS ${MML_ARTISYNTH_BIN}) AND (EXISTS ${MML_ARTISYNTH_WORKING_DIR}))
    # Compile ARTISYNTH simulator files (ON or OFF)
    option(MML_ARTISYNTH_SUPPORT "Compile Artisynth simulator?" ON)

    # ARTISYNTH settings
    if(MML_ARTISYNTH_SUPPORT)
        # compilation flag
        add_definitions(-DARTISYNTH_SUPPORT)
        add_definitions(-DARTISYNTH_BIN="${MML_ARTISYNTH_BIN}" -DARTISYNTH_WORKING_DIR="${MML_ARTISYNTH_WORKING_DIR}")
        set(MML_ARTISYNTH_HEADERS   ArtiSynthSimulator.h)
        set(MML_ARTISYNTH_SRC       ArtiSynthSimulator.cpp)
    endif(MML_ARTISYNTH_SUPPORT)
endif()