/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef TOOLS_ATOMITERATOR_H
#define TOOLS_ATOMITERATOR_H

#include <Load.h>
#include <PhysicalModel.h>
#include <cstring>
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * Allow iteration over a set of atoms given by a target list (component name or index list)
 */
class AtomIterator {

public:

    /// constructor
    AtomIterator(PhysicalModel* pml, std::string targetList);
    /// destructor
    ~AtomIterator() = default;

    /// place iterator on first atom
    void begin();
    /// return true if iterator is over the last atom
    bool end();
    /// if iteration is not finished, advance to next atom
    void next();
    /// return current atom
    Atom* currentAtom();

    /// redefinition of ++ operator, advance iterator to next atom
    void operator++();

private:
    /// atoms list
    std::vector<Atom*> atoms;
    /// current atom index
    unsigned int index;

};

#endif // TOOLS_ATOMITERATOR_H
