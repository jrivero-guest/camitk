/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorDistanceY.h"

// Tools includes
#include "Tools.h"
#include "AtomIterator.h"

#include <cmath>

// -------------------- constructor --------------------
MonitorDistanceY::MonitorDistanceY(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, SCALARSET) {}

// -------------------- calculate --------------------
void MonitorDistanceY::calculate() {
    values.clear();
    double posSimul[3];
    double posRef[3];
    double realTime;

    AtomIterator it = AtomIterator(monitoringManager->getPml(), target);
    for (it.begin(); !it.end(); it.next()) {
        it.currentAtom()->getPosition(posSimul);
        posSimul[1] = posSimul[1] + dy;
        if (references[0]->getMonitoredData("Position", monitoringManager->getCurrentTime(), it.currentAtom()->getIndex(), realTime, posRef)) {
            double resultat = fabs(posSimul[1] - posRef[1]);
            values.push_back(resultat);
        }
        else {
            std::cerr << "getPosition non implemented" << std::endl;
        }
    }

    write();
}


// -------------------- getType --------------------
std::string MonitorDistanceY::getTypeName() {
    return "Distance y";
}

