/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// Stopping criteria includes
#include "Position.h"
// Stopping criteria includes
#include "Tools.h"
#include "AtomIterator.h"

#include <iostream>

// -------------------- constructor --------------------
Position::Position(mml::Position* c, MonitoringManager* monitoringManager, MultipleCriterion* parent): Criterion(c, monitoringManager, parent) {

    if (c->target().present()) {
        target = c->target().get();
    }
    else {
        // if target is not provided, use all atoms
        target = monitoringManager->getPml()->getAtoms()->getName();
    }

    switch (c->unit()) {
        case mml::PositionUnit::nm:
            factor = 1000000;
            unit = "nm";
            break;
        case mml::PositionUnit::mm:
            factor = 1000;
            unit = "mm";
            break;
        default:
            std::cerr << "Position unit error" << std::endl;
    }
}

// -------------------- destructor --------------------
Position::~Position() {}

// -------------------- calculate --------------------
void Position::calculate() {
    values.clear();
    PhysicalModel* pml = monitoringManager->getPml();
    double pos[3];
    double posOld[3];
    // Iteration over all atoms
    AtomIterator it = AtomIterator(monitoringManager->getPml(), target);
    for (it.begin(); !it.end(); it.next()) {
        //for (unsigned int i = 0; i < pml->getAtoms()->getNumberOfStructures(); i++) {
        int index = it.currentAtom()->getIndex();
        //int index =pml->getAtoms()->getStructure(i)->getIndex();
        it.currentAtom()->getPosition(pos);
        //pml->getAtom(index)->getPosition(pos);
        monitoringManager->getOldPosition(posOld, index);
        values.push_back(distance(pos, posOld)*factor);
    }
    write();
}

// -------------------- getName --------------------
std::string Position::getName() {
    return "Position";
}
