/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITERS_PREPWRITER_H
#define SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITERS_PREPWRITER_H

#include <string>

#include "AnsysBatchWriter.h"

/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * TODO Comment class here.
 */
class PrepWriter: public AnsysBatchWriter {

public:
    /// constructor
    PrepWriter(std::string wd, MonitoringManager* monitoringManager);
    /// destructor
    ~PrepWriter();

    /// write the PREP part of batch file into a string
    std::string write();

private:
    /// young modulus
    double young;
    /// poisson ratio
    double poisson;
    /// element type used
    std::string elem; //"et,1,solid45"
    /// name for .node ans .elem generated files
    std::string fileName;
    /// Ansys working directory
    std::string workingDir;
};

#endif // SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITERS_PREPWRITER_H