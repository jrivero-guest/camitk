/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Monitor includes
#include "MonitorVolume.h"
#include <iostream>

// -------------------- constructor --------------------
MonitorVolume::MonitorVolume(mml::Monitor* m, MonitoringManager* monitoringManager): Monitor(m, monitoringManager, SCALAR) {}

// -------------------- destructor --------------------
MonitorVolume::~MonitorVolume() {}

// -------------------- calculate --------------------
void MonitorVolume::calculate() {
    values.clear();
    double volume = 0;

    StructuralComponent* scSimul = dynamic_cast<StructuralComponent*>(monitoringManager->getPml()->getComponentByName(target));
    if (scSimul) {
        // loop on all structure
        for (unsigned int i = 0; i < scSimul->getNumberOfStructures(); i++) {
            Cell* cSimul = dynamic_cast<Cell*>(scSimul->getStructure(i));
            if (cSimul) {
                volume += cSimul->volume();
            }
        }
    }

    values.push_back(volume);
    write();
}

// -------------------- getType --------------------
std::string MonitorVolume::getTypeName() {
    return "Volume";
}

// -------------------- write --------------------
void MonitorVolume::write() {
    // write data
    std::string s = "";
    for (unsigned int i = 0; i < values.size(); i++) {
        std::ostringstream ss;
        ss << values[i];
        s = s + ss.str() + " ";
    }
    mappedObject->data(s);
}

