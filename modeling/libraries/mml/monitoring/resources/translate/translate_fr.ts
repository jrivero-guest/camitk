<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>AnsysWidget</name>
    <message>
        <location filename="../../AnsysWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../AnsysWidget.ui" line="35"/>
        <source>Ansys batch file:</source>
        <translation>Fichier batch Ansys:</translation>
    </message>
    <message>
        <location filename="../../AnsysWidget.ui" line="75"/>
        <source>Apply changes</source>
        <translation>Appliquer les changements</translation>
    </message>
    <message>
        <location filename="../../AnsysWidget.cpp" line="53"/>
        <source>Apply scene changes</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../AnsysWidget.cpp" line="53"/>
        <source>This will save current changes batch file and remake calculation.
 Not saved data will be lost.</source>
        <translation>Cela sauvera les changements actuels du fichier batch et refera les calculs.
Les données non sauvées seront perdues.</translation>
    </message>
</context>
<context>
    <name>SofaWidget</name>
    <message>
        <location filename="../../SofaWidget.cpp" line="68"/>
        <source>Apply scene changes</source>
        <translation>Appliquer les changements de scene</translation>
    </message>
    <message>
        <location filename="../../SofaWidget.cpp" line="68"/>
        <source>This will save current changes in scn file and reload simulation.
 Not saved data will be lost.</source>
        <translation>Cela sauvera les changements actuels du fichier scnh et refera la simulation.
Les données non sauvées seront perdues.</translation>
    </message>
    <message>
        <location filename="../../SofaWidget.cpp" line="91"/>
        <source>/bin/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../SofaWidget.cpp" line="97"/>
        <source>/bin/runSofa</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ui_SofaWidget</name>
    <message>
        <location filename="../../SofaWidget.ui" line="14"/>
        <source>Form</source>
        <translation>Forme</translation>
    </message>
    <message>
        <location filename="../../SofaWidget.ui" line="35"/>
        <source>Sofa scn file:</source>
        <translation>Fichier scn Sofa:</translation>
    </message>
    <message>
        <location filename="../../SofaWidget.ui" line="62"/>
        <source>Apply changes</source>
        <translation>Appliques les changements</translation>
    </message>
    <message>
        <location filename="../../SofaWidget.ui" line="82"/>
        <source>Run into Sofa</source>
        <translation>Demarrer dans Sofa</translation>
    </message>
</context>
</TS>
