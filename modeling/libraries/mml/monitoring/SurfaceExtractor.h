/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef TOOLS_SURFACEEXTRACTOR_SURFACEEXTRACTOR_H
#define TOOLS_SURFACEEXTRACTOR_SURFACEEXTRACTOR_H

#include <vector>

// Tools includes
#include "Facet.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * A class to generate the external surface of a pml
 * to use it just add the cells among which you want to extract an external surface
 * designed for the case where a pml represent several object and thus there are several surface to extract
 * for an exemple of use see SofaSimulator.cpp
 */
class SurfaceExtractor {

public:
    /// constructor
    SurfaceExtractor(PhysicalModel* pm);
    /// destructor
    ~SurfaceExtractor();

    /// initialize the extractor
    void init();
    /// try to add a cell in the surface extractor and do it if this cell is not in
    void AddCell(Cell* c);
    /** generate the external surface
     * @param surface add the cells of the surface in the component surface
     * @param surfacePoints add the points of the surface in the component surfacePoints
     */
    void generateExternalSurface(StructuralComponent& surface, StructuralComponent& surfacePoints);

private:
    /// vector with all the facets
    std::vector<Facet*> facets;
    /// check if equivalent of already existing facet
    void equivalent(int size, unsigned int id[]);
    /// the physical model
    PhysicalModel* pm;

};

#endif // TOOLS_SURFACEEXTRACTOR_SURFACEEXTRACTOR_H
