/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITER_H
#define SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITER_H

#include <string>

#include "MonitoringManager.h"

/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 *  Ansys batch writer helps to write a part of a batch file
 */
class AnsysBatchWriter {

public:
    /// constructor
    AnsysBatchWriter(MonitoringManager* monitoringManager);
    /// destructor
    virtual ~AnsysBatchWriter();

    /// write a part of batch file into a string
    virtual std::string write() = 0;

protected:
    /// monitoring manager
    MonitoringManager* monitoringManager;
};

#endif // SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCHWRITER_H