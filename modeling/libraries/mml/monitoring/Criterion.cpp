/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <cmath>
#include <iostream>

// Stopping criteria includes
#include "Criterion.h"
#include "MethodFactory.h"

using namespace std;

// -------------------- constructor --------------------
Criterion::Criterion(mml::Criteria* c, MonitoringManager* monitoringManager, MultipleCriterion* parent): StoppingCriterion(monitoringManager, parent), mappedObject(c) {
    meth = MethodFactory::createMethod(c->method());
    unit = "";
}

// -------------------- destructor --------------------

Criterion::~Criterion() {
    if (meth) {
        delete meth;
    }
    values.clear();
}


// -------------------- checkCriteria --------------------
bool Criterion::checkCriterion() {
    calculate();
    return (meth->test(values));
}

// -------------------- write --------------------
void Criterion::write() {
    string s = "";
    for (unsigned int i = 0; i < values.size(); i++) {
        ostringstream ss;
        ss << values[i];
        s = s + ss.str() + " ";
    }
    mappedObject->data(s);
}

// -------------------- getChild --------------------
StoppingCriterion* Criterion::getChild(const unsigned int i) {
    return nullptr;
}

// -------------------- getNumberOfChildren --------------------
int Criterion::getNumberOfChildren() {
    return -1;
}

// -------------------- getMethodString --------------------
std::string Criterion::getMethodString() {
    return meth->toString() + unit;
}

// -------------------- scopeToString --------------------
std::string Criterion::scopeToString() {
    return meth->scopeTosString();
}
