/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#include <iostream>

// Stopping criteria includes
#include "MultipleCriterion.h"
#include "CriterionFactory.h"
#include "MultipleCriterionFactory.h"

using namespace std;

// -------------------- constructor --------------------
MultipleCriterion::MultipleCriterion(mml::MultipleCriteria* m, MonitoringManager* monitoringManager, MultipleCriterion* parent): StoppingCriterion(monitoringManager, parent), mappedObject(m) {
    mml::MultipleCriteria::criteria_sequence& crit(m->criteria());
    for (mml::MultipleCriteria::criteria_iterator i(crit.begin()); i != crit.end(); ++i) {
        criterias.push_back(CriterionFactory::createCriterion(&(*i), monitoringManager, this));
    }
    mml::MultipleCriteria::multipleCriteria_sequence& crit2(m->multipleCriteria());
    for (mml::MultipleCriteria::multipleCriteria_iterator i(crit2.begin()); i != crit2.end(); ++i) {
        criterias.push_back(MultipleCriterionFactory::createMultipleCriteria(&(*i), monitoringManager, this));
    }
}

// -------------------- destructor --------------------

MultipleCriterion::~MultipleCriterion() {
    while (!criterias.empty()) {
        delete criterias.back();
        criterias.pop_back();
    }
}

// -------------------- getNumberOfChildren --------------------
int MultipleCriterion::getNumberOfChildren() {
    return (int) criterias.size();
}

// -------------------- getChild --------------------
StoppingCriterion* MultipleCriterion::getChild(const unsigned int i) {
    if (i < criterias.size()) {
        return criterias[i];
    }
    else {
        return nullptr;
    }
}

// -------------------- addChild --------------------
void MultipleCriterion::addChild(StoppingCriterion* sc) {
    criterias.push_back(sc);
}

// -------------------- removeChild --------------------
void MultipleCriterion::removeChild(const unsigned int i) {
    std::vector <StoppingCriterion*>::iterator it;
    it = criterias.begin() + i;
    criterias.erase(it);
}

