/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
// Stopping criteria includes
#include "AndMultipleCriterion.h"

// -------------------- cosntructor --------------------
AndMultipleCriterion::AndMultipleCriterion(mml::MultipleCriteria* m, MonitoringManager* monitoringManager, MultipleCriterion* parent): MultipleCriterion(m, monitoringManager, parent) {}

// -------------------- checkCriteria --------------------
bool AndMultipleCriterion::checkCriterion() {
    bool result = true;
    std::vector<StoppingCriterion*>::iterator it;
    for (it = criterias.begin(); it != criterias.end(); it++) {
        result = result && ((*it)->checkCriterion());
    }
    return result;
}

// -------------------- getName --------------------
std::string AndMultipleCriterion::getName() {
    return "And";
}
