/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCH_H
#define SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCH_H

#include <string>

#include "MonitoringManager.h"

#include "PrepWriter.h"
#include "SoluWriter.h"
#include "PostWriter.h"
#include "ParametersWriter.h"
/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * Ansys batch file handler
 */
class AnsysBatch {

public:
    /**
     * constructor
     * @param wd Ansys working directory
     */
    AnsysBatch(std::string wd, MonitoringManager* monitoringManager);

    /// destructor
    ~AnsysBatch();

    /// write entire batch file if file do not exist
    void write();
    /// return batch file's name
    std::string getFileName();

private:
    /// batch file's name
    std::string fileName;
    /// Ansys working directory
    std::string workingDir;
    /// allows one to write the PREP part of the batch
    PrepWriter* prepw;
    /// allows one to write the SOLU part of the batch
    SoluWriter* soluw;
    /// allows one to write the POST part of the batch
    PostWriter* postw;
    /// allows one to write the parameters in the batch
    ParametersWriter* paramw;
    /// monitoring manager
    MonitoringManager* monitoringManager;

};

#endif // SIMULATOR_SIMULATORS_ANSYS_ANSYSBATCH_H
