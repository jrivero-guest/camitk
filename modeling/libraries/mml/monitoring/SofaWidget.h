/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMULATOR_SIMULATORS_SOFA_SOFAWIDGET_H
#define SIMULATOR_SIMULATORS_SOFA_SOFAWIDGET_H

#include <QProcess>

#include "SimulatorWidget.h"
#include "SofaSimulator.h"
#include "Xmlhighlighter.h"

#include "ui_SofaWidget.h"

/**
 *
 * @ingroup group_cepmodeling_libraries_mml
 *
 * @brief
 * TODO Comment class here.
 */
class SofaWidget: public SimulatorWidget {
    Q_OBJECT

public:
    /// constructor
    SofaWidget(QWidget* parent = 0, SofaSimulator* sofaSimulator = NULL);
    /// destructor
    ~SofaWidget();

    /** write text typed in the highlighted QTextEdit into a scn file
     * @param path filename to use
     */
    void writeScn(QString path);


public  slots:
    /// slot called when runSofa button is pressed
    void runSofa();
    /// slot called when apply changes is pressed
    void applyChanges();
    /// slot called when sofa is finished
    void sofaExited();

private:
    /// the ui widget designed in qtdesigner
    Ui::ui_SofaWidget ui;
    /// sofa scn file
    std::string scnFile;
    /// temp copy of scn filr
    std::string scnFileTemp;
    /// xml highlighter
    XmlHighlighter* xmlHighlighter;
    /// the sofa simulator
    SofaSimulator* sofaSimulator;

};

#endif