/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "StructuralComponentProperties.h"
//pmlschema includes
#include <StructuralComponent.hxx>

StructuralComponentProperties::StructuralComponentProperties(PhysicalModel* p, physicalModel::StructuralComponent xmlSC) : Properties(p) {
    alloc();

    setName(xmlSC.name().get());

    if (xmlSC.mode().present()) {
        if (!xmlSC.mode().get().compare("NONE")) {
            setMode(RenderingMode::NONE);
        }
        else if (!xmlSC.mode().get().compare("POINTS")) {
            setMode(RenderingMode::POINTS);
        }
        else if (!xmlSC.mode().get().compare("POINTS_AND_SURFACE")) {
            setMode(RenderingMode::POINTS_AND_SURFACE);
        }
        else if (!xmlSC.mode().get().compare("SURFACE")) {
            setMode(RenderingMode::SURFACE);
        }
        else if (!xmlSC.mode().get().compare("WIREFRAME")) {
            setMode(RenderingMode::WIREFRAME);
        }
        else if (!xmlSC.mode().get().compare("WIREFRAME_AND_POINTS")) {
            setMode(RenderingMode::WIREFRAME_AND_POINTS);
        }
        else if (!xmlSC.mode().get().compare("WIREFRAME_AND_SURFACE")) {
            setMode(RenderingMode::WIREFRAME_AND_SURFACE);
        }
        else if (!xmlSC.mode().get().compare("WIREFRAME_AND_SURFACE_AND_POINTS")) {
            setMode(RenderingMode::WIREFRAME_AND_SURFACE_AND_POINTS);
        }
        else {
            std::cerr << "BasicSCProperties::BasicSCProperties: Error parsing structural component rendering mode (" << xmlSC.mode().get() << ")" << std::endl;
        }
    }
    else {
        setMode(RenderingMode::NONE);
    }

    if (xmlSC.color().present()) {
        setRGBA(xmlSC.color().get().r(), xmlSC.color().get().g(), xmlSC.color().get().b(), xmlSC.color().get().a());
    }
    else {
        setColor(DEFAULT);
    }

    // unkown attributes
    xmlToFields(xmlSC.any_attribute());

}

// --------------- xmlPrint ---------------
void StructuralComponentProperties::xmlPrint(std::ostream& o) {

    // beginning of the SC properties
    // print the name if there is one
    if (getName() != "") {
        o << " name=\"" << getName().c_str() << "\"";
    }

    // the mode property (if different than default)
    if (mode.getMode() != RenderingMode::NONE) {
        o << " mode=\"" << mode.getModeString() << "\"";
    }


    // print all fields
    for (std::map<std::string, std::string>::iterator it = fields.begin(); it != fields.end() ; it++) {
        o << " " << (*it).first << "=\"" << (*it).second << "\"";
    }
}
