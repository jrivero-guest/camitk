/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Structure.h"
#include "StructureProperties.h"

//-------------------- should become inline ------------------------
unsigned int Structure::getIndex() const {
    return Structure::properties->getIndex();
}

bool Structure::setIndex(const unsigned int newIndex) {
    Structure::properties->setIndex(newIndex);
    hasIndex = true;
    return true;
}

StructureProperties::GeometricType Structure::getType() const {
    return Structure::properties->getType();
}

void Structure::setName(std::string n) {
    properties->setName(n);
}

std::string Structure::getName() const {
    return properties->getName();
}

// --------------- setPhysicalModel ---------------
void Structure::setPhysicalModel(PhysicalModel* pm) {
    properties->setPhysicalModel(pm);
}


