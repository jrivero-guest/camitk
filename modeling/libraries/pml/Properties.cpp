/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Properties.h"

// XSD / XercesC stuffs
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/dom/DOMAttr.hpp>
#include <xercesc/util/XMLString.hpp>

// ---------------- constructor -----------------
Properties::Properties(const std::string n) {
    name = n;
    myPM = nullptr;
}

Properties::Properties(PhysicalModel* p, const std::string n) {
    name = n;
    myPM = p;
}

// ---------------- domToFields -----------------
void Properties::xmlToFields(xsd::cxx::tree::attribute_set<char> allAttributes) {

    for (auto& attribute : allAttributes) {
        std::string unknownAttrName, unknownAttrValue;
        char* buffer;

        // use transcoding util to transform the XML name, value from XMLString to std::string
        // name
        buffer = xercesc::XMLString::transcode(attribute.getName());
        unknownAttrName = buffer;
        xercesc::XMLString::release(&buffer);
        // value
        buffer = xercesc::XMLString::transcode(attribute.getValue());
        unknownAttrValue = buffer;
        xercesc::XMLString::release(&buffer);

        std::pair<std::string, std::string> attr(unknownAttrName, unknownAttrValue);
        this->fields.insert(attr);
    }
}

// ---------------- numberOfFields -----------------
unsigned int Properties::numberOfFields() const {
    return (unsigned int) fields.size();
}

// ---------------- getField -----------------
std::string Properties::getField(unsigned int id) const {
    auto it = fields.begin();
    unsigned int i = 0;

    while (it != fields.end() && i != id) {
        i++;
        it++;
    }

    if (it != fields.end()) {
        return it->first;
    }
    else {
        return "";
    }
}


