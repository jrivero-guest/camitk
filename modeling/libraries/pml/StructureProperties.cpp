/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "StructureProperties.h"

// ------------ Constructor -------------------
StructureProperties::StructureProperties(PhysicalModel* p, const StructureProperties::GeometricType t) : Properties(p) {
    type = t;
}


// ------------ toType (static) -------------------
StructureProperties::GeometricType StructureProperties::toType(const std::string t) {
    if (t == "ATOM") {
        return StructureProperties::ATOM;
    }
    else if (t == "TETRAHEDRON") {
        return StructureProperties::TETRAHEDRON;
    }
    else if (t == "HEXAHEDRON") {
        return StructureProperties::HEXAHEDRON;
    }
    else if (t == "WEDGE") {
        return StructureProperties::WEDGE;
    }
    else if (t == "PYRAMID") {
        return StructureProperties::PYRAMID;
    }
    else if (t == "POLY_LINE") {
        return StructureProperties::POLY_LINE;
    }
    else if (t == "POLY_VERTEX") {
        return StructureProperties::POLY_VERTEX;
    }
    else if (t == "LINE") {
        return StructureProperties::LINE;
    }
    else if (t == "TRIANGLE") {
        return StructureProperties::TRIANGLE;
    }
    else if (t == "QUAD") {
        return StructureProperties::QUAD;
    }
    else {
        return StructureProperties::INVALID;
    }
}

// ------------ toString (static) -------------------
std::string StructureProperties::toString(const StructureProperties::GeometricType t) {
    std::string typeStr;

    switch (t) {
        case StructureProperties::ATOM:
            typeStr = "ATOM";
            break;
        case StructureProperties::TETRAHEDRON:
            typeStr = "TETRAHEDRON";
            break;
        case StructureProperties::HEXAHEDRON:
            typeStr = "HEXAHEDRON";
            break;
        case StructureProperties::WEDGE:
            typeStr = "WEDGE";
            break;
        case StructureProperties::PYRAMID:
            typeStr = "PYRAMID";
            break;
        case StructureProperties::LINE:
            typeStr = "LINE";
            break;
        case StructureProperties::POLY_LINE:
            typeStr = "POLY_LINE";
            break;
        case StructureProperties::POLY_VERTEX:
            typeStr = "POLY_VERTEX";
            break;
        case StructureProperties::TRIANGLE:
            typeStr = "TRIANGLE";
            break;
        case StructureProperties::QUAD:
            typeStr = "QUAD";
            break;
        default:
            typeStr = "INVALID";
            break;
    }
    return typeStr;
}


// ------------ xmlPrint -------------------
void StructureProperties::xmlPrint(std::ostream& o) const {
    o << "<StructureProperties index=\"" << index << "\">" << std::endl;
    o << "<type = \"" << toString(type) << "\"/>" << std::endl;
    o << "</StructureProperties>" << std::endl;
}
