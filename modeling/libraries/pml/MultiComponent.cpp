/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "MultiComponent.h"

// ------------------ default constructor ---------------------
MultiComponent::MultiComponent(PhysicalModel* p) : Component(p) {
    components.clear();
}
// ------------------ constructor with name ---------------------
MultiComponent::MultiComponent(PhysicalModel* p, std::string n) : Component(p, n) {
    components.clear();
};

// ------------------ destructor ---------------------
MultiComponent::~MultiComponent() {
    deleteProperties();

    deleteAllSubComponents();

    // tell all parents that I am going away to the paradise of pointers
    removeFromParents();
}

// ------------------ deleteAllSubComponents ---------------------
void MultiComponent::deleteAllSubComponents() {
    for (auto& component : components) {
        delete component;
    }
    components.clear();
}

// --------------- xmlPrint ---------------
void MultiComponent::xmlPrint(std::ostream& o) const {
    if (getNumberOfSubComponents() > 0) {
        o << "<multiComponent";
        if (getName() != "") {
            o << " name=\"" << getName().c_str() << "\"";
        }

        // print extra properties
        for (unsigned int i = 0; i < properties->numberOfFields(); i++) {
            o << " " << properties->getField(i) << "=\"" << properties->getString(properties->getField(i)) << "\"";
        }

        o << ">" << std::endl;
        for (auto component : components) {
            component->xmlPrint(o);
        }
        o << "</multiComponent>" << std::endl;
    }
}

// --------------- getNumberOfCells ---------------
unsigned int MultiComponent::getNumberOfCells() const {
    unsigned int nrOfCells = 0;

    // add all the cells of all the sub components
    for (auto component : components) {
        nrOfCells += component->getNumberOfCells();
    }

    return nrOfCells;
}

// --------------- getCell ---------------
Cell* MultiComponent::getCell(unsigned int cellOrderNr) const {
    bool found;
    unsigned int i;
    unsigned int startOrderNr;
    unsigned int nrOfCells;

    if (components.size() == 0) {
        return nullptr;
    }

    // check in which component this cell is
    i = 0;
    startOrderNr = 0;
    do {
        nrOfCells = components[i]->getNumberOfCells();
        found = (cellOrderNr >= startOrderNr && cellOrderNr < (startOrderNr + nrOfCells));
        startOrderNr += nrOfCells;
    }
    while (!found && ++i < components.size());

    // get it
    return components[i]->getCell(cellOrderNr - (startOrderNr - nrOfCells));
}

// --------------- isVisible ---------------
bool MultiComponent::isVisible(const RenderingMode::Mode mode) const {
    unsigned int i = 0;
    while (i < components.size() && !components[i]->isVisible(mode)) {
        i++;
    }
    return (i != components.size());
}

// --------------- isVisible ---------------
void MultiComponent::setVisible(const RenderingMode::Mode mode, const bool b) {
    // set all subcomponents
    for (auto& component : components) {
        component->setVisible(mode, b);
    }
}

// --------------- setPhysicalModel ---------------
void MultiComponent::setPhysicalModel(PhysicalModel* pm) {
    Component::setPhysicalModel(pm);
    for (auto& component : components) {
        component->setPhysicalModel(pm);
    }
}
