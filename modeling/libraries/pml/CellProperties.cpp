/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "CellProperties.h"

// pmlschema includes
#include <CellProperties.hxx>

//----------------------- Class member init -----------------------
void CellProperties::resetUniqueIndex() {
    CellProperties::maxUniqueIndex = 0;
}

CellProperties::CellProperties(PhysicalModel* p, const StructureProperties::GeometricType t, physicalModel::CellProperties xmlCellProp)  : StructureProperties(p, t) {

    // index attribute
    if (xmlCellProp.index().present()) {
        index = xmlCellProp.index().get();
    }
    else {
        index = maxUniqueIndex++;
    }

    // name attribute
    if (xmlCellProp.name().present()) {
        setName(xmlCellProp.name().get());
    }

    // unknow attributes
    xmlToFields(xmlCellProp.any_attribute());
}

// initializing the static class member
unsigned int CellProperties::maxUniqueIndex = 0;

//----------------------- Constructors -----------------------
CellProperties::CellProperties(PhysicalModel* p, const StructureProperties::GeometricType t)  : StructureProperties(p, t) {
    index = maxUniqueIndex++;
}

CellProperties::CellProperties(PhysicalModel* p, const StructureProperties::GeometricType t, const unsigned int ind)  :  StructureProperties(p, t) {
    index = ind;

    if (ind >= maxUniqueIndex) {
        maxUniqueIndex = ind + 1;
    }
}

// --------------- xmlPrint ---------------
void CellProperties::xmlPrint(std::ostream& o) {

    // beginning of the atom properties
    o << "<cellProperties index=\"" << index << "\"";
    // print the type

    switch (getType()) {

        case StructureProperties::TETRAHEDRON:
            o << " type=\"TETRAHEDRON\"";
            break;

        case StructureProperties::HEXAHEDRON:
            o << " type=\"HEXAHEDRON\"";
            break;

        case StructureProperties::WEDGE:
            o << " type=\"WEDGE\"";
            break;

        case StructureProperties::PYRAMID:
            o << " type=\"PYRAMID\"";
            break;

        case StructureProperties::POLY_LINE:
            o << " type=\"POLY_LINE\"";
            break;

        case StructureProperties::POLY_VERTEX:
            o << " type=\"POLY_VERTEX\"";
            break;

        case StructureProperties::LINE:
            o << " type=\"LINE\"";
            break;

        case StructureProperties::TRIANGLE:
            o << " type=\"TRIANGLE\"";
            break;

        case StructureProperties::QUAD:
            o << " type=\"QUAD\"";
            break;

        default:
            o << " type=\"???\"";
            break;
    }

    if (getName() != "") {
        o << " name=\"" << getName().c_str() << "\"";
    }


    for (std::map<std::string, std::string>::iterator it = fields.begin(); it != fields.end() ; it++) {
        o << " " << (*it).first << "=\"" << (*it).second << "\"";
    }

    // end of the properties
    o << "/>" << std::endl;
}
