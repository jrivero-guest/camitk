/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Atom.h"
#include "StructuralComponent.h"
#include "PhysicalModel.h"

// pmlschema includes
#include <Atom.hxx>

//----------------------- Constructors -----------------------
Atom::Atom(PhysicalModel* p) {
    properties = new AtomProperties(p);
}

Atom::Atom(PhysicalModel* p, physicalModel::Atom node, unsigned int id) {
    properties = new AtomProperties(p, node.atomProperties());
    indexInAtoms = id;
}

Atom::Atom(PhysicalModel* p, const double pos[3]) {
    properties = new AtomProperties(p, pos);
}

Atom::Atom(PhysicalModel* p, const unsigned int ind) {
    properties = new AtomProperties(p, ind);
}

Atom::Atom(PhysicalModel* p, const unsigned int ind, const double pos[3]) {
    properties = new AtomProperties(p, ind, pos);
}


//----------------------- Destructor -----------------------
Atom::~Atom() {
    delete (AtomProperties*) properties;
    properties = nullptr;
}

//----------------------- setIndex -----------------------
bool Atom::setIndex(const unsigned int index) {
    // set the property
    Structure::setIndex(index);
    // tell the physical model about the change (and return true if insertion was ok)
    return properties->getPhysicalModel()->addGlobalIndexAtomPair(std::GlobalIndexStructurePair(index, this));
}

//----------------------- getIndexInAtoms -----------------------
unsigned int Atom::getIndexInAtoms() const {
    return indexInAtoms;
}

// --------------- xmlPrint ---------------
void Atom::xmlPrint(std::ostream& o, const StructuralComponent* sc) {
    // nothing is to be done in particular with sc, but it is here
    // because of the Structure::xmlPrint method has to be overriden

    // depending on the structure who do the calls, two cases:
    if (properties->getPhysicalModel() != nullptr && sc == properties->getPhysicalModel()->getAtoms()) {
        // - if it is the physical model atom list: the atom print its properties
        // print the atom and its properties
        o << "<atom>" << std::endl;
        ((AtomProperties*) properties)->xmlPrint(o);
        o << "</atom>" << std::endl;
    }
    else {
        // - if it is any other structural component: the atom print its ref
        o << "<atomRef index=\"" << getIndex() << "\"/>" << std::endl;
    }
}


