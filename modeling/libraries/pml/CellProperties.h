/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CELLPROPERTIES_H
#define CELLPROPERTIES_H

#include "StructureProperties.h"

#include <memory>

// pml schema forward declarations
namespace physicalModel {
class CellProperties;
}
/**
  * @ingroup group_cepmodeling_libraries_pml
  *
  * @brief
  * Describes and manages the properties attached to cells.
  *
 **/
class CellProperties : public StructureProperties {
public:
    /** Default constructor : generate an unique index
     *  @param myPM the physical model the atom belongs to
     *  @param t the type of the cell
     */
    CellProperties(PhysicalModel* myPM, const StructureProperties::GeometricType t);

    /** constructor from xml node: try to read and get the properties from xml
     *  @param myPM the physical model the atom belongs to
     *  @param t the type of the cell
     *  @param n the xml node to read to get the information
     */
    CellProperties(PhysicalModel* myPM, const StructureProperties::GeometricType t, physicalModel::CellProperties xmlCellProp);

    /** Use this constructor when you specifically want to set the index
     *  @param myPM the physical model the atom belongs to
     *  @param t the type of the cell
     *  @param ind an unique index
     */
    CellProperties(PhysicalModel* myPM, const StructureProperties::GeometricType t, const unsigned int ind);

    /// the destructor...
    ~CellProperties() = default;

    /** print to an output stream in "pseaudo" XML format.
       */
    virtual void xmlPrint(std::ostream&);

    /** Reinitialize the unique index to zero (usually that what you want to do when you
        * start to load a new PhysicalModel
        */
    static void resetUniqueIndex();

private:
    /// unique number (used to generate unique index for cells if not given at the instanciation)
    static unsigned int maxUniqueIndex;

};
#endif // CELLPROPERTIES_H
