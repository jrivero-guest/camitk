#!/usr/bin/env perl
# Compile the latex documentation

q^
/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2016 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/.
^ if 0;
 
use strict;
use File::Copy;

# No check on parameters. Should only be called from makefile

die "Invalid number of arguments\n" if @ARGV != 5;

my $LatexCompiler		= $ARGV[0];
my $LatexDirectory		= $ARGV[1];
my $LatexMainFile		= $ARGV[2];
my $PdfFile			= $ARGV[3];
my $DestinationFile		= $ARGV[4];


chdir $LatexDirectory or die "Error changing to dir $LatexDirectory: $!\n";

foreach my $Index (1..3)
{
  system($LatexCompiler, $LatexMainFile)
	and die "\n\nLatex compilation error\n\n";
}

move $PdfFile, $DestinationFile
	or die "Error renaming $PdfFile to $DestinationFile: $!\n";
