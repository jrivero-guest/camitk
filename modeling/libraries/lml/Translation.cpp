/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Translation.h"
#include "TranslationUnit.h"
#include <cmath>

Translation::Translation() {
    typeString = "Translation";
    unit = TranslationUnit::M();
}

void Translation::ansysPrint(std::ostream& o) const {
    // select target nodes
    Load::ansysPrint(o);
    double norm = sqrt(dir.getX() * dir.getX() + dir.getY() * dir.getY() + dir.getZ() * dir.getZ());
    // get the last value
    // TODO multiple Value events (one load step for each one)
    // TODO check the unit and convert to SI if needed
    double val = getValueEvent(numberOfValueEvents() - 1)->getValue();
    // intermediate normalize value if non zero
    double normalizedVal = 1.0;

    // TODO define a specific precision value as a constant in LML
    if (norm > 1e-10) {
        normalizedVal = val / norm;
    }

    // print the translation in ansys format
    if (dir.isXNull()) {
        o << "D, ALL, UX" << std::endl;
    }
    else {
        o << "D, ALL, UX, " << dir.getX()*normalizedVal << std::endl;
    }

    if (dir.isYNull()) {
        o << "D, ALL, UY" << std::endl;
    }
    else {
        o << "D, ALL, UY, " << dir.getY()*normalizedVal << std::endl;
    }

    if (dir.isZNull()) {
        o << "D, ALL, UZ" << std::endl;
    }
    else {
        o << "D, ALL, UZ, " << dir.getZ()*normalizedVal << std::endl;
    }
}


