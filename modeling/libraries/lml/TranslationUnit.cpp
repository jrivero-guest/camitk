/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "TranslationUnit.h"
// initializing the static class member using a singleton hidden in the member
// (avoid multiple declaration of static members across multiple loaded dlls/shared object)
TranslationUnit& TranslationUnit::M() {
    static TranslationUnit M("m");
    return M;
}

TranslationUnit& TranslationUnit::MICRO_M() {
    static TranslationUnit MICRO_M("µm");
    return MICRO_M;
}

TranslationUnit& TranslationUnit::NM() {
    static TranslationUnit NM("nm");
    return NM;
}

TranslationUnit& TranslationUnit::MM() {
    static TranslationUnit MM("mm");
    return MM;
}





