/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef VALUEEVENT_H
#define VALUEEVENT_H

#include <iostream>

/**
 * @ingroup group_cepmodeling_libraries_lml
 *
 *
 * @brief
 * A valueEvent of a load is composed of a value and a date.
 *
 *
 *
 **/
class ValueEvent {

public:
    /// constructor with initial values
    ValueEvent(const double v, const double d): value(v), date(d) {};

    /// default constructor
    ValueEvent() {};

    /// destructor
    ~ValueEvent() = default;

    /// return true if the event is active at time t
    bool isActive(const double t) const;

    /// return the scalar value of the event
    double getValue() ;
    /// return the scalar value of the event at time t
    double getValue(const double t) ;
    /// return the scalar value of the event at time t, knowing that next event is nextE
    double getValue(const double t, ValueEvent* nextE) ;

    /// set the value event date
    void setDate(const double);
    /// set the value event value
    void setValue(const double);

    /// double get start time
    double getDate() const;

    /** print to an output stream in XML format.
     * @see load.xsd the loadML XML schema
     */
    friend std::ostream& operator << (std::ostream&, ValueEvent);

    /// Print to an ostream
    void xmlPrint(std::ostream&);

private:
    /// actual value
    double value{0.0};
    /// actual date
    double date{0.0};

};

#endif //VALUEEVENT_H
