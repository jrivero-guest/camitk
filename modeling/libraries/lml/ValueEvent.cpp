/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

///a valueEvent of a load is composed of a value and a date

#include "ValueEvent.h"

//--------- isActive ----------
bool ValueEvent::isActive(const double t) const {
    return (t >= date);
}

//--------- getValue ----------
double ValueEvent::getValue()  {
    return value;
}

double ValueEvent::getValue(const double t)  {
    if (isActive(t)) {
        return getValue();
    }
    else {
        return 0.0;
    }
}

double ValueEvent::getValue(const double t, ValueEvent* nextEvent)  {
    if (isActive(t)) {
        return (getValue()
                + (t - getDate())
                * ((nextEvent->getValue() - getValue()) / (nextEvent->getDate() - getDate())));
    }
    else {
        return 0.0;    // Value is not active
    }
}


//--------- getDate ----------
double ValueEvent::getDate() const {
    return date;
}

// --------------- xmlPrint ---------------
void ValueEvent::xmlPrint(std::ostream& o) {
    o << "\t" << "<valueEvent date=\"" << date
      << "\" value=\"" << value
      << "\"/>" << std::endl;
}

// --------------- operator << ---------------
std::ostream& operator << (std::ostream& o,  ValueEvent e) {

    o << "\t" << "<valueEvent date=\"" << e.getDate()
      << "\" value=\"" << e.getValue()
      << "\"/>" << std::endl;

    return o;
}

// --------------- setDate ---------------
void ValueEvent::setDate(const double d) {
    date = d;
}

// --------------- setValue ---------------
void ValueEvent::setValue(const double v) {
    value = v;
}


