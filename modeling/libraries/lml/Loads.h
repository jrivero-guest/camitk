/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef LOADS_H
#define LOADS_H

#include "Load.h"
#include "ValueEvent.h"
#include "Direction.h"

#include <vector>

/**
 * @ingroup group_cepmodeling_libraries_lml
 *
 * @brief
 * This class makes it possible to manage a list of "Load".
 * Remember that Load is an abstract class (concrete instances are in
 * instances of Translation, Force...etc)
 *
 * Usage example:
    <pre>
    // reading:
    main()
    {
        Loads allLoads("myFile.lml");
        ...
        cout << allLoads;
    }

    // creating and writing:
    main()
    {
        Loads allLoads;
        Translation *t = new Translation();
        t->setUnit(..);
        ...
        allLoads->addLoad(t);
        ...
        cout << allLoads;
    }
    </pre>
 *
 * @note
 * All loads that are added to an object of this class are then taking over by it
 * (i.e. when an object of this class is deleted, it will delete all its loads).
 *
 **/
class Loads {

public:
    /// default constructor
    Loads() = default;

    /// build a list of load from an LML file
    Loads(std::string);

    /// destructor
    ~Loads();

    /// add a load to the list
    void addLoad(Load* ld);

    /// get a load by its index in the list
    Load* getLoad(const unsigned int i) const;

    /// delete a load and remove it from the list using its index
    void deleteLoad(const unsigned int i);

    /// get the number of "Load" stored in the list
    unsigned int numberOfLoads() const;

    /** print to an output stream in XML format.
    *  @see Loads.xsd
    */
    friend std::ostream& operator << (std::ostream&, const Loads);

    /// read the input xml file using xsd and instanciate the loads
    /// @param filename: the input lml file (xml).
    void xmlRead(std::string filename);

    /// Print to an ostream
    void xmlPrint(std::ostream&) const;

    /// Print the load list in ansys format (BEWARE: not everything is implemented)
    void ansysPrint(std::ostream&) const;

    /** get the first event date present in the list of loads
      * @return -1.0 if no events are found
      */
    double getFirstEventDate();

    /** get the last event date present in the list of loads
      * @return -1.0 if no events are found
      */
    double getLastEventDate();

    /// current version of the library
    static const char* VERSION;

private:
    /// vector of loads : these "Load" are created while the file is parsed
    std::vector <Load*> loads;

};


#endif //LOADS_H
