/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef PRESSUREUNIT_H
#define PRESSUREUNIT_H

#include "Unit.h"
/**
 * @ingroup group_cepmodeling_libraries_lml
 *
 * @brief
 * Class that defines the different units of a Load 'Pressure'.
 *
 * @note
 * This class implements the type-safe design pattern.
 *
 **/
class PressureUnit : public Unit {

public:
    /// Pascal
    static PressureUnit& PA();
    /// kiloPascal
    static PressureUnit& KPA();
    /// Millimeters of mercure
    static PressureUnit& MMHG();

private:
    PressureUnit(std::string n) {
        unitString = n;
    }
};


#endif //PRESSUREUNIT_H
