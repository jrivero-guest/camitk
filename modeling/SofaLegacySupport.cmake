# Old Legacy support for Sofa, not supported

# "find" SOFA (you need to have a SOFA_BUILD_DIR environment variable
# or to define it manually during configuration process)
set(SOFA_BUILD_DIR $ENV{SOFA_BUILD_DIR} CACHE PATH "Where to find SOFA (for SOFA svn, this is the build directory)")
if(EXISTS ${SOFA_BUILD_DIR})
  # Compile SOFA .scn import filter? (ON or OFF)
  option(SOFA_SUPPORT "Compile SOFA Simulator?" ON)
endif(EXISTS ${SOFA_BUILD_DIR})

# sofa settings
# previous versions of Sofa
# support for any other version of SOFA is not guaranted.
# Previous versions of Sofa were supported in previous version of CamiTK:
# - sofa svn version (or initial git version up to november 2014) -> CamiTK 3.4.0
# - sofa 1.0 RC1 from the sofa website -> CamiTK 3.x up to 3.3  
# - the old sofa beta 4 from the debian repository -> CamiTK 2.x
if(SOFA_SUPPORT)
    # unsupported previous versions
    # use at your own risk, no guarantee what so ever!
    add_definitions(-DSOFA_SUPPORT)
    add_definitions(-DSOFA_PATH="${SOFA_BUILD_DIR}")
    
    option(MML_SOFA_SVN "Set to ON (default) if SOFA svn build dir is available in ${SOFA_BUILD_DIR}" ON)  
    option(MML_SOFA_1_0_RC1 "Set to ON if SOFA 1.0 RC1 is available in ${SOFA_BUILD_DIR}" OFF)
    option(MML_SOFA_1_0_BETA4 "Set to if SOFA 1.0 beta 4 is available in ${SOFA_BUILD_DIR}" OFF)

    # compilation flag
    if (MML_SOFA_SVN)
        # include and everything else needed => path to source tree needed
        # automatically get the value of CMAKE_SOURCE_DIR in the build directory toplevel Makefile
        # 1. Get the correct line from the CMakeCache file 
        file(STRINGS ${SOFA_BUILD_DIR}/CMakeCache.txt SOFA_SOURCE_DIR_LINE REGEX "^Sofa_SOURCE_DIR:STATIC=(.*)$")
        # 2. Use a regex to extract the path from the line
        string(REGEX REPLACE "^Sofa_SOURCE_DIR:STATIC=(.*)$" "\\1" SOFA_SOURCE_DIR "${SOFA_SOURCE_DIR_LINE}")
        message("Detected sofa source directory is ${SOFA_SOURCE_DIR}")

        add_definitions(-DSOFA_SVN)
    endif()
    
    if (MML_SOFA_1_0_RC1)
        add_definitions(-DSOFA_1_0_RC1)
    endif()
    
    if (MML_SOFA_1_0_BETA4)
        add_definitions(-DSOFA_1_0_BETA4)
    endif()
     
    # library settings depends on the sofa version
    
    if (MML_SOFA_SVN)
        set(SOFA_LIBS 
            # SOFA_LIB_NAME
            #SofaGui            
            #SofaTree           
            SofaCore            
            SofaGuiQt           
            SofaHelper          
            SofaGuiGlut         
            SofaGuiMain         
            #SofaModeler        
            #SofaSimulation     
            #SofaObjectCreator  
            SofaDefaultType     
            #SofaGraph          

            # SOFA_LIB_COMPONENT_NAME
            #SofaComponent           
            #SofaComponentDev        
            SofaComponentBase        
            SofaComponentMisc        
            SofaComponentCommon      
            SofaComponentGeneral     
            #SofaComponentMiscDev    
            SofaComponentAdvanced    
            #SofaComponentAdvancedDev

            # SOFA_LIB_BASE_NAME
            SofaBaseAnimationLoop      
            SofaBaseCollision          
            SofaBaseLinearSolver       
            SofaBaseMechanics          
            SofaBaseTopology           
            SofaBaseVisual             

            # SOFA_LIB_GENERAL_NAME
            SofaBoundaryCondition     
            SofaConstraint            
            SofaDenseSolver           
            SofaEngine                
            SofaExporter              
            SofaGraphComponent        
            SofaOpenglVisual          
            SofaPreconditioner        
            SofaTopologyMapping       
            SofaUserInteraction       
            SofaValidation            
            SofaHaptics               
            
            # SOFA_LIB_ADVANCED_NAME
            #SofaAdvancedConstraint  
            #SofaAdvancedFem         
            #SofaAdvancedInteraction 
            SofaEigen2Solver         
            SofaEulerianFluid        
            #SofaMjedFem             
            SofaNonUniformFem        
            #SofaNonUniformFemDev    
            SofaSphFluid             
            SofaVolumetricData       
            
            # SOFA_LIB_MISC_NAME
            SofaMisc                  
            SofaMiscCollision         
            #SofaMiscCollisionDev      
            SofaMiscFem              
            #SofaMiscDev             
            #SofaMiscFemDev          
            SofaMiscForceField       
            #SofaMiscForcefieldDev   
            SofaMiscMapping          
            #SofaMiscMappingDev      
            SofaMiscSolver           
            #SofaMiscSolverDev       
            SofaMiscTopology         
            #SofaMiscTopologyDev     
            SofaMiscEngine           
        )
        set(SOFA_LIB_DIR ${SOFA_BUILD_DIR}/lib)
    endif()

    if (MML_SOFA_1_0_RC1)
        # old sofa RC1 from the tarballs
        if(WIN32)
            set(SOFA_LIB_DIR ${SOFA_BUILD_DIR}/lib/win32/ReleaseVC9 ${SOFA_BUILD_DIR}/lib/win32 ${SOFA_BUILD_DIR}/lib ${SOFA_BUILD_DIR}/bin )
        else()
            set(SOFA_LIB_DIR ${SOFA_BUILD_DIR}/lib/linux ${SOFA_BUILD_DIR}/lib)
        endif()

        # set dependencies
        set(SOFA_LIBS sofacore #sofa_component_dev
        #sofa_component_advanced_dev sofa_component_misc_dev
        sofa_component
        #sofa_non_uniform_fem_dev
        #sofa_advanced_fem 
        #sofa_mjed_fem 
        #sofa_advanced_constraint 
        #sofa_advanced_interaction 
        #sofa_misc_topology_dev 
        #sofa_misc_mapping_dev 
        #sofa_misc_forcefield_dev 
        #sofa_misc_fem_dev 
        #sofa_misc_collision_dev 
        #sofa_misc_solver_dev 
        #sofa_misc_dev
        sofa_exporter sofa_loader sofa_misc_forcefield sofa_component_base sofa_component_common sofa_component_general sofa_component_advanced sofa_component_misc sofahelper sofa_engine sofa_sph_fluid sofa_non_uniform_fem sofa_base_topology sofa_boundary_condition sofa_haptics sofa_constraint sofatree sofa_base_linear_solver sofa_implicit_ode_solver sofa_simple_fem sofa_topology_mapping sofa_user_interaction sofa_misc_collision sofa_preconditioner sofa_misc_topology sofa_mesh_collision sofa_volumetric_data 
        #sofabgl 
        sofa_base_collision sofa_deformable sofa_graph_component sofa_base_mechanics sofa_rigid sofa_base_animation_loop sofa_object_interaction sofa_explicit_ode_solver sofa_eigen2_solver sofa_validation sofa_dense_solver sofa_eulerian_fluid sofa_misc_mapping sofa_misc_fem sofa_misc_engine sofa_misc_solver sofa_misc
        sofa_base_visual #-lnewmat -lGLEW -lpng
            sofasimulation #-lminiFlowVR
            sofacore #-ltinyxml
            sofadefaulttype #-lQt3Support -lQtOpenGL -lQtGui -lQtCore -lGLU -lGL -lpthread
                sofamodeler
        )
    endif()

    if(MML_SOFA_1_0_BETA4)
        # Sofa1.0beta4 from the package
        set(SOFA_LIBS sofacore sofacomponent sofasimulation sofatree sofahelper sofadefaulttype)
    endif()
    
    # add debug suffix (TODO update for SOFA SVN)
    if(WIN32)
            if( CMAKE_BUILD_TYPE STREQUAL "Debug" OR CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo" )
                set(SOFA_LIBS_WIN32 ${SOFA_LIBS})
                set(SOFA_LIBS)
                if(MML_SOFA_1_0_RC1)
                    set(SOFA_VERSION_SUFFIX "_1_0d")
                endif()
                if(MML_SOFA_1_0_BETA4)
                    set(SOFA_VERSION_SUFFIX "d")
                endif()            
                foreach(SOFA_LIB ${SOFA_LIBS_WIN32})
                        set(SOFA_LIBS ${SOFA_LIBS} ${SOFA_LIB}${SOFA_VERSION_SUFFIX})
                endforeach()
            endif()
    endif()
     
    # SOFA include directories  
    # use sofa source directory for sofa svn version
    if (MML_SOFA_SVN)
        set(SOFA_INCLUDE_DIR ${SOFA_SOURCE_DIR}/include ${SOFA_SOURCE_DIR}/modules ${SOFA_SOURCE_DIR}/framework
            ${SOFA_SOURCE_DIR}/extlibs/miniBoost ${SOFA_SOURCE_DIR}/include/modules
            ${SOFA_SOURCE_DIR}/include/framework ${SOFA_SOURCE_DIR}/include/extlibs/miniBoost
            ${SOFA_SOURCE_DIR}/extlibs/tinyxml )
    endif()
    
    if (MML_SOFA_1_0_BETA4 OR MML_SOFA_1_0_RC1)
        # includes
        set(SOFA_INCLUDE_DIR ${SOFA_BUILD_DIR}/include ${SOFA_BUILD_DIR}/modules ${SOFA_BUILD_DIR}/framework
            ${SOFA_BUILD_DIR}/extlibs/miniBoost ${SOFA_BUILD_DIR}/include/modules
            ${SOFA_BUILD_DIR}/include/framework ${SOFA_BUILD_DIR}/include/extlibs/miniBoost )
    endif()
endif()
