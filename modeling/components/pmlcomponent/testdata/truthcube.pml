<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="truthcube" nrOfAtoms="729"
 nrOfExclusiveComponents="3"
 nrOfInformativeComponents="2"
 nrOfCells="1499"
 comment=" This is a representation of the Truth cube. See:  Kerdok, A. E.; Cotin, S. M.; Ottensmeyer, M. P.; Galea, A. M.; Howe, R. D. and Dawson, S. L. Truth Cube: Establishing Physical Standards for Soft Tissue Simulation Medical Image Analysis, 2003, 7, 283-291  doi:10.1016/S1361-8415(03)00008-2"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="element list">
<nrOfStructures value="729"/>
<atom>
<atomProperties index="0" x="-40" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="1" x="-30" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="2" x="-20" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="3" x="-10" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="4" x="0" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="5" x="10" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="6" x="20" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="7" x="30" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="8" x="40" y="-37" z="-57"/>
</atom>
<atom>
<atomProperties index="9" x="-40" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="10" x="-29" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="11" x="-20" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="12" x="-10" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="13" x="0" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="14" x="10" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="15" x="20" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="16" x="30" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="17" x="40" y="-27" z="-57"/>
</atom>
<atom>
<atomProperties index="18" x="-40" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="19" x="-30" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="20" x="-20" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="21" x="-10" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="22" x="0" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="23" x="10" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="24" x="20" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="25" x="30" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="26" x="40" y="-17" z="-57"/>
</atom>
<atom>
<atomProperties index="27" x="-40" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="28" x="-30" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="29" x="-20" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="30" x="-10" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="31" x="0" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="32" x="10" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="33" x="20" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="34" x="30" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="35" x="40" y="-7" z="-57"/>
</atom>
<atom>
<atomProperties index="36" x="-40" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="37" x="-30" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="38" x="-20" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="39" x="-10" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="40" x="0" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="41" x="10" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="42" x="20" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="43" x="30" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="44" x="40" y="3" z="-57"/>
</atom>
<atom>
<atomProperties index="45" x="-40" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="46" x="-30" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="47" x="-20" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="48" x="-10" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="49" x="0" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="50" x="10" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="51" x="20" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="52" x="30" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="53" x="40" y="13" z="-57"/>
</atom>
<atom>
<atomProperties index="54" x="-40" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="55" x="-30" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="56" x="-20" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="57" x="-10" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="58" x="0" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="59" x="10" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="60" x="20" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="61" x="30" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="62" x="40" y="23" z="-57"/>
</atom>
<atom>
<atomProperties index="63" x="-40" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="64" x="-30" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="65" x="-20" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="66" x="-10" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="67" x="0" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="68" x="10" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="69" x="20" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="70" x="30" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="71" x="40" y="33" z="-57"/>
</atom>
<atom>
<atomProperties index="72" x="-40" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="73" x="-30" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="74" x="-20" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="75" x="-10" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="76" x="0" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="77" x="10" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="78" x="20" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="79" x="30" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="80" x="40" y="43" z="-57"/>
</atom>
<atom>
<atomProperties index="81" x="-40" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="82" x="-30" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="83" x="-20" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="84" x="-10" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="85" x="0" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="86" x="10" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="87" x="20" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="88" x="30" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="89" x="40" y="-37" z="-47"/>
</atom>
<atom>
<atomProperties index="90" x="-40" y="-27" z="-47"/>
</atom>
<atom>
<atomProperties index="98" x="40" y="-27" z="-47"/>
</atom>
<atom>
<atomProperties index="99" x="-40" y="-17" z="-47"/>
</atom>
<atom>
<atomProperties index="107" x="40" y="-17" z="-47"/>
</atom>
<atom>
<atomProperties index="108" x="-40" y="-7" z="-47"/>
</atom>
<atom>
<atomProperties index="116" x="40" y="-7" z="-47"/>
</atom>
<atom>
<atomProperties index="117" x="-40" y="3" z="-47"/>
</atom>
<atom>
<atomProperties index="125" x="40" y="3" z="-47"/>
</atom>
<atom>
<atomProperties index="126" x="-40" y="13" z="-47"/>
</atom>
<atom>
<atomProperties index="134" x="40" y="13" z="-47"/>
</atom>
<atom>
<atomProperties index="135" x="-40" y="23" z="-47"/>
</atom>
<atom>
<atomProperties index="143" x="40" y="23" z="-47"/>
</atom>
<atom>
<atomProperties index="144" x="-40" y="33" z="-47"/>
</atom>
<atom>
<atomProperties index="152" x="40" y="33" z="-47"/>
</atom>
<atom>
<atomProperties index="153" x="-40" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="154" x="-30" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="155" x="-20" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="156" x="-10" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="157" x="0" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="158" x="10" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="159" x="20" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="160" x="30" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="161" x="40" y="43" z="-47"/>
</atom>
<atom>
<atomProperties index="162" x="-40" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="163" x="-30" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="164" x="-20" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="165" x="-10" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="166" x="0" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="167" x="10" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="168" x="20" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="169" x="30" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="170" x="40" y="-37" z="-37"/>
</atom>
<atom>
<atomProperties index="171" x="-40" y="-27" z="-37"/>
</atom>
<atom>
<atomProperties index="179" x="40" y="-27" z="-37"/>
</atom>
<atom>
<atomProperties index="180" x="-40" y="-17" z="-37"/>
</atom>
<atom>
<atomProperties index="188" x="40" y="-17" z="-37"/>
</atom>
<atom>
<atomProperties index="189" x="-40" y="-7" z="-37"/>
</atom>
<atom>
<atomProperties index="197" x="40" y="-7" z="-37"/>
</atom>
<atom>
<atomProperties index="198" x="-40" y="3" z="-37"/>
</atom>
<atom>
<atomProperties index="206" x="40" y="3" z="-37"/>
</atom>
<atom>
<atomProperties index="207" x="-40" y="13" z="-37"/>
</atom>
<atom>
<atomProperties index="215" x="40" y="13" z="-37"/>
</atom>
<atom>
<atomProperties index="216" x="-40" y="23" z="-37"/>
</atom>
<atom>
<atomProperties index="224" x="40" y="23" z="-37"/>
</atom>
<atom>
<atomProperties index="225" x="-40" y="33" z="-37"/>
</atom>
<atom>
<atomProperties index="233" x="40" y="33" z="-37"/>
</atom>
<atom>
<atomProperties index="234" x="-40" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="235" x="-30" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="236" x="-20" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="237" x="-10" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="238" x="0" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="239" x="10" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="240" x="20" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="241" x="30" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="242" x="40" y="43" z="-37"/>
</atom>
<atom>
<atomProperties index="243" x="-40" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="244" x="-30" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="245" x="-20" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="246" x="-10" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="247" x="0" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="248" x="10" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="249" x="20" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="250" x="30" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="251" x="40" y="-37" z="-27"/>
</atom>
<atom>
<atomProperties index="252" x="-40" y="-27" z="-27"/>
</atom>
<atom>
<atomProperties index="260" x="40" y="-27" z="-27"/>
</atom>
<atom>
<atomProperties index="261" x="-40" y="-17" z="-27"/>
</atom>
<atom>
<atomProperties index="269" x="40" y="-17" z="-27"/>
</atom>
<atom>
<atomProperties index="270" x="-40" y="-7" z="-27"/>
</atom>
<atom>
<atomProperties index="278" x="40" y="-7" z="-27"/>
</atom>
<atom>
<atomProperties index="279" x="-40" y="3" z="-27"/>
</atom>
<atom>
<atomProperties index="287" x="40" y="3" z="-27"/>
</atom>
<atom>
<atomProperties index="288" x="-40" y="13" z="-27"/>
</atom>
<atom>
<atomProperties index="296" x="40" y="13" z="-27"/>
</atom>
<atom>
<atomProperties index="297" x="-40" y="23" z="-27"/>
</atom>
<atom>
<atomProperties index="305" x="40" y="23" z="-27"/>
</atom>
<atom>
<atomProperties index="306" x="-40" y="33" z="-27"/>
</atom>
<atom>
<atomProperties index="314" x="40" y="33" z="-27"/>
</atom>
<atom>
<atomProperties index="315" x="-40" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="316" x="-30" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="317" x="-20" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="318" x="-10" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="319" x="0" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="320" x="10" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="321" x="20" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="322" x="30" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="323" x="40" y="43" z="-27"/>
</atom>
<atom>
<atomProperties index="324" x="-40" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="325" x="-30" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="326" x="-20" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="327" x="-10" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="328" x="0" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="329" x="10" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="330" x="20" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="331" x="30" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="332" x="40" y="-37" z="-17"/>
</atom>
<atom>
<atomProperties index="333" x="-40" y="-27" z="-17"/>
</atom>
<atom>
<atomProperties index="341" x="40" y="-27" z="-17"/>
</atom>
<atom>
<atomProperties index="342" x="-40" y="-17" z="-17"/>
</atom>
<atom>
<atomProperties index="350" x="40" y="-17" z="-17"/>
</atom>
<atom>
<atomProperties index="351" x="-40" y="-7" z="-17"/>
</atom>
<atom>
<atomProperties index="359" x="40" y="-7" z="-17"/>
</atom>
<atom>
<atomProperties index="360" x="-40" y="3" z="-17"/>
</atom>
<atom>
<atomProperties index="368" x="40" y="3" z="-17"/>
</atom>
<atom>
<atomProperties index="369" x="-40" y="13" z="-17"/>
</atom>
<atom>
<atomProperties index="377" x="40" y="13" z="-17"/>
</atom>
<atom>
<atomProperties index="378" x="-40" y="23" z="-17"/>
</atom>
<atom>
<atomProperties index="386" x="40" y="23" z="-17"/>
</atom>
<atom>
<atomProperties index="387" x="-40" y="33" z="-17"/>
</atom>
<atom>
<atomProperties index="395" x="40" y="33" z="-17"/>
</atom>
<atom>
<atomProperties index="396" x="-40" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="397" x="-30" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="398" x="-20" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="399" x="-10" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="400" x="0" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="401" x="10" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="402" x="20" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="403" x="30" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="404" x="40" y="43" z="-17"/>
</atom>
<atom>
<atomProperties index="405" x="-40" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="406" x="-30" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="407" x="-20" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="408" x="-10" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="409" x="0" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="410" x="10" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="411" x="20" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="412" x="30" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="413" x="40" y="-37" z="-7"/>
</atom>
<atom>
<atomProperties index="414" x="-40" y="-27" z="-7"/>
</atom>
<atom>
<atomProperties index="422" x="40" y="-27" z="-7"/>
</atom>
<atom>
<atomProperties index="423" x="-40" y="-17" z="-7"/>
</atom>
<atom>
<atomProperties index="431" x="40" y="-17" z="-7"/>
</atom>
<atom>
<atomProperties index="432" x="-40" y="-7" z="-7"/>
</atom>
<atom>
<atomProperties index="440" x="40" y="-7" z="-7"/>
</atom>
<atom>
<atomProperties index="441" x="-40" y="3" z="-7"/>
</atom>
<atom>
<atomProperties index="449" x="40" y="3" z="-7"/>
</atom>
<atom>
<atomProperties index="450" x="-40" y="13" z="-7"/>
</atom>
<atom>
<atomProperties index="458" x="40" y="13" z="-7"/>
</atom>
<atom>
<atomProperties index="459" x="-40" y="23" z="-7"/>
</atom>
<atom>
<atomProperties index="467" x="40" y="23" z="-7"/>
</atom>
<atom>
<atomProperties index="468" x="-40" y="33" z="-7"/>
</atom>
<atom>
<atomProperties index="476" x="40" y="33" z="-7"/>
</atom>
<atom>
<atomProperties index="477" x="-40" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="478" x="-30" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="479" x="-20" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="480" x="-10" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="481" x="0" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="482" x="10" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="483" x="20" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="484" x="30" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="485" x="40" y="43" z="-7"/>
</atom>
<atom>
<atomProperties index="486" x="-40" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="487" x="-30" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="488" x="-20" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="489" x="-10" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="490" x="0" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="491" x="10" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="492" x="20" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="493" x="30" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="494" x="40" y="-37" z="3"/>
</atom>
<atom>
<atomProperties index="495" x="-40" y="-27" z="3"/>
</atom>
<atom>
<atomProperties index="503" x="40" y="-27" z="3"/>
</atom>
<atom>
<atomProperties index="504" x="-40" y="-17" z="3"/>
</atom>
<atom>
<atomProperties index="512" x="40" y="-17" z="3"/>
</atom>
<atom>
<atomProperties index="513" x="-40" y="-7" z="3"/>
</atom>
<atom>
<atomProperties index="521" x="40" y="-7" z="3"/>
</atom>
<atom>
<atomProperties index="522" x="-40" y="3" z="3"/>
</atom>
<atom>
<atomProperties index="530" x="40" y="3" z="3"/>
</atom>
<atom>
<atomProperties index="531" x="-40" y="13" z="3"/>
</atom>
<atom>
<atomProperties index="539" x="40" y="13" z="3"/>
</atom>
<atom>
<atomProperties index="540" x="-40" y="23" z="3"/>
</atom>
<atom>
<atomProperties index="548" x="40" y="23" z="3"/>
</atom>
<atom>
<atomProperties index="549" x="-40" y="33" z="3"/>
</atom>
<atom>
<atomProperties index="557" x="40" y="33" z="3"/>
</atom>
<atom>
<atomProperties index="558" x="-40" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="559" x="-30" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="560" x="-20" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="561" x="-10" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="562" x="0" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="563" x="10" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="564" x="20" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="565" x="30" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="566" x="40" y="43" z="3"/>
</atom>
<atom>
<atomProperties index="567" x="-40" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="568" x="-30" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="569" x="-20" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="570" x="-10" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="571" x="0" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="572" x="10" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="573" x="20" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="574" x="30" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="575" x="40" y="-37" z="13"/>
</atom>
<atom>
<atomProperties index="576" x="-40" y="-27" z="13"/>
</atom>
<atom>
<atomProperties index="584" x="40" y="-27" z="13"/>
</atom>
<atom>
<atomProperties index="585" x="-40" y="-17" z="13"/>
</atom>
<atom>
<atomProperties index="593" x="40" y="-17" z="13"/>
</atom>
<atom>
<atomProperties index="594" x="-40" y="-7" z="13"/>
</atom>
<atom>
<atomProperties index="602" x="40" y="-7" z="13"/>
</atom>
<atom>
<atomProperties index="603" x="-40" y="3" z="13"/>
</atom>
<atom>
<atomProperties index="611" x="40" y="3" z="13"/>
</atom>
<atom>
<atomProperties index="612" x="-40" y="13" z="13"/>
</atom>
<atom>
<atomProperties index="620" x="40" y="13" z="13"/>
</atom>
<atom>
<atomProperties index="621" x="-40" y="23" z="13"/>
</atom>
<atom>
<atomProperties index="629" x="40" y="23" z="13"/>
</atom>
<atom>
<atomProperties index="630" x="-40" y="33" z="13"/>
</atom>
<atom>
<atomProperties index="638" x="40" y="33" z="13"/>
</atom>
<atom>
<atomProperties index="639" x="-40" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="640" x="-30" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="641" x="-20" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="642" x="-10" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="643" x="0" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="644" x="10" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="645" x="20" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="646" x="30" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="647" x="40" y="43" z="13"/>
</atom>
<atom>
<atomProperties index="648" x="-40" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="649" x="-30" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="650" x="-20" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="651" x="-10" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="652" x="0" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="653" x="10" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="654" x="20" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="655" x="30" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="656" x="40" y="-37" z="23"/>
</atom>
<atom>
<atomProperties index="657" x="-40" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="658" x="-29" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="659" x="-20" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="660" x="-10" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="661" x="0" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="662" x="10" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="663" x="20" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="664" x="30" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="665" x="40" y="-27" z="23"/>
</atom>
<atom>
<atomProperties index="666" x="-40" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="667" x="-30" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="668" x="-20" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="669" x="-10" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="670" x="0" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="671" x="10" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="672" x="20" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="673" x="30" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="674" x="40" y="-17" z="23"/>
</atom>
<atom>
<atomProperties index="675" x="-40" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="676" x="-30" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="677" x="-20" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="678" x="-10" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="679" x="0" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="680" x="10" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="681" x="20" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="682" x="30" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="683" x="40" y="-7" z="23"/>
</atom>
<atom>
<atomProperties index="684" x="-40" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="685" x="-30" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="686" x="-20" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="687" x="-10" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="688" x="0" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="689" x="10" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="690" x="20" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="691" x="30" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="692" x="40" y="3" z="23"/>
</atom>
<atom>
<atomProperties index="693" x="-40" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="694" x="-30" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="695" x="-20" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="696" x="-10" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="697" x="0" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="698" x="10" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="699" x="20" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="700" x="30" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="701" x="40" y="13" z="23"/>
</atom>
<atom>
<atomProperties index="702" x="-40" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="703" x="-30" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="704" x="-20" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="705" x="-10" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="706" x="0" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="707" x="10" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="708" x="20" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="709" x="30" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="710" x="40" y="23" z="23"/>
</atom>
<atom>
<atomProperties index="711" x="-40" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="712" x="-30" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="713" x="-20" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="714" x="-10" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="715" x="0" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="716" x="10" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="717" x="20" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="718" x="30" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="719" x="40" y="33" z="23"/>
</atom>
<atom>
<atomProperties index="720" x="-40" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="721" x="-30" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="722" x="-20" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="723" x="-10" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="724" x="0" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="725" x="10" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="726" x="20" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="727" x="30" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="728" x="40" y="43" z="23"/>
</atom>
<atom>
<atomProperties index="577" x="-29.69" y="-28.86" z="12.62"/>
</atom>
<atom>
<atomProperties index="496" x="-29.07" y="-27.43" z="3.33"/>
</atom>
<atom>
<atomProperties index="415" x="-28.76" y="-26.88" z="-6.49"/>
</atom>
<atom>
<atomProperties index="334" x="-28.91" y="-27.44" z="-16.83"/>
</atom>
<atom>
<atomProperties index="253" x="-28.95" y="-26.88" z="-26.91"/>
</atom>
<atom>
<atomProperties index="172" x="-28.76" y="-26.88" z="-37.29"/>
</atom>
<atom>
<atomProperties index="91" x="-28.76" y="-26.88" z="-47.31"/>
</atom>
<atom>
<atomProperties index="586" x="-30.16" y="-18.75" z="12.43"/>
</atom>
<atom>
<atomProperties index="505" x="-29.18" y="-17.45" z="3.31"/>
</atom>
<atom>
<atomProperties index="424" x="-28.95" y="-17.5" z="-6.68"/>
</atom>
<atom>
<atomProperties index="343" x="-29.01" y="-17.4" z="-16.88"/>
</atom>
<atom>
<atomProperties index="262" x="-29.14" y="-16.88" z="-26.77"/>
</atom>
<atom>
<atomProperties index="181" x="-28.72" y="-17.45" z="-37.14"/>
</atom>
<atom>
<atomProperties index="100" x="-28.72" y="-17.33" z="-47.28"/>
</atom>
<atom>
<atomProperties index="595" x="-30.76" y="-8.13" z="12.29"/>
</atom>
<atom>
<atomProperties index="514" x="-29.22" y="-6.88" z="3.07"/>
</atom>
<atom>
<atomProperties index="433" x="-29.13" y="-6.88" z="-6.87"/>
</atom>
<atom>
<atomProperties index="352" x="-29.79" y="-7.58" z="-17.02"/>
</atom>
<atom>
<atomProperties index="271" x="-28.95" y="-6.88" z="-26.91"/>
</atom>
<atom>
<atomProperties index="190" x="-28.76" y="-6.88" z="-37.29"/>
</atom>
<atom>
<atomProperties index="109" x="-28.93" y="-7.49" z="-47.15"/>
</atom>
<atom>
<atomProperties index="604" x="-30.05" y="1.33" z="12.27"/>
</atom>
<atom>
<atomProperties index="523" x="-29.5" y="3.13" z="3.15"/>
</atom>
<atom>
<atomProperties index="442" x="-29.5" y="3.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="361" x="-29.1" y="3.13" z="-17.22"/>
</atom>
<atom>
<atomProperties index="280" x="-29.1" y="3.13" z="-26.94"/>
</atom>
<atom>
<atomProperties index="199" x="-29.13" y="3.13" z="-37.29"/>
</atom>
<atom>
<atomProperties index="118" x="-28.87" y="3.13" z="-47.5"/>
</atom>
<atom>
<atomProperties index="613" x="-30.27" y="12.52" z="12.08"/>
</atom>
<atom>
<atomProperties index="532" x="-29.74" y="13.13" z="3"/>
</atom>
<atom>
<atomProperties index="451" x="-29.35" y="13.13" z="-6.91"/>
</atom>
<atom>
<atomProperties index="370" x="-30.62" y="13.13" z="-17.26"/>
</atom>
<atom>
<atomProperties index="289" x="-29.14" y="13.13" z="-27.09"/>
</atom>
<atom>
<atomProperties index="208" x="-29.08" y="12.43" z="-37.47"/>
</atom>
<atom>
<atomProperties index="127" x="-28.82" y="12.57" z="-47.49"/>
</atom>
<atom>
<atomProperties index="622" x="-30.69" y="23.83" z="12.29"/>
</atom>
<atom>
<atomProperties index="541" x="-30.11" y="23.12" z="3"/>
</atom>
<atom>
<atomProperties index="460" x="-30.08" y="22.49" z="-7.03"/>
</atom>
<atom>
<atomProperties index="379" x="-30.24" y="23.12" z="-17.26"/>
</atom>
<atom>
<atomProperties index="298" x="-29.32" y="22.63" z="-27.28"/>
</atom>
<atom>
<atomProperties index="217" x="-29.13" y="23.12" z="-37.67"/>
</atom>
<atom>
<atomProperties index="136" x="-28.72" y="22.67" z="-47.65"/>
</atom>
<atom>
<atomProperties index="631" x="-30.71" y="34.08" z="12.22"/>
</atom>
<atom>
<atomProperties index="550" x="-29.89" y="33.63" z="3"/>
</atom>
<atom>
<atomProperties index="469" x="-29.99" y="33.13" z="-7.05"/>
</atom>
<atom>
<atomProperties index="388" x="-29.53" y="33.13" z="-17.22"/>
</atom>
<atom>
<atomProperties index="307" x="-29.5" y="33.13" z="-27.28"/>
</atom>
<atom>
<atomProperties index="226" x="-29.5" y="33.13" z="-37.67"/>
</atom>
<atom>
<atomProperties index="145" x="-29.32" y="33.13" z="-47.69"/>
</atom>
<atom>
<atomProperties index="578" x="-19.25" y="-29.37" z="12.58"/>
</atom>
<atom>
<atomProperties index="497" x="-19.26" y="-27.48" z="3.37"/>
</atom>
<atom>
<atomProperties index="416" x="-17.81" y="-26.88" z="-6.49"/>
</atom>
<atom>
<atomProperties index="335" x="-18.93" y="-26.88" z="-16.89"/>
</atom>
<atom>
<atomProperties index="254" x="-18.55" y="-26.88" z="-26.91"/>
</atom>
<atom>
<atomProperties index="173" x="-18.77" y="-26.88" z="-36.96"/>
</atom>
<atom>
<atomProperties index="92" x="-18.55" y="-26.88" z="-47.13"/>
</atom>
<atom>
<atomProperties index="587" x="-20.04" y="-19.37" z="12.43"/>
</atom>
<atom>
<atomProperties index="506" x="-19.2" y="-17.62" z="3.24"/>
</atom>
<atom>
<atomProperties index="425" x="-18.62" y="-16.88" z="-6.69"/>
</atom>
<atom>
<atomProperties index="344" x="-18.77" y="-18.12" z="-16.85"/>
</atom>
<atom>
<atomProperties index="263" x="-18.74" y="-16.88" z="-26.9"/>
</atom>
<atom>
<atomProperties index="182" x="-18.74" y="-16.88" z="-37.29"/>
</atom>
<atom>
<atomProperties index="101" x="-18.74" y="-16.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="596" x="-20.07" y="-10.05" z="12.38"/>
</atom>
<atom>
<atomProperties index="515" x="-19.2" y="-6.88" z="3.24"/>
</atom>
<atom>
<atomProperties index="434" x="-19.11" y="-6.88" z="-6.87"/>
</atom>
<atom>
<atomProperties index="353" x="-19.14" y="-6.88" z="-16.92"/>
</atom>
<atom>
<atomProperties index="272" x="-18.74" y="-6.88" z="-26.9"/>
</atom>
<atom>
<atomProperties index="191" x="-18.93" y="-6.88" z="-37.3"/>
</atom>
<atom>
<atomProperties index="110" x="-18.73" y="-7.38" z="-47.16"/>
</atom>
<atom>
<atomProperties index="605" x="-20.78" y="0.98" z="12.18"/>
</atom>
<atom>
<atomProperties index="524" x="-19.52" y="3.13" z="3"/>
</atom>
<atom>
<atomProperties index="443" x="-18.77" y="3.13" z="-6.83"/>
</atom>
<atom>
<atomProperties index="362" x="-19.23" y="3.13" z="-17.07"/>
</atom>
<atom>
<atomProperties index="281" x="-18.77" y="3.13" z="-26.94"/>
</atom>
<atom>
<atomProperties index="200" x="-19.11" y="3.13" z="-37.29"/>
</atom>
<atom>
<atomProperties index="119" x="-19.06" y="3.59" z="-47.27"/>
</atom>
<atom>
<atomProperties index="614" x="-20.18" y="11.88" z="12.38"/>
</atom>
<atom>
<atomProperties index="533" x="-19.85" y="13.13" z="3.15"/>
</atom>
<atom>
<atomProperties index="452" x="-19.11" y="13.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="371" x="-19.64" y="13.13" z="-17.1"/>
</atom>
<atom>
<atomProperties index="290" x="-19.11" y="13.13" z="-26.9"/>
</atom>
<atom>
<atomProperties index="209" x="-19.11" y="13.13" z="-37.29"/>
</atom>
<atom>
<atomProperties index="128" x="-18.74" y="13.13" z="-47.31"/>
</atom>
<atom>
<atomProperties index="623" x="-21.15" y="23.42" z="12.14"/>
</atom>
<atom>
<atomProperties index="542" x="-19.67" y="23.12" z="2.97"/>
</atom>
<atom>
<atomProperties index="461" x="-19.49" y="23.12" z="-7.05"/>
</atom>
<atom>
<atomProperties index="380" x="-18.37" y="23.12" z="-17.26"/>
</atom>
<atom>
<atomProperties index="299" x="-19.09" y="23.12" z="-27.24"/>
</atom>
<atom>
<atomProperties index="218" x="-19.09" y="23.12" z="-37.63"/>
</atom>
<atom>
<atomProperties index="137" x="-18.93" y="22.5" z="-47.5"/>
</atom>
<atom>
<atomProperties index="632" x="-20.73" y="33.58" z="12.06"/>
</atom>
<atom>
<atomProperties index="551" x="-19.82" y="33.13" z="3.12"/>
</atom>
<atom>
<atomProperties index="470" x="-19.69" y="33.74" z="-7.03"/>
</atom>
<atom>
<atomProperties index="389" x="-19.11" y="33.13" z="-17.26"/>
</atom>
<atom>
<atomProperties index="308" x="-19.46" y="33.13" z="-27.24"/>
</atom>
<atom>
<atomProperties index="227" x="-19.46" y="33.13" z="-37.63"/>
</atom>
<atom>
<atomProperties index="146" x="-19.11" y="33.13" z="-47.69"/>
</atom>
<atom>
<atomProperties index="579" x="-8.61" y="-28.92" z="12.85"/>
</atom>
<atom>
<atomProperties index="498" x="-8.78" y="-26.88" z="3.58"/>
</atom>
<atom>
<atomProperties index="417" x="-8.54" y="-26.88" z="-6.61"/>
</atom>
<atom>
<atomProperties index="336" x="-8.73" y="-26.88" z="-16.7"/>
</atom>
<atom>
<atomProperties index="255" x="-8.35" y="-26.88" z="-26.9"/>
</atom>
<atom>
<atomProperties index="174" x="-8.57" y="-26.88" z="-37.09"/>
</atom>
<atom>
<atomProperties index="93" x="-8.72" y="-26.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="588" x="-8.91" y="-19.96" z="12.66"/>
</atom>
<atom>
<atomProperties index="507" x="-9.12" y="-16.88" z="3.19"/>
</atom>
<atom>
<atomProperties index="426" x="-8.69" y="-16.88" z="-6.53"/>
</atom>
<atom>
<atomProperties index="345" x="-8.93" y="-17.49" z="-16.72"/>
</atom>
<atom>
<atomProperties index="264" x="-8.69" y="-16.88" z="-26.87"/>
</atom>
<atom>
<atomProperties index="183" x="-8.73" y="-16.88" z="-37.11"/>
</atom>
<atom>
<atomProperties index="102" x="-8.72" y="-16.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="597" x="-8.58" y="-8.98" z="12.6"/>
</atom>
<atom>
<atomProperties index="516" x="-9.09" y="-6.88" z="3.15"/>
</atom>
<atom>
<atomProperties index="435" x="-8.98" y="-6.88" z="-6.69"/>
</atom>
<atom>
<atomProperties index="354" x="-9.07" y="-6.88" z="-16.85"/>
</atom>
<atom>
<atomProperties index="273" x="-8.91" y="-6.88" z="-26.91"/>
</atom>
<atom>
<atomProperties index="192" x="-9.07" y="-6.88" z="-37.26"/>
</atom>
<atom>
<atomProperties index="111" x="-8.68" y="-7.45" z="-47.1"/>
</atom>
<atom>
<atomProperties index="606" x="-10.39" y="1.88" z="12.43"/>
</atom>
<atom>
<atomProperties index="525" x="-9.31" y="3.13" z="3.11"/>
</atom>
<atom>
<atomProperties index="444" x="-10.17" y="4.38" z="-6.9"/>
</atom>
<atom>
<atomProperties index="363" x="-9.09" y="3.13" z="-16.88"/>
</atom>
<atom>
<atomProperties index="282" x="-8.69" y="3.13" z="-26.94"/>
</atom>
<atom>
<atomProperties index="201" x="-9.09" y="3.13" z="-37.29"/>
</atom>
<atom>
<atomProperties index="120" x="-8.87" y="3.13" z="-47.17"/>
</atom>
<atom>
<atomProperties index="615" x="-10.03" y="12.65" z="12.27"/>
</atom>
<atom>
<atomProperties index="534" x="-9.46" y="13.13" z="3.15"/>
</atom>
<atom>
<atomProperties index="453" x="-9.09" y="13.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="372" x="-9.44" y="13.13" z="-17.22"/>
</atom>
<atom>
<atomProperties index="291" x="-9.46" y="13.88" z="-27.08"/>
</atom>
<atom>
<atomProperties index="210" x="-9.09" y="13.13" z="-37.29"/>
</atom>
<atom>
<atomProperties index="129" x="-8.72" y="13.13" z="-47.31"/>
</atom>
<atom>
<atomProperties index="624" x="-10.44" y="22.78" z="12.12"/>
</atom>
<atom>
<atomProperties index="543" x="-9.44" y="23.12" z="2.82"/>
</atom>
<atom>
<atomProperties index="462" x="-9.12" y="23.12" z="-6.9"/>
</atom>
<atom>
<atomProperties index="381" x="-9.46" y="23.12" z="-17.26"/>
</atom>
<atom>
<atomProperties index="300" x="-9.12" y="23.12" z="-27.24"/>
</atom>
<atom>
<atomProperties index="219" x="-9.07" y="23.12" z="-37.63"/>
</atom>
<atom>
<atomProperties index="138" x="-9.09" y="23.12" z="-47.31"/>
</atom>
<atom>
<atomProperties index="633" x="-11.04" y="33.8" z="12.08"/>
</atom>
<atom>
<atomProperties index="552" x="-9.47" y="33.13" z="2.97"/>
</atom>
<atom>
<atomProperties index="471" x="-9.28" y="34.38" z="-6.87"/>
</atom>
<atom>
<atomProperties index="390" x="-9.46" y="33.13" z="-17.26"/>
</atom>
<atom>
<atomProperties index="309" x="-9.09" y="33.13" z="-27.28"/>
</atom>
<atom>
<atomProperties index="228" x="-8.72" y="33.13" z="-37.67"/>
</atom>
<atom>
<atomProperties index="147" x="-9.42" y="33.13" z="-47.53"/>
</atom>
<atom>
<atomProperties index="580" x="2.3" y="-28.71" z="12.63"/>
</atom>
<atom>
<atomProperties index="499" x="0.78" y="-26.88" z="3.48"/>
</atom>
<atom>
<atomProperties index="418" x="2.04" y="-26.88" z="-6.49"/>
</atom>
<atom>
<atomProperties index="337" x="1.31" y="-27.37" z="-16.65"/>
</atom>
<atom>
<atomProperties index="256" x="1.64" y="-26.88" z="-26.57"/>
</atom>
<atom>
<atomProperties index="175" x="1.48" y="-26.88" z="-36.93"/>
</atom>
<atom>
<atomProperties index="94" x="1.48" y="-26.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="589" x="2.04" y="-20" z="12.62"/>
</atom>
<atom>
<atomProperties index="508" x="0.96" y="-16.88" z="3.49"/>
</atom>
<atom>
<atomProperties index="427" x="2.41" y="-16.88" z="-6.49"/>
</atom>
<atom>
<atomProperties index="346" x="0.95" y="-16.88" z="-16.85"/>
</atom>
<atom>
<atomProperties index="265" x="1.64" y="-16.88" z="-26.87"/>
</atom>
<atom>
<atomProperties index="184" x="1.33" y="-16.88" z="-36.95"/>
</atom>
<atom>
<atomProperties index="103" x="0.93" y="-16.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="598" x="1.3" y="-9.38" z="12.43"/>
</atom>
<atom>
<atomProperties index="517" x="1.04" y="-6.88" z="3.33"/>
</atom>
<atom>
<atomProperties index="436" x="2.01" y="-6.88" z="-6.83"/>
</atom>
<atom>
<atomProperties index="355" x="0.58" y="-6.88" z="-16.85"/>
</atom>
<atom>
<atomProperties index="274" x="1.3" y="-6.88" z="-26.9"/>
</atom>
<atom>
<atomProperties index="193" x="1.64" y="-6.88" z="-36.96"/>
</atom>
<atom>
<atomProperties index="112" x="1.3" y="-6.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="607" x="1.39" y="1.2" z="12.46"/>
</atom>
<atom>
<atomProperties index="526" x="1.15" y="3.13" z="3.19"/>
</atom>
<atom>
<atomProperties index="445" x="1.33" y="3.13" z="-6.83"/>
</atom>
<atom>
<atomProperties index="364" x="0.56" y="3.13" z="-16.88"/>
</atom>
<atom>
<atomProperties index="283" x="1.3" y="3.13" z="-26.9"/>
</atom>
<atom>
<atomProperties index="202" x="1.27" y="3.13" z="-37.26"/>
</atom>
<atom>
<atomProperties index="121" x="1.26" y="3.13" z="-47.16"/>
</atom>
<atom>
<atomProperties index="616" x="1.17" y="11.88" z="12.28"/>
</atom>
<atom>
<atomProperties index="535" x="0.93" y="13.13" z="3.15"/>
</atom>
<atom>
<atomProperties index="454" x="1.48" y="13.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="373" x="1.27" y="13.13" z="-16.92"/>
</atom>
<atom>
<atomProperties index="292" x="0.96" y="13.13" z="-26.94"/>
</atom>
<atom>
<atomProperties index="211" x="1.11" y="13.13" z="-37.3"/>
</atom>
<atom>
<atomProperties index="130" x="1.06" y="13.13" z="-47.16"/>
</atom>
<atom>
<atomProperties index="625" x="0.74" y="23.12" z="12.25"/>
</atom>
<atom>
<atomProperties index="544" x="0.73" y="23.12" z="3.04"/>
</atom>
<atom>
<atomProperties index="463" x="1.3" y="23.12" z="-6.87"/>
</atom>
<atom>
<atomProperties index="382" x="0.9" y="23.12" z="-16.92"/>
</atom>
<atom>
<atomProperties index="301" x="0.92" y="23.12" z="-27.09"/>
</atom>
<atom>
<atomProperties index="220" x="0.96" y="23.12" z="-37.33"/>
</atom>
<atom>
<atomProperties index="139" x="0.93" y="23.12" z="-47.31"/>
</atom>
<atom>
<atomProperties index="634" x="-0.02" y="33.86" z="12.07"/>
</atom>
<atom>
<atomProperties index="553" x="0.55" y="33.62" z="2.98"/>
</atom>
<atom>
<atomProperties index="472" x="0.55" y="33.13" z="-7.05"/>
</atom>
<atom>
<atomProperties index="391" x="0.93" y="33.13" z="-17.26"/>
</atom>
<atom>
<atomProperties index="310" x="0.9" y="33.13" z="-27.24"/>
</atom>
<atom>
<atomProperties index="229" x="0.92" y="33.13" z="-37.48"/>
</atom>
<atom>
<atomProperties index="148" x="0.93" y="33.13" z="-47.31"/>
</atom>
<atom>
<atomProperties index="581" x="12.29" y="-28.62" z="12.83"/>
</atom>
<atom>
<atomProperties index="500" x="11.69" y="-26.88" z="3.53"/>
</atom>
<atom>
<atomProperties index="419" x="11.32" y="-26.88" z="-6.49"/>
</atom>
<atom>
<atomProperties index="338" x="11.32" y="-26.88" z="-16.51"/>
</atom>
<atom>
<atomProperties index="257" x="11.5" y="-26.88" z="-26.54"/>
</atom>
<atom>
<atomProperties index="176" x="11.69" y="-26.88" z="-36.92"/>
</atom>
<atom>
<atomProperties index="95" x="11.69" y="-26.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="590" x="12.8" y="-19.37" z="12.8"/>
</atom>
<atom>
<atomProperties index="509" x="12.03" y="-16.88" z="3.49"/>
</atom>
<atom>
<atomProperties index="428" x="12.43" y="-16.88" z="-6.49"/>
</atom>
<atom>
<atomProperties index="347" x="12.03" y="-16.88" z="-16.55"/>
</atom>
<atom>
<atomProperties index="266" x="11.66" y="-16.88" z="-26.57"/>
</atom>
<atom>
<atomProperties index="185" x="11.69" y="-16.88" z="-36.92"/>
</atom>
<atom>
<atomProperties index="104" x="11.66" y="-16.88" z="-46.91"/>
</atom>
<atom>
<atomProperties index="599" x="12.76" y="-8.61" z="12.78"/>
</atom>
<atom>
<atomProperties index="518" x="10.99" y="-6.88" z="3.37"/>
</atom>
<atom>
<atomProperties index="437" x="11.69" y="-6.88" z="-6.49"/>
</atom>
<atom>
<atomProperties index="356" x="11.29" y="-6.88" z="-16.85"/>
</atom>
<atom>
<atomProperties index="275" x="11.29" y="-6.88" z="-26.87"/>
</atom>
<atom>
<atomProperties index="194" x="10.98" y="-6.88" z="-36.95"/>
</atom>
<atom>
<atomProperties index="113" x="11.5" y="-6.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="608" x="11.91" y="1.88" z="12.39"/>
</atom>
<atom>
<atomProperties index="527" x="10.95" y="3.13" z="3.15"/>
</atom>
<atom>
<atomProperties index="446" x="11.69" y="3.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="365" x="11.32" y="3.13" z="-16.88"/>
</atom>
<atom>
<atomProperties index="284" x="11.32" y="3.13" z="-26.9"/>
</atom>
<atom>
<atomProperties index="203" x="11.34" y="3.13" z="-37.26"/>
</atom>
<atom>
<atomProperties index="122" x="11.08" y="3.13" z="-47.16"/>
</atom>
<atom>
<atomProperties index="617" x="11.91" y="11.88" z="12.39"/>
</atom>
<atom>
<atomProperties index="536" x="10.8" y="13.13" z="3.19"/>
</atom>
<atom>
<atomProperties index="455" x="11.69" y="13.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="374" x="11.17" y="13.85" z="-16.92"/>
</atom>
<atom>
<atomProperties index="293" x="10.95" y="13.13" z="-26.9"/>
</atom>
<atom>
<atomProperties index="212" x="10.6" y="13.13" z="-37.26"/>
</atom>
<atom>
<atomProperties index="131" x="11.5" y="13.13" z="-47.13"/>
</atom>
<atom>
<atomProperties index="626" x="11.32" y="23.12" z="12.06"/>
</atom>
<atom>
<atomProperties index="545" x="11.23" y="23.13" z="2.87"/>
</atom>
<atom>
<atomProperties index="464" x="10.98" y="23.12" z="-6.9"/>
</atom>
<atom>
<atomProperties index="383" x="10.57" y="23.12" z="-17.07"/>
</atom>
<atom>
<atomProperties index="302" x="10.98" y="23.12" z="-26.94"/>
</atom>
<atom>
<atomProperties index="221" x="10.38" y="23.12" z="-37.41"/>
</atom>
<atom>
<atomProperties index="140" x="10.58" y="23.12" z="-47.31"/>
</atom>
<atom>
<atomProperties index="635" x="10.61" y="33.56" z="12.06"/>
</atom>
<atom>
<atomProperties index="554" x="10.83" y="33.13" z="2.97"/>
</atom>
<atom>
<atomProperties index="473" x="10.95" y="33.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="392" x="10.6" y="33.13" z="-17.22"/>
</atom>
<atom>
<atomProperties index="311" x="10.94" y="33.13" z="-27.09"/>
</atom>
<atom>
<atomProperties index="230" x="10.61" y="33.13" z="-37.33"/>
</atom>
<atom>
<atomProperties index="149" x="11.32" y="33.13" z="-47.31"/>
</atom>
<atom>
<atomProperties index="582" x="23.14" y="-28.59" z="12.76"/>
</atom>
<atom>
<atomProperties index="501" x="21.52" y="-26.88" z="3.64"/>
</atom>
<atom>
<atomProperties index="420" x="22.27" y="-26.88" z="-6.31"/>
</atom>
<atom>
<atomProperties index="339" x="21.71" y="-26.88" z="-16.51"/>
</atom>
<atom>
<atomProperties index="258" x="21.74" y="-26.88" z="-26.56"/>
</atom>
<atom>
<atomProperties index="177" x="21.59" y="-26.88" z="-36.74"/>
</atom>
<atom>
<atomProperties index="96" x="21.52" y="-26.88" z="-46.76"/>
</atom>
<atom>
<atomProperties index="591" x="23.41" y="-19.96" z="12.76"/>
</atom>
<atom>
<atomProperties index="510" x="21.12" y="-17.45" z="3.48"/>
</atom>
<atom>
<atomProperties index="429" x="22.43" y="-17.31" z="-6.46"/>
</atom>
<atom>
<atomProperties index="348" x="21.37" y="-16.88" z="-16.54"/>
</atom>
<atom>
<atomProperties index="267" x="21.68" y="-16.88" z="-26.57"/>
</atom>
<atom>
<atomProperties index="186" x="21.71" y="-16.88" z="-36.92"/>
</atom>
<atom>
<atomProperties index="105" x="21.68" y="-16.88" z="-46.91"/>
</atom>
<atom>
<atomProperties index="600" x="23.16" y="-8.58" z="12.74"/>
</atom>
<atom>
<atomProperties index="519" x="21.56" y="-6.88" z="3.48"/>
</atom>
<atom>
<atomProperties index="438" x="21.87" y="-6.23" z="-6.5"/>
</atom>
<atom>
<atomProperties index="357" x="21.08" y="-6.88" z="-16.71"/>
</atom>
<atom>
<atomProperties index="276" x="21.37" y="-6.88" z="-26.56"/>
</atom>
<atom>
<atomProperties index="195" x="21.37" y="-6.88" z="-36.95"/>
</atom>
<atom>
<atomProperties index="114" x="21.34" y="-6.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="609" x="22.45" y="1.88" z="12.43"/>
</atom>
<atom>
<atomProperties index="528" x="21.31" y="3.13" z="3.19"/>
</atom>
<atom>
<atomProperties index="447" x="21.79" y="3.13" z="-6.58"/>
</atom>
<atom>
<atomProperties index="366" x="21.34" y="3.13" z="-16.88"/>
</atom>
<atom>
<atomProperties index="285" x="21.52" y="3.13" z="-26.91"/>
</atom>
<atom>
<atomProperties index="204" x="21.15" y="3.13" z="-37.11"/>
</atom>
<atom>
<atomProperties index="123" x="21.34" y="3.13" z="-46.94"/>
</atom>
<atom>
<atomProperties index="618" x="22.09" y="12.48" z="12.27"/>
</atom>
<atom>
<atomProperties index="537" x="21.31" y="13.13" z="3.19"/>
</atom>
<atom>
<atomProperties index="456" x="22.27" y="13.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="375" x="20.97" y="13.13" z="-16.88"/>
</atom>
<atom>
<atomProperties index="294" x="21.35" y="13.59" z="-26.91"/>
</atom>
<atom>
<atomProperties index="213" x="21.36" y="13.13" z="-37.26"/>
</atom>
<atom>
<atomProperties index="132" x="21.52" y="13.13" z="-47.13"/>
</atom>
<atom>
<atomProperties index="627" x="22.21" y="23.12" z="12.28"/>
</atom>
<atom>
<atomProperties index="546" x="20.94" y="23.93" z="3.11"/>
</atom>
<atom>
<atomProperties index="465" x="21.71" y="23.12" z="-6.87"/>
</atom>
<atom>
<atomProperties index="384" x="20.63" y="23.12" z="-16.92"/>
</atom>
<atom>
<atomProperties index="303" x="21.15" y="23.75" z="-27.09"/>
</atom>
<atom>
<atomProperties index="222" x="21.34" y="23.12" z="-37.29"/>
</atom>
<atom>
<atomProperties index="141" x="21.12" y="23.12" z="-47.27"/>
</atom>
<atom>
<atomProperties index="636" x="21.71" y="33.95" z="12.05"/>
</atom>
<atom>
<atomProperties index="555" x="20.78" y="33.53" z="2.99"/>
</atom>
<atom>
<atomProperties index="474" x="21.71" y="33.13" z="-6.87"/>
</atom>
<atom>
<atomProperties index="393" x="20.56" y="33.13" z="-17.04"/>
</atom>
<atom>
<atomProperties index="312" x="21.33" y="33.62" z="-27.1"/>
</atom>
<atom>
<atomProperties index="231" x="20.97" y="33.13" z="-37.29"/>
</atom>
<atom>
<atomProperties index="150" x="20.97" y="33.13" z="-47.31"/>
</atom>
<atom>
<atomProperties index="583" x="32.47" y="-28.12" z="12.8"/>
</atom>
<atom>
<atomProperties index="502" x="31.77" y="-26.88" z="3.75"/>
</atom>
<atom>
<atomProperties index="421" x="32.22" y="-26.88" z="-6.32"/>
</atom>
<atom>
<atomProperties index="340" x="31.73" y="-26.88" z="-16.51"/>
</atom>
<atom>
<atomProperties index="259" x="32.29" y="-26.88" z="-26.54"/>
</atom>
<atom>
<atomProperties index="178" x="31.7" y="-26.88" z="-36.89"/>
</atom>
<atom>
<atomProperties index="97" x="31.61" y="-26.88" z="-46.76"/>
</atom>
<atom>
<atomProperties index="592" x="32.69" y="-19" z="12.77"/>
</atom>
<atom>
<atomProperties index="511" x="31.36" y="-16.88" z="3.71"/>
</atom>
<atom>
<atomProperties index="430" x="32.59" y="-16.88" z="-6.32"/>
</atom>
<atom>
<atomProperties index="349" x="31.36" y="-16.88" z="-16.51"/>
</atom>
<atom>
<atomProperties index="268" x="32.1" y="-16.88" z="-26.53"/>
</atom>
<atom>
<atomProperties index="187" x="31.73" y="-16.88" z="-36.92"/>
</atom>
<atom>
<atomProperties index="106" x="31.54" y="-16.88" z="-46.83"/>
</atom>
<atom>
<atomProperties index="601" x="32.46" y="-7.7" z="12.8"/>
</atom>
<atom>
<atomProperties index="520" x="31.73" y="-6.88" z="3.53"/>
</atom>
<atom>
<atomProperties index="439" x="32.28" y="-6.32" z="-6.43"/>
</atom>
<atom>
<atomProperties index="358" x="31.73" y="-6.88" z="-16.51"/>
</atom>
<atom>
<atomProperties index="277" x="31.67" y="-6.32" z="-26.72"/>
</atom>
<atom>
<atomProperties index="196" x="31.54" y="-6.88" z="-36.92"/>
</atom>
<atom>
<atomProperties index="115" x="31.36" y="-6.88" z="-46.94"/>
</atom>
<atom>
<atomProperties index="610" x="32.88" y="3.13" z="12.65"/>
</atom>
<atom>
<atomProperties index="529" x="31.32" y="3.57" z="3.49"/>
</atom>
<atom>
<atomProperties index="448" x="32.1" y="3.13" z="-6.49"/>
</atom>
<atom>
<atomProperties index="367" x="31.33" y="3.13" z="-16.85"/>
</atom>
<atom>
<atomProperties index="286" x="31.34" y="3.94" z="-26.87"/>
</atom>
<atom>
<atomProperties index="205" x="31.69" y="3.13" z="-37.07"/>
</atom>
<atom>
<atomProperties index="124" x="30.99" y="3.13" z="-46.94"/>
</atom>
<atom>
<atomProperties index="619" x="32.35" y="12.74" z="12.59"/>
</atom>
<atom>
<atomProperties index="538" x="31.17" y="13.13" z="3.34"/>
</atom>
<atom>
<atomProperties index="457" x="32.13" y="14.37" z="-6.53"/>
</atom>
<atom>
<atomProperties index="376" x="30.99" y="13.13" z="-16.88"/>
</atom>
<atom>
<atomProperties index="295" x="31.7" y="13.65" z="-26.83"/>
</atom>
<atom>
<atomProperties index="214" x="31.36" y="13.13" z="-37.11"/>
</atom>
<atom>
<atomProperties index="133" x="31.24" y="13.13" z="-47.13"/>
</atom>
<atom>
<atomProperties index="628" x="31.88" y="23.12" z="12.47"/>
</atom>
<atom>
<atomProperties index="547" x="30.96" y="23.12" z="3.19"/>
</atom>
<atom>
<atomProperties index="466" x="31.91" y="23.62" z="-6.87"/>
</atom>
<atom>
<atomProperties index="385" x="31.36" y="23.12" z="-16.88"/>
</atom>
<atom>
<atomProperties index="304" x="31.32" y="23.57" z="-26.94"/>
</atom>
<atom>
<atomProperties index="223" x="31.36" y="23.12" z="-37.29"/>
</atom>
<atom>
<atomProperties index="142" x="31.34" y="22.66" z="-47.26"/>
</atom>
<atom>
<atomProperties index="637" x="32.1" y="34.38" z="12.25"/>
</atom>
<atom>
<atomProperties index="556" x="30.8" y="33.13" z="3.15"/>
</atom>
<atom>
<atomProperties index="475" x="31.73" y="34.38" z="-6.87"/>
</atom>
<atom>
<atomProperties index="394" x="30.58" y="33.13" z="-16.92"/>
</atom>
<atom>
<atomProperties index="313" x="31.24" y="33.61" z="-27.17"/>
</atom>
<atom>
<atomProperties index="232" x="31.21" y="33.13" z="-37.34"/>
</atom>
<atom>
<atomProperties index="151" x="31.14" y="33.13" z="-47.36"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Exclusive Components ">
<multiComponent name="Enclosed Volumes">
<structuralComponent name="volume #0" mode="WIREFRAME_AND_SURFACE" incompressibility="true">
<nrOfStructures value="768"/>
<cell>
<cellProperties index="0" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="9"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="1" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="2" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="10"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="3" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="4" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="11"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="5" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="6" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="12"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="7" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="8" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="13"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="9" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="10" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="14"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="11" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="12" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="15"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="13" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="14" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="16"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="15" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="16" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="18"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="17" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="18" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="19"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="19" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="20" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="11"/>
<atomRef index="20"/>
<atomRef index="12"/>
</cell>
<cell>
<cellProperties index="21" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="12"/>
</cell>
<cell>
<cellProperties index="22" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="12"/>
<atomRef index="21"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="23" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="24" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="13"/>
<atomRef index="22"/>
<atomRef index="14"/>
</cell>
<cell>
<cellProperties index="25" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="14"/>
</cell>
<cell>
<cellProperties index="26" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="14"/>
<atomRef index="23"/>
<atomRef index="15"/>
</cell>
<cell>
<cellProperties index="27" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="15"/>
</cell>
<cell>
<cellProperties index="28" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="15"/>
<atomRef index="24"/>
<atomRef index="16"/>
</cell>
<cell>
<cellProperties index="29" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="16"/>
</cell>
<cell>
<cellProperties index="30" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="16"/>
<atomRef index="25"/>
<atomRef index="17"/>
</cell>
<cell>
<cellProperties index="31" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="17"/>
</cell>
<cell>
<cellProperties index="32" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="18"/>
<atomRef index="27"/>
<atomRef index="19"/>
</cell>
<cell>
<cellProperties index="33" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="19"/>
</cell>
<cell>
<cellProperties index="34" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="19"/>
<atomRef index="28"/>
<atomRef index="20"/>
</cell>
<cell>
<cellProperties index="35" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="20"/>
</cell>
<cell>
<cellProperties index="36" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="20"/>
<atomRef index="29"/>
<atomRef index="21"/>
</cell>
<cell>
<cellProperties index="37" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="21"/>
</cell>
<cell>
<cellProperties index="38" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="21"/>
<atomRef index="30"/>
<atomRef index="22"/>
</cell>
<cell>
<cellProperties index="39" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="22"/>
</cell>
<cell>
<cellProperties index="40" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="22"/>
<atomRef index="31"/>
<atomRef index="23"/>
</cell>
<cell>
<cellProperties index="41" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="23"/>
</cell>
<cell>
<cellProperties index="42" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="23"/>
<atomRef index="32"/>
<atomRef index="24"/>
</cell>
<cell>
<cellProperties index="43" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="24"/>
</cell>
<cell>
<cellProperties index="44" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="24"/>
<atomRef index="33"/>
<atomRef index="25"/>
</cell>
<cell>
<cellProperties index="45" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="25"/>
</cell>
<cell>
<cellProperties index="46" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="25"/>
<atomRef index="34"/>
<atomRef index="26"/>
</cell>
<cell>
<cellProperties index="47" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="26"/>
</cell>
<cell>
<cellProperties index="48" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="27"/>
<atomRef index="36"/>
<atomRef index="28"/>
</cell>
<cell>
<cellProperties index="49" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="28"/>
</cell>
<cell>
<cellProperties index="50" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="28"/>
<atomRef index="37"/>
<atomRef index="29"/>
</cell>
<cell>
<cellProperties index="51" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="29"/>
</cell>
<cell>
<cellProperties index="52" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="38"/>
<atomRef index="30"/>
</cell>
<cell>
<cellProperties index="53" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="30"/>
</cell>
<cell>
<cellProperties index="54" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="30"/>
<atomRef index="39"/>
<atomRef index="31"/>
</cell>
<cell>
<cellProperties index="55" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="31"/>
</cell>
<cell>
<cellProperties index="56" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="40"/>
<atomRef index="32"/>
</cell>
<cell>
<cellProperties index="57" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="32"/>
</cell>
<cell>
<cellProperties index="58" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="32"/>
<atomRef index="41"/>
<atomRef index="33"/>
</cell>
<cell>
<cellProperties index="59" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="33"/>
</cell>
<cell>
<cellProperties index="60" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="42"/>
<atomRef index="34"/>
</cell>
<cell>
<cellProperties index="61" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="34"/>
</cell>
<cell>
<cellProperties index="62" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="34"/>
<atomRef index="43"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="63" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="64" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="36"/>
<atomRef index="45"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="65" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="66" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="37"/>
<atomRef index="46"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="67" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="68" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="38"/>
<atomRef index="47"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="69" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="70" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="39"/>
<atomRef index="48"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="71" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="72" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="40"/>
<atomRef index="49"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="73" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="74" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="41"/>
<atomRef index="50"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="75" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="76" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="42"/>
<atomRef index="51"/>
<atomRef index="43"/>
</cell>
<cell>
<cellProperties index="77" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="43"/>
</cell>
<cell>
<cellProperties index="78" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="43"/>
<atomRef index="52"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="79" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="80" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="45"/>
<atomRef index="54"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="81" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="82" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="46"/>
<atomRef index="55"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="83" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="84" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="47"/>
<atomRef index="56"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="85" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="86" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="48"/>
<atomRef index="57"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="87" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="88" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="49"/>
<atomRef index="58"/>
<atomRef index="50"/>
</cell>
<cell>
<cellProperties index="89" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="50"/>
</cell>
<cell>
<cellProperties index="90" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="50"/>
<atomRef index="59"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="91" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="92" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="51"/>
<atomRef index="60"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="93" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="94" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="52"/>
<atomRef index="61"/>
<atomRef index="53"/>
</cell>
<cell>
<cellProperties index="95" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="53"/>
</cell>
<cell>
<cellProperties index="96" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="54"/>
<atomRef index="63"/>
<atomRef index="55"/>
</cell>
<cell>
<cellProperties index="97" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="55"/>
</cell>
<cell>
<cellProperties index="98" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="55"/>
<atomRef index="64"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="99" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="100" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="56"/>
<atomRef index="65"/>
<atomRef index="57"/>
</cell>
<cell>
<cellProperties index="101" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="57"/>
</cell>
<cell>
<cellProperties index="102" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="57"/>
<atomRef index="66"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="103" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="104" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="58"/>
<atomRef index="67"/>
<atomRef index="59"/>
</cell>
<cell>
<cellProperties index="105" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="59"/>
</cell>
<cell>
<cellProperties index="106" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="59"/>
<atomRef index="68"/>
<atomRef index="60"/>
</cell>
<cell>
<cellProperties index="107" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="60"/>
</cell>
<cell>
<cellProperties index="108" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="60"/>
<atomRef index="69"/>
<atomRef index="61"/>
</cell>
<cell>
<cellProperties index="109" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="61"/>
</cell>
<cell>
<cellProperties index="110" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="61"/>
<atomRef index="70"/>
<atomRef index="62"/>
</cell>
<cell>
<cellProperties index="111" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="62"/>
</cell>
<cell>
<cellProperties index="112" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="63"/>
<atomRef index="72"/>
<atomRef index="64"/>
</cell>
<cell>
<cellProperties index="113" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="64"/>
</cell>
<cell>
<cellProperties index="114" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="64"/>
<atomRef index="73"/>
<atomRef index="65"/>
</cell>
<cell>
<cellProperties index="115" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="65"/>
</cell>
<cell>
<cellProperties index="116" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="65"/>
<atomRef index="74"/>
<atomRef index="66"/>
</cell>
<cell>
<cellProperties index="117" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="66"/>
</cell>
<cell>
<cellProperties index="118" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="66"/>
<atomRef index="75"/>
<atomRef index="67"/>
</cell>
<cell>
<cellProperties index="119" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="67"/>
</cell>
<cell>
<cellProperties index="120" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="67"/>
<atomRef index="76"/>
<atomRef index="68"/>
</cell>
<cell>
<cellProperties index="121" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="68"/>
</cell>
<cell>
<cellProperties index="122" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="68"/>
<atomRef index="77"/>
<atomRef index="69"/>
</cell>
<cell>
<cellProperties index="123" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="69"/>
</cell>
<cell>
<cellProperties index="124" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="69"/>
<atomRef index="78"/>
<atomRef index="70"/>
</cell>
<cell>
<cellProperties index="125" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="70"/>
</cell>
<cell>
<cellProperties index="126" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="70"/>
<atomRef index="79"/>
<atomRef index="71"/>
</cell>
<cell>
<cellProperties index="127" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="71"/>
</cell>
<cell>
<cellProperties index="128" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="657"/>
</cell>
<cell>
<cellProperties index="129" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="649"/>
<atomRef index="658"/>
<atomRef index="657"/>
</cell>
<cell>
<cellProperties index="130" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="658"/>
</cell>
<cell>
<cellProperties index="131" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="650"/>
<atomRef index="659"/>
<atomRef index="658"/>
</cell>
<cell>
<cellProperties index="132" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="650"/>
<atomRef index="651"/>
<atomRef index="659"/>
</cell>
<cell>
<cellProperties index="133" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="651"/>
<atomRef index="660"/>
<atomRef index="659"/>
</cell>
<cell>
<cellProperties index="134" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="651"/>
<atomRef index="652"/>
<atomRef index="660"/>
</cell>
<cell>
<cellProperties index="135" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="652"/>
<atomRef index="661"/>
<atomRef index="660"/>
</cell>
<cell>
<cellProperties index="136" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="652"/>
<atomRef index="653"/>
<atomRef index="661"/>
</cell>
<cell>
<cellProperties index="137" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="653"/>
<atomRef index="662"/>
<atomRef index="661"/>
</cell>
<cell>
<cellProperties index="138" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="653"/>
<atomRef index="654"/>
<atomRef index="662"/>
</cell>
<cell>
<cellProperties index="139" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="654"/>
<atomRef index="663"/>
<atomRef index="662"/>
</cell>
<cell>
<cellProperties index="140" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="654"/>
<atomRef index="655"/>
<atomRef index="663"/>
</cell>
<cell>
<cellProperties index="141" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="655"/>
<atomRef index="664"/>
<atomRef index="663"/>
</cell>
<cell>
<cellProperties index="142" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="664"/>
</cell>
<cell>
<cellProperties index="143" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="656"/>
<atomRef index="665"/>
<atomRef index="664"/>
</cell>
<cell>
<cellProperties index="144" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="666"/>
</cell>
<cell>
<cellProperties index="145" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="658"/>
<atomRef index="667"/>
<atomRef index="666"/>
</cell>
<cell>
<cellProperties index="146" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="667"/>
</cell>
<cell>
<cellProperties index="147" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="659"/>
<atomRef index="668"/>
<atomRef index="667"/>
</cell>
<cell>
<cellProperties index="148" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="668"/>
</cell>
<cell>
<cellProperties index="149" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="660"/>
<atomRef index="669"/>
<atomRef index="668"/>
</cell>
<cell>
<cellProperties index="150" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="669"/>
</cell>
<cell>
<cellProperties index="151" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="661"/>
<atomRef index="670"/>
<atomRef index="669"/>
</cell>
<cell>
<cellProperties index="152" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="670"/>
</cell>
<cell>
<cellProperties index="153" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="662"/>
<atomRef index="671"/>
<atomRef index="670"/>
</cell>
<cell>
<cellProperties index="154" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="671"/>
</cell>
<cell>
<cellProperties index="155" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="663"/>
<atomRef index="672"/>
<atomRef index="671"/>
</cell>
<cell>
<cellProperties index="156" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="672"/>
</cell>
<cell>
<cellProperties index="157" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="664"/>
<atomRef index="673"/>
<atomRef index="672"/>
</cell>
<cell>
<cellProperties index="158" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="673"/>
</cell>
<cell>
<cellProperties index="159" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="665"/>
<atomRef index="674"/>
<atomRef index="673"/>
</cell>
<cell>
<cellProperties index="160" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="675"/>
</cell>
<cell>
<cellProperties index="161" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="667"/>
<atomRef index="676"/>
<atomRef index="675"/>
</cell>
<cell>
<cellProperties index="162" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="676"/>
</cell>
<cell>
<cellProperties index="163" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="668"/>
<atomRef index="677"/>
<atomRef index="676"/>
</cell>
<cell>
<cellProperties index="164" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="677"/>
</cell>
<cell>
<cellProperties index="165" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="669"/>
<atomRef index="678"/>
<atomRef index="677"/>
</cell>
<cell>
<cellProperties index="166" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="678"/>
</cell>
<cell>
<cellProperties index="167" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="670"/>
<atomRef index="679"/>
<atomRef index="678"/>
</cell>
<cell>
<cellProperties index="168" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="679"/>
</cell>
<cell>
<cellProperties index="169" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="671"/>
<atomRef index="680"/>
<atomRef index="679"/>
</cell>
<cell>
<cellProperties index="170" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="680"/>
</cell>
<cell>
<cellProperties index="171" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="672"/>
<atomRef index="681"/>
<atomRef index="680"/>
</cell>
<cell>
<cellProperties index="172" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="681"/>
</cell>
<cell>
<cellProperties index="173" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="673"/>
<atomRef index="682"/>
<atomRef index="681"/>
</cell>
<cell>
<cellProperties index="174" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="682"/>
</cell>
<cell>
<cellProperties index="175" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="674"/>
<atomRef index="683"/>
<atomRef index="682"/>
</cell>
<cell>
<cellProperties index="176" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="684"/>
</cell>
<cell>
<cellProperties index="177" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="676"/>
<atomRef index="685"/>
<atomRef index="684"/>
</cell>
<cell>
<cellProperties index="178" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="685"/>
</cell>
<cell>
<cellProperties index="179" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="677"/>
<atomRef index="686"/>
<atomRef index="685"/>
</cell>
<cell>
<cellProperties index="180" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="686"/>
</cell>
<cell>
<cellProperties index="181" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="678"/>
<atomRef index="687"/>
<atomRef index="686"/>
</cell>
<cell>
<cellProperties index="182" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="687"/>
</cell>
<cell>
<cellProperties index="183" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="679"/>
<atomRef index="688"/>
<atomRef index="687"/>
</cell>
<cell>
<cellProperties index="184" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="688"/>
</cell>
<cell>
<cellProperties index="185" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="680"/>
<atomRef index="689"/>
<atomRef index="688"/>
</cell>
<cell>
<cellProperties index="186" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="689"/>
</cell>
<cell>
<cellProperties index="187" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="681"/>
<atomRef index="690"/>
<atomRef index="689"/>
</cell>
<cell>
<cellProperties index="188" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="690"/>
</cell>
<cell>
<cellProperties index="189" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="682"/>
<atomRef index="691"/>
<atomRef index="690"/>
</cell>
<cell>
<cellProperties index="190" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="691"/>
</cell>
<cell>
<cellProperties index="191" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="683"/>
<atomRef index="692"/>
<atomRef index="691"/>
</cell>
<cell>
<cellProperties index="192" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="693"/>
</cell>
<cell>
<cellProperties index="193" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="685"/>
<atomRef index="694"/>
<atomRef index="693"/>
</cell>
<cell>
<cellProperties index="194" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="694"/>
</cell>
<cell>
<cellProperties index="195" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="686"/>
<atomRef index="695"/>
<atomRef index="694"/>
</cell>
<cell>
<cellProperties index="196" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="695"/>
</cell>
<cell>
<cellProperties index="197" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="687"/>
<atomRef index="696"/>
<atomRef index="695"/>
</cell>
<cell>
<cellProperties index="198" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="696"/>
</cell>
<cell>
<cellProperties index="199" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="688"/>
<atomRef index="697"/>
<atomRef index="696"/>
</cell>
<cell>
<cellProperties index="200" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="697"/>
</cell>
<cell>
<cellProperties index="201" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="689"/>
<atomRef index="698"/>
<atomRef index="697"/>
</cell>
<cell>
<cellProperties index="202" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="698"/>
</cell>
<cell>
<cellProperties index="203" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="690"/>
<atomRef index="699"/>
<atomRef index="698"/>
</cell>
<cell>
<cellProperties index="204" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="699"/>
</cell>
<cell>
<cellProperties index="205" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="691"/>
<atomRef index="700"/>
<atomRef index="699"/>
</cell>
<cell>
<cellProperties index="206" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="700"/>
</cell>
<cell>
<cellProperties index="207" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="692"/>
<atomRef index="701"/>
<atomRef index="700"/>
</cell>
<cell>
<cellProperties index="208" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="702"/>
</cell>
<cell>
<cellProperties index="209" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="694"/>
<atomRef index="703"/>
<atomRef index="702"/>
</cell>
<cell>
<cellProperties index="210" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="703"/>
</cell>
<cell>
<cellProperties index="211" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="695"/>
<atomRef index="704"/>
<atomRef index="703"/>
</cell>
<cell>
<cellProperties index="212" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="704"/>
</cell>
<cell>
<cellProperties index="213" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="696"/>
<atomRef index="705"/>
<atomRef index="704"/>
</cell>
<cell>
<cellProperties index="214" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="705"/>
</cell>
<cell>
<cellProperties index="215" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="697"/>
<atomRef index="706"/>
<atomRef index="705"/>
</cell>
<cell>
<cellProperties index="216" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="706"/>
</cell>
<cell>
<cellProperties index="217" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="698"/>
<atomRef index="707"/>
<atomRef index="706"/>
</cell>
<cell>
<cellProperties index="218" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="707"/>
</cell>
<cell>
<cellProperties index="219" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="699"/>
<atomRef index="708"/>
<atomRef index="707"/>
</cell>
<cell>
<cellProperties index="220" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="708"/>
</cell>
<cell>
<cellProperties index="221" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="700"/>
<atomRef index="709"/>
<atomRef index="708"/>
</cell>
<cell>
<cellProperties index="222" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="709"/>
</cell>
<cell>
<cellProperties index="223" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="701"/>
<atomRef index="710"/>
<atomRef index="709"/>
</cell>
<cell>
<cellProperties index="224" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="711"/>
</cell>
<cell>
<cellProperties index="225" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="703"/>
<atomRef index="712"/>
<atomRef index="711"/>
</cell>
<cell>
<cellProperties index="226" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="712"/>
</cell>
<cell>
<cellProperties index="227" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="704"/>
<atomRef index="713"/>
<atomRef index="712"/>
</cell>
<cell>
<cellProperties index="228" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="713"/>
</cell>
<cell>
<cellProperties index="229" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="705"/>
<atomRef index="714"/>
<atomRef index="713"/>
</cell>
<cell>
<cellProperties index="230" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="714"/>
</cell>
<cell>
<cellProperties index="231" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="706"/>
<atomRef index="715"/>
<atomRef index="714"/>
</cell>
<cell>
<cellProperties index="232" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="715"/>
</cell>
<cell>
<cellProperties index="233" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="707"/>
<atomRef index="716"/>
<atomRef index="715"/>
</cell>
<cell>
<cellProperties index="234" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="716"/>
</cell>
<cell>
<cellProperties index="235" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="708"/>
<atomRef index="717"/>
<atomRef index="716"/>
</cell>
<cell>
<cellProperties index="236" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="717"/>
</cell>
<cell>
<cellProperties index="237" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="709"/>
<atomRef index="718"/>
<atomRef index="717"/>
</cell>
<cell>
<cellProperties index="238" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="718"/>
</cell>
<cell>
<cellProperties index="239" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="710"/>
<atomRef index="719"/>
<atomRef index="718"/>
</cell>
<cell>
<cellProperties index="240" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="720"/>
</cell>
<cell>
<cellProperties index="241" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="712"/>
<atomRef index="721"/>
<atomRef index="720"/>
</cell>
<cell>
<cellProperties index="242" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="721"/>
</cell>
<cell>
<cellProperties index="243" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="713"/>
<atomRef index="722"/>
<atomRef index="721"/>
</cell>
<cell>
<cellProperties index="244" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="722"/>
</cell>
<cell>
<cellProperties index="245" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="714"/>
<atomRef index="723"/>
<atomRef index="722"/>
</cell>
<cell>
<cellProperties index="246" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="723"/>
</cell>
<cell>
<cellProperties index="247" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="715"/>
<atomRef index="724"/>
<atomRef index="723"/>
</cell>
<cell>
<cellProperties index="248" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="724"/>
</cell>
<cell>
<cellProperties index="249" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="716"/>
<atomRef index="725"/>
<atomRef index="724"/>
</cell>
<cell>
<cellProperties index="250" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="725"/>
</cell>
<cell>
<cellProperties index="251" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="717"/>
<atomRef index="726"/>
<atomRef index="725"/>
</cell>
<cell>
<cellProperties index="252" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="726"/>
</cell>
<cell>
<cellProperties index="253" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="718"/>
<atomRef index="727"/>
<atomRef index="726"/>
</cell>
<cell>
<cellProperties index="254" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="727"/>
</cell>
<cell>
<cellProperties index="255" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="719"/>
<atomRef index="728"/>
<atomRef index="727"/>
</cell>
<cell>
<cellProperties index="256" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="81"/>
<atomRef index="9"/>
</cell>
<cell>
<cellProperties index="257" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="81"/>
<atomRef index="90"/>
<atomRef index="9"/>
</cell>
<cell>
<cellProperties index="258" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="81"/>
<atomRef index="162"/>
<atomRef index="90"/>
</cell>
<cell>
<cellProperties index="259" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="162"/>
<atomRef index="171"/>
<atomRef index="90"/>
</cell>
<cell>
<cellProperties index="260" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="162"/>
<atomRef index="243"/>
<atomRef index="171"/>
</cell>
<cell>
<cellProperties index="261" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="243"/>
<atomRef index="252"/>
<atomRef index="171"/>
</cell>
<cell>
<cellProperties index="262" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="243"/>
<atomRef index="324"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="263" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="324"/>
<atomRef index="333"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="264" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="324"/>
<atomRef index="405"/>
<atomRef index="333"/>
</cell>
<cell>
<cellProperties index="265" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="405"/>
<atomRef index="414"/>
<atomRef index="333"/>
</cell>
<cell>
<cellProperties index="266" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="405"/>
<atomRef index="486"/>
<atomRef index="414"/>
</cell>
<cell>
<cellProperties index="267" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="486"/>
<atomRef index="495"/>
<atomRef index="414"/>
</cell>
<cell>
<cellProperties index="268" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="486"/>
<atomRef index="567"/>
<atomRef index="495"/>
</cell>
<cell>
<cellProperties index="269" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="567"/>
<atomRef index="576"/>
<atomRef index="495"/>
</cell>
<cell>
<cellProperties index="270" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="567"/>
<atomRef index="648"/>
<atomRef index="576"/>
</cell>
<cell>
<cellProperties index="271" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="648"/>
<atomRef index="657"/>
<atomRef index="576"/>
</cell>
<cell>
<cellProperties index="272" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="90"/>
<atomRef index="18"/>
</cell>
<cell>
<cellProperties index="273" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="90"/>
<atomRef index="99"/>
<atomRef index="18"/>
</cell>
<cell>
<cellProperties index="274" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="90"/>
<atomRef index="171"/>
<atomRef index="99"/>
</cell>
<cell>
<cellProperties index="275" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="171"/>
<atomRef index="180"/>
<atomRef index="99"/>
</cell>
<cell>
<cellProperties index="276" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="171"/>
<atomRef index="252"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="277" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="252"/>
<atomRef index="261"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="278" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="252"/>
<atomRef index="333"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="279" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="333"/>
<atomRef index="342"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="280" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="333"/>
<atomRef index="414"/>
<atomRef index="342"/>
</cell>
<cell>
<cellProperties index="281" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="342"/>
</cell>
<cell>
<cellProperties index="282" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="414"/>
<atomRef index="495"/>
<atomRef index="423"/>
</cell>
<cell>
<cellProperties index="283" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="423"/>
</cell>
<cell>
<cellProperties index="284" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="495"/>
<atomRef index="576"/>
<atomRef index="504"/>
</cell>
<cell>
<cellProperties index="285" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="504"/>
</cell>
<cell>
<cellProperties index="286" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="576"/>
<atomRef index="657"/>
<atomRef index="585"/>
</cell>
<cell>
<cellProperties index="287" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="657"/>
<atomRef index="666"/>
<atomRef index="585"/>
</cell>
<cell>
<cellProperties index="288" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="18"/>
<atomRef index="99"/>
<atomRef index="27"/>
</cell>
<cell>
<cellProperties index="289" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="99"/>
<atomRef index="108"/>
<atomRef index="27"/>
</cell>
<cell>
<cellProperties index="290" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="99"/>
<atomRef index="180"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="291" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="180"/>
<atomRef index="189"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="292" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="180"/>
<atomRef index="261"/>
<atomRef index="189"/>
</cell>
<cell>
<cellProperties index="293" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="189"/>
</cell>
<cell>
<cellProperties index="294" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="261"/>
<atomRef index="342"/>
<atomRef index="270"/>
</cell>
<cell>
<cellProperties index="295" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="270"/>
</cell>
<cell>
<cellProperties index="296" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="342"/>
<atomRef index="423"/>
<atomRef index="351"/>
</cell>
<cell>
<cellProperties index="297" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="351"/>
</cell>
<cell>
<cellProperties index="298" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="423"/>
<atomRef index="504"/>
<atomRef index="432"/>
</cell>
<cell>
<cellProperties index="299" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="432"/>
</cell>
<cell>
<cellProperties index="300" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="504"/>
<atomRef index="585"/>
<atomRef index="513"/>
</cell>
<cell>
<cellProperties index="301" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="513"/>
</cell>
<cell>
<cellProperties index="302" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="585"/>
<atomRef index="666"/>
<atomRef index="594"/>
</cell>
<cell>
<cellProperties index="303" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="666"/>
<atomRef index="675"/>
<atomRef index="594"/>
</cell>
<cell>
<cellProperties index="304" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="27"/>
<atomRef index="108"/>
<atomRef index="36"/>
</cell>
<cell>
<cellProperties index="305" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="117"/>
<atomRef index="36"/>
</cell>
<cell>
<cellProperties index="306" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="189"/>
<atomRef index="117"/>
</cell>
<cell>
<cellProperties index="307" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="189"/>
<atomRef index="198"/>
<atomRef index="117"/>
</cell>
<cell>
<cellProperties index="308" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="189"/>
<atomRef index="270"/>
<atomRef index="198"/>
</cell>
<cell>
<cellProperties index="309" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="198"/>
</cell>
<cell>
<cellProperties index="310" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="270"/>
<atomRef index="351"/>
<atomRef index="279"/>
</cell>
<cell>
<cellProperties index="311" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="279"/>
</cell>
<cell>
<cellProperties index="312" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="351"/>
<atomRef index="432"/>
<atomRef index="360"/>
</cell>
<cell>
<cellProperties index="313" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="360"/>
</cell>
<cell>
<cellProperties index="314" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="432"/>
<atomRef index="513"/>
<atomRef index="441"/>
</cell>
<cell>
<cellProperties index="315" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="441"/>
</cell>
<cell>
<cellProperties index="316" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="513"/>
<atomRef index="594"/>
<atomRef index="522"/>
</cell>
<cell>
<cellProperties index="317" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="522"/>
</cell>
<cell>
<cellProperties index="318" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="594"/>
<atomRef index="675"/>
<atomRef index="603"/>
</cell>
<cell>
<cellProperties index="319" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="675"/>
<atomRef index="684"/>
<atomRef index="603"/>
</cell>
<cell>
<cellProperties index="320" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="36"/>
<atomRef index="117"/>
<atomRef index="45"/>
</cell>
<cell>
<cellProperties index="321" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="45"/>
</cell>
<cell>
<cellProperties index="322" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="117"/>
<atomRef index="198"/>
<atomRef index="126"/>
</cell>
<cell>
<cellProperties index="323" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="126"/>
</cell>
<cell>
<cellProperties index="324" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="198"/>
<atomRef index="279"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="325" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="326" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="279"/>
<atomRef index="360"/>
<atomRef index="288"/>
</cell>
<cell>
<cellProperties index="327" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="288"/>
</cell>
<cell>
<cellProperties index="328" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="360"/>
<atomRef index="441"/>
<atomRef index="369"/>
</cell>
<cell>
<cellProperties index="329" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="369"/>
</cell>
<cell>
<cellProperties index="330" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="441"/>
<atomRef index="522"/>
<atomRef index="450"/>
</cell>
<cell>
<cellProperties index="331" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="450"/>
</cell>
<cell>
<cellProperties index="332" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="522"/>
<atomRef index="603"/>
<atomRef index="531"/>
</cell>
<cell>
<cellProperties index="333" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="531"/>
</cell>
<cell>
<cellProperties index="334" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="603"/>
<atomRef index="684"/>
<atomRef index="612"/>
</cell>
<cell>
<cellProperties index="335" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="684"/>
<atomRef index="693"/>
<atomRef index="612"/>
</cell>
<cell>
<cellProperties index="336" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="45"/>
<atomRef index="126"/>
<atomRef index="54"/>
</cell>
<cell>
<cellProperties index="337" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="54"/>
</cell>
<cell>
<cellProperties index="338" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="126"/>
<atomRef index="207"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="339" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="340" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="207"/>
<atomRef index="288"/>
<atomRef index="216"/>
</cell>
<cell>
<cellProperties index="341" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="216"/>
</cell>
<cell>
<cellProperties index="342" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="288"/>
<atomRef index="369"/>
<atomRef index="297"/>
</cell>
<cell>
<cellProperties index="343" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="297"/>
</cell>
<cell>
<cellProperties index="344" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="369"/>
<atomRef index="450"/>
<atomRef index="378"/>
</cell>
<cell>
<cellProperties index="345" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="378"/>
</cell>
<cell>
<cellProperties index="346" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="450"/>
<atomRef index="531"/>
<atomRef index="459"/>
</cell>
<cell>
<cellProperties index="347" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="459"/>
</cell>
<cell>
<cellProperties index="348" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="531"/>
<atomRef index="612"/>
<atomRef index="540"/>
</cell>
<cell>
<cellProperties index="349" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="540"/>
</cell>
<cell>
<cellProperties index="350" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="612"/>
<atomRef index="693"/>
<atomRef index="621"/>
</cell>
<cell>
<cellProperties index="351" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="693"/>
<atomRef index="702"/>
<atomRef index="621"/>
</cell>
<cell>
<cellProperties index="352" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="54"/>
<atomRef index="135"/>
<atomRef index="63"/>
</cell>
<cell>
<cellProperties index="353" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="63"/>
</cell>
<cell>
<cellProperties index="354" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="135"/>
<atomRef index="216"/>
<atomRef index="144"/>
</cell>
<cell>
<cellProperties index="355" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="144"/>
</cell>
<cell>
<cellProperties index="356" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="216"/>
<atomRef index="297"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="357" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="358" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="297"/>
<atomRef index="378"/>
<atomRef index="306"/>
</cell>
<cell>
<cellProperties index="359" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="306"/>
</cell>
<cell>
<cellProperties index="360" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="378"/>
<atomRef index="459"/>
<atomRef index="387"/>
</cell>
<cell>
<cellProperties index="361" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="387"/>
</cell>
<cell>
<cellProperties index="362" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="459"/>
<atomRef index="540"/>
<atomRef index="468"/>
</cell>
<cell>
<cellProperties index="363" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="468"/>
</cell>
<cell>
<cellProperties index="364" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="540"/>
<atomRef index="621"/>
<atomRef index="549"/>
</cell>
<cell>
<cellProperties index="365" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="549"/>
</cell>
<cell>
<cellProperties index="366" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="621"/>
<atomRef index="702"/>
<atomRef index="630"/>
</cell>
<cell>
<cellProperties index="367" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="702"/>
<atomRef index="711"/>
<atomRef index="630"/>
</cell>
<cell>
<cellProperties index="368" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="63"/>
<atomRef index="144"/>
<atomRef index="72"/>
</cell>
<cell>
<cellProperties index="369" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="72"/>
</cell>
<cell>
<cellProperties index="370" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="144"/>
<atomRef index="225"/>
<atomRef index="153"/>
</cell>
<cell>
<cellProperties index="371" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="153"/>
</cell>
<cell>
<cellProperties index="372" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="225"/>
<atomRef index="306"/>
<atomRef index="234"/>
</cell>
<cell>
<cellProperties index="373" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="234"/>
</cell>
<cell>
<cellProperties index="374" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="306"/>
<atomRef index="387"/>
<atomRef index="315"/>
</cell>
<cell>
<cellProperties index="375" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="315"/>
</cell>
<cell>
<cellProperties index="376" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="387"/>
<atomRef index="468"/>
<atomRef index="396"/>
</cell>
<cell>
<cellProperties index="377" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="396"/>
</cell>
<cell>
<cellProperties index="378" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="468"/>
<atomRef index="549"/>
<atomRef index="477"/>
</cell>
<cell>
<cellProperties index="379" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="477"/>
</cell>
<cell>
<cellProperties index="380" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="549"/>
<atomRef index="630"/>
<atomRef index="558"/>
</cell>
<cell>
<cellProperties index="381" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="558"/>
</cell>
<cell>
<cellProperties index="382" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="630"/>
<atomRef index="711"/>
<atomRef index="639"/>
</cell>
<cell>
<cellProperties index="383" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="711"/>
<atomRef index="720"/>
<atomRef index="639"/>
</cell>
<cell>
<cellProperties index="384" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="17"/>
<atomRef index="89"/>
</cell>
<cell>
<cellProperties index="385" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="17"/>
<atomRef index="98"/>
<atomRef index="89"/>
</cell>
<cell>
<cellProperties index="386" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="170"/>
</cell>
<cell>
<cellProperties index="387" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="98"/>
<atomRef index="179"/>
<atomRef index="170"/>
</cell>
<cell>
<cellProperties index="388" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="251"/>
</cell>
<cell>
<cellProperties index="389" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="179"/>
<atomRef index="260"/>
<atomRef index="251"/>
</cell>
<cell>
<cellProperties index="390" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="332"/>
</cell>
<cell>
<cellProperties index="391" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="260"/>
<atomRef index="341"/>
<atomRef index="332"/>
</cell>
<cell>
<cellProperties index="392" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="413"/>
</cell>
<cell>
<cellProperties index="393" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="341"/>
<atomRef index="422"/>
<atomRef index="413"/>
</cell>
<cell>
<cellProperties index="394" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="494"/>
</cell>
<cell>
<cellProperties index="395" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="422"/>
<atomRef index="503"/>
<atomRef index="494"/>
</cell>
<cell>
<cellProperties index="396" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="575"/>
</cell>
<cell>
<cellProperties index="397" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="503"/>
<atomRef index="584"/>
<atomRef index="575"/>
</cell>
<cell>
<cellProperties index="398" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="656"/>
</cell>
<cell>
<cellProperties index="399" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="584"/>
<atomRef index="665"/>
<atomRef index="656"/>
</cell>
<cell>
<cellProperties index="400" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="17"/>
<atomRef index="26"/>
<atomRef index="98"/>
</cell>
<cell>
<cellProperties index="401" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="26"/>
<atomRef index="107"/>
<atomRef index="98"/>
</cell>
<cell>
<cellProperties index="402" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="179"/>
</cell>
<cell>
<cellProperties index="403" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="188"/>
<atomRef index="179"/>
</cell>
<cell>
<cellProperties index="404" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="260"/>
</cell>
<cell>
<cellProperties index="405" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="188"/>
<atomRef index="269"/>
<atomRef index="260"/>
</cell>
<cell>
<cellProperties index="406" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="341"/>
</cell>
<cell>
<cellProperties index="407" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="269"/>
<atomRef index="350"/>
<atomRef index="341"/>
</cell>
<cell>
<cellProperties index="408" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="422"/>
</cell>
<cell>
<cellProperties index="409" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="350"/>
<atomRef index="431"/>
<atomRef index="422"/>
</cell>
<cell>
<cellProperties index="410" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="503"/>
</cell>
<cell>
<cellProperties index="411" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="431"/>
<atomRef index="512"/>
<atomRef index="503"/>
</cell>
<cell>
<cellProperties index="412" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="584"/>
</cell>
<cell>
<cellProperties index="413" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="512"/>
<atomRef index="593"/>
<atomRef index="584"/>
</cell>
<cell>
<cellProperties index="414" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="665"/>
</cell>
<cell>
<cellProperties index="415" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="593"/>
<atomRef index="674"/>
<atomRef index="665"/>
</cell>
<cell>
<cellProperties index="416" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="26"/>
<atomRef index="35"/>
<atomRef index="107"/>
</cell>
<cell>
<cellProperties index="417" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="35"/>
<atomRef index="116"/>
<atomRef index="107"/>
</cell>
<cell>
<cellProperties index="418" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="188"/>
</cell>
<cell>
<cellProperties index="419" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="116"/>
<atomRef index="197"/>
<atomRef index="188"/>
</cell>
<cell>
<cellProperties index="420" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="269"/>
</cell>
<cell>
<cellProperties index="421" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="197"/>
<atomRef index="278"/>
<atomRef index="269"/>
</cell>
<cell>
<cellProperties index="422" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="350"/>
</cell>
<cell>
<cellProperties index="423" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="278"/>
<atomRef index="359"/>
<atomRef index="350"/>
</cell>
<cell>
<cellProperties index="424" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="431"/>
</cell>
<cell>
<cellProperties index="425" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="359"/>
<atomRef index="440"/>
<atomRef index="431"/>
</cell>
<cell>
<cellProperties index="426" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="512"/>
</cell>
<cell>
<cellProperties index="427" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="440"/>
<atomRef index="521"/>
<atomRef index="512"/>
</cell>
<cell>
<cellProperties index="428" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="593"/>
</cell>
<cell>
<cellProperties index="429" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="521"/>
<atomRef index="602"/>
<atomRef index="593"/>
</cell>
<cell>
<cellProperties index="430" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="674"/>
</cell>
<cell>
<cellProperties index="431" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="602"/>
<atomRef index="683"/>
<atomRef index="674"/>
</cell>
<cell>
<cellProperties index="432" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="35"/>
<atomRef index="44"/>
<atomRef index="116"/>
</cell>
<cell>
<cellProperties index="433" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="44"/>
<atomRef index="125"/>
<atomRef index="116"/>
</cell>
<cell>
<cellProperties index="434" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="435" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="125"/>
<atomRef index="206"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="436" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="278"/>
</cell>
<cell>
<cellProperties index="437" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="206"/>
<atomRef index="287"/>
<atomRef index="278"/>
</cell>
<cell>
<cellProperties index="438" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="359"/>
</cell>
<cell>
<cellProperties index="439" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="287"/>
<atomRef index="368"/>
<atomRef index="359"/>
</cell>
<cell>
<cellProperties index="440" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="440"/>
</cell>
<cell>
<cellProperties index="441" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="368"/>
<atomRef index="449"/>
<atomRef index="440"/>
</cell>
<cell>
<cellProperties index="442" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="521"/>
</cell>
<cell>
<cellProperties index="443" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="449"/>
<atomRef index="530"/>
<atomRef index="521"/>
</cell>
<cell>
<cellProperties index="444" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="602"/>
</cell>
<cell>
<cellProperties index="445" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="530"/>
<atomRef index="611"/>
<atomRef index="602"/>
</cell>
<cell>
<cellProperties index="446" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="683"/>
</cell>
<cell>
<cellProperties index="447" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="611"/>
<atomRef index="692"/>
<atomRef index="683"/>
</cell>
<cell>
<cellProperties index="448" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="44"/>
<atomRef index="53"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="449" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="53"/>
<atomRef index="134"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="450" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="206"/>
</cell>
<cell>
<cellProperties index="451" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="134"/>
<atomRef index="215"/>
<atomRef index="206"/>
</cell>
<cell>
<cellProperties index="452" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="287"/>
</cell>
<cell>
<cellProperties index="453" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="215"/>
<atomRef index="296"/>
<atomRef index="287"/>
</cell>
<cell>
<cellProperties index="454" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="368"/>
</cell>
<cell>
<cellProperties index="455" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="296"/>
<atomRef index="377"/>
<atomRef index="368"/>
</cell>
<cell>
<cellProperties index="456" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="449"/>
</cell>
<cell>
<cellProperties index="457" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="377"/>
<atomRef index="458"/>
<atomRef index="449"/>
</cell>
<cell>
<cellProperties index="458" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="530"/>
</cell>
<cell>
<cellProperties index="459" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="458"/>
<atomRef index="539"/>
<atomRef index="530"/>
</cell>
<cell>
<cellProperties index="460" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="611"/>
</cell>
<cell>
<cellProperties index="461" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="539"/>
<atomRef index="620"/>
<atomRef index="611"/>
</cell>
<cell>
<cellProperties index="462" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="692"/>
</cell>
<cell>
<cellProperties index="463" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="620"/>
<atomRef index="701"/>
<atomRef index="692"/>
</cell>
<cell>
<cellProperties index="464" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="53"/>
<atomRef index="62"/>
<atomRef index="134"/>
</cell>
<cell>
<cellProperties index="465" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="62"/>
<atomRef index="143"/>
<atomRef index="134"/>
</cell>
<cell>
<cellProperties index="466" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="215"/>
</cell>
<cell>
<cellProperties index="467" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="143"/>
<atomRef index="224"/>
<atomRef index="215"/>
</cell>
<cell>
<cellProperties index="468" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="296"/>
</cell>
<cell>
<cellProperties index="469" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="224"/>
<atomRef index="305"/>
<atomRef index="296"/>
</cell>
<cell>
<cellProperties index="470" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="377"/>
</cell>
<cell>
<cellProperties index="471" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="305"/>
<atomRef index="386"/>
<atomRef index="377"/>
</cell>
<cell>
<cellProperties index="472" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="458"/>
</cell>
<cell>
<cellProperties index="473" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="386"/>
<atomRef index="467"/>
<atomRef index="458"/>
</cell>
<cell>
<cellProperties index="474" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="539"/>
</cell>
<cell>
<cellProperties index="475" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="467"/>
<atomRef index="548"/>
<atomRef index="539"/>
</cell>
<cell>
<cellProperties index="476" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="620"/>
</cell>
<cell>
<cellProperties index="477" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="548"/>
<atomRef index="629"/>
<atomRef index="620"/>
</cell>
<cell>
<cellProperties index="478" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="701"/>
</cell>
<cell>
<cellProperties index="479" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="629"/>
<atomRef index="710"/>
<atomRef index="701"/>
</cell>
<cell>
<cellProperties index="480" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="62"/>
<atomRef index="71"/>
<atomRef index="143"/>
</cell>
<cell>
<cellProperties index="481" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="71"/>
<atomRef index="152"/>
<atomRef index="143"/>
</cell>
<cell>
<cellProperties index="482" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="483" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="152"/>
<atomRef index="233"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="484" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="305"/>
</cell>
<cell>
<cellProperties index="485" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="233"/>
<atomRef index="314"/>
<atomRef index="305"/>
</cell>
<cell>
<cellProperties index="486" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="386"/>
</cell>
<cell>
<cellProperties index="487" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="314"/>
<atomRef index="395"/>
<atomRef index="386"/>
</cell>
<cell>
<cellProperties index="488" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="467"/>
</cell>
<cell>
<cellProperties index="489" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="395"/>
<atomRef index="476"/>
<atomRef index="467"/>
</cell>
<cell>
<cellProperties index="490" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="548"/>
</cell>
<cell>
<cellProperties index="491" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="476"/>
<atomRef index="557"/>
<atomRef index="548"/>
</cell>
<cell>
<cellProperties index="492" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="629"/>
</cell>
<cell>
<cellProperties index="493" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="557"/>
<atomRef index="638"/>
<atomRef index="629"/>
</cell>
<cell>
<cellProperties index="494" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="710"/>
</cell>
<cell>
<cellProperties index="495" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="638"/>
<atomRef index="719"/>
<atomRef index="710"/>
</cell>
<cell>
<cellProperties index="496" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="71"/>
<atomRef index="80"/>
<atomRef index="152"/>
</cell>
<cell>
<cellProperties index="497" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="80"/>
<atomRef index="161"/>
<atomRef index="152"/>
</cell>
<cell>
<cellProperties index="498" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="152"/>
<atomRef index="161"/>
<atomRef index="233"/>
</cell>
<cell>
<cellProperties index="499" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="161"/>
<atomRef index="242"/>
<atomRef index="233"/>
</cell>
<cell>
<cellProperties index="500" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="233"/>
<atomRef index="242"/>
<atomRef index="314"/>
</cell>
<cell>
<cellProperties index="501" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="242"/>
<atomRef index="323"/>
<atomRef index="314"/>
</cell>
<cell>
<cellProperties index="502" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="314"/>
<atomRef index="323"/>
<atomRef index="395"/>
</cell>
<cell>
<cellProperties index="503" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="323"/>
<atomRef index="404"/>
<atomRef index="395"/>
</cell>
<cell>
<cellProperties index="504" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="395"/>
<atomRef index="404"/>
<atomRef index="476"/>
</cell>
<cell>
<cellProperties index="505" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="404"/>
<atomRef index="485"/>
<atomRef index="476"/>
</cell>
<cell>
<cellProperties index="506" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="476"/>
<atomRef index="485"/>
<atomRef index="557"/>
</cell>
<cell>
<cellProperties index="507" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="485"/>
<atomRef index="566"/>
<atomRef index="557"/>
</cell>
<cell>
<cellProperties index="508" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="557"/>
<atomRef index="566"/>
<atomRef index="638"/>
</cell>
<cell>
<cellProperties index="509" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="566"/>
<atomRef index="647"/>
<atomRef index="638"/>
</cell>
<cell>
<cellProperties index="510" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="638"/>
<atomRef index="647"/>
<atomRef index="719"/>
</cell>
<cell>
<cellProperties index="511" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="647"/>
<atomRef index="728"/>
<atomRef index="719"/>
</cell>
<cell>
<cellProperties index="512" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="81"/>
</cell>
<cell>
<cellProperties index="513" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="82"/>
<atomRef index="81"/>
</cell>
<cell>
<cellProperties index="514" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="82"/>
</cell>
<cell>
<cellProperties index="515" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="83"/>
<atomRef index="82"/>
</cell>
<cell>
<cellProperties index="516" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="83"/>
</cell>
<cell>
<cellProperties index="517" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="84"/>
<atomRef index="83"/>
</cell>
<cell>
<cellProperties index="518" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="84"/>
</cell>
<cell>
<cellProperties index="519" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="85"/>
<atomRef index="84"/>
</cell>
<cell>
<cellProperties index="520" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="85"/>
</cell>
<cell>
<cellProperties index="521" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="86"/>
<atomRef index="85"/>
</cell>
<cell>
<cellProperties index="522" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="86"/>
</cell>
<cell>
<cellProperties index="523" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="87"/>
<atomRef index="86"/>
</cell>
<cell>
<cellProperties index="524" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="87"/>
</cell>
<cell>
<cellProperties index="525" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="88"/>
<atomRef index="87"/>
</cell>
<cell>
<cellProperties index="526" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="88"/>
</cell>
<cell>
<cellProperties index="527" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="89"/>
<atomRef index="88"/>
</cell>
<cell>
<cellProperties index="528" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="162"/>
</cell>
<cell>
<cellProperties index="529" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="82"/>
<atomRef index="163"/>
<atomRef index="162"/>
</cell>
<cell>
<cellProperties index="530" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="163"/>
</cell>
<cell>
<cellProperties index="531" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="83"/>
<atomRef index="164"/>
<atomRef index="163"/>
</cell>
<cell>
<cellProperties index="532" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="164"/>
</cell>
<cell>
<cellProperties index="533" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="84"/>
<atomRef index="165"/>
<atomRef index="164"/>
</cell>
<cell>
<cellProperties index="534" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="165"/>
</cell>
<cell>
<cellProperties index="535" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="85"/>
<atomRef index="166"/>
<atomRef index="165"/>
</cell>
<cell>
<cellProperties index="536" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="166"/>
</cell>
<cell>
<cellProperties index="537" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="86"/>
<atomRef index="167"/>
<atomRef index="166"/>
</cell>
<cell>
<cellProperties index="538" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="167"/>
</cell>
<cell>
<cellProperties index="539" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="87"/>
<atomRef index="168"/>
<atomRef index="167"/>
</cell>
<cell>
<cellProperties index="540" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="541" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="88"/>
<atomRef index="169"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="542" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="169"/>
</cell>
<cell>
<cellProperties index="543" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="89"/>
<atomRef index="170"/>
<atomRef index="169"/>
</cell>
<cell>
<cellProperties index="544" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="243"/>
</cell>
<cell>
<cellProperties index="545" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="163"/>
<atomRef index="244"/>
<atomRef index="243"/>
</cell>
<cell>
<cellProperties index="546" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="244"/>
</cell>
<cell>
<cellProperties index="547" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="164"/>
<atomRef index="245"/>
<atomRef index="244"/>
</cell>
<cell>
<cellProperties index="548" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="549" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="165"/>
<atomRef index="246"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="550" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="246"/>
</cell>
<cell>
<cellProperties index="551" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="166"/>
<atomRef index="247"/>
<atomRef index="246"/>
</cell>
<cell>
<cellProperties index="552" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="247"/>
</cell>
<cell>
<cellProperties index="553" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="167"/>
<atomRef index="248"/>
<atomRef index="247"/>
</cell>
<cell>
<cellProperties index="554" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="248"/>
</cell>
<cell>
<cellProperties index="555" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="168"/>
<atomRef index="249"/>
<atomRef index="248"/>
</cell>
<cell>
<cellProperties index="556" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="249"/>
</cell>
<cell>
<cellProperties index="557" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="169"/>
<atomRef index="250"/>
<atomRef index="249"/>
</cell>
<cell>
<cellProperties index="558" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="250"/>
</cell>
<cell>
<cellProperties index="559" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="170"/>
<atomRef index="251"/>
<atomRef index="250"/>
</cell>
<cell>
<cellProperties index="560" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="561" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="244"/>
<atomRef index="325"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="562" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="325"/>
</cell>
<cell>
<cellProperties index="563" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="245"/>
<atomRef index="326"/>
<atomRef index="325"/>
</cell>
<cell>
<cellProperties index="564" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="326"/>
</cell>
<cell>
<cellProperties index="565" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="246"/>
<atomRef index="327"/>
<atomRef index="326"/>
</cell>
<cell>
<cellProperties index="566" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="327"/>
</cell>
<cell>
<cellProperties index="567" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="247"/>
<atomRef index="328"/>
<atomRef index="327"/>
</cell>
<cell>
<cellProperties index="568" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="328"/>
</cell>
<cell>
<cellProperties index="569" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="248"/>
<atomRef index="329"/>
<atomRef index="328"/>
</cell>
<cell>
<cellProperties index="570" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="329"/>
</cell>
<cell>
<cellProperties index="571" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="249"/>
<atomRef index="330"/>
<atomRef index="329"/>
</cell>
<cell>
<cellProperties index="572" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="330"/>
</cell>
<cell>
<cellProperties index="573" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="250"/>
<atomRef index="331"/>
<atomRef index="330"/>
</cell>
<cell>
<cellProperties index="574" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="331"/>
</cell>
<cell>
<cellProperties index="575" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="251"/>
<atomRef index="332"/>
<atomRef index="331"/>
</cell>
<cell>
<cellProperties index="576" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="405"/>
</cell>
<cell>
<cellProperties index="577" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="325"/>
<atomRef index="406"/>
<atomRef index="405"/>
</cell>
<cell>
<cellProperties index="578" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="406"/>
</cell>
<cell>
<cellProperties index="579" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="326"/>
<atomRef index="407"/>
<atomRef index="406"/>
</cell>
<cell>
<cellProperties index="580" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="407"/>
</cell>
<cell>
<cellProperties index="581" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="327"/>
<atomRef index="408"/>
<atomRef index="407"/>
</cell>
<cell>
<cellProperties index="582" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="408"/>
</cell>
<cell>
<cellProperties index="583" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="328"/>
<atomRef index="409"/>
<atomRef index="408"/>
</cell>
<cell>
<cellProperties index="584" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="409"/>
</cell>
<cell>
<cellProperties index="585" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="329"/>
<atomRef index="410"/>
<atomRef index="409"/>
</cell>
<cell>
<cellProperties index="586" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="410"/>
</cell>
<cell>
<cellProperties index="587" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="330"/>
<atomRef index="411"/>
<atomRef index="410"/>
</cell>
<cell>
<cellProperties index="588" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="411"/>
</cell>
<cell>
<cellProperties index="589" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="331"/>
<atomRef index="412"/>
<atomRef index="411"/>
</cell>
<cell>
<cellProperties index="590" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="412"/>
</cell>
<cell>
<cellProperties index="591" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="332"/>
<atomRef index="413"/>
<atomRef index="412"/>
</cell>
<cell>
<cellProperties index="592" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="486"/>
</cell>
<cell>
<cellProperties index="593" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="406"/>
<atomRef index="487"/>
<atomRef index="486"/>
</cell>
<cell>
<cellProperties index="594" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="487"/>
</cell>
<cell>
<cellProperties index="595" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="407"/>
<atomRef index="488"/>
<atomRef index="487"/>
</cell>
<cell>
<cellProperties index="596" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="488"/>
</cell>
<cell>
<cellProperties index="597" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="408"/>
<atomRef index="489"/>
<atomRef index="488"/>
</cell>
<cell>
<cellProperties index="598" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="489"/>
</cell>
<cell>
<cellProperties index="599" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="409"/>
<atomRef index="490"/>
<atomRef index="489"/>
</cell>
<cell>
<cellProperties index="600" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="490"/>
</cell>
<cell>
<cellProperties index="601" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="410"/>
<atomRef index="491"/>
<atomRef index="490"/>
</cell>
<cell>
<cellProperties index="602" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="491"/>
</cell>
<cell>
<cellProperties index="603" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="411"/>
<atomRef index="492"/>
<atomRef index="491"/>
</cell>
<cell>
<cellProperties index="604" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="492"/>
</cell>
<cell>
<cellProperties index="605" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="412"/>
<atomRef index="493"/>
<atomRef index="492"/>
</cell>
<cell>
<cellProperties index="606" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="493"/>
</cell>
<cell>
<cellProperties index="607" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="413"/>
<atomRef index="494"/>
<atomRef index="493"/>
</cell>
<cell>
<cellProperties index="608" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="567"/>
</cell>
<cell>
<cellProperties index="609" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="487"/>
<atomRef index="568"/>
<atomRef index="567"/>
</cell>
<cell>
<cellProperties index="610" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="568"/>
</cell>
<cell>
<cellProperties index="611" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="488"/>
<atomRef index="569"/>
<atomRef index="568"/>
</cell>
<cell>
<cellProperties index="612" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="569"/>
</cell>
<cell>
<cellProperties index="613" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="489"/>
<atomRef index="570"/>
<atomRef index="569"/>
</cell>
<cell>
<cellProperties index="614" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="570"/>
</cell>
<cell>
<cellProperties index="615" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="490"/>
<atomRef index="571"/>
<atomRef index="570"/>
</cell>
<cell>
<cellProperties index="616" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="571"/>
</cell>
<cell>
<cellProperties index="617" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="491"/>
<atomRef index="572"/>
<atomRef index="571"/>
</cell>
<cell>
<cellProperties index="618" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="572"/>
</cell>
<cell>
<cellProperties index="619" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="492"/>
<atomRef index="573"/>
<atomRef index="572"/>
</cell>
<cell>
<cellProperties index="620" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="573"/>
</cell>
<cell>
<cellProperties index="621" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="493"/>
<atomRef index="574"/>
<atomRef index="573"/>
</cell>
<cell>
<cellProperties index="622" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="574"/>
</cell>
<cell>
<cellProperties index="623" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="494"/>
<atomRef index="575"/>
<atomRef index="574"/>
</cell>
<cell>
<cellProperties index="624" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="648"/>
</cell>
<cell>
<cellProperties index="625" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="568"/>
<atomRef index="649"/>
<atomRef index="648"/>
</cell>
<cell>
<cellProperties index="626" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="649"/>
</cell>
<cell>
<cellProperties index="627" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="569"/>
<atomRef index="650"/>
<atomRef index="649"/>
</cell>
<cell>
<cellProperties index="628" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="650"/>
</cell>
<cell>
<cellProperties index="629" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="570"/>
<atomRef index="651"/>
<atomRef index="650"/>
</cell>
<cell>
<cellProperties index="630" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="651"/>
</cell>
<cell>
<cellProperties index="631" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="571"/>
<atomRef index="652"/>
<atomRef index="651"/>
</cell>
<cell>
<cellProperties index="632" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="652"/>
</cell>
<cell>
<cellProperties index="633" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="572"/>
<atomRef index="653"/>
<atomRef index="652"/>
</cell>
<cell>
<cellProperties index="634" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="653"/>
</cell>
<cell>
<cellProperties index="635" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="573"/>
<atomRef index="654"/>
<atomRef index="653"/>
</cell>
<cell>
<cellProperties index="636" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="654"/>
</cell>
<cell>
<cellProperties index="637" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="574"/>
<atomRef index="655"/>
<atomRef index="654"/>
</cell>
<cell>
<cellProperties index="638" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="655"/>
</cell>
<cell>
<cellProperties index="639" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="575"/>
<atomRef index="656"/>
<atomRef index="655"/>
</cell>
<cell>
<cellProperties index="640" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="72"/>
<atomRef index="153"/>
<atomRef index="73"/>
</cell>
<cell>
<cellProperties index="641" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="73"/>
</cell>
<cell>
<cellProperties index="642" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="73"/>
<atomRef index="154"/>
<atomRef index="74"/>
</cell>
<cell>
<cellProperties index="643" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="74"/>
</cell>
<cell>
<cellProperties index="644" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="74"/>
<atomRef index="155"/>
<atomRef index="75"/>
</cell>
<cell>
<cellProperties index="645" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="75"/>
</cell>
<cell>
<cellProperties index="646" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="75"/>
<atomRef index="156"/>
<atomRef index="76"/>
</cell>
<cell>
<cellProperties index="647" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="76"/>
</cell>
<cell>
<cellProperties index="648" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="76"/>
<atomRef index="157"/>
<atomRef index="77"/>
</cell>
<cell>
<cellProperties index="649" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="77"/>
</cell>
<cell>
<cellProperties index="650" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="77"/>
<atomRef index="158"/>
<atomRef index="78"/>
</cell>
<cell>
<cellProperties index="651" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="78"/>
</cell>
<cell>
<cellProperties index="652" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="78"/>
<atomRef index="159"/>
<atomRef index="79"/>
</cell>
<cell>
<cellProperties index="653" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="79"/>
</cell>
<cell>
<cellProperties index="654" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="79"/>
<atomRef index="160"/>
<atomRef index="80"/>
</cell>
<cell>
<cellProperties index="655" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="80"/>
</cell>
<cell>
<cellProperties index="656" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="153"/>
<atomRef index="234"/>
<atomRef index="154"/>
</cell>
<cell>
<cellProperties index="657" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="154"/>
</cell>
<cell>
<cellProperties index="658" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="154"/>
<atomRef index="235"/>
<atomRef index="155"/>
</cell>
<cell>
<cellProperties index="659" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="155"/>
</cell>
<cell>
<cellProperties index="660" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="155"/>
<atomRef index="236"/>
<atomRef index="156"/>
</cell>
<cell>
<cellProperties index="661" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="156"/>
</cell>
<cell>
<cellProperties index="662" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="156"/>
<atomRef index="237"/>
<atomRef index="157"/>
</cell>
<cell>
<cellProperties index="663" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="157"/>
</cell>
<cell>
<cellProperties index="664" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="157"/>
<atomRef index="238"/>
<atomRef index="158"/>
</cell>
<cell>
<cellProperties index="665" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="158"/>
</cell>
<cell>
<cellProperties index="666" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="158"/>
<atomRef index="239"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="667" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="668" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="159"/>
<atomRef index="240"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="669" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="670" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="160"/>
<atomRef index="241"/>
<atomRef index="161"/>
</cell>
<cell>
<cellProperties index="671" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="161"/>
</cell>
<cell>
<cellProperties index="672" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="234"/>
<atomRef index="315"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="673" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="674" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="235"/>
<atomRef index="316"/>
<atomRef index="236"/>
</cell>
<cell>
<cellProperties index="675" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="236"/>
</cell>
<cell>
<cellProperties index="676" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="236"/>
<atomRef index="317"/>
<atomRef index="237"/>
</cell>
<cell>
<cellProperties index="677" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="237"/>
</cell>
<cell>
<cellProperties index="678" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="237"/>
<atomRef index="318"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="679" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="680" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="238"/>
<atomRef index="319"/>
<atomRef index="239"/>
</cell>
<cell>
<cellProperties index="681" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="239"/>
</cell>
<cell>
<cellProperties index="682" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="239"/>
<atomRef index="320"/>
<atomRef index="240"/>
</cell>
<cell>
<cellProperties index="683" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="240"/>
</cell>
<cell>
<cellProperties index="684" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="240"/>
<atomRef index="321"/>
<atomRef index="241"/>
</cell>
<cell>
<cellProperties index="685" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="241"/>
</cell>
<cell>
<cellProperties index="686" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="241"/>
<atomRef index="322"/>
<atomRef index="242"/>
</cell>
<cell>
<cellProperties index="687" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="242"/>
</cell>
<cell>
<cellProperties index="688" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="315"/>
<atomRef index="396"/>
<atomRef index="316"/>
</cell>
<cell>
<cellProperties index="689" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="316"/>
</cell>
<cell>
<cellProperties index="690" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="316"/>
<atomRef index="397"/>
<atomRef index="317"/>
</cell>
<cell>
<cellProperties index="691" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="317"/>
</cell>
<cell>
<cellProperties index="692" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="317"/>
<atomRef index="398"/>
<atomRef index="318"/>
</cell>
<cell>
<cellProperties index="693" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="318"/>
</cell>
<cell>
<cellProperties index="694" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="318"/>
<atomRef index="399"/>
<atomRef index="319"/>
</cell>
<cell>
<cellProperties index="695" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="319"/>
</cell>
<cell>
<cellProperties index="696" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="319"/>
<atomRef index="400"/>
<atomRef index="320"/>
</cell>
<cell>
<cellProperties index="697" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="320"/>
</cell>
<cell>
<cellProperties index="698" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="320"/>
<atomRef index="401"/>
<atomRef index="321"/>
</cell>
<cell>
<cellProperties index="699" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="321"/>
</cell>
<cell>
<cellProperties index="700" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="321"/>
<atomRef index="402"/>
<atomRef index="322"/>
</cell>
<cell>
<cellProperties index="701" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="322"/>
</cell>
<cell>
<cellProperties index="702" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="322"/>
<atomRef index="403"/>
<atomRef index="323"/>
</cell>
<cell>
<cellProperties index="703" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="323"/>
</cell>
<cell>
<cellProperties index="704" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="396"/>
<atomRef index="477"/>
<atomRef index="397"/>
</cell>
<cell>
<cellProperties index="705" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="397"/>
</cell>
<cell>
<cellProperties index="706" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="397"/>
<atomRef index="478"/>
<atomRef index="398"/>
</cell>
<cell>
<cellProperties index="707" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="398"/>
</cell>
<cell>
<cellProperties index="708" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="398"/>
<atomRef index="479"/>
<atomRef index="399"/>
</cell>
<cell>
<cellProperties index="709" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="399"/>
</cell>
<cell>
<cellProperties index="710" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="399"/>
<atomRef index="480"/>
<atomRef index="400"/>
</cell>
<cell>
<cellProperties index="711" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="400"/>
</cell>
<cell>
<cellProperties index="712" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="400"/>
<atomRef index="481"/>
<atomRef index="401"/>
</cell>
<cell>
<cellProperties index="713" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="401"/>
</cell>
<cell>
<cellProperties index="714" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="401"/>
<atomRef index="482"/>
<atomRef index="402"/>
</cell>
<cell>
<cellProperties index="715" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="402"/>
</cell>
<cell>
<cellProperties index="716" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="402"/>
<atomRef index="483"/>
<atomRef index="403"/>
</cell>
<cell>
<cellProperties index="717" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="403"/>
</cell>
<cell>
<cellProperties index="718" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="403"/>
<atomRef index="484"/>
<atomRef index="404"/>
</cell>
<cell>
<cellProperties index="719" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="404"/>
</cell>
<cell>
<cellProperties index="720" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="477"/>
<atomRef index="558"/>
<atomRef index="478"/>
</cell>
<cell>
<cellProperties index="721" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="478"/>
</cell>
<cell>
<cellProperties index="722" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="478"/>
<atomRef index="559"/>
<atomRef index="479"/>
</cell>
<cell>
<cellProperties index="723" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="479"/>
</cell>
<cell>
<cellProperties index="724" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="479"/>
<atomRef index="560"/>
<atomRef index="480"/>
</cell>
<cell>
<cellProperties index="725" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="480"/>
</cell>
<cell>
<cellProperties index="726" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="480"/>
<atomRef index="561"/>
<atomRef index="481"/>
</cell>
<cell>
<cellProperties index="727" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="481"/>
</cell>
<cell>
<cellProperties index="728" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="481"/>
<atomRef index="562"/>
<atomRef index="482"/>
</cell>
<cell>
<cellProperties index="729" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="482"/>
</cell>
<cell>
<cellProperties index="730" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="482"/>
<atomRef index="563"/>
<atomRef index="483"/>
</cell>
<cell>
<cellProperties index="731" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="483"/>
</cell>
<cell>
<cellProperties index="732" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="483"/>
<atomRef index="564"/>
<atomRef index="484"/>
</cell>
<cell>
<cellProperties index="733" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="484"/>
</cell>
<cell>
<cellProperties index="734" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="484"/>
<atomRef index="565"/>
<atomRef index="485"/>
</cell>
<cell>
<cellProperties index="735" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="485"/>
</cell>
<cell>
<cellProperties index="736" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="558"/>
<atomRef index="639"/>
<atomRef index="559"/>
</cell>
<cell>
<cellProperties index="737" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="559"/>
</cell>
<cell>
<cellProperties index="738" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="559"/>
<atomRef index="640"/>
<atomRef index="560"/>
</cell>
<cell>
<cellProperties index="739" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="560"/>
</cell>
<cell>
<cellProperties index="740" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="560"/>
<atomRef index="641"/>
<atomRef index="561"/>
</cell>
<cell>
<cellProperties index="741" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="561"/>
</cell>
<cell>
<cellProperties index="742" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="561"/>
<atomRef index="642"/>
<atomRef index="562"/>
</cell>
<cell>
<cellProperties index="743" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="562"/>
</cell>
<cell>
<cellProperties index="744" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="562"/>
<atomRef index="643"/>
<atomRef index="563"/>
</cell>
<cell>
<cellProperties index="745" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="563"/>
</cell>
<cell>
<cellProperties index="746" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="563"/>
<atomRef index="644"/>
<atomRef index="564"/>
</cell>
<cell>
<cellProperties index="747" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="564"/>
</cell>
<cell>
<cellProperties index="748" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="564"/>
<atomRef index="645"/>
<atomRef index="565"/>
</cell>
<cell>
<cellProperties index="749" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="565"/>
</cell>
<cell>
<cellProperties index="750" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="565"/>
<atomRef index="646"/>
<atomRef index="566"/>
</cell>
<cell>
<cellProperties index="751" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="566"/>
</cell>
<cell>
<cellProperties index="752" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="639"/>
<atomRef index="720"/>
<atomRef index="640"/>
</cell>
<cell>
<cellProperties index="753" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="640"/>
</cell>
<cell>
<cellProperties index="754" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="640"/>
<atomRef index="721"/>
<atomRef index="641"/>
</cell>
<cell>
<cellProperties index="755" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="641"/>
</cell>
<cell>
<cellProperties index="756" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="641"/>
<atomRef index="722"/>
<atomRef index="642"/>
</cell>
<cell>
<cellProperties index="757" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="642"/>
</cell>
<cell>
<cellProperties index="758" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="642"/>
<atomRef index="723"/>
<atomRef index="643"/>
</cell>
<cell>
<cellProperties index="759" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="723"/>
<atomRef index="724"/>
<atomRef index="643"/>
</cell>
<cell>
<cellProperties index="760" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="643"/>
<atomRef index="724"/>
<atomRef index="644"/>
</cell>
<cell>
<cellProperties index="761" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="644"/>
</cell>
<cell>
<cellProperties index="762" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="644"/>
<atomRef index="725"/>
<atomRef index="645"/>
</cell>
<cell>
<cellProperties index="763" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="725"/>
<atomRef index="726"/>
<atomRef index="645"/>
</cell>
<cell>
<cellProperties index="764" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="645"/>
<atomRef index="726"/>
<atomRef index="646"/>
</cell>
<cell>
<cellProperties index="765" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="726"/>
<atomRef index="727"/>
<atomRef index="646"/>
</cell>
<cell>
<cellProperties index="766" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="646"/>
<atomRef index="727"/>
<atomRef index="647"/>
</cell>
<cell>
<cellProperties index="767" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="727"/>
<atomRef index="728"/>
<atomRef index="647"/>
</cell>
</structuralComponent>
</multiComponent>
<structuralComponent name="Regions" mass="1" targetPosition="0 0 0" viscosityW="1">
<nrOfStructures value="1"/>
<cell>
<cellProperties index="768" type="POLY_VERTEX" name="cubeVol-Sub8" attractorType="MASS_SPRING" materialType="elastic" shapeW="100"/>
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="729"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="90"/>
<atomRef index="91"/>
<atomRef index="92"/>
<atomRef index="93"/>
<atomRef index="94"/>
<atomRef index="95"/>
<atomRef index="96"/>
<atomRef index="97"/>
<atomRef index="98"/>
<atomRef index="99"/>
<atomRef index="100"/>
<atomRef index="101"/>
<atomRef index="102"/>
<atomRef index="103"/>
<atomRef index="104"/>
<atomRef index="105"/>
<atomRef index="106"/>
<atomRef index="107"/>
<atomRef index="108"/>
<atomRef index="109"/>
<atomRef index="110"/>
<atomRef index="111"/>
<atomRef index="112"/>
<atomRef index="113"/>
<atomRef index="114"/>
<atomRef index="115"/>
<atomRef index="116"/>
<atomRef index="117"/>
<atomRef index="118"/>
<atomRef index="119"/>
<atomRef index="120"/>
<atomRef index="121"/>
<atomRef index="122"/>
<atomRef index="123"/>
<atomRef index="124"/>
<atomRef index="125"/>
<atomRef index="126"/>
<atomRef index="127"/>
<atomRef index="128"/>
<atomRef index="129"/>
<atomRef index="130"/>
<atomRef index="131"/>
<atomRef index="132"/>
<atomRef index="133"/>
<atomRef index="134"/>
<atomRef index="135"/>
<atomRef index="136"/>
<atomRef index="137"/>
<atomRef index="138"/>
<atomRef index="139"/>
<atomRef index="140"/>
<atomRef index="141"/>
<atomRef index="142"/>
<atomRef index="143"/>
<atomRef index="144"/>
<atomRef index="145"/>
<atomRef index="146"/>
<atomRef index="147"/>
<atomRef index="148"/>
<atomRef index="149"/>
<atomRef index="150"/>
<atomRef index="151"/>
<atomRef index="152"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="171"/>
<atomRef index="172"/>
<atomRef index="173"/>
<atomRef index="174"/>
<atomRef index="175"/>
<atomRef index="176"/>
<atomRef index="177"/>
<atomRef index="178"/>
<atomRef index="179"/>
<atomRef index="180"/>
<atomRef index="181"/>
<atomRef index="182"/>
<atomRef index="183"/>
<atomRef index="184"/>
<atomRef index="185"/>
<atomRef index="186"/>
<atomRef index="187"/>
<atomRef index="188"/>
<atomRef index="189"/>
<atomRef index="190"/>
<atomRef index="191"/>
<atomRef index="192"/>
<atomRef index="193"/>
<atomRef index="194"/>
<atomRef index="195"/>
<atomRef index="196"/>
<atomRef index="197"/>
<atomRef index="198"/>
<atomRef index="199"/>
<atomRef index="200"/>
<atomRef index="201"/>
<atomRef index="202"/>
<atomRef index="203"/>
<atomRef index="204"/>
<atomRef index="205"/>
<atomRef index="206"/>
<atomRef index="207"/>
<atomRef index="208"/>
<atomRef index="209"/>
<atomRef index="210"/>
<atomRef index="211"/>
<atomRef index="212"/>
<atomRef index="213"/>
<atomRef index="214"/>
<atomRef index="215"/>
<atomRef index="216"/>
<atomRef index="217"/>
<atomRef index="218"/>
<atomRef index="219"/>
<atomRef index="220"/>
<atomRef index="221"/>
<atomRef index="222"/>
<atomRef index="223"/>
<atomRef index="224"/>
<atomRef index="225"/>
<atomRef index="226"/>
<atomRef index="227"/>
<atomRef index="228"/>
<atomRef index="229"/>
<atomRef index="230"/>
<atomRef index="231"/>
<atomRef index="232"/>
<atomRef index="233"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="252"/>
<atomRef index="253"/>
<atomRef index="254"/>
<atomRef index="255"/>
<atomRef index="256"/>
<atomRef index="257"/>
<atomRef index="258"/>
<atomRef index="259"/>
<atomRef index="260"/>
<atomRef index="261"/>
<atomRef index="262"/>
<atomRef index="263"/>
<atomRef index="264"/>
<atomRef index="265"/>
<atomRef index="266"/>
<atomRef index="267"/>
<atomRef index="268"/>
<atomRef index="269"/>
<atomRef index="270"/>
<atomRef index="271"/>
<atomRef index="272"/>
<atomRef index="273"/>
<atomRef index="274"/>
<atomRef index="275"/>
<atomRef index="276"/>
<atomRef index="277"/>
<atomRef index="278"/>
<atomRef index="279"/>
<atomRef index="280"/>
<atomRef index="281"/>
<atomRef index="282"/>
<atomRef index="283"/>
<atomRef index="284"/>
<atomRef index="285"/>
<atomRef index="286"/>
<atomRef index="287"/>
<atomRef index="288"/>
<atomRef index="289"/>
<atomRef index="290"/>
<atomRef index="291"/>
<atomRef index="292"/>
<atomRef index="293"/>
<atomRef index="294"/>
<atomRef index="295"/>
<atomRef index="296"/>
<atomRef index="297"/>
<atomRef index="298"/>
<atomRef index="299"/>
<atomRef index="300"/>
<atomRef index="301"/>
<atomRef index="302"/>
<atomRef index="303"/>
<atomRef index="304"/>
<atomRef index="305"/>
<atomRef index="306"/>
<atomRef index="307"/>
<atomRef index="308"/>
<atomRef index="309"/>
<atomRef index="310"/>
<atomRef index="311"/>
<atomRef index="312"/>
<atomRef index="313"/>
<atomRef index="314"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="333"/>
<atomRef index="334"/>
<atomRef index="335"/>
<atomRef index="336"/>
<atomRef index="337"/>
<atomRef index="338"/>
<atomRef index="339"/>
<atomRef index="340"/>
<atomRef index="341"/>
<atomRef index="342"/>
<atomRef index="343"/>
<atomRef index="344"/>
<atomRef index="345"/>
<atomRef index="346"/>
<atomRef index="347"/>
<atomRef index="348"/>
<atomRef index="349"/>
<atomRef index="350"/>
<atomRef index="351"/>
<atomRef index="352"/>
<atomRef index="353"/>
<atomRef index="354"/>
<atomRef index="355"/>
<atomRef index="356"/>
<atomRef index="357"/>
<atomRef index="358"/>
<atomRef index="359"/>
<atomRef index="360"/>
<atomRef index="361"/>
<atomRef index="362"/>
<atomRef index="363"/>
<atomRef index="364"/>
<atomRef index="365"/>
<atomRef index="366"/>
<atomRef index="367"/>
<atomRef index="368"/>
<atomRef index="369"/>
<atomRef index="370"/>
<atomRef index="371"/>
<atomRef index="372"/>
<atomRef index="373"/>
<atomRef index="374"/>
<atomRef index="375"/>
<atomRef index="376"/>
<atomRef index="377"/>
<atomRef index="378"/>
<atomRef index="379"/>
<atomRef index="380"/>
<atomRef index="381"/>
<atomRef index="382"/>
<atomRef index="383"/>
<atomRef index="384"/>
<atomRef index="385"/>
<atomRef index="386"/>
<atomRef index="387"/>
<atomRef index="388"/>
<atomRef index="389"/>
<atomRef index="390"/>
<atomRef index="391"/>
<atomRef index="392"/>
<atomRef index="393"/>
<atomRef index="394"/>
<atomRef index="395"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="414"/>
<atomRef index="415"/>
<atomRef index="416"/>
<atomRef index="417"/>
<atomRef index="418"/>
<atomRef index="419"/>
<atomRef index="420"/>
<atomRef index="421"/>
<atomRef index="422"/>
<atomRef index="423"/>
<atomRef index="424"/>
<atomRef index="425"/>
<atomRef index="426"/>
<atomRef index="427"/>
<atomRef index="428"/>
<atomRef index="429"/>
<atomRef index="430"/>
<atomRef index="431"/>
<atomRef index="432"/>
<atomRef index="433"/>
<atomRef index="434"/>
<atomRef index="435"/>
<atomRef index="436"/>
<atomRef index="437"/>
<atomRef index="438"/>
<atomRef index="439"/>
<atomRef index="440"/>
<atomRef index="441"/>
<atomRef index="442"/>
<atomRef index="443"/>
<atomRef index="444"/>
<atomRef index="445"/>
<atomRef index="446"/>
<atomRef index="447"/>
<atomRef index="448"/>
<atomRef index="449"/>
<atomRef index="450"/>
<atomRef index="451"/>
<atomRef index="452"/>
<atomRef index="453"/>
<atomRef index="454"/>
<atomRef index="455"/>
<atomRef index="456"/>
<atomRef index="457"/>
<atomRef index="458"/>
<atomRef index="459"/>
<atomRef index="460"/>
<atomRef index="461"/>
<atomRef index="462"/>
<atomRef index="463"/>
<atomRef index="464"/>
<atomRef index="465"/>
<atomRef index="466"/>
<atomRef index="467"/>
<atomRef index="468"/>
<atomRef index="469"/>
<atomRef index="470"/>
<atomRef index="471"/>
<atomRef index="472"/>
<atomRef index="473"/>
<atomRef index="474"/>
<atomRef index="475"/>
<atomRef index="476"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="495"/>
<atomRef index="496"/>
<atomRef index="497"/>
<atomRef index="498"/>
<atomRef index="499"/>
<atomRef index="500"/>
<atomRef index="501"/>
<atomRef index="502"/>
<atomRef index="503"/>
<atomRef index="504"/>
<atomRef index="505"/>
<atomRef index="506"/>
<atomRef index="507"/>
<atomRef index="508"/>
<atomRef index="509"/>
<atomRef index="510"/>
<atomRef index="511"/>
<atomRef index="512"/>
<atomRef index="513"/>
<atomRef index="514"/>
<atomRef index="515"/>
<atomRef index="516"/>
<atomRef index="517"/>
<atomRef index="518"/>
<atomRef index="519"/>
<atomRef index="520"/>
<atomRef index="521"/>
<atomRef index="522"/>
<atomRef index="523"/>
<atomRef index="524"/>
<atomRef index="525"/>
<atomRef index="526"/>
<atomRef index="527"/>
<atomRef index="528"/>
<atomRef index="529"/>
<atomRef index="530"/>
<atomRef index="531"/>
<atomRef index="532"/>
<atomRef index="533"/>
<atomRef index="534"/>
<atomRef index="535"/>
<atomRef index="536"/>
<atomRef index="537"/>
<atomRef index="538"/>
<atomRef index="539"/>
<atomRef index="540"/>
<atomRef index="541"/>
<atomRef index="542"/>
<atomRef index="543"/>
<atomRef index="544"/>
<atomRef index="545"/>
<atomRef index="546"/>
<atomRef index="547"/>
<atomRef index="548"/>
<atomRef index="549"/>
<atomRef index="550"/>
<atomRef index="551"/>
<atomRef index="552"/>
<atomRef index="553"/>
<atomRef index="554"/>
<atomRef index="555"/>
<atomRef index="556"/>
<atomRef index="557"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="576"/>
<atomRef index="577"/>
<atomRef index="578"/>
<atomRef index="579"/>
<atomRef index="580"/>
<atomRef index="581"/>
<atomRef index="582"/>
<atomRef index="583"/>
<atomRef index="584"/>
<atomRef index="585"/>
<atomRef index="586"/>
<atomRef index="587"/>
<atomRef index="588"/>
<atomRef index="589"/>
<atomRef index="590"/>
<atomRef index="591"/>
<atomRef index="592"/>
<atomRef index="593"/>
<atomRef index="594"/>
<atomRef index="595"/>
<atomRef index="596"/>
<atomRef index="597"/>
<atomRef index="598"/>
<atomRef index="599"/>
<atomRef index="600"/>
<atomRef index="601"/>
<atomRef index="602"/>
<atomRef index="603"/>
<atomRef index="604"/>
<atomRef index="605"/>
<atomRef index="606"/>
<atomRef index="607"/>
<atomRef index="608"/>
<atomRef index="609"/>
<atomRef index="610"/>
<atomRef index="611"/>
<atomRef index="612"/>
<atomRef index="613"/>
<atomRef index="614"/>
<atomRef index="615"/>
<atomRef index="616"/>
<atomRef index="617"/>
<atomRef index="618"/>
<atomRef index="619"/>
<atomRef index="620"/>
<atomRef index="621"/>
<atomRef index="622"/>
<atomRef index="623"/>
<atomRef index="624"/>
<atomRef index="625"/>
<atomRef index="626"/>
<atomRef index="627"/>
<atomRef index="628"/>
<atomRef index="629"/>
<atomRef index="630"/>
<atomRef index="631"/>
<atomRef index="632"/>
<atomRef index="633"/>
<atomRef index="634"/>
<atomRef index="635"/>
<atomRef index="636"/>
<atomRef index="637"/>
<atomRef index="638"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="651"/>
<atomRef index="652"/>
<atomRef index="653"/>
<atomRef index="654"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="726"/>
<atomRef index="727"/>
<atomRef index="728"/>
</cell>
</structuralComponent>
<structuralComponent name="Neighborhoods" mode="WIREFRAME_AND_SURFACE">
<nrOfStructures value="729"/>
<cell>
<cellProperties index="769" type="POLY_VERTEX" name="Atom #0"/>
<nrOfStructures value="6"/>
<atomRef index="1"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="90"/>
</cell>
<cell>
<cellProperties index="770" type="POLY_VERTEX" name="Atom #1"/>
<nrOfStructures value="10"/>
<atomRef index="0"/>
<atomRef index="2"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="90"/>
<atomRef index="91"/>
</cell>
<cell>
<cellProperties index="771" type="POLY_VERTEX" name="Atom #2"/>
<nrOfStructures value="10"/>
<atomRef index="1"/>
<atomRef index="3"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="91"/>
<atomRef index="92"/>
</cell>
<cell>
<cellProperties index="772" type="POLY_VERTEX" name="Atom #3"/>
<nrOfStructures value="10"/>
<atomRef index="2"/>
<atomRef index="4"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="92"/>
<atomRef index="93"/>
</cell>
<cell>
<cellProperties index="773" type="POLY_VERTEX" name="Atom #4"/>
<nrOfStructures value="10"/>
<atomRef index="3"/>
<atomRef index="5"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="93"/>
<atomRef index="94"/>
</cell>
<cell>
<cellProperties index="774" type="POLY_VERTEX" name="Atom #5"/>
<nrOfStructures value="10"/>
<atomRef index="4"/>
<atomRef index="6"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="94"/>
<atomRef index="95"/>
</cell>
<cell>
<cellProperties index="775" type="POLY_VERTEX" name="Atom #6"/>
<nrOfStructures value="10"/>
<atomRef index="5"/>
<atomRef index="7"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="95"/>
<atomRef index="96"/>
</cell>
<cell>
<cellProperties index="776" type="POLY_VERTEX" name="Atom #7"/>
<nrOfStructures value="11"/>
<atomRef index="6"/>
<atomRef index="8"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="96"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="777" type="POLY_VERTEX" name="Atom #8"/>
<nrOfStructures value="7"/>
<atomRef index="7"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="778" type="POLY_VERTEX" name="Atom #9"/>
<nrOfStructures value="9"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="10"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="90"/>
<atomRef index="99"/>
</cell>
<cell>
<cellProperties index="779" type="POLY_VERTEX" name="Atom #10"/>
<nrOfStructures value="17"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="9"/>
<atomRef index="11"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="90"/>
<atomRef index="99"/>
<atomRef index="91"/>
<atomRef index="100"/>
<atomRef index="92"/>
<atomRef index="101"/>
</cell>
<cell>
<cellProperties index="780" type="POLY_VERTEX" name="Atom #11"/>
<nrOfStructures value="15"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="10"/>
<atomRef index="12"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="91"/>
<atomRef index="100"/>
<atomRef index="92"/>
<atomRef index="101"/>
</cell>
<cell>
<cellProperties index="781" type="POLY_VERTEX" name="Atom #12"/>
<nrOfStructures value="16"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="11"/>
<atomRef index="13"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="92"/>
<atomRef index="101"/>
<atomRef index="93"/>
<atomRef index="102"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="782" type="POLY_VERTEX" name="Atom #13"/>
<nrOfStructures value="15"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="12"/>
<atomRef index="14"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="93"/>
<atomRef index="102"/>
<atomRef index="94"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="783" type="POLY_VERTEX" name="Atom #14"/>
<nrOfStructures value="15"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="13"/>
<atomRef index="15"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="94"/>
<atomRef index="103"/>
<atomRef index="95"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="784" type="POLY_VERTEX" name="Atom #15"/>
<nrOfStructures value="15"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="14"/>
<atomRef index="16"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="95"/>
<atomRef index="104"/>
<atomRef index="96"/>
<atomRef index="105"/>
</cell>
<cell>
<cellProperties index="785" type="POLY_VERTEX" name="Atom #16"/>
<nrOfStructures value="17"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="15"/>
<atomRef index="17"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="96"/>
<atomRef index="105"/>
<atomRef index="97"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="786" type="POLY_VERTEX" name="Atom #17"/>
<nrOfStructures value="11"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="16"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="97"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="787" type="POLY_VERTEX" name="Atom #18"/>
<nrOfStructures value="8"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="19"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="90"/>
<atomRef index="99"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="788" type="POLY_VERTEX" name="Atom #19"/>
<nrOfStructures value="14"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="18"/>
<atomRef index="20"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="90"/>
<atomRef index="99"/>
<atomRef index="108"/>
<atomRef index="91"/>
<atomRef index="100"/>
<atomRef index="109"/>
</cell>
<cell>
<cellProperties index="789" type="POLY_VERTEX" name="Atom #20"/>
<nrOfStructures value="14"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="19"/>
<atomRef index="21"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="91"/>
<atomRef index="100"/>
<atomRef index="109"/>
<atomRef index="92"/>
<atomRef index="101"/>
<atomRef index="110"/>
</cell>
<cell>
<cellProperties index="790" type="POLY_VERTEX" name="Atom #21"/>
<nrOfStructures value="15"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="20"/>
<atomRef index="22"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="92"/>
<atomRef index="101"/>
<atomRef index="110"/>
<atomRef index="93"/>
<atomRef index="102"/>
<atomRef index="111"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="791" type="POLY_VERTEX" name="Atom #22"/>
<nrOfStructures value="14"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="21"/>
<atomRef index="23"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="93"/>
<atomRef index="102"/>
<atomRef index="111"/>
<atomRef index="94"/>
<atomRef index="103"/>
<atomRef index="112"/>
</cell>
<cell>
<cellProperties index="792" type="POLY_VERTEX" name="Atom #23"/>
<nrOfStructures value="14"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="22"/>
<atomRef index="24"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="94"/>
<atomRef index="103"/>
<atomRef index="112"/>
<atomRef index="95"/>
<atomRef index="104"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="793" type="POLY_VERTEX" name="Atom #24"/>
<nrOfStructures value="14"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="23"/>
<atomRef index="25"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="95"/>
<atomRef index="104"/>
<atomRef index="113"/>
<atomRef index="96"/>
<atomRef index="105"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="794" type="POLY_VERTEX" name="Atom #25"/>
<nrOfStructures value="17"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="24"/>
<atomRef index="26"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="96"/>
<atomRef index="105"/>
<atomRef index="114"/>
<atomRef index="97"/>
<atomRef index="106"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="795" type="POLY_VERTEX" name="Atom #26"/>
<nrOfStructures value="11"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="25"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="97"/>
<atomRef index="106"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="796" type="POLY_VERTEX" name="Atom #27"/>
<nrOfStructures value="8"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="28"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="99"/>
<atomRef index="108"/>
<atomRef index="117"/>
</cell>
<cell>
<cellProperties index="797" type="POLY_VERTEX" name="Atom #28"/>
<nrOfStructures value="15"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="27"/>
<atomRef index="29"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="99"/>
<atomRef index="108"/>
<atomRef index="117"/>
<atomRef index="100"/>
<atomRef index="109"/>
<atomRef index="118"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="798" type="POLY_VERTEX" name="Atom #29"/>
<nrOfStructures value="14"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="28"/>
<atomRef index="30"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="100"/>
<atomRef index="109"/>
<atomRef index="118"/>
<atomRef index="101"/>
<atomRef index="110"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="799" type="POLY_VERTEX" name="Atom #30"/>
<nrOfStructures value="15"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="29"/>
<atomRef index="31"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="101"/>
<atomRef index="110"/>
<atomRef index="119"/>
<atomRef index="102"/>
<atomRef index="111"/>
<atomRef index="120"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="800" type="POLY_VERTEX" name="Atom #31"/>
<nrOfStructures value="14"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="30"/>
<atomRef index="32"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="102"/>
<atomRef index="111"/>
<atomRef index="120"/>
<atomRef index="103"/>
<atomRef index="112"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="801" type="POLY_VERTEX" name="Atom #32"/>
<nrOfStructures value="14"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="31"/>
<atomRef index="33"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="103"/>
<atomRef index="112"/>
<atomRef index="121"/>
<atomRef index="104"/>
<atomRef index="113"/>
<atomRef index="122"/>
</cell>
<cell>
<cellProperties index="802" type="POLY_VERTEX" name="Atom #33"/>
<nrOfStructures value="15"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="32"/>
<atomRef index="34"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="104"/>
<atomRef index="113"/>
<atomRef index="122"/>
<atomRef index="105"/>
<atomRef index="114"/>
<atomRef index="123"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="803" type="POLY_VERTEX" name="Atom #34"/>
<nrOfStructures value="17"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="33"/>
<atomRef index="35"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="105"/>
<atomRef index="114"/>
<atomRef index="123"/>
<atomRef index="106"/>
<atomRef index="115"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="804" type="POLY_VERTEX" name="Atom #35"/>
<nrOfStructures value="11"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="34"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="106"/>
<atomRef index="115"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="805" type="POLY_VERTEX" name="Atom #36"/>
<nrOfStructures value="8"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="37"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="108"/>
<atomRef index="117"/>
<atomRef index="126"/>
</cell>
<cell>
<cellProperties index="806" type="POLY_VERTEX" name="Atom #37"/>
<nrOfStructures value="15"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="36"/>
<atomRef index="38"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="108"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="109"/>
<atomRef index="118"/>
<atomRef index="127"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="807" type="POLY_VERTEX" name="Atom #38"/>
<nrOfStructures value="14"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="37"/>
<atomRef index="39"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="109"/>
<atomRef index="118"/>
<atomRef index="127"/>
<atomRef index="110"/>
<atomRef index="119"/>
<atomRef index="128"/>
</cell>
<cell>
<cellProperties index="808" type="POLY_VERTEX" name="Atom #39"/>
<nrOfStructures value="14"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="38"/>
<atomRef index="40"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="110"/>
<atomRef index="119"/>
<atomRef index="128"/>
<atomRef index="111"/>
<atomRef index="120"/>
<atomRef index="129"/>
</cell>
<cell>
<cellProperties index="809" type="POLY_VERTEX" name="Atom #40"/>
<nrOfStructures value="14"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="39"/>
<atomRef index="41"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="111"/>
<atomRef index="120"/>
<atomRef index="129"/>
<atomRef index="112"/>
<atomRef index="121"/>
<atomRef index="130"/>
</cell>
<cell>
<cellProperties index="810" type="POLY_VERTEX" name="Atom #41"/>
<nrOfStructures value="14"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="40"/>
<atomRef index="42"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="112"/>
<atomRef index="121"/>
<atomRef index="130"/>
<atomRef index="113"/>
<atomRef index="122"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="811" type="POLY_VERTEX" name="Atom #42"/>
<nrOfStructures value="15"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="41"/>
<atomRef index="43"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="113"/>
<atomRef index="122"/>
<atomRef index="131"/>
<atomRef index="114"/>
<atomRef index="123"/>
<atomRef index="132"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="812" type="POLY_VERTEX" name="Atom #43"/>
<nrOfStructures value="17"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="42"/>
<atomRef index="44"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="114"/>
<atomRef index="123"/>
<atomRef index="132"/>
<atomRef index="115"/>
<atomRef index="124"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="813" type="POLY_VERTEX" name="Atom #44"/>
<nrOfStructures value="11"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="43"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="115"/>
<atomRef index="124"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="814" type="POLY_VERTEX" name="Atom #45"/>
<nrOfStructures value="8"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="46"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="815" type="POLY_VERTEX" name="Atom #46"/>
<nrOfStructures value="15"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="45"/>
<atomRef index="47"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="118"/>
<atomRef index="127"/>
<atomRef index="136"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="816" type="POLY_VERTEX" name="Atom #47"/>
<nrOfStructures value="15"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="46"/>
<atomRef index="48"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="118"/>
<atomRef index="127"/>
<atomRef index="136"/>
<atomRef index="119"/>
<atomRef index="128"/>
<atomRef index="137"/>
<atomRef index="138"/>
</cell>
<cell>
<cellProperties index="817" type="POLY_VERTEX" name="Atom #48"/>
<nrOfStructures value="15"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="47"/>
<atomRef index="49"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="119"/>
<atomRef index="128"/>
<atomRef index="137"/>
<atomRef index="120"/>
<atomRef index="129"/>
<atomRef index="138"/>
<atomRef index="139"/>
</cell>
<cell>
<cellProperties index="818" type="POLY_VERTEX" name="Atom #49"/>
<nrOfStructures value="15"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="48"/>
<atomRef index="50"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="120"/>
<atomRef index="129"/>
<atomRef index="138"/>
<atomRef index="121"/>
<atomRef index="130"/>
<atomRef index="139"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="819" type="POLY_VERTEX" name="Atom #50"/>
<nrOfStructures value="14"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="49"/>
<atomRef index="51"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="121"/>
<atomRef index="130"/>
<atomRef index="139"/>
<atomRef index="122"/>
<atomRef index="131"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="820" type="POLY_VERTEX" name="Atom #51"/>
<nrOfStructures value="15"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="50"/>
<atomRef index="52"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="122"/>
<atomRef index="131"/>
<atomRef index="140"/>
<atomRef index="123"/>
<atomRef index="132"/>
<atomRef index="141"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="821" type="POLY_VERTEX" name="Atom #52"/>
<nrOfStructures value="17"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="51"/>
<atomRef index="53"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="123"/>
<atomRef index="132"/>
<atomRef index="141"/>
<atomRef index="124"/>
<atomRef index="133"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="822" type="POLY_VERTEX" name="Atom #53"/>
<nrOfStructures value="11"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="52"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="124"/>
<atomRef index="133"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="823" type="POLY_VERTEX" name="Atom #54"/>
<nrOfStructures value="9"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="55"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="824" type="POLY_VERTEX" name="Atom #55"/>
<nrOfStructures value="15"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="54"/>
<atomRef index="56"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="127"/>
<atomRef index="136"/>
<atomRef index="145"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="825" type="POLY_VERTEX" name="Atom #56"/>
<nrOfStructures value="16"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="55"/>
<atomRef index="57"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="127"/>
<atomRef index="136"/>
<atomRef index="145"/>
<atomRef index="128"/>
<atomRef index="137"/>
<atomRef index="146"/>
<atomRef index="138"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="826" type="POLY_VERTEX" name="Atom #57"/>
<nrOfStructures value="16"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="56"/>
<atomRef index="58"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="128"/>
<atomRef index="137"/>
<atomRef index="146"/>
<atomRef index="129"/>
<atomRef index="138"/>
<atomRef index="147"/>
<atomRef index="139"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="827" type="POLY_VERTEX" name="Atom #58"/>
<nrOfStructures value="15"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="57"/>
<atomRef index="59"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="129"/>
<atomRef index="138"/>
<atomRef index="147"/>
<atomRef index="130"/>
<atomRef index="139"/>
<atomRef index="148"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="828" type="POLY_VERTEX" name="Atom #59"/>
<nrOfStructures value="15"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="58"/>
<atomRef index="60"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="130"/>
<atomRef index="139"/>
<atomRef index="148"/>
<atomRef index="131"/>
<atomRef index="140"/>
<atomRef index="149"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="829" type="POLY_VERTEX" name="Atom #60"/>
<nrOfStructures value="14"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="59"/>
<atomRef index="61"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="131"/>
<atomRef index="140"/>
<atomRef index="149"/>
<atomRef index="132"/>
<atomRef index="141"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="830" type="POLY_VERTEX" name="Atom #61"/>
<nrOfStructures value="17"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="60"/>
<atomRef index="62"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="132"/>
<atomRef index="141"/>
<atomRef index="150"/>
<atomRef index="133"/>
<atomRef index="142"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="831" type="POLY_VERTEX" name="Atom #62"/>
<nrOfStructures value="11"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="61"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="133"/>
<atomRef index="142"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="832" type="POLY_VERTEX" name="Atom #63"/>
<nrOfStructures value="10"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="64"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="833" type="POLY_VERTEX" name="Atom #64"/>
<nrOfStructures value="16"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="63"/>
<atomRef index="65"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="136"/>
<atomRef index="145"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="834" type="POLY_VERTEX" name="Atom #65"/>
<nrOfStructures value="17"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="64"/>
<atomRef index="66"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="136"/>
<atomRef index="145"/>
<atomRef index="137"/>
<atomRef index="146"/>
<atomRef index="138"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="835" type="POLY_VERTEX" name="Atom #66"/>
<nrOfStructures value="17"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="65"/>
<atomRef index="67"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="137"/>
<atomRef index="146"/>
<atomRef index="138"/>
<atomRef index="147"/>
<atomRef index="139"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="836" type="POLY_VERTEX" name="Atom #67"/>
<nrOfStructures value="16"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="66"/>
<atomRef index="68"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="138"/>
<atomRef index="147"/>
<atomRef index="139"/>
<atomRef index="148"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="837" type="POLY_VERTEX" name="Atom #68"/>
<nrOfStructures value="16"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="67"/>
<atomRef index="69"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="139"/>
<atomRef index="148"/>
<atomRef index="140"/>
<atomRef index="149"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="838" type="POLY_VERTEX" name="Atom #69"/>
<nrOfStructures value="15"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="68"/>
<atomRef index="70"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="140"/>
<atomRef index="149"/>
<atomRef index="141"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="839" type="POLY_VERTEX" name="Atom #70"/>
<nrOfStructures value="17"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="69"/>
<atomRef index="71"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="141"/>
<atomRef index="150"/>
<atomRef index="142"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="840" type="POLY_VERTEX" name="Atom #71"/>
<nrOfStructures value="11"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="70"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="142"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="841" type="POLY_VERTEX" name="Atom #72"/>
<nrOfStructures value="7"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="73"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="842" type="POLY_VERTEX" name="Atom #73"/>
<nrOfStructures value="11"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="72"/>
<atomRef index="74"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="145"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="843" type="POLY_VERTEX" name="Atom #74"/>
<nrOfStructures value="11"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="73"/>
<atomRef index="75"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="145"/>
<atomRef index="146"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="844" type="POLY_VERTEX" name="Atom #75"/>
<nrOfStructures value="11"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="74"/>
<atomRef index="76"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="146"/>
<atomRef index="147"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="845" type="POLY_VERTEX" name="Atom #76"/>
<nrOfStructures value="10"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="75"/>
<atomRef index="77"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="147"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="846" type="POLY_VERTEX" name="Atom #77"/>
<nrOfStructures value="11"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="76"/>
<atomRef index="78"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="148"/>
<atomRef index="149"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="847" type="POLY_VERTEX" name="Atom #78"/>
<nrOfStructures value="10"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="77"/>
<atomRef index="79"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="149"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="848" type="POLY_VERTEX" name="Atom #79"/>
<nrOfStructures value="11"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="78"/>
<atomRef index="80"/>
<atomRef index="152"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="150"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="849" type="POLY_VERTEX" name="Atom #80"/>
<nrOfStructures value="7"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="79"/>
<atomRef index="152"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="850" type="POLY_VERTEX" name="Atom #81"/>
<nrOfStructures value="9"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="82"/>
<atomRef index="90"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="171"/>
</cell>
<cell>
<cellProperties index="851" type="POLY_VERTEX" name="Atom #82"/>
<nrOfStructures value="15"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="81"/>
<atomRef index="83"/>
<atomRef index="90"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="171"/>
<atomRef index="172"/>
<atomRef index="91"/>
</cell>
<cell>
<cellProperties index="852" type="POLY_VERTEX" name="Atom #83"/>
<nrOfStructures value="15"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="82"/>
<atomRef index="84"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="173"/>
<atomRef index="92"/>
</cell>
<cell>
<cellProperties index="853" type="POLY_VERTEX" name="Atom #84"/>
<nrOfStructures value="15"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="83"/>
<atomRef index="85"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="174"/>
<atomRef index="93"/>
</cell>
<cell>
<cellProperties index="854" type="POLY_VERTEX" name="Atom #85"/>
<nrOfStructures value="15"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="84"/>
<atomRef index="86"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="175"/>
<atomRef index="94"/>
</cell>
<cell>
<cellProperties index="855" type="POLY_VERTEX" name="Atom #86"/>
<nrOfStructures value="15"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="85"/>
<atomRef index="87"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="176"/>
<atomRef index="95"/>
</cell>
<cell>
<cellProperties index="856" type="POLY_VERTEX" name="Atom #87"/>
<nrOfStructures value="15"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="86"/>
<atomRef index="88"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="177"/>
<atomRef index="96"/>
</cell>
<cell>
<cellProperties index="857" type="POLY_VERTEX" name="Atom #88"/>
<nrOfStructures value="17"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="87"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="178"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="858" type="POLY_VERTEX" name="Atom #89"/>
<nrOfStructures value="11"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="88"/>
<atomRef index="98"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="178"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="859" type="POLY_VERTEX" name="Atom #90"/>
<nrOfStructures value="13"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="99"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="171"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="860" type="POLY_VERTEX" name="Atom #98"/>
<nrOfStructures value="17"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="107"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="187"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="861" type="POLY_VERTEX" name="Atom #99"/>
<nrOfStructures value="11"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="90"/>
<atomRef index="108"/>
<atomRef index="171"/>
<atomRef index="180"/>
<atomRef index="189"/>
</cell>
<cell>
<cellProperties index="862" type="POLY_VERTEX" name="Atom #107"/>
<nrOfStructures value="17"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="98"/>
<atomRef index="116"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="196"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="863" type="POLY_VERTEX" name="Atom #108"/>
<nrOfStructures value="12"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="99"/>
<atomRef index="117"/>
<atomRef index="180"/>
<atomRef index="189"/>
<atomRef index="198"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="864" type="POLY_VERTEX" name="Atom #116"/>
<nrOfStructures value="17"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="107"/>
<atomRef index="125"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="205"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="865" type="POLY_VERTEX" name="Atom #117"/>
<nrOfStructures value="13"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="108"/>
<atomRef index="126"/>
<atomRef index="189"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="199"/>
<atomRef index="208"/>
</cell>
<cell>
<cellProperties index="866" type="POLY_VERTEX" name="Atom #125"/>
<nrOfStructures value="17"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="116"/>
<atomRef index="134"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="214"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="867" type="POLY_VERTEX" name="Atom #126"/>
<nrOfStructures value="14"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="117"/>
<atomRef index="135"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="199"/>
<atomRef index="208"/>
<atomRef index="217"/>
</cell>
<cell>
<cellProperties index="868" type="POLY_VERTEX" name="Atom #134"/>
<nrOfStructures value="17"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="125"/>
<atomRef index="143"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="223"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="869" type="POLY_VERTEX" name="Atom #135"/>
<nrOfStructures value="15"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="126"/>
<atomRef index="144"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="208"/>
<atomRef index="217"/>
<atomRef index="226"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="870" type="POLY_VERTEX" name="Atom #143"/>
<nrOfStructures value="17"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="134"/>
<atomRef index="152"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="871" type="POLY_VERTEX" name="Atom #144"/>
<nrOfStructures value="16"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="135"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="217"/>
<atomRef index="226"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="872" type="POLY_VERTEX" name="Atom #152"/>
<nrOfStructures value="17"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="143"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="873" type="POLY_VERTEX" name="Atom #153"/>
<nrOfStructures value="11"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="144"/>
<atomRef index="154"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="226"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="874" type="POLY_VERTEX" name="Atom #154"/>
<nrOfStructures value="17"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="155"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="227"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="875" type="POLY_VERTEX" name="Atom #155"/>
<nrOfStructures value="16"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="154"/>
<atomRef index="156"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="876" type="POLY_VERTEX" name="Atom #156"/>
<nrOfStructures value="17"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="155"/>
<atomRef index="157"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="229"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="877" type="POLY_VERTEX" name="Atom #157"/>
<nrOfStructures value="16"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="156"/>
<atomRef index="158"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="878" type="POLY_VERTEX" name="Atom #158"/>
<nrOfStructures value="17"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="157"/>
<atomRef index="159"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="879" type="POLY_VERTEX" name="Atom #159"/>
<nrOfStructures value="15"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="158"/>
<atomRef index="160"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="880" type="POLY_VERTEX" name="Atom #160"/>
<nrOfStructures value="17"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="152"/>
<atomRef index="159"/>
<atomRef index="161"/>
<atomRef index="233"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="881" type="POLY_VERTEX" name="Atom #161"/>
<nrOfStructures value="11"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="152"/>
<atomRef index="160"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="882" type="POLY_VERTEX" name="Atom #162"/>
<nrOfStructures value="8"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="90"/>
<atomRef index="163"/>
<atomRef index="171"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="883" type="POLY_VERTEX" name="Atom #163"/>
<nrOfStructures value="14"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="90"/>
<atomRef index="162"/>
<atomRef index="164"/>
<atomRef index="171"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="252"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="91"/>
</cell>
<cell>
<cellProperties index="884" type="POLY_VERTEX" name="Atom #164"/>
<nrOfStructures value="14"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="163"/>
<atomRef index="165"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="92"/>
</cell>
<cell>
<cellProperties index="885" type="POLY_VERTEX" name="Atom #165"/>
<nrOfStructures value="14"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="164"/>
<atomRef index="166"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="93"/>
</cell>
<cell>
<cellProperties index="886" type="POLY_VERTEX" name="Atom #166"/>
<nrOfStructures value="14"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="165"/>
<atomRef index="167"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="94"/>
</cell>
<cell>
<cellProperties index="887" type="POLY_VERTEX" name="Atom #167"/>
<nrOfStructures value="14"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="166"/>
<atomRef index="168"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="95"/>
</cell>
<cell>
<cellProperties index="888" type="POLY_VERTEX" name="Atom #168"/>
<nrOfStructures value="14"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="167"/>
<atomRef index="169"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="96"/>
</cell>
<cell>
<cellProperties index="889" type="POLY_VERTEX" name="Atom #169"/>
<nrOfStructures value="17"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="168"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="890" type="POLY_VERTEX" name="Atom #170"/>
<nrOfStructures value="11"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="169"/>
<atomRef index="179"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="891" type="POLY_VERTEX" name="Atom #171"/>
<nrOfStructures value="12"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="90"/>
<atomRef index="99"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="180"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="252"/>
<atomRef index="261"/>
<atomRef index="262"/>
</cell>
<cell>
<cellProperties index="892" type="POLY_VERTEX" name="Atom #179"/>
<nrOfStructures value="17"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="188"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="893" type="POLY_VERTEX" name="Atom #180"/>
<nrOfStructures value="9"/>
<atomRef index="90"/>
<atomRef index="99"/>
<atomRef index="108"/>
<atomRef index="171"/>
<atomRef index="189"/>
<atomRef index="252"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="262"/>
</cell>
<cell>
<cellProperties index="894" type="POLY_VERTEX" name="Atom #188"/>
<nrOfStructures value="17"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="179"/>
<atomRef index="197"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="895" type="POLY_VERTEX" name="Atom #189"/>
<nrOfStructures value="11"/>
<atomRef index="99"/>
<atomRef index="108"/>
<atomRef index="117"/>
<atomRef index="180"/>
<atomRef index="198"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="262"/>
<atomRef index="280"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="896" type="POLY_VERTEX" name="Atom #197"/>
<nrOfStructures value="17"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="188"/>
<atomRef index="206"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="897" type="POLY_VERTEX" name="Atom #198"/>
<nrOfStructures value="12"/>
<atomRef index="108"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="189"/>
<atomRef index="207"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="289"/>
<atomRef index="208"/>
</cell>
<cell>
<cellProperties index="898" type="POLY_VERTEX" name="Atom #206"/>
<nrOfStructures value="17"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="197"/>
<atomRef index="215"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="899" type="POLY_VERTEX" name="Atom #207"/>
<nrOfStructures value="14"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="198"/>
<atomRef index="216"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="298"/>
<atomRef index="217"/>
</cell>
<cell>
<cellProperties index="900" type="POLY_VERTEX" name="Atom #215"/>
<nrOfStructures value="17"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="206"/>
<atomRef index="224"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="901" type="POLY_VERTEX" name="Atom #216"/>
<nrOfStructures value="15"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="207"/>
<atomRef index="225"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="902" type="POLY_VERTEX" name="Atom #224"/>
<nrOfStructures value="17"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="215"/>
<atomRef index="233"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="313"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="903" type="POLY_VERTEX" name="Atom #225"/>
<nrOfStructures value="16"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="216"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="904" type="POLY_VERTEX" name="Atom #233"/>
<nrOfStructures value="17"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="224"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="313"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="905" type="POLY_VERTEX" name="Atom #234"/>
<nrOfStructures value="11"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="225"/>
<atomRef index="235"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="906" type="POLY_VERTEX" name="Atom #235"/>
<nrOfStructures value="17"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="236"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="907" type="POLY_VERTEX" name="Atom #236"/>
<nrOfStructures value="16"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="235"/>
<atomRef index="237"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="309"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="908" type="POLY_VERTEX" name="Atom #237"/>
<nrOfStructures value="17"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="236"/>
<atomRef index="238"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="909" type="POLY_VERTEX" name="Atom #238"/>
<nrOfStructures value="16"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="237"/>
<atomRef index="239"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="311"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="910" type="POLY_VERTEX" name="Atom #239"/>
<nrOfStructures value="16"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="238"/>
<atomRef index="240"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="911" type="POLY_VERTEX" name="Atom #240"/>
<nrOfStructures value="14"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="239"/>
<atomRef index="241"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="912" type="POLY_VERTEX" name="Atom #241"/>
<nrOfStructures value="17"/>
<atomRef index="152"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="233"/>
<atomRef index="240"/>
<atomRef index="242"/>
<atomRef index="314"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="313"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="913" type="POLY_VERTEX" name="Atom #242"/>
<nrOfStructures value="11"/>
<atomRef index="152"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="314"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="313"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="914" type="POLY_VERTEX" name="Atom #243"/>
<nrOfStructures value="8"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="171"/>
<atomRef index="244"/>
<atomRef index="252"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="333"/>
</cell>
<cell>
<cellProperties index="915" type="POLY_VERTEX" name="Atom #244"/>
<nrOfStructures value="14"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="171"/>
<atomRef index="243"/>
<atomRef index="245"/>
<atomRef index="252"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="333"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="172"/>
</cell>
<cell>
<cellProperties index="916" type="POLY_VERTEX" name="Atom #245"/>
<nrOfStructures value="14"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="244"/>
<atomRef index="246"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="173"/>
</cell>
<cell>
<cellProperties index="917" type="POLY_VERTEX" name="Atom #246"/>
<nrOfStructures value="14"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="245"/>
<atomRef index="247"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="174"/>
</cell>
<cell>
<cellProperties index="918" type="POLY_VERTEX" name="Atom #247"/>
<nrOfStructures value="14"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="246"/>
<atomRef index="248"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="175"/>
</cell>
<cell>
<cellProperties index="919" type="POLY_VERTEX" name="Atom #248"/>
<nrOfStructures value="14"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="247"/>
<atomRef index="249"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="176"/>
</cell>
<cell>
<cellProperties index="920" type="POLY_VERTEX" name="Atom #249"/>
<nrOfStructures value="14"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="248"/>
<atomRef index="250"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="177"/>
</cell>
<cell>
<cellProperties index="921" type="POLY_VERTEX" name="Atom #250"/>
<nrOfStructures value="17"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="249"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="178"/>
</cell>
<cell>
<cellProperties index="922" type="POLY_VERTEX" name="Atom #251"/>
<nrOfStructures value="11"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="250"/>
<atomRef index="260"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="178"/>
</cell>
<cell>
<cellProperties index="923" type="POLY_VERTEX" name="Atom #252"/>
<nrOfStructures value="13"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="171"/>
<atomRef index="180"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="261"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="333"/>
<atomRef index="342"/>
<atomRef index="343"/>
<atomRef index="262"/>
</cell>
<cell>
<cellProperties index="924" type="POLY_VERTEX" name="Atom #260"/>
<nrOfStructures value="17"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="269"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
</cell>
<cell>
<cellProperties index="925" type="POLY_VERTEX" name="Atom #261"/>
<nrOfStructures value="11"/>
<atomRef index="171"/>
<atomRef index="180"/>
<atomRef index="189"/>
<atomRef index="252"/>
<atomRef index="270"/>
<atomRef index="333"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="352"/>
</cell>
<cell>
<cellProperties index="926" type="POLY_VERTEX" name="Atom #269"/>
<nrOfStructures value="17"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="260"/>
<atomRef index="278"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
</cell>
<cell>
<cellProperties index="927" type="POLY_VERTEX" name="Atom #270"/>
<nrOfStructures value="14"/>
<atomRef index="180"/>
<atomRef index="189"/>
<atomRef index="198"/>
<atomRef index="261"/>
<atomRef index="279"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="352"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="928" type="POLY_VERTEX" name="Atom #278"/>
<nrOfStructures value="17"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="269"/>
<atomRef index="287"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
</cell>
<cell>
<cellProperties index="929" type="POLY_VERTEX" name="Atom #279"/>
<nrOfStructures value="15"/>
<atomRef index="189"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="270"/>
<atomRef index="288"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="352"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="208"/>
</cell>
<cell>
<cellProperties index="930" type="POLY_VERTEX" name="Atom #287"/>
<nrOfStructures value="17"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="278"/>
<atomRef index="296"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="931" type="POLY_VERTEX" name="Atom #288"/>
<nrOfStructures value="17"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="279"/>
<atomRef index="297"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="217"/>
</cell>
<cell>
<cellProperties index="932" type="POLY_VERTEX" name="Atom #296"/>
<nrOfStructures value="17"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="287"/>
<atomRef index="305"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
</cell>
<cell>
<cellProperties index="933" type="POLY_VERTEX" name="Atom #297"/>
<nrOfStructures value="17"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="288"/>
<atomRef index="306"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
</cell>
<cell>
<cellProperties index="934" type="POLY_VERTEX" name="Atom #305"/>
<nrOfStructures value="17"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="296"/>
<atomRef index="314"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="394"/>
<atomRef index="313"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="935" type="POLY_VERTEX" name="Atom #306"/>
<nrOfStructures value="17"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="297"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
</cell>
<cell>
<cellProperties index="936" type="POLY_VERTEX" name="Atom #314"/>
<nrOfStructures value="17"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="305"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="394"/>
<atomRef index="313"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="937" type="POLY_VERTEX" name="Atom #315"/>
<nrOfStructures value="11"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="306"/>
<atomRef index="316"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
</cell>
<cell>
<cellProperties index="938" type="POLY_VERTEX" name="Atom #316"/>
<nrOfStructures value="17"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="317"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
</cell>
<cell>
<cellProperties index="939" type="POLY_VERTEX" name="Atom #317"/>
<nrOfStructures value="16"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="316"/>
<atomRef index="318"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="390"/>
<atomRef index="309"/>
</cell>
<cell>
<cellProperties index="940" type="POLY_VERTEX" name="Atom #318"/>
<nrOfStructures value="17"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="317"/>
<atomRef index="319"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
</cell>
<cell>
<cellProperties index="941" type="POLY_VERTEX" name="Atom #319"/>
<nrOfStructures value="17"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="318"/>
<atomRef index="320"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="942" type="POLY_VERTEX" name="Atom #320"/>
<nrOfStructures value="16"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="319"/>
<atomRef index="321"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="393"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="943" type="POLY_VERTEX" name="Atom #321"/>
<nrOfStructures value="15"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="320"/>
<atomRef index="322"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="944" type="POLY_VERTEX" name="Atom #322"/>
<nrOfStructures value="17"/>
<atomRef index="233"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="314"/>
<atomRef index="321"/>
<atomRef index="323"/>
<atomRef index="395"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="394"/>
<atomRef index="313"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="945" type="POLY_VERTEX" name="Atom #323"/>
<nrOfStructures value="11"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="314"/>
<atomRef index="322"/>
<atomRef index="395"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="394"/>
<atomRef index="313"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="946" type="POLY_VERTEX" name="Atom #324"/>
<nrOfStructures value="8"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="252"/>
<atomRef index="325"/>
<atomRef index="333"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="414"/>
</cell>
<cell>
<cellProperties index="947" type="POLY_VERTEX" name="Atom #325"/>
<nrOfStructures value="14"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="252"/>
<atomRef index="324"/>
<atomRef index="326"/>
<atomRef index="333"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="414"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="253"/>
</cell>
<cell>
<cellProperties index="948" type="POLY_VERTEX" name="Atom #326"/>
<nrOfStructures value="14"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="325"/>
<atomRef index="327"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="254"/>
</cell>
<cell>
<cellProperties index="949" type="POLY_VERTEX" name="Atom #327"/>
<nrOfStructures value="14"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="326"/>
<atomRef index="328"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="255"/>
</cell>
<cell>
<cellProperties index="950" type="POLY_VERTEX" name="Atom #328"/>
<nrOfStructures value="14"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="327"/>
<atomRef index="329"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="256"/>
</cell>
<cell>
<cellProperties index="951" type="POLY_VERTEX" name="Atom #329"/>
<nrOfStructures value="14"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="328"/>
<atomRef index="330"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="257"/>
</cell>
<cell>
<cellProperties index="952" type="POLY_VERTEX" name="Atom #330"/>
<nrOfStructures value="14"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="329"/>
<atomRef index="331"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="258"/>
</cell>
<cell>
<cellProperties index="953" type="POLY_VERTEX" name="Atom #331"/>
<nrOfStructures value="17"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="330"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="259"/>
</cell>
<cell>
<cellProperties index="954" type="POLY_VERTEX" name="Atom #332"/>
<nrOfStructures value="11"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="331"/>
<atomRef index="341"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="259"/>
</cell>
<cell>
<cellProperties index="955" type="POLY_VERTEX" name="Atom #333"/>
<nrOfStructures value="13"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="252"/>
<atomRef index="261"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="342"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="343"/>
<atomRef index="262"/>
</cell>
<cell>
<cellProperties index="956" type="POLY_VERTEX" name="Atom #341"/>
<nrOfStructures value="17"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="350"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="957" type="POLY_VERTEX" name="Atom #342"/>
<nrOfStructures value="12"/>
<atomRef index="252"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="333"/>
<atomRef index="351"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="433"/>
<atomRef index="352"/>
</cell>
<cell>
<cellProperties index="958" type="POLY_VERTEX" name="Atom #350"/>
<nrOfStructures value="17"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="341"/>
<atomRef index="359"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="277"/>
</cell>
<cell>
<cellProperties index="959" type="POLY_VERTEX" name="Atom #351"/>
<nrOfStructures value="15"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="342"/>
<atomRef index="360"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
</cell>
<cell>
<cellProperties index="960" type="POLY_VERTEX" name="Atom #359"/>
<nrOfStructures value="17"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="350"/>
<atomRef index="368"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="286"/>
</cell>
<cell>
<cellProperties index="961" type="POLY_VERTEX" name="Atom #360"/>
<nrOfStructures value="16"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="351"/>
<atomRef index="369"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="289"/>
</cell>
<cell>
<cellProperties index="962" type="POLY_VERTEX" name="Atom #368"/>
<nrOfStructures value="16"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="359"/>
<atomRef index="377"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="376"/>
<atomRef index="295"/>
</cell>
<cell>
<cellProperties index="963" type="POLY_VERTEX" name="Atom #369"/>
<nrOfStructures value="17"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="360"/>
<atomRef index="378"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="298"/>
</cell>
<cell>
<cellProperties index="964" type="POLY_VERTEX" name="Atom #377"/>
<nrOfStructures value="17"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="368"/>
<atomRef index="386"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="304"/>
</cell>
<cell>
<cellProperties index="965" type="POLY_VERTEX" name="Atom #378"/>
<nrOfStructures value="17"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="369"/>
<atomRef index="387"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="307"/>
</cell>
<cell>
<cellProperties index="966" type="POLY_VERTEX" name="Atom #386"/>
<nrOfStructures value="16"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="377"/>
<atomRef index="395"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="394"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="967" type="POLY_VERTEX" name="Atom #387"/>
<nrOfStructures value="17"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="378"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="307"/>
</cell>
<cell>
<cellProperties index="968" type="POLY_VERTEX" name="Atom #395"/>
<nrOfStructures value="17"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="386"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="475"/>
<atomRef index="394"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="969" type="POLY_VERTEX" name="Atom #396"/>
<nrOfStructures value="11"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="387"/>
<atomRef index="397"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="307"/>
</cell>
<cell>
<cellProperties index="970" type="POLY_VERTEX" name="Atom #397"/>
<nrOfStructures value="17"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="398"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="308"/>
</cell>
<cell>
<cellProperties index="971" type="POLY_VERTEX" name="Atom #398"/>
<nrOfStructures value="17"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="397"/>
<atomRef index="399"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="309"/>
</cell>
<cell>
<cellProperties index="972" type="POLY_VERTEX" name="Atom #399"/>
<nrOfStructures value="17"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="398"/>
<atomRef index="400"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
</cell>
<cell>
<cellProperties index="973" type="POLY_VERTEX" name="Atom #400"/>
<nrOfStructures value="17"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="399"/>
<atomRef index="401"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
</cell>
<cell>
<cellProperties index="974" type="POLY_VERTEX" name="Atom #401"/>
<nrOfStructures value="15"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="400"/>
<atomRef index="402"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="393"/>
</cell>
<cell>
<cellProperties index="975" type="POLY_VERTEX" name="Atom #402"/>
<nrOfStructures value="15"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="401"/>
<atomRef index="403"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="976" type="POLY_VERTEX" name="Atom #403"/>
<nrOfStructures value="17"/>
<atomRef index="314"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="395"/>
<atomRef index="402"/>
<atomRef index="404"/>
<atomRef index="476"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="475"/>
<atomRef index="394"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="977" type="POLY_VERTEX" name="Atom #404"/>
<nrOfStructures value="11"/>
<atomRef index="314"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="395"/>
<atomRef index="403"/>
<atomRef index="476"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="475"/>
<atomRef index="394"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="978" type="POLY_VERTEX" name="Atom #405"/>
<nrOfStructures value="9"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="333"/>
<atomRef index="406"/>
<atomRef index="414"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="495"/>
<atomRef index="496"/>
</cell>
<cell>
<cellProperties index="979" type="POLY_VERTEX" name="Atom #406"/>
<nrOfStructures value="15"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="333"/>
<atomRef index="405"/>
<atomRef index="407"/>
<atomRef index="414"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="495"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="497"/>
</cell>
<cell>
<cellProperties index="980" type="POLY_VERTEX" name="Atom #407"/>
<nrOfStructures value="14"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="406"/>
<atomRef index="408"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="335"/>
</cell>
<cell>
<cellProperties index="981" type="POLY_VERTEX" name="Atom #408"/>
<nrOfStructures value="15"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="407"/>
<atomRef index="409"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="499"/>
</cell>
<cell>
<cellProperties index="982" type="POLY_VERTEX" name="Atom #409"/>
<nrOfStructures value="14"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="408"/>
<atomRef index="410"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="337"/>
</cell>
<cell>
<cellProperties index="983" type="POLY_VERTEX" name="Atom #410"/>
<nrOfStructures value="14"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="409"/>
<atomRef index="411"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="338"/>
</cell>
<cell>
<cellProperties index="984" type="POLY_VERTEX" name="Atom #411"/>
<nrOfStructures value="14"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="410"/>
<atomRef index="412"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="339"/>
</cell>
<cell>
<cellProperties index="985" type="POLY_VERTEX" name="Atom #412"/>
<nrOfStructures value="17"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="411"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="340"/>
</cell>
<cell>
<cellProperties index="986" type="POLY_VERTEX" name="Atom #413"/>
<nrOfStructures value="11"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="412"/>
<atomRef index="422"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="340"/>
</cell>
<cell>
<cellProperties index="987" type="POLY_VERTEX" name="Atom #414"/>
<nrOfStructures value="14"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="333"/>
<atomRef index="342"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="423"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="496"/>
<atomRef index="505"/>
<atomRef index="343"/>
</cell>
<cell>
<cellProperties index="988" type="POLY_VERTEX" name="Atom #422"/>
<nrOfStructures value="17"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="431"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
</cell>
<cell>
<cellProperties index="989" type="POLY_VERTEX" name="Atom #423"/>
<nrOfStructures value="14"/>
<atomRef index="333"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="414"/>
<atomRef index="432"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="496"/>
<atomRef index="505"/>
<atomRef index="343"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="352"/>
</cell>
<cell>
<cellProperties index="990" type="POLY_VERTEX" name="Atom #431"/>
<nrOfStructures value="17"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="422"/>
<atomRef index="440"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
</cell>
<cell>
<cellProperties index="991" type="POLY_VERTEX" name="Atom #432"/>
<nrOfStructures value="16"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="423"/>
<atomRef index="441"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="505"/>
<atomRef index="343"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
</cell>
<cell>
<cellProperties index="992" type="POLY_VERTEX" name="Atom #440"/>
<nrOfStructures value="17"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="431"/>
<atomRef index="449"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
</cell>
<cell>
<cellProperties index="993" type="POLY_VERTEX" name="Atom #441"/>
<nrOfStructures value="17"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="432"/>
<atomRef index="450"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="370"/>
</cell>
<cell>
<cellProperties index="994" type="POLY_VERTEX" name="Atom #449"/>
<nrOfStructures value="16"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="440"/>
<atomRef index="458"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="538"/>
<atomRef index="376"/>
</cell>
<cell>
<cellProperties index="995" type="POLY_VERTEX" name="Atom #450"/>
<nrOfStructures value="17"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="441"/>
<atomRef index="459"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="379"/>
</cell>
<cell>
<cellProperties index="996" type="POLY_VERTEX" name="Atom #458"/>
<nrOfStructures value="17"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="449"/>
<atomRef index="467"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="385"/>
</cell>
<cell>
<cellProperties index="997" type="POLY_VERTEX" name="Atom #459"/>
<nrOfStructures value="17"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="450"/>
<atomRef index="468"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="388"/>
</cell>
<cell>
<cellProperties index="998" type="POLY_VERTEX" name="Atom #467"/>
<nrOfStructures value="16"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="458"/>
<atomRef index="476"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="556"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="999" type="POLY_VERTEX" name="Atom #468"/>
<nrOfStructures value="17"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="459"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="388"/>
</cell>
<cell>
<cellProperties index="1000" type="POLY_VERTEX" name="Atom #476"/>
<nrOfStructures value="17"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="467"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="556"/>
<atomRef index="475"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1001" type="POLY_VERTEX" name="Atom #477"/>
<nrOfStructures value="11"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="468"/>
<atomRef index="478"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="388"/>
</cell>
<cell>
<cellProperties index="1002" type="POLY_VERTEX" name="Atom #478"/>
<nrOfStructures value="17"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="479"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="389"/>
</cell>
<cell>
<cellProperties index="1003" type="POLY_VERTEX" name="Atom #479"/>
<nrOfStructures value="17"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="478"/>
<atomRef index="480"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="390"/>
</cell>
<cell>
<cellProperties index="1004" type="POLY_VERTEX" name="Atom #480"/>
<nrOfStructures value="17"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="479"/>
<atomRef index="481"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
</cell>
<cell>
<cellProperties index="1005" type="POLY_VERTEX" name="Atom #481"/>
<nrOfStructures value="17"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="480"/>
<atomRef index="482"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="392"/>
</cell>
<cell>
<cellProperties index="1006" type="POLY_VERTEX" name="Atom #482"/>
<nrOfStructures value="16"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="481"/>
<atomRef index="483"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="555"/>
<atomRef index="393"/>
</cell>
<cell>
<cellProperties index="1007" type="POLY_VERTEX" name="Atom #483"/>
<nrOfStructures value="16"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="482"/>
<atomRef index="484"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="556"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1008" type="POLY_VERTEX" name="Atom #484"/>
<nrOfStructures value="17"/>
<atomRef index="395"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="476"/>
<atomRef index="483"/>
<atomRef index="485"/>
<atomRef index="557"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="556"/>
<atomRef index="475"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1009" type="POLY_VERTEX" name="Atom #485"/>
<nrOfStructures value="11"/>
<atomRef index="395"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="476"/>
<atomRef index="484"/>
<atomRef index="557"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="556"/>
<atomRef index="475"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1010" type="POLY_VERTEX" name="Atom #486"/>
<nrOfStructures value="10"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="414"/>
<atomRef index="487"/>
<atomRef index="495"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="576"/>
<atomRef index="577"/>
<atomRef index="496"/>
</cell>
<cell>
<cellProperties index="1011" type="POLY_VERTEX" name="Atom #487"/>
<nrOfStructures value="16"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="414"/>
<atomRef index="486"/>
<atomRef index="488"/>
<atomRef index="495"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="576"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="578"/>
<atomRef index="497"/>
</cell>
<cell>
<cellProperties index="1012" type="POLY_VERTEX" name="Atom #488"/>
<nrOfStructures value="14"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="487"/>
<atomRef index="489"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="416"/>
</cell>
<cell>
<cellProperties index="1013" type="POLY_VERTEX" name="Atom #489"/>
<nrOfStructures value="15"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="488"/>
<atomRef index="490"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="499"/>
</cell>
<cell>
<cellProperties index="1014" type="POLY_VERTEX" name="Atom #490"/>
<nrOfStructures value="14"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="489"/>
<atomRef index="491"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="580"/>
<atomRef index="499"/>
<atomRef index="418"/>
</cell>
<cell>
<cellProperties index="1015" type="POLY_VERTEX" name="Atom #491"/>
<nrOfStructures value="14"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="490"/>
<atomRef index="492"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="580"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="581"/>
<atomRef index="500"/>
<atomRef index="419"/>
</cell>
<cell>
<cellProperties index="1016" type="POLY_VERTEX" name="Atom #492"/>
<nrOfStructures value="14"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="491"/>
<atomRef index="493"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="581"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="582"/>
<atomRef index="501"/>
<atomRef index="420"/>
</cell>
<cell>
<cellProperties index="1017" type="POLY_VERTEX" name="Atom #493"/>
<nrOfStructures value="17"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="492"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="582"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="421"/>
</cell>
<cell>
<cellProperties index="1018" type="POLY_VERTEX" name="Atom #494"/>
<nrOfStructures value="11"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="493"/>
<atomRef index="503"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="421"/>
</cell>
<cell>
<cellProperties index="1019" type="POLY_VERTEX" name="Atom #495"/>
<nrOfStructures value="15"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="504"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="586"/>
<atomRef index="505"/>
</cell>
<cell>
<cellProperties index="1020" type="POLY_VERTEX" name="Atom #503"/>
<nrOfStructures value="17"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="512"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="592"/>
<atomRef index="511"/>
<atomRef index="430"/>
</cell>
<cell>
<cellProperties index="1021" type="POLY_VERTEX" name="Atom #504"/>
<nrOfStructures value="14"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="495"/>
<atomRef index="513"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="496"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="595"/>
<atomRef index="514"/>
<atomRef index="433"/>
</cell>
<cell>
<cellProperties index="1022" type="POLY_VERTEX" name="Atom #512"/>
<nrOfStructures value="16"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="503"/>
<atomRef index="521"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="592"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="439"/>
</cell>
<cell>
<cellProperties index="1023" type="POLY_VERTEX" name="Atom #513"/>
<nrOfStructures value="15"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="504"/>
<atomRef index="522"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="505"/>
<atomRef index="595"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="442"/>
</cell>
<cell>
<cellProperties index="1024" type="POLY_VERTEX" name="Atom #521"/>
<nrOfStructures value="16"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="512"/>
<atomRef index="530"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="448"/>
</cell>
<cell>
<cellProperties index="1025" type="POLY_VERTEX" name="Atom #522"/>
<nrOfStructures value="16"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="513"/>
<atomRef index="531"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
</cell>
<cell>
<cellProperties index="1026" type="POLY_VERTEX" name="Atom #530"/>
<nrOfStructures value="16"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="521"/>
<atomRef index="539"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="619"/>
<atomRef index="538"/>
</cell>
<cell>
<cellProperties index="1027" type="POLY_VERTEX" name="Atom #531"/>
<nrOfStructures value="16"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="522"/>
<atomRef index="540"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="460"/>
</cell>
<cell>
<cellProperties index="1028" type="POLY_VERTEX" name="Atom #539"/>
<nrOfStructures value="17"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="530"/>
<atomRef index="548"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="466"/>
</cell>
<cell>
<cellProperties index="1029" type="POLY_VERTEX" name="Atom #540"/>
<nrOfStructures value="16"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="531"/>
<atomRef index="549"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="550"/>
<atomRef index="469"/>
</cell>
<cell>
<cellProperties index="1030" type="POLY_VERTEX" name="Atom #548"/>
<nrOfStructures value="15"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="539"/>
<atomRef index="557"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1031" type="POLY_VERTEX" name="Atom #549"/>
<nrOfStructures value="17"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="540"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="469"/>
</cell>
<cell>
<cellProperties index="1032" type="POLY_VERTEX" name="Atom #557"/>
<nrOfStructures value="17"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="548"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="637"/>
<atomRef index="556"/>
<atomRef index="475"/>
</cell>
<cell>
<cellProperties index="1033" type="POLY_VERTEX" name="Atom #558"/>
<nrOfStructures value="11"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="549"/>
<atomRef index="559"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="469"/>
</cell>
<cell>
<cellProperties index="1034" type="POLY_VERTEX" name="Atom #559"/>
<nrOfStructures value="17"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="560"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="470"/>
</cell>
<cell>
<cellProperties index="1035" type="POLY_VERTEX" name="Atom #560"/>
<nrOfStructures value="17"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="559"/>
<atomRef index="561"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="633"/>
<atomRef index="552"/>
<atomRef index="471"/>
</cell>
<cell>
<cellProperties index="1036" type="POLY_VERTEX" name="Atom #561"/>
<nrOfStructures value="17"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="560"/>
<atomRef index="562"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="633"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="472"/>
</cell>
<cell>
<cellProperties index="1037" type="POLY_VERTEX" name="Atom #562"/>
<nrOfStructures value="16"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="561"/>
<atomRef index="563"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
</cell>
<cell>
<cellProperties index="1038" type="POLY_VERTEX" name="Atom #563"/>
<nrOfStructures value="15"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="562"/>
<atomRef index="564"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="555"/>
</cell>
<cell>
<cellProperties index="1039" type="POLY_VERTEX" name="Atom #564"/>
<nrOfStructures value="15"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="563"/>
<atomRef index="565"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1040" type="POLY_VERTEX" name="Atom #565"/>
<nrOfStructures value="17"/>
<atomRef index="476"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="557"/>
<atomRef index="564"/>
<atomRef index="566"/>
<atomRef index="638"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="637"/>
<atomRef index="556"/>
<atomRef index="475"/>
</cell>
<cell>
<cellProperties index="1041" type="POLY_VERTEX" name="Atom #566"/>
<nrOfStructures value="11"/>
<atomRef index="476"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="557"/>
<atomRef index="565"/>
<atomRef index="638"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="637"/>
<atomRef index="556"/>
<atomRef index="475"/>
</cell>
<cell>
<cellProperties index="1042" type="POLY_VERTEX" name="Atom #567"/>
<nrOfStructures value="11"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="495"/>
<atomRef index="568"/>
<atomRef index="576"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="577"/>
<atomRef index="496"/>
</cell>
<cell>
<cellProperties index="1043" type="POLY_VERTEX" name="Atom #568"/>
<nrOfStructures value="17"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="495"/>
<atomRef index="567"/>
<atomRef index="569"/>
<atomRef index="576"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="578"/>
<atomRef index="497"/>
</cell>
<cell>
<cellProperties index="1044" type="POLY_VERTEX" name="Atom #569"/>
<nrOfStructures value="15"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="568"/>
<atomRef index="570"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="651"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="578"/>
<atomRef index="497"/>
</cell>
<cell>
<cellProperties index="1045" type="POLY_VERTEX" name="Atom #570"/>
<nrOfStructures value="16"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="569"/>
<atomRef index="571"/>
<atomRef index="650"/>
<atomRef index="651"/>
<atomRef index="652"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="499"/>
</cell>
<cell>
<cellProperties index="1046" type="POLY_VERTEX" name="Atom #571"/>
<nrOfStructures value="15"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="570"/>
<atomRef index="572"/>
<atomRef index="651"/>
<atomRef index="652"/>
<atomRef index="653"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="580"/>
<atomRef index="499"/>
</cell>
<cell>
<cellProperties index="1047" type="POLY_VERTEX" name="Atom #572"/>
<nrOfStructures value="15"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="571"/>
<atomRef index="573"/>
<atomRef index="652"/>
<atomRef index="653"/>
<atomRef index="654"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="580"/>
<atomRef index="499"/>
<atomRef index="581"/>
<atomRef index="500"/>
</cell>
<cell>
<cellProperties index="1048" type="POLY_VERTEX" name="Atom #573"/>
<nrOfStructures value="15"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="572"/>
<atomRef index="574"/>
<atomRef index="653"/>
<atomRef index="654"/>
<atomRef index="655"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="581"/>
<atomRef index="500"/>
<atomRef index="582"/>
<atomRef index="501"/>
</cell>
<cell>
<cellProperties index="1049" type="POLY_VERTEX" name="Atom #574"/>
<nrOfStructures value="17"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="573"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="654"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="582"/>
<atomRef index="501"/>
<atomRef index="583"/>
<atomRef index="502"/>
</cell>
<cell>
<cellProperties index="1050" type="POLY_VERTEX" name="Atom #575"/>
<nrOfStructures value="11"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="574"/>
<atomRef index="584"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="583"/>
<atomRef index="502"/>
</cell>
<cell>
<cellProperties index="1051" type="POLY_VERTEX" name="Atom #576"/>
<nrOfStructures value="17"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="585"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="586"/>
<atomRef index="505"/>
</cell>
<cell>
<cellProperties index="1052" type="POLY_VERTEX" name="Atom #584"/>
<nrOfStructures value="17"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="593"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="592"/>
<atomRef index="511"/>
</cell>
<cell>
<cellProperties index="1053" type="POLY_VERTEX" name="Atom #585"/>
<nrOfStructures value="16"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="576"/>
<atomRef index="594"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="496"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="595"/>
<atomRef index="514"/>
</cell>
<cell>
<cellProperties index="1054" type="POLY_VERTEX" name="Atom #593"/>
<nrOfStructures value="16"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="584"/>
<atomRef index="602"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="502"/>
<atomRef index="592"/>
<atomRef index="511"/>
<atomRef index="601"/>
<atomRef index="520"/>
</cell>
<cell>
<cellProperties index="1055" type="POLY_VERTEX" name="Atom #594"/>
<nrOfStructures value="16"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="585"/>
<atomRef index="603"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="505"/>
<atomRef index="595"/>
<atomRef index="514"/>
<atomRef index="604"/>
<atomRef index="523"/>
</cell>
<cell>
<cellProperties index="1056" type="POLY_VERTEX" name="Atom #602"/>
<nrOfStructures value="16"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="593"/>
<atomRef index="611"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="511"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="610"/>
<atomRef index="529"/>
</cell>
<cell>
<cellProperties index="1057" type="POLY_VERTEX" name="Atom #603"/>
<nrOfStructures value="16"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="594"/>
<atomRef index="612"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="514"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="613"/>
<atomRef index="532"/>
</cell>
<cell>
<cellProperties index="1058" type="POLY_VERTEX" name="Atom #611"/>
<nrOfStructures value="17"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="602"/>
<atomRef index="620"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="619"/>
<atomRef index="538"/>
</cell>
<cell>
<cellProperties index="1059" type="POLY_VERTEX" name="Atom #612"/>
<nrOfStructures value="16"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="603"/>
<atomRef index="621"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="523"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="622"/>
<atomRef index="541"/>
</cell>
<cell>
<cellProperties index="1060" type="POLY_VERTEX" name="Atom #620"/>
<nrOfStructures value="17"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="611"/>
<atomRef index="629"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="628"/>
<atomRef index="547"/>
</cell>
<cell>
<cellProperties index="1061" type="POLY_VERTEX" name="Atom #621"/>
<nrOfStructures value="16"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="612"/>
<atomRef index="630"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="550"/>
</cell>
<cell>
<cellProperties index="1062" type="POLY_VERTEX" name="Atom #629"/>
<nrOfStructures value="16"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="620"/>
<atomRef index="638"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1063" type="POLY_VERTEX" name="Atom #630"/>
<nrOfStructures value="17"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="621"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="631"/>
<atomRef index="550"/>
</cell>
<cell>
<cellProperties index="1064" type="POLY_VERTEX" name="Atom #638"/>
<nrOfStructures value="17"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="629"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="727"/>
<atomRef index="728"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="637"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1065" type="POLY_VERTEX" name="Atom #639"/>
<nrOfStructures value="11"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="630"/>
<atomRef index="640"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="631"/>
<atomRef index="550"/>
</cell>
<cell>
<cellProperties index="1066" type="POLY_VERTEX" name="Atom #640"/>
<nrOfStructures value="17"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="641"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="632"/>
<atomRef index="551"/>
</cell>
<cell>
<cellProperties index="1067" type="POLY_VERTEX" name="Atom #641"/>
<nrOfStructures value="17"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="640"/>
<atomRef index="642"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="633"/>
<atomRef index="552"/>
</cell>
<cell>
<cellProperties index="1068" type="POLY_VERTEX" name="Atom #642"/>
<nrOfStructures value="17"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="641"/>
<atomRef index="643"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="724"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="633"/>
<atomRef index="552"/>
<atomRef index="634"/>
<atomRef index="553"/>
</cell>
<cell>
<cellProperties index="1069" type="POLY_VERTEX" name="Atom #643"/>
<nrOfStructures value="16"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="642"/>
<atomRef index="644"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="723"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="552"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="635"/>
<atomRef index="554"/>
</cell>
<cell>
<cellProperties index="1070" type="POLY_VERTEX" name="Atom #644"/>
<nrOfStructures value="16"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="643"/>
<atomRef index="645"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="726"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="555"/>
</cell>
<cell>
<cellProperties index="1071" type="POLY_VERTEX" name="Atom #645"/>
<nrOfStructures value="16"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="644"/>
<atomRef index="646"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="725"/>
<atomRef index="726"/>
<atomRef index="727"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1072" type="POLY_VERTEX" name="Atom #646"/>
<nrOfStructures value="17"/>
<atomRef index="557"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="638"/>
<atomRef index="645"/>
<atomRef index="647"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="726"/>
<atomRef index="727"/>
<atomRef index="728"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="637"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1073" type="POLY_VERTEX" name="Atom #647"/>
<nrOfStructures value="11"/>
<atomRef index="557"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="638"/>
<atomRef index="646"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="727"/>
<atomRef index="728"/>
<atomRef index="637"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1074" type="POLY_VERTEX" name="Atom #648"/>
<nrOfStructures value="7"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="576"/>
<atomRef index="649"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="577"/>
</cell>
<cell>
<cellProperties index="1075" type="POLY_VERTEX" name="Atom #649"/>
<nrOfStructures value="11"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="576"/>
<atomRef index="648"/>
<atomRef index="650"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="577"/>
<atomRef index="578"/>
</cell>
<cell>
<cellProperties index="1076" type="POLY_VERTEX" name="Atom #650"/>
<nrOfStructures value="10"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="649"/>
<atomRef index="651"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="577"/>
<atomRef index="578"/>
</cell>
<cell>
<cellProperties index="1077" type="POLY_VERTEX" name="Atom #651"/>
<nrOfStructures value="10"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="650"/>
<atomRef index="652"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="578"/>
<atomRef index="579"/>
</cell>
<cell>
<cellProperties index="1078" type="POLY_VERTEX" name="Atom #652"/>
<nrOfStructures value="10"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="651"/>
<atomRef index="653"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="579"/>
<atomRef index="580"/>
</cell>
<cell>
<cellProperties index="1079" type="POLY_VERTEX" name="Atom #653"/>
<nrOfStructures value="10"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="652"/>
<atomRef index="654"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="580"/>
<atomRef index="581"/>
</cell>
<cell>
<cellProperties index="1080" type="POLY_VERTEX" name="Atom #654"/>
<nrOfStructures value="10"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="653"/>
<atomRef index="655"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="581"/>
<atomRef index="582"/>
</cell>
<cell>
<cellProperties index="1081" type="POLY_VERTEX" name="Atom #655"/>
<nrOfStructures value="11"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="654"/>
<atomRef index="656"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="582"/>
<atomRef index="583"/>
</cell>
<cell>
<cellProperties index="1082" type="POLY_VERTEX" name="Atom #656"/>
<nrOfStructures value="7"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="655"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="583"/>
</cell>
<cell>
<cellProperties index="1083" type="POLY_VERTEX" name="Atom #657"/>
<nrOfStructures value="11"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="658"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="577"/>
<atomRef index="586"/>
</cell>
<cell>
<cellProperties index="1084" type="POLY_VERTEX" name="Atom #658"/>
<nrOfStructures value="17"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="657"/>
<atomRef index="659"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="577"/>
<atomRef index="586"/>
<atomRef index="578"/>
<atomRef index="587"/>
</cell>
<cell>
<cellProperties index="1085" type="POLY_VERTEX" name="Atom #659"/>
<nrOfStructures value="15"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="651"/>
<atomRef index="658"/>
<atomRef index="660"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="577"/>
<atomRef index="586"/>
<atomRef index="578"/>
<atomRef index="587"/>
</cell>
<cell>
<cellProperties index="1086" type="POLY_VERTEX" name="Atom #660"/>
<nrOfStructures value="15"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="650"/>
<atomRef index="651"/>
<atomRef index="652"/>
<atomRef index="659"/>
<atomRef index="661"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="578"/>
<atomRef index="587"/>
<atomRef index="579"/>
<atomRef index="588"/>
</cell>
<cell>
<cellProperties index="1087" type="POLY_VERTEX" name="Atom #661"/>
<nrOfStructures value="15"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="651"/>
<atomRef index="652"/>
<atomRef index="653"/>
<atomRef index="660"/>
<atomRef index="662"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="579"/>
<atomRef index="588"/>
<atomRef index="580"/>
<atomRef index="589"/>
</cell>
<cell>
<cellProperties index="1088" type="POLY_VERTEX" name="Atom #662"/>
<nrOfStructures value="15"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="652"/>
<atomRef index="653"/>
<atomRef index="654"/>
<atomRef index="661"/>
<atomRef index="663"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="580"/>
<atomRef index="589"/>
<atomRef index="581"/>
<atomRef index="590"/>
</cell>
<cell>
<cellProperties index="1089" type="POLY_VERTEX" name="Atom #663"/>
<nrOfStructures value="15"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="653"/>
<atomRef index="654"/>
<atomRef index="655"/>
<atomRef index="662"/>
<atomRef index="664"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="581"/>
<atomRef index="590"/>
<atomRef index="582"/>
<atomRef index="591"/>
</cell>
<cell>
<cellProperties index="1090" type="POLY_VERTEX" name="Atom #664"/>
<nrOfStructures value="17"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="654"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="663"/>
<atomRef index="665"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="582"/>
<atomRef index="591"/>
<atomRef index="583"/>
<atomRef index="592"/>
</cell>
<cell>
<cellProperties index="1091" type="POLY_VERTEX" name="Atom #665"/>
<nrOfStructures value="11"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="664"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="583"/>
<atomRef index="592"/>
</cell>
<cell>
<cellProperties index="1092" type="POLY_VERTEX" name="Atom #666"/>
<nrOfStructures value="10"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="667"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="586"/>
<atomRef index="595"/>
</cell>
<cell>
<cellProperties index="1093" type="POLY_VERTEX" name="Atom #667"/>
<nrOfStructures value="15"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="666"/>
<atomRef index="668"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="586"/>
<atomRef index="595"/>
<atomRef index="587"/>
<atomRef index="596"/>
</cell>
<cell>
<cellProperties index="1094" type="POLY_VERTEX" name="Atom #668"/>
<nrOfStructures value="12"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="667"/>
<atomRef index="669"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="586"/>
<atomRef index="595"/>
<atomRef index="587"/>
<atomRef index="596"/>
</cell>
<cell>
<cellProperties index="1095" type="POLY_VERTEX" name="Atom #669"/>
<nrOfStructures value="12"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="668"/>
<atomRef index="670"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="587"/>
<atomRef index="596"/>
<atomRef index="588"/>
<atomRef index="597"/>
</cell>
<cell>
<cellProperties index="1096" type="POLY_VERTEX" name="Atom #670"/>
<nrOfStructures value="12"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="669"/>
<atomRef index="671"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="588"/>
<atomRef index="597"/>
<atomRef index="589"/>
<atomRef index="598"/>
</cell>
<cell>
<cellProperties index="1097" type="POLY_VERTEX" name="Atom #671"/>
<nrOfStructures value="12"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="670"/>
<atomRef index="672"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="589"/>
<atomRef index="598"/>
<atomRef index="590"/>
<atomRef index="599"/>
</cell>
<cell>
<cellProperties index="1098" type="POLY_VERTEX" name="Atom #672"/>
<nrOfStructures value="12"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="671"/>
<atomRef index="673"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="590"/>
<atomRef index="599"/>
<atomRef index="591"/>
<atomRef index="600"/>
</cell>
<cell>
<cellProperties index="1099" type="POLY_VERTEX" name="Atom #673"/>
<nrOfStructures value="15"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="672"/>
<atomRef index="674"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="591"/>
<atomRef index="600"/>
<atomRef index="592"/>
<atomRef index="601"/>
</cell>
<cell>
<cellProperties index="1100" type="POLY_VERTEX" name="Atom #674"/>
<nrOfStructures value="10"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="673"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="592"/>
<atomRef index="601"/>
</cell>
<cell>
<cellProperties index="1101" type="POLY_VERTEX" name="Atom #675"/>
<nrOfStructures value="10"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="676"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="595"/>
<atomRef index="604"/>
</cell>
<cell>
<cellProperties index="1102" type="POLY_VERTEX" name="Atom #676"/>
<nrOfStructures value="15"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="675"/>
<atomRef index="677"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="595"/>
<atomRef index="604"/>
<atomRef index="596"/>
<atomRef index="605"/>
</cell>
<cell>
<cellProperties index="1103" type="POLY_VERTEX" name="Atom #677"/>
<nrOfStructures value="13"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="676"/>
<atomRef index="678"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="595"/>
<atomRef index="604"/>
<atomRef index="596"/>
<atomRef index="605"/>
<atomRef index="606"/>
</cell>
<cell>
<cellProperties index="1104" type="POLY_VERTEX" name="Atom #678"/>
<nrOfStructures value="12"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="677"/>
<atomRef index="679"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="596"/>
<atomRef index="605"/>
<atomRef index="597"/>
<atomRef index="606"/>
</cell>
<cell>
<cellProperties index="1105" type="POLY_VERTEX" name="Atom #679"/>
<nrOfStructures value="12"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="678"/>
<atomRef index="680"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="597"/>
<atomRef index="606"/>
<atomRef index="598"/>
<atomRef index="607"/>
</cell>
<cell>
<cellProperties index="1106" type="POLY_VERTEX" name="Atom #680"/>
<nrOfStructures value="12"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="679"/>
<atomRef index="681"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="598"/>
<atomRef index="607"/>
<atomRef index="599"/>
<atomRef index="608"/>
</cell>
<cell>
<cellProperties index="1107" type="POLY_VERTEX" name="Atom #681"/>
<nrOfStructures value="12"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="680"/>
<atomRef index="682"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="599"/>
<atomRef index="608"/>
<atomRef index="600"/>
<atomRef index="609"/>
</cell>
<cell>
<cellProperties index="1108" type="POLY_VERTEX" name="Atom #682"/>
<nrOfStructures value="15"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="681"/>
<atomRef index="683"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="600"/>
<atomRef index="609"/>
<atomRef index="601"/>
<atomRef index="610"/>
</cell>
<cell>
<cellProperties index="1109" type="POLY_VERTEX" name="Atom #683"/>
<nrOfStructures value="10"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="682"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="601"/>
<atomRef index="610"/>
</cell>
<cell>
<cellProperties index="1110" type="POLY_VERTEX" name="Atom #684"/>
<nrOfStructures value="10"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="685"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="604"/>
<atomRef index="613"/>
</cell>
<cell>
<cellProperties index="1111" type="POLY_VERTEX" name="Atom #685"/>
<nrOfStructures value="15"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="684"/>
<atomRef index="686"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="604"/>
<atomRef index="613"/>
<atomRef index="605"/>
<atomRef index="614"/>
</cell>
<cell>
<cellProperties index="1112" type="POLY_VERTEX" name="Atom #686"/>
<nrOfStructures value="14"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="685"/>
<atomRef index="687"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="604"/>
<atomRef index="613"/>
<atomRef index="605"/>
<atomRef index="614"/>
<atomRef index="606"/>
<atomRef index="615"/>
</cell>
<cell>
<cellProperties index="1113" type="POLY_VERTEX" name="Atom #687"/>
<nrOfStructures value="12"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="686"/>
<atomRef index="688"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="605"/>
<atomRef index="614"/>
<atomRef index="606"/>
<atomRef index="615"/>
</cell>
<cell>
<cellProperties index="1114" type="POLY_VERTEX" name="Atom #688"/>
<nrOfStructures value="12"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="687"/>
<atomRef index="689"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="606"/>
<atomRef index="615"/>
<atomRef index="607"/>
<atomRef index="616"/>
</cell>
<cell>
<cellProperties index="1115" type="POLY_VERTEX" name="Atom #689"/>
<nrOfStructures value="12"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="688"/>
<atomRef index="690"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="607"/>
<atomRef index="616"/>
<atomRef index="608"/>
<atomRef index="617"/>
</cell>
<cell>
<cellProperties index="1116" type="POLY_VERTEX" name="Atom #690"/>
<nrOfStructures value="12"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="689"/>
<atomRef index="691"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="608"/>
<atomRef index="617"/>
<atomRef index="609"/>
<atomRef index="618"/>
</cell>
<cell>
<cellProperties index="1117" type="POLY_VERTEX" name="Atom #691"/>
<nrOfStructures value="16"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="690"/>
<atomRef index="692"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="609"/>
<atomRef index="618"/>
<atomRef index="601"/>
<atomRef index="610"/>
<atomRef index="619"/>
</cell>
<cell>
<cellProperties index="1118" type="POLY_VERTEX" name="Atom #692"/>
<nrOfStructures value="11"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="691"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="601"/>
<atomRef index="610"/>
<atomRef index="619"/>
</cell>
<cell>
<cellProperties index="1119" type="POLY_VERTEX" name="Atom #693"/>
<nrOfStructures value="10"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="694"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="613"/>
<atomRef index="622"/>
</cell>
<cell>
<cellProperties index="1120" type="POLY_VERTEX" name="Atom #694"/>
<nrOfStructures value="15"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="693"/>
<atomRef index="695"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="613"/>
<atomRef index="622"/>
<atomRef index="614"/>
<atomRef index="623"/>
</cell>
<cell>
<cellProperties index="1121" type="POLY_VERTEX" name="Atom #695"/>
<nrOfStructures value="14"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="694"/>
<atomRef index="696"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="613"/>
<atomRef index="622"/>
<atomRef index="614"/>
<atomRef index="623"/>
<atomRef index="615"/>
<atomRef index="624"/>
</cell>
<cell>
<cellProperties index="1122" type="POLY_VERTEX" name="Atom #696"/>
<nrOfStructures value="12"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="695"/>
<atomRef index="697"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="614"/>
<atomRef index="615"/>
<atomRef index="624"/>
<atomRef index="625"/>
</cell>
<cell>
<cellProperties index="1123" type="POLY_VERTEX" name="Atom #697"/>
<nrOfStructures value="12"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="696"/>
<atomRef index="698"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="615"/>
<atomRef index="624"/>
<atomRef index="616"/>
<atomRef index="625"/>
</cell>
<cell>
<cellProperties index="1124" type="POLY_VERTEX" name="Atom #698"/>
<nrOfStructures value="12"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="697"/>
<atomRef index="699"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="616"/>
<atomRef index="625"/>
<atomRef index="617"/>
<atomRef index="626"/>
</cell>
<cell>
<cellProperties index="1125" type="POLY_VERTEX" name="Atom #699"/>
<nrOfStructures value="12"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="698"/>
<atomRef index="700"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="617"/>
<atomRef index="626"/>
<atomRef index="618"/>
<atomRef index="627"/>
</cell>
<cell>
<cellProperties index="1126" type="POLY_VERTEX" name="Atom #700"/>
<nrOfStructures value="16"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="699"/>
<atomRef index="701"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="618"/>
<atomRef index="627"/>
<atomRef index="610"/>
<atomRef index="619"/>
<atomRef index="628"/>
</cell>
<cell>
<cellProperties index="1127" type="POLY_VERTEX" name="Atom #701"/>
<nrOfStructures value="11"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="700"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="610"/>
<atomRef index="619"/>
<atomRef index="628"/>
</cell>
<cell>
<cellProperties index="1128" type="POLY_VERTEX" name="Atom #702"/>
<nrOfStructures value="10"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="703"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="613"/>
<atomRef index="622"/>
</cell>
<cell>
<cellProperties index="1129" type="POLY_VERTEX" name="Atom #703"/>
<nrOfStructures value="15"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="702"/>
<atomRef index="704"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="613"/>
<atomRef index="622"/>
<atomRef index="623"/>
<atomRef index="632"/>
</cell>
<cell>
<cellProperties index="1130" type="POLY_VERTEX" name="Atom #704"/>
<nrOfStructures value="15"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="703"/>
<atomRef index="705"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="613"/>
<atomRef index="622"/>
<atomRef index="623"/>
<atomRef index="632"/>
<atomRef index="615"/>
<atomRef index="624"/>
<atomRef index="633"/>
</cell>
<cell>
<cellProperties index="1131" type="POLY_VERTEX" name="Atom #705"/>
<nrOfStructures value="14"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="704"/>
<atomRef index="706"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="632"/>
<atomRef index="615"/>
<atomRef index="624"/>
<atomRef index="633"/>
<atomRef index="625"/>
<atomRef index="634"/>
</cell>
<cell>
<cellProperties index="1132" type="POLY_VERTEX" name="Atom #706"/>
<nrOfStructures value="13"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="705"/>
<atomRef index="707"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="615"/>
<atomRef index="624"/>
<atomRef index="625"/>
<atomRef index="634"/>
<atomRef index="635"/>
</cell>
<cell>
<cellProperties index="1133" type="POLY_VERTEX" name="Atom #707"/>
<nrOfStructures value="12"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="706"/>
<atomRef index="708"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="625"/>
<atomRef index="634"/>
<atomRef index="626"/>
<atomRef index="635"/>
</cell>
<cell>
<cellProperties index="1134" type="POLY_VERTEX" name="Atom #708"/>
<nrOfStructures value="13"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="707"/>
<atomRef index="709"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="626"/>
<atomRef index="635"/>
<atomRef index="618"/>
<atomRef index="627"/>
<atomRef index="636"/>
</cell>
<cell>
<cellProperties index="1135" type="POLY_VERTEX" name="Atom #709"/>
<nrOfStructures value="16"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="708"/>
<atomRef index="710"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="618"/>
<atomRef index="627"/>
<atomRef index="636"/>
<atomRef index="619"/>
<atomRef index="628"/>
</cell>
<cell>
<cellProperties index="1136" type="POLY_VERTEX" name="Atom #710"/>
<nrOfStructures value="10"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="709"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="619"/>
<atomRef index="628"/>
</cell>
<cell>
<cellProperties index="1137" type="POLY_VERTEX" name="Atom #711"/>
<nrOfStructures value="11"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="712"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="622"/>
<atomRef index="631"/>
</cell>
<cell>
<cellProperties index="1138" type="POLY_VERTEX" name="Atom #712"/>
<nrOfStructures value="17"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="711"/>
<atomRef index="713"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="622"/>
<atomRef index="631"/>
<atomRef index="623"/>
<atomRef index="632"/>
</cell>
<cell>
<cellProperties index="1139" type="POLY_VERTEX" name="Atom #713"/>
<nrOfStructures value="17"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="712"/>
<atomRef index="714"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="622"/>
<atomRef index="631"/>
<atomRef index="623"/>
<atomRef index="632"/>
<atomRef index="624"/>
<atomRef index="633"/>
</cell>
<cell>
<cellProperties index="1140" type="POLY_VERTEX" name="Atom #714"/>
<nrOfStructures value="16"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="713"/>
<atomRef index="715"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="724"/>
<atomRef index="632"/>
<atomRef index="624"/>
<atomRef index="633"/>
<atomRef index="625"/>
<atomRef index="634"/>
</cell>
<cell>
<cellProperties index="1141" type="POLY_VERTEX" name="Atom #715"/>
<nrOfStructures value="15"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="714"/>
<atomRef index="716"/>
<atomRef index="723"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="624"/>
<atomRef index="625"/>
<atomRef index="634"/>
<atomRef index="635"/>
</cell>
<cell>
<cellProperties index="1142" type="POLY_VERTEX" name="Atom #716"/>
<nrOfStructures value="15"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="715"/>
<atomRef index="717"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="726"/>
<atomRef index="625"/>
<atomRef index="634"/>
<atomRef index="626"/>
<atomRef index="635"/>
</cell>
<cell>
<cellProperties index="1143" type="POLY_VERTEX" name="Atom #717"/>
<nrOfStructures value="15"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="716"/>
<atomRef index="718"/>
<atomRef index="725"/>
<atomRef index="726"/>
<atomRef index="727"/>
<atomRef index="626"/>
<atomRef index="635"/>
<atomRef index="627"/>
<atomRef index="636"/>
</cell>
<cell>
<cellProperties index="1144" type="POLY_VERTEX" name="Atom #718"/>
<nrOfStructures value="17"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="717"/>
<atomRef index="719"/>
<atomRef index="726"/>
<atomRef index="727"/>
<atomRef index="728"/>
<atomRef index="627"/>
<atomRef index="636"/>
<atomRef index="628"/>
<atomRef index="637"/>
</cell>
<cell>
<cellProperties index="1145" type="POLY_VERTEX" name="Atom #719"/>
<nrOfStructures value="11"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="718"/>
<atomRef index="727"/>
<atomRef index="728"/>
<atomRef index="628"/>
<atomRef index="637"/>
</cell>
<cell>
<cellProperties index="1146" type="POLY_VERTEX" name="Atom #720"/>
<nrOfStructures value="7"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="721"/>
<atomRef index="631"/>
</cell>
<cell>
<cellProperties index="1147" type="POLY_VERTEX" name="Atom #721"/>
<nrOfStructures value="11"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="720"/>
<atomRef index="722"/>
<atomRef index="631"/>
<atomRef index="632"/>
</cell>
<cell>
<cellProperties index="1148" type="POLY_VERTEX" name="Atom #722"/>
<nrOfStructures value="11"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="721"/>
<atomRef index="723"/>
<atomRef index="631"/>
<atomRef index="632"/>
<atomRef index="633"/>
</cell>
<cell>
<cellProperties index="1149" type="POLY_VERTEX" name="Atom #723"/>
<nrOfStructures value="11"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="722"/>
<atomRef index="724"/>
<atomRef index="632"/>
<atomRef index="633"/>
<atomRef index="634"/>
</cell>
<cell>
<cellProperties index="1150" type="POLY_VERTEX" name="Atom #724"/>
<nrOfStructures value="10"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="723"/>
<atomRef index="725"/>
<atomRef index="634"/>
<atomRef index="635"/>
</cell>
<cell>
<cellProperties index="1151" type="POLY_VERTEX" name="Atom #725"/>
<nrOfStructures value="10"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="724"/>
<atomRef index="726"/>
<atomRef index="634"/>
<atomRef index="635"/>
</cell>
<cell>
<cellProperties index="1152" type="POLY_VERTEX" name="Atom #726"/>
<nrOfStructures value="10"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="725"/>
<atomRef index="727"/>
<atomRef index="635"/>
<atomRef index="636"/>
</cell>
<cell>
<cellProperties index="1153" type="POLY_VERTEX" name="Atom #727"/>
<nrOfStructures value="11"/>
<atomRef index="638"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="726"/>
<atomRef index="728"/>
<atomRef index="636"/>
<atomRef index="637"/>
</cell>
<cell>
<cellProperties index="1154" type="POLY_VERTEX" name="Atom #728"/>
<nrOfStructures value="7"/>
<atomRef index="638"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="727"/>
<atomRef index="637"/>
</cell>
<cell>
<cellProperties index="1155" type="POLY_VERTEX" name="Atom #577"/>
<nrOfStructures value="19"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="495"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="576"/>
<atomRef index="648"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="496"/>
<atomRef index="586"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="587"/>
</cell>
<cell>
<cellProperties index="1156" type="POLY_VERTEX" name="Atom #496"/>
<nrOfStructures value="25"/>
<atomRef index="405"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="486"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="567"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="577"/>
<atomRef index="415"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="587"/>
<atomRef index="506"/>
<atomRef index="425"/>
</cell>
<cell>
<cellProperties index="1157" type="POLY_VERTEX" name="Atom #415"/>
<nrOfStructures value="17"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="496"/>
<atomRef index="334"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="344"/>
</cell>
<cell>
<cellProperties index="1158" type="POLY_VERTEX" name="Atom #334"/>
<nrOfStructures value="16"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="415"/>
<atomRef index="253"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="263"/>
</cell>
<cell>
<cellProperties index="1159" type="POLY_VERTEX" name="Atom #253"/>
<nrOfStructures value="17"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="334"/>
<atomRef index="172"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="182"/>
</cell>
<cell>
<cellProperties index="1160" type="POLY_VERTEX" name="Atom #172"/>
<nrOfStructures value="17"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="253"/>
<atomRef index="91"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
</cell>
<cell>
<cellProperties index="1161" type="POLY_VERTEX" name="Atom #91"/>
<nrOfStructures value="17"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="172"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="182"/>
<atomRef index="101"/>
</cell>
<cell>
<cellProperties index="1162" type="POLY_VERTEX" name="Atom #586"/>
<nrOfStructures value="19"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="657"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="505"/>
<atomRef index="595"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="587"/>
<atomRef index="506"/>
<atomRef index="596"/>
</cell>
<cell>
<cellProperties index="1163" type="POLY_VERTEX" name="Atom #505"/>
<nrOfStructures value="23"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="495"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="576"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="586"/>
<atomRef index="424"/>
<atomRef index="595"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="497"/>
<atomRef index="587"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="596"/>
<atomRef index="515"/>
<atomRef index="434"/>
</cell>
<cell>
<cellProperties index="1164" type="POLY_VERTEX" name="Atom #424"/>
<nrOfStructures value="16"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="505"/>
<atomRef index="343"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="497"/>
<atomRef index="335"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="353"/>
</cell>
<cell>
<cellProperties index="1165" type="POLY_VERTEX" name="Atom #343"/>
<nrOfStructures value="25"/>
<atomRef index="252"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="333"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="414"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="424"/>
<atomRef index="262"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="271"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="272"/>
</cell>
<cell>
<cellProperties index="1166" type="POLY_VERTEX" name="Atom #262"/>
<nrOfStructures value="26"/>
<atomRef index="171"/>
<atomRef index="180"/>
<atomRef index="189"/>
<atomRef index="252"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="333"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="343"/>
<atomRef index="181"/>
<atomRef index="352"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
</cell>
<cell>
<cellProperties index="1167" type="POLY_VERTEX" name="Atom #181"/>
<nrOfStructures value="17"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="262"/>
<atomRef index="100"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
</cell>
<cell>
<cellProperties index="1168" type="POLY_VERTEX" name="Atom #100"/>
<nrOfStructures value="17"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="181"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="191"/>
<atomRef index="110"/>
</cell>
<cell>
<cellProperties index="1169" type="POLY_VERTEX" name="Atom #595"/>
<nrOfStructures value="16"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="666"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="514"/>
<atomRef index="604"/>
<atomRef index="596"/>
<atomRef index="605"/>
</cell>
<cell>
<cellProperties index="1170" type="POLY_VERTEX" name="Atom #514"/>
<nrOfStructures value="24"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="585"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="595"/>
<atomRef index="433"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="596"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="605"/>
<atomRef index="524"/>
<atomRef index="443"/>
</cell>
<cell>
<cellProperties index="1171" type="POLY_VERTEX" name="Atom #433"/>
<nrOfStructures value="25"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="504"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="514"/>
<atomRef index="352"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
</cell>
<cell>
<cellProperties index="1172" type="POLY_VERTEX" name="Atom #352"/>
<nrOfStructures value="20"/>
<atomRef index="261"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="342"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="423"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="433"/>
<atomRef index="271"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="362"/>
</cell>
<cell>
<cellProperties index="1173" type="POLY_VERTEX" name="Atom #271"/>
<nrOfStructures value="16"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="352"/>
<atomRef index="190"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
</cell>
<cell>
<cellProperties index="1174" type="POLY_VERTEX" name="Atom #190"/>
<nrOfStructures value="17"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="271"/>
<atomRef index="109"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="1175" type="POLY_VERTEX" name="Atom #109"/>
<nrOfStructures value="16"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="190"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="200"/>
</cell>
<cell>
<cellProperties index="1176" type="POLY_VERTEX" name="Atom #604"/>
<nrOfStructures value="17"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="675"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="595"/>
<atomRef index="514"/>
<atomRef index="523"/>
<atomRef index="515"/>
<atomRef index="605"/>
<atomRef index="524"/>
<atomRef index="614"/>
</cell>
<cell>
<cellProperties index="1177" type="POLY_VERTEX" name="Atom #523"/>
<nrOfStructures value="24"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="594"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="604"/>
<atomRef index="442"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="605"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="452"/>
</cell>
<cell>
<cellProperties index="1178" type="POLY_VERTEX" name="Atom #442"/>
<nrOfStructures value="26"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="513"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="523"/>
<atomRef index="361"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
</cell>
<cell>
<cellProperties index="1179" type="POLY_VERTEX" name="Atom #361"/>
<nrOfStructures value="26"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="432"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="271"/>
<atomRef index="442"/>
<atomRef index="280"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="290"/>
</cell>
<cell>
<cellProperties index="1180" type="POLY_VERTEX" name="Atom #280"/>
<nrOfStructures value="26"/>
<atomRef index="189"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="351"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="352"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="361"/>
<atomRef index="199"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
</cell>
<cell>
<cellProperties index="1181" type="POLY_VERTEX" name="Atom #199"/>
<nrOfStructures value="26"/>
<atomRef index="108"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="189"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="270"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="280"/>
<atomRef index="118"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
</cell>
<cell>
<cellProperties index="1182" type="POLY_VERTEX" name="Atom #118"/>
<nrOfStructures value="17"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="199"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="209"/>
<atomRef index="128"/>
</cell>
<cell>
<cellProperties index="1183" type="POLY_VERTEX" name="Atom #613"/>
<nrOfStructures value="23"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="684"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="523"/>
<atomRef index="532"/>
<atomRef index="541"/>
<atomRef index="524"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="623"/>
<atomRef index="542"/>
</cell>
<cell>
<cellProperties index="1184" type="POLY_VERTEX" name="Atom #532"/>
<nrOfStructures value="24"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="603"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="613"/>
<atomRef index="451"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="461"/>
</cell>
<cell>
<cellProperties index="1185" type="POLY_VERTEX" name="Atom #451"/>
<nrOfStructures value="26"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="522"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="532"/>
<atomRef index="370"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="380"/>
</cell>
<cell>
<cellProperties index="1186" type="POLY_VERTEX" name="Atom #370"/>
<nrOfStructures value="18"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="441"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="451"/>
<atomRef index="289"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="371"/>
</cell>
<cell>
<cellProperties index="1187" type="POLY_VERTEX" name="Atom #289"/>
<nrOfStructures value="26"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="360"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="370"/>
<atomRef index="208"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="218"/>
</cell>
<cell>
<cellProperties index="1188" type="POLY_VERTEX" name="Atom #208"/>
<nrOfStructures value="26"/>
<atomRef index="117"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="198"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="279"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="289"/>
<atomRef index="127"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="1189" type="POLY_VERTEX" name="Atom #127"/>
<nrOfStructures value="17"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="208"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="218"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="1190" type="POLY_VERTEX" name="Atom #622"/>
<nrOfStructures value="23"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="693"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="702"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="532"/>
<atomRef index="541"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="533"/>
<atomRef index="623"/>
<atomRef index="632"/>
<atomRef index="551"/>
</cell>
<cell>
<cellProperties index="1191" type="POLY_VERTEX" name="Atom #541"/>
<nrOfStructures value="25"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="612"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="622"/>
<atomRef index="460"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="470"/>
</cell>
<cell>
<cellProperties index="1192" type="POLY_VERTEX" name="Atom #460"/>
<nrOfStructures value="23"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="531"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="541"/>
<atomRef index="379"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="551"/>
<atomRef index="389"/>
</cell>
<cell>
<cellProperties index="1193" type="POLY_VERTEX" name="Atom #379"/>
<nrOfStructures value="21"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="450"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="460"/>
<atomRef index="298"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="371"/>
<atomRef index="461"/>
<atomRef index="470"/>
<atomRef index="308"/>
</cell>
<cell>
<cellProperties index="1194" type="POLY_VERTEX" name="Atom #298"/>
<nrOfStructures value="26"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="369"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="379"/>
<atomRef index="217"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
</cell>
<cell>
<cellProperties index="1195" type="POLY_VERTEX" name="Atom #217"/>
<nrOfStructures value="26"/>
<atomRef index="126"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="207"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="288"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="298"/>
<atomRef index="136"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="1196" type="POLY_VERTEX" name="Atom #136"/>
<nrOfStructures value="17"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="217"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="227"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="1197" type="POLY_VERTEX" name="Atom #631"/>
<nrOfStructures value="20"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="711"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="720"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="550"/>
<atomRef index="623"/>
<atomRef index="632"/>
<atomRef index="551"/>
</cell>
<cell>
<cellProperties index="1198" type="POLY_VERTEX" name="Atom #550"/>
<nrOfStructures value="25"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="621"/>
<atomRef index="630"/>
<atomRef index="639"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="631"/>
<atomRef index="469"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="470"/>
</cell>
<cell>
<cellProperties index="1199" type="POLY_VERTEX" name="Atom #469"/>
<nrOfStructures value="25"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="540"/>
<atomRef index="549"/>
<atomRef index="558"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="550"/>
<atomRef index="388"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="389"/>
</cell>
<cell>
<cellProperties index="1200" type="POLY_VERTEX" name="Atom #388"/>
<nrOfStructures value="25"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="459"/>
<atomRef index="468"/>
<atomRef index="477"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="469"/>
<atomRef index="307"/>
<atomRef index="461"/>
<atomRef index="299"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="308"/>
</cell>
<cell>
<cellProperties index="1201" type="POLY_VERTEX" name="Atom #307"/>
<nrOfStructures value="25"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="378"/>
<atomRef index="387"/>
<atomRef index="396"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="388"/>
<atomRef index="226"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
</cell>
<cell>
<cellProperties index="1202" type="POLY_VERTEX" name="Atom #226"/>
<nrOfStructures value="26"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="297"/>
<atomRef index="306"/>
<atomRef index="315"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="307"/>
<atomRef index="145"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="1203" type="POLY_VERTEX" name="Atom #145"/>
<nrOfStructures value="26"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="135"/>
<atomRef index="144"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="216"/>
<atomRef index="225"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="226"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="227"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="1204" type="POLY_VERTEX" name="Atom #578"/>
<nrOfStructures value="20"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="649"/>
<atomRef index="650"/>
<atomRef index="651"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="586"/>
<atomRef index="497"/>
<atomRef index="587"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="588"/>
</cell>
<cell>
<cellProperties index="1205" type="POLY_VERTEX" name="Atom #497"/>
<nrOfStructures value="26"/>
<atomRef index="406"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="487"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="568"/>
<atomRef index="569"/>
<atomRef index="570"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="578"/>
<atomRef index="416"/>
<atomRef index="587"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="588"/>
<atomRef index="507"/>
<atomRef index="426"/>
</cell>
<cell>
<cellProperties index="1206" type="POLY_VERTEX" name="Atom #416"/>
<nrOfStructures value="18"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="488"/>
<atomRef index="489"/>
<atomRef index="415"/>
<atomRef index="497"/>
<atomRef index="335"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="345"/>
</cell>
<cell>
<cellProperties index="1207" type="POLY_VERTEX" name="Atom #335"/>
<nrOfStructures value="23"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="407"/>
<atomRef index="408"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="416"/>
<atomRef index="254"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="1208" type="POLY_VERTEX" name="Atom #254"/>
<nrOfStructures value="23"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="335"/>
<atomRef index="173"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
</cell>
<cell>
<cellProperties index="1209" type="POLY_VERTEX" name="Atom #173"/>
<nrOfStructures value="23"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="254"/>
<atomRef index="92"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
</cell>
<cell>
<cellProperties index="1210" type="POLY_VERTEX" name="Atom #92"/>
<nrOfStructures value="22"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="173"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="183"/>
<atomRef index="102"/>
</cell>
<cell>
<cellProperties index="1211" type="POLY_VERTEX" name="Atom #587"/>
<nrOfStructures value="15"/>
<atomRef index="658"/>
<atomRef index="659"/>
<atomRef index="660"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="577"/>
<atomRef index="496"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="506"/>
<atomRef index="596"/>
<atomRef index="507"/>
</cell>
<cell>
<cellProperties index="1212" type="POLY_VERTEX" name="Atom #506"/>
<nrOfStructures value="22"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="587"/>
<atomRef index="425"/>
<atomRef index="596"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="588"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="435"/>
</cell>
<cell>
<cellProperties index="1213" type="POLY_VERTEX" name="Atom #425"/>
<nrOfStructures value="25"/>
<atomRef index="496"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="506"/>
<atomRef index="344"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="354"/>
</cell>
<cell>
<cellProperties index="1214" type="POLY_VERTEX" name="Atom #344"/>
<nrOfStructures value="17"/>
<atomRef index="415"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="425"/>
<atomRef index="263"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="1215" type="POLY_VERTEX" name="Atom #263"/>
<nrOfStructures value="25"/>
<atomRef index="334"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="344"/>
<atomRef index="182"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
</cell>
<cell>
<cellProperties index="1216" type="POLY_VERTEX" name="Atom #182"/>
<nrOfStructures value="26"/>
<atomRef index="253"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="263"/>
<atomRef index="101"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
</cell>
<cell>
<cellProperties index="1217" type="POLY_VERTEX" name="Atom #101"/>
<nrOfStructures value="24"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="172"/>
<atomRef index="91"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="182"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="192"/>
<atomRef index="111"/>
</cell>
<cell>
<cellProperties index="1218" type="POLY_VERTEX" name="Atom #596"/>
<nrOfStructures value="15"/>
<atomRef index="667"/>
<atomRef index="668"/>
<atomRef index="669"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="586"/>
<atomRef index="505"/>
<atomRef index="595"/>
<atomRef index="514"/>
<atomRef index="587"/>
<atomRef index="506"/>
<atomRef index="515"/>
<atomRef index="507"/>
<atomRef index="516"/>
</cell>
<cell>
<cellProperties index="1219" type="POLY_VERTEX" name="Atom #515"/>
<nrOfStructures value="21"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="596"/>
<atomRef index="434"/>
<atomRef index="605"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="606"/>
<atomRef index="525"/>
</cell>
<cell>
<cellProperties index="1220" type="POLY_VERTEX" name="Atom #434"/>
<nrOfStructures value="24"/>
<atomRef index="505"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="515"/>
<atomRef index="353"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="525"/>
<atomRef index="363"/>
</cell>
<cell>
<cellProperties index="1221" type="POLY_VERTEX" name="Atom #353"/>
<nrOfStructures value="24"/>
<atomRef index="424"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="271"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="425"/>
<atomRef index="263"/>
<atomRef index="434"/>
<atomRef index="272"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="363"/>
<atomRef index="282"/>
</cell>
<cell>
<cellProperties index="1222" type="POLY_VERTEX" name="Atom #272"/>
<nrOfStructures value="24"/>
<atomRef index="343"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="353"/>
<atomRef index="191"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
</cell>
<cell>
<cellProperties index="1223" type="POLY_VERTEX" name="Atom #191"/>
<nrOfStructures value="26"/>
<atomRef index="262"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="272"/>
<atomRef index="110"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
</cell>
<cell>
<cellProperties index="1224" type="POLY_VERTEX" name="Atom #110"/>
<nrOfStructures value="23"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="181"/>
<atomRef index="100"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="191"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="201"/>
<atomRef index="120"/>
</cell>
<cell>
<cellProperties index="1225" type="POLY_VERTEX" name="Atom #605"/>
<nrOfStructures value="14"/>
<atomRef index="676"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="595"/>
<atomRef index="514"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="515"/>
<atomRef index="524"/>
<atomRef index="614"/>
<atomRef index="606"/>
</cell>
<cell>
<cellProperties index="1226" type="POLY_VERTEX" name="Atom #524"/>
<nrOfStructures value="23"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="605"/>
<atomRef index="443"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="606"/>
<atomRef index="525"/>
<atomRef index="444"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="453"/>
</cell>
<cell>
<cellProperties index="1227" type="POLY_VERTEX" name="Atom #443"/>
<nrOfStructures value="24"/>
<atomRef index="514"/>
<atomRef index="433"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="524"/>
<atomRef index="362"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="525"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
</cell>
<cell>
<cellProperties index="1228" type="POLY_VERTEX" name="Atom #362"/>
<nrOfStructures value="25"/>
<atomRef index="433"/>
<atomRef index="352"/>
<atomRef index="271"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="451"/>
<atomRef index="289"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="443"/>
<atomRef index="281"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
</cell>
<cell>
<cellProperties index="1229" type="POLY_VERTEX" name="Atom #281"/>
<nrOfStructures value="24"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="362"/>
<atomRef index="200"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
</cell>
<cell>
<cellProperties index="1230" type="POLY_VERTEX" name="Atom #200"/>
<nrOfStructures value="26"/>
<atomRef index="271"/>
<atomRef index="190"/>
<atomRef index="109"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="281"/>
<atomRef index="119"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
</cell>
<cell>
<cellProperties index="1231" type="POLY_VERTEX" name="Atom #119"/>
<nrOfStructures value="24"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="190"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="200"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="192"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="210"/>
<atomRef index="129"/>
</cell>
<cell>
<cellProperties index="1232" type="POLY_VERTEX" name="Atom #614"/>
<nrOfStructures value="18"/>
<atomRef index="685"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="605"/>
<atomRef index="524"/>
<atomRef index="533"/>
<atomRef index="606"/>
<atomRef index="525"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="624"/>
</cell>
<cell>
<cellProperties index="1233" type="POLY_VERTEX" name="Atom #533"/>
<nrOfStructures value="23"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="614"/>
<atomRef index="452"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="525"/>
<atomRef index="444"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="624"/>
<atomRef index="543"/>
<atomRef index="462"/>
</cell>
<cell>
<cellProperties index="1234" type="POLY_VERTEX" name="Atom #452"/>
<nrOfStructures value="24"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="533"/>
<atomRef index="371"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="525"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="381"/>
</cell>
<cell>
<cellProperties index="1235" type="POLY_VERTEX" name="Atom #371"/>
<nrOfStructures value="26"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="451"/>
<atomRef index="370"/>
<atomRef index="289"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="452"/>
<atomRef index="290"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
</cell>
<cell>
<cellProperties index="1236" type="POLY_VERTEX" name="Atom #290"/>
<nrOfStructures value="24"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="371"/>
<atomRef index="209"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
</cell>
<cell>
<cellProperties index="1237" type="POLY_VERTEX" name="Atom #209"/>
<nrOfStructures value="26"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="290"/>
<atomRef index="128"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
</cell>
<cell>
<cellProperties index="1238" type="POLY_VERTEX" name="Atom #128"/>
<nrOfStructures value="23"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="209"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="219"/>
<atomRef index="138"/>
</cell>
<cell>
<cellProperties index="1239" type="POLY_VERTEX" name="Atom #623"/>
<nrOfStructures value="18"/>
<atomRef index="694"/>
<atomRef index="695"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="533"/>
<atomRef index="542"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="624"/>
<atomRef index="633"/>
</cell>
<cell>
<cellProperties index="1240" type="POLY_VERTEX" name="Atom #542"/>
<nrOfStructures value="22"/>
<atomRef index="613"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="623"/>
<atomRef index="461"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="624"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="633"/>
<atomRef index="552"/>
</cell>
<cell>
<cellProperties index="1241" type="POLY_VERTEX" name="Atom #461"/>
<nrOfStructures value="24"/>
<atomRef index="532"/>
<atomRef index="451"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="379"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="542"/>
<atomRef index="380"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="552"/>
<atomRef index="390"/>
</cell>
<cell>
<cellProperties index="1242" type="POLY_VERTEX" name="Atom #380"/>
<nrOfStructures value="19"/>
<atomRef index="451"/>
<atomRef index="289"/>
<atomRef index="298"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="461"/>
<atomRef index="299"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="390"/>
<atomRef index="309"/>
</cell>
<cell>
<cellProperties index="1243" type="POLY_VERTEX" name="Atom #299"/>
<nrOfStructures value="24"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="380"/>
<atomRef index="218"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="228"/>
</cell>
<cell>
<cellProperties index="1244" type="POLY_VERTEX" name="Atom #218"/>
<nrOfStructures value="26"/>
<atomRef index="289"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="299"/>
<atomRef index="137"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="1245" type="POLY_VERTEX" name="Atom #137"/>
<nrOfStructures value="23"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="208"/>
<atomRef index="127"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="218"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="228"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="1246" type="POLY_VERTEX" name="Atom #632"/>
<nrOfStructures value="24"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="703"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="712"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="721"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="551"/>
<atomRef index="624"/>
<atomRef index="633"/>
</cell>
<cell>
<cellProperties index="1247" type="POLY_VERTEX" name="Atom #551"/>
<nrOfStructures value="26"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="640"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="622"/>
<atomRef index="541"/>
<atomRef index="460"/>
<atomRef index="631"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="632"/>
<atomRef index="470"/>
<atomRef index="624"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="633"/>
<atomRef index="552"/>
<atomRef index="471"/>
</cell>
<cell>
<cellProperties index="1248" type="POLY_VERTEX" name="Atom #470"/>
<nrOfStructures value="25"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="559"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="541"/>
<atomRef index="379"/>
<atomRef index="550"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="551"/>
<atomRef index="389"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="390"/>
</cell>
<cell>
<cellProperties index="1249" type="POLY_VERTEX" name="Atom #389"/>
<nrOfStructures value="25"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="478"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="460"/>
<atomRef index="298"/>
<atomRef index="469"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="470"/>
<atomRef index="308"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="309"/>
</cell>
<cell>
<cellProperties index="1250" type="POLY_VERTEX" name="Atom #308"/>
<nrOfStructures value="26"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="397"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="379"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="388"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="389"/>
<atomRef index="227"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="228"/>
</cell>
<cell>
<cellProperties index="1251" type="POLY_VERTEX" name="Atom #227"/>
<nrOfStructures value="26"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="298"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="307"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="308"/>
<atomRef index="146"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="1252" type="POLY_VERTEX" name="Atom #146"/>
<nrOfStructures value="26"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="217"/>
<atomRef index="136"/>
<atomRef index="226"/>
<atomRef index="145"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="227"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="228"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="1253" type="POLY_VERTEX" name="Atom #579"/>
<nrOfStructures value="15"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="651"/>
<atomRef index="652"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="498"/>
<atomRef index="588"/>
<atomRef index="580"/>
<atomRef index="499"/>
<atomRef index="589"/>
</cell>
<cell>
<cellProperties index="1254" type="POLY_VERTEX" name="Atom #498"/>
<nrOfStructures value="20"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="579"/>
<atomRef index="417"/>
<atomRef index="588"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="589"/>
<atomRef index="508"/>
</cell>
<cell>
<cellProperties index="1255" type="POLY_VERTEX" name="Atom #417"/>
<nrOfStructures value="23"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="498"/>
<atomRef index="336"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="346"/>
</cell>
<cell>
<cellProperties index="1256" type="POLY_VERTEX" name="Atom #336"/>
<nrOfStructures value="22"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="417"/>
<atomRef index="255"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="346"/>
<atomRef index="265"/>
</cell>
<cell>
<cellProperties index="1257" type="POLY_VERTEX" name="Atom #255"/>
<nrOfStructures value="23"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="336"/>
<atomRef index="174"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
</cell>
<cell>
<cellProperties index="1258" type="POLY_VERTEX" name="Atom #174"/>
<nrOfStructures value="23"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="255"/>
<atomRef index="93"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="1259" type="POLY_VERTEX" name="Atom #93"/>
<nrOfStructures value="21"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="174"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="184"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="1260" type="POLY_VERTEX" name="Atom #588"/>
<nrOfStructures value="15"/>
<atomRef index="660"/>
<atomRef index="661"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="578"/>
<atomRef index="497"/>
<atomRef index="506"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="507"/>
<atomRef index="597"/>
<atomRef index="499"/>
<atomRef index="589"/>
<atomRef index="508"/>
<atomRef index="598"/>
</cell>
<cell>
<cellProperties index="1261" type="POLY_VERTEX" name="Atom #507"/>
<nrOfStructures value="19"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="587"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="596"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="588"/>
<atomRef index="426"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="499"/>
<atomRef index="508"/>
<atomRef index="598"/>
<atomRef index="517"/>
</cell>
<cell>
<cellProperties index="1262" type="POLY_VERTEX" name="Atom #426"/>
<nrOfStructures value="25"/>
<atomRef index="497"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="507"/>
<atomRef index="345"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="508"/>
<atomRef index="346"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="355"/>
</cell>
<cell>
<cellProperties index="1263" type="POLY_VERTEX" name="Atom #345"/>
<nrOfStructures value="25"/>
<atomRef index="416"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="425"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="426"/>
<atomRef index="264"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="274"/>
</cell>
<cell>
<cellProperties index="1264" type="POLY_VERTEX" name="Atom #264"/>
<nrOfStructures value="26"/>
<atomRef index="335"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="344"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="345"/>
<atomRef index="183"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="193"/>
</cell>
<cell>
<cellProperties index="1265" type="POLY_VERTEX" name="Atom #183"/>
<nrOfStructures value="26"/>
<atomRef index="254"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="264"/>
<atomRef index="102"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
</cell>
<cell>
<cellProperties index="1266" type="POLY_VERTEX" name="Atom #102"/>
<nrOfStructures value="23"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="173"/>
<atomRef index="92"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="183"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="193"/>
<atomRef index="112"/>
</cell>
<cell>
<cellProperties index="1267" type="POLY_VERTEX" name="Atom #597"/>
<nrOfStructures value="14"/>
<atomRef index="669"/>
<atomRef index="670"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="506"/>
<atomRef index="515"/>
<atomRef index="588"/>
<atomRef index="507"/>
<atomRef index="516"/>
<atomRef index="606"/>
<atomRef index="508"/>
<atomRef index="598"/>
<atomRef index="517"/>
<atomRef index="607"/>
</cell>
<cell>
<cellProperties index="1268" type="POLY_VERTEX" name="Atom #516"/>
<nrOfStructures value="19"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="596"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="597"/>
<atomRef index="435"/>
<atomRef index="606"/>
<atomRef index="525"/>
<atomRef index="508"/>
<atomRef index="598"/>
<atomRef index="517"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="445"/>
</cell>
<cell>
<cellProperties index="1269" type="POLY_VERTEX" name="Atom #435"/>
<nrOfStructures value="23"/>
<atomRef index="506"/>
<atomRef index="425"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="516"/>
<atomRef index="354"/>
<atomRef index="525"/>
<atomRef index="363"/>
<atomRef index="508"/>
<atomRef index="346"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="364"/>
</cell>
<cell>
<cellProperties index="1270" type="POLY_VERTEX" name="Atom #354"/>
<nrOfStructures value="22"/>
<atomRef index="425"/>
<atomRef index="263"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="435"/>
<atomRef index="273"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
</cell>
<cell>
<cellProperties index="1271" type="POLY_VERTEX" name="Atom #273"/>
<nrOfStructures value="25"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="354"/>
<atomRef index="192"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
</cell>
<cell>
<cellProperties index="1272" type="POLY_VERTEX" name="Atom #192"/>
<nrOfStructures value="26"/>
<atomRef index="263"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="273"/>
<atomRef index="111"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="1273" type="POLY_VERTEX" name="Atom #111"/>
<nrOfStructures value="22"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="182"/>
<atomRef index="101"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="200"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="192"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="202"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="1274" type="POLY_VERTEX" name="Atom #606"/>
<nrOfStructures value="14"/>
<atomRef index="677"/>
<atomRef index="678"/>
<atomRef index="679"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="515"/>
<atomRef index="605"/>
<atomRef index="524"/>
<atomRef index="614"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="525"/>
<atomRef index="615"/>
</cell>
<cell>
<cellProperties index="1275" type="POLY_VERTEX" name="Atom #525"/>
<nrOfStructures value="21"/>
<atomRef index="515"/>
<atomRef index="434"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="606"/>
<atomRef index="444"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="517"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="616"/>
<atomRef index="535"/>
<atomRef index="454"/>
</cell>
<cell>
<cellProperties index="1276" type="POLY_VERTEX" name="Atom #444"/>
<nrOfStructures value="12"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="525"/>
<atomRef index="363"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="364"/>
</cell>
<cell>
<cellProperties index="1277" type="POLY_VERTEX" name="Atom #363"/>
<nrOfStructures value="25"/>
<atomRef index="434"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="444"/>
<atomRef index="282"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
</cell>
<cell>
<cellProperties index="1278" type="POLY_VERTEX" name="Atom #282"/>
<nrOfStructures value="26"/>
<atomRef index="353"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="363"/>
<atomRef index="201"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
</cell>
<cell>
<cellProperties index="1279" type="POLY_VERTEX" name="Atom #201"/>
<nrOfStructures value="26"/>
<atomRef index="272"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="282"/>
<atomRef index="120"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
</cell>
<cell>
<cellProperties index="1280" type="POLY_VERTEX" name="Atom #120"/>
<nrOfStructures value="23"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="191"/>
<atomRef index="110"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="201"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="211"/>
<atomRef index="130"/>
</cell>
<cell>
<cellProperties index="1281" type="POLY_VERTEX" name="Atom #615"/>
<nrOfStructures value="21"/>
<atomRef index="686"/>
<atomRef index="687"/>
<atomRef index="688"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="524"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="542"/>
<atomRef index="606"/>
<atomRef index="525"/>
<atomRef index="534"/>
<atomRef index="624"/>
<atomRef index="543"/>
<atomRef index="535"/>
<atomRef index="625"/>
<atomRef index="544"/>
</cell>
<cell>
<cellProperties index="1282" type="POLY_VERTEX" name="Atom #534"/>
<nrOfStructures value="22"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="525"/>
<atomRef index="444"/>
<atomRef index="615"/>
<atomRef index="453"/>
<atomRef index="624"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="616"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
</cell>
<cell>
<cellProperties index="1283" type="POLY_VERTEX" name="Atom #453"/>
<nrOfStructures value="26"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="525"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="534"/>
<atomRef index="372"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="382"/>
</cell>
<cell>
<cellProperties index="1284" type="POLY_VERTEX" name="Atom #372"/>
<nrOfStructures value="26"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="453"/>
<atomRef index="291"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
</cell>
<cell>
<cellProperties index="1285" type="POLY_VERTEX" name="Atom #291"/>
<nrOfStructures value="26"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="372"/>
<atomRef index="210"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
</cell>
<cell>
<cellProperties index="1286" type="POLY_VERTEX" name="Atom #210"/>
<nrOfStructures value="26"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="291"/>
<atomRef index="129"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
</cell>
<cell>
<cellProperties index="1287" type="POLY_VERTEX" name="Atom #129"/>
<nrOfStructures value="23"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="210"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="220"/>
<atomRef index="139"/>
</cell>
<cell>
<cellProperties index="1288" type="POLY_VERTEX" name="Atom #624"/>
<nrOfStructures value="20"/>
<atomRef index="695"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="614"/>
<atomRef index="533"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="543"/>
<atomRef index="552"/>
<atomRef index="553"/>
</cell>
<cell>
<cellProperties index="1289" type="POLY_VERTEX" name="Atom #543"/>
<nrOfStructures value="21"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="624"/>
<atomRef index="462"/>
<atomRef index="633"/>
<atomRef index="552"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="472"/>
</cell>
<cell>
<cellProperties index="1290" type="POLY_VERTEX" name="Atom #462"/>
<nrOfStructures value="25"/>
<atomRef index="533"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="543"/>
<atomRef index="381"/>
<atomRef index="552"/>
<atomRef index="390"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
</cell>
<cell>
<cellProperties index="1291" type="POLY_VERTEX" name="Atom #381"/>
<nrOfStructures value="25"/>
<atomRef index="452"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="462"/>
<atomRef index="300"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
</cell>
<cell>
<cellProperties index="1292" type="POLY_VERTEX" name="Atom #300"/>
<nrOfStructures value="26"/>
<atomRef index="371"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="381"/>
<atomRef index="219"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
</cell>
<cell>
<cellProperties index="1293" type="POLY_VERTEX" name="Atom #219"/>
<nrOfStructures value="26"/>
<atomRef index="290"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="300"/>
<atomRef index="138"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="1294" type="POLY_VERTEX" name="Atom #138"/>
<nrOfStructures value="26"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="209"/>
<atomRef index="128"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="219"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="229"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="1295" type="POLY_VERTEX" name="Atom #633"/>
<nrOfStructures value="16"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="704"/>
<atomRef index="705"/>
<atomRef index="713"/>
<atomRef index="714"/>
<atomRef index="722"/>
<atomRef index="723"/>
<atomRef index="623"/>
<atomRef index="542"/>
<atomRef index="632"/>
<atomRef index="551"/>
<atomRef index="543"/>
<atomRef index="552"/>
</cell>
<cell>
<cellProperties index="1296" type="POLY_VERTEX" name="Atom #552"/>
<nrOfStructures value="24"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="641"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="542"/>
<atomRef index="461"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="624"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="633"/>
<atomRef index="471"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="472"/>
</cell>
<cell>
<cellProperties index="1297" type="POLY_VERTEX" name="Atom #471"/>
<nrOfStructures value="17"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="560"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="551"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="552"/>
<atomRef index="390"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
</cell>
<cell>
<cellProperties index="1298" type="POLY_VERTEX" name="Atom #390"/>
<nrOfStructures value="26"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="479"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="461"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="470"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="471"/>
<atomRef index="309"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
</cell>
<cell>
<cellProperties index="1299" type="POLY_VERTEX" name="Atom #309"/>
<nrOfStructures value="26"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="398"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="380"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="389"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="390"/>
<atomRef index="228"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
</cell>
<cell>
<cellProperties index="1300" type="POLY_VERTEX" name="Atom #228"/>
<nrOfStructures value="23"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="299"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="308"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="309"/>
<atomRef index="147"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="1301" type="POLY_VERTEX" name="Atom #147"/>
<nrOfStructures value="26"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="218"/>
<atomRef index="137"/>
<atomRef index="227"/>
<atomRef index="146"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="228"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="229"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="1302" type="POLY_VERTEX" name="Atom #580"/>
<nrOfStructures value="14"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="652"/>
<atomRef index="653"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="579"/>
<atomRef index="499"/>
<atomRef index="589"/>
<atomRef index="581"/>
<atomRef index="500"/>
<atomRef index="590"/>
</cell>
<cell>
<cellProperties index="1303" type="POLY_VERTEX" name="Atom #499"/>
<nrOfStructures value="22"/>
<atomRef index="408"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="489"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="570"/>
<atomRef index="571"/>
<atomRef index="572"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="588"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="580"/>
<atomRef index="418"/>
<atomRef index="589"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="500"/>
<atomRef index="419"/>
</cell>
<cell>
<cellProperties index="1304" type="POLY_VERTEX" name="Atom #418"/>
<nrOfStructures value="22"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="490"/>
<atomRef index="491"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="499"/>
<atomRef index="337"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
</cell>
<cell>
<cellProperties index="1305" type="POLY_VERTEX" name="Atom #337"/>
<nrOfStructures value="22"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="409"/>
<atomRef index="410"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="418"/>
<atomRef index="256"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="347"/>
<atomRef index="266"/>
</cell>
<cell>
<cellProperties index="1306" type="POLY_VERTEX" name="Atom #256"/>
<nrOfStructures value="23"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="337"/>
<atomRef index="175"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
</cell>
<cell>
<cellProperties index="1307" type="POLY_VERTEX" name="Atom #175"/>
<nrOfStructures value="23"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="256"/>
<atomRef index="94"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="1308" type="POLY_VERTEX" name="Atom #94"/>
<nrOfStructures value="21"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="175"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="185"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="1309" type="POLY_VERTEX" name="Atom #589"/>
<nrOfStructures value="15"/>
<atomRef index="661"/>
<atomRef index="662"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="579"/>
<atomRef index="498"/>
<atomRef index="588"/>
<atomRef index="580"/>
<atomRef index="499"/>
<atomRef index="508"/>
<atomRef index="598"/>
<atomRef index="581"/>
<atomRef index="500"/>
<atomRef index="590"/>
<atomRef index="509"/>
</cell>
<cell>
<cellProperties index="1310" type="POLY_VERTEX" name="Atom #508"/>
<nrOfStructures value="19"/>
<atomRef index="498"/>
<atomRef index="417"/>
<atomRef index="588"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="589"/>
<atomRef index="427"/>
<atomRef index="598"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="518"/>
<atomRef index="437"/>
</cell>
<cell>
<cellProperties index="1311" type="POLY_VERTEX" name="Atom #427"/>
<nrOfStructures value="18"/>
<atomRef index="417"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="508"/>
<atomRef index="346"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="356"/>
</cell>
<cell>
<cellProperties index="1312" type="POLY_VERTEX" name="Atom #346"/>
<nrOfStructures value="24"/>
<atomRef index="417"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="427"/>
<atomRef index="265"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="266"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="275"/>
</cell>
<cell>
<cellProperties index="1313" type="POLY_VERTEX" name="Atom #265"/>
<nrOfStructures value="26"/>
<atomRef index="336"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="346"/>
<atomRef index="184"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
</cell>
<cell>
<cellProperties index="1314" type="POLY_VERTEX" name="Atom #184"/>
<nrOfStructures value="26"/>
<atomRef index="255"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="265"/>
<atomRef index="103"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="1315" type="POLY_VERTEX" name="Atom #103"/>
<nrOfStructures value="26"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="174"/>
<atomRef index="93"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="184"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="194"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="1316" type="POLY_VERTEX" name="Atom #598"/>
<nrOfStructures value="14"/>
<atomRef index="670"/>
<atomRef index="671"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="588"/>
<atomRef index="507"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="589"/>
<atomRef index="508"/>
<atomRef index="517"/>
<atomRef index="607"/>
<atomRef index="509"/>
<atomRef index="518"/>
</cell>
<cell>
<cellProperties index="1317" type="POLY_VERTEX" name="Atom #517"/>
<nrOfStructures value="19"/>
<atomRef index="507"/>
<atomRef index="426"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="525"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="598"/>
<atomRef index="436"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="509"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="446"/>
</cell>
<cell>
<cellProperties index="1318" type="POLY_VERTEX" name="Atom #436"/>
<nrOfStructures value="20"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="435"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="517"/>
<atomRef index="355"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
</cell>
<cell>
<cellProperties index="1319" type="POLY_VERTEX" name="Atom #355"/>
<nrOfStructures value="20"/>
<atomRef index="426"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="436"/>
<atomRef index="274"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="365"/>
<atomRef index="284"/>
</cell>
<cell>
<cellProperties index="1320" type="POLY_VERTEX" name="Atom #274"/>
<nrOfStructures value="26"/>
<atomRef index="345"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="355"/>
<atomRef index="193"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
</cell>
<cell>
<cellProperties index="1321" type="POLY_VERTEX" name="Atom #193"/>
<nrOfStructures value="26"/>
<atomRef index="264"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="274"/>
<atomRef index="112"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
</cell>
<cell>
<cellProperties index="1322" type="POLY_VERTEX" name="Atom #112"/>
<nrOfStructures value="23"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="183"/>
<atomRef index="102"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="193"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="203"/>
<atomRef index="122"/>
</cell>
<cell>
<cellProperties index="1323" type="POLY_VERTEX" name="Atom #607"/>
<nrOfStructures value="15"/>
<atomRef index="679"/>
<atomRef index="680"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="597"/>
<atomRef index="516"/>
<atomRef index="525"/>
<atomRef index="598"/>
<atomRef index="517"/>
<atomRef index="526"/>
<atomRef index="616"/>
<atomRef index="518"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="617"/>
</cell>
<cell>
<cellProperties index="1324" type="POLY_VERTEX" name="Atom #526"/>
<nrOfStructures value="20"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="525"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="607"/>
<atomRef index="445"/>
<atomRef index="616"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="617"/>
<atomRef index="536"/>
<atomRef index="455"/>
</cell>
<cell>
<cellProperties index="1325" type="POLY_VERTEX" name="Atom #445"/>
<nrOfStructures value="25"/>
<atomRef index="516"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="525"/>
<atomRef index="363"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="526"/>
<atomRef index="364"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="374"/>
</cell>
<cell>
<cellProperties index="1326" type="POLY_VERTEX" name="Atom #364"/>
<nrOfStructures value="23"/>
<atomRef index="435"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="445"/>
<atomRef index="283"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="374"/>
<atomRef index="293"/>
</cell>
<cell>
<cellProperties index="1327" type="POLY_VERTEX" name="Atom #283"/>
<nrOfStructures value="26"/>
<atomRef index="354"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="364"/>
<atomRef index="202"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
</cell>
<cell>
<cellProperties index="1328" type="POLY_VERTEX" name="Atom #202"/>
<nrOfStructures value="26"/>
<atomRef index="273"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="283"/>
<atomRef index="121"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="1329" type="POLY_VERTEX" name="Atom #121"/>
<nrOfStructures value="23"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="192"/>
<atomRef index="111"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="202"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="212"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="1330" type="POLY_VERTEX" name="Atom #616"/>
<nrOfStructures value="13"/>
<atomRef index="688"/>
<atomRef index="689"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="525"/>
<atomRef index="534"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="535"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="617"/>
<atomRef index="536"/>
</cell>
<cell>
<cellProperties index="1331" type="POLY_VERTEX" name="Atom #535"/>
<nrOfStructures value="21"/>
<atomRef index="525"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="616"/>
<atomRef index="454"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="617"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
</cell>
<cell>
<cellProperties index="1332" type="POLY_VERTEX" name="Atom #454"/>
<nrOfStructures value="25"/>
<atomRef index="525"/>
<atomRef index="363"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="535"/>
<atomRef index="373"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="383"/>
</cell>
<cell>
<cellProperties index="1333" type="POLY_VERTEX" name="Atom #373"/>
<nrOfStructures value="25"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="454"/>
<atomRef index="292"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
</cell>
<cell>
<cellProperties index="1334" type="POLY_VERTEX" name="Atom #292"/>
<nrOfStructures value="26"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="373"/>
<atomRef index="211"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
</cell>
<cell>
<cellProperties index="1335" type="POLY_VERTEX" name="Atom #211"/>
<nrOfStructures value="26"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="292"/>
<atomRef index="130"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="1336" type="POLY_VERTEX" name="Atom #130"/>
<nrOfStructures value="23"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="211"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="221"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="1337" type="POLY_VERTEX" name="Atom #625"/>
<nrOfStructures value="22"/>
<atomRef index="696"/>
<atomRef index="697"/>
<atomRef index="698"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="543"/>
<atomRef index="552"/>
<atomRef index="535"/>
<atomRef index="544"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="536"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="635"/>
<atomRef index="554"/>
</cell>
<cell>
<cellProperties index="1338" type="POLY_VERTEX" name="Atom #544"/>
<nrOfStructures value="21"/>
<atomRef index="615"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="552"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="625"/>
<atomRef index="463"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
</cell>
<cell>
<cellProperties index="1339" type="POLY_VERTEX" name="Atom #463"/>
<nrOfStructures value="25"/>
<atomRef index="534"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="552"/>
<atomRef index="390"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="544"/>
<atomRef index="382"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="392"/>
</cell>
<cell>
<cellProperties index="1340" type="POLY_VERTEX" name="Atom #382"/>
<nrOfStructures value="25"/>
<atomRef index="453"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="463"/>
<atomRef index="301"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
</cell>
<cell>
<cellProperties index="1341" type="POLY_VERTEX" name="Atom #301"/>
<nrOfStructures value="26"/>
<atomRef index="372"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="382"/>
<atomRef index="220"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="1342" type="POLY_VERTEX" name="Atom #220"/>
<nrOfStructures value="26"/>
<atomRef index="291"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="301"/>
<atomRef index="139"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="149"/>
</cell>
<cell>
<cellProperties index="1343" type="POLY_VERTEX" name="Atom #139"/>
<nrOfStructures value="26"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="210"/>
<atomRef index="129"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="220"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="230"/>
<atomRef index="149"/>
</cell>
<cell>
<cellProperties index="1344" type="POLY_VERTEX" name="Atom #634"/>
<nrOfStructures value="22"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="705"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="714"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="723"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="543"/>
<atomRef index="552"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="553"/>
<atomRef index="635"/>
<atomRef index="554"/>
</cell>
<cell>
<cellProperties index="1345" type="POLY_VERTEX" name="Atom #553"/>
<nrOfStructures value="25"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="642"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="624"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="634"/>
<atomRef index="472"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
</cell>
<cell>
<cellProperties index="1346" type="POLY_VERTEX" name="Atom #472"/>
<nrOfStructures value="26"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="561"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="543"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="552"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="553"/>
<atomRef index="391"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="392"/>
</cell>
<cell>
<cellProperties index="1347" type="POLY_VERTEX" name="Atom #391"/>
<nrOfStructures value="26"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="480"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="462"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="471"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="472"/>
<atomRef index="310"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
</cell>
<cell>
<cellProperties index="1348" type="POLY_VERTEX" name="Atom #310"/>
<nrOfStructures value="26"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="399"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="381"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="390"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="391"/>
<atomRef index="229"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="1349" type="POLY_VERTEX" name="Atom #229"/>
<nrOfStructures value="26"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="300"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="309"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="310"/>
<atomRef index="148"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="149"/>
</cell>
<cell>
<cellProperties index="1350" type="POLY_VERTEX" name="Atom #148"/>
<nrOfStructures value="26"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="219"/>
<atomRef index="138"/>
<atomRef index="228"/>
<atomRef index="147"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="229"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="230"/>
<atomRef index="149"/>
</cell>
<cell>
<cellProperties index="1351" type="POLY_VERTEX" name="Atom #581"/>
<nrOfStructures value="14"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="653"/>
<atomRef index="654"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="580"/>
<atomRef index="589"/>
<atomRef index="500"/>
<atomRef index="590"/>
<atomRef index="582"/>
<atomRef index="501"/>
</cell>
<cell>
<cellProperties index="1352" type="POLY_VERTEX" name="Atom #500"/>
<nrOfStructures value="21"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="572"/>
<atomRef index="573"/>
<atomRef index="580"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="589"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="581"/>
<atomRef index="419"/>
<atomRef index="590"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="510"/>
<atomRef index="429"/>
</cell>
<cell>
<cellProperties index="1353" type="POLY_VERTEX" name="Atom #419"/>
<nrOfStructures value="22"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="491"/>
<atomRef index="492"/>
<atomRef index="499"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="500"/>
<atomRef index="338"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="510"/>
<atomRef index="348"/>
</cell>
<cell>
<cellProperties index="1354" type="POLY_VERTEX" name="Atom #338"/>
<nrOfStructures value="22"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="410"/>
<atomRef index="411"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="419"/>
<atomRef index="257"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="348"/>
<atomRef index="267"/>
</cell>
<cell>
<cellProperties index="1355" type="POLY_VERTEX" name="Atom #257"/>
<nrOfStructures value="23"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="338"/>
<atomRef index="176"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
</cell>
<cell>
<cellProperties index="1356" type="POLY_VERTEX" name="Atom #176"/>
<nrOfStructures value="23"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="257"/>
<atomRef index="95"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
</cell>
<cell>
<cellProperties index="1357" type="POLY_VERTEX" name="Atom #95"/>
<nrOfStructures value="21"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="176"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="186"/>
<atomRef index="105"/>
</cell>
<cell>
<cellProperties index="1358" type="POLY_VERTEX" name="Atom #590"/>
<nrOfStructures value="15"/>
<atomRef index="662"/>
<atomRef index="663"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="580"/>
<atomRef index="589"/>
<atomRef index="581"/>
<atomRef index="500"/>
<atomRef index="509"/>
<atomRef index="599"/>
<atomRef index="582"/>
<atomRef index="501"/>
<atomRef index="591"/>
<atomRef index="510"/>
<atomRef index="600"/>
</cell>
<cell>
<cellProperties index="1359" type="POLY_VERTEX" name="Atom #509"/>
<nrOfStructures value="19"/>
<atomRef index="418"/>
<atomRef index="589"/>
<atomRef index="427"/>
<atomRef index="598"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="590"/>
<atomRef index="428"/>
<atomRef index="599"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="519"/>
<atomRef index="438"/>
</cell>
<cell>
<cellProperties index="1360" type="POLY_VERTEX" name="Atom #428"/>
<nrOfStructures value="20"/>
<atomRef index="418"/>
<atomRef index="427"/>
<atomRef index="436"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="509"/>
<atomRef index="347"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="357"/>
</cell>
<cell>
<cellProperties index="1361" type="POLY_VERTEX" name="Atom #347"/>
<nrOfStructures value="24"/>
<atomRef index="418"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="427"/>
<atomRef index="265"/>
<atomRef index="436"/>
<atomRef index="274"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="428"/>
<atomRef index="266"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
</cell>
<cell>
<cellProperties index="1362" type="POLY_VERTEX" name="Atom #266"/>
<nrOfStructures value="25"/>
<atomRef index="337"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="347"/>
<atomRef index="185"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="195"/>
</cell>
<cell>
<cellProperties index="1363" type="POLY_VERTEX" name="Atom #185"/>
<nrOfStructures value="26"/>
<atomRef index="256"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="266"/>
<atomRef index="104"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="1364" type="POLY_VERTEX" name="Atom #104"/>
<nrOfStructures value="23"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="175"/>
<atomRef index="94"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="185"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="195"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="1365" type="POLY_VERTEX" name="Atom #599"/>
<nrOfStructures value="12"/>
<atomRef index="671"/>
<atomRef index="672"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="590"/>
<atomRef index="509"/>
<atomRef index="518"/>
<atomRef index="608"/>
<atomRef index="510"/>
<atomRef index="600"/>
<atomRef index="519"/>
<atomRef index="609"/>
</cell>
<cell>
<cellProperties index="1366" type="POLY_VERTEX" name="Atom #518"/>
<nrOfStructures value="20"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="598"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="599"/>
<atomRef index="437"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="510"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="528"/>
<atomRef index="447"/>
</cell>
<cell>
<cellProperties index="1367" type="POLY_VERTEX" name="Atom #437"/>
<nrOfStructures value="24"/>
<atomRef index="508"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="518"/>
<atomRef index="356"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
</cell>
<cell>
<cellProperties index="1368" type="POLY_VERTEX" name="Atom #356"/>
<nrOfStructures value="25"/>
<atomRef index="427"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="437"/>
<atomRef index="275"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
</cell>
<cell>
<cellProperties index="1369" type="POLY_VERTEX" name="Atom #275"/>
<nrOfStructures value="26"/>
<atomRef index="346"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="356"/>
<atomRef index="194"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
</cell>
<cell>
<cellProperties index="1370" type="POLY_VERTEX" name="Atom #194"/>
<nrOfStructures value="26"/>
<atomRef index="265"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="275"/>
<atomRef index="113"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
</cell>
<cell>
<cellProperties index="1371" type="POLY_VERTEX" name="Atom #113"/>
<nrOfStructures value="23"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="184"/>
<atomRef index="103"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="194"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="204"/>
<atomRef index="123"/>
</cell>
<cell>
<cellProperties index="1372" type="POLY_VERTEX" name="Atom #608"/>
<nrOfStructures value="16"/>
<atomRef index="680"/>
<atomRef index="681"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="517"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="616"/>
<atomRef index="599"/>
<atomRef index="518"/>
<atomRef index="527"/>
<atomRef index="617"/>
<atomRef index="519"/>
<atomRef index="609"/>
<atomRef index="528"/>
<atomRef index="618"/>
</cell>
<cell>
<cellProperties index="1373" type="POLY_VERTEX" name="Atom #527"/>
<nrOfStructures value="20"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="616"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="608"/>
<atomRef index="446"/>
<atomRef index="617"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="537"/>
</cell>
<cell>
<cellProperties index="1374" type="POLY_VERTEX" name="Atom #446"/>
<nrOfStructures value="24"/>
<atomRef index="517"/>
<atomRef index="436"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="527"/>
<atomRef index="365"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="375"/>
</cell>
<cell>
<cellProperties index="1375" type="POLY_VERTEX" name="Atom #365"/>
<nrOfStructures value="26"/>
<atomRef index="436"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="446"/>
<atomRef index="284"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="294"/>
</cell>
<cell>
<cellProperties index="1376" type="POLY_VERTEX" name="Atom #284"/>
<nrOfStructures value="26"/>
<atomRef index="355"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="365"/>
<atomRef index="203"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
</cell>
<cell>
<cellProperties index="1377" type="POLY_VERTEX" name="Atom #203"/>
<nrOfStructures value="26"/>
<atomRef index="274"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="284"/>
<atomRef index="122"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="132"/>
</cell>
<cell>
<cellProperties index="1378" type="POLY_VERTEX" name="Atom #122"/>
<nrOfStructures value="23"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="193"/>
<atomRef index="112"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="203"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="213"/>
<atomRef index="132"/>
</cell>
<cell>
<cellProperties index="1379" type="POLY_VERTEX" name="Atom #617"/>
<nrOfStructures value="15"/>
<atomRef index="689"/>
<atomRef index="690"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="616"/>
<atomRef index="535"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="536"/>
<atomRef index="609"/>
<atomRef index="528"/>
<atomRef index="618"/>
<atomRef index="537"/>
</cell>
<cell>
<cellProperties index="1380" type="POLY_VERTEX" name="Atom #536"/>
<nrOfStructures value="20"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="616"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="617"/>
<atomRef index="455"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="537"/>
<atomRef index="546"/>
<atomRef index="465"/>
</cell>
<cell>
<cellProperties index="1381" type="POLY_VERTEX" name="Atom #455"/>
<nrOfStructures value="25"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="536"/>
<atomRef index="374"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="384"/>
</cell>
<cell>
<cellProperties index="1382" type="POLY_VERTEX" name="Atom #374"/>
<nrOfStructures value="25"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="455"/>
<atomRef index="293"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="303"/>
</cell>
<cell>
<cellProperties index="1383" type="POLY_VERTEX" name="Atom #293"/>
<nrOfStructures value="26"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="374"/>
<atomRef index="212"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="222"/>
</cell>
<cell>
<cellProperties index="1384" type="POLY_VERTEX" name="Atom #212"/>
<nrOfStructures value="26"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="293"/>
<atomRef index="131"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
</cell>
<cell>
<cellProperties index="1385" type="POLY_VERTEX" name="Atom #131"/>
<nrOfStructures value="23"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="212"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="222"/>
<atomRef index="141"/>
</cell>
<cell>
<cellProperties index="1386" type="POLY_VERTEX" name="Atom #626"/>
<nrOfStructures value="20"/>
<atomRef index="698"/>
<atomRef index="699"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="535"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="553"/>
<atomRef index="536"/>
<atomRef index="545"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="636"/>
<atomRef index="555"/>
</cell>
<cell>
<cellProperties index="1387" type="POLY_VERTEX" name="Atom #545"/>
<nrOfStructures value="22"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="626"/>
<atomRef index="464"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="474"/>
</cell>
<cell>
<cellProperties index="1388" type="POLY_VERTEX" name="Atom #464"/>
<nrOfStructures value="25"/>
<atomRef index="535"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="545"/>
<atomRef index="383"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="537"/>
<atomRef index="375"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="393"/>
</cell>
<cell>
<cellProperties index="1389" type="POLY_VERTEX" name="Atom #383"/>
<nrOfStructures value="23"/>
<atomRef index="454"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="464"/>
<atomRef index="302"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="393"/>
<atomRef index="312"/>
</cell>
<cell>
<cellProperties index="1390" type="POLY_VERTEX" name="Atom #302"/>
<nrOfStructures value="26"/>
<atomRef index="373"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="383"/>
<atomRef index="221"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="1391" type="POLY_VERTEX" name="Atom #221"/>
<nrOfStructures value="25"/>
<atomRef index="292"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="302"/>
<atomRef index="140"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="1392" type="POLY_VERTEX" name="Atom #140"/>
<nrOfStructures value="26"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="211"/>
<atomRef index="130"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="221"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="1393" type="POLY_VERTEX" name="Atom #635"/>
<nrOfStructures value="24"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="706"/>
<atomRef index="707"/>
<atomRef index="708"/>
<atomRef index="715"/>
<atomRef index="716"/>
<atomRef index="717"/>
<atomRef index="724"/>
<atomRef index="725"/>
<atomRef index="726"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="554"/>
<atomRef index="546"/>
<atomRef index="555"/>
</cell>
<cell>
<cellProperties index="1394" type="POLY_VERTEX" name="Atom #554"/>
<nrOfStructures value="25"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="643"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="625"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="634"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="635"/>
<atomRef index="473"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="474"/>
</cell>
<cell>
<cellProperties index="1395" type="POLY_VERTEX" name="Atom #473"/>
<nrOfStructures value="26"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="562"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="544"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="553"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="554"/>
<atomRef index="392"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="393"/>
</cell>
<cell>
<cellProperties index="1396" type="POLY_VERTEX" name="Atom #392"/>
<nrOfStructures value="24"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="481"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="463"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="472"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="473"/>
<atomRef index="311"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="393"/>
<atomRef index="312"/>
</cell>
<cell>
<cellProperties index="1397" type="POLY_VERTEX" name="Atom #311"/>
<nrOfStructures value="26"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="400"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="382"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="391"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="392"/>
<atomRef index="230"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="1398" type="POLY_VERTEX" name="Atom #230"/>
<nrOfStructures value="26"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="301"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="310"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="311"/>
<atomRef index="149"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="1399" type="POLY_VERTEX" name="Atom #149"/>
<nrOfStructures value="21"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="220"/>
<atomRef index="139"/>
<atomRef index="229"/>
<atomRef index="148"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="230"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="231"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="1400" type="POLY_VERTEX" name="Atom #582"/>
<nrOfStructures value="15"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="654"/>
<atomRef index="655"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="581"/>
<atomRef index="590"/>
<atomRef index="501"/>
<atomRef index="591"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="592"/>
</cell>
<cell>
<cellProperties index="1401" type="POLY_VERTEX" name="Atom #501"/>
<nrOfStructures value="21"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="573"/>
<atomRef index="574"/>
<atomRef index="581"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="590"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="582"/>
<atomRef index="420"/>
<atomRef index="591"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="511"/>
</cell>
<cell>
<cellProperties index="1402" type="POLY_VERTEX" name="Atom #420"/>
<nrOfStructures value="23"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="492"/>
<atomRef index="493"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="501"/>
<atomRef index="339"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
</cell>
<cell>
<cellProperties index="1403" type="POLY_VERTEX" name="Atom #339"/>
<nrOfStructures value="23"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="411"/>
<atomRef index="412"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="420"/>
<atomRef index="258"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="1404" type="POLY_VERTEX" name="Atom #258"/>
<nrOfStructures value="23"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="339"/>
<atomRef index="177"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
</cell>
<cell>
<cellProperties index="1405" type="POLY_VERTEX" name="Atom #177"/>
<nrOfStructures value="23"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="258"/>
<atomRef index="96"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="1406" type="POLY_VERTEX" name="Atom #96"/>
<nrOfStructures value="21"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="177"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="187"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="1407" type="POLY_VERTEX" name="Atom #591"/>
<nrOfStructures value="12"/>
<atomRef index="663"/>
<atomRef index="664"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="590"/>
<atomRef index="582"/>
<atomRef index="501"/>
<atomRef index="510"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="592"/>
<atomRef index="511"/>
</cell>
<cell>
<cellProperties index="1408" type="POLY_VERTEX" name="Atom #510"/>
<nrOfStructures value="17"/>
<atomRef index="500"/>
<atomRef index="419"/>
<atomRef index="590"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="599"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="591"/>
<atomRef index="429"/>
<atomRef index="600"/>
<atomRef index="519"/>
<atomRef index="502"/>
<atomRef index="511"/>
<atomRef index="520"/>
</cell>
<cell>
<cellProperties index="1409" type="POLY_VERTEX" name="Atom #429"/>
<nrOfStructures value="21"/>
<atomRef index="500"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="437"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="510"/>
<atomRef index="348"/>
<atomRef index="519"/>
<atomRef index="357"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
</cell>
<cell>
<cellProperties index="1410" type="POLY_VERTEX" name="Atom #348"/>
<nrOfStructures value="25"/>
<atomRef index="419"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="429"/>
<atomRef index="267"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="277"/>
</cell>
<cell>
<cellProperties index="1411" type="POLY_VERTEX" name="Atom #267"/>
<nrOfStructures value="26"/>
<atomRef index="338"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="348"/>
<atomRef index="186"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
</cell>
<cell>
<cellProperties index="1412" type="POLY_VERTEX" name="Atom #186"/>
<nrOfStructures value="26"/>
<atomRef index="257"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="267"/>
<atomRef index="105"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="1413" type="POLY_VERTEX" name="Atom #105"/>
<nrOfStructures value="23"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="176"/>
<atomRef index="95"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="186"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="196"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="1414" type="POLY_VERTEX" name="Atom #600"/>
<nrOfStructures value="13"/>
<atomRef index="672"/>
<atomRef index="673"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="590"/>
<atomRef index="599"/>
<atomRef index="510"/>
<atomRef index="519"/>
<atomRef index="609"/>
<atomRef index="592"/>
<atomRef index="511"/>
<atomRef index="601"/>
<atomRef index="520"/>
</cell>
<cell>
<cellProperties index="1415" type="POLY_VERTEX" name="Atom #519"/>
<nrOfStructures value="21"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="599"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="600"/>
<atomRef index="438"/>
<atomRef index="609"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="511"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="529"/>
<atomRef index="448"/>
</cell>
<cell>
<cellProperties index="1416" type="POLY_VERTEX" name="Atom #438"/>
<nrOfStructures value="24"/>
<atomRef index="509"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="348"/>
<atomRef index="519"/>
<atomRef index="357"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
</cell>
<cell>
<cellProperties index="1417" type="POLY_VERTEX" name="Atom #357"/>
<nrOfStructures value="22"/>
<atomRef index="428"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="438"/>
<atomRef index="276"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="349"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="367"/>
<atomRef index="286"/>
</cell>
<cell>
<cellProperties index="1418" type="POLY_VERTEX" name="Atom #276"/>
<nrOfStructures value="26"/>
<atomRef index="347"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="357"/>
<atomRef index="195"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
</cell>
<cell>
<cellProperties index="1419" type="POLY_VERTEX" name="Atom #195"/>
<nrOfStructures value="26"/>
<atomRef index="266"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="276"/>
<atomRef index="114"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="1420" type="POLY_VERTEX" name="Atom #114"/>
<nrOfStructures value="23"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="185"/>
<atomRef index="104"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="195"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="205"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="1421" type="POLY_VERTEX" name="Atom #609"/>
<nrOfStructures value="16"/>
<atomRef index="681"/>
<atomRef index="682"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="599"/>
<atomRef index="608"/>
<atomRef index="617"/>
<atomRef index="600"/>
<atomRef index="519"/>
<atomRef index="528"/>
<atomRef index="618"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="619"/>
</cell>
<cell>
<cellProperties index="1422" type="POLY_VERTEX" name="Atom #528"/>
<nrOfStructures value="20"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="617"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="609"/>
<atomRef index="447"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="538"/>
</cell>
<cell>
<cellProperties index="1423" type="POLY_VERTEX" name="Atom #447"/>
<nrOfStructures value="25"/>
<atomRef index="518"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="528"/>
<atomRef index="366"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="538"/>
<atomRef index="376"/>
</cell>
<cell>
<cellProperties index="1424" type="POLY_VERTEX" name="Atom #366"/>
<nrOfStructures value="25"/>
<atomRef index="437"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="447"/>
<atomRef index="285"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="376"/>
<atomRef index="295"/>
</cell>
<cell>
<cellProperties index="1425" type="POLY_VERTEX" name="Atom #285"/>
<nrOfStructures value="26"/>
<atomRef index="356"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="366"/>
<atomRef index="204"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="1426" type="POLY_VERTEX" name="Atom #204"/>
<nrOfStructures value="26"/>
<atomRef index="275"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="285"/>
<atomRef index="123"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="1427" type="POLY_VERTEX" name="Atom #123"/>
<nrOfStructures value="23"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="194"/>
<atomRef index="113"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="204"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="214"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="1428" type="POLY_VERTEX" name="Atom #618"/>
<nrOfStructures value="20"/>
<atomRef index="690"/>
<atomRef index="691"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="608"/>
<atomRef index="617"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="609"/>
<atomRef index="528"/>
<atomRef index="537"/>
<atomRef index="627"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="628"/>
<atomRef index="547"/>
</cell>
<cell>
<cellProperties index="1429" type="POLY_VERTEX" name="Atom #537"/>
<nrOfStructures value="22"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="617"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="618"/>
<atomRef index="456"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="466"/>
</cell>
<cell>
<cellProperties index="1430" type="POLY_VERTEX" name="Atom #456"/>
<nrOfStructures value="20"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="455"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="537"/>
<atomRef index="375"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="385"/>
</cell>
<cell>
<cellProperties index="1431" type="POLY_VERTEX" name="Atom #375"/>
<nrOfStructures value="24"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="456"/>
<atomRef index="294"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="304"/>
</cell>
<cell>
<cellProperties index="1432" type="POLY_VERTEX" name="Atom #294"/>
<nrOfStructures value="26"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="375"/>
<atomRef index="213"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
</cell>
<cell>
<cellProperties index="1433" type="POLY_VERTEX" name="Atom #213"/>
<nrOfStructures value="26"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="294"/>
<atomRef index="132"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="1434" type="POLY_VERTEX" name="Atom #132"/>
<nrOfStructures value="22"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="140"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="213"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="223"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="1435" type="POLY_VERTEX" name="Atom #627"/>
<nrOfStructures value="18"/>
<atomRef index="699"/>
<atomRef index="700"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="546"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1436" type="POLY_VERTEX" name="Atom #546"/>
<nrOfStructures value="21"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="627"/>
<atomRef index="465"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="538"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="556"/>
<atomRef index="475"/>
</cell>
<cell>
<cellProperties index="1437" type="POLY_VERTEX" name="Atom #465"/>
<nrOfStructures value="23"/>
<atomRef index="536"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="546"/>
<atomRef index="384"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="556"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1438" type="POLY_VERTEX" name="Atom #384"/>
<nrOfStructures value="22"/>
<atomRef index="455"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="465"/>
<atomRef index="303"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="376"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="394"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="1439" type="POLY_VERTEX" name="Atom #303"/>
<nrOfStructures value="26"/>
<atomRef index="374"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="384"/>
<atomRef index="222"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="394"/>
<atomRef index="313"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="1440" type="POLY_VERTEX" name="Atom #222"/>
<nrOfStructures value="26"/>
<atomRef index="293"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="303"/>
<atomRef index="141"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="313"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="1441" type="POLY_VERTEX" name="Atom #141"/>
<nrOfStructures value="23"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="212"/>
<atomRef index="131"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="222"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="1442" type="POLY_VERTEX" name="Atom #636"/>
<nrOfStructures value="20"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="708"/>
<atomRef index="709"/>
<atomRef index="717"/>
<atomRef index="718"/>
<atomRef index="726"/>
<atomRef index="727"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="554"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="555"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="637"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1443" type="POLY_VERTEX" name="Atom #555"/>
<nrOfStructures value="23"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="563"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="644"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="626"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="635"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="636"/>
<atomRef index="474"/>
<atomRef index="547"/>
<atomRef index="556"/>
<atomRef index="475"/>
</cell>
<cell>
<cellProperties index="1444" type="POLY_VERTEX" name="Atom #474"/>
<nrOfStructures value="21"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="545"/>
<atomRef index="464"/>
<atomRef index="554"/>
<atomRef index="473"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="555"/>
<atomRef index="393"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="556"/>
<atomRef index="475"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1445" type="POLY_VERTEX" name="Atom #393"/>
<nrOfStructures value="24"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="401"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="482"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="464"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="473"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="474"/>
<atomRef index="312"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="394"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="1446" type="POLY_VERTEX" name="Atom #312"/>
<nrOfStructures value="23"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="383"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="392"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="393"/>
<atomRef index="231"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="394"/>
<atomRef index="313"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="1447" type="POLY_VERTEX" name="Atom #231"/>
<nrOfStructures value="26"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="302"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="311"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="312"/>
<atomRef index="150"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="313"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="1448" type="POLY_VERTEX" name="Atom #150"/>
<nrOfStructures value="26"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="221"/>
<atomRef index="140"/>
<atomRef index="230"/>
<atomRef index="149"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="231"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="1449" type="POLY_VERTEX" name="Atom #583"/>
<nrOfStructures value="15"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="655"/>
<atomRef index="656"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="582"/>
<atomRef index="501"/>
<atomRef index="591"/>
<atomRef index="502"/>
<atomRef index="592"/>
</cell>
<cell>
<cellProperties index="1450" type="POLY_VERTEX" name="Atom #502"/>
<nrOfStructures value="23"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="574"/>
<atomRef index="575"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="582"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="591"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="583"/>
<atomRef index="421"/>
<atomRef index="592"/>
<atomRef index="511"/>
<atomRef index="430"/>
</cell>
<cell>
<cellProperties index="1451" type="POLY_VERTEX" name="Atom #421"/>
<nrOfStructures value="22"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="493"/>
<atomRef index="494"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="502"/>
<atomRef index="340"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
</cell>
<cell>
<cellProperties index="1452" type="POLY_VERTEX" name="Atom #340"/>
<nrOfStructures value="23"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="412"/>
<atomRef index="413"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="421"/>
<atomRef index="259"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="1453" type="POLY_VERTEX" name="Atom #259"/>
<nrOfStructures value="23"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="340"/>
<atomRef index="178"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
</cell>
<cell>
<cellProperties index="1454" type="POLY_VERTEX" name="Atom #178"/>
<nrOfStructures value="23"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="259"/>
<atomRef index="97"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="1455" type="POLY_VERTEX" name="Atom #97"/>
<nrOfStructures value="21"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="178"/>
<atomRef index="187"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="1456" type="POLY_VERTEX" name="Atom #592"/>
<nrOfStructures value="14"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="664"/>
<atomRef index="665"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="582"/>
<atomRef index="591"/>
<atomRef index="600"/>
<atomRef index="583"/>
<atomRef index="502"/>
<atomRef index="511"/>
</cell>
<cell>
<cellProperties index="1457" type="POLY_VERTEX" name="Atom #511"/>
<nrOfStructures value="24"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="584"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="501"/>
<atomRef index="420"/>
<atomRef index="591"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="600"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="592"/>
<atomRef index="430"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="439"/>
</cell>
<cell>
<cellProperties index="1458" type="POLY_VERTEX" name="Atom #430"/>
<nrOfStructures value="21"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="503"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="429"/>
<atomRef index="438"/>
<atomRef index="502"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="511"/>
<atomRef index="349"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
</cell>
<cell>
<cellProperties index="1459" type="POLY_VERTEX" name="Atom #349"/>
<nrOfStructures value="26"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="422"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="420"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="421"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="430"/>
<atomRef index="268"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="277"/>
</cell>
<cell>
<cellProperties index="1460" type="POLY_VERTEX" name="Atom #268"/>
<nrOfStructures value="25"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="341"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="339"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="340"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="349"/>
<atomRef index="187"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
</cell>
<cell>
<cellProperties index="1461" type="POLY_VERTEX" name="Atom #187"/>
<nrOfStructures value="26"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="260"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="258"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="259"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="268"/>
<atomRef index="106"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="1462" type="POLY_VERTEX" name="Atom #106"/>
<nrOfStructures value="23"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="98"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="179"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="177"/>
<atomRef index="96"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="178"/>
<atomRef index="97"/>
<atomRef index="187"/>
<atomRef index="196"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="1463" type="POLY_VERTEX" name="Atom #601"/>
<nrOfStructures value="18"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="673"/>
<atomRef index="674"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="600"/>
<atomRef index="519"/>
<atomRef index="609"/>
<atomRef index="511"/>
<atomRef index="520"/>
<atomRef index="610"/>
</cell>
<cell>
<cellProperties index="1464" type="POLY_VERTEX" name="Atom #520"/>
<nrOfStructures value="24"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="593"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="510"/>
<atomRef index="429"/>
<atomRef index="600"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="609"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="601"/>
<atomRef index="439"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="448"/>
</cell>
<cell>
<cellProperties index="1465" type="POLY_VERTEX" name="Atom #439"/>
<nrOfStructures value="24"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="512"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="511"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="520"/>
<atomRef index="358"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
</cell>
<cell>
<cellProperties index="1466" type="POLY_VERTEX" name="Atom #358"/>
<nrOfStructures value="26"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="431"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="429"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="430"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="439"/>
<atomRef index="277"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="286"/>
</cell>
<cell>
<cellProperties index="1467" type="POLY_VERTEX" name="Atom #277"/>
<nrOfStructures value="26"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="350"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="348"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="349"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="358"/>
<atomRef index="196"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
</cell>
<cell>
<cellProperties index="1468" type="POLY_VERTEX" name="Atom #196"/>
<nrOfStructures value="26"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="269"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="267"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="268"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="277"/>
<atomRef index="115"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="1469" type="POLY_VERTEX" name="Atom #115"/>
<nrOfStructures value="23"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="107"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="188"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="186"/>
<atomRef index="105"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="187"/>
<atomRef index="106"/>
<atomRef index="196"/>
<atomRef index="205"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="1470" type="POLY_VERTEX" name="Atom #610"/>
<nrOfStructures value="19"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="682"/>
<atomRef index="683"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="609"/>
<atomRef index="618"/>
<atomRef index="601"/>
<atomRef index="520"/>
<atomRef index="529"/>
<atomRef index="619"/>
<atomRef index="538"/>
</cell>
<cell>
<cellProperties index="1471" type="POLY_VERTEX" name="Atom #529"/>
<nrOfStructures value="24"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="602"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="609"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="610"/>
<atomRef index="448"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="457"/>
</cell>
<cell>
<cellProperties index="1472" type="POLY_VERTEX" name="Atom #448"/>
<nrOfStructures value="23"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="521"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="519"/>
<atomRef index="438"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="520"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="529"/>
<atomRef index="367"/>
<atomRef index="538"/>
<atomRef index="376"/>
</cell>
<cell>
<cellProperties index="1473" type="POLY_VERTEX" name="Atom #367"/>
<nrOfStructures value="25"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="440"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="438"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="439"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="448"/>
<atomRef index="286"/>
<atomRef index="376"/>
<atomRef index="295"/>
</cell>
<cell>
<cellProperties index="1474" type="POLY_VERTEX" name="Atom #286"/>
<nrOfStructures value="26"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="359"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="357"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="358"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="367"/>
<atomRef index="205"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="1475" type="POLY_VERTEX" name="Atom #205"/>
<nrOfStructures value="26"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="278"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="276"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="277"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="286"/>
<atomRef index="124"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="1476" type="POLY_VERTEX" name="Atom #124"/>
<nrOfStructures value="26"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="116"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="197"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="195"/>
<atomRef index="114"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="196"/>
<atomRef index="115"/>
<atomRef index="205"/>
<atomRef index="214"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="1477" type="POLY_VERTEX" name="Atom #619"/>
<nrOfStructures value="20"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="691"/>
<atomRef index="692"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="609"/>
<atomRef index="618"/>
<atomRef index="627"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="538"/>
<atomRef index="628"/>
<atomRef index="547"/>
</cell>
<cell>
<cellProperties index="1478" type="POLY_VERTEX" name="Atom #538"/>
<nrOfStructures value="25"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="530"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="611"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="619"/>
<atomRef index="457"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="466"/>
</cell>
<cell>
<cellProperties index="1479" type="POLY_VERTEX" name="Atom #457"/>
<nrOfStructures value="15"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="465"/>
<atomRef index="529"/>
<atomRef index="538"/>
<atomRef index="376"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="385"/>
</cell>
<cell>
<cellProperties index="1480" type="POLY_VERTEX" name="Atom #376"/>
<nrOfStructures value="26"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="449"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="457"/>
<atomRef index="295"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="304"/>
</cell>
<cell>
<cellProperties index="1481" type="POLY_VERTEX" name="Atom #295"/>
<nrOfStructures value="25"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="368"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="376"/>
<atomRef index="214"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
</cell>
<cell>
<cellProperties index="1482" type="POLY_VERTEX" name="Atom #214"/>
<nrOfStructures value="26"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="287"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="295"/>
<atomRef index="133"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="1483" type="POLY_VERTEX" name="Atom #133"/>
<nrOfStructures value="23"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="125"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="206"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="205"/>
<atomRef index="124"/>
<atomRef index="214"/>
<atomRef index="223"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="1484" type="POLY_VERTEX" name="Atom #628"/>
<nrOfStructures value="21"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="700"/>
<atomRef index="701"/>
<atomRef index="709"/>
<atomRef index="710"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="636"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="547"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1485" type="POLY_VERTEX" name="Atom #547"/>
<nrOfStructures value="24"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="620"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="618"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="619"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="628"/>
<atomRef index="466"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1486" type="POLY_VERTEX" name="Atom #466"/>
<nrOfStructures value="23"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="539"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="537"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="474"/>
<atomRef index="538"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="547"/>
<atomRef index="385"/>
<atomRef index="556"/>
<atomRef index="475"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1487" type="POLY_VERTEX" name="Atom #385"/>
<nrOfStructures value="25"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="458"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="456"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="457"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="466"/>
<atomRef index="304"/>
<atomRef index="394"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="1488" type="POLY_VERTEX" name="Atom #304"/>
<nrOfStructures value="26"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="377"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="375"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="376"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="385"/>
<atomRef index="223"/>
<atomRef index="394"/>
<atomRef index="313"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="1489" type="POLY_VERTEX" name="Atom #223"/>
<nrOfStructures value="26"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="296"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="294"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="295"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="304"/>
<atomRef index="142"/>
<atomRef index="313"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="1490" type="POLY_VERTEX" name="Atom #142"/>
<nrOfStructures value="23"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="134"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="215"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="213"/>
<atomRef index="132"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="214"/>
<atomRef index="133"/>
<atomRef index="223"/>
<atomRef index="232"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="1491" type="POLY_VERTEX" name="Atom #637"/>
<nrOfStructures value="12"/>
<atomRef index="557"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="638"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="718"/>
<atomRef index="719"/>
<atomRef index="727"/>
<atomRef index="728"/>
<atomRef index="636"/>
<atomRef index="556"/>
</cell>
<cell>
<cellProperties index="1492" type="POLY_VERTEX" name="Atom #556"/>
<nrOfStructures value="26"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="548"/>
<atomRef index="557"/>
<atomRef index="564"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="629"/>
<atomRef index="638"/>
<atomRef index="645"/>
<atomRef index="646"/>
<atomRef index="647"/>
<atomRef index="627"/>
<atomRef index="546"/>
<atomRef index="465"/>
<atomRef index="636"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="628"/>
<atomRef index="547"/>
<atomRef index="466"/>
<atomRef index="637"/>
<atomRef index="475"/>
</cell>
<cell>
<cellProperties index="1493" type="POLY_VERTEX" name="Atom #475"/>
<nrOfStructures value="15"/>
<atomRef index="395"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="476"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="557"/>
<atomRef index="565"/>
<atomRef index="566"/>
<atomRef index="546"/>
<atomRef index="555"/>
<atomRef index="474"/>
<atomRef index="466"/>
<atomRef index="556"/>
<atomRef index="394"/>
</cell>
<cell>
<cellProperties index="1494" type="POLY_VERTEX" name="Atom #394"/>
<nrOfStructures value="26"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="402"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="467"/>
<atomRef index="476"/>
<atomRef index="483"/>
<atomRef index="484"/>
<atomRef index="485"/>
<atomRef index="465"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="474"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="466"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="475"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="1495" type="POLY_VERTEX" name="Atom #313"/>
<nrOfStructures value="23"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="386"/>
<atomRef index="395"/>
<atomRef index="403"/>
<atomRef index="404"/>
<atomRef index="384"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="393"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="385"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="394"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="1496" type="POLY_VERTEX" name="Atom #232"/>
<nrOfStructures value="23"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="305"/>
<atomRef index="314"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="303"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="312"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="304"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="313"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="1497" type="POLY_VERTEX" name="Atom #151"/>
<nrOfStructures value="21"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="143"/>
<atomRef index="152"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="224"/>
<atomRef index="233"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="222"/>
<atomRef index="141"/>
<atomRef index="231"/>
<atomRef index="150"/>
<atomRef index="223"/>
<atomRef index="142"/>
<atomRef index="232"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Informative Components">
<structuralComponent name="mid slice coronal" mode="POINTS">
<nrOfStructures value="49"/>
<atomRef index="604"/>
<atomRef index="523"/>
<atomRef index="442"/>
<atomRef index="361"/>
<atomRef index="280"/>
<atomRef index="199"/>
<atomRef index="118"/>
<atomRef index="605"/>
<atomRef index="524"/>
<atomRef index="443"/>
<atomRef index="362"/>
<atomRef index="281"/>
<atomRef index="200"/>
<atomRef index="119"/>
<atomRef index="606"/>
<atomRef index="525"/>
<atomRef index="444"/>
<atomRef index="363"/>
<atomRef index="282"/>
<atomRef index="201"/>
<atomRef index="120"/>
<atomRef index="607"/>
<atomRef index="526"/>
<atomRef index="445"/>
<atomRef index="364"/>
<atomRef index="283"/>
<atomRef index="202"/>
<atomRef index="121"/>
<atomRef index="608"/>
<atomRef index="527"/>
<atomRef index="446"/>
<atomRef index="365"/>
<atomRef index="284"/>
<atomRef index="203"/>
<atomRef index="122"/>
<atomRef index="609"/>
<atomRef index="528"/>
<atomRef index="447"/>
<atomRef index="366"/>
<atomRef index="285"/>
<atomRef index="204"/>
<atomRef index="123"/>
<atomRef index="610"/>
<atomRef index="529"/>
<atomRef index="448"/>
<atomRef index="367"/>
<atomRef index="286"/>
<atomRef index="205"/>
<atomRef index="124"/>
</structuralComponent>
<structuralComponent name="inside/ouside points">
<nrOfStructures value="1"/>
<cell>
<cellProperties index="1499" type="POLY_VERTEX" name="beads"/>
<color r="1" g="0" b="0" a="1"/>
<nrOfStructures value="343"/>
<atomRef index="91"/>
<atomRef index="92"/>
<atomRef index="93"/>
<atomRef index="94"/>
<atomRef index="95"/>
<atomRef index="96"/>
<atomRef index="97"/>
<atomRef index="100"/>
<atomRef index="101"/>
<atomRef index="102"/>
<atomRef index="103"/>
<atomRef index="104"/>
<atomRef index="105"/>
<atomRef index="106"/>
<atomRef index="109"/>
<atomRef index="110"/>
<atomRef index="111"/>
<atomRef index="112"/>
<atomRef index="113"/>
<atomRef index="114"/>
<atomRef index="115"/>
<atomRef index="118"/>
<atomRef index="119"/>
<atomRef index="120"/>
<atomRef index="121"/>
<atomRef index="122"/>
<atomRef index="123"/>
<atomRef index="124"/>
<atomRef index="127"/>
<atomRef index="128"/>
<atomRef index="129"/>
<atomRef index="130"/>
<atomRef index="131"/>
<atomRef index="132"/>
<atomRef index="133"/>
<atomRef index="136"/>
<atomRef index="137"/>
<atomRef index="138"/>
<atomRef index="139"/>
<atomRef index="140"/>
<atomRef index="141"/>
<atomRef index="142"/>
<atomRef index="145"/>
<atomRef index="146"/>
<atomRef index="147"/>
<atomRef index="148"/>
<atomRef index="149"/>
<atomRef index="150"/>
<atomRef index="151"/>
<atomRef index="172"/>
<atomRef index="173"/>
<atomRef index="174"/>
<atomRef index="175"/>
<atomRef index="176"/>
<atomRef index="177"/>
<atomRef index="178"/>
<atomRef index="181"/>
<atomRef index="182"/>
<atomRef index="183"/>
<atomRef index="184"/>
<atomRef index="185"/>
<atomRef index="186"/>
<atomRef index="187"/>
<atomRef index="190"/>
<atomRef index="191"/>
<atomRef index="192"/>
<atomRef index="193"/>
<atomRef index="194"/>
<atomRef index="195"/>
<atomRef index="196"/>
<atomRef index="199"/>
<atomRef index="200"/>
<atomRef index="201"/>
<atomRef index="202"/>
<atomRef index="203"/>
<atomRef index="204"/>
<atomRef index="205"/>
<atomRef index="208"/>
<atomRef index="209"/>
<atomRef index="210"/>
<atomRef index="211"/>
<atomRef index="212"/>
<atomRef index="213"/>
<atomRef index="214"/>
<atomRef index="217"/>
<atomRef index="218"/>
<atomRef index="219"/>
<atomRef index="220"/>
<atomRef index="221"/>
<atomRef index="222"/>
<atomRef index="223"/>
<atomRef index="226"/>
<atomRef index="227"/>
<atomRef index="228"/>
<atomRef index="229"/>
<atomRef index="230"/>
<atomRef index="231"/>
<atomRef index="232"/>
<atomRef index="253"/>
<atomRef index="254"/>
<atomRef index="255"/>
<atomRef index="256"/>
<atomRef index="257"/>
<atomRef index="258"/>
<atomRef index="259"/>
<atomRef index="262"/>
<atomRef index="263"/>
<atomRef index="264"/>
<atomRef index="265"/>
<atomRef index="266"/>
<atomRef index="267"/>
<atomRef index="268"/>
<atomRef index="271"/>
<atomRef index="272"/>
<atomRef index="273"/>
<atomRef index="274"/>
<atomRef index="275"/>
<atomRef index="276"/>
<atomRef index="277"/>
<atomRef index="280"/>
<atomRef index="281"/>
<atomRef index="282"/>
<atomRef index="283"/>
<atomRef index="284"/>
<atomRef index="285"/>
<atomRef index="286"/>
<atomRef index="289"/>
<atomRef index="290"/>
<atomRef index="291"/>
<atomRef index="292"/>
<atomRef index="293"/>
<atomRef index="294"/>
<atomRef index="295"/>
<atomRef index="298"/>
<atomRef index="299"/>
<atomRef index="300"/>
<atomRef index="301"/>
<atomRef index="302"/>
<atomRef index="303"/>
<atomRef index="304"/>
<atomRef index="307"/>
<atomRef index="308"/>
<atomRef index="309"/>
<atomRef index="310"/>
<atomRef index="311"/>
<atomRef index="312"/>
<atomRef index="313"/>
<atomRef index="334"/>
<atomRef index="335"/>
<atomRef index="336"/>
<atomRef index="337"/>
<atomRef index="338"/>
<atomRef index="339"/>
<atomRef index="340"/>
<atomRef index="343"/>
<atomRef index="344"/>
<atomRef index="345"/>
<atomRef index="346"/>
<atomRef index="347"/>
<atomRef index="348"/>
<atomRef index="349"/>
<atomRef index="352"/>
<atomRef index="353"/>
<atomRef index="354"/>
<atomRef index="355"/>
<atomRef index="356"/>
<atomRef index="357"/>
<atomRef index="358"/>
<atomRef index="361"/>
<atomRef index="362"/>
<atomRef index="363"/>
<atomRef index="364"/>
<atomRef index="365"/>
<atomRef index="366"/>
<atomRef index="367"/>
<atomRef index="370"/>
<atomRef index="371"/>
<atomRef index="372"/>
<atomRef index="373"/>
<atomRef index="374"/>
<atomRef index="375"/>
<atomRef index="376"/>
<atomRef index="379"/>
<atomRef index="380"/>
<atomRef index="381"/>
<atomRef index="382"/>
<atomRef index="383"/>
<atomRef index="384"/>
<atomRef index="385"/>
<atomRef index="388"/>
<atomRef index="389"/>
<atomRef index="390"/>
<atomRef index="391"/>
<atomRef index="392"/>
<atomRef index="393"/>
<atomRef index="394"/>
<atomRef index="415"/>
<atomRef index="416"/>
<atomRef index="417"/>
<atomRef index="418"/>
<atomRef index="419"/>
<atomRef index="420"/>
<atomRef index="421"/>
<atomRef index="424"/>
<atomRef index="425"/>
<atomRef index="426"/>
<atomRef index="427"/>
<atomRef index="428"/>
<atomRef index="429"/>
<atomRef index="430"/>
<atomRef index="433"/>
<atomRef index="434"/>
<atomRef index="435"/>
<atomRef index="436"/>
<atomRef index="437"/>
<atomRef index="438"/>
<atomRef index="439"/>
<atomRef index="442"/>
<atomRef index="443"/>
<atomRef index="444"/>
<atomRef index="445"/>
<atomRef index="446"/>
<atomRef index="447"/>
<atomRef index="448"/>
<atomRef index="451"/>
<atomRef index="452"/>
<atomRef index="453"/>
<atomRef index="454"/>
<atomRef index="455"/>
<atomRef index="456"/>
<atomRef index="457"/>
<atomRef index="460"/>
<atomRef index="461"/>
<atomRef index="462"/>
<atomRef index="463"/>
<atomRef index="464"/>
<atomRef index="465"/>
<atomRef index="466"/>
<atomRef index="469"/>
<atomRef index="470"/>
<atomRef index="471"/>
<atomRef index="472"/>
<atomRef index="473"/>
<atomRef index="474"/>
<atomRef index="475"/>
<atomRef index="496"/>
<atomRef index="497"/>
<atomRef index="498"/>
<atomRef index="499"/>
<atomRef index="500"/>
<atomRef index="501"/>
<atomRef index="502"/>
<atomRef index="505"/>
<atomRef index="506"/>
<atomRef index="507"/>
<atomRef index="508"/>
<atomRef index="509"/>
<atomRef index="510"/>
<atomRef index="511"/>
<atomRef index="514"/>
<atomRef index="515"/>
<atomRef index="516"/>
<atomRef index="517"/>
<atomRef index="518"/>
<atomRef index="519"/>
<atomRef index="520"/>
<atomRef index="523"/>
<atomRef index="524"/>
<atomRef index="525"/>
<atomRef index="526"/>
<atomRef index="527"/>
<atomRef index="528"/>
<atomRef index="529"/>
<atomRef index="532"/>
<atomRef index="533"/>
<atomRef index="534"/>
<atomRef index="535"/>
<atomRef index="536"/>
<atomRef index="537"/>
<atomRef index="538"/>
<atomRef index="541"/>
<atomRef index="542"/>
<atomRef index="543"/>
<atomRef index="544"/>
<atomRef index="545"/>
<atomRef index="546"/>
<atomRef index="547"/>
<atomRef index="550"/>
<atomRef index="551"/>
<atomRef index="552"/>
<atomRef index="553"/>
<atomRef index="554"/>
<atomRef index="555"/>
<atomRef index="556"/>
<atomRef index="577"/>
<atomRef index="578"/>
<atomRef index="579"/>
<atomRef index="580"/>
<atomRef index="581"/>
<atomRef index="582"/>
<atomRef index="583"/>
<atomRef index="586"/>
<atomRef index="587"/>
<atomRef index="588"/>
<atomRef index="589"/>
<atomRef index="590"/>
<atomRef index="591"/>
<atomRef index="592"/>
<atomRef index="595"/>
<atomRef index="596"/>
<atomRef index="597"/>
<atomRef index="598"/>
<atomRef index="599"/>
<atomRef index="600"/>
<atomRef index="601"/>
<atomRef index="604"/>
<atomRef index="605"/>
<atomRef index="606"/>
<atomRef index="607"/>
<atomRef index="608"/>
<atomRef index="609"/>
<atomRef index="610"/>
<atomRef index="613"/>
<atomRef index="614"/>
<atomRef index="615"/>
<atomRef index="616"/>
<atomRef index="617"/>
<atomRef index="618"/>
<atomRef index="619"/>
<atomRef index="622"/>
<atomRef index="623"/>
<atomRef index="624"/>
<atomRef index="625"/>
<atomRef index="626"/>
<atomRef index="627"/>
<atomRef index="628"/>
<atomRef index="631"/>
<atomRef index="632"/>
<atomRef index="633"/>
<atomRef index="634"/>
<atomRef index="635"/>
<atomRef index="636"/>
<atomRef index="637"/>
</cell>
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>
