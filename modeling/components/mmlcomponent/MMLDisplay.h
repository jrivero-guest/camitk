/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef MMLDisplay_H
#define MMLDisplay_H

#include <QObject>

class MMLComponent;
class MMLMonitorDisplay;

// Monitoring includes
#include <Monitor.h>
#include "MMLComponentAPI.h"

/**
 * @ingroup group_cepmodeling_components_mml
 *
 * @brief
 * TODO Describe the class here
 *
 *
 **/
class MMLCOMPONENT_API MMLDisplay : QObject {
    Q_OBJECT

public:
    MMLDisplay(MMLComponent* mmlManager);
    ~MMLDisplay() override;

public slots:
    /// slot called when display have to be updated
    void updateDisplay();
    /// slot called when Monitor displayed have changed
    void connectMonitor();
    /// slot called when Monitors tab have to be updated
    void updateMonitorsTable();
    /// slot called when physical model have changed
    void connectPml();
    /// get the displayed monitor
    Monitor* getDisplayedMonitor();
    /// get the line of the displayed monitor in the monitor tab
    int getDisplayedMonitorLine();

private:
    MMLComponent* mmlManager;
    MMLMonitorDisplay* monitorDisplay;
};

#endif
