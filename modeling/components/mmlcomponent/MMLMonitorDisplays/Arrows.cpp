/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "Arrows.h"

#include <PMLComponent.h>

#include "MMLMonitorDisplayFactory.h"
#include "MMLComponent.h"

#include <vtkDoubleArray.h>

using namespace camitk;

bool arrowsRegistered = MMLMonitorDisplayFactory::getInstance()->registerClass<Arrows>("Arrows", Monitor::VECTORSET);

Arrows::Arrows(Monitor* monitor, MMLComponent* manager): MMLMonitorDisplay(monitor, manager) {

}

// ---------------------- update ----------------------------
void Arrows::update() {

    // create the corresponding point data
    PMLComponent* pmlComp = manager->getPMLComponent();

    // Setup MeshComponent DataArray
    vtkSmartPointer<vtkDoubleArray> monitorDataArray = vtkDoubleArray::New();
    monitorDataArray->SetNumberOfComponents(3);
    monitorDataArray->Allocate(3 * pmlComp->getPhysicalModel()->getNumberOfAtoms());
    monitorDataArray->SetName(monitor->getTypeName().c_str());
    monitorDataArray->SetNumberOfTuples(pmlComp->getPhysicalModel()->getNumberOfAtoms());

    // init to zero
    double value[3] = { 0.0, 0.0, 0.0};
    for (vtkIdType i = 0; i < pmlComp->getPhysicalModel()->getNumberOfAtoms(); i++) {
        monitorDataArray->SetTuple(i, value);
    }

    // update the known data
    for (unsigned int i = 0; i < monitor->getNumberOfIndex(); i++) {
        Atom* a = pmlComp->getPhysicalModel()->getAtom(monitor->getIndexOfValues(i));
        for (unsigned int j = 0; j < 3; j++) {
            value[j] = monitor->getValue(3 * i + j);
        }
        monitorDataArray->SetTuple(pmlComp->getPointId(a), value);
    }

    pmlComp->addDataArray(camitk::MeshComponent::POINTS, QString(monitor->getTypeName().c_str()), monitorDataArray);
}

// ---------------------- hide ----------------------------
void Arrows::hide() {
    // remove the current colors
    manager->getPMLComponent()->setDataRepresentationOff();
}



