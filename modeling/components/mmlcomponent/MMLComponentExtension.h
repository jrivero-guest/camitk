/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MMLCOMPONENTEXTENSION_H
#define MMLCOMPONENTEXTENSION_H

#include <QObject>
#include <ComponentExtension.h>
#include "MMLComponentAPI.h"
/**
 * @ingroup group_cepmodeling_components_mml
 *
 * @brief
 * This MML ComponentPlugin allows you to manipulate MML document.
 * see http://www-timc.imag.fr/Aurelien.Deram/ for more information on
 * this type of documents.
 *
 *
 **/
class MMLCOMPONENT_API MMLComponentExtension : public camitk::ComponentExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ComponentExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.modeling.component.mmlcomponent")

public:
    /// the constructor (do nothing really)
    MMLComponentExtension() : ComponentExtension() {};

    /// get the plugin name
    virtual QString getName() const;

    /// get the plugin description (can be html)
    virtual QString getDescription() const;

    /// get the list of managed extensions (each file with an extension in the list can be loaded by this Extension
    virtual QStringList getFileExtensions() const;

    /** get a new instance from data stored in a file (this is the most important method to redefine in your subclass).
     *  This method may throw an AbortException if a problem occurs.
     */
    virtual camitk::Component* open(const QString&);

    /// save (only work with mml component)
    virtual bool save(camitk::Component* component) const;

protected:
    /// the destructor
    virtual ~MMLComponentExtension() = default;
};

#endif
