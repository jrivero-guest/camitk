/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef MMLCOMPONENT_H
#define MMLCOMPONENT_H

#include <camitkcore/Component.h> // Nico: do not remove the camitkcore/ prefix

#include <QObject>
#include "MMLComponentAPI.h"

class QDockWidget;

class MMLDisplay;
class MonitoringGuiManager;
class PMLComponent;
/**
 * @ingroup group_cepmodeling_components_mml
 *
 * @brief
 * This class manage an MML monitoring document ".mml".
 *
 *
 **/
class MMLCOMPONENT_API MMLComponent : public camitk::Component {
    Q_OBJECT

public:

    /** Default constructor: give it the name of the file containing the data (.mml file)
     *  This method may throw an AbortException if a problem occurs.
     */
    MMLComponent(const QString&);

    /// destructor
    virtual ~MMLComponent();

    ///@name Redefined from Component
    ///@{

    /** save the current mml in the current file name.
      *  @return false if there was a problem during the saving procedure
      */
    bool save();

    /// init the representation (nothing to do, every 3D representation is managed by the PMLComponent)
    void initRepresentation() {};

    /** Update the selection flag (this method is overridden in order to show the default
     *  modeling action when the component is selected for the first time).
     *
     * @param b the value of the flag (true means "is selected")
     * @param recursive if true (default), also updates the children Component selection flags.
     */
    virtual void setSelected(const bool b, const bool recursive = true);

    ///@}

    /// get MonitoringGuiManager
    MonitoringGuiManager* getMonitoringGuiManager();

    /// get Physical model manager
    PMLComponent* getPMLComponent();

    /// get the display
    MMLDisplay* getDisplay();

    /// connect pml
    void connectPml();

    /// save the mml in the current filename (and the pml if needed)
    void saveMML();

    /// check if the mml or pml were modified in the UI
    virtual bool getModified() const;

private:

    /// the mml GUI class is used directly
    MonitoringGuiManager* mmlGUI;

    /// physical model manager
    PMLComponent* pmlComponent;

    /// display manager
    MMLDisplay* display;

    /// when a .scn is loaded, name of the automatically exported mml file (empty string otherwise)
    QString exportedMml;

    /// the first selection should trigger the pml exporer action (default action)
    bool neverSelected;

};


#endif
