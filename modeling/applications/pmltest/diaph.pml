<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="Phymulob" nrOfAtoms="283"
 nrOfExclusiveComponents="1"
 nrOfInformativeComponents="2"
 nrOfCells="767"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="element list">
<nrOfStructures value="283"/>
<atom>
<atomProperties index="0" x="-0.525" y="1.41491" z="-0.88"/>
</atom>
<atom>
<atomProperties index="1" x="0.225" y="1.5" z="-0.88"/>
</atom>
<atom>
<atomProperties index="2" x="0.375" y="1.45" z="-0.88"/>
</atom>
<atom>
<atomProperties index="3" x="0.525" y="1.41019" z="-0.88"/>
</atom>
<atom>
<atomProperties index="4" x="0.675" y="1.38019" z="-0.88"/>
</atom>
<atom>
<atomProperties index="5" x="0.825" y="1.35495" z="-0.84"/>
</atom>
<atom>
<atomProperties index="6" x="-0.825987" y="1.25572" z="-0.771593"/>
</atom>
<atom>
<atomProperties index="7" x="-0.674784" y="1.33001" z="-0.819262"/>
</atom>
<atom>
<atomProperties index="8" x="-0.375" y="1.5" z="-0.86"/>
</atom>
<atom>
<atomProperties index="9" x="0.225" y="2.1218" z="-0.77"/>
</atom>
<atom>
<atomProperties index="10" x="0.375" y="2.408" z="-0.77"/>
</atom>
<atom>
<atomProperties index="11" x="0.525" y="2.55791" z="-0.77"/>
</atom>
<atom>
<atomProperties index="12" x="0.675" y="2.59199" z="-0.77"/>
</atom>
<atom>
<atomProperties index="13" x="0.975108" y="1.30849" z="-0.773181"/>
</atom>
<atom>
<atomProperties index="14" x="-0.975" y="1.2101" z="-0.66"/>
</atom>
<atom>
<atomProperties index="15" x="-0.825" y="2.42466" z="-0.66"/>
</atom>
<atom>
<atomProperties index="16" x="-0.675" y="2.47108" z="-0.66"/>
</atom>
<atom>
<atomProperties index="17" x="-0.525" y="2.41968" z="-0.66"/>
</atom>
<atom>
<atomProperties index="18" x="-0.375" y="2.31973" z="-0.66"/>
</atom>
<atom>
<atomProperties index="19" x="-0.22815" y="1.94897" z="-0.69354"/>
</atom>
<atom>
<atomProperties index="20" x="0.225" y="2.27191" z="-0.66"/>
</atom>
<atom>
<atomProperties index="21" x="0.375" y="2.54818" z="-0.66"/>
</atom>
<atom>
<atomProperties index="22" x="0.525" y="2.65098" z="-0.66"/>
</atom>
<atom>
<atomProperties index="23" x="0.675" y="2.67668" z="-0.66"/>
</atom>
<atom>
<atomProperties index="24" x="0.825" y="2.66383" z="-0.66"/>
</atom>
<atom>
<atomProperties index="25" x="0.975" y="2.62528" z="-0.66"/>
</atom>
<atom>
<atomProperties index="26" x="1.125" y="2.56103" z="-0.66"/>
</atom>
<atom>
<atomProperties index="27" x="1.275" y="2.41326" z="-0.66"/>
</atom>
<atom>
<atomProperties index="28" x="1.175" y="1.2231" z="-0.66"/>
</atom>
<atom>
<atomProperties index="29" x="-1.125" y="1.17845" z="-0.56"/>
</atom>
<atom>
<atomProperties index="30" x="-0.975" y="2.48698" z="-0.55"/>
</atom>
<atom>
<atomProperties index="31" x="-0.825" y="2.54526" z="-0.55"/>
</atom>
<atom>
<atomProperties index="32" x="-0.675" y="2.55733" z="-0.55"/>
</atom>
<atom>
<atomProperties index="33" x="-0.525" y="2.50508" z="-0.55"/>
</atom>
<atom>
<atomProperties index="34" x="-0.375" y="2.38662" z="-0.55"/>
</atom>
<atom>
<atomProperties index="35" x="-0.225" y="2.17591" z="-0.55"/>
</atom>
<atom>
<atomProperties index="36" x="0.225" y="2.33419" z="-0.55"/>
</atom>
<atom>
<atomProperties index="37" x="0.375" y="2.59958" z="-0.55"/>
</atom>
<atom>
<atomProperties index="38" x="0.525" y="2.70822" z="-0.55"/>
</atom>
<atom>
<atomProperties index="39" x="0.675" y="2.75047" z="-0.55"/>
</atom>
<atom>
<atomProperties index="40" x="0.825" y="2.7384" z="-0.55"/>
</atom>
<atom>
<atomProperties index="41" x="0.975" y="2.69011" z="-0.55"/>
</atom>
<atom>
<atomProperties index="42" x="1.125" y="2.62372" z="-0.55"/>
</atom>
<atom>
<atomProperties index="43" x="1.275" y="2.52112" z="-0.55"/>
</atom>
<atom>
<atomProperties index="44" x="1.28597" y="1.18367" z="-0.559787"/>
</atom>
<atom>
<atomProperties index="45" x="-1.275" y="1.15179" z="-0.44"/>
</atom>
<atom>
<atomProperties index="46" x="-1.125" y="2.48023" z="-0.44"/>
</atom>
<atom>
<atomProperties index="47" x="-0.975" y="2.56492" z="-0.44"/>
</atom>
<atom>
<atomProperties index="48" x="-0.825" y="2.60445" z="-0.44"/>
</atom>
<atom>
<atomProperties index="49" x="-0.675" y="2.59315" z="-0.44"/>
</atom>
<atom>
<atomProperties index="50" x="-0.525" y="2.55363" z="-0.44"/>
</atom>
<atom>
<atomProperties index="51" x="-0.375" y="2.44939" z="-0.44"/>
</atom>
<atom>
<atomProperties index="52" x="-0.225" y="2.29479" z="-0.44"/>
</atom>
<atom>
<atomProperties index="53" x="0.225" y="2.38731" z="-0.44"/>
</atom>
<atom>
<atomProperties index="54" x="0.375" y="2.60844" z="-0.44"/>
</atom>
<atom>
<atomProperties index="55" x="0.525" y="2.72866" z="-0.44"/>
</atom>
<atom>
<atomProperties index="56" x="0.675" y="2.75125" z="-0.44"/>
</atom>
<atom>
<atomProperties index="57" x="0.825" y="2.7456" z="-0.44"/>
</atom>
<atom>
<atomProperties index="58" x="0.975" y="2.73431" z="-0.44"/>
</atom>
<atom>
<atomProperties index="59" x="1.125" y="2.68914" z="-0.44"/>
</atom>
<atom>
<atomProperties index="60" x="1.275" y="2.61574" z="-0.44"/>
</atom>
<atom>
<atomProperties index="61" x="1.39475" y="2.34351" z="-0.481052"/>
</atom>
<atom>
<atomProperties index="62" x="1.415" y="1.15973" z="-0.44"/>
</atom>
<atom>
<atomProperties index="63" x="-1.36836" y="1.11024" z="-0.33252"/>
</atom>
<atom>
<atomProperties index="64" x="-1.275" y="2.41863" z="-0.33"/>
</atom>
<atom>
<atomProperties index="65" x="-1.125" y="2.55725" z="-0.33"/>
</atom>
<atom>
<atomProperties index="66" x="-0.975" y="2.63752" z="-0.33"/>
</atom>
<atom>
<atomProperties index="67" x="-0.825" y="2.65368" z="-0.33"/>
</atom>
<atom>
<atomProperties index="68" x="-0.675" y="2.6356" z="-0.33"/>
</atom>
<atom>
<atomProperties index="69" x="-0.525" y="2.60341" z="-0.33"/>
</atom>
<atom>
<atomProperties index="70" x="-0.375" y="2.51095" z="-0.33"/>
</atom>
<atom>
<atomProperties index="71" x="-0.225" y="2.37042" z="-0.33"/>
</atom>
<atom>
<atomProperties index="72" x="0.225" y="2.44274" z="-0.33"/>
</atom>
<atom>
<atomProperties index="73" x="0.375" y="2.60972" z="-0.33"/>
</atom>
<atom>
<atomProperties index="74" x="0.525" y="2.71997" z="-0.33"/>
</atom>
<atom>
<atomProperties index="75" x="0.675" y="2.76216" z="-0.33"/>
</atom>
<atom>
<atomProperties index="76" x="0.825" y="2.76818" z="-0.33"/>
</atom>
<atom>
<atomProperties index="77" x="0.975" y="2.76216" z="-0.33"/>
</atom>
<atom>
<atomProperties index="78" x="1.125" y="2.73202" z="-0.33"/>
</atom>
<atom>
<atomProperties index="79" x="1.275" y="2.63715" z="-0.33"/>
</atom>
<atom>
<atomProperties index="80" x="1.425" y="2.46999" z="-0.33"/>
</atom>
<atom>
<atomProperties index="81" x="1.485" y="1.14468" z="-0.33"/>
</atom>
<atom>
<atomProperties index="82" x="-1.385" y="1.07736" z="-0.22"/>
</atom>
<atom>
<atomProperties index="83" x="-1.275" y="2.50281" z="-0.22"/>
</atom>
<atom>
<atomProperties index="84" x="-1.125" y="2.64375" z="-0.22"/>
</atom>
<atom>
<atomProperties index="85" x="-0.975" y="2.71703" z="-0.22"/>
</atom>
<atom>
<atomProperties index="86" x="-0.825" y="2.72831" z="-0.22"/>
</atom>
<atom>
<atomProperties index="87" x="-0.675" y="2.71703" z="-0.22"/>
</atom>
<atom>
<atomProperties index="88" x="-0.525" y="2.67193" z="-0.22"/>
</atom>
<atom>
<atomProperties index="89" x="-0.375" y="2.59301" z="-0.22"/>
</atom>
<atom>
<atomProperties index="90" x="-0.225" y="2.48053" z="-0.22"/>
</atom>
<atom>
<atomProperties index="91" x="0.225" y="2.50053" z="-0.22"/>
</atom>
<atom>
<atomProperties index="92" x="0.375" y="2.63247" z="-0.22"/>
</atom>
<atom>
<atomProperties index="93" x="0.525" y="2.7114" z="-0.22"/>
</atom>
<atom>
<atomProperties index="94" x="0.675" y="2.76777" z="-0.22"/>
</atom>
<atom>
<atomProperties index="95" x="0.825" y="2.80159" z="-0.22"/>
</atom>
<atom>
<atomProperties index="96" x="0.975" y="2.80159" z="-0.22"/>
</atom>
<atom>
<atomProperties index="97" x="1.125" y="2.77341" z="-0.22"/>
</atom>
<atom>
<atomProperties index="98" x="1.275" y="2.70012" z="-0.22"/>
</atom>
<atom>
<atomProperties index="99" x="1.425" y="2.52355" z="-0.22"/>
</atom>
<atom>
<atomProperties index="100" x="1.515" y="1.11913" z="-0.22"/>
</atom>
<atom>
<atomProperties index="101" x="-1.42379" y="1.05809" z="-0.116919"/>
</atom>
<atom>
<atomProperties index="102" x="-1.275" y="2.55257" z="-0.11"/>
</atom>
<atom>
<atomProperties index="103" x="-1.125" y="2.70102" z="-0.11"/>
</atom>
<atom>
<atomProperties index="104" x="-0.975" y="2.76287" z="-0.11"/>
</atom>
<atom>
<atomProperties index="105" x="-0.825" y="2.78762" z="-0.11"/>
</atom>
<atom>
<atomProperties index="106" x="-0.675" y="2.7938" z="-0.11"/>
</atom>
<atom>
<atomProperties index="107" x="-0.525" y="2.74432" z="-0.11"/>
</atom>
<atom>
<atomProperties index="108" x="-0.375" y="2.69483" z="-0.11"/>
</atom>
<atom>
<atomProperties index="109" x="-0.225" y="2.59679" z="-0.11"/>
</atom>
<atom>
<atomProperties index="110" x="0.225" y="2.57113" z="-0.11"/>
</atom>
<atom>
<atomProperties index="111" x="0.375" y="2.63298" z="-0.11"/>
</atom>
<atom>
<atomProperties index="112" x="0.525" y="2.68656" z="-0.11"/>
</atom>
<atom>
<atomProperties index="113" x="0.675" y="2.74432" z="-0.11"/>
</atom>
<atom>
<atomProperties index="114" x="0.825" y="2.78143" z="-0.11"/>
</atom>
<atom>
<atomProperties index="115" x="0.975" y="2.79737" z="-0.11"/>
</atom>
<atom>
<atomProperties index="116" x="1.125" y="2.76906" z="-0.11"/>
</atom>
<atom>
<atomProperties index="117" x="1.275" y="2.70957" z="-0.11"/>
</atom>
<atom>
<atomProperties index="118" x="1.425" y="2.54684" z="-0.11"/>
</atom>
<atom>
<atomProperties index="119" x="1.535" y="1.10007" z="-0.11"/>
</atom>
<atom>
<atomProperties index="120" x="-1.425" y="1.04674" z="0"/>
</atom>
<atom>
<atomProperties index="121" x="-1.275" y="2.59008" z="0"/>
</atom>
<atom>
<atomProperties index="122" x="-1.125" y="2.72376" z="0"/>
</atom>
<atom>
<atomProperties index="123" x="-0.975" y="2.80932" z="0"/>
</atom>
<atom>
<atomProperties index="124" x="-0.825" y="2.82588" z="0"/>
</atom>
<atom>
<atomProperties index="125" x="-0.675" y="2.83425" z="0"/>
</atom>
<atom>
<atomProperties index="126" x="-0.525" y="2.79269" z="0"/>
</atom>
<atom>
<atomProperties index="127" x="-0.375" y="2.76826" z="0"/>
</atom>
<atom>
<atomProperties index="128" x="-0.225" y="2.67098" z="0"/>
</atom>
<atom>
<atomProperties index="129" x="-0.075" y="2.63447" z="-0.1"/>
</atom>
<atom>
<atomProperties index="130" x="0.075" y="2.58447" z="-0.1"/>
</atom>
<atom>
<atomProperties index="131" x="0.225" y="2.59275" z="0"/>
</atom>
<atom>
<atomProperties index="132" x="0.375" y="2.61905" z="0"/>
</atom>
<atom>
<atomProperties index="133" x="0.525" y="2.66535" z="0"/>
</atom>
<atom>
<atomProperties index="134" x="0.675" y="2.70575" z="0"/>
</atom>
<atom>
<atomProperties index="135" x="0.825" y="2.74472" z="0"/>
</atom>
<atom>
<atomProperties index="136" x="0.975" y="2.8053" z="0"/>
</atom>
<atom>
<atomProperties index="137" x="1.125" y="2.76632" z="0"/>
</atom>
<atom>
<atomProperties index="138" x="1.275" y="2.70836" z="0"/>
</atom>
<atom>
<atomProperties index="139" x="1.425" y="2.53614" z="0"/>
</atom>
<atom>
<atomProperties index="140" x="1.53509" y="1.09258" z="-0.0039482"/>
</atom>
<atom>
<atomProperties index="141" x="-1.425" y="1.03891" z="0.11"/>
</atom>
<atom>
<atomProperties index="142" x="-1.275" y="2.59244" z="0.11"/>
</atom>
<atom>
<atomProperties index="143" x="-1.125" y="2.73392" z="0.11"/>
</atom>
<atom>
<atomProperties index="144" x="-0.975" y="2.80466" z="0.11"/>
</atom>
<atom>
<atomProperties index="145" x="-0.825" y="2.85418" z="0.11"/>
</atom>
<atom>
<atomProperties index="146" x="-0.675" y="2.8754" z="0.11"/>
</atom>
<atom>
<atomProperties index="147" x="-0.525" y="2.8454" z="0.11"/>
</atom>
<atom>
<atomProperties index="148" x="-0.375" y="2.80588" z="0.11"/>
</atom>
<atom>
<atomProperties index="149" x="-0.225" y="2.73221" z="0.11"/>
</atom>
<atom>
<atomProperties index="150" x="-0.075" y="2.69977" z="0.11"/>
</atom>
<atom>
<atomProperties index="151" x="0.075" y="2.6544" z="0.11"/>
</atom>
<atom>
<atomProperties index="152" x="0.225" y="2.63025" z="0.11"/>
</atom>
<atom>
<atomProperties index="153" x="0.375" y="2.62733" z="0.11"/>
</atom>
<atom>
<atomProperties index="154" x="0.525" y="2.64912" z="0.11"/>
</atom>
<atom>
<atomProperties index="155" x="0.675" y="2.6844" z="0.11"/>
</atom>
<atom>
<atomProperties index="156" x="0.825" y="2.72627" z="0.11"/>
</atom>
<atom>
<atomProperties index="157" x="0.975" y="2.77384" z="0.11"/>
</atom>
<atom>
<atomProperties index="158" x="1.125" y="2.75514" z="0.11"/>
</atom>
<atom>
<atomProperties index="159" x="1.275" y="2.70562" z="0.11"/>
</atom>
<atom>
<atomProperties index="160" x="1.425" y="2.51264" z="0.11"/>
</atom>
<atom>
<atomProperties index="161" x="1.525" y="1.11236" z="0.108433"/>
</atom>
<atom>
<atomProperties index="162" x="-1.35606" y="1.04144" z="0.231453"/>
</atom>
<atom>
<atomProperties index="163" x="-1.275" y="2.52989" z="0.22"/>
</atom>
<atom>
<atomProperties index="164" x="-1.125" y="2.67734" z="0.22"/>
</atom>
<atom>
<atomProperties index="165" x="-0.975" y="2.77787" z="0.22"/>
</atom>
<atom>
<atomProperties index="166" x="-0.825" y="2.8316" z="0.22"/>
</atom>
<atom>
<atomProperties index="167" x="-0.675" y="2.8583" z="0.22"/>
</atom>
<atom>
<atomProperties index="168" x="-0.525" y="2.8449" z="0.22"/>
</atom>
<atom>
<atomProperties index="169" x="-0.375" y="2.80468" z="0.22"/>
</atom>
<atom>
<atomProperties index="170" x="-0.225" y="2.74436" z="0.22"/>
</atom>
<atom>
<atomProperties index="171" x="-0.075" y="2.70415" z="0.22"/>
</atom>
<atom>
<atomProperties index="172" x="0.075" y="2.65393" z="0.22"/>
</atom>
<atom>
<atomProperties index="173" x="0.225" y="2.64383" z="0.22"/>
</atom>
<atom>
<atomProperties index="174" x="0.375" y="2.61702" z="0.22"/>
</atom>
<atom>
<atomProperties index="175" x="0.525" y="2.61712" z="0.22"/>
</atom>
<atom>
<atomProperties index="176" x="0.675" y="2.62259" z="0.22"/>
</atom>
<atom>
<atomProperties index="177" x="0.825" y="2.66755" z="0.22"/>
</atom>
<atom>
<atomProperties index="178" x="0.975" y="2.69686" z="0.22"/>
</atom>
<atom>
<atomProperties index="179" x="1.125" y="2.69132" z="0.22"/>
</atom>
<atom>
<atomProperties index="180" x="1.275" y="2.63032" z="0.22"/>
</atom>
<atom>
<atomProperties index="181" x="1.425" y="2.46949" z="0.22"/>
</atom>
<atom>
<atomProperties index="182" x="1.515" y="1.13" z="0.22"/>
</atom>
<atom>
<atomProperties index="183" x="-1.275" y="1.03219" z="0.345987"/>
</atom>
<atom>
<atomProperties index="184" x="-1.125" y="2.61497" z="0.33"/>
</atom>
<atom>
<atomProperties index="185" x="-0.975" y="2.73359" z="0.33"/>
</atom>
<atom>
<atomProperties index="186" x="-0.825" y="2.80322" z="0.33"/>
</atom>
<atom>
<atomProperties index="187" x="-0.675" y="2.82855" z="0.33"/>
</atom>
<atom>
<atomProperties index="188" x="-0.525" y="2.80955" z="0.33"/>
</atom>
<atom>
<atomProperties index="189" x="-0.375" y="2.77157" z="0.33"/>
</atom>
<atom>
<atomProperties index="190" x="-0.225" y="2.72726" z="0.33"/>
</atom>
<atom>
<atomProperties index="191" x="-0.075" y="2.68028" z="0.33"/>
</atom>
<atom>
<atomProperties index="192" x="0.075" y="2.63229" z="0.33"/>
</atom>
<atom>
<atomProperties index="193" x="0.225" y="2.61165" z="0.33"/>
</atom>
<atom>
<atomProperties index="194" x="0.375" y="2.58165" z="0.33"/>
</atom>
<atom>
<atomProperties index="195" x="0.525" y="2.58165" z="0.33"/>
</atom>
<atom>
<atomProperties index="196" x="0.675" y="2.57064" z="0.33"/>
</atom>
<atom>
<atomProperties index="197" x="0.825" y="2.60064" z="0.33"/>
</atom>
<atom>
<atomProperties index="198" x="0.975" y="2.59496" z="0.33"/>
</atom>
<atom>
<atomProperties index="199" x="1.125" y="2.58862" z="0.33"/>
</atom>
<atom>
<atomProperties index="200" x="1.275" y="2.52532" z="0.33"/>
</atom>
<atom>
<atomProperties index="201" x="1.425" y="2.36072" z="0.33"/>
</atom>
<atom>
<atomProperties index="202" x="1.465" y="1.15601" z="0.330118"/>
</atom>
<atom>
<atomProperties index="203" x="-1.205" y="1.02" z="0.44"/>
</atom>
<atom>
<atomProperties index="204" x="-1.125" y="2.5092" z="0.44"/>
</atom>
<atom>
<atomProperties index="205" x="-0.975" y="2.66557" z="0.44"/>
</atom>
<atom>
<atomProperties index="206" x="-0.825" y="2.74715" z="0.44"/>
</atom>
<atom>
<atomProperties index="207" x="-0.675" y="2.77401" z="0.44"/>
</atom>
<atom>
<atomProperties index="208" x="-0.375" y="2.71316" z="0.44"/>
</atom>
<atom>
<atomProperties index="209" x="-0.225" y="2.66557" z="0.44"/>
</atom>
<atom>
<atomProperties index="210" x="-0.075" y="2.64798" z="0.44"/>
</atom>
<atom>
<atomProperties index="211" x="0.075" y="2.60611" z="0.44"/>
</atom>
<atom>
<atomProperties index="212" x="0.225" y="2.5696" z="0.44"/>
</atom>
<atom>
<atomProperties index="213" x="0.375" y="2.5324" z="0.44"/>
</atom>
<atom>
<atomProperties index="214" x="0.525" y="2.52561" z="0.44"/>
</atom>
<atom>
<atomProperties index="215" x="0.675" y="2.5024" z="0.44"/>
</atom>
<atom>
<atomProperties index="216" x="0.825" y="2.53392" z="0.44"/>
</atom>
<atom>
<atomProperties index="217" x="0.975" y="2.52348" z="0.44"/>
</atom>
<atom>
<atomProperties index="218" x="1.125" y="2.48201" z="0.44"/>
</atom>
<atom>
<atomProperties index="219" x="1.275" y="2.37284" z="0.44"/>
</atom>
<atom>
<atomProperties index="220" x="1.425" y="1.20636" z="0.44"/>
</atom>
<atom>
<atomProperties index="221" x="-1.09705" y="1.06094" z="0.560768"/>
</atom>
<atom>
<atomProperties index="222" x="-0.975" y="2.54499" z="0.55"/>
</atom>
<atom>
<atomProperties index="223" x="-0.825" y="2.63742" z="0.55"/>
</atom>
<atom>
<atomProperties index="224" x="-0.675" y="2.65284" z="0.55"/>
</atom>
<atom>
<atomProperties index="225" x="-0.525" y="2.64328" z="0.55"/>
</atom>
<atom>
<atomProperties index="226" x="-0.375" y="2.60774" z="0.55"/>
</atom>
<atom>
<atomProperties index="227" x="-0.225" y="2.5914" z="0.55"/>
</atom>
<atom>
<atomProperties index="228" x="-0.075" y="2.56862" z="0.55"/>
</atom>
<atom>
<atomProperties index="229" x="0.075" y="2.561" z="0.55"/>
</atom>
<atom>
<atomProperties index="230" x="0.225" y="2.52917" z="0.55"/>
</atom>
<atom>
<atomProperties index="231" x="0.375" y="2.50025" z="0.55"/>
</atom>
<atom>
<atomProperties index="232" x="0.525" y="2.45718" z="0.55"/>
</atom>
<atom>
<atomProperties index="233" x="0.675" y="2.45428" z="0.55"/>
</atom>
<atom>
<atomProperties index="234" x="0.825" y="2.45133" z="0.55"/>
</atom>
<atom>
<atomProperties index="235" x="0.975" y="2.41982" z="0.55"/>
</atom>
<atom>
<atomProperties index="236" x="1.125" y="2.33954" z="0.55"/>
</atom>
<atom>
<atomProperties index="237" x="1.275" y="2.1884" z="0.55"/>
</atom>
<atom>
<atomProperties index="238" x="1.375" y="1.25684" z="0.550314"/>
</atom>
<atom>
<atomProperties index="239" x="-0.976482" y="1.17034" z="0.649778"/>
</atom>
<atom>
<atomProperties index="240" x="-0.825" y="2.33897" z="0.66"/>
</atom>
<atom>
<atomProperties index="241" x="-0.675" y="2.3914" z="0.66"/>
</atom>
<atom>
<atomProperties index="242" x="-0.525" y="2.40256" z="0.66"/>
</atom>
<atom>
<atomProperties index="243" x="-0.375" y="2.42026" z="0.66"/>
</atom>
<atom>
<atomProperties index="244" x="-0.225" y="2.42719" z="0.66"/>
</atom>
<atom>
<atomProperties index="245" x="-0.075" y="2.43412" z="0.66"/>
</atom>
<atom>
<atomProperties index="246" x="0.075" y="2.44106" z="0.66"/>
</atom>
<atom>
<atomProperties index="247" x="0.225" y="2.44106" z="0.66"/>
</atom>
<atom>
<atomProperties index="248" x="0.375" y="2.39945" z="0.66"/>
</atom>
<atom>
<atomProperties index="249" x="0.525" y="2.35785" z="0.66"/>
</atom>
<atom>
<atomProperties index="250" x="0.675" y="2.29262" z="0.66"/>
</atom>
<atom>
<atomProperties index="251" x="0.825" y="2.24589" z="0.66"/>
</atom>
<atom>
<atomProperties index="252" x="0.975" y="2.13597" z="0.66"/>
</atom>
<atom>
<atomProperties index="253" x="1.125" y="2.04246" z="0.66"/>
</atom>
<atom>
<atomProperties index="254" x="1.275" y="1.32939" z="0.66"/>
</atom>
<atom>
<atomProperties index="255" x="-0.825" y="1.28927" z="0.77"/>
</atom>
<atom>
<atomProperties index="256" x="-0.676739" y="2.03348" z="0.762133"/>
</atom>
<atom>
<atomProperties index="257" x="-0.525" y="2.06806" z="0.77"/>
</atom>
<atom>
<atomProperties index="258" x="-0.375" y="2.14465" z="0.77"/>
</atom>
<atom>
<atomProperties index="259" x="-0.225" y="2.22244" z="0.77"/>
</atom>
<atom>
<atomProperties index="260" x="-0.075" y="2.29508" z="0.77"/>
</atom>
<atom>
<atomProperties index="261" x="0.075" y="2.32332" z="0.77"/>
</atom>
<atom>
<atomProperties index="262" x="0.225" y="2.33991" z="0.77"/>
</atom>
<atom>
<atomProperties index="263" x="0.382587" y="2.29193" z="0.766709"/>
</atom>
<atom>
<atomProperties index="264" x="0.535045" y="2.23269" z="0.774862"/>
</atom>
<atom>
<atomProperties index="265" x="0.688361" y="2.16884" z="0.779068"/>
</atom>
<atom>
<atomProperties index="266" x="0.838661" y="2.06412" z="0.769179"/>
</atom>
<atom>
<atomProperties index="267" x="0.973313" y="1.92409" z="0.786482"/>
</atom>
<atom>
<atomProperties index="268" x="1.19356" y="1.38926" z="0.755883"/>
</atom>
<atom>
<atomProperties index="269" x="-0.675" y="1.44556" z="0.88"/>
</atom>
<atom>
<atomProperties index="270" x="-0.525" y="1.65" z="0.88"/>
</atom>
<atom>
<atomProperties index="271" x="-0.375" y="1.83" z="0.92"/>
</atom>
<atom>
<atomProperties index="272" x="-0.225" y="2" z="0.91"/>
</atom>
<atom>
<atomProperties index="273" x="-0.075" y="2.13" z="0.91"/>
</atom>
<atom>
<atomProperties index="274" x="0.075" y="2.19" z="0.91"/>
</atom>
<atom>
<atomProperties index="275" x="0.225" y="2.2" z="0.89"/>
</atom>
<atom>
<atomProperties index="276" x="0.375" y="2.17" z="0.91"/>
</atom>
<atom>
<atomProperties index="277" x="0.525" y="2.11158" z="0.935"/>
</atom>
<atom>
<atomProperties index="278" x="0.675" y="1.98186" z="0.935"/>
</atom>
<atom>
<atomProperties index="279" x="0.825" y="1.81754" z="0.935"/>
</atom>
<atom>
<atomProperties index="280" x="0.975" y="1.64824" z="0.935"/>
</atom>
<atom>
<atomProperties index="281" x="1.125" y="1.50952" z="0.88"/>
</atom>
<atom>
<atomProperties index="282" x="-0.525" y="2.75313" z="0.44"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Regions ">
<structuralComponent name="Region #0 initialMatrix">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX"/>
<nrOfStructures value="283"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="12"/>
<atomRef index="13"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="18"/>
<atomRef index="19"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="27"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="61"/>
<atomRef index="62"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="81"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="90"/>
<atomRef index="91"/>
<atomRef index="92"/>
<atomRef index="93"/>
<atomRef index="94"/>
<atomRef index="95"/>
<atomRef index="96"/>
<atomRef index="97"/>
<atomRef index="98"/>
<atomRef index="99"/>
<atomRef index="100"/>
<atomRef index="101"/>
<atomRef index="102"/>
<atomRef index="103"/>
<atomRef index="104"/>
<atomRef index="105"/>
<atomRef index="106"/>
<atomRef index="107"/>
<atomRef index="108"/>
<atomRef index="109"/>
<atomRef index="110"/>
<atomRef index="111"/>
<atomRef index="112"/>
<atomRef index="113"/>
<atomRef index="114"/>
<atomRef index="115"/>
<atomRef index="116"/>
<atomRef index="117"/>
<atomRef index="118"/>
<atomRef index="119"/>
<atomRef index="120"/>
<atomRef index="121"/>
<atomRef index="122"/>
<atomRef index="123"/>
<atomRef index="124"/>
<atomRef index="125"/>
<atomRef index="126"/>
<atomRef index="127"/>
<atomRef index="128"/>
<atomRef index="129"/>
<atomRef index="130"/>
<atomRef index="131"/>
<atomRef index="132"/>
<atomRef index="133"/>
<atomRef index="134"/>
<atomRef index="135"/>
<atomRef index="136"/>
<atomRef index="137"/>
<atomRef index="138"/>
<atomRef index="139"/>
<atomRef index="140"/>
<atomRef index="141"/>
<atomRef index="142"/>
<atomRef index="143"/>
<atomRef index="144"/>
<atomRef index="145"/>
<atomRef index="146"/>
<atomRef index="147"/>
<atomRef index="148"/>
<atomRef index="149"/>
<atomRef index="150"/>
<atomRef index="151"/>
<atomRef index="152"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="171"/>
<atomRef index="172"/>
<atomRef index="173"/>
<atomRef index="174"/>
<atomRef index="175"/>
<atomRef index="176"/>
<atomRef index="177"/>
<atomRef index="178"/>
<atomRef index="179"/>
<atomRef index="180"/>
<atomRef index="181"/>
<atomRef index="182"/>
<atomRef index="183"/>
<atomRef index="184"/>
<atomRef index="185"/>
<atomRef index="186"/>
<atomRef index="187"/>
<atomRef index="188"/>
<atomRef index="189"/>
<atomRef index="190"/>
<atomRef index="191"/>
<atomRef index="192"/>
<atomRef index="193"/>
<atomRef index="194"/>
<atomRef index="195"/>
<atomRef index="196"/>
<atomRef index="197"/>
<atomRef index="198"/>
<atomRef index="199"/>
<atomRef index="200"/>
<atomRef index="201"/>
<atomRef index="202"/>
<atomRef index="203"/>
<atomRef index="204"/>
<atomRef index="205"/>
<atomRef index="206"/>
<atomRef index="207"/>
<atomRef index="208"/>
<atomRef index="209"/>
<atomRef index="210"/>
<atomRef index="211"/>
<atomRef index="212"/>
<atomRef index="213"/>
<atomRef index="214"/>
<atomRef index="215"/>
<atomRef index="216"/>
<atomRef index="217"/>
<atomRef index="218"/>
<atomRef index="219"/>
<atomRef index="220"/>
<atomRef index="221"/>
<atomRef index="222"/>
<atomRef index="223"/>
<atomRef index="224"/>
<atomRef index="225"/>
<atomRef index="226"/>
<atomRef index="227"/>
<atomRef index="228"/>
<atomRef index="229"/>
<atomRef index="230"/>
<atomRef index="231"/>
<atomRef index="232"/>
<atomRef index="233"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="238"/>
<atomRef index="239"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="252"/>
<atomRef index="253"/>
<atomRef index="254"/>
<atomRef index="255"/>
<atomRef index="256"/>
<atomRef index="257"/>
<atomRef index="258"/>
<atomRef index="259"/>
<atomRef index="260"/>
<atomRef index="261"/>
<atomRef index="262"/>
<atomRef index="263"/>
<atomRef index="264"/>
<atomRef index="265"/>
<atomRef index="266"/>
<atomRef index="267"/>
<atomRef index="268"/>
<atomRef index="269"/>
<atomRef index="270"/>
<atomRef index="271"/>
<atomRef index="272"/>
<atomRef index="273"/>
<atomRef index="274"/>
<atomRef index="275"/>
<atomRef index="276"/>
<atomRef index="277"/>
<atomRef index="278"/>
<atomRef index="279"/>
<atomRef index="280"/>
<atomRef index="281"/>
<atomRef index="282"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Phymul Informative Component">
<structuralComponent name="Element Neighborhoods">
<color r="0.5" g="0.5" b="0.5" a="1"/>
<nrOfStructures value="283"/>
<cell>
<cellProperties index="1" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="7"/>
<atomRef index="17"/>
<atomRef index="18"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="2" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="3" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="10"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="4" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="2"/>
<atomRef index="10"/>
<atomRef index="11"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="5" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="12"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="6" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="4"/>
<atomRef index="12"/>
<atomRef index="24"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="7" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="14"/>
<atomRef index="15"/>
<atomRef index="16"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="8" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="6"/>
<atomRef index="16"/>
<atomRef index="17"/>
<atomRef index="0"/>
</cell>
<cell>
<cellProperties index="9" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="18"/>
<atomRef index="19"/>
</cell>
<cell>
<cellProperties index="10" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="20"/>
<atomRef index="10"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="11" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="1"/>
<atomRef index="9"/>
<atomRef index="20"/>
<atomRef index="21"/>
<atomRef index="22"/>
<atomRef index="11"/>
<atomRef index="3"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="12" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="10"/>
<atomRef index="22"/>
<atomRef index="12"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="13" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="3"/>
<atomRef index="11"/>
<atomRef index="22"/>
<atomRef index="23"/>
<atomRef index="24"/>
<atomRef index="5"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="14" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="5"/>
<atomRef index="24"/>
<atomRef index="25"/>
<atomRef index="26"/>
<atomRef index="28"/>
</cell>
<cell>
<cellProperties index="15" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="29"/>
<atomRef index="30"/>
<atomRef index="15"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="16" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="14"/>
<atomRef index="30"/>
<atomRef index="31"/>
<atomRef index="16"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="17" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="6"/>
<atomRef index="15"/>
<atomRef index="31"/>
<atomRef index="32"/>
<atomRef index="33"/>
<atomRef index="17"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="18" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="7"/>
<atomRef index="16"/>
<atomRef index="33"/>
<atomRef index="18"/>
<atomRef index="0"/>
</cell>
<cell>
<cellProperties index="19" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="17"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="19"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="20" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="18"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="21" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="21"/>
<atomRef index="10"/>
<atomRef index="9"/>
</cell>
<cell>
<cellProperties index="22" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="20"/>
<atomRef index="37"/>
<atomRef index="22"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="23" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="10"/>
<atomRef index="21"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="23"/>
<atomRef index="12"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="24" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="22"/>
<atomRef index="39"/>
<atomRef index="24"/>
<atomRef index="12"/>
</cell>
<cell>
<cellProperties index="25" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="12"/>
<atomRef index="23"/>
<atomRef index="39"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="25"/>
<atomRef index="13"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="26" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="24"/>
<atomRef index="41"/>
<atomRef index="26"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="27" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="13"/>
<atomRef index="25"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="27"/>
</cell>
<cell>
<cellProperties index="28" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="28"/>
<atomRef index="26"/>
<atomRef index="43"/>
<atomRef index="61"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="29" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="13"/>
<atomRef index="26"/>
<atomRef index="27"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="30" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="45"/>
<atomRef index="46"/>
<atomRef index="30"/>
<atomRef index="14"/>
</cell>
<cell>
<cellProperties index="31" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="29"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="31"/>
<atomRef index="15"/>
<atomRef index="14"/>
</cell>
<cell>
<cellProperties index="32" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="30"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="32"/>
<atomRef index="16"/>
<atomRef index="15"/>
</cell>
<cell>
<cellProperties index="33" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="31"/>
<atomRef index="49"/>
<atomRef index="33"/>
<atomRef index="16"/>
</cell>
<cell>
<cellProperties index="34" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="16"/>
<atomRef index="32"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="34"/>
<atomRef index="18"/>
<atomRef index="17"/>
</cell>
<cell>
<cellProperties index="35" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="33"/>
<atomRef index="51"/>
<atomRef index="35"/>
<atomRef index="18"/>
</cell>
<cell>
<cellProperties index="36" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="18"/>
<atomRef index="34"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="19"/>
</cell>
<cell>
<cellProperties index="37" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="53"/>
<atomRef index="37"/>
<atomRef index="20"/>
</cell>
<cell>
<cellProperties index="38" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="20"/>
<atomRef index="36"/>
<atomRef index="53"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="38"/>
<atomRef index="22"/>
<atomRef index="21"/>
</cell>
<cell>
<cellProperties index="39" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="37"/>
<atomRef index="55"/>
<atomRef index="39"/>
<atomRef index="22"/>
</cell>
<cell>
<cellProperties index="40" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="22"/>
<atomRef index="38"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="40"/>
<atomRef index="24"/>
<atomRef index="23"/>
</cell>
<cell>
<cellProperties index="41" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="39"/>
<atomRef index="57"/>
<atomRef index="41"/>
<atomRef index="24"/>
</cell>
<cell>
<cellProperties index="42" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="24"/>
<atomRef index="40"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="42"/>
<atomRef index="26"/>
<atomRef index="25"/>
</cell>
<cell>
<cellProperties index="43" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="41"/>
<atomRef index="59"/>
<atomRef index="43"/>
<atomRef index="26"/>
</cell>
<cell>
<cellProperties index="44" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="27"/>
<atomRef index="26"/>
<atomRef index="42"/>
<atomRef index="59"/>
<atomRef index="60"/>
<atomRef index="61"/>
</cell>
<cell>
<cellProperties index="45" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="28"/>
<atomRef index="27"/>
<atomRef index="61"/>
<atomRef index="62"/>
</cell>
<cell>
<cellProperties index="46" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="63"/>
<atomRef index="64"/>
<atomRef index="46"/>
<atomRef index="29"/>
</cell>
<cell>
<cellProperties index="47" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="64"/>
<atomRef index="65"/>
<atomRef index="47"/>
<atomRef index="30"/>
<atomRef index="29"/>
<atomRef index="45"/>
</cell>
<cell>
<cellProperties index="48" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="46"/>
<atomRef index="65"/>
<atomRef index="66"/>
<atomRef index="67"/>
<atomRef index="48"/>
<atomRef index="31"/>
<atomRef index="30"/>
</cell>
<cell>
<cellProperties index="49" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="47"/>
<atomRef index="67"/>
<atomRef index="49"/>
<atomRef index="31"/>
</cell>
<cell>
<cellProperties index="50" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="31"/>
<atomRef index="48"/>
<atomRef index="67"/>
<atomRef index="68"/>
<atomRef index="69"/>
<atomRef index="50"/>
<atomRef index="33"/>
<atomRef index="32"/>
</cell>
<cell>
<cellProperties index="51" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="49"/>
<atomRef index="69"/>
<atomRef index="51"/>
<atomRef index="33"/>
</cell>
<cell>
<cellProperties index="52" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="33"/>
<atomRef index="50"/>
<atomRef index="69"/>
<atomRef index="70"/>
<atomRef index="71"/>
<atomRef index="52"/>
<atomRef index="35"/>
<atomRef index="34"/>
</cell>
<cell>
<cellProperties index="53" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="51"/>
<atomRef index="71"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="54" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="72"/>
<atomRef index="73"/>
<atomRef index="54"/>
<atomRef index="37"/>
<atomRef index="36"/>
</cell>
<cell>
<cellProperties index="55" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="53"/>
<atomRef index="73"/>
<atomRef index="55"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="56" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="37"/>
<atomRef index="54"/>
<atomRef index="73"/>
<atomRef index="74"/>
<atomRef index="75"/>
<atomRef index="56"/>
<atomRef index="39"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="57" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="55"/>
<atomRef index="75"/>
<atomRef index="57"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="58" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="39"/>
<atomRef index="56"/>
<atomRef index="75"/>
<atomRef index="76"/>
<atomRef index="77"/>
<atomRef index="58"/>
<atomRef index="41"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="59" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="57"/>
<atomRef index="77"/>
<atomRef index="59"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="60" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="41"/>
<atomRef index="58"/>
<atomRef index="77"/>
<atomRef index="78"/>
<atomRef index="79"/>
<atomRef index="60"/>
<atomRef index="43"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="61" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="59"/>
<atomRef index="79"/>
<atomRef index="61"/>
<atomRef index="43"/>
</cell>
<cell>
<cellProperties index="62" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="27"/>
<atomRef index="43"/>
<atomRef index="60"/>
<atomRef index="79"/>
<atomRef index="80"/>
<atomRef index="81"/>
<atomRef index="62"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="63" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="44"/>
<atomRef index="61"/>
<atomRef index="81"/>
</cell>
<cell>
<cellProperties index="64" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="82"/>
<atomRef index="83"/>
<atomRef index="64"/>
<atomRef index="45"/>
</cell>
<cell>
<cellProperties index="65" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="63"/>
<atomRef index="83"/>
<atomRef index="65"/>
<atomRef index="45"/>
</cell>
<cell>
<cellProperties index="66" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="64"/>
<atomRef index="83"/>
<atomRef index="84"/>
<atomRef index="85"/>
<atomRef index="66"/>
<atomRef index="47"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="67" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="65"/>
<atomRef index="85"/>
<atomRef index="67"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="68" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="47"/>
<atomRef index="66"/>
<atomRef index="85"/>
<atomRef index="86"/>
<atomRef index="87"/>
<atomRef index="68"/>
<atomRef index="49"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="69" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="67"/>
<atomRef index="87"/>
<atomRef index="69"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="70" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="49"/>
<atomRef index="68"/>
<atomRef index="87"/>
<atomRef index="88"/>
<atomRef index="89"/>
<atomRef index="70"/>
<atomRef index="51"/>
<atomRef index="50"/>
</cell>
<cell>
<cellProperties index="71" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="69"/>
<atomRef index="89"/>
<atomRef index="71"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="72" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="51"/>
<atomRef index="70"/>
<atomRef index="89"/>
<atomRef index="90"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="73" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="91"/>
<atomRef index="73"/>
<atomRef index="53"/>
</cell>
<cell>
<cellProperties index="74" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="53"/>
<atomRef index="72"/>
<atomRef index="91"/>
<atomRef index="92"/>
<atomRef index="93"/>
<atomRef index="74"/>
<atomRef index="55"/>
<atomRef index="54"/>
</cell>
<cell>
<cellProperties index="75" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="73"/>
<atomRef index="93"/>
<atomRef index="75"/>
<atomRef index="55"/>
</cell>
<cell>
<cellProperties index="76" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="55"/>
<atomRef index="74"/>
<atomRef index="93"/>
<atomRef index="94"/>
<atomRef index="95"/>
<atomRef index="76"/>
<atomRef index="57"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="77" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="75"/>
<atomRef index="95"/>
<atomRef index="77"/>
<atomRef index="57"/>
</cell>
<cell>
<cellProperties index="78" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="57"/>
<atomRef index="76"/>
<atomRef index="95"/>
<atomRef index="96"/>
<atomRef index="97"/>
<atomRef index="78"/>
<atomRef index="59"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="79" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="77"/>
<atomRef index="97"/>
<atomRef index="79"/>
<atomRef index="59"/>
</cell>
<cell>
<cellProperties index="80" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="59"/>
<atomRef index="78"/>
<atomRef index="97"/>
<atomRef index="98"/>
<atomRef index="99"/>
<atomRef index="80"/>
<atomRef index="61"/>
<atomRef index="60"/>
</cell>
<cell>
<cellProperties index="81" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="79"/>
<atomRef index="99"/>
<atomRef index="81"/>
<atomRef index="61"/>
</cell>
<cell>
<cellProperties index="82" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="61"/>
<atomRef index="80"/>
<atomRef index="99"/>
<atomRef index="100"/>
<atomRef index="62"/>
</cell>
<cell>
<cellProperties index="83" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="101"/>
<atomRef index="83"/>
<atomRef index="63"/>
</cell>
<cell>
<cellProperties index="84" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="63"/>
<atomRef index="82"/>
<atomRef index="101"/>
<atomRef index="102"/>
<atomRef index="103"/>
<atomRef index="84"/>
<atomRef index="65"/>
<atomRef index="64"/>
</cell>
<cell>
<cellProperties index="85" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="83"/>
<atomRef index="103"/>
<atomRef index="85"/>
<atomRef index="65"/>
</cell>
<cell>
<cellProperties index="86" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="65"/>
<atomRef index="84"/>
<atomRef index="103"/>
<atomRef index="104"/>
<atomRef index="105"/>
<atomRef index="86"/>
<atomRef index="67"/>
<atomRef index="66"/>
</cell>
<cell>
<cellProperties index="87" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="85"/>
<atomRef index="105"/>
<atomRef index="87"/>
<atomRef index="67"/>
</cell>
<cell>
<cellProperties index="88" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="67"/>
<atomRef index="86"/>
<atomRef index="105"/>
<atomRef index="106"/>
<atomRef index="107"/>
<atomRef index="88"/>
<atomRef index="69"/>
<atomRef index="68"/>
</cell>
<cell>
<cellProperties index="89" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="87"/>
<atomRef index="107"/>
<atomRef index="89"/>
<atomRef index="69"/>
</cell>
<cell>
<cellProperties index="90" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="69"/>
<atomRef index="88"/>
<atomRef index="107"/>
<atomRef index="108"/>
<atomRef index="109"/>
<atomRef index="90"/>
<atomRef index="71"/>
<atomRef index="70"/>
</cell>
<cell>
<cellProperties index="91" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="89"/>
<atomRef index="109"/>
<atomRef index="71"/>
</cell>
<cell>
<cellProperties index="92" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="110"/>
<atomRef index="111"/>
<atomRef index="92"/>
<atomRef index="73"/>
<atomRef index="72"/>
</cell>
<cell>
<cellProperties index="93" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="91"/>
<atomRef index="111"/>
<atomRef index="93"/>
<atomRef index="73"/>
</cell>
<cell>
<cellProperties index="94" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="73"/>
<atomRef index="92"/>
<atomRef index="111"/>
<atomRef index="112"/>
<atomRef index="113"/>
<atomRef index="94"/>
<atomRef index="75"/>
<atomRef index="74"/>
</cell>
<cell>
<cellProperties index="95" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="93"/>
<atomRef index="113"/>
<atomRef index="95"/>
<atomRef index="75"/>
</cell>
<cell>
<cellProperties index="96" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="75"/>
<atomRef index="94"/>
<atomRef index="113"/>
<atomRef index="114"/>
<atomRef index="115"/>
<atomRef index="96"/>
<atomRef index="77"/>
<atomRef index="76"/>
</cell>
<cell>
<cellProperties index="97" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="95"/>
<atomRef index="115"/>
<atomRef index="97"/>
<atomRef index="77"/>
</cell>
<cell>
<cellProperties index="98" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="77"/>
<atomRef index="96"/>
<atomRef index="115"/>
<atomRef index="116"/>
<atomRef index="117"/>
<atomRef index="98"/>
<atomRef index="79"/>
<atomRef index="78"/>
</cell>
<cell>
<cellProperties index="99" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="97"/>
<atomRef index="117"/>
<atomRef index="99"/>
<atomRef index="79"/>
</cell>
<cell>
<cellProperties index="100" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="79"/>
<atomRef index="98"/>
<atomRef index="117"/>
<atomRef index="118"/>
<atomRef index="119"/>
<atomRef index="100"/>
<atomRef index="81"/>
<atomRef index="80"/>
</cell>
<cell>
<cellProperties index="101" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="99"/>
<atomRef index="119"/>
<atomRef index="81"/>
</cell>
<cell>
<cellProperties index="102" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="120"/>
<atomRef index="121"/>
<atomRef index="102"/>
<atomRef index="83"/>
<atomRef index="82"/>
</cell>
<cell>
<cellProperties index="103" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="101"/>
<atomRef index="121"/>
<atomRef index="103"/>
<atomRef index="83"/>
</cell>
<cell>
<cellProperties index="104" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="83"/>
<atomRef index="102"/>
<atomRef index="121"/>
<atomRef index="122"/>
<atomRef index="123"/>
<atomRef index="104"/>
<atomRef index="85"/>
<atomRef index="84"/>
</cell>
<cell>
<cellProperties index="105" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="103"/>
<atomRef index="123"/>
<atomRef index="105"/>
<atomRef index="85"/>
</cell>
<cell>
<cellProperties index="106" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="85"/>
<atomRef index="104"/>
<atomRef index="123"/>
<atomRef index="124"/>
<atomRef index="125"/>
<atomRef index="106"/>
<atomRef index="87"/>
<atomRef index="86"/>
</cell>
<cell>
<cellProperties index="107" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="105"/>
<atomRef index="125"/>
<atomRef index="107"/>
<atomRef index="87"/>
</cell>
<cell>
<cellProperties index="108" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="87"/>
<atomRef index="106"/>
<atomRef index="125"/>
<atomRef index="126"/>
<atomRef index="127"/>
<atomRef index="108"/>
<atomRef index="89"/>
<atomRef index="88"/>
</cell>
<cell>
<cellProperties index="109" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="107"/>
<atomRef index="127"/>
<atomRef index="109"/>
<atomRef index="89"/>
</cell>
<cell>
<cellProperties index="110" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="89"/>
<atomRef index="108"/>
<atomRef index="127"/>
<atomRef index="128"/>
<atomRef index="129"/>
<atomRef index="90"/>
</cell>
<cell>
<cellProperties index="111" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="131"/>
<atomRef index="111"/>
<atomRef index="91"/>
</cell>
<cell>
<cellProperties index="112" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="91"/>
<atomRef index="110"/>
<atomRef index="131"/>
<atomRef index="132"/>
<atomRef index="133"/>
<atomRef index="112"/>
<atomRef index="93"/>
<atomRef index="92"/>
</cell>
<cell>
<cellProperties index="113" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="111"/>
<atomRef index="133"/>
<atomRef index="113"/>
<atomRef index="93"/>
</cell>
<cell>
<cellProperties index="114" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="93"/>
<atomRef index="112"/>
<atomRef index="133"/>
<atomRef index="134"/>
<atomRef index="135"/>
<atomRef index="114"/>
<atomRef index="95"/>
<atomRef index="94"/>
</cell>
<cell>
<cellProperties index="115" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="113"/>
<atomRef index="135"/>
<atomRef index="115"/>
<atomRef index="95"/>
</cell>
<cell>
<cellProperties index="116" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="95"/>
<atomRef index="114"/>
<atomRef index="135"/>
<atomRef index="136"/>
<atomRef index="137"/>
<atomRef index="116"/>
<atomRef index="97"/>
<atomRef index="96"/>
</cell>
<cell>
<cellProperties index="117" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="115"/>
<atomRef index="137"/>
<atomRef index="117"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="118" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="97"/>
<atomRef index="116"/>
<atomRef index="137"/>
<atomRef index="138"/>
<atomRef index="139"/>
<atomRef index="118"/>
<atomRef index="99"/>
<atomRef index="98"/>
</cell>
<cell>
<cellProperties index="119" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="117"/>
<atomRef index="139"/>
<atomRef index="119"/>
<atomRef index="99"/>
</cell>
<cell>
<cellProperties index="120" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="99"/>
<atomRef index="118"/>
<atomRef index="139"/>
<atomRef index="140"/>
<atomRef index="100"/>
</cell>
<cell>
<cellProperties index="121" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="141"/>
<atomRef index="121"/>
<atomRef index="101"/>
</cell>
<cell>
<cellProperties index="122" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="142"/>
<atomRef index="143"/>
<atomRef index="122"/>
<atomRef index="103"/>
<atomRef index="102"/>
<atomRef index="101"/>
<atomRef index="120"/>
<atomRef index="141"/>
</cell>
<cell>
<cellProperties index="123" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="121"/>
<atomRef index="143"/>
<atomRef index="123"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="124" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="103"/>
<atomRef index="122"/>
<atomRef index="143"/>
<atomRef index="144"/>
<atomRef index="145"/>
<atomRef index="124"/>
<atomRef index="105"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="125" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="123"/>
<atomRef index="145"/>
<atomRef index="125"/>
<atomRef index="105"/>
</cell>
<cell>
<cellProperties index="126" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="105"/>
<atomRef index="124"/>
<atomRef index="145"/>
<atomRef index="146"/>
<atomRef index="147"/>
<atomRef index="126"/>
<atomRef index="107"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="127" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="125"/>
<atomRef index="147"/>
<atomRef index="127"/>
<atomRef index="107"/>
</cell>
<cell>
<cellProperties index="128" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="107"/>
<atomRef index="126"/>
<atomRef index="147"/>
<atomRef index="148"/>
<atomRef index="149"/>
<atomRef index="128"/>
<atomRef index="109"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="129" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="127"/>
<atomRef index="149"/>
<atomRef index="129"/>
<atomRef index="109"/>
</cell>
<cell>
<cellProperties index="130" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="90"/>
<atomRef index="109"/>
<atomRef index="128"/>
<atomRef index="150"/>
<atomRef index="151"/>
<atomRef index="130"/>
</cell>
<cell>
<cellProperties index="131" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="129"/>
<atomRef index="150"/>
<atomRef index="151"/>
<atomRef index="131"/>
<atomRef index="110"/>
<atomRef index="91"/>
</cell>
<cell>
<cellProperties index="132" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="130"/>
<atomRef index="151"/>
<atomRef index="152"/>
<atomRef index="153"/>
<atomRef index="132"/>
<atomRef index="111"/>
<atomRef index="110"/>
</cell>
<cell>
<cellProperties index="133" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="131"/>
<atomRef index="153"/>
<atomRef index="133"/>
<atomRef index="111"/>
</cell>
<cell>
<cellProperties index="134" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="111"/>
<atomRef index="132"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
<atomRef index="134"/>
<atomRef index="113"/>
<atomRef index="112"/>
</cell>
<cell>
<cellProperties index="135" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="133"/>
<atomRef index="155"/>
<atomRef index="135"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="136" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="113"/>
<atomRef index="134"/>
<atomRef index="155"/>
<atomRef index="156"/>
<atomRef index="157"/>
<atomRef index="136"/>
<atomRef index="115"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="137" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="135"/>
<atomRef index="157"/>
<atomRef index="137"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="138" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="115"/>
<atomRef index="136"/>
<atomRef index="157"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="138"/>
<atomRef index="117"/>
<atomRef index="116"/>
</cell>
<cell>
<cellProperties index="139" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="137"/>
<atomRef index="159"/>
<atomRef index="139"/>
<atomRef index="117"/>
</cell>
<cell>
<cellProperties index="140" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="117"/>
<atomRef index="138"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="140"/>
<atomRef index="119"/>
<atomRef index="118"/>
</cell>
<cell>
<cellProperties index="141" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="139"/>
<atomRef index="161"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="142" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="162"/>
<atomRef index="163"/>
<atomRef index="142"/>
<atomRef index="121"/>
<atomRef index="120"/>
</cell>
<cell>
<cellProperties index="143" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="141"/>
<atomRef index="163"/>
<atomRef index="143"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="144" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="121"/>
<atomRef index="142"/>
<atomRef index="163"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="144"/>
<atomRef index="123"/>
<atomRef index="122"/>
</cell>
<cell>
<cellProperties index="145" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="143"/>
<atomRef index="165"/>
<atomRef index="145"/>
<atomRef index="123"/>
</cell>
<cell>
<cellProperties index="146" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="123"/>
<atomRef index="144"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="146"/>
<atomRef index="125"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="147" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="145"/>
<atomRef index="167"/>
<atomRef index="147"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="148" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="125"/>
<atomRef index="146"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="169"/>
<atomRef index="148"/>
<atomRef index="127"/>
<atomRef index="126"/>
</cell>
<cell>
<cellProperties index="149" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="147"/>
<atomRef index="169"/>
<atomRef index="149"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="150" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="128"/>
<atomRef index="127"/>
<atomRef index="148"/>
<atomRef index="169"/>
<atomRef index="170"/>
<atomRef index="171"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="151" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="129"/>
<atomRef index="128"/>
<atomRef index="149"/>
<atomRef index="171"/>
<atomRef index="151"/>
<atomRef index="130"/>
</cell>
<cell>
<cellProperties index="152" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="150"/>
<atomRef index="171"/>
<atomRef index="172"/>
<atomRef index="173"/>
<atomRef index="152"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="153" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="151"/>
<atomRef index="173"/>
<atomRef index="153"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="154" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="131"/>
<atomRef index="152"/>
<atomRef index="173"/>
<atomRef index="174"/>
<atomRef index="175"/>
<atomRef index="154"/>
<atomRef index="133"/>
<atomRef index="132"/>
</cell>
<cell>
<cellProperties index="155" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="153"/>
<atomRef index="175"/>
<atomRef index="155"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="156" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="133"/>
<atomRef index="154"/>
<atomRef index="175"/>
<atomRef index="176"/>
<atomRef index="177"/>
<atomRef index="156"/>
<atomRef index="135"/>
<atomRef index="134"/>
</cell>
<cell>
<cellProperties index="157" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="155"/>
<atomRef index="177"/>
<atomRef index="157"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="158" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="135"/>
<atomRef index="156"/>
<atomRef index="177"/>
<atomRef index="178"/>
<atomRef index="179"/>
<atomRef index="158"/>
<atomRef index="137"/>
<atomRef index="136"/>
</cell>
<cell>
<cellProperties index="159" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="157"/>
<atomRef index="179"/>
<atomRef index="159"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="160" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="137"/>
<atomRef index="158"/>
<atomRef index="179"/>
<atomRef index="180"/>
<atomRef index="181"/>
<atomRef index="160"/>
<atomRef index="139"/>
<atomRef index="138"/>
</cell>
<cell>
<cellProperties index="161" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="159"/>
<atomRef index="181"/>
<atomRef index="161"/>
<atomRef index="139"/>
</cell>
<cell>
<cellProperties index="162" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="139"/>
<atomRef index="160"/>
<atomRef index="181"/>
<atomRef index="182"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="163" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="183"/>
<atomRef index="163"/>
<atomRef index="141"/>
</cell>
<cell>
<cellProperties index="164" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="204"/>
<atomRef index="184"/>
<atomRef index="164"/>
<atomRef index="143"/>
<atomRef index="142"/>
<atomRef index="141"/>
<atomRef index="162"/>
<atomRef index="183"/>
</cell>
<cell>
<cellProperties index="165" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="163"/>
<atomRef index="184"/>
<atomRef index="165"/>
<atomRef index="143"/>
</cell>
<cell>
<cellProperties index="166" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="143"/>
<atomRef index="164"/>
<atomRef index="184"/>
<atomRef index="185"/>
<atomRef index="186"/>
<atomRef index="166"/>
<atomRef index="145"/>
<atomRef index="144"/>
</cell>
<cell>
<cellProperties index="167" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="165"/>
<atomRef index="186"/>
<atomRef index="167"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="168" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="145"/>
<atomRef index="166"/>
<atomRef index="186"/>
<atomRef index="187"/>
<atomRef index="188"/>
<atomRef index="168"/>
<atomRef index="147"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="169" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="167"/>
<atomRef index="188"/>
<atomRef index="169"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="170" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="147"/>
<atomRef index="168"/>
<atomRef index="188"/>
<atomRef index="189"/>
<atomRef index="190"/>
<atomRef index="170"/>
<atomRef index="149"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="171" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="169"/>
<atomRef index="190"/>
<atomRef index="171"/>
<atomRef index="149"/>
</cell>
<cell>
<cellProperties index="172" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="149"/>
<atomRef index="170"/>
<atomRef index="190"/>
<atomRef index="191"/>
<atomRef index="192"/>
<atomRef index="172"/>
<atomRef index="151"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="173" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="171"/>
<atomRef index="192"/>
<atomRef index="173"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="174" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="151"/>
<atomRef index="172"/>
<atomRef index="192"/>
<atomRef index="193"/>
<atomRef index="194"/>
<atomRef index="174"/>
<atomRef index="153"/>
<atomRef index="152"/>
</cell>
<cell>
<cellProperties index="175" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="173"/>
<atomRef index="194"/>
<atomRef index="175"/>
<atomRef index="153"/>
</cell>
<cell>
<cellProperties index="176" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="153"/>
<atomRef index="174"/>
<atomRef index="194"/>
<atomRef index="195"/>
<atomRef index="196"/>
<atomRef index="176"/>
<atomRef index="155"/>
<atomRef index="154"/>
</cell>
<cell>
<cellProperties index="177" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="175"/>
<atomRef index="196"/>
<atomRef index="177"/>
<atomRef index="155"/>
</cell>
<cell>
<cellProperties index="178" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="155"/>
<atomRef index="176"/>
<atomRef index="196"/>
<atomRef index="197"/>
<atomRef index="198"/>
<atomRef index="178"/>
<atomRef index="157"/>
<atomRef index="156"/>
</cell>
<cell>
<cellProperties index="179" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="177"/>
<atomRef index="198"/>
<atomRef index="179"/>
<atomRef index="157"/>
</cell>
<cell>
<cellProperties index="180" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="157"/>
<atomRef index="178"/>
<atomRef index="198"/>
<atomRef index="199"/>
<atomRef index="200"/>
<atomRef index="180"/>
<atomRef index="159"/>
<atomRef index="158"/>
</cell>
<cell>
<cellProperties index="181" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="179"/>
<atomRef index="200"/>
<atomRef index="181"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="182" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="159"/>
<atomRef index="180"/>
<atomRef index="200"/>
<atomRef index="201"/>
<atomRef index="202"/>
<atomRef index="182"/>
<atomRef index="161"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="183" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="181"/>
<atomRef index="202"/>
<atomRef index="161"/>
</cell>
<cell>
<cellProperties index="184" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="203"/>
<atomRef index="204"/>
<atomRef index="163"/>
<atomRef index="162"/>
</cell>
<cell>
<cellProperties index="185" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="163"/>
<atomRef index="204"/>
<atomRef index="205"/>
<atomRef index="185"/>
<atomRef index="165"/>
<atomRef index="164"/>
</cell>
<cell>
<cellProperties index="186" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="184"/>
<atomRef index="205"/>
<atomRef index="186"/>
<atomRef index="165"/>
</cell>
<cell>
<cellProperties index="187" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="165"/>
<atomRef index="185"/>
<atomRef index="205"/>
<atomRef index="206"/>
<atomRef index="207"/>
<atomRef index="187"/>
<atomRef index="167"/>
<atomRef index="166"/>
</cell>
<cell>
<cellProperties index="188" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="186"/>
<atomRef index="207"/>
<atomRef index="188"/>
<atomRef index="167"/>
</cell>
<cell>
<cellProperties index="189" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="187"/>
<atomRef index="282"/>
<atomRef index="189"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="190" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="188"/>
<atomRef index="208"/>
<atomRef index="190"/>
<atomRef index="169"/>
</cell>
<cell>
<cellProperties index="191" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="169"/>
<atomRef index="189"/>
<atomRef index="208"/>
<atomRef index="209"/>
<atomRef index="210"/>
<atomRef index="191"/>
<atomRef index="171"/>
<atomRef index="170"/>
</cell>
<cell>
<cellProperties index="192" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="190"/>
<atomRef index="210"/>
<atomRef index="192"/>
<atomRef index="171"/>
</cell>
<cell>
<cellProperties index="193" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="171"/>
<atomRef index="191"/>
<atomRef index="210"/>
<atomRef index="211"/>
<atomRef index="212"/>
<atomRef index="193"/>
<atomRef index="173"/>
<atomRef index="172"/>
</cell>
<cell>
<cellProperties index="194" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="192"/>
<atomRef index="212"/>
<atomRef index="194"/>
<atomRef index="173"/>
</cell>
<cell>
<cellProperties index="195" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="195"/>
<atomRef index="175"/>
<atomRef index="174"/>
<atomRef index="173"/>
<atomRef index="193"/>
<atomRef index="212"/>
<atomRef index="213"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="196" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="194"/>
<atomRef index="214"/>
<atomRef index="196"/>
<atomRef index="175"/>
</cell>
<cell>
<cellProperties index="197" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="175"/>
<atomRef index="195"/>
<atomRef index="214"/>
<atomRef index="215"/>
<atomRef index="216"/>
<atomRef index="197"/>
<atomRef index="177"/>
<atomRef index="176"/>
</cell>
<cell>
<cellProperties index="198" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="196"/>
<atomRef index="216"/>
<atomRef index="198"/>
<atomRef index="177"/>
</cell>
<cell>
<cellProperties index="199" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="177"/>
<atomRef index="197"/>
<atomRef index="216"/>
<atomRef index="217"/>
<atomRef index="218"/>
<atomRef index="199"/>
<atomRef index="179"/>
<atomRef index="178"/>
</cell>
<cell>
<cellProperties index="200" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="198"/>
<atomRef index="218"/>
<atomRef index="200"/>
<atomRef index="179"/>
</cell>
<cell>
<cellProperties index="201" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="201"/>
<atomRef index="181"/>
<atomRef index="180"/>
<atomRef index="179"/>
<atomRef index="199"/>
<atomRef index="218"/>
<atomRef index="219"/>
</cell>
<cell>
<cellProperties index="202" type="POLY_VERTEX"/>
<nrOfStructures value="6"/>
<atomRef index="181"/>
<atomRef index="200"/>
<atomRef index="219"/>
<atomRef index="237"/>
<atomRef index="220"/>
<atomRef index="202"/>
</cell>
<cell>
<cellProperties index="203" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="182"/>
<atomRef index="181"/>
<atomRef index="201"/>
<atomRef index="220"/>
</cell>
<cell>
<cellProperties index="204" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="183"/>
<atomRef index="221"/>
<atomRef index="204"/>
</cell>
<cell>
<cellProperties index="205" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="183"/>
<atomRef index="203"/>
<atomRef index="221"/>
<atomRef index="222"/>
<atomRef index="205"/>
<atomRef index="184"/>
<atomRef index="163"/>
</cell>
<cell>
<cellProperties index="206" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="222"/>
<atomRef index="223"/>
<atomRef index="206"/>
<atomRef index="186"/>
<atomRef index="185"/>
<atomRef index="184"/>
<atomRef index="204"/>
</cell>
<cell>
<cellProperties index="207" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="205"/>
<atomRef index="223"/>
<atomRef index="207"/>
<atomRef index="186"/>
</cell>
<cell>
<cellProperties index="208" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="282"/>
<atomRef index="188"/>
<atomRef index="187"/>
<atomRef index="206"/>
<atomRef index="223"/>
<atomRef index="224"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="209" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="188"/>
<atomRef index="282"/>
<atomRef index="225"/>
<atomRef index="226"/>
<atomRef index="227"/>
<atomRef index="209"/>
<atomRef index="190"/>
<atomRef index="189"/>
</cell>
<cell>
<cellProperties index="210" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="208"/>
<atomRef index="227"/>
<atomRef index="210"/>
<atomRef index="190"/>
</cell>
<cell>
<cellProperties index="211" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="190"/>
<atomRef index="209"/>
<atomRef index="227"/>
<atomRef index="228"/>
<atomRef index="229"/>
<atomRef index="211"/>
<atomRef index="192"/>
<atomRef index="191"/>
</cell>
<cell>
<cellProperties index="212" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="210"/>
<atomRef index="229"/>
<atomRef index="212"/>
<atomRef index="192"/>
</cell>
<cell>
<cellProperties index="213" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="192"/>
<atomRef index="211"/>
<atomRef index="229"/>
<atomRef index="230"/>
<atomRef index="231"/>
<atomRef index="213"/>
<atomRef index="194"/>
<atomRef index="193"/>
</cell>
<cell>
<cellProperties index="214" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="212"/>
<atomRef index="231"/>
<atomRef index="214"/>
<atomRef index="194"/>
</cell>
<cell>
<cellProperties index="215" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="194"/>
<atomRef index="213"/>
<atomRef index="231"/>
<atomRef index="232"/>
<atomRef index="233"/>
<atomRef index="215"/>
<atomRef index="196"/>
<atomRef index="195"/>
</cell>
<cell>
<cellProperties index="216" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="214"/>
<atomRef index="233"/>
<atomRef index="216"/>
<atomRef index="196"/>
</cell>
<cell>
<cellProperties index="217" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="196"/>
<atomRef index="215"/>
<atomRef index="233"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="217"/>
<atomRef index="198"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="218" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="216"/>
<atomRef index="235"/>
<atomRef index="218"/>
<atomRef index="198"/>
</cell>
<cell>
<cellProperties index="219" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="198"/>
<atomRef index="217"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="219"/>
<atomRef index="200"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="220" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="201"/>
<atomRef index="200"/>
<atomRef index="218"/>
<atomRef index="237"/>
</cell>
<cell>
<cellProperties index="221" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="202"/>
<atomRef index="201"/>
<atomRef index="237"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="222" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="204"/>
<atomRef index="203"/>
<atomRef index="239"/>
<atomRef index="222"/>
</cell>
<cell>
<cellProperties index="223" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="223"/>
<atomRef index="205"/>
<atomRef index="204"/>
<atomRef index="221"/>
<atomRef index="239"/>
</cell>
<cell>
<cellProperties index="224" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="205"/>
<atomRef index="222"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="224"/>
<atomRef index="207"/>
<atomRef index="206"/>
</cell>
<cell>
<cellProperties index="225" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="223"/>
<atomRef index="241"/>
<atomRef index="225"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="226" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="207"/>
<atomRef index="224"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="243"/>
<atomRef index="226"/>
<atomRef index="208"/>
<atomRef index="282"/>
</cell>
<cell>
<cellProperties index="227" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="225"/>
<atomRef index="243"/>
<atomRef index="227"/>
<atomRef index="208"/>
</cell>
<cell>
<cellProperties index="228" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="208"/>
<atomRef index="226"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="245"/>
<atomRef index="228"/>
<atomRef index="210"/>
<atomRef index="209"/>
</cell>
<cell>
<cellProperties index="229" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="227"/>
<atomRef index="245"/>
<atomRef index="229"/>
<atomRef index="210"/>
</cell>
<cell>
<cellProperties index="230" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="210"/>
<atomRef index="228"/>
<atomRef index="245"/>
<atomRef index="246"/>
<atomRef index="247"/>
<atomRef index="230"/>
<atomRef index="212"/>
<atomRef index="211"/>
</cell>
<cell>
<cellProperties index="231" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="229"/>
<atomRef index="247"/>
<atomRef index="231"/>
<atomRef index="212"/>
</cell>
<cell>
<cellProperties index="232" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="212"/>
<atomRef index="230"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="232"/>
<atomRef index="214"/>
<atomRef index="213"/>
</cell>
<cell>
<cellProperties index="233" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="231"/>
<atomRef index="249"/>
<atomRef index="233"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="234" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="214"/>
<atomRef index="232"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="234"/>
<atomRef index="216"/>
<atomRef index="215"/>
</cell>
<cell>
<cellProperties index="235" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="233"/>
<atomRef index="251"/>
<atomRef index="235"/>
<atomRef index="216"/>
</cell>
<cell>
<cellProperties index="236" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="216"/>
<atomRef index="234"/>
<atomRef index="251"/>
<atomRef index="252"/>
<atomRef index="253"/>
<atomRef index="236"/>
<atomRef index="218"/>
<atomRef index="217"/>
</cell>
<cell>
<cellProperties index="237" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="235"/>
<atomRef index="253"/>
<atomRef index="237"/>
<atomRef index="218"/>
</cell>
<cell>
<cellProperties index="238" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="218"/>
<atomRef index="236"/>
<atomRef index="253"/>
<atomRef index="254"/>
<atomRef index="238"/>
<atomRef index="220"/>
<atomRef index="201"/>
<atomRef index="219"/>
</cell>
<cell>
<cellProperties index="239" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="220"/>
<atomRef index="237"/>
<atomRef index="254"/>
</cell>
<cell>
<cellProperties index="240" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="222"/>
<atomRef index="221"/>
<atomRef index="255"/>
<atomRef index="240"/>
</cell>
<cell>
<cellProperties index="241" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="255"/>
<atomRef index="241"/>
<atomRef index="223"/>
<atomRef index="222"/>
<atomRef index="239"/>
</cell>
<cell>
<cellProperties index="242" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="223"/>
<atomRef index="240"/>
<atomRef index="255"/>
<atomRef index="256"/>
<atomRef index="257"/>
<atomRef index="242"/>
<atomRef index="225"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="243" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="241"/>
<atomRef index="257"/>
<atomRef index="243"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="244" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="225"/>
<atomRef index="242"/>
<atomRef index="257"/>
<atomRef index="258"/>
<atomRef index="259"/>
<atomRef index="244"/>
<atomRef index="227"/>
<atomRef index="226"/>
</cell>
<cell>
<cellProperties index="245" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="243"/>
<atomRef index="259"/>
<atomRef index="245"/>
<atomRef index="227"/>
</cell>
<cell>
<cellProperties index="246" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="227"/>
<atomRef index="244"/>
<atomRef index="259"/>
<atomRef index="260"/>
<atomRef index="261"/>
<atomRef index="246"/>
<atomRef index="229"/>
<atomRef index="228"/>
</cell>
<cell>
<cellProperties index="247" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="245"/>
<atomRef index="261"/>
<atomRef index="247"/>
<atomRef index="229"/>
</cell>
<cell>
<cellProperties index="248" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="229"/>
<atomRef index="246"/>
<atomRef index="261"/>
<atomRef index="262"/>
<atomRef index="263"/>
<atomRef index="248"/>
<atomRef index="231"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="249" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="247"/>
<atomRef index="263"/>
<atomRef index="249"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="250" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="231"/>
<atomRef index="248"/>
<atomRef index="263"/>
<atomRef index="264"/>
<atomRef index="265"/>
<atomRef index="250"/>
<atomRef index="233"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="251" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="249"/>
<atomRef index="265"/>
<atomRef index="251"/>
<atomRef index="233"/>
</cell>
<cell>
<cellProperties index="252" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="233"/>
<atomRef index="250"/>
<atomRef index="265"/>
<atomRef index="266"/>
<atomRef index="267"/>
<atomRef index="252"/>
<atomRef index="235"/>
<atomRef index="234"/>
</cell>
<cell>
<cellProperties index="253" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="251"/>
<atomRef index="267"/>
<atomRef index="253"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="254" type="POLY_VERTEX"/>
<nrOfStructures value="7"/>
<atomRef index="235"/>
<atomRef index="252"/>
<atomRef index="267"/>
<atomRef index="268"/>
<atomRef index="254"/>
<atomRef index="237"/>
<atomRef index="236"/>
</cell>
<cell>
<cellProperties index="255" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="238"/>
<atomRef index="237"/>
<atomRef index="253"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="256" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="239"/>
<atomRef index="269"/>
<atomRef index="256"/>
<atomRef index="241"/>
<atomRef index="240"/>
</cell>
<cell>
<cellProperties index="257" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="255"/>
<atomRef index="269"/>
<atomRef index="257"/>
<atomRef index="241"/>
</cell>
<cell>
<cellProperties index="258" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="241"/>
<atomRef index="256"/>
<atomRef index="269"/>
<atomRef index="270"/>
<atomRef index="271"/>
<atomRef index="258"/>
<atomRef index="243"/>
<atomRef index="242"/>
</cell>
<cell>
<cellProperties index="259" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="257"/>
<atomRef index="271"/>
<atomRef index="259"/>
<atomRef index="243"/>
</cell>
<cell>
<cellProperties index="260" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="243"/>
<atomRef index="258"/>
<atomRef index="271"/>
<atomRef index="272"/>
<atomRef index="273"/>
<atomRef index="260"/>
<atomRef index="245"/>
<atomRef index="244"/>
</cell>
<cell>
<cellProperties index="261" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="259"/>
<atomRef index="273"/>
<atomRef index="261"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="262" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="245"/>
<atomRef index="260"/>
<atomRef index="273"/>
<atomRef index="274"/>
<atomRef index="275"/>
<atomRef index="262"/>
<atomRef index="247"/>
<atomRef index="246"/>
</cell>
<cell>
<cellProperties index="263" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="261"/>
<atomRef index="275"/>
<atomRef index="263"/>
<atomRef index="247"/>
</cell>
<cell>
<cellProperties index="264" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="247"/>
<atomRef index="262"/>
<atomRef index="275"/>
<atomRef index="276"/>
<atomRef index="277"/>
<atomRef index="264"/>
<atomRef index="249"/>
<atomRef index="248"/>
</cell>
<cell>
<cellProperties index="265" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="263"/>
<atomRef index="277"/>
<atomRef index="265"/>
<atomRef index="249"/>
</cell>
<cell>
<cellProperties index="266" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="249"/>
<atomRef index="264"/>
<atomRef index="277"/>
<atomRef index="278"/>
<atomRef index="279"/>
<atomRef index="266"/>
<atomRef index="251"/>
<atomRef index="250"/>
</cell>
<cell>
<cellProperties index="267" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="265"/>
<atomRef index="279"/>
<atomRef index="267"/>
<atomRef index="251"/>
</cell>
<cell>
<cellProperties index="268" type="POLY_VERTEX"/>
<nrOfStructures value="8"/>
<atomRef index="251"/>
<atomRef index="266"/>
<atomRef index="279"/>
<atomRef index="280"/>
<atomRef index="281"/>
<atomRef index="268"/>
<atomRef index="253"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="269" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="254"/>
<atomRef index="253"/>
<atomRef index="267"/>
<atomRef index="281"/>
</cell>
<cell>
<cellProperties index="270" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="255"/>
<atomRef index="270"/>
<atomRef index="257"/>
<atomRef index="256"/>
</cell>
<cell>
<cellProperties index="271" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="269"/>
<atomRef index="271"/>
<atomRef index="257"/>
</cell>
<cell>
<cellProperties index="272" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="257"/>
<atomRef index="270"/>
<atomRef index="272"/>
<atomRef index="259"/>
<atomRef index="258"/>
</cell>
<cell>
<cellProperties index="273" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="271"/>
<atomRef index="273"/>
<atomRef index="259"/>
</cell>
<cell>
<cellProperties index="274" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="259"/>
<atomRef index="272"/>
<atomRef index="274"/>
<atomRef index="261"/>
<atomRef index="260"/>
</cell>
<cell>
<cellProperties index="275" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="273"/>
<atomRef index="275"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="276" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="261"/>
<atomRef index="274"/>
<atomRef index="276"/>
<atomRef index="263"/>
<atomRef index="262"/>
</cell>
<cell>
<cellProperties index="277" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="275"/>
<atomRef index="277"/>
<atomRef index="263"/>
</cell>
<cell>
<cellProperties index="278" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="263"/>
<atomRef index="276"/>
<atomRef index="278"/>
<atomRef index="265"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="279" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="277"/>
<atomRef index="279"/>
<atomRef index="265"/>
</cell>
<cell>
<cellProperties index="280" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="265"/>
<atomRef index="278"/>
<atomRef index="280"/>
<atomRef index="267"/>
<atomRef index="266"/>
</cell>
<cell>
<cellProperties index="281" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="279"/>
<atomRef index="281"/>
<atomRef index="267"/>
</cell>
<cell>
<cellProperties index="282" type="POLY_VERTEX"/>
<nrOfStructures value="3"/>
<atomRef index="267"/>
<atomRef index="280"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="283" type="POLY_VERTEX"/>
<nrOfStructures value="4"/>
<atomRef index="188"/>
<atomRef index="207"/>
<atomRef index="225"/>
<atomRef index="208"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #0 (initialMatrix)" mode="WIREFRAME_AND_SURFACE_AND_POINTS">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="483"/>
<cell>
<cellProperties index="284" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="7"/>
<atomRef index="17"/>
</cell>
<cell>
<cellProperties index="285" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="17"/>
<atomRef index="18"/>
</cell>
<cell>
<cellProperties index="286" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="18"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="287" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="9"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="288" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="10"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="289" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="10"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="290" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="10"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="291" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="12"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="292" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="12"/>
<atomRef index="24"/>
</cell>
<cell>
<cellProperties index="293" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="24"/>
<atomRef index="13"/>
</cell>
<cell>
<cellProperties index="294" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="14"/>
<atomRef index="15"/>
</cell>
<cell>
<cellProperties index="295" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="15"/>
<atomRef index="16"/>
</cell>
<cell>
<cellProperties index="296" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="16"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="297" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="16"/>
<atomRef index="17"/>
</cell>
<cell>
<cellProperties index="298" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="18"/>
<atomRef index="19"/>
</cell>
<cell>
<cellProperties index="299" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="20"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="300" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="20"/>
<atomRef index="21"/>
</cell>
<cell>
<cellProperties index="301" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="21"/>
<atomRef index="22"/>
</cell>
<cell>
<cellProperties index="302" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="22"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="303" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="11"/>
<atomRef index="22"/>
<atomRef index="12"/>
</cell>
<cell>
<cellProperties index="304" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="12"/>
<atomRef index="22"/>
<atomRef index="23"/>
</cell>
<cell>
<cellProperties index="305" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="12"/>
<atomRef index="23"/>
<atomRef index="24"/>
</cell>
<cell>
<cellProperties index="306" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="13"/>
<atomRef index="24"/>
<atomRef index="25"/>
</cell>
<cell>
<cellProperties index="307" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="13"/>
<atomRef index="25"/>
<atomRef index="26"/>
</cell>
<cell>
<cellProperties index="308" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="14"/>
<atomRef index="29"/>
<atomRef index="30"/>
</cell>
<cell>
<cellProperties index="309" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="14"/>
<atomRef index="30"/>
<atomRef index="15"/>
</cell>
<cell>
<cellProperties index="310" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="15"/>
<atomRef index="30"/>
<atomRef index="31"/>
</cell>
<cell>
<cellProperties index="311" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="15"/>
<atomRef index="31"/>
<atomRef index="16"/>
</cell>
<cell>
<cellProperties index="312" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="16"/>
<atomRef index="31"/>
<atomRef index="32"/>
</cell>
<cell>
<cellProperties index="313" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="16"/>
<atomRef index="32"/>
<atomRef index="33"/>
</cell>
<cell>
<cellProperties index="314" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="16"/>
<atomRef index="33"/>
<atomRef index="17"/>
</cell>
<cell>
<cellProperties index="315" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="17"/>
<atomRef index="33"/>
<atomRef index="18"/>
</cell>
<cell>
<cellProperties index="316" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="18"/>
<atomRef index="33"/>
<atomRef index="34"/>
</cell>
<cell>
<cellProperties index="317" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="18"/>
<atomRef index="34"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="318" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="18"/>
<atomRef index="35"/>
<atomRef index="19"/>
</cell>
<cell>
<cellProperties index="319" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="20"/>
<atomRef index="36"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="320" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="20"/>
<atomRef index="37"/>
<atomRef index="21"/>
</cell>
<cell>
<cellProperties index="321" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="21"/>
<atomRef index="37"/>
<atomRef index="22"/>
</cell>
<cell>
<cellProperties index="322" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="22"/>
<atomRef index="37"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="323" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="22"/>
<atomRef index="38"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="324" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="22"/>
<atomRef index="39"/>
<atomRef index="23"/>
</cell>
<cell>
<cellProperties index="325" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="23"/>
<atomRef index="39"/>
<atomRef index="24"/>
</cell>
<cell>
<cellProperties index="326" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="24"/>
<atomRef index="39"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="327" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="24"/>
<atomRef index="40"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="328" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="24"/>
<atomRef index="41"/>
<atomRef index="25"/>
</cell>
<cell>
<cellProperties index="329" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="25"/>
<atomRef index="41"/>
<atomRef index="26"/>
</cell>
<cell>
<cellProperties index="330" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="26"/>
<atomRef index="41"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="331" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="26"/>
<atomRef index="42"/>
<atomRef index="43"/>
</cell>
<cell>
<cellProperties index="332" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="26"/>
<atomRef index="43"/>
<atomRef index="27"/>
</cell>
<cell>
<cellProperties index="333" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="27"/>
<atomRef index="43"/>
<atomRef index="61"/>
</cell>
<cell>
<cellProperties index="334" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="27"/>
<atomRef index="61"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="335" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="27"/>
<atomRef index="44"/>
<atomRef index="28"/>
</cell>
<cell>
<cellProperties index="336" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="45"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="337" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="46"/>
<atomRef index="30"/>
</cell>
<cell>
<cellProperties index="338" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="30"/>
<atomRef index="46"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="339" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="30"/>
<atomRef index="47"/>
<atomRef index="31"/>
</cell>
<cell>
<cellProperties index="340" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="47"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="341" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="48"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="342" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="49"/>
<atomRef index="32"/>
</cell>
<cell>
<cellProperties index="343" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="32"/>
<atomRef index="49"/>
<atomRef index="33"/>
</cell>
<cell>
<cellProperties index="344" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="49"/>
<atomRef index="50"/>
</cell>
<cell>
<cellProperties index="345" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="50"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="346" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="51"/>
<atomRef index="34"/>
</cell>
<cell>
<cellProperties index="347" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="34"/>
<atomRef index="51"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="348" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="35"/>
<atomRef index="51"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="349" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="36"/>
<atomRef index="53"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="350" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="37"/>
<atomRef index="53"/>
<atomRef index="54"/>
</cell>
<cell>
<cellProperties index="351" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="37"/>
<atomRef index="54"/>
<atomRef index="55"/>
</cell>
<cell>
<cellProperties index="352" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="37"/>
<atomRef index="55"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="353" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="38"/>
<atomRef index="55"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="354" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="39"/>
<atomRef index="55"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="355" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="39"/>
<atomRef index="56"/>
<atomRef index="57"/>
</cell>
<cell>
<cellProperties index="356" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="39"/>
<atomRef index="57"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="357" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="40"/>
<atomRef index="57"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="358" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="41"/>
<atomRef index="57"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="359" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="41"/>
<atomRef index="58"/>
<atomRef index="59"/>
</cell>
<cell>
<cellProperties index="360" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="41"/>
<atomRef index="59"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="361" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="42"/>
<atomRef index="59"/>
<atomRef index="43"/>
</cell>
<cell>
<cellProperties index="362" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="43"/>
<atomRef index="59"/>
<atomRef index="60"/>
</cell>
<cell>
<cellProperties index="363" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="43"/>
<atomRef index="60"/>
<atomRef index="61"/>
</cell>
<cell>
<cellProperties index="364" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="44"/>
<atomRef index="61"/>
<atomRef index="62"/>
</cell>
<cell>
<cellProperties index="365" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="45"/>
<atomRef index="63"/>
<atomRef index="64"/>
</cell>
<cell>
<cellProperties index="366" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="46"/>
<atomRef index="64"/>
<atomRef index="65"/>
</cell>
<cell>
<cellProperties index="367" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="46"/>
<atomRef index="65"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="368" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="47"/>
<atomRef index="65"/>
<atomRef index="66"/>
</cell>
<cell>
<cellProperties index="369" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="47"/>
<atomRef index="66"/>
<atomRef index="67"/>
</cell>
<cell>
<cellProperties index="370" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="47"/>
<atomRef index="67"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="371" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="48"/>
<atomRef index="67"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="372" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="49"/>
<atomRef index="67"/>
<atomRef index="68"/>
</cell>
<cell>
<cellProperties index="373" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="49"/>
<atomRef index="68"/>
<atomRef index="69"/>
</cell>
<cell>
<cellProperties index="374" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="49"/>
<atomRef index="69"/>
<atomRef index="50"/>
</cell>
<cell>
<cellProperties index="375" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="50"/>
<atomRef index="69"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="376" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="51"/>
<atomRef index="69"/>
<atomRef index="70"/>
</cell>
<cell>
<cellProperties index="377" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="51"/>
<atomRef index="70"/>
<atomRef index="71"/>
</cell>
<cell>
<cellProperties index="378" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="51"/>
<atomRef index="71"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="379" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="53"/>
<atomRef index="72"/>
<atomRef index="73"/>
</cell>
<cell>
<cellProperties index="380" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="53"/>
<atomRef index="73"/>
<atomRef index="54"/>
</cell>
<cell>
<cellProperties index="381" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="54"/>
<atomRef index="73"/>
<atomRef index="55"/>
</cell>
<cell>
<cellProperties index="382" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="55"/>
<atomRef index="73"/>
<atomRef index="74"/>
</cell>
<cell>
<cellProperties index="383" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="55"/>
<atomRef index="74"/>
<atomRef index="75"/>
</cell>
<cell>
<cellProperties index="384" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="55"/>
<atomRef index="75"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="385" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="56"/>
<atomRef index="75"/>
<atomRef index="57"/>
</cell>
<cell>
<cellProperties index="386" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="57"/>
<atomRef index="75"/>
<atomRef index="76"/>
</cell>
<cell>
<cellProperties index="387" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="57"/>
<atomRef index="76"/>
<atomRef index="77"/>
</cell>
<cell>
<cellProperties index="388" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="57"/>
<atomRef index="77"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="389" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="58"/>
<atomRef index="77"/>
<atomRef index="59"/>
</cell>
<cell>
<cellProperties index="390" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="59"/>
<atomRef index="77"/>
<atomRef index="78"/>
</cell>
<cell>
<cellProperties index="391" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="59"/>
<atomRef index="78"/>
<atomRef index="79"/>
</cell>
<cell>
<cellProperties index="392" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="59"/>
<atomRef index="79"/>
<atomRef index="60"/>
</cell>
<cell>
<cellProperties index="393" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="60"/>
<atomRef index="79"/>
<atomRef index="61"/>
</cell>
<cell>
<cellProperties index="394" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="61"/>
<atomRef index="79"/>
<atomRef index="80"/>
</cell>
<cell>
<cellProperties index="395" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="61"/>
<atomRef index="80"/>
<atomRef index="81"/>
</cell>
<cell>
<cellProperties index="396" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="61"/>
<atomRef index="81"/>
<atomRef index="62"/>
</cell>
<cell>
<cellProperties index="397" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="63"/>
<atomRef index="82"/>
<atomRef index="83"/>
</cell>
<cell>
<cellProperties index="398" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="63"/>
<atomRef index="83"/>
<atomRef index="64"/>
</cell>
<cell>
<cellProperties index="399" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="64"/>
<atomRef index="83"/>
<atomRef index="65"/>
</cell>
<cell>
<cellProperties index="400" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="65"/>
<atomRef index="83"/>
<atomRef index="84"/>
</cell>
<cell>
<cellProperties index="401" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="65"/>
<atomRef index="84"/>
<atomRef index="85"/>
</cell>
<cell>
<cellProperties index="402" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="65"/>
<atomRef index="85"/>
<atomRef index="66"/>
</cell>
<cell>
<cellProperties index="403" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="66"/>
<atomRef index="85"/>
<atomRef index="67"/>
</cell>
<cell>
<cellProperties index="404" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="67"/>
<atomRef index="85"/>
<atomRef index="86"/>
</cell>
<cell>
<cellProperties index="405" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="67"/>
<atomRef index="86"/>
<atomRef index="87"/>
</cell>
<cell>
<cellProperties index="406" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="67"/>
<atomRef index="87"/>
<atomRef index="68"/>
</cell>
<cell>
<cellProperties index="407" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="68"/>
<atomRef index="87"/>
<atomRef index="69"/>
</cell>
<cell>
<cellProperties index="408" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="69"/>
<atomRef index="87"/>
<atomRef index="88"/>
</cell>
<cell>
<cellProperties index="409" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="69"/>
<atomRef index="88"/>
<atomRef index="89"/>
</cell>
<cell>
<cellProperties index="410" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="69"/>
<atomRef index="89"/>
<atomRef index="70"/>
</cell>
<cell>
<cellProperties index="411" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="70"/>
<atomRef index="89"/>
<atomRef index="71"/>
</cell>
<cell>
<cellProperties index="412" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="71"/>
<atomRef index="89"/>
<atomRef index="90"/>
</cell>
<cell>
<cellProperties index="413" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="72"/>
<atomRef index="91"/>
<atomRef index="73"/>
</cell>
<cell>
<cellProperties index="414" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="73"/>
<atomRef index="91"/>
<atomRef index="92"/>
</cell>
<cell>
<cellProperties index="415" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="73"/>
<atomRef index="92"/>
<atomRef index="93"/>
</cell>
<cell>
<cellProperties index="416" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="73"/>
<atomRef index="93"/>
<atomRef index="74"/>
</cell>
<cell>
<cellProperties index="417" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="74"/>
<atomRef index="93"/>
<atomRef index="75"/>
</cell>
<cell>
<cellProperties index="418" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="75"/>
<atomRef index="93"/>
<atomRef index="94"/>
</cell>
<cell>
<cellProperties index="419" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="75"/>
<atomRef index="94"/>
<atomRef index="95"/>
</cell>
<cell>
<cellProperties index="420" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="75"/>
<atomRef index="95"/>
<atomRef index="76"/>
</cell>
<cell>
<cellProperties index="421" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="76"/>
<atomRef index="95"/>
<atomRef index="77"/>
</cell>
<cell>
<cellProperties index="422" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="77"/>
<atomRef index="95"/>
<atomRef index="96"/>
</cell>
<cell>
<cellProperties index="423" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="77"/>
<atomRef index="96"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="424" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="77"/>
<atomRef index="97"/>
<atomRef index="78"/>
</cell>
<cell>
<cellProperties index="425" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="78"/>
<atomRef index="97"/>
<atomRef index="79"/>
</cell>
<cell>
<cellProperties index="426" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="79"/>
<atomRef index="97"/>
<atomRef index="98"/>
</cell>
<cell>
<cellProperties index="427" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="79"/>
<atomRef index="98"/>
<atomRef index="99"/>
</cell>
<cell>
<cellProperties index="428" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="79"/>
<atomRef index="99"/>
<atomRef index="80"/>
</cell>
<cell>
<cellProperties index="429" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="80"/>
<atomRef index="99"/>
<atomRef index="81"/>
</cell>
<cell>
<cellProperties index="430" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="81"/>
<atomRef index="99"/>
<atomRef index="100"/>
</cell>
<cell>
<cellProperties index="431" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="82"/>
<atomRef index="101"/>
<atomRef index="83"/>
</cell>
<cell>
<cellProperties index="432" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="83"/>
<atomRef index="101"/>
<atomRef index="102"/>
</cell>
<cell>
<cellProperties index="433" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="83"/>
<atomRef index="102"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="434" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="83"/>
<atomRef index="103"/>
<atomRef index="84"/>
</cell>
<cell>
<cellProperties index="435" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="84"/>
<atomRef index="103"/>
<atomRef index="85"/>
</cell>
<cell>
<cellProperties index="436" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="85"/>
<atomRef index="103"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="437" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="85"/>
<atomRef index="104"/>
<atomRef index="105"/>
</cell>
<cell>
<cellProperties index="438" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="85"/>
<atomRef index="105"/>
<atomRef index="86"/>
</cell>
<cell>
<cellProperties index="439" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="86"/>
<atomRef index="105"/>
<atomRef index="87"/>
</cell>
<cell>
<cellProperties index="440" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="87"/>
<atomRef index="105"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="441" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="87"/>
<atomRef index="106"/>
<atomRef index="107"/>
</cell>
<cell>
<cellProperties index="442" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="87"/>
<atomRef index="107"/>
<atomRef index="88"/>
</cell>
<cell>
<cellProperties index="443" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="88"/>
<atomRef index="107"/>
<atomRef index="89"/>
</cell>
<cell>
<cellProperties index="444" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="89"/>
<atomRef index="107"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="445" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="89"/>
<atomRef index="108"/>
<atomRef index="109"/>
</cell>
<cell>
<cellProperties index="446" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="89"/>
<atomRef index="109"/>
<atomRef index="90"/>
</cell>
<cell>
<cellProperties index="447" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="91"/>
<atomRef index="110"/>
<atomRef index="111"/>
</cell>
<cell>
<cellProperties index="448" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="91"/>
<atomRef index="111"/>
<atomRef index="92"/>
</cell>
<cell>
<cellProperties index="449" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="92"/>
<atomRef index="111"/>
<atomRef index="93"/>
</cell>
<cell>
<cellProperties index="450" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="93"/>
<atomRef index="111"/>
<atomRef index="112"/>
</cell>
<cell>
<cellProperties index="451" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="93"/>
<atomRef index="112"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="452" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="93"/>
<atomRef index="113"/>
<atomRef index="94"/>
</cell>
<cell>
<cellProperties index="453" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="94"/>
<atomRef index="113"/>
<atomRef index="95"/>
</cell>
<cell>
<cellProperties index="454" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="95"/>
<atomRef index="113"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="455" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="95"/>
<atomRef index="114"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="456" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="95"/>
<atomRef index="115"/>
<atomRef index="96"/>
</cell>
<cell>
<cellProperties index="457" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="96"/>
<atomRef index="115"/>
<atomRef index="97"/>
</cell>
<cell>
<cellProperties index="458" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="97"/>
<atomRef index="115"/>
<atomRef index="116"/>
</cell>
<cell>
<cellProperties index="459" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="97"/>
<atomRef index="116"/>
<atomRef index="117"/>
</cell>
<cell>
<cellProperties index="460" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="97"/>
<atomRef index="117"/>
<atomRef index="98"/>
</cell>
<cell>
<cellProperties index="461" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="98"/>
<atomRef index="117"/>
<atomRef index="99"/>
</cell>
<cell>
<cellProperties index="462" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="99"/>
<atomRef index="117"/>
<atomRef index="118"/>
</cell>
<cell>
<cellProperties index="463" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="99"/>
<atomRef index="118"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="464" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="99"/>
<atomRef index="119"/>
<atomRef index="100"/>
</cell>
<cell>
<cellProperties index="465" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="101"/>
<atomRef index="120"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="466" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="101"/>
<atomRef index="121"/>
<atomRef index="102"/>
</cell>
<cell>
<cellProperties index="467" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="102"/>
<atomRef index="121"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="468" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="103"/>
<atomRef index="121"/>
<atomRef index="122"/>
</cell>
<cell>
<cellProperties index="469" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="103"/>
<atomRef index="122"/>
<atomRef index="123"/>
</cell>
<cell>
<cellProperties index="470" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="103"/>
<atomRef index="123"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="471" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="104"/>
<atomRef index="123"/>
<atomRef index="105"/>
</cell>
<cell>
<cellProperties index="472" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="123"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="473" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="124"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="474" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="125"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="475" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="106"/>
<atomRef index="125"/>
<atomRef index="107"/>
</cell>
<cell>
<cellProperties index="476" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="125"/>
<atomRef index="126"/>
</cell>
<cell>
<cellProperties index="477" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="126"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="478" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="127"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="479" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="127"/>
<atomRef index="109"/>
</cell>
<cell>
<cellProperties index="480" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="109"/>
<atomRef index="127"/>
<atomRef index="128"/>
</cell>
<cell>
<cellProperties index="481" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="109"/>
<atomRef index="128"/>
<atomRef index="129"/>
</cell>
<cell>
<cellProperties index="482" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="110"/>
<atomRef index="131"/>
<atomRef index="111"/>
</cell>
<cell>
<cellProperties index="483" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="111"/>
<atomRef index="131"/>
<atomRef index="132"/>
</cell>
<cell>
<cellProperties index="484" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="111"/>
<atomRef index="132"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="485" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="111"/>
<atomRef index="133"/>
<atomRef index="112"/>
</cell>
<cell>
<cellProperties index="486" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="112"/>
<atomRef index="133"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="487" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="113"/>
<atomRef index="133"/>
<atomRef index="134"/>
</cell>
<cell>
<cellProperties index="488" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="113"/>
<atomRef index="134"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="489" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="113"/>
<atomRef index="135"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="490" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="114"/>
<atomRef index="135"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="491" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="115"/>
<atomRef index="135"/>
<atomRef index="136"/>
</cell>
<cell>
<cellProperties index="492" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="115"/>
<atomRef index="136"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="493" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="115"/>
<atomRef index="137"/>
<atomRef index="116"/>
</cell>
<cell>
<cellProperties index="494" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="116"/>
<atomRef index="137"/>
<atomRef index="117"/>
</cell>
<cell>
<cellProperties index="495" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="117"/>
<atomRef index="137"/>
<atomRef index="138"/>
</cell>
<cell>
<cellProperties index="496" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="117"/>
<atomRef index="138"/>
<atomRef index="139"/>
</cell>
<cell>
<cellProperties index="497" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="117"/>
<atomRef index="139"/>
<atomRef index="118"/>
</cell>
<cell>
<cellProperties index="498" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="118"/>
<atomRef index="139"/>
<atomRef index="119"/>
</cell>
<cell>
<cellProperties index="499" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="119"/>
<atomRef index="139"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="500" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="120"/>
<atomRef index="141"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="501" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="121"/>
<atomRef index="142"/>
<atomRef index="143"/>
</cell>
<cell>
<cellProperties index="502" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="121"/>
<atomRef index="143"/>
<atomRef index="122"/>
</cell>
<cell>
<cellProperties index="503" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="121"/>
<atomRef index="141"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="504" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="122"/>
<atomRef index="143"/>
<atomRef index="123"/>
</cell>
<cell>
<cellProperties index="505" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="123"/>
<atomRef index="143"/>
<atomRef index="144"/>
</cell>
<cell>
<cellProperties index="506" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="123"/>
<atomRef index="144"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="507" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="123"/>
<atomRef index="145"/>
<atomRef index="124"/>
</cell>
<cell>
<cellProperties index="508" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="124"/>
<atomRef index="145"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="509" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="125"/>
<atomRef index="145"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="510" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="125"/>
<atomRef index="146"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="511" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="125"/>
<atomRef index="147"/>
<atomRef index="126"/>
</cell>
<cell>
<cellProperties index="512" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="126"/>
<atomRef index="147"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="513" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="127"/>
<atomRef index="147"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="514" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="127"/>
<atomRef index="148"/>
<atomRef index="149"/>
</cell>
<cell>
<cellProperties index="515" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="127"/>
<atomRef index="149"/>
<atomRef index="128"/>
</cell>
<cell>
<cellProperties index="516" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="129"/>
<atomRef index="150"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="517" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="130"/>
<atomRef index="150"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="518" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="130"/>
<atomRef index="151"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="519" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="131"/>
<atomRef index="151"/>
<atomRef index="152"/>
</cell>
<cell>
<cellProperties index="520" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="131"/>
<atomRef index="152"/>
<atomRef index="153"/>
</cell>
<cell>
<cellProperties index="521" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="131"/>
<atomRef index="153"/>
<atomRef index="132"/>
</cell>
<cell>
<cellProperties index="522" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="132"/>
<atomRef index="153"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="523" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="133"/>
<atomRef index="153"/>
<atomRef index="154"/>
</cell>
<cell>
<cellProperties index="524" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="133"/>
<atomRef index="154"/>
<atomRef index="155"/>
</cell>
<cell>
<cellProperties index="525" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="133"/>
<atomRef index="155"/>
<atomRef index="134"/>
</cell>
<cell>
<cellProperties index="526" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="134"/>
<atomRef index="155"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="527" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="135"/>
<atomRef index="155"/>
<atomRef index="156"/>
</cell>
<cell>
<cellProperties index="528" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="135"/>
<atomRef index="156"/>
<atomRef index="157"/>
</cell>
<cell>
<cellProperties index="529" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="135"/>
<atomRef index="157"/>
<atomRef index="136"/>
</cell>
<cell>
<cellProperties index="530" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="136"/>
<atomRef index="157"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="531" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="137"/>
<atomRef index="157"/>
<atomRef index="158"/>
</cell>
<cell>
<cellProperties index="532" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="137"/>
<atomRef index="158"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="533" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="137"/>
<atomRef index="159"/>
<atomRef index="138"/>
</cell>
<cell>
<cellProperties index="534" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="138"/>
<atomRef index="159"/>
<atomRef index="139"/>
</cell>
<cell>
<cellProperties index="535" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="139"/>
<atomRef index="159"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="536" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="139"/>
<atomRef index="160"/>
<atomRef index="161"/>
</cell>
<cell>
<cellProperties index="537" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="139"/>
<atomRef index="161"/>
<atomRef index="140"/>
</cell>
<cell>
<cellProperties index="538" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="141"/>
<atomRef index="162"/>
<atomRef index="163"/>
</cell>
<cell>
<cellProperties index="539" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="141"/>
<atomRef index="163"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="540" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="142"/>
<atomRef index="163"/>
<atomRef index="143"/>
</cell>
<cell>
<cellProperties index="541" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="143"/>
<atomRef index="163"/>
<atomRef index="164"/>
</cell>
<cell>
<cellProperties index="542" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="143"/>
<atomRef index="164"/>
<atomRef index="165"/>
</cell>
<cell>
<cellProperties index="543" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="143"/>
<atomRef index="165"/>
<atomRef index="144"/>
</cell>
<cell>
<cellProperties index="544" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="144"/>
<atomRef index="165"/>
<atomRef index="145"/>
</cell>
<cell>
<cellProperties index="545" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="145"/>
<atomRef index="165"/>
<atomRef index="166"/>
</cell>
<cell>
<cellProperties index="546" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="145"/>
<atomRef index="166"/>
<atomRef index="167"/>
</cell>
<cell>
<cellProperties index="547" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="145"/>
<atomRef index="167"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="548" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="146"/>
<atomRef index="167"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="549" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="147"/>
<atomRef index="167"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="550" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="147"/>
<atomRef index="168"/>
<atomRef index="169"/>
</cell>
<cell>
<cellProperties index="551" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="147"/>
<atomRef index="169"/>
<atomRef index="148"/>
</cell>
<cell>
<cellProperties index="552" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="148"/>
<atomRef index="169"/>
<atomRef index="149"/>
</cell>
<cell>
<cellProperties index="553" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="149"/>
<atomRef index="169"/>
<atomRef index="170"/>
</cell>
<cell>
<cellProperties index="554" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="149"/>
<atomRef index="170"/>
<atomRef index="171"/>
</cell>
<cell>
<cellProperties index="555" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="149"/>
<atomRef index="171"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="556" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="150"/>
<atomRef index="171"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="557" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="151"/>
<atomRef index="171"/>
<atomRef index="172"/>
</cell>
<cell>
<cellProperties index="558" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="151"/>
<atomRef index="172"/>
<atomRef index="173"/>
</cell>
<cell>
<cellProperties index="559" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="151"/>
<atomRef index="173"/>
<atomRef index="152"/>
</cell>
<cell>
<cellProperties index="560" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="152"/>
<atomRef index="173"/>
<atomRef index="153"/>
</cell>
<cell>
<cellProperties index="561" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="153"/>
<atomRef index="173"/>
<atomRef index="174"/>
</cell>
<cell>
<cellProperties index="562" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="153"/>
<atomRef index="174"/>
<atomRef index="175"/>
</cell>
<cell>
<cellProperties index="563" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="153"/>
<atomRef index="175"/>
<atomRef index="154"/>
</cell>
<cell>
<cellProperties index="564" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="154"/>
<atomRef index="175"/>
<atomRef index="155"/>
</cell>
<cell>
<cellProperties index="565" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="155"/>
<atomRef index="175"/>
<atomRef index="176"/>
</cell>
<cell>
<cellProperties index="566" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="155"/>
<atomRef index="176"/>
<atomRef index="177"/>
</cell>
<cell>
<cellProperties index="567" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="155"/>
<atomRef index="177"/>
<atomRef index="156"/>
</cell>
<cell>
<cellProperties index="568" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="156"/>
<atomRef index="177"/>
<atomRef index="157"/>
</cell>
<cell>
<cellProperties index="569" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="157"/>
<atomRef index="177"/>
<atomRef index="178"/>
</cell>
<cell>
<cellProperties index="570" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="157"/>
<atomRef index="178"/>
<atomRef index="179"/>
</cell>
<cell>
<cellProperties index="571" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="157"/>
<atomRef index="179"/>
<atomRef index="158"/>
</cell>
<cell>
<cellProperties index="572" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="158"/>
<atomRef index="179"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="573" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="159"/>
<atomRef index="179"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="574" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="159"/>
<atomRef index="180"/>
<atomRef index="181"/>
</cell>
<cell>
<cellProperties index="575" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="159"/>
<atomRef index="181"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="576" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="160"/>
<atomRef index="181"/>
<atomRef index="161"/>
</cell>
<cell>
<cellProperties index="577" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="161"/>
<atomRef index="181"/>
<atomRef index="182"/>
</cell>
<cell>
<cellProperties index="578" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="162"/>
<atomRef index="183"/>
<atomRef index="163"/>
</cell>
<cell>
<cellProperties index="579" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="163"/>
<atomRef index="204"/>
<atomRef index="184"/>
</cell>
<cell>
<cellProperties index="580" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="163"/>
<atomRef index="184"/>
<atomRef index="164"/>
</cell>
<cell>
<cellProperties index="581" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="163"/>
<atomRef index="183"/>
<atomRef index="204"/>
</cell>
<cell>
<cellProperties index="582" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="164"/>
<atomRef index="184"/>
<atomRef index="165"/>
</cell>
<cell>
<cellProperties index="583" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="165"/>
<atomRef index="184"/>
<atomRef index="185"/>
</cell>
<cell>
<cellProperties index="584" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="165"/>
<atomRef index="185"/>
<atomRef index="186"/>
</cell>
<cell>
<cellProperties index="585" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="165"/>
<atomRef index="186"/>
<atomRef index="166"/>
</cell>
<cell>
<cellProperties index="586" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="166"/>
<atomRef index="186"/>
<atomRef index="167"/>
</cell>
<cell>
<cellProperties index="587" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="167"/>
<atomRef index="186"/>
<atomRef index="187"/>
</cell>
<cell>
<cellProperties index="588" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="167"/>
<atomRef index="187"/>
<atomRef index="188"/>
</cell>
<cell>
<cellProperties index="589" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="167"/>
<atomRef index="188"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="590" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="169"/>
<atomRef index="188"/>
<atomRef index="189"/>
</cell>
<cell>
<cellProperties index="591" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="169"/>
<atomRef index="189"/>
<atomRef index="190"/>
</cell>
<cell>
<cellProperties index="592" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="169"/>
<atomRef index="190"/>
<atomRef index="170"/>
</cell>
<cell>
<cellProperties index="593" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="170"/>
<atomRef index="190"/>
<atomRef index="171"/>
</cell>
<cell>
<cellProperties index="594" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="171"/>
<atomRef index="190"/>
<atomRef index="191"/>
</cell>
<cell>
<cellProperties index="595" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="171"/>
<atomRef index="191"/>
<atomRef index="192"/>
</cell>
<cell>
<cellProperties index="596" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="171"/>
<atomRef index="192"/>
<atomRef index="172"/>
</cell>
<cell>
<cellProperties index="597" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="172"/>
<atomRef index="192"/>
<atomRef index="173"/>
</cell>
<cell>
<cellProperties index="598" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="173"/>
<atomRef index="192"/>
<atomRef index="193"/>
</cell>
<cell>
<cellProperties index="599" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="173"/>
<atomRef index="193"/>
<atomRef index="194"/>
</cell>
<cell>
<cellProperties index="600" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="173"/>
<atomRef index="194"/>
<atomRef index="174"/>
</cell>
<cell>
<cellProperties index="601" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="174"/>
<atomRef index="194"/>
<atomRef index="175"/>
</cell>
<cell>
<cellProperties index="602" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="175"/>
<atomRef index="194"/>
<atomRef index="195"/>
</cell>
<cell>
<cellProperties index="603" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="175"/>
<atomRef index="195"/>
<atomRef index="196"/>
</cell>
<cell>
<cellProperties index="604" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="175"/>
<atomRef index="196"/>
<atomRef index="176"/>
</cell>
<cell>
<cellProperties index="605" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="176"/>
<atomRef index="196"/>
<atomRef index="177"/>
</cell>
<cell>
<cellProperties index="606" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="177"/>
<atomRef index="196"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="607" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="177"/>
<atomRef index="197"/>
<atomRef index="198"/>
</cell>
<cell>
<cellProperties index="608" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="177"/>
<atomRef index="198"/>
<atomRef index="178"/>
</cell>
<cell>
<cellProperties index="609" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="178"/>
<atomRef index="198"/>
<atomRef index="179"/>
</cell>
<cell>
<cellProperties index="610" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="179"/>
<atomRef index="198"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="611" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="179"/>
<atomRef index="199"/>
<atomRef index="200"/>
</cell>
<cell>
<cellProperties index="612" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="179"/>
<atomRef index="200"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="613" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="180"/>
<atomRef index="200"/>
<atomRef index="181"/>
</cell>
<cell>
<cellProperties index="614" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="181"/>
<atomRef index="200"/>
<atomRef index="201"/>
</cell>
<cell>
<cellProperties index="615" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="181"/>
<atomRef index="201"/>
<atomRef index="202"/>
</cell>
<cell>
<cellProperties index="616" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="181"/>
<atomRef index="202"/>
<atomRef index="182"/>
</cell>
<cell>
<cellProperties index="617" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="183"/>
<atomRef index="203"/>
<atomRef index="204"/>
</cell>
<cell>
<cellProperties index="618" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="184"/>
<atomRef index="204"/>
<atomRef index="205"/>
</cell>
<cell>
<cellProperties index="619" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="184"/>
<atomRef index="205"/>
<atomRef index="185"/>
</cell>
<cell>
<cellProperties index="620" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="185"/>
<atomRef index="205"/>
<atomRef index="186"/>
</cell>
<cell>
<cellProperties index="621" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="186"/>
<atomRef index="205"/>
<atomRef index="206"/>
</cell>
<cell>
<cellProperties index="622" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="186"/>
<atomRef index="206"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="623" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="186"/>
<atomRef index="207"/>
<atomRef index="187"/>
</cell>
<cell>
<cellProperties index="624" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="189"/>
<atomRef index="208"/>
<atomRef index="190"/>
</cell>
<cell>
<cellProperties index="625" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="190"/>
<atomRef index="208"/>
<atomRef index="209"/>
</cell>
<cell>
<cellProperties index="626" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="190"/>
<atomRef index="209"/>
<atomRef index="210"/>
</cell>
<cell>
<cellProperties index="627" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="190"/>
<atomRef index="210"/>
<atomRef index="191"/>
</cell>
<cell>
<cellProperties index="628" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="191"/>
<atomRef index="210"/>
<atomRef index="192"/>
</cell>
<cell>
<cellProperties index="629" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="192"/>
<atomRef index="210"/>
<atomRef index="211"/>
</cell>
<cell>
<cellProperties index="630" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="192"/>
<atomRef index="211"/>
<atomRef index="212"/>
</cell>
<cell>
<cellProperties index="631" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="192"/>
<atomRef index="212"/>
<atomRef index="193"/>
</cell>
<cell>
<cellProperties index="632" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="193"/>
<atomRef index="212"/>
<atomRef index="194"/>
</cell>
<cell>
<cellProperties index="633" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="194"/>
<atomRef index="212"/>
<atomRef index="213"/>
</cell>
<cell>
<cellProperties index="634" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="194"/>
<atomRef index="213"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="635" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="194"/>
<atomRef index="214"/>
<atomRef index="195"/>
</cell>
<cell>
<cellProperties index="636" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="195"/>
<atomRef index="214"/>
<atomRef index="196"/>
</cell>
<cell>
<cellProperties index="637" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="196"/>
<atomRef index="214"/>
<atomRef index="215"/>
</cell>
<cell>
<cellProperties index="638" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="196"/>
<atomRef index="215"/>
<atomRef index="216"/>
</cell>
<cell>
<cellProperties index="639" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="196"/>
<atomRef index="216"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="640" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="197"/>
<atomRef index="216"/>
<atomRef index="198"/>
</cell>
<cell>
<cellProperties index="641" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="198"/>
<atomRef index="216"/>
<atomRef index="217"/>
</cell>
<cell>
<cellProperties index="642" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="198"/>
<atomRef index="217"/>
<atomRef index="218"/>
</cell>
<cell>
<cellProperties index="643" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="198"/>
<atomRef index="218"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="644" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="199"/>
<atomRef index="218"/>
<atomRef index="200"/>
</cell>
<cell>
<cellProperties index="645" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="200"/>
<atomRef index="218"/>
<atomRef index="219"/>
</cell>
<cell>
<cellProperties index="646" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="200"/>
<atomRef index="219"/>
<atomRef index="201"/>
</cell>
<cell>
<cellProperties index="647" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="201"/>
<atomRef index="219"/>
<atomRef index="237"/>
</cell>
<cell>
<cellProperties index="648" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="201"/>
<atomRef index="237"/>
<atomRef index="220"/>
</cell>
<cell>
<cellProperties index="649" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="201"/>
<atomRef index="220"/>
<atomRef index="202"/>
</cell>
<cell>
<cellProperties index="650" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="203"/>
<atomRef index="221"/>
<atomRef index="204"/>
</cell>
<cell>
<cellProperties index="651" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="204"/>
<atomRef index="221"/>
<atomRef index="222"/>
</cell>
<cell>
<cellProperties index="652" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="204"/>
<atomRef index="222"/>
<atomRef index="205"/>
</cell>
<cell>
<cellProperties index="653" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="205"/>
<atomRef index="222"/>
<atomRef index="223"/>
</cell>
<cell>
<cellProperties index="654" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="205"/>
<atomRef index="223"/>
<atomRef index="206"/>
</cell>
<cell>
<cellProperties index="655" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="206"/>
<atomRef index="223"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="656" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="207"/>
<atomRef index="223"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="657" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="207"/>
<atomRef index="224"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="658" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="207"/>
<atomRef index="225"/>
<atomRef index="282"/>
</cell>
<cell>
<cellProperties index="659" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="208"/>
<atomRef index="282"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="660" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="208"/>
<atomRef index="225"/>
<atomRef index="226"/>
</cell>
<cell>
<cellProperties index="661" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="208"/>
<atomRef index="226"/>
<atomRef index="227"/>
</cell>
<cell>
<cellProperties index="662" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="208"/>
<atomRef index="227"/>
<atomRef index="209"/>
</cell>
<cell>
<cellProperties index="663" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="209"/>
<atomRef index="227"/>
<atomRef index="210"/>
</cell>
<cell>
<cellProperties index="664" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="210"/>
<atomRef index="227"/>
<atomRef index="228"/>
</cell>
<cell>
<cellProperties index="665" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="210"/>
<atomRef index="228"/>
<atomRef index="229"/>
</cell>
<cell>
<cellProperties index="666" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="210"/>
<atomRef index="229"/>
<atomRef index="211"/>
</cell>
<cell>
<cellProperties index="667" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="211"/>
<atomRef index="229"/>
<atomRef index="212"/>
</cell>
<cell>
<cellProperties index="668" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="212"/>
<atomRef index="229"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="669" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="212"/>
<atomRef index="230"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="670" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="212"/>
<atomRef index="231"/>
<atomRef index="213"/>
</cell>
<cell>
<cellProperties index="671" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="213"/>
<atomRef index="231"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="672" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="214"/>
<atomRef index="231"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="673" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="214"/>
<atomRef index="232"/>
<atomRef index="233"/>
</cell>
<cell>
<cellProperties index="674" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="214"/>
<atomRef index="233"/>
<atomRef index="215"/>
</cell>
<cell>
<cellProperties index="675" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="215"/>
<atomRef index="233"/>
<atomRef index="216"/>
</cell>
<cell>
<cellProperties index="676" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="216"/>
<atomRef index="233"/>
<atomRef index="234"/>
</cell>
<cell>
<cellProperties index="677" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="216"/>
<atomRef index="234"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="678" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="216"/>
<atomRef index="235"/>
<atomRef index="217"/>
</cell>
<cell>
<cellProperties index="679" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="217"/>
<atomRef index="235"/>
<atomRef index="218"/>
</cell>
<cell>
<cellProperties index="680" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="218"/>
<atomRef index="235"/>
<atomRef index="236"/>
</cell>
<cell>
<cellProperties index="681" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="218"/>
<atomRef index="236"/>
<atomRef index="237"/>
</cell>
<cell>
<cellProperties index="682" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="218"/>
<atomRef index="237"/>
<atomRef index="219"/>
</cell>
<cell>
<cellProperties index="683" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="220"/>
<atomRef index="237"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="684" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="221"/>
<atomRef index="239"/>
<atomRef index="222"/>
</cell>
<cell>
<cellProperties index="685" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="223"/>
<atomRef index="240"/>
<atomRef index="241"/>
</cell>
<cell>
<cellProperties index="686" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="223"/>
<atomRef index="241"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="687" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="224"/>
<atomRef index="241"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="688" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="225"/>
<atomRef index="241"/>
<atomRef index="242"/>
</cell>
<cell>
<cellProperties index="689" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="225"/>
<atomRef index="242"/>
<atomRef index="243"/>
</cell>
<cell>
<cellProperties index="690" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="225"/>
<atomRef index="243"/>
<atomRef index="226"/>
</cell>
<cell>
<cellProperties index="691" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="226"/>
<atomRef index="243"/>
<atomRef index="227"/>
</cell>
<cell>
<cellProperties index="692" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="227"/>
<atomRef index="243"/>
<atomRef index="244"/>
</cell>
<cell>
<cellProperties index="693" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="227"/>
<atomRef index="244"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="694" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="227"/>
<atomRef index="245"/>
<atomRef index="228"/>
</cell>
<cell>
<cellProperties index="695" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="228"/>
<atomRef index="245"/>
<atomRef index="229"/>
</cell>
<cell>
<cellProperties index="696" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="229"/>
<atomRef index="245"/>
<atomRef index="246"/>
</cell>
<cell>
<cellProperties index="697" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="229"/>
<atomRef index="246"/>
<atomRef index="247"/>
</cell>
<cell>
<cellProperties index="698" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="229"/>
<atomRef index="247"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="699" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="230"/>
<atomRef index="247"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="700" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="231"/>
<atomRef index="247"/>
<atomRef index="248"/>
</cell>
<cell>
<cellProperties index="701" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="231"/>
<atomRef index="248"/>
<atomRef index="249"/>
</cell>
<cell>
<cellProperties index="702" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="231"/>
<atomRef index="249"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="703" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="232"/>
<atomRef index="249"/>
<atomRef index="233"/>
</cell>
<cell>
<cellProperties index="704" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="233"/>
<atomRef index="249"/>
<atomRef index="250"/>
</cell>
<cell>
<cellProperties index="705" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="233"/>
<atomRef index="250"/>
<atomRef index="251"/>
</cell>
<cell>
<cellProperties index="706" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="233"/>
<atomRef index="251"/>
<atomRef index="234"/>
</cell>
<cell>
<cellProperties index="707" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="234"/>
<atomRef index="251"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="708" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="235"/>
<atomRef index="251"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="709" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="235"/>
<atomRef index="252"/>
<atomRef index="253"/>
</cell>
<cell>
<cellProperties index="710" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="235"/>
<atomRef index="253"/>
<atomRef index="236"/>
</cell>
<cell>
<cellProperties index="711" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="236"/>
<atomRef index="253"/>
<atomRef index="237"/>
</cell>
<cell>
<cellProperties index="712" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="237"/>
<atomRef index="253"/>
<atomRef index="254"/>
</cell>
<cell>
<cellProperties index="713" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="237"/>
<atomRef index="254"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="714" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="239"/>
<atomRef index="255"/>
<atomRef index="240"/>
</cell>
<cell>
<cellProperties index="715" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="240"/>
<atomRef index="255"/>
<atomRef index="241"/>
</cell>
<cell>
<cellProperties index="716" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="241"/>
<atomRef index="255"/>
<atomRef index="256"/>
</cell>
<cell>
<cellProperties index="717" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="241"/>
<atomRef index="256"/>
<atomRef index="257"/>
</cell>
<cell>
<cellProperties index="718" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="241"/>
<atomRef index="257"/>
<atomRef index="242"/>
</cell>
<cell>
<cellProperties index="719" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="242"/>
<atomRef index="257"/>
<atomRef index="243"/>
</cell>
<cell>
<cellProperties index="720" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="243"/>
<atomRef index="257"/>
<atomRef index="258"/>
</cell>
<cell>
<cellProperties index="721" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="243"/>
<atomRef index="258"/>
<atomRef index="259"/>
</cell>
<cell>
<cellProperties index="722" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="243"/>
<atomRef index="259"/>
<atomRef index="244"/>
</cell>
<cell>
<cellProperties index="723" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="244"/>
<atomRef index="259"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="724" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="245"/>
<atomRef index="259"/>
<atomRef index="260"/>
</cell>
<cell>
<cellProperties index="725" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="245"/>
<atomRef index="260"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="726" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="245"/>
<atomRef index="261"/>
<atomRef index="246"/>
</cell>
<cell>
<cellProperties index="727" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="246"/>
<atomRef index="261"/>
<atomRef index="247"/>
</cell>
<cell>
<cellProperties index="728" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="247"/>
<atomRef index="261"/>
<atomRef index="262"/>
</cell>
<cell>
<cellProperties index="729" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="247"/>
<atomRef index="262"/>
<atomRef index="263"/>
</cell>
<cell>
<cellProperties index="730" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="247"/>
<atomRef index="263"/>
<atomRef index="248"/>
</cell>
<cell>
<cellProperties index="731" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="248"/>
<atomRef index="263"/>
<atomRef index="249"/>
</cell>
<cell>
<cellProperties index="732" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="249"/>
<atomRef index="263"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="733" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="249"/>
<atomRef index="264"/>
<atomRef index="265"/>
</cell>
<cell>
<cellProperties index="734" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="249"/>
<atomRef index="265"/>
<atomRef index="250"/>
</cell>
<cell>
<cellProperties index="735" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="250"/>
<atomRef index="265"/>
<atomRef index="251"/>
</cell>
<cell>
<cellProperties index="736" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="251"/>
<atomRef index="265"/>
<atomRef index="266"/>
</cell>
<cell>
<cellProperties index="737" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="251"/>
<atomRef index="266"/>
<atomRef index="267"/>
</cell>
<cell>
<cellProperties index="738" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="251"/>
<atomRef index="267"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="739" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="252"/>
<atomRef index="267"/>
<atomRef index="253"/>
</cell>
<cell>
<cellProperties index="740" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="253"/>
<atomRef index="267"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="741" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="253"/>
<atomRef index="268"/>
<atomRef index="254"/>
</cell>
<cell>
<cellProperties index="742" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="255"/>
<atomRef index="269"/>
<atomRef index="256"/>
</cell>
<cell>
<cellProperties index="743" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="256"/>
<atomRef index="269"/>
<atomRef index="257"/>
</cell>
<cell>
<cellProperties index="744" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="257"/>
<atomRef index="269"/>
<atomRef index="270"/>
</cell>
<cell>
<cellProperties index="745" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="257"/>
<atomRef index="270"/>
<atomRef index="271"/>
</cell>
<cell>
<cellProperties index="746" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="257"/>
<atomRef index="271"/>
<atomRef index="258"/>
</cell>
<cell>
<cellProperties index="747" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="258"/>
<atomRef index="271"/>
<atomRef index="259"/>
</cell>
<cell>
<cellProperties index="748" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="259"/>
<atomRef index="271"/>
<atomRef index="272"/>
</cell>
<cell>
<cellProperties index="749" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="259"/>
<atomRef index="272"/>
<atomRef index="273"/>
</cell>
<cell>
<cellProperties index="750" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="259"/>
<atomRef index="273"/>
<atomRef index="260"/>
</cell>
<cell>
<cellProperties index="751" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="260"/>
<atomRef index="273"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="752" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="261"/>
<atomRef index="273"/>
<atomRef index="274"/>
</cell>
<cell>
<cellProperties index="753" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="261"/>
<atomRef index="274"/>
<atomRef index="275"/>
</cell>
<cell>
<cellProperties index="754" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="261"/>
<atomRef index="275"/>
<atomRef index="262"/>
</cell>
<cell>
<cellProperties index="755" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="262"/>
<atomRef index="275"/>
<atomRef index="263"/>
</cell>
<cell>
<cellProperties index="756" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="263"/>
<atomRef index="275"/>
<atomRef index="276"/>
</cell>
<cell>
<cellProperties index="757" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="263"/>
<atomRef index="276"/>
<atomRef index="277"/>
</cell>
<cell>
<cellProperties index="758" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="263"/>
<atomRef index="277"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="759" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="264"/>
<atomRef index="277"/>
<atomRef index="265"/>
</cell>
<cell>
<cellProperties index="760" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="265"/>
<atomRef index="277"/>
<atomRef index="278"/>
</cell>
<cell>
<cellProperties index="761" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="265"/>
<atomRef index="278"/>
<atomRef index="279"/>
</cell>
<cell>
<cellProperties index="762" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="265"/>
<atomRef index="279"/>
<atomRef index="266"/>
</cell>
<cell>
<cellProperties index="763" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="266"/>
<atomRef index="279"/>
<atomRef index="267"/>
</cell>
<cell>
<cellProperties index="764" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="267"/>
<atomRef index="279"/>
<atomRef index="280"/>
</cell>
<cell>
<cellProperties index="765" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="267"/>
<atomRef index="280"/>
<atomRef index="281"/>
</cell>
<cell>
<cellProperties index="766" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="267"/>
<atomRef index="281"/>
<atomRef index="268"/>
</cell>
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>
