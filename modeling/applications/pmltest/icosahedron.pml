<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="Ico" nrOfAtoms="12"
 nrOfExclusiveComponents="1"
 nrOfInformativeComponents="3"
 nrOfCells="33"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="element list" myCustomProperty1="123" myCustomProperty2="another value">
<nrOfStructures value="12"/>
<atom>
<atomProperties index="0" x="0" y="77.8211" z="-42.533" myCustomProperty1="222"/>
</atom>
<atom>
<atomProperties index="1" x="40.4508" y="77.8211" z="-13.143" myCustomProperty2="yo!"/>
</atom>
<atom>
<atomProperties index="2" x="25" y="77.8211" z="34.41"/>
</atom>
<atom>
<atomProperties index="3" x="-25" y="77.8211" z="34.41" myCustomProperty1="999" myCustomProperty2="2 customs at the same time"/>
</atom>
<atom>
<atomProperties index="4" x="-40.4508" y="77.8211" z="-13.143"/>
</atom>
<atom>
<atomProperties index="5" x="25" y="120.354" z="-34.41"/>
</atom>
<atom>
<atomProperties index="6" x="40.4508" y="120.354" z="13.143"/>
</atom>
<atom>
<atomProperties index="7" x="0" y="120.354" z="42.533"/>
</atom>
<atom>
<atomProperties index="8" x="-40.4508" y="120.354" z="13.143"/>
</atom>
<atom>
<atomProperties index="9" x="-25" y="120.354" z="-34.41"/>
</atom>
<atom>
<atomProperties index="10" x="0" y="51.5346" z="0"/>
</atom>
<atom>
<atomProperties index="11" x="0" y="146.64" z="0"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Regions">
<structuralComponent name="Region #0 icosahedron" mode="POINTS">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX" myCustomProperty1="14.5" myCustomProperty2="2"/>
<nrOfStructures value="12"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="11"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Facets and neighbourhood">
<structuralComponent name="Element Neighborhoods">
<color r="0.5" g="0.5" b="0.5" a="1"/>
<nrOfStructures value="12"/>
<cell>
<cellProperties index="1" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="10"/>
<atomRef index="4"/>
<atomRef index="9"/>
<atomRef index="5"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="2" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="10"/>
<atomRef index="0"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="3" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="10"/>
<atomRef index="1"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="4" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="10"/>
<atomRef index="2"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="5" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="10"/>
<atomRef index="3"/>
<atomRef index="8"/>
<atomRef index="9"/>
<atomRef index="0"/>
</cell>
<cell>
<cellProperties index="6" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="0"/>
<atomRef index="9"/>
<atomRef index="11"/>
<atomRef index="6"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="7" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="1"/>
<atomRef index="5"/>
<atomRef index="11"/>
<atomRef index="7"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="8" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="2"/>
<atomRef index="6"/>
<atomRef index="11"/>
<atomRef index="8"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="9" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="3"/>
<atomRef index="7"/>
<atomRef index="11"/>
<atomRef index="9"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="10" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="4"/>
<atomRef index="8"/>
<atomRef index="11"/>
<atomRef index="5"/>
<atomRef index="0"/>
</cell>
<cell>
<cellProperties index="11" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="12" type="POLY_VERTEX"/>
<nrOfStructures value="5"/>
<atomRef index="9"/>
<atomRef index="8"/>
<atomRef index="7"/>
<atomRef index="6"/>
<atomRef index="5"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #0 (icosahedron)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="15"/>
<cell>
<cellProperties index="13" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="10"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="14" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="4"/>
<atomRef index="9"/>
</cell>
<cell>
<cellProperties index="15" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="9"/>
<atomRef index="5"/>
</cell>
<cell>
<cellProperties index="16" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="5"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="17" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="18" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="5"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="19" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="6"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="20" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="21" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="22" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="8"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="23" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="10"/>
</cell>
<cell>
<cellProperties index="24" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="8"/>
<atomRef index="9"/>
</cell>
<cell>
<cellProperties index="25" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="7"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="26" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="7"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="27" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="6"/>
<atomRef index="7"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of Chapeau(5)" mode="WIREFRAME_AND_SURFACE">
<color r="1" g="0.74902" b="0" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="28" type="TRIANGLE"/>
<color r="1" g="0.74902" b="0" a="1"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="9"/>
<atomRef index="11"/>
</cell>
<cell>
<cellProperties index="29" type="TRIANGLE"/>
<color r="1" g="0.74902" b="0" a="1"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="11"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="30" type="TRIANGLE"/>
<color r="1" g="0.74902" b="0" a="1"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="11"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="31" type="TRIANGLE"/>
<color r="1" g="0.74902" b="0" a="1"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="11"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="32" type="TRIANGLE"/>
<color r="1" g="0.74902" b="0" a="1"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="11"/>
<atomRef index="9"/>
</cell>
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>
