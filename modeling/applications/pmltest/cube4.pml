<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="cube 4" nrOfAtoms="8"
 nrOfExclusiveComponents="1"
 nrOfInformativeComponents="2"
 nrOfCells="1"
 comment="This is a comment, testing the physicalModel properties!"
 otherGlobalAttributes="4.0"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="element list">
<nrOfStructures value="8"/>
<atom>
<atomProperties index="0" x="-5" y="5" z="-5" name="a0"/>
</atom>
<atom>
<atomProperties index="1" x="5" y="5" z="-5"/>
</atom>
<atom>
<atomProperties index="2" x="5" y="5" z="5"/>
</atom>
<atom>
<atomProperties index="3" x="-5" y="5" z="5"/>
</atom>
<atom>
<atomProperties index="4" x="-5" y="-5" z="-5"/>
</atom>
<atom>
<atomProperties index="5" x="5" y="-5" z="-5"/>
</atom>
<atom>
<atomProperties index="6" x="5" y="-5" z="5"/>
</atom>
<atom>
<atomProperties index="7" x="-5" y="-5" z="5"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Exclusive components">
<structuralComponent name="One cubic cell" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="HEXAHEDRON"/>
<nrOfStructures value="8"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Informative Components">
<structuralComponent name="up facet" mode="SURFACE">
<nrOfStructures value="4"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
</structuralComponent>
<structuralComponent name="down facet" mode="SURFACE">
<nrOfStructures value="4"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>
