<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="custom physical properties test" nrOfAtoms="8"
 nrOfExclusiveComponents="1"
 nrOfCells="1"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="atom list">
<nrOfStructures value="8"/>
<atom>
<atomProperties index="0" x="-5" y="5" z="-5" name="a0" atomIntProp="-103"/>
</atom>
<atom>
<atomProperties index="1" x="5" y="5" z="-5" atomStringProp="a string property!"/>
</atom>
<atom>
<atomProperties index="2" x="5" y="5" z="5" AtomUIntProp="141304"/>
</atom>
<atom>
<atomProperties index="3" x="-5" y="5" z="5" aToMdOuBlEpRoP="-155.34"/>
</atom>
<atom>
<atomProperties index="4" x="-5" y="-5" z="-5" aToMdOuBlEpRoP="4.88801" atomStringProp="same string property!"/>
</atom>
<atom>
<atomProperties index="5" x="5" y="-5" z="-5"/>
</atom>
<atom>
<atomProperties index="6" x="5" y="-5" z="5"/>
</atom>
<atom>
<atomProperties index="7" x="-5" y="-5" z="5"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Exclusive components" evenHere="it works!" globalToMulticomponent="yes">
<structuralComponent name="One SC" mode="WIREFRAME_AND_SURFACE" scBoolProp="1" scCharProp="$">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="HEXAHEDRON" name="cell property test" cellFloatProp="100.256" cellIntProp="4" scBoolProp="1" scCharProp="@"/>
<nrOfStructures value="8"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
</physicalModel>
