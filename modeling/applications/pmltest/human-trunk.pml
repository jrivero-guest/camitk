<?xml version="1.0" encoding="UTF-8"?>
<!-- physical model (PML) is a generic representation for 3D physical model.
     PML supports not continous indexes and multiple non-exclusive labelling.
  --> 
<physicalModel name="F_02-sept.pdo" nrOfAtoms="252"
 nrOfExclusiveComponents="29"
 nrOfInformativeComponents="30"
 nrOfCells="597"
>
<!-- list of atoms: -->
<atoms>
<structuralComponent name="element list">
<nrOfStructures value="252"/>
<atom>
<atomProperties index="139" x="-0.889569" y="732.568" z="91.1403" mass="6"/>
</atom>
<atom>
<atomProperties index="142" x="-136.727" y="617.803" z="29.8059" mass="6"/>
</atom>
<atom>
<atomProperties index="143" x="-147.369" y="614.469" z="2.15225" mass="6"/>
</atom>
<atom>
<atomProperties index="146" x="137.365" y="614.469" z="32.9428" mass="6"/>
</atom>
<atom>
<atomProperties index="147" x="147.48" y="614.469" z="2.15225" mass="6"/>
</atom>
<atom>
<atomProperties index="211" x="-157.374" y="639.259" z="6.25861" mass="5"/>
</atom>
<atom>
<atomProperties index="280" x="157.374" y="639.259" z="6.25861" mass="5"/>
</atom>
<atom>
<atomProperties index="302" x="0.0553562" y="614.469" z="-101.643" mass="5"/>
</atom>
<atom>
<atomProperties index="303" x="-82.0227" y="614.469" z="-81.4197" mass="5"/>
</atom>
<atom>
<atomProperties index="304" x="-115.32" y="614.469" z="-60.7813" mass="5"/>
</atom>
<atom>
<atomProperties index="305" x="-146.151" y="614.469" z="-28.6383" mass="5"/>
</atom>
<atom>
<atomProperties index="306" x="-147.369" y="614.469" z="2.15225" mass="5"/>
</atom>
<atom>
<atomProperties index="287" x="0.0553562" y="614.469" z="-101.643" mass="5"/>
</atom>
<atom>
<atomProperties index="288" x="82.1913" y="614.469" z="-81.4197" mass="5"/>
</atom>
<atom>
<atomProperties index="289" x="115.458" y="614.469" z="-62.9945" mass="5"/>
</atom>
<atom>
<atomProperties index="290" x="146.261" y="614.469" z="-28.6383" mass="5"/>
</atom>
<atom>
<atomProperties index="291" x="147.48" y="614.469" z="2.15225" mass="5"/>
</atom>
<atom>
<atomProperties index="311" x="-155.616" y="636.793" z="-24.9362" mass="5"/>
</atom>
<atom>
<atomProperties index="0" x="0.0553562" y="614.469" z="-101.643" mass="12"/>
</atom>
<atom>
<atomProperties index="1" x="146.261" y="614.469" z="-28.6383" mass="12"/>
</atom>
<atom>
<atomProperties index="4" x="-146.151" y="614.469" z="-28.6383" mass="12"/>
</atom>
<atom>
<atomProperties index="5" x="83.3054" y="752.468" z="-83.7198" mass="12"/>
</atom>
<atom>
<atomProperties index="6" x="139.471" y="779.496" z="39.4424" mass="12"/>
</atom>
<atom>
<atomProperties index="7" x="0.0553562" y="746.457" z="95.447" mass="12"/>
</atom>
<atom>
<atomProperties index="8" x="-139.36" y="779.496" z="39.4424" mass="12"/>
</atom>
<atom>
<atomProperties index="9" x="-83.1946" y="752.468" z="-83.7198" mass="12"/>
</atom>
<atom>
<atomProperties index="10" x="0.0553562" y="768.632" z="5.40205" mass="12"/>
</atom>
<atom>
<atomProperties index="31" x="-136.164" y="779.496" z="-22.1387" mass="12"/>
</atom>
<atom>
<atomProperties index="33" x="136.275" y="779.496" z="-22.1387" mass="12"/>
</atom>
<atom>
<atomProperties index="34" x="146.261" y="779.496" z="8.65185" mass="12"/>
</atom>
<atom>
<atomProperties index="35" x="115.391" y="779.496" z="-52.9293" mass="12"/>
</atom>
<atom>
<atomProperties index="36" x="-115.28" y="779.496" z="-52.9293" mass="12"/>
</atom>
<atom>
<atomProperties index="37" x="-146.151" y="779.496" z="8.65185" mass="12"/>
</atom>
<atom>
<atomProperties index="38" x="-147.369" y="614.469" z="2.15225" mass="12"/>
</atom>
<atom>
<atomProperties index="39" x="147.48" y="614.469" z="2.15225" mass="12"/>
</atom>
<atom>
<atomProperties index="28" x="-136.727" y="617.803" z="29.8059" mass="12"/>
</atom>
<atom>
<atomProperties index="29" x="0.0553562" y="735.444" z="-92.2214" mass="12"/>
</atom>
<atom>
<atomProperties index="40" x="-124.89" y="644.955" z="65.9013" mass="12"/>
</atom>
<atom>
<atomProperties index="41" x="-67.2952" y="719.028" z="77.5025" mass="12"/>
</atom>
<atom>
<atomProperties index="42" x="67.4059" y="719.028" z="77.5025" mass="12"/>
</atom>
<atom>
<atomProperties index="43" x="137.365" y="614.469" z="32.9428" mass="12"/>
</atom>
<atom>
<atomProperties index="44" x="117.529" y="637.832" z="66.3751" mass="12"/>
</atom>
<atom>
<atomProperties index="46" x="44.3449" y="765.361" z="38.8459" mass="12"/>
</atom>
<atom>
<atomProperties index="47" x="93.1041" y="797.782" z="22.4222" mass="12"/>
</atom>
<atom>
<atomProperties index="48" x="84.2288" y="797.782" z="7.02695" mass="12"/>
</atom>
<atom>
<atomProperties index="49" x="75.3535" y="797.782" z="-8.36833" mass="12"/>
</atom>
<atom>
<atomProperties index="50" x="66.4782" y="797.782" z="-23.7636" mass="12"/>
</atom>
<atom>
<atomProperties index="51" x="57.6028" y="797.782" z="-39.1589" mass="12"/>
</atom>
<atom>
<atomProperties index="52" x="0.0553562" y="768.632" z="-39.1589" mass="12"/>
</atom>
<atom>
<atomProperties index="53" x="-57.2822" y="797.782" z="-39.1589" mass="12"/>
</atom>
<atom>
<atomProperties index="54" x="-66.1575" y="797.782" z="-23.7636" mass="12"/>
</atom>
<atom>
<atomProperties index="55" x="-75.0328" y="797.782" z="-8.36833" mass="12"/>
</atom>
<atom>
<atomProperties index="56" x="-83.9081" y="797.782" z="7.02695" mass="12"/>
</atom>
<atom>
<atomProperties index="57" x="-92.7835" y="797.782" z="22.4222" mass="12"/>
</atom>
<atom>
<atomProperties index="58" x="-48.5989" y="765.361" z="38.8459" mass="12"/>
</atom>
<atom>
<atomProperties index="59" x="0.0553562" y="772.205" z="60.4823" mass="12"/>
</atom>
<atom>
<atomProperties index="125" x="82.1913" y="614.469" z="-81.4197" mass="12"/>
</atom>
<atom>
<atomProperties index="127" x="-82.0227" y="614.469" z="-81.4197" mass="12"/>
</atom>
<atom>
<atomProperties index="129" x="115.458" y="614.469" z="-62.9945" mass="12"/>
</atom>
<atom>
<atomProperties index="131" x="-115.32" y="614.469" z="-60.7813" mass="12"/>
</atom>
<atom>
<atomProperties index="2" x="76.8272" y="613.268" z="92.2917" mass="250"/>
</atom>
<atom>
<atomProperties index="3" x="-76.8272" y="613.268" z="92.2917" mass="250"/>
</atom>
<atom>
<atomProperties index="114" x="0.0553562" y="613.268" z="115.447" mass="250"/>
</atom>
<atom>
<atomProperties index="100" x="-76.8272" y="448.241" z="92.2917" mass="250"/>
</atom>
<atom>
<atomProperties index="101" x="-110.474" y="449.443" z="63.7334" mass="250"/>
</atom>
<atom>
<atomProperties index="102" x="-137.254" y="449.443" z="32.9428" mass="250"/>
</atom>
<atom>
<atomProperties index="103" x="-147.369" y="449.443" z="2.15225" mass="250"/>
</atom>
<atom>
<atomProperties index="104" x="-146.151" y="449.443" z="-28.6383" mass="250"/>
</atom>
<atom>
<atomProperties index="105" x="-67.224" y="449.443" z="-83.7198" mass="250"/>
</atom>
<atom>
<atomProperties index="106" x="0.0553562" y="449.443" z="-97.8187" mass="250"/>
</atom>
<atom>
<atomProperties index="107" x="67.4059" y="449.443" z="-83.7198" mass="250"/>
</atom>
<atom>
<atomProperties index="108" x="146.261" y="449.443" z="-28.6383" mass="250"/>
</atom>
<atom>
<atomProperties index="109" x="147.48" y="449.443" z="2.15225" mass="250"/>
</atom>
<atom>
<atomProperties index="110" x="137.365" y="449.443" z="32.9428" mass="250"/>
</atom>
<atom>
<atomProperties index="111" x="110.585" y="449.443" z="63.7335" mass="250"/>
</atom>
<atom>
<atomProperties index="112" x="76.8272" y="448.241" z="92.2917" mass="250"/>
</atom>
<atom>
<atomProperties index="113" x="0.0553562" y="449.443" z="5.40205" mass="250"/>
</atom>
<atom>
<atomProperties index="115" x="0.0553562" y="448.241" z="112.292" mass="250"/>
</atom>
<atom>
<atomProperties index="121" x="115.5" y="449.443" z="-63" mass="250"/>
</atom>
<atom>
<atomProperties index="123" x="-115.5" y="449.443" z="-63" mass="250"/>
</atom>
<atom>
<atomProperties index="193" x="-67.2952" y="719.028" z="77.5025" mass="6"/>
</atom>
<atom>
<atomProperties index="316" x="-44.4607" y="901.796" z="13.9504" mass="6"/>
</atom>
<atom>
<atomProperties index="317" x="2.60796" y="893.929" z="19.591" mass="6"/>
</atom>
<atom>
<atomProperties index="318" x="44.4607" y="901.796" z="13.9504" mass="6"/>
</atom>
<atom>
<atomProperties index="319" x="51.8037" y="860.045" z="35.218" mass="6"/>
</atom>
<atom>
<atomProperties index="320" x="2.42319" y="861.71" z="37.2625" mass="6"/>
</atom>
<atom>
<atomProperties index="321" x="-51.8037" y="860.045" z="35.218" mass="6"/>
</atom>
<atom>
<atomProperties index="322" x="-63.3503" y="831.443" z="50.1407" mass="6"/>
</atom>
<atom>
<atomProperties index="323" x="63.3503" y="831.443" z="50.1407" mass="6"/>
</atom>
<atom>
<atomProperties index="324" x="1.98413" y="831.282" z="53.4617" mass="6"/>
</atom>
<atom>
<atomProperties index="325" x="-84.0083" y="808.936" z="61.7695" mass="6"/>
</atom>
<atom>
<atomProperties index="326" x="1.61841" y="806.2" z="66.7775" mass="6"/>
</atom>
<atom>
<atomProperties index="327" x="84.0083" y="808.936" z="61.7695" mass="6"/>
</atom>
<atom>
<atomProperties index="328" x="-94.9634" y="791.081" z="64.0167" mass="6"/>
</atom>
<atom>
<atomProperties index="329" x="1.07439" y="788.171" z="73.4604" mass="6"/>
</atom>
<atom>
<atomProperties index="330" x="94.9634" y="791.081" z="64.0167" mass="6"/>
</atom>
<atom>
<atomProperties index="331" x="-110.561" y="763.262" z="74.2686" mass="6"/>
</atom>
<atom>
<atomProperties index="332" x="110.561" y="763.262" z="74.2686" mass="6"/>
</atom>
<atom>
<atomProperties index="333" x="0.807777" y="775.44" z="79.3865" mass="6"/>
</atom>
<atom>
<atomProperties index="337" x="0.0553562" y="746.457" z="95.447" mass="6"/>
</atom>
<atom>
<atomProperties index="338" x="67.4059" y="719.028" z="77.5025" mass="6"/>
</atom>
<atom>
<atomProperties index="341" x="-124.89" y="644.955" z="65.9013" mass="6"/>
</atom>
<atom>
<atomProperties index="342" x="117.529" y="637.832" z="66.3751" mass="6"/>
</atom>
<atom>
<atomProperties index="194" x="-92.4555" y="715.132" z="77.5025" mass="5"/>
</atom>
<atom>
<atomProperties index="195" x="-117.887" y="716.904" z="72.7059" mass="5"/>
</atom>
<atom>
<atomProperties index="336" x="-67.2952" y="719.028" z="77.5025" mass="6"/>
</atom>
<atom>
<atomProperties index="263" x="92.4555" y="715.132" z="77.5025" mass="5"/>
</atom>
<atom>
<atomProperties index="264" x="117.887" y="716.904" z="72.7059" mass="5"/>
</atom>
<atom>
<atomProperties index="203" x="-137.628" y="660.289" z="65.4216" mass="5"/>
</atom>
<atom>
<atomProperties index="272" x="137.628" y="660.289" z="65.4216" mass="5"/>
</atom>
<atom>
<atomProperties index="171" x="-44.4607" y="901.796" z="13.9504" mass="5"/>
</atom>
<atom>
<atomProperties index="172" x="-31.482" y="935.189" z="-39.4426" mass="5"/>
</atom>
<atom>
<atomProperties index="173" x="-46.0105" y="938.146" z="-39.9927" mass="5"/>
</atom>
<atom>
<atomProperties index="174" x="-67.6233" y="933.452" z="-29.8524" mass="5"/>
</atom>
<atom>
<atomProperties index="175" x="-75.9694" y="921.153" z="-11.3892" mass="5"/>
</atom>
<atom>
<atomProperties index="176" x="-66.8723" y="908.024" z="5.45941" mass="5"/>
</atom>
<atom>
<atomProperties index="240" x="44.4607" y="901.796" z="13.9504" mass="5"/>
</atom>
<atom>
<atomProperties index="241" x="31.482" y="935.189" z="-39.4426" mass="5"/>
</atom>
<atom>
<atomProperties index="242" x="46.0105" y="938.146" z="-39.9927" mass="5"/>
</atom>
<atom>
<atomProperties index="243" x="67.6233" y="933.452" z="-29.8524" mass="5"/>
</atom>
<atom>
<atomProperties index="244" x="75.9694" y="921.153" z="-11.3892" mass="5"/>
</atom>
<atom>
<atomProperties index="245" x="66.8723" y="908.024" z="5.45941" mass="5"/>
</atom>
<atom>
<atomProperties index="164" x="-51.8037" y="860.045" z="35.218" mass="5"/>
</atom>
<atom>
<atomProperties index="165" x="-37.7128" y="911.961" z="-42.4889" mass="5"/>
</atom>
<atom>
<atomProperties index="166" x="-58.2488" y="915.662" z="-44.2914" mass="5"/>
</atom>
<atom>
<atomProperties index="167" x="-95.4571" y="907.616" z="-30.2761" mass="5"/>
</atom>
<atom>
<atomProperties index="168" x="-105.816" y="888.615" z="-1.56345" mass="5"/>
</atom>
<atom>
<atomProperties index="169" x="-85.5503" y="872.312" z="20.8281" mass="5"/>
</atom>
<atom>
<atomProperties index="232" x="51.8037" y="860.045" z="35.218" mass="5"/>
</atom>
<atom>
<atomProperties index="234" x="37.7128" y="911.961" z="-42.4889" mass="5"/>
</atom>
<atom>
<atomProperties index="235" x="58.2488" y="915.662" z="-44.2914" mass="5"/>
</atom>
<atom>
<atomProperties index="236" x="95.4571" y="907.616" z="-30.2761" mass="5"/>
</atom>
<atom>
<atomProperties index="237" x="105.816" y="888.615" z="-1.56345" mass="5"/>
</atom>
<atom>
<atomProperties index="238" x="85.5503" y="872.312" z="20.8281" mass="5"/>
</atom>
<atom>
<atomProperties index="156" x="-63.3503" y="831.443" z="50.1407" mass="5"/>
</atom>
<atom>
<atomProperties index="158" x="-39.5307" y="887.496" z="-42.871" mass="5"/>
</atom>
<atom>
<atomProperties index="159" x="-68.6064" y="891.141" z="-49.8831" mass="5"/>
</atom>
<atom>
<atomProperties index="160" x="-117.272" y="881.684" z="-29.8808" mass="5"/>
</atom>
<atom>
<atomProperties index="161" x="-129.191" y="862.584" z="1.38089" mass="5"/>
</atom>
<atom>
<atomProperties index="162" x="-103.934" y="840.986" z="32.4558" mass="5"/>
</atom>
<atom>
<atomProperties index="225" x="63.3503" y="831.443" z="50.1407" mass="5"/>
</atom>
<atom>
<atomProperties index="227" x="39.5307" y="887.496" z="-42.871" mass="5"/>
</atom>
<atom>
<atomProperties index="228" x="68.6064" y="891.141" z="-49.8831" mass="5"/>
</atom>
<atom>
<atomProperties index="229" x="117.272" y="881.684" z="-29.8808" mass="5"/>
</atom>
<atom>
<atomProperties index="230" x="129.191" y="862.584" z="1.38089" mass="5"/>
</atom>
<atom>
<atomProperties index="231" x="103.934" y="840.986" z="32.4558" mass="5"/>
</atom>
<atom>
<atomProperties index="178" x="-84.0083" y="808.936" z="61.7695" mass="5"/>
</atom>
<atom>
<atomProperties index="179" x="-43.1655" y="862.942" z="-42.0577" mass="5"/>
</atom>
<atom>
<atomProperties index="180" x="-86.3656" y="867.546" z="-50.0324" mass="5"/>
</atom>
<atom>
<atomProperties index="181" x="-129.299" y="857.391" z="-31.6863" mass="5"/>
</atom>
<atom>
<atomProperties index="182" x="-140.613" y="838.702" z="4.40205" mass="5"/>
</atom>
<atom>
<atomProperties index="183" x="-122.422" y="821.983" z="38.697" mass="5"/>
</atom>
<atom>
<atomProperties index="247" x="84.0083" y="808.936" z="61.7695" mass="5"/>
</atom>
<atom>
<atomProperties index="248" x="43.1655" y="862.942" z="-42.0577" mass="5"/>
</atom>
<atom>
<atomProperties index="249" x="86.3656" y="867.546" z="-50.0324" mass="5"/>
</atom>
<atom>
<atomProperties index="250" x="129.299" y="857.391" z="-31.6863" mass="5"/>
</atom>
<atom>
<atomProperties index="251" x="140.613" y="838.702" z="4.40205" mass="5"/>
</atom>
<atom>
<atomProperties index="252" x="122.422" y="821.983" z="38.697" mass="5"/>
</atom>
<atom>
<atomProperties index="149" x="-94.9634" y="791.081" z="64.0167" mass="5"/>
</atom>
<atom>
<atomProperties index="150" x="-42.1811" y="839.754" z="-45.791" mass="5"/>
</atom>
<atom>
<atomProperties index="151" x="-58.0048" y="845.723" z="-56.8813" mass="5"/>
</atom>
<atom>
<atomProperties index="152" x="-96.9947" y="843.358" z="-58.131" mass="5"/>
</atom>
<atom>
<atomProperties index="153" x="-133.609" y="834.576" z="-35.7511" mass="5"/>
</atom>
<atom>
<atomProperties index="154" x="-149.583" y="820.883" z="0.975995" mass="5"/>
</atom>
<atom>
<atomProperties index="155" x="-136.708" y="804.801" z="37.9604" mass="5"/>
</atom>
<atom>
<atomProperties index="218" x="94.9634" y="791.081" z="64.0167" mass="5"/>
</atom>
<atom>
<atomProperties index="219" x="42.1811" y="839.754" z="-45.791" mass="5"/>
</atom>
<atom>
<atomProperties index="220" x="58.0048" y="845.723" z="-56.8813" mass="5"/>
</atom>
<atom>
<atomProperties index="221" x="96.9947" y="843.358" z="-58.131" mass="5"/>
</atom>
<atom>
<atomProperties index="222" x="133.609" y="834.576" z="-35.7511" mass="5"/>
</atom>
<atom>
<atomProperties index="223" x="149.583" y="820.883" z="0.975995" mass="5"/>
</atom>
<atom>
<atomProperties index="224" x="136.708" y="804.801" z="37.9604" mass="5"/>
</atom>
<atom>
<atomProperties index="186" x="-110.561" y="763.262" z="74.2686" mass="5"/>
</atom>
<atom>
<atomProperties index="187" x="-45.6379" y="820.052" z="-60.0876" mass="5"/>
</atom>
<atom>
<atomProperties index="188" x="-65.1241" y="823.971" z="-63.942" mass="5"/>
</atom>
<atom>
<atomProperties index="189" x="-101.408" y="822.87" z="-57.4964" mass="5"/>
</atom>
<atom>
<atomProperties index="190" x="-139.943" y="810.913" z="-30.2392" mass="5"/>
</atom>
<atom>
<atomProperties index="191" x="-155.5" y="793.216" z="5.17362" mass="5"/>
</atom>
<atom>
<atomProperties index="192" x="-146.309" y="776.97" z="42.7446" mass="5"/>
</atom>
<atom>
<atomProperties index="255" x="110.561" y="763.262" z="74.2686" mass="5"/>
</atom>
<atom>
<atomProperties index="256" x="45.6379" y="820.052" z="-60.0876" mass="5"/>
</atom>
<atom>
<atomProperties index="257" x="65.1241" y="823.971" z="-63.942" mass="5"/>
</atom>
<atom>
<atomProperties index="258" x="101.408" y="822.87" z="-57.4964" mass="5"/>
</atom>
<atom>
<atomProperties index="259" x="139.943" y="810.913" z="-30.2392" mass="5"/>
</atom>
<atom>
<atomProperties index="260" x="155.5" y="793.216" z="5.17362" mass="5"/>
</atom>
<atom>
<atomProperties index="261" x="146.309" y="776.97" z="42.7446" mass="5"/>
</atom>
<atom>
<atomProperties index="239" x="2.60796" y="893.929" z="19.591" mass="5"/>
</atom>
<atom>
<atomProperties index="133" x="2.42319" y="861.71" z="37.2625" mass="6"/>
</atom>
<atom>
<atomProperties index="134" x="1.98413" y="831.282" z="53.4617" mass="6"/>
</atom>
<atom>
<atomProperties index="135" x="1.61841" y="806.2" z="66.7775" mass="6"/>
</atom>
<atom>
<atomProperties index="136" x="1.07439" y="788.171" z="73.4604" mass="6"/>
</atom>
<atom>
<atomProperties index="137" x="0.807777" y="775.44" z="79.3865" mass="6"/>
</atom>
<atom>
<atomProperties index="138" x="0.0553562" y="746.457" z="95.447" mass="6"/>
</atom>
<atom>
<atomProperties index="196" x="-45.6379" y="799.022" z="-69.4922" mass="5"/>
</atom>
<atom>
<atomProperties index="197" x="-65.1241" y="799.823" z="-69.9699" mass="5"/>
</atom>
<atom>
<atomProperties index="198" x="-101.408" y="796.077" z="-59.3836" mass="5"/>
</atom>
<atom>
<atomProperties index="199" x="-139.943" y="783.629" z="-35.0995" mass="5"/>
</atom>
<atom>
<atomProperties index="200" x="-155.672" y="765.036" z="5.31009" mass="5"/>
</atom>
<atom>
<atomProperties index="201" x="-146.309" y="744.701" z="47.5676" mass="5"/>
</atom>
<atom>
<atomProperties index="265" x="45.6379" y="799.022" z="-69.4922" mass="5"/>
</atom>
<atom>
<atomProperties index="266" x="65.1241" y="799.823" z="-69.9699" mass="5"/>
</atom>
<atom>
<atomProperties index="267" x="101.408" y="796.077" z="-59.3836" mass="5"/>
</atom>
<atom>
<atomProperties index="268" x="139.943" y="783.629" z="-35.0995" mass="5"/>
</atom>
<atom>
<atomProperties index="269" x="155.672" y="765.036" z="5.31009" mass="5"/>
</atom>
<atom>
<atomProperties index="270" x="146.309" y="744.701" z="47.5676" mass="5"/>
</atom>
<atom>
<atomProperties index="204" x="-45.6379" y="773.532" z="-86.7058" mass="5"/>
</atom>
<atom>
<atomProperties index="205" x="-65.1241" y="771.023" z="-84.867" mass="5"/>
</atom>
<atom>
<atomProperties index="206" x="-114.003" y="762.117" z="-64.5698" mass="5"/>
</atom>
<atom>
<atomProperties index="207" x="-150.112" y="746.122" z="-30.8255" mass="5"/>
</atom>
<atom>
<atomProperties index="208" x="-161.269" y="723.308" z="8.30346" mass="5"/>
</atom>
<atom>
<atomProperties index="209" x="-160.675" y="700.737" z="39.0292" mass="5"/>
</atom>
<atom>
<atomProperties index="273" x="45.6379" y="773.532" z="-86.7058" mass="5"/>
</atom>
<atom>
<atomProperties index="274" x="65.1241" y="771.023" z="-84.867" mass="5"/>
</atom>
<atom>
<atomProperties index="275" x="114.003" y="762.117" z="-64.5698" mass="5"/>
</atom>
<atom>
<atomProperties index="276" x="150.112" y="746.122" z="-30.8255" mass="5"/>
</atom>
<atom>
<atomProperties index="277" x="161.269" y="723.308" z="8.30346" mass="5"/>
</atom>
<atom>
<atomProperties index="278" x="160.675" y="700.737" z="39.0292" mass="5"/>
</atom>
<atom>
<atomProperties index="212" x="-49.285" y="748.642" z="-99.5624" mass="5"/>
</atom>
<atom>
<atomProperties index="213" x="-65.1241" y="742.983" z="-99.5838" mass="5"/>
</atom>
<atom>
<atomProperties index="214" x="-101.408" y="729.9" z="-85.8138" mass="5"/>
</atom>
<atom>
<atomProperties index="215" x="-148.1" y="686.949" z="-42.7864" mass="5"/>
</atom>
<atom>
<atomProperties index="216" x="-162.511" y="663.866" z="-10.5922" mass="5"/>
</atom>
<atom>
<atomProperties index="281" x="49.285" y="748.642" z="-99.5624" mass="5"/>
</atom>
<atom>
<atomProperties index="282" x="65.1241" y="742.983" z="-99.5838" mass="5"/>
</atom>
<atom>
<atomProperties index="283" x="101.408" y="729.9" z="-85.8138" mass="5"/>
</atom>
<atom>
<atomProperties index="284" x="148.1" y="686.949" z="-42.7864" mass="5"/>
</atom>
<atom>
<atomProperties index="285" x="162.511" y="663.866" z="-10.5922" mass="5"/>
</atom>
<atom>
<atomProperties index="307" x="-51.1876" y="700.406" z="-103.108" mass="5"/>
</atom>
<atom>
<atomProperties index="308" x="-76.6793" y="692.099" z="-100.521" mass="5"/>
</atom>
<atom>
<atomProperties index="309" x="-120.113" y="672.357" z="-81.9964" mass="5"/>
</atom>
<atom>
<atomProperties index="310" x="-139.362" y="654.683" z="-53.8544" mass="5"/>
</atom>
<atom>
<atomProperties index="292" x="51.1876" y="700.406" z="-103.108" mass="5"/>
</atom>
<atom>
<atomProperties index="293" x="76.6793" y="692.099" z="-100.521" mass="5"/>
</atom>
<atom>
<atomProperties index="294" x="120.113" y="672.357" z="-81.9964" mass="5"/>
</atom>
<atom>
<atomProperties index="295" x="139.362" y="654.683" z="-53.8544" mass="5"/>
</atom>
<atom>
<atomProperties index="296" x="155.616" y="636.793" z="-24.9362" mass="5"/>
</atom>
<atom>
<atomProperties index="312" x="-48.3084" y="678.514" z="-102.227" mass="5"/>
</atom>
<atom>
<atomProperties index="313" x="-69.338" y="669.811" z="-104.884" mass="5"/>
</atom>
<atom>
<atomProperties index="314" x="-89.7639" y="656.64" z="-101.599" mass="5"/>
</atom>
<atom>
<atomProperties index="315" x="-102.304" y="639.924" z="-89.022" mass="5"/>
</atom>
<atom>
<atomProperties index="297" x="48.3084" y="678.514" z="-102.227" mass="5"/>
</atom>
<atom>
<atomProperties index="298" x="69.338" y="669.811" z="-104.884" mass="5"/>
</atom>
<atom>
<atomProperties index="299" x="89.7639" y="656.64" z="-101.599" mass="5"/>
</atom>
<atom>
<atomProperties index="300" x="102.304" y="639.924" z="-89.022" mass="5"/>
</atom>
<atom>
<atomProperties index="343" x="-48.3084" y="652.52" z="-102.227" mass="5"/>
</atom>
<atom>
<atomProperties index="344" x="-69.338" y="643.817" z="-104.883" mass="5"/>
</atom>
<atom>
<atomProperties index="345" x="-89.7639" y="630.646" z="-101.599" mass="5"/>
</atom>
<atom>
<atomProperties index="346" x="-82.0227" y="614.469" z="-81.4197" mass="5"/>
</atom>
<atom>
<atomProperties index="347" x="48.3084" y="652.52" z="-102.227" mass="5"/>
</atom>
<atom>
<atomProperties index="348" x="69.338" y="643.817" z="-104.883" mass="5"/>
</atom>
<atom>
<atomProperties index="349" x="89.7639" y="630.646" z="-101.599" mass="5"/>
</atom>
<atom>
<atomProperties index="350" x="82.1913" y="614.469" z="-81.4197" mass="5"/>
</atom>
</structuralComponent>
</atoms>
<!-- list of exclusive components : -->
<exclusiveComponents>
<multiComponent name="Regions ">
<structuralComponent name="Region #0 cage_tho">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="18"/>
<atomRef index="139"/>
<atomRef index="142"/>
<atomRef index="143"/>
<atomRef index="146"/>
<atomRef index="147"/>
<atomRef index="211"/>
<atomRef index="280"/>
<atomRef index="302"/>
<atomRef index="303"/>
<atomRef index="304"/>
<atomRef index="305"/>
<atomRef index="306"/>
<atomRef index="287"/>
<atomRef index="288"/>
<atomRef index="289"/>
<atomRef index="290"/>
<atomRef index="291"/>
<atomRef index="311"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #1 diaphragme">
<color r="0.8" g="0.2" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="1" type="POLY_VERTEX" phymulType="muscular"/>
<nrOfStructures value="42"/>
<atomRef index="0"/>
<atomRef index="1"/>
<atomRef index="4"/>
<atomRef index="5"/>
<atomRef index="6"/>
<atomRef index="7"/>
<atomRef index="8"/>
<atomRef index="9"/>
<atomRef index="10"/>
<atomRef index="31"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="35"/>
<atomRef index="36"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="39"/>
<atomRef index="28"/>
<atomRef index="29"/>
<atomRef index="40"/>
<atomRef index="41"/>
<atomRef index="42"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="52"/>
<atomRef index="53"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="125"/>
<atomRef index="127"/>
<atomRef index="129"/>
<atomRef index="131"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #2 abdomen">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="2" type="POLY_VERTEX" phymulType="elastic"/>
<nrOfStructures value="20"/>
<atomRef index="2"/>
<atomRef index="3"/>
<atomRef index="114"/>
<atomRef index="100"/>
<atomRef index="101"/>
<atomRef index="102"/>
<atomRef index="103"/>
<atomRef index="104"/>
<atomRef index="105"/>
<atomRef index="106"/>
<atomRef index="107"/>
<atomRef index="108"/>
<atomRef index="109"/>
<atomRef index="110"/>
<atomRef index="111"/>
<atomRef index="112"/>
<atomRef index="113"/>
<atomRef index="115"/>
<atomRef index="121"/>
<atomRef index="123"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #3 cartillage">
<color r="0.8" g="0.8" b="0.2" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="3" type="POLY_VERTEX" phymulType="elastic"/>
<nrOfStructures value="30"/>
<atomRef index="193"/>
<atomRef index="316"/>
<atomRef index="317"/>
<atomRef index="318"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="323"/>
<atomRef index="324"/>
<atomRef index="325"/>
<atomRef index="326"/>
<atomRef index="327"/>
<atomRef index="328"/>
<atomRef index="329"/>
<atomRef index="330"/>
<atomRef index="331"/>
<atomRef index="332"/>
<atomRef index="333"/>
<atomRef index="337"/>
<atomRef index="338"/>
<atomRef index="341"/>
<atomRef index="342"/>
<atomRef index="194"/>
<atomRef index="195"/>
<atomRef index="336"/>
<atomRef index="263"/>
<atomRef index="264"/>
<atomRef index="203"/>
<atomRef index="272"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #4 rib_1_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="4" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="171"/>
<atomRef index="172"/>
<atomRef index="173"/>
<atomRef index="174"/>
<atomRef index="175"/>
<atomRef index="176"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #5 rib_1_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="5" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="240"/>
<atomRef index="241"/>
<atomRef index="242"/>
<atomRef index="243"/>
<atomRef index="244"/>
<atomRef index="245"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #6 rib_2_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="6" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="164"/>
<atomRef index="165"/>
<atomRef index="166"/>
<atomRef index="167"/>
<atomRef index="168"/>
<atomRef index="169"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #7 rib_2_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="7" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="232"/>
<atomRef index="234"/>
<atomRef index="235"/>
<atomRef index="236"/>
<atomRef index="237"/>
<atomRef index="238"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #8 rib_3_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="8" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="156"/>
<atomRef index="158"/>
<atomRef index="159"/>
<atomRef index="160"/>
<atomRef index="161"/>
<atomRef index="162"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #9 rib_3_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="9" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="225"/>
<atomRef index="227"/>
<atomRef index="228"/>
<atomRef index="229"/>
<atomRef index="230"/>
<atomRef index="231"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #10 rib_4_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="10" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="178"/>
<atomRef index="179"/>
<atomRef index="180"/>
<atomRef index="181"/>
<atomRef index="182"/>
<atomRef index="183"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #11 rib_4_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="11" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="247"/>
<atomRef index="248"/>
<atomRef index="249"/>
<atomRef index="250"/>
<atomRef index="251"/>
<atomRef index="252"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #12 rib_5_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="12" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="7"/>
<atomRef index="149"/>
<atomRef index="150"/>
<atomRef index="151"/>
<atomRef index="152"/>
<atomRef index="153"/>
<atomRef index="154"/>
<atomRef index="155"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #13 rib_5_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="13" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="7"/>
<atomRef index="218"/>
<atomRef index="219"/>
<atomRef index="220"/>
<atomRef index="221"/>
<atomRef index="222"/>
<atomRef index="223"/>
<atomRef index="224"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #14 rib_6_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="14" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="7"/>
<atomRef index="186"/>
<atomRef index="187"/>
<atomRef index="188"/>
<atomRef index="189"/>
<atomRef index="190"/>
<atomRef index="191"/>
<atomRef index="192"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #15 rib_6_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="15" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="7"/>
<atomRef index="255"/>
<atomRef index="256"/>
<atomRef index="257"/>
<atomRef index="258"/>
<atomRef index="259"/>
<atomRef index="260"/>
<atomRef index="261"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #16 sternum">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="16" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="7"/>
<atomRef index="239"/>
<atomRef index="133"/>
<atomRef index="134"/>
<atomRef index="135"/>
<atomRef index="136"/>
<atomRef index="137"/>
<atomRef index="138"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #17 rib_7_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="17" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="196"/>
<atomRef index="197"/>
<atomRef index="198"/>
<atomRef index="199"/>
<atomRef index="200"/>
<atomRef index="201"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #18 rib_7_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="18" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="265"/>
<atomRef index="266"/>
<atomRef index="267"/>
<atomRef index="268"/>
<atomRef index="269"/>
<atomRef index="270"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #19 rib_8_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="19" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="204"/>
<atomRef index="205"/>
<atomRef index="206"/>
<atomRef index="207"/>
<atomRef index="208"/>
<atomRef index="209"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #20 rib_8_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="20" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="6"/>
<atomRef index="273"/>
<atomRef index="274"/>
<atomRef index="275"/>
<atomRef index="276"/>
<atomRef index="277"/>
<atomRef index="278"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #21 rib_9_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="21" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="5"/>
<atomRef index="212"/>
<atomRef index="213"/>
<atomRef index="214"/>
<atomRef index="215"/>
<atomRef index="216"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #22 rib_9_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="22" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="5"/>
<atomRef index="281"/>
<atomRef index="282"/>
<atomRef index="283"/>
<atomRef index="284"/>
<atomRef index="285"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #23 rib_10_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="23" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="4"/>
<atomRef index="307"/>
<atomRef index="308"/>
<atomRef index="309"/>
<atomRef index="310"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #24 rib_10_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="24" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="5"/>
<atomRef index="292"/>
<atomRef index="293"/>
<atomRef index="294"/>
<atomRef index="295"/>
<atomRef index="296"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #25 rib_11_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="25" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="4"/>
<atomRef index="312"/>
<atomRef index="313"/>
<atomRef index="314"/>
<atomRef index="315"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #26 rib_11_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="26" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="4"/>
<atomRef index="297"/>
<atomRef index="298"/>
<atomRef index="299"/>
<atomRef index="300"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #27 rib_12_R">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="27" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="4"/>
<atomRef index="343"/>
<atomRef index="344"/>
<atomRef index="345"/>
<atomRef index="346"/>
</cell>
</structuralComponent>
<structuralComponent name="Region #28 rib_12_L">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="1"/>
<cell>
<cellProperties index="28" type="POLY_VERTEX" phymulType="solid"/>
<nrOfStructures value="4"/>
<atomRef index="347"/>
<atomRef index="348"/>
<atomRef index="349"/>
<atomRef index="350"/>
</cell>
</structuralComponent>
</multiComponent>
</exclusiveComponents>
<!-- list of informative components : -->
<informativeComponents>
<multiComponent name="Phymul Informative Component">
<structuralComponent name="Element Neighborhoods">
<color r="0.5" g="0.5" b="0.5" a="1"/>
<nrOfStructures value="251"/>
<cell>
<cellProperties index="0" type="POLY_VERTEX" name="E#142"/>
<nrOfStructures value="4"/>
<atomRef index="211"/>
<atomRef index="143"/>
<atomRef index="40"/>
<atomRef index="203"/>
</cell>
<cell>
<cellProperties index="1" type="POLY_VERTEX" name="E#143"/>
<nrOfStructures value="1"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="2" type="POLY_VERTEX" name="E#146"/>
<nrOfStructures value="4"/>
<atomRef index="147"/>
<atomRef index="280"/>
<atomRef index="272"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="3" type="POLY_VERTEX" name="E#147"/>
<nrOfStructures value="1"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="4" type="POLY_VERTEX" name="E#211"/>
<nrOfStructures value="2"/>
<atomRef index="216"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="5" type="POLY_VERTEX" name="E#280"/>
<nrOfStructures value="2"/>
<atomRef index="285"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="6" type="POLY_VERTEX" name="E#302"/>
<nrOfStructures value="1"/>
<atomRef index="303"/>
</cell>
<cell>
<cellProperties index="7" type="POLY_VERTEX" name="E#303"/>
<nrOfStructures value="2"/>
<atomRef index="304"/>
<atomRef index="302"/>
</cell>
<cell>
<cellProperties index="8" type="POLY_VERTEX" name="E#304"/>
<nrOfStructures value="2"/>
<atomRef index="305"/>
<atomRef index="303"/>
</cell>
<cell>
<cellProperties index="9" type="POLY_VERTEX" name="E#305"/>
<nrOfStructures value="2"/>
<atomRef index="304"/>
<atomRef index="306"/>
</cell>
<cell>
<cellProperties index="10" type="POLY_VERTEX" name="E#306"/>
<nrOfStructures value="1"/>
<atomRef index="305"/>
</cell>
<cell>
<cellProperties index="11" type="POLY_VERTEX" name="E#287"/>
<nrOfStructures value="1"/>
<atomRef index="288"/>
</cell>
<cell>
<cellProperties index="12" type="POLY_VERTEX" name="E#288"/>
<nrOfStructures value="2"/>
<atomRef index="287"/>
<atomRef index="289"/>
</cell>
<cell>
<cellProperties index="13" type="POLY_VERTEX" name="E#289"/>
<nrOfStructures value="2"/>
<atomRef index="288"/>
<atomRef index="290"/>
</cell>
<cell>
<cellProperties index="14" type="POLY_VERTEX" name="E#290"/>
<nrOfStructures value="2"/>
<atomRef index="291"/>
<atomRef index="289"/>
</cell>
<cell>
<cellProperties index="15" type="POLY_VERTEX" name="E#291"/>
<nrOfStructures value="1"/>
<atomRef index="290"/>
</cell>
<cell>
<cellProperties index="16" type="POLY_VERTEX" name="E#311"/>
<nrOfStructures value="2"/>
<atomRef index="310"/>
<atomRef index="306"/>
</cell>
<cell>
<cellProperties index="17" type="POLY_VERTEX" name="E#0"/>
<nrOfStructures value="4"/>
<atomRef index="127"/>
<atomRef index="29"/>
<atomRef index="125"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="18" type="POLY_VERTEX" name="E#1"/>
<nrOfStructures value="4"/>
<atomRef index="129"/>
<atomRef index="33"/>
<atomRef index="39"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="19" type="POLY_VERTEX" name="E#4"/>
<nrOfStructures value="4"/>
<atomRef index="38"/>
<atomRef index="31"/>
<atomRef index="131"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="20" type="POLY_VERTEX" name="E#5"/>
<nrOfStructures value="4"/>
<atomRef index="29"/>
<atomRef index="51"/>
<atomRef index="35"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="21" type="POLY_VERTEX" name="E#6"/>
<nrOfStructures value="7"/>
<atomRef index="34"/>
<atomRef index="47"/>
<atomRef index="46"/>
<atomRef index="42"/>
<atomRef index="44"/>
<atomRef index="43"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="22" type="POLY_VERTEX" name="E#7"/>
<nrOfStructures value="8"/>
<atomRef index="42"/>
<atomRef index="46"/>
<atomRef index="59"/>
<atomRef index="58"/>
<atomRef index="41"/>
<atomRef index="3"/>
<atomRef index="114"/>
<atomRef index="2"/>
</cell>
<cell>
<cellProperties index="23" type="POLY_VERTEX" name="E#8"/>
<nrOfStructures value="7"/>
<atomRef index="41"/>
<atomRef index="58"/>
<atomRef index="57"/>
<atomRef index="37"/>
<atomRef index="38"/>
<atomRef index="28"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="24" type="POLY_VERTEX" name="E#9"/>
<nrOfStructures value="4"/>
<atomRef index="36"/>
<atomRef index="53"/>
<atomRef index="29"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="25" type="POLY_VERTEX" name="E#10"/>
<nrOfStructures value="14"/>
<atomRef index="53"/>
<atomRef index="54"/>
<atomRef index="55"/>
<atomRef index="56"/>
<atomRef index="57"/>
<atomRef index="58"/>
<atomRef index="59"/>
<atomRef index="46"/>
<atomRef index="47"/>
<atomRef index="48"/>
<atomRef index="49"/>
<atomRef index="50"/>
<atomRef index="51"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="26" type="POLY_VERTEX" name="E#31"/>
<nrOfStructures value="7"/>
<atomRef index="37"/>
<atomRef index="56"/>
<atomRef index="55"/>
<atomRef index="36"/>
<atomRef index="131"/>
<atomRef index="4"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="27" type="POLY_VERTEX" name="E#33"/>
<nrOfStructures value="7"/>
<atomRef index="49"/>
<atomRef index="48"/>
<atomRef index="34"/>
<atomRef index="39"/>
<atomRef index="1"/>
<atomRef index="129"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="28" type="POLY_VERTEX" name="E#34"/>
<nrOfStructures value="5"/>
<atomRef index="33"/>
<atomRef index="48"/>
<atomRef index="47"/>
<atomRef index="6"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="29" type="POLY_VERTEX" name="E#35"/>
<nrOfStructures value="7"/>
<atomRef index="5"/>
<atomRef index="51"/>
<atomRef index="50"/>
<atomRef index="49"/>
<atomRef index="33"/>
<atomRef index="129"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="30" type="POLY_VERTEX" name="E#36"/>
<nrOfStructures value="7"/>
<atomRef index="31"/>
<atomRef index="55"/>
<atomRef index="54"/>
<atomRef index="53"/>
<atomRef index="9"/>
<atomRef index="127"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="31" type="POLY_VERTEX" name="E#37"/>
<nrOfStructures value="5"/>
<atomRef index="8"/>
<atomRef index="57"/>
<atomRef index="56"/>
<atomRef index="31"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="32" type="POLY_VERTEX" name="E#38"/>
<nrOfStructures value="8"/>
<atomRef index="28"/>
<atomRef index="8"/>
<atomRef index="37"/>
<atomRef index="31"/>
<atomRef index="4"/>
<atomRef index="104"/>
<atomRef index="103"/>
<atomRef index="102"/>
</cell>
<cell>
<cellProperties index="33" type="POLY_VERTEX" name="E#39"/>
<nrOfStructures value="8"/>
<atomRef index="1"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="6"/>
<atomRef index="43"/>
<atomRef index="110"/>
<atomRef index="109"/>
<atomRef index="108"/>
</cell>
<cell>
<cellProperties index="34" type="POLY_VERTEX" name="E#28"/>
<nrOfStructures value="4"/>
<atomRef index="40"/>
<atomRef index="8"/>
<atomRef index="38"/>
<atomRef index="102"/>
</cell>
<cell>
<cellProperties index="35" type="POLY_VERTEX" name="E#29"/>
<nrOfStructures value="8"/>
<atomRef index="9"/>
<atomRef index="53"/>
<atomRef index="52"/>
<atomRef index="51"/>
<atomRef index="5"/>
<atomRef index="125"/>
<atomRef index="0"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="36" type="POLY_VERTEX" name="E#40"/>
<nrOfStructures value="11"/>
<atomRef index="3"/>
<atomRef index="41"/>
<atomRef index="336"/>
<atomRef index="194"/>
<atomRef index="195"/>
<atomRef index="8"/>
<atomRef index="203"/>
<atomRef index="142"/>
<atomRef index="28"/>
<atomRef index="102"/>
<atomRef index="101"/>
</cell>
<cell>
<cellProperties index="37" type="POLY_VERTEX" name="E#41"/>
<nrOfStructures value="5"/>
<atomRef index="7"/>
<atomRef index="58"/>
<atomRef index="8"/>
<atomRef index="40"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="38" type="POLY_VERTEX" name="E#42"/>
<nrOfStructures value="5"/>
<atomRef index="7"/>
<atomRef index="2"/>
<atomRef index="44"/>
<atomRef index="6"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="39" type="POLY_VERTEX" name="E#43"/>
<nrOfStructures value="4"/>
<atomRef index="44"/>
<atomRef index="110"/>
<atomRef index="39"/>
<atomRef index="6"/>
</cell>
<cell>
<cellProperties index="40" type="POLY_VERTEX" name="E#44"/>
<nrOfStructures value="9"/>
<atomRef index="43"/>
<atomRef index="272"/>
<atomRef index="6"/>
<atomRef index="264"/>
<atomRef index="263"/>
<atomRef index="42"/>
<atomRef index="2"/>
<atomRef index="111"/>
<atomRef index="110"/>
</cell>
<cell>
<cellProperties index="41" type="POLY_VERTEX" name="E#46"/>
<nrOfStructures value="6"/>
<atomRef index="10"/>
<atomRef index="59"/>
<atomRef index="7"/>
<atomRef index="42"/>
<atomRef index="6"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="42" type="POLY_VERTEX" name="E#47"/>
<nrOfStructures value="5"/>
<atomRef index="6"/>
<atomRef index="34"/>
<atomRef index="48"/>
<atomRef index="10"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="43" type="POLY_VERTEX" name="E#48"/>
<nrOfStructures value="5"/>
<atomRef index="34"/>
<atomRef index="33"/>
<atomRef index="49"/>
<atomRef index="10"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="44" type="POLY_VERTEX" name="E#49"/>
<nrOfStructures value="5"/>
<atomRef index="33"/>
<atomRef index="35"/>
<atomRef index="50"/>
<atomRef index="10"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="45" type="POLY_VERTEX" name="E#50"/>
<nrOfStructures value="4"/>
<atomRef index="35"/>
<atomRef index="51"/>
<atomRef index="10"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="46" type="POLY_VERTEX" name="E#51"/>
<nrOfStructures value="6"/>
<atomRef index="5"/>
<atomRef index="29"/>
<atomRef index="52"/>
<atomRef index="10"/>
<atomRef index="50"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="47" type="POLY_VERTEX" name="E#52"/>
<nrOfStructures value="4"/>
<atomRef index="29"/>
<atomRef index="53"/>
<atomRef index="10"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="48" type="POLY_VERTEX" name="E#53"/>
<nrOfStructures value="6"/>
<atomRef index="9"/>
<atomRef index="36"/>
<atomRef index="54"/>
<atomRef index="10"/>
<atomRef index="52"/>
<atomRef index="29"/>
</cell>
<cell>
<cellProperties index="49" type="POLY_VERTEX" name="E#54"/>
<nrOfStructures value="4"/>
<atomRef index="36"/>
<atomRef index="55"/>
<atomRef index="10"/>
<atomRef index="53"/>
</cell>
<cell>
<cellProperties index="50" type="POLY_VERTEX" name="E#55"/>
<nrOfStructures value="5"/>
<atomRef index="31"/>
<atomRef index="56"/>
<atomRef index="10"/>
<atomRef index="54"/>
<atomRef index="36"/>
</cell>
<cell>
<cellProperties index="51" type="POLY_VERTEX" name="E#56"/>
<nrOfStructures value="5"/>
<atomRef index="37"/>
<atomRef index="57"/>
<atomRef index="10"/>
<atomRef index="55"/>
<atomRef index="31"/>
</cell>
<cell>
<cellProperties index="52" type="POLY_VERTEX" name="E#57"/>
<nrOfStructures value="5"/>
<atomRef index="8"/>
<atomRef index="58"/>
<atomRef index="10"/>
<atomRef index="56"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="53" type="POLY_VERTEX" name="E#58"/>
<nrOfStructures value="6"/>
<atomRef index="41"/>
<atomRef index="7"/>
<atomRef index="59"/>
<atomRef index="10"/>
<atomRef index="57"/>
<atomRef index="8"/>
</cell>
<cell>
<cellProperties index="54" type="POLY_VERTEX" name="E#59"/>
<nrOfStructures value="4"/>
<atomRef index="7"/>
<atomRef index="46"/>
<atomRef index="10"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="55" type="POLY_VERTEX" name="E#125"/>
<nrOfStructures value="7"/>
<atomRef index="0"/>
<atomRef index="29"/>
<atomRef index="5"/>
<atomRef index="35"/>
<atomRef index="129"/>
<atomRef index="107"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="56" type="POLY_VERTEX" name="E#127"/>
<nrOfStructures value="7"/>
<atomRef index="131"/>
<atomRef index="36"/>
<atomRef index="9"/>
<atomRef index="29"/>
<atomRef index="0"/>
<atomRef index="106"/>
<atomRef index="105"/>
</cell>
<cell>
<cellProperties index="57" type="POLY_VERTEX" name="E#129"/>
<nrOfStructures value="7"/>
<atomRef index="125"/>
<atomRef index="35"/>
<atomRef index="33"/>
<atomRef index="1"/>
<atomRef index="108"/>
<atomRef index="121"/>
<atomRef index="107"/>
</cell>
<cell>
<cellProperties index="58" type="POLY_VERTEX" name="E#131"/>
<nrOfStructures value="7"/>
<atomRef index="4"/>
<atomRef index="31"/>
<atomRef index="36"/>
<atomRef index="127"/>
<atomRef index="105"/>
<atomRef index="123"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="59" type="POLY_VERTEX" name="E#2"/>
<nrOfStructures value="7"/>
<atomRef index="42"/>
<atomRef index="7"/>
<atomRef index="114"/>
<atomRef index="115"/>
<atomRef index="112"/>
<atomRef index="111"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="60" type="POLY_VERTEX" name="E#3"/>
<nrOfStructures value="7"/>
<atomRef index="114"/>
<atomRef index="7"/>
<atomRef index="41"/>
<atomRef index="40"/>
<atomRef index="101"/>
<atomRef index="100"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="61" type="POLY_VERTEX" name="E#114"/>
<nrOfStructures value="4"/>
<atomRef index="2"/>
<atomRef index="7"/>
<atomRef index="3"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="62" type="POLY_VERTEX" name="E#100"/>
<nrOfStructures value="4"/>
<atomRef index="115"/>
<atomRef index="3"/>
<atomRef index="101"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="63" type="POLY_VERTEX" name="E#101"/>
<nrOfStructures value="5"/>
<atomRef index="100"/>
<atomRef index="3"/>
<atomRef index="40"/>
<atomRef index="102"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="64" type="POLY_VERTEX" name="E#102"/>
<nrOfStructures value="6"/>
<atomRef index="101"/>
<atomRef index="40"/>
<atomRef index="28"/>
<atomRef index="38"/>
<atomRef index="103"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="65" type="POLY_VERTEX" name="E#103"/>
<nrOfStructures value="4"/>
<atomRef index="102"/>
<atomRef index="38"/>
<atomRef index="104"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="66" type="POLY_VERTEX" name="E#104"/>
<nrOfStructures value="6"/>
<atomRef index="103"/>
<atomRef index="38"/>
<atomRef index="4"/>
<atomRef index="131"/>
<atomRef index="123"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="67" type="POLY_VERTEX" name="E#105"/>
<nrOfStructures value="5"/>
<atomRef index="123"/>
<atomRef index="131"/>
<atomRef index="127"/>
<atomRef index="106"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="68" type="POLY_VERTEX" name="E#106"/>
<nrOfStructures value="6"/>
<atomRef index="105"/>
<atomRef index="127"/>
<atomRef index="0"/>
<atomRef index="125"/>
<atomRef index="107"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="69" type="POLY_VERTEX" name="E#107"/>
<nrOfStructures value="5"/>
<atomRef index="106"/>
<atomRef index="125"/>
<atomRef index="129"/>
<atomRef index="121"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="70" type="POLY_VERTEX" name="E#108"/>
<nrOfStructures value="6"/>
<atomRef index="121"/>
<atomRef index="129"/>
<atomRef index="1"/>
<atomRef index="39"/>
<atomRef index="109"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="71" type="POLY_VERTEX" name="E#109"/>
<nrOfStructures value="4"/>
<atomRef index="108"/>
<atomRef index="39"/>
<atomRef index="110"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="72" type="POLY_VERTEX" name="E#110"/>
<nrOfStructures value="6"/>
<atomRef index="109"/>
<atomRef index="39"/>
<atomRef index="43"/>
<atomRef index="44"/>
<atomRef index="111"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="73" type="POLY_VERTEX" name="E#111"/>
<nrOfStructures value="5"/>
<atomRef index="110"/>
<atomRef index="44"/>
<atomRef index="2"/>
<atomRef index="112"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="74" type="POLY_VERTEX" name="E#112"/>
<nrOfStructures value="4"/>
<atomRef index="111"/>
<atomRef index="2"/>
<atomRef index="115"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="75" type="POLY_VERTEX" name="E#113"/>
<nrOfStructures value="16"/>
<atomRef index="100"/>
<atomRef index="101"/>
<atomRef index="102"/>
<atomRef index="103"/>
<atomRef index="104"/>
<atomRef index="123"/>
<atomRef index="105"/>
<atomRef index="106"/>
<atomRef index="107"/>
<atomRef index="121"/>
<atomRef index="108"/>
<atomRef index="109"/>
<atomRef index="110"/>
<atomRef index="111"/>
<atomRef index="112"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="76" type="POLY_VERTEX" name="E#115"/>
<nrOfStructures value="6"/>
<atomRef index="100"/>
<atomRef index="113"/>
<atomRef index="112"/>
<atomRef index="2"/>
<atomRef index="114"/>
<atomRef index="3"/>
</cell>
<cell>
<cellProperties index="77" type="POLY_VERTEX" name="E#121"/>
<nrOfStructures value="4"/>
<atomRef index="107"/>
<atomRef index="129"/>
<atomRef index="108"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="78" type="POLY_VERTEX" name="E#123"/>
<nrOfStructures value="4"/>
<atomRef index="104"/>
<atomRef index="131"/>
<atomRef index="105"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="79" type="POLY_VERTEX" name="E#193"/>
<nrOfStructures value="3"/>
<atomRef index="194"/>
<atomRef index="337"/>
<atomRef index="331"/>
</cell>
<cell>
<cellProperties index="80" type="POLY_VERTEX" name="E#316"/>
<nrOfStructures value="3"/>
<atomRef index="321"/>
<atomRef index="320"/>
<atomRef index="317"/>
</cell>
<cell>
<cellProperties index="81" type="POLY_VERTEX" name="E#317"/>
<nrOfStructures value="3"/>
<atomRef index="316"/>
<atomRef index="320"/>
<atomRef index="318"/>
</cell>
<cell>
<cellProperties index="82" type="POLY_VERTEX" name="E#318"/>
<nrOfStructures value="3"/>
<atomRef index="317"/>
<atomRef index="320"/>
<atomRef index="319"/>
</cell>
<cell>
<cellProperties index="83" type="POLY_VERTEX" name="E#319"/>
<nrOfStructures value="4"/>
<atomRef index="318"/>
<atomRef index="320"/>
<atomRef index="324"/>
<atomRef index="323"/>
</cell>
<cell>
<cellProperties index="84" type="POLY_VERTEX" name="E#320"/>
<nrOfStructures value="6"/>
<atomRef index="319"/>
<atomRef index="318"/>
<atomRef index="317"/>
<atomRef index="316"/>
<atomRef index="321"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="85" type="POLY_VERTEX" name="E#321"/>
<nrOfStructures value="4"/>
<atomRef index="320"/>
<atomRef index="316"/>
<atomRef index="322"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="86" type="POLY_VERTEX" name="E#322"/>
<nrOfStructures value="4"/>
<atomRef index="324"/>
<atomRef index="321"/>
<atomRef index="325"/>
<atomRef index="326"/>
</cell>
<cell>
<cellProperties index="87" type="POLY_VERTEX" name="E#323"/>
<nrOfStructures value="4"/>
<atomRef index="319"/>
<atomRef index="324"/>
<atomRef index="326"/>
<atomRef index="327"/>
</cell>
<cell>
<cellProperties index="88" type="POLY_VERTEX" name="E#324"/>
<nrOfStructures value="6"/>
<atomRef index="323"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="326"/>
</cell>
<cell>
<cellProperties index="89" type="POLY_VERTEX" name="E#325"/>
<nrOfStructures value="4"/>
<atomRef index="326"/>
<atomRef index="322"/>
<atomRef index="328"/>
<atomRef index="329"/>
</cell>
<cell>
<cellProperties index="90" type="POLY_VERTEX" name="E#326"/>
<nrOfStructures value="6"/>
<atomRef index="327"/>
<atomRef index="323"/>
<atomRef index="324"/>
<atomRef index="322"/>
<atomRef index="325"/>
<atomRef index="329"/>
</cell>
<cell>
<cellProperties index="91" type="POLY_VERTEX" name="E#327"/>
<nrOfStructures value="4"/>
<atomRef index="323"/>
<atomRef index="326"/>
<atomRef index="329"/>
<atomRef index="330"/>
</cell>
<cell>
<cellProperties index="92" type="POLY_VERTEX" name="E#328"/>
<nrOfStructures value="4"/>
<atomRef index="333"/>
<atomRef index="329"/>
<atomRef index="325"/>
<atomRef index="331"/>
</cell>
<cell>
<cellProperties index="93" type="POLY_VERTEX" name="E#329"/>
<nrOfStructures value="6"/>
<atomRef index="330"/>
<atomRef index="327"/>
<atomRef index="326"/>
<atomRef index="325"/>
<atomRef index="328"/>
<atomRef index="333"/>
</cell>
<cell>
<cellProperties index="94" type="POLY_VERTEX" name="E#330"/>
<nrOfStructures value="4"/>
<atomRef index="327"/>
<atomRef index="329"/>
<atomRef index="333"/>
<atomRef index="332"/>
</cell>
<cell>
<cellProperties index="95" type="POLY_VERTEX" name="E#331"/>
<nrOfStructures value="6"/>
<atomRef index="333"/>
<atomRef index="328"/>
<atomRef index="195"/>
<atomRef index="194"/>
<atomRef index="193"/>
<atomRef index="337"/>
</cell>
<cell>
<cellProperties index="96" type="POLY_VERTEX" name="E#332"/>
<nrOfStructures value="6"/>
<atomRef index="330"/>
<atomRef index="333"/>
<atomRef index="337"/>
<atomRef index="338"/>
<atomRef index="263"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="97" type="POLY_VERTEX" name="E#333"/>
<nrOfStructures value="6"/>
<atomRef index="332"/>
<atomRef index="330"/>
<atomRef index="329"/>
<atomRef index="328"/>
<atomRef index="331"/>
<atomRef index="337"/>
</cell>
<cell>
<cellProperties index="98" type="POLY_VERTEX" name="E#337"/>
<nrOfStructures value="5"/>
<atomRef index="338"/>
<atomRef index="332"/>
<atomRef index="333"/>
<atomRef index="331"/>
<atomRef index="336"/>
</cell>
<cell>
<cellProperties index="99" type="POLY_VERTEX" name="E#338"/>
<nrOfStructures value="4"/>
<atomRef index="332"/>
<atomRef index="337"/>
<atomRef index="342"/>
<atomRef index="263"/>
</cell>
<cell>
<cellProperties index="100" type="POLY_VERTEX" name="E#341"/>
<nrOfStructures value="9"/>
<atomRef index="336"/>
<atomRef index="194"/>
<atomRef index="337"/>
<atomRef index="331"/>
<atomRef index="8"/>
<atomRef index="40"/>
<atomRef index="3"/>
<atomRef index="7"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="101" type="POLY_VERTEX" name="E#342"/>
<nrOfStructures value="4"/>
<atomRef index="338"/>
<atomRef index="2"/>
<atomRef index="146"/>
<atomRef index="272"/>
</cell>
<cell>
<cellProperties index="102" type="POLY_VERTEX" name="E#194"/>
<nrOfStructures value="5"/>
<atomRef index="336"/>
<atomRef index="193"/>
<atomRef index="186"/>
<atomRef index="195"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="103" type="POLY_VERTEX" name="E#195"/>
<nrOfStructures value="5"/>
<atomRef index="201"/>
<atomRef index="203"/>
<atomRef index="40"/>
<atomRef index="194"/>
<atomRef index="186"/>
</cell>
<cell>
<cellProperties index="104" type="POLY_VERTEX" name="E#336"/>
<nrOfStructures value="4"/>
<atomRef index="331"/>
<atomRef index="194"/>
<atomRef index="341"/>
<atomRef index="337"/>
</cell>
<cell>
<cellProperties index="105" type="POLY_VERTEX" name="E#263"/>
<nrOfStructures value="5"/>
<atomRef index="338"/>
<atomRef index="42"/>
<atomRef index="44"/>
<atomRef index="264"/>
<atomRef index="332"/>
</cell>
<cell>
<cellProperties index="106" type="POLY_VERTEX" name="E#264"/>
<nrOfStructures value="5"/>
<atomRef index="44"/>
<atomRef index="270"/>
<atomRef index="332"/>
<atomRef index="263"/>
<atomRef index="272"/>
</cell>
<cell>
<cellProperties index="107" type="POLY_VERTEX" name="E#203"/>
<nrOfStructures value="4"/>
<atomRef index="209"/>
<atomRef index="142"/>
<atomRef index="40"/>
<atomRef index="195"/>
</cell>
<cell>
<cellProperties index="108" type="POLY_VERTEX" name="E#272"/>
<nrOfStructures value="4"/>
<atomRef index="278"/>
<atomRef index="264"/>
<atomRef index="44"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="109" type="POLY_VERTEX" name="E#171"/>
<nrOfStructures value="1"/>
<atomRef index="176"/>
</cell>
<cell>
<cellProperties index="110" type="POLY_VERTEX" name="E#172"/>
<nrOfStructures value="1"/>
<atomRef index="173"/>
</cell>
<cell>
<cellProperties index="111" type="POLY_VERTEX" name="E#173"/>
<nrOfStructures value="2"/>
<atomRef index="174"/>
<atomRef index="172"/>
</cell>
<cell>
<cellProperties index="112" type="POLY_VERTEX" name="E#174"/>
<nrOfStructures value="2"/>
<atomRef index="175"/>
<atomRef index="173"/>
</cell>
<cell>
<cellProperties index="113" type="POLY_VERTEX" name="E#175"/>
<nrOfStructures value="2"/>
<atomRef index="176"/>
<atomRef index="174"/>
</cell>
<cell>
<cellProperties index="114" type="POLY_VERTEX" name="E#176"/>
<nrOfStructures value="2"/>
<atomRef index="171"/>
<atomRef index="175"/>
</cell>
<cell>
<cellProperties index="115" type="POLY_VERTEX" name="E#240"/>
<nrOfStructures value="1"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="116" type="POLY_VERTEX" name="E#241"/>
<nrOfStructures value="1"/>
<atomRef index="242"/>
</cell>
<cell>
<cellProperties index="117" type="POLY_VERTEX" name="E#242"/>
<nrOfStructures value="2"/>
<atomRef index="241"/>
<atomRef index="243"/>
</cell>
<cell>
<cellProperties index="118" type="POLY_VERTEX" name="E#243"/>
<nrOfStructures value="2"/>
<atomRef index="242"/>
<atomRef index="244"/>
</cell>
<cell>
<cellProperties index="119" type="POLY_VERTEX" name="E#244"/>
<nrOfStructures value="2"/>
<atomRef index="243"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="120" type="POLY_VERTEX" name="E#245"/>
<nrOfStructures value="2"/>
<atomRef index="244"/>
<atomRef index="240"/>
</cell>
<cell>
<cellProperties index="121" type="POLY_VERTEX" name="E#164"/>
<nrOfStructures value="1"/>
<atomRef index="169"/>
</cell>
<cell>
<cellProperties index="122" type="POLY_VERTEX" name="E#165"/>
<nrOfStructures value="1"/>
<atomRef index="166"/>
</cell>
<cell>
<cellProperties index="123" type="POLY_VERTEX" name="E#166"/>
<nrOfStructures value="2"/>
<atomRef index="167"/>
<atomRef index="165"/>
</cell>
<cell>
<cellProperties index="124" type="POLY_VERTEX" name="E#167"/>
<nrOfStructures value="2"/>
<atomRef index="166"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="125" type="POLY_VERTEX" name="E#168"/>
<nrOfStructures value="2"/>
<atomRef index="167"/>
<atomRef index="169"/>
</cell>
<cell>
<cellProperties index="126" type="POLY_VERTEX" name="E#169"/>
<nrOfStructures value="2"/>
<atomRef index="164"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="127" type="POLY_VERTEX" name="E#232"/>
<nrOfStructures value="1"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="128" type="POLY_VERTEX" name="E#234"/>
<nrOfStructures value="1"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="129" type="POLY_VERTEX" name="E#235"/>
<nrOfStructures value="2"/>
<atomRef index="234"/>
<atomRef index="236"/>
</cell>
<cell>
<cellProperties index="130" type="POLY_VERTEX" name="E#236"/>
<nrOfStructures value="2"/>
<atomRef index="237"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="131" type="POLY_VERTEX" name="E#237"/>
<nrOfStructures value="2"/>
<atomRef index="236"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="132" type="POLY_VERTEX" name="E#238"/>
<nrOfStructures value="2"/>
<atomRef index="237"/>
<atomRef index="232"/>
</cell>
<cell>
<cellProperties index="133" type="POLY_VERTEX" name="E#156"/>
<nrOfStructures value="1"/>
<atomRef index="162"/>
</cell>
<cell>
<cellProperties index="134" type="POLY_VERTEX" name="E#158"/>
<nrOfStructures value="1"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="135" type="POLY_VERTEX" name="E#159"/>
<nrOfStructures value="2"/>
<atomRef index="158"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="136" type="POLY_VERTEX" name="E#160"/>
<nrOfStructures value="2"/>
<atomRef index="161"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="137" type="POLY_VERTEX" name="E#161"/>
<nrOfStructures value="2"/>
<atomRef index="162"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="138" type="POLY_VERTEX" name="E#162"/>
<nrOfStructures value="2"/>
<atomRef index="156"/>
<atomRef index="161"/>
</cell>
<cell>
<cellProperties index="139" type="POLY_VERTEX" name="E#225"/>
<nrOfStructures value="1"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="140" type="POLY_VERTEX" name="E#227"/>
<nrOfStructures value="1"/>
<atomRef index="228"/>
</cell>
<cell>
<cellProperties index="141" type="POLY_VERTEX" name="E#228"/>
<nrOfStructures value="2"/>
<atomRef index="229"/>
<atomRef index="227"/>
</cell>
<cell>
<cellProperties index="142" type="POLY_VERTEX" name="E#229"/>
<nrOfStructures value="2"/>
<atomRef index="228"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="143" type="POLY_VERTEX" name="E#230"/>
<nrOfStructures value="2"/>
<atomRef index="229"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="144" type="POLY_VERTEX" name="E#231"/>
<nrOfStructures value="2"/>
<atomRef index="230"/>
<atomRef index="225"/>
</cell>
<cell>
<cellProperties index="145" type="POLY_VERTEX" name="E#178"/>
<nrOfStructures value="1"/>
<atomRef index="183"/>
</cell>
<cell>
<cellProperties index="146" type="POLY_VERTEX" name="E#179"/>
<nrOfStructures value="1"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="147" type="POLY_VERTEX" name="E#180"/>
<nrOfStructures value="2"/>
<atomRef index="179"/>
<atomRef index="181"/>
</cell>
<cell>
<cellProperties index="148" type="POLY_VERTEX" name="E#181"/>
<nrOfStructures value="2"/>
<atomRef index="182"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="149" type="POLY_VERTEX" name="E#182"/>
<nrOfStructures value="2"/>
<atomRef index="183"/>
<atomRef index="181"/>
</cell>
<cell>
<cellProperties index="150" type="POLY_VERTEX" name="E#183"/>
<nrOfStructures value="2"/>
<atomRef index="178"/>
<atomRef index="182"/>
</cell>
<cell>
<cellProperties index="151" type="POLY_VERTEX" name="E#247"/>
<nrOfStructures value="1"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="152" type="POLY_VERTEX" name="E#248"/>
<nrOfStructures value="1"/>
<atomRef index="249"/>
</cell>
<cell>
<cellProperties index="153" type="POLY_VERTEX" name="E#249"/>
<nrOfStructures value="2"/>
<atomRef index="250"/>
<atomRef index="248"/>
</cell>
<cell>
<cellProperties index="154" type="POLY_VERTEX" name="E#250"/>
<nrOfStructures value="2"/>
<atomRef index="249"/>
<atomRef index="251"/>
</cell>
<cell>
<cellProperties index="155" type="POLY_VERTEX" name="E#251"/>
<nrOfStructures value="2"/>
<atomRef index="250"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="156" type="POLY_VERTEX" name="E#252"/>
<nrOfStructures value="2"/>
<atomRef index="251"/>
<atomRef index="247"/>
</cell>
<cell>
<cellProperties index="157" type="POLY_VERTEX" name="E#149"/>
<nrOfStructures value="1"/>
<atomRef index="155"/>
</cell>
<cell>
<cellProperties index="158" type="POLY_VERTEX" name="E#150"/>
<nrOfStructures value="1"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="159" type="POLY_VERTEX" name="E#151"/>
<nrOfStructures value="2"/>
<atomRef index="152"/>
<atomRef index="150"/>
</cell>
<cell>
<cellProperties index="160" type="POLY_VERTEX" name="E#152"/>
<nrOfStructures value="2"/>
<atomRef index="153"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="161" type="POLY_VERTEX" name="E#153"/>
<nrOfStructures value="2"/>
<atomRef index="154"/>
<atomRef index="152"/>
</cell>
<cell>
<cellProperties index="162" type="POLY_VERTEX" name="E#154"/>
<nrOfStructures value="2"/>
<atomRef index="155"/>
<atomRef index="153"/>
</cell>
<cell>
<cellProperties index="163" type="POLY_VERTEX" name="E#155"/>
<nrOfStructures value="2"/>
<atomRef index="149"/>
<atomRef index="154"/>
</cell>
<cell>
<cellProperties index="164" type="POLY_VERTEX" name="E#218"/>
<nrOfStructures value="1"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="165" type="POLY_VERTEX" name="E#219"/>
<nrOfStructures value="1"/>
<atomRef index="220"/>
</cell>
<cell>
<cellProperties index="166" type="POLY_VERTEX" name="E#220"/>
<nrOfStructures value="2"/>
<atomRef index="219"/>
<atomRef index="221"/>
</cell>
<cell>
<cellProperties index="167" type="POLY_VERTEX" name="E#221"/>
<nrOfStructures value="2"/>
<atomRef index="220"/>
<atomRef index="222"/>
</cell>
<cell>
<cellProperties index="168" type="POLY_VERTEX" name="E#222"/>
<nrOfStructures value="2"/>
<atomRef index="221"/>
<atomRef index="223"/>
</cell>
<cell>
<cellProperties index="169" type="POLY_VERTEX" name="E#223"/>
<nrOfStructures value="2"/>
<atomRef index="222"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="170" type="POLY_VERTEX" name="E#224"/>
<nrOfStructures value="2"/>
<atomRef index="223"/>
<atomRef index="218"/>
</cell>
<cell>
<cellProperties index="171" type="POLY_VERTEX" name="E#186"/>
<nrOfStructures value="1"/>
<atomRef index="192"/>
</cell>
<cell>
<cellProperties index="172" type="POLY_VERTEX" name="E#187"/>
<nrOfStructures value="1"/>
<atomRef index="188"/>
</cell>
<cell>
<cellProperties index="173" type="POLY_VERTEX" name="E#188"/>
<nrOfStructures value="2"/>
<atomRef index="189"/>
<atomRef index="187"/>
</cell>
<cell>
<cellProperties index="174" type="POLY_VERTEX" name="E#189"/>
<nrOfStructures value="2"/>
<atomRef index="190"/>
<atomRef index="188"/>
</cell>
<cell>
<cellProperties index="175" type="POLY_VERTEX" name="E#190"/>
<nrOfStructures value="2"/>
<atomRef index="191"/>
<atomRef index="189"/>
</cell>
<cell>
<cellProperties index="176" type="POLY_VERTEX" name="E#191"/>
<nrOfStructures value="2"/>
<atomRef index="192"/>
<atomRef index="190"/>
</cell>
<cell>
<cellProperties index="177" type="POLY_VERTEX" name="E#192"/>
<nrOfStructures value="2"/>
<atomRef index="186"/>
<atomRef index="191"/>
</cell>
<cell>
<cellProperties index="178" type="POLY_VERTEX" name="E#255"/>
<nrOfStructures value="1"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="179" type="POLY_VERTEX" name="E#256"/>
<nrOfStructures value="1"/>
<atomRef index="257"/>
</cell>
<cell>
<cellProperties index="180" type="POLY_VERTEX" name="E#257"/>
<nrOfStructures value="2"/>
<atomRef index="256"/>
<atomRef index="258"/>
</cell>
<cell>
<cellProperties index="181" type="POLY_VERTEX" name="E#258"/>
<nrOfStructures value="2"/>
<atomRef index="257"/>
<atomRef index="259"/>
</cell>
<cell>
<cellProperties index="182" type="POLY_VERTEX" name="E#259"/>
<nrOfStructures value="2"/>
<atomRef index="258"/>
<atomRef index="260"/>
</cell>
<cell>
<cellProperties index="183" type="POLY_VERTEX" name="E#260"/>
<nrOfStructures value="2"/>
<atomRef index="259"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="184" type="POLY_VERTEX" name="E#261"/>
<nrOfStructures value="2"/>
<atomRef index="260"/>
<atomRef index="255"/>
</cell>
<cell>
<cellProperties index="185" type="POLY_VERTEX" name="E#239"/>
<nrOfStructures value="1"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="186" type="POLY_VERTEX" name="E#133"/>
<nrOfStructures value="1"/>
<atomRef index="134"/>
</cell>
<cell>
<cellProperties index="187" type="POLY_VERTEX" name="E#134"/>
<nrOfStructures value="2"/>
<atomRef index="133"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="188" type="POLY_VERTEX" name="E#135"/>
<nrOfStructures value="2"/>
<atomRef index="134"/>
<atomRef index="136"/>
</cell>
<cell>
<cellProperties index="189" type="POLY_VERTEX" name="E#136"/>
<nrOfStructures value="2"/>
<atomRef index="135"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="190" type="POLY_VERTEX" name="E#137"/>
<nrOfStructures value="2"/>
<atomRef index="136"/>
<atomRef index="138"/>
</cell>
<cell>
<cellProperties index="191" type="POLY_VERTEX" name="E#138"/>
<nrOfStructures value="1"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="192" type="POLY_VERTEX" name="E#196"/>
<nrOfStructures value="1"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="193" type="POLY_VERTEX" name="E#197"/>
<nrOfStructures value="2"/>
<atomRef index="198"/>
<atomRef index="196"/>
</cell>
<cell>
<cellProperties index="194" type="POLY_VERTEX" name="E#198"/>
<nrOfStructures value="2"/>
<atomRef index="199"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="195" type="POLY_VERTEX" name="E#199"/>
<nrOfStructures value="2"/>
<atomRef index="200"/>
<atomRef index="198"/>
</cell>
<cell>
<cellProperties index="196" type="POLY_VERTEX" name="E#200"/>
<nrOfStructures value="2"/>
<atomRef index="201"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="197" type="POLY_VERTEX" name="E#201"/>
<nrOfStructures value="2"/>
<atomRef index="195"/>
<atomRef index="200"/>
</cell>
<cell>
<cellProperties index="198" type="POLY_VERTEX" name="E#265"/>
<nrOfStructures value="1"/>
<atomRef index="266"/>
</cell>
<cell>
<cellProperties index="199" type="POLY_VERTEX" name="E#266"/>
<nrOfStructures value="2"/>
<atomRef index="265"/>
<atomRef index="267"/>
</cell>
<cell>
<cellProperties index="200" type="POLY_VERTEX" name="E#267"/>
<nrOfStructures value="2"/>
<atomRef index="266"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="201" type="POLY_VERTEX" name="E#268"/>
<nrOfStructures value="2"/>
<atomRef index="267"/>
<atomRef index="269"/>
</cell>
<cell>
<cellProperties index="202" type="POLY_VERTEX" name="E#269"/>
<nrOfStructures value="2"/>
<atomRef index="268"/>
<atomRef index="270"/>
</cell>
<cell>
<cellProperties index="203" type="POLY_VERTEX" name="E#270"/>
<nrOfStructures value="2"/>
<atomRef index="269"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="204" type="POLY_VERTEX" name="E#204"/>
<nrOfStructures value="1"/>
<atomRef index="205"/>
</cell>
<cell>
<cellProperties index="205" type="POLY_VERTEX" name="E#205"/>
<nrOfStructures value="2"/>
<atomRef index="206"/>
<atomRef index="204"/>
</cell>
<cell>
<cellProperties index="206" type="POLY_VERTEX" name="E#206"/>
<nrOfStructures value="2"/>
<atomRef index="205"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="207" type="POLY_VERTEX" name="E#207"/>
<nrOfStructures value="2"/>
<atomRef index="208"/>
<atomRef index="206"/>
</cell>
<cell>
<cellProperties index="208" type="POLY_VERTEX" name="E#208"/>
<nrOfStructures value="2"/>
<atomRef index="209"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="209" type="POLY_VERTEX" name="E#209"/>
<nrOfStructures value="2"/>
<atomRef index="203"/>
<atomRef index="208"/>
</cell>
<cell>
<cellProperties index="210" type="POLY_VERTEX" name="E#273"/>
<nrOfStructures value="1"/>
<atomRef index="274"/>
</cell>
<cell>
<cellProperties index="211" type="POLY_VERTEX" name="E#274"/>
<nrOfStructures value="2"/>
<atomRef index="273"/>
<atomRef index="275"/>
</cell>
<cell>
<cellProperties index="212" type="POLY_VERTEX" name="E#275"/>
<nrOfStructures value="2"/>
<atomRef index="276"/>
<atomRef index="274"/>
</cell>
<cell>
<cellProperties index="213" type="POLY_VERTEX" name="E#276"/>
<nrOfStructures value="2"/>
<atomRef index="275"/>
<atomRef index="277"/>
</cell>
<cell>
<cellProperties index="214" type="POLY_VERTEX" name="E#277"/>
<nrOfStructures value="2"/>
<atomRef index="276"/>
<atomRef index="278"/>
</cell>
<cell>
<cellProperties index="215" type="POLY_VERTEX" name="E#278"/>
<nrOfStructures value="2"/>
<atomRef index="277"/>
<atomRef index="272"/>
</cell>
<cell>
<cellProperties index="216" type="POLY_VERTEX" name="E#212"/>
<nrOfStructures value="1"/>
<atomRef index="213"/>
</cell>
<cell>
<cellProperties index="217" type="POLY_VERTEX" name="E#213"/>
<nrOfStructures value="2"/>
<atomRef index="214"/>
<atomRef index="212"/>
</cell>
<cell>
<cellProperties index="218" type="POLY_VERTEX" name="E#214"/>
<nrOfStructures value="2"/>
<atomRef index="213"/>
<atomRef index="215"/>
</cell>
<cell>
<cellProperties index="219" type="POLY_VERTEX" name="E#215"/>
<nrOfStructures value="2"/>
<atomRef index="216"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="220" type="POLY_VERTEX" name="E#216"/>
<nrOfStructures value="2"/>
<atomRef index="215"/>
<atomRef index="211"/>
</cell>
<cell>
<cellProperties index="221" type="POLY_VERTEX" name="E#281"/>
<nrOfStructures value="1"/>
<atomRef index="282"/>
</cell>
<cell>
<cellProperties index="222" type="POLY_VERTEX" name="E#282"/>
<nrOfStructures value="2"/>
<atomRef index="281"/>
<atomRef index="283"/>
</cell>
<cell>
<cellProperties index="223" type="POLY_VERTEX" name="E#283"/>
<nrOfStructures value="2"/>
<atomRef index="284"/>
<atomRef index="282"/>
</cell>
<cell>
<cellProperties index="224" type="POLY_VERTEX" name="E#284"/>
<nrOfStructures value="2"/>
<atomRef index="283"/>
<atomRef index="285"/>
</cell>
<cell>
<cellProperties index="225" type="POLY_VERTEX" name="E#285"/>
<nrOfStructures value="2"/>
<atomRef index="280"/>
<atomRef index="284"/>
</cell>
<cell>
<cellProperties index="226" type="POLY_VERTEX" name="E#307"/>
<nrOfStructures value="1"/>
<atomRef index="308"/>
</cell>
<cell>
<cellProperties index="227" type="POLY_VERTEX" name="E#308"/>
<nrOfStructures value="2"/>
<atomRef index="307"/>
<atomRef index="309"/>
</cell>
<cell>
<cellProperties index="228" type="POLY_VERTEX" name="E#309"/>
<nrOfStructures value="2"/>
<atomRef index="308"/>
<atomRef index="310"/>
</cell>
<cell>
<cellProperties index="229" type="POLY_VERTEX" name="E#310"/>
<nrOfStructures value="2"/>
<atomRef index="309"/>
<atomRef index="311"/>
</cell>
<cell>
<cellProperties index="230" type="POLY_VERTEX" name="E#292"/>
<nrOfStructures value="1"/>
<atomRef index="293"/>
</cell>
<cell>
<cellProperties index="231" type="POLY_VERTEX" name="E#293"/>
<nrOfStructures value="2"/>
<atomRef index="294"/>
<atomRef index="292"/>
</cell>
<cell>
<cellProperties index="232" type="POLY_VERTEX" name="E#294"/>
<nrOfStructures value="2"/>
<atomRef index="295"/>
<atomRef index="293"/>
</cell>
<cell>
<cellProperties index="233" type="POLY_VERTEX" name="E#295"/>
<nrOfStructures value="2"/>
<atomRef index="296"/>
<atomRef index="294"/>
</cell>
<cell>
<cellProperties index="234" type="POLY_VERTEX" name="E#296"/>
<nrOfStructures value="2"/>
<atomRef index="295"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="235" type="POLY_VERTEX" name="E#312"/>
<nrOfStructures value="1"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="236" type="POLY_VERTEX" name="E#313"/>
<nrOfStructures value="2"/>
<atomRef index="314"/>
<atomRef index="312"/>
</cell>
<cell>
<cellProperties index="237" type="POLY_VERTEX" name="E#314"/>
<nrOfStructures value="2"/>
<atomRef index="313"/>
<atomRef index="315"/>
</cell>
<cell>
<cellProperties index="238" type="POLY_VERTEX" name="E#315"/>
<nrOfStructures value="2"/>
<atomRef index="314"/>
<atomRef index="304"/>
</cell>
<cell>
<cellProperties index="239" type="POLY_VERTEX" name="E#297"/>
<nrOfStructures value="1"/>
<atomRef index="298"/>
</cell>
<cell>
<cellProperties index="240" type="POLY_VERTEX" name="E#298"/>
<nrOfStructures value="2"/>
<atomRef index="297"/>
<atomRef index="299"/>
</cell>
<cell>
<cellProperties index="241" type="POLY_VERTEX" name="E#299"/>
<nrOfStructures value="2"/>
<atomRef index="300"/>
<atomRef index="298"/>
</cell>
<cell>
<cellProperties index="242" type="POLY_VERTEX" name="E#300"/>
<nrOfStructures value="2"/>
<atomRef index="299"/>
<atomRef index="289"/>
</cell>
<cell>
<cellProperties index="243" type="POLY_VERTEX" name="E#343"/>
<nrOfStructures value="1"/>
<atomRef index="344"/>
</cell>
<cell>
<cellProperties index="244" type="POLY_VERTEX" name="E#344"/>
<nrOfStructures value="2"/>
<atomRef index="343"/>
<atomRef index="345"/>
</cell>
<cell>
<cellProperties index="245" type="POLY_VERTEX" name="E#345"/>
<nrOfStructures value="2"/>
<atomRef index="344"/>
<atomRef index="346"/>
</cell>
<cell>
<cellProperties index="246" type="POLY_VERTEX" name="E#346"/>
<nrOfStructures value="1"/>
<atomRef index="345"/>
</cell>
<cell>
<cellProperties index="247" type="POLY_VERTEX" name="E#347"/>
<nrOfStructures value="1"/>
<atomRef index="348"/>
</cell>
<cell>
<cellProperties index="248" type="POLY_VERTEX" name="E#348"/>
<nrOfStructures value="2"/>
<atomRef index="347"/>
<atomRef index="349"/>
</cell>
<cell>
<cellProperties index="249" type="POLY_VERTEX" name="E#349"/>
<nrOfStructures value="2"/>
<atomRef index="348"/>
<atomRef index="350"/>
</cell>
<cell>
<cellProperties index="250" type="POLY_VERTEX" name="E#350"/>
<nrOfStructures value="1"/>
<atomRef index="349"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #0 (cage_tho)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="22"/>
<cell>
<cellProperties index="251" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="142"/>
<atomRef index="211"/>
</cell>
<cell>
<cellProperties index="252" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="142"/>
<atomRef index="143"/>
</cell>
<cell>
<cellProperties index="253" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="142"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="254" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="142"/>
<atomRef index="203"/>
</cell>
<cell>
<cellProperties index="255" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="143"/>
<atomRef index="142"/>
</cell>
<cell>
<cellProperties index="256" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="146"/>
<atomRef index="147"/>
</cell>
<cell>
<cellProperties index="257" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="146"/>
<atomRef index="280"/>
</cell>
<cell>
<cellProperties index="258" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="146"/>
<atomRef index="272"/>
</cell>
<cell>
<cellProperties index="259" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="146"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="260" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="211"/>
<atomRef index="216"/>
</cell>
<cell>
<cellProperties index="261" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="280"/>
<atomRef index="285"/>
</cell>
<cell>
<cellProperties index="262" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="280"/>
<atomRef index="146"/>
</cell>
<cell>
<cellProperties index="263" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="302"/>
<atomRef index="303"/>
</cell>
<cell>
<cellProperties index="264" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="303"/>
<atomRef index="304"/>
</cell>
<cell>
<cellProperties index="265" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="304"/>
<atomRef index="305"/>
</cell>
<cell>
<cellProperties index="266" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="305"/>
<atomRef index="306"/>
</cell>
<cell>
<cellProperties index="267" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="287"/>
<atomRef index="288"/>
</cell>
<cell>
<cellProperties index="268" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="288"/>
<atomRef index="289"/>
</cell>
<cell>
<cellProperties index="269" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="289"/>
<atomRef index="290"/>
</cell>
<cell>
<cellProperties index="270" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="290"/>
<atomRef index="291"/>
</cell>
<cell>
<cellProperties index="271" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="311"/>
<atomRef index="310"/>
</cell>
<cell>
<cellProperties index="272" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="311"/>
<atomRef index="306"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #1 (diaphragme)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.2" b="0.2" a="1"/>
<nrOfStructures value="78"/>
<cell>
<cellProperties index="273" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="127"/>
<atomRef index="29"/>
</cell>
<cell>
<cellProperties index="274" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="0"/>
<atomRef index="29"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="275" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="129"/>
<atomRef index="33"/>
</cell>
<cell>
<cellProperties index="276" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="1"/>
<atomRef index="33"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="277" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="38"/>
<atomRef index="31"/>
</cell>
<cell>
<cellProperties index="278" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="4"/>
<atomRef index="31"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="279" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="29"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="280" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="51"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="281" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="35"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="282" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="5"/>
<atomRef index="125"/>
<atomRef index="29"/>
</cell>
<cell>
<cellProperties index="283" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="34"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="284" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="47"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="285" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="46"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="286" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="42"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="287" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="44"/>
<atomRef index="43"/>
</cell>
<cell>
<cellProperties index="288" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="43"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="289" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="6"/>
<atomRef index="39"/>
<atomRef index="34"/>
</cell>
<cell>
<cellProperties index="290" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="42"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="291" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="46"/>
<atomRef index="59"/>
</cell>
<cell>
<cellProperties index="292" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="59"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="293" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="7"/>
<atomRef index="58"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="294" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="41"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="295" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="58"/>
<atomRef index="57"/>
</cell>
<cell>
<cellProperties index="296" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="57"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="297" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="37"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="298" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="38"/>
<atomRef index="28"/>
</cell>
<cell>
<cellProperties index="299" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="28"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="300" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="8"/>
<atomRef index="40"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="301" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="36"/>
<atomRef index="53"/>
</cell>
<cell>
<cellProperties index="302" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="53"/>
<atomRef index="29"/>
</cell>
<cell>
<cellProperties index="303" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="29"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="304" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="9"/>
<atomRef index="127"/>
<atomRef index="36"/>
</cell>
<cell>
<cellProperties index="305" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="53"/>
<atomRef index="54"/>
</cell>
<cell>
<cellProperties index="306" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="54"/>
<atomRef index="55"/>
</cell>
<cell>
<cellProperties index="307" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="55"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="308" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="56"/>
<atomRef index="57"/>
</cell>
<cell>
<cellProperties index="309" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="57"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="310" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="58"/>
<atomRef index="59"/>
</cell>
<cell>
<cellProperties index="311" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="59"/>
<atomRef index="46"/>
</cell>
<cell>
<cellProperties index="312" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="46"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="313" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="47"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="314" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="48"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="315" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="49"/>
<atomRef index="50"/>
</cell>
<cell>
<cellProperties index="316" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="50"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="317" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="51"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="318" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="10"/>
<atomRef index="52"/>
<atomRef index="53"/>
</cell>
<cell>
<cellProperties index="319" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="37"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="320" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="56"/>
<atomRef index="55"/>
</cell>
<cell>
<cellProperties index="321" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="55"/>
<atomRef index="36"/>
</cell>
<cell>
<cellProperties index="322" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="36"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="323" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="131"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="324" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="4"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="325" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="31"/>
<atomRef index="38"/>
<atomRef index="37"/>
</cell>
<cell>
<cellProperties index="326" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="49"/>
<atomRef index="48"/>
</cell>
<cell>
<cellProperties index="327" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="48"/>
<atomRef index="34"/>
</cell>
<cell>
<cellProperties index="328" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="34"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="329" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="39"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="330" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="1"/>
<atomRef index="129"/>
</cell>
<cell>
<cellProperties index="331" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="129"/>
<atomRef index="35"/>
</cell>
<cell>
<cellProperties index="332" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="33"/>
<atomRef index="35"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="333" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="34"/>
<atomRef index="48"/>
<atomRef index="47"/>
</cell>
<cell>
<cellProperties index="334" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="35"/>
<atomRef index="51"/>
<atomRef index="50"/>
</cell>
<cell>
<cellProperties index="335" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="35"/>
<atomRef index="50"/>
<atomRef index="49"/>
</cell>
<cell>
<cellProperties index="336" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="35"/>
<atomRef index="129"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="337" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="36"/>
<atomRef index="55"/>
<atomRef index="54"/>
</cell>
<cell>
<cellProperties index="338" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="36"/>
<atomRef index="54"/>
<atomRef index="53"/>
</cell>
<cell>
<cellProperties index="339" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="36"/>
<atomRef index="127"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="340" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="37"/>
<atomRef index="57"/>
<atomRef index="56"/>
</cell>
<cell>
<cellProperties index="341" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="53"/>
<atomRef index="52"/>
</cell>
<cell>
<cellProperties index="342" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="52"/>
<atomRef index="51"/>
</cell>
<cell>
<cellProperties index="343" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="125"/>
<atomRef index="0"/>
</cell>
<cell>
<cellProperties index="344" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="29"/>
<atomRef index="0"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="345" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="41"/>
<atomRef index="7"/>
<atomRef index="58"/>
</cell>
<cell>
<cellProperties index="346" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="42"/>
<atomRef index="46"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="347" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="46"/>
<atomRef index="59"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="348" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="46"/>
<atomRef index="7"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="349" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="58"/>
<atomRef index="41"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="350" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="58"/>
<atomRef index="7"/>
<atomRef index="59"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #2 (abdomen)" mode="WIREFRAME_AND_SURFACE">
<color r="1" g="0.89" b="0.53" a="1"/>
<nrOfStructures value="54"/>
<cell>
<cellProperties index="351" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="42"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="352" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="7"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="353" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="114"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="354" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="115"/>
<atomRef index="112"/>
</cell>
<cell>
<cellProperties index="355" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="112"/>
<atomRef index="111"/>
</cell>
<cell>
<cellProperties index="356" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="111"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="357" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="2"/>
<atomRef index="44"/>
<atomRef index="42"/>
</cell>
<cell>
<cellProperties index="358" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="114"/>
<atomRef index="7"/>
</cell>
<cell>
<cellProperties index="359" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="7"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="360" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="41"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="361" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="40"/>
<atomRef index="101"/>
</cell>
<cell>
<cellProperties index="362" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="101"/>
<atomRef index="100"/>
</cell>
<cell>
<cellProperties index="363" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="100"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="364" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="3"/>
<atomRef index="115"/>
<atomRef index="114"/>
</cell>
<cell>
<cellProperties index="365" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="100"/>
<atomRef index="101"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="366" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="100"/>
<atomRef index="113"/>
<atomRef index="115"/>
</cell>
<cell>
<cellProperties index="367" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="101"/>
<atomRef index="40"/>
<atomRef index="102"/>
</cell>
<cell>
<cellProperties index="368" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="101"/>
<atomRef index="102"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="369" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="102"/>
<atomRef index="40"/>
<atomRef index="28"/>
</cell>
<cell>
<cellProperties index="370" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="102"/>
<atomRef index="28"/>
<atomRef index="38"/>
</cell>
<cell>
<cellProperties index="371" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="102"/>
<atomRef index="38"/>
<atomRef index="103"/>
</cell>
<cell>
<cellProperties index="372" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="102"/>
<atomRef index="103"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="373" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="103"/>
<atomRef index="38"/>
<atomRef index="104"/>
</cell>
<cell>
<cellProperties index="374" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="103"/>
<atomRef index="104"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="375" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="104"/>
<atomRef index="38"/>
<atomRef index="4"/>
</cell>
<cell>
<cellProperties index="376" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="104"/>
<atomRef index="4"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="377" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="104"/>
<atomRef index="131"/>
<atomRef index="123"/>
</cell>
<cell>
<cellProperties index="378" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="104"/>
<atomRef index="123"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="379" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="123"/>
<atomRef index="131"/>
</cell>
<cell>
<cellProperties index="380" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="131"/>
<atomRef index="127"/>
</cell>
<cell>
<cellProperties index="381" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="127"/>
<atomRef index="106"/>
</cell>
<cell>
<cellProperties index="382" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="106"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="383" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="105"/>
<atomRef index="113"/>
<atomRef index="123"/>
</cell>
<cell>
<cellProperties index="384" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="106"/>
<atomRef index="127"/>
<atomRef index="0"/>
</cell>
<cell>
<cellProperties index="385" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="106"/>
<atomRef index="0"/>
<atomRef index="125"/>
</cell>
<cell>
<cellProperties index="386" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="106"/>
<atomRef index="125"/>
<atomRef index="107"/>
</cell>
<cell>
<cellProperties index="387" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="106"/>
<atomRef index="107"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="388" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="125"/>
<atomRef index="129"/>
</cell>
<cell>
<cellProperties index="389" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="129"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="390" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="107"/>
<atomRef index="121"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="391" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="121"/>
<atomRef index="129"/>
</cell>
<cell>
<cellProperties index="392" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="129"/>
<atomRef index="1"/>
</cell>
<cell>
<cellProperties index="393" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="1"/>
<atomRef index="39"/>
</cell>
<cell>
<cellProperties index="394" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="39"/>
<atomRef index="109"/>
</cell>
<cell>
<cellProperties index="395" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="109"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="396" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="108"/>
<atomRef index="113"/>
<atomRef index="121"/>
</cell>
<cell>
<cellProperties index="397" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="109"/>
<atomRef index="39"/>
<atomRef index="110"/>
</cell>
<cell>
<cellProperties index="398" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="109"/>
<atomRef index="110"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="399" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="110"/>
<atomRef index="39"/>
<atomRef index="43"/>
</cell>
<cell>
<cellProperties index="400" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="110"/>
<atomRef index="43"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="401" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="110"/>
<atomRef index="44"/>
<atomRef index="111"/>
</cell>
<cell>
<cellProperties index="402" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="110"/>
<atomRef index="111"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="403" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="111"/>
<atomRef index="112"/>
<atomRef index="113"/>
</cell>
<cell>
<cellProperties index="404" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="112"/>
<atomRef index="115"/>
<atomRef index="113"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #3 (cartillage)" mode="WIREFRAME_AND_SURFACE">
<color r="1" g="0.89" b="0.53" a="1"/>
<nrOfStructures value="36"/>
<cell>
<cellProperties index="405" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="193"/>
<atomRef index="337"/>
<atomRef index="331"/>
</cell>
<cell>
<cellProperties index="406" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="316"/>
<atomRef index="321"/>
<atomRef index="320"/>
</cell>
<cell>
<cellProperties index="407" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="316"/>
<atomRef index="320"/>
<atomRef index="317"/>
</cell>
<cell>
<cellProperties index="408" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="317"/>
<atomRef index="320"/>
<atomRef index="318"/>
</cell>
<cell>
<cellProperties index="409" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="318"/>
<atomRef index="320"/>
<atomRef index="319"/>
</cell>
<cell>
<cellProperties index="410" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="319"/>
<atomRef index="320"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="411" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="319"/>
<atomRef index="324"/>
<atomRef index="323"/>
</cell>
<cell>
<cellProperties index="412" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="320"/>
<atomRef index="321"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="413" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="321"/>
<atomRef index="322"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="414" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="322"/>
<atomRef index="325"/>
<atomRef index="326"/>
</cell>
<cell>
<cellProperties index="415" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="322"/>
<atomRef index="326"/>
<atomRef index="324"/>
</cell>
<cell>
<cellProperties index="416" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="323"/>
<atomRef index="324"/>
<atomRef index="326"/>
</cell>
<cell>
<cellProperties index="417" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="323"/>
<atomRef index="326"/>
<atomRef index="327"/>
</cell>
<cell>
<cellProperties index="418" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="325"/>
<atomRef index="328"/>
<atomRef index="329"/>
</cell>
<cell>
<cellProperties index="419" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="325"/>
<atomRef index="329"/>
<atomRef index="326"/>
</cell>
<cell>
<cellProperties index="420" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="326"/>
<atomRef index="329"/>
<atomRef index="327"/>
</cell>
<cell>
<cellProperties index="421" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="327"/>
<atomRef index="329"/>
<atomRef index="330"/>
</cell>
<cell>
<cellProperties index="422" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="328"/>
<atomRef index="333"/>
<atomRef index="329"/>
</cell>
<cell>
<cellProperties index="423" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="328"/>
<atomRef index="331"/>
<atomRef index="333"/>
</cell>
<cell>
<cellProperties index="424" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="329"/>
<atomRef index="333"/>
<atomRef index="330"/>
</cell>
<cell>
<cellProperties index="425" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="330"/>
<atomRef index="333"/>
<atomRef index="332"/>
</cell>
<cell>
<cellProperties index="426" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="331"/>
<atomRef index="195"/>
<atomRef index="194"/>
</cell>
<cell>
<cellProperties index="427" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="331"/>
<atomRef index="337"/>
<atomRef index="333"/>
</cell>
<cell>
<cellProperties index="428" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="332"/>
<atomRef index="333"/>
<atomRef index="337"/>
</cell>
<cell>
<cellProperties index="429" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="332"/>
<atomRef index="337"/>
<atomRef index="338"/>
</cell>
<cell>
<cellProperties index="430" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="332"/>
<atomRef index="338"/>
<atomRef index="263"/>
</cell>
<cell>
<cellProperties index="431" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="332"/>
<atomRef index="263"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="432" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="341"/>
<atomRef index="336"/>
<atomRef index="194"/>
</cell>
<cell>
<cellProperties index="433" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="342"/>
<atomRef index="146"/>
<atomRef index="272"/>
</cell>
<cell>
<cellProperties index="434" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="194"/>
<atomRef index="195"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="435" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="331"/>
<atomRef index="194"/>
<atomRef index="41"/>
</cell>
<cell>
<cellProperties index="436" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="195"/>
<atomRef index="203"/>
<atomRef index="40"/>
</cell>
<cell>
<cellProperties index="437" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="263"/>
<atomRef index="42"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="438" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="263"/>
<atomRef index="44"/>
<atomRef index="264"/>
</cell>
<cell>
<cellProperties index="439" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="264"/>
<atomRef index="272"/>
<atomRef index="44"/>
</cell>
<cell>
<cellProperties index="440" type="TRIANGLE"/>
<nrOfStructures value="3"/>
<atomRef index="203"/>
<atomRef index="142"/>
<atomRef index="40"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #4 (rib_1_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="441" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="171"/>
<atomRef index="176"/>
</cell>
<cell>
<cellProperties index="442" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="172"/>
<atomRef index="173"/>
</cell>
<cell>
<cellProperties index="443" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="173"/>
<atomRef index="174"/>
</cell>
<cell>
<cellProperties index="444" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="174"/>
<atomRef index="175"/>
</cell>
<cell>
<cellProperties index="445" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="175"/>
<atomRef index="176"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #5 (rib_1_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="446" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="240"/>
<atomRef index="245"/>
</cell>
<cell>
<cellProperties index="447" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="241"/>
<atomRef index="242"/>
</cell>
<cell>
<cellProperties index="448" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="242"/>
<atomRef index="243"/>
</cell>
<cell>
<cellProperties index="449" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="243"/>
<atomRef index="244"/>
</cell>
<cell>
<cellProperties index="450" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="244"/>
<atomRef index="245"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #6 (rib_2_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="451" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="164"/>
<atomRef index="169"/>
</cell>
<cell>
<cellProperties index="452" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="165"/>
<atomRef index="166"/>
</cell>
<cell>
<cellProperties index="453" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="166"/>
<atomRef index="167"/>
</cell>
<cell>
<cellProperties index="454" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="167"/>
<atomRef index="168"/>
</cell>
<cell>
<cellProperties index="455" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="168"/>
<atomRef index="169"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #7 (rib_2_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="456" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="232"/>
<atomRef index="238"/>
</cell>
<cell>
<cellProperties index="457" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="234"/>
<atomRef index="235"/>
</cell>
<cell>
<cellProperties index="458" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="235"/>
<atomRef index="236"/>
</cell>
<cell>
<cellProperties index="459" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="236"/>
<atomRef index="237"/>
</cell>
<cell>
<cellProperties index="460" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="237"/>
<atomRef index="238"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #8 (rib_3_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="461" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="156"/>
<atomRef index="162"/>
</cell>
<cell>
<cellProperties index="462" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="158"/>
<atomRef index="159"/>
</cell>
<cell>
<cellProperties index="463" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="159"/>
<atomRef index="160"/>
</cell>
<cell>
<cellProperties index="464" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="160"/>
<atomRef index="161"/>
</cell>
<cell>
<cellProperties index="465" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="161"/>
<atomRef index="162"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #9 (rib_3_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="466" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="225"/>
<atomRef index="231"/>
</cell>
<cell>
<cellProperties index="467" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="227"/>
<atomRef index="228"/>
</cell>
<cell>
<cellProperties index="468" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="228"/>
<atomRef index="229"/>
</cell>
<cell>
<cellProperties index="469" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="229"/>
<atomRef index="230"/>
</cell>
<cell>
<cellProperties index="470" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="230"/>
<atomRef index="231"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #10 (rib_4_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="471" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="178"/>
<atomRef index="183"/>
</cell>
<cell>
<cellProperties index="472" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="179"/>
<atomRef index="180"/>
</cell>
<cell>
<cellProperties index="473" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="180"/>
<atomRef index="181"/>
</cell>
<cell>
<cellProperties index="474" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="181"/>
<atomRef index="182"/>
</cell>
<cell>
<cellProperties index="475" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="182"/>
<atomRef index="183"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #11 (rib_4_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="476" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="247"/>
<atomRef index="252"/>
</cell>
<cell>
<cellProperties index="477" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="248"/>
<atomRef index="249"/>
</cell>
<cell>
<cellProperties index="478" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="249"/>
<atomRef index="250"/>
</cell>
<cell>
<cellProperties index="479" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="250"/>
<atomRef index="251"/>
</cell>
<cell>
<cellProperties index="480" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="251"/>
<atomRef index="252"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #12 (rib_5_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="481" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="149"/>
<atomRef index="155"/>
</cell>
<cell>
<cellProperties index="482" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="150"/>
<atomRef index="151"/>
</cell>
<cell>
<cellProperties index="483" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="151"/>
<atomRef index="152"/>
</cell>
<cell>
<cellProperties index="484" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="152"/>
<atomRef index="153"/>
</cell>
<cell>
<cellProperties index="485" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="153"/>
<atomRef index="154"/>
</cell>
<cell>
<cellProperties index="486" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="154"/>
<atomRef index="155"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #13 (rib_5_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="487" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="218"/>
<atomRef index="224"/>
</cell>
<cell>
<cellProperties index="488" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="219"/>
<atomRef index="220"/>
</cell>
<cell>
<cellProperties index="489" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="220"/>
<atomRef index="221"/>
</cell>
<cell>
<cellProperties index="490" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="221"/>
<atomRef index="222"/>
</cell>
<cell>
<cellProperties index="491" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="222"/>
<atomRef index="223"/>
</cell>
<cell>
<cellProperties index="492" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="223"/>
<atomRef index="224"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #14 (rib_6_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="493" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="186"/>
<atomRef index="192"/>
</cell>
<cell>
<cellProperties index="494" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="187"/>
<atomRef index="188"/>
</cell>
<cell>
<cellProperties index="495" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="188"/>
<atomRef index="189"/>
</cell>
<cell>
<cellProperties index="496" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="189"/>
<atomRef index="190"/>
</cell>
<cell>
<cellProperties index="497" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="190"/>
<atomRef index="191"/>
</cell>
<cell>
<cellProperties index="498" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="191"/>
<atomRef index="192"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #15 (rib_6_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="499" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="255"/>
<atomRef index="261"/>
</cell>
<cell>
<cellProperties index="500" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="256"/>
<atomRef index="257"/>
</cell>
<cell>
<cellProperties index="501" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="257"/>
<atomRef index="258"/>
</cell>
<cell>
<cellProperties index="502" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="258"/>
<atomRef index="259"/>
</cell>
<cell>
<cellProperties index="503" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="259"/>
<atomRef index="260"/>
</cell>
<cell>
<cellProperties index="504" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="260"/>
<atomRef index="261"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #16 (sternum)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="505" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="239"/>
<atomRef index="133"/>
</cell>
<cell>
<cellProperties index="506" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="133"/>
<atomRef index="134"/>
</cell>
<cell>
<cellProperties index="507" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="134"/>
<atomRef index="135"/>
</cell>
<cell>
<cellProperties index="508" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="135"/>
<atomRef index="136"/>
</cell>
<cell>
<cellProperties index="509" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="136"/>
<atomRef index="137"/>
</cell>
<cell>
<cellProperties index="510" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="137"/>
<atomRef index="138"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #17 (rib_7_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="511" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="196"/>
<atomRef index="197"/>
</cell>
<cell>
<cellProperties index="512" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="197"/>
<atomRef index="198"/>
</cell>
<cell>
<cellProperties index="513" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="198"/>
<atomRef index="199"/>
</cell>
<cell>
<cellProperties index="514" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="199"/>
<atomRef index="200"/>
</cell>
<cell>
<cellProperties index="515" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="200"/>
<atomRef index="201"/>
</cell>
<cell>
<cellProperties index="516" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="201"/>
<atomRef index="195"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #18 (rib_7_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="517" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="265"/>
<atomRef index="266"/>
</cell>
<cell>
<cellProperties index="518" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="266"/>
<atomRef index="267"/>
</cell>
<cell>
<cellProperties index="519" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="267"/>
<atomRef index="268"/>
</cell>
<cell>
<cellProperties index="520" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="268"/>
<atomRef index="269"/>
</cell>
<cell>
<cellProperties index="521" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="269"/>
<atomRef index="270"/>
</cell>
<cell>
<cellProperties index="522" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="270"/>
<atomRef index="264"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #19 (rib_8_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="523" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="204"/>
<atomRef index="205"/>
</cell>
<cell>
<cellProperties index="524" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="205"/>
<atomRef index="206"/>
</cell>
<cell>
<cellProperties index="525" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="206"/>
<atomRef index="207"/>
</cell>
<cell>
<cellProperties index="526" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="207"/>
<atomRef index="208"/>
</cell>
<cell>
<cellProperties index="527" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="208"/>
<atomRef index="209"/>
</cell>
<cell>
<cellProperties index="528" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="209"/>
<atomRef index="203"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #20 (rib_8_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="6"/>
<cell>
<cellProperties index="529" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="273"/>
<atomRef index="274"/>
</cell>
<cell>
<cellProperties index="530" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="274"/>
<atomRef index="275"/>
</cell>
<cell>
<cellProperties index="531" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="275"/>
<atomRef index="276"/>
</cell>
<cell>
<cellProperties index="532" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="276"/>
<atomRef index="277"/>
</cell>
<cell>
<cellProperties index="533" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="277"/>
<atomRef index="278"/>
</cell>
<cell>
<cellProperties index="534" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="278"/>
<atomRef index="272"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #21 (rib_9_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="535" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="212"/>
<atomRef index="213"/>
</cell>
<cell>
<cellProperties index="536" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="213"/>
<atomRef index="214"/>
</cell>
<cell>
<cellProperties index="537" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="214"/>
<atomRef index="215"/>
</cell>
<cell>
<cellProperties index="538" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="215"/>
<atomRef index="216"/>
</cell>
<cell>
<cellProperties index="539" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="216"/>
<atomRef index="211"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #22 (rib_9_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="540" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="281"/>
<atomRef index="282"/>
</cell>
<cell>
<cellProperties index="541" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="282"/>
<atomRef index="283"/>
</cell>
<cell>
<cellProperties index="542" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="283"/>
<atomRef index="284"/>
</cell>
<cell>
<cellProperties index="543" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="284"/>
<atomRef index="285"/>
</cell>
<cell>
<cellProperties index="544" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="285"/>
<atomRef index="280"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #23 (rib_10_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="4"/>
<cell>
<cellProperties index="545" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="307"/>
<atomRef index="308"/>
</cell>
<cell>
<cellProperties index="546" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="308"/>
<atomRef index="309"/>
</cell>
<cell>
<cellProperties index="547" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="309"/>
<atomRef index="310"/>
</cell>
<cell>
<cellProperties index="548" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="310"/>
<atomRef index="311"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #24 (rib_10_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="5"/>
<cell>
<cellProperties index="549" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="292"/>
<atomRef index="293"/>
</cell>
<cell>
<cellProperties index="550" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="293"/>
<atomRef index="294"/>
</cell>
<cell>
<cellProperties index="551" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="294"/>
<atomRef index="295"/>
</cell>
<cell>
<cellProperties index="552" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="295"/>
<atomRef index="296"/>
</cell>
<cell>
<cellProperties index="553" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="296"/>
<atomRef index="147"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #25 (rib_11_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="4"/>
<cell>
<cellProperties index="554" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="312"/>
<atomRef index="313"/>
</cell>
<cell>
<cellProperties index="555" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="313"/>
<atomRef index="314"/>
</cell>
<cell>
<cellProperties index="556" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="314"/>
<atomRef index="315"/>
</cell>
<cell>
<cellProperties index="557" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="315"/>
<atomRef index="304"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #26 (rib_11_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="4"/>
<cell>
<cellProperties index="558" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="297"/>
<atomRef index="298"/>
</cell>
<cell>
<cellProperties index="559" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="298"/>
<atomRef index="299"/>
</cell>
<cell>
<cellProperties index="560" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="299"/>
<atomRef index="300"/>
</cell>
<cell>
<cellProperties index="561" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="300"/>
<atomRef index="289"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #27 (rib_12_R)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="3"/>
<cell>
<cellProperties index="562" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="343"/>
<atomRef index="344"/>
</cell>
<cell>
<cellProperties index="563" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="344"/>
<atomRef index="345"/>
</cell>
<cell>
<cellProperties index="564" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="345"/>
<atomRef index="346"/>
</cell>
</structuralComponent>
<structuralComponent name="Facet of region #28 (rib_12_L)" mode="WIREFRAME_AND_SURFACE">
<color r="0.8" g="0.8" b="0.8" a="1"/>
<nrOfStructures value="3"/>
<cell>
<cellProperties index="565" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="347"/>
<atomRef index="348"/>
</cell>
<cell>
<cellProperties index="566" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="348"/>
<atomRef index="349"/>
</cell>
<cell>
<cellProperties index="567" type="POLY_LINE"/>
<nrOfStructures value="2"/>
<atomRef index="349"/>
<atomRef index="350"/>
</cell>
</structuralComponent>
</multiComponent>
</informativeComponents>
</physicalModel>
