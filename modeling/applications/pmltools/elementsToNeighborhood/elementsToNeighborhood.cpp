/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <iostream>
#include <string>
using namespace std;

#include <pml/PhysicalModel.h>
#include <pml/MultiComponent.h>
#include <pml/StructuralComponent.h>
#include <pml/Atom.h>

/// class facet to old and compare facet
class Facet {
public:
    /// create a facet using size nodes and their indexes
    Facet(unsigned int size, unsigned int id[]);

    /// destructor
    virtual ~Facet();

    /// if it is the same (equivalent) facet, increment used (return true if equivalence)
    bool testEquivalence(unsigned int size, unsigned int id[]);

    /// return the corresponding PML cell
    Cell* getCell(PhysicalModel*) const;

    /// print on stdout
    void debug();

    /// get the number of time it is being used
    unsigned int getUsed() const;

private:
    /// is this atom index present in this facet (no check on the order)
    bool isIn(unsigned int) const;

    /// the facet atom indexes
    unsigned int* id;

    /// nr of atoms composing the facet (3 = triangle, 4 = quad)
    unsigned int size;

    /// nr of times the facet is used
    unsigned int used;
};


// -------------------- constructor/destructor ------------------------
Facet::Facet(unsigned int size, unsigned int id[]) {
    this->size = size;
    this->id = new unsigned int[size];
    for (unsigned int i = 0; i < size; i++) {
        this->id[i] = id[i];
    }
    used = 1;
}

Facet::~Facet() {
    delete [] id;
}

// -------------------- debug ------------------------
void Facet::debug() {
    switch (size) {
        case 3:
            cout << "triangle <";
            break;
        case 4:
            cout << "quad <";
            break;
        default:
            cout << "unknown facet <";
            break;
    }
    unsigned int i;
    for (i = 0; i < size - 1; i++) {
        cout << id[i] << ",";
    }
    cout << id[i] << "> used " << used << " times" << endl;
}


// -------------------- testEquivalence ------------------------
bool Facet::testEquivalence(unsigned int size, unsigned int id[]) {
    if (this->size != size) {
        return false;
    }
    else {
        unsigned int i = 0;
        while (i < size && isIn(id[i])) {
            i++;
        }

        if (i == size) {
            used++;
        }

        return (i == size);
    }
}

// -------------------- isIn ------------------------
bool Facet::isIn(unsigned int index) const {
    unsigned int i = 0;
    while (i < size && id[i] != index) {
        i++;
    }
    return (i != size);
}

// -------------------- getCell ------------------------
Cell* Facet::getCell(PhysicalModel* pm) const {
    Cell* c;
    // create the correct geometric type cell
    switch (size) {
        case 3:
            c = new Cell(NULL, StructureProperties::TRIANGLE);
            break;
        case 4:
            c = new Cell(NULL, StructureProperties::QUAD);
            break;
        default:
            c = NULL;
    }
    // get the atom corresponding to the index stored in id
    // and insert them in the cell
    for (unsigned int i = 0; i < size; i++) {
        Atom* a = pm->getAtom(id[i]);
        if (a == NULL) {
            cout << "Argh! Cannot find atom #" << id[i] << endl;
        }
        else {
            c->addStructureIfNotIn(a);
        }
    }
    return c;
}

// -------------------- getUsed ------------------------
unsigned int Facet::getUsed() const {
    return used;
}

// -------------------- Neigborhood Map ------------------------
// associative map of all the neighboors for a given index of an atom
map<unsigned int, Cell*> neighMap;

// -------------------- getIterator ------------------------
// get the iterator on the correct atom index in the neighMap
// if non existant create it
map<unsigned int, Cell*>::iterator getIterator(unsigned int index) {
    map<unsigned int, Cell*>::iterator it;

    // find atom index in the map
    it = neighMap.find(index);

    // not present, insert a new cell, associated with index
    if (it == neighMap.end()) {
        // instanciate
        Cell* nc = new Cell(NULL, StructureProperties::POLY_VERTEX);
        // name
        stringstream n(std::stringstream::out);
        n << "Atom #" << index << '\0';
        ((Structure*)nc)->setName(n.str());
        // insert the new cell in the map
        pair<map<unsigned int, Cell*>::iterator, bool> ins = neighMap.insert(map<unsigned int, Cell*>::value_type(index, nc));
        // it show where it has been inserted
        it = ins.first;
    }

    return it;
}

// -------------------- generateNeighborhood ------------------------
/// generate the neighborhoods
StructuralComponent* generateNeighborhood(StructuralComponent* sc) {
    // an iterator
    map<unsigned int, Cell*>::iterator it;

    // for each cells recreate neighborhoods
    for (unsigned int i = 0; i < sc->getNumberOfCells(); i++) {
        // c is an hexahedron
        Cell* c = sc->getCell(i);

        switch (c->getType()) {
            case StructureProperties::TRIANGLE:
                if (c->getNumberOfStructures() != 3) {
                    cout << "cell #" << c->getIndex() << " of type TRIANGLE does not contains 3 atoms (found only " <<  c->getNumberOfStructures() << "). Cell skipped." << endl;
                }
                else {
                    // TRIANGLE
                    //       1      facet:   lines:
                    //      / \     0,1,2    0,1
                    //     /   \             1,2
                    //    2-----0            2,0

                    // -- add atom 0 neighbors
                    // get neighbors list for atom 0
                    it = getIterator(c->getStructure(0)->getIndex());
                    // it.second is the cell containing all the atoms that are neighbors of atom 0
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    (*it).second->addStructureIfNotIn(c->getStructure(2));

                    // -- add atom 1 neighbors
                    it = getIterator(c->getStructure(1)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    (*it).second->addStructureIfNotIn(c->getStructure(0));

                    // -- add atom 2 neighbors
                    it = getIterator(c->getStructure(2)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                }
                break;
            case StructureProperties::QUAD:
                if (c->getNumberOfStructures() != 4) {
                    cout << "cell #" << c->getIndex() << " of type QUAD does not contains 4 atoms (found only " <<  c->getNumberOfStructures() << "). Cell skipped." << endl;
                }
                else {
                    // QUAD
                    //    3--------2      lines:
                    //    |        |      0,1
                    //    |        |      1,2
                    //    |        |      2,3
                    //    0--------1      3,0

                    // -- add atom 0 neighbors
                    // get neighbors list for atom 0
                    it = getIterator(c->getStructure(0)->getIndex());
                    // it.second is the cell containing all the atoms that are neighbors of atom 0
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    // complete neighborhood
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    (*it).second->addStructureIfNotIn(c->getStructure(3));

                    // -- add atom 1 neighbors
                    it = getIterator(c->getStructure(1)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    // complete neighborhood
                    (*it).second->addStructureIfNotIn(c->getStructure(3));
                    (*it).second->addStructureIfNotIn(c->getStructure(0));

                    // -- add atom 2 neighbors
                    it = getIterator(c->getStructure(2)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(3));
                    // complete neighborhood
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    (*it).second->addStructureIfNotIn(c->getStructure(1));

                    // -- add atom 3 neighbors
                    it = getIterator(c->getStructure(3)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    // complete neighborhood
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                }
                break;
            case StructureProperties::WEDGE:
                if (c->getNumberOfStructures() != 6) {
                    cout << "cell #" << c->getIndex() << " of type WEDGE does not contains 6 atoms (found only " <<  c->getNumberOfStructures() << "). Cell skipped." << endl;
                }
                else {
                    // WEDGE
                    //     1-------------4       facets (quad):   facets (triangles):     lines:
                    //     /\           . \       2,5,4,1          0,2,1                   0,1      2,5
                    //    /  \         /   \      0,1,4,3          3,4,5                   0,2      3,4
                    //   0- - \ - - - 3     \     2,0,3,5                                  1,2      4,5
                    //     \   \         \   \                                             0,3      5,3
                    //       \ 2-----------\--5                                            1,4
                    it = getIterator(c->getStructure(0)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    (*it).second->addStructureIfNotIn(c->getStructure(3));

                    it = getIterator(c->getStructure(1)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    (*it).second->addStructureIfNotIn(c->getStructure(4));

                    it = getIterator(c->getStructure(2)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    (*it).second->addStructureIfNotIn(c->getStructure(5));

                    it = getIterator(c->getStructure(3)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(4));
                    (*it).second->addStructureIfNotIn(c->getStructure(5));
                    (*it).second->addStructureIfNotIn(c->getStructure(0));

                    it = getIterator(c->getStructure(4)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(3));
                    (*it).second->addStructureIfNotIn(c->getStructure(5));
                    (*it).second->addStructureIfNotIn(c->getStructure(1));

                    it = getIterator(c->getStructure(5)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(3));
                    (*it).second->addStructureIfNotIn(c->getStructure(4));
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                }
                break;

            case StructureProperties::TETRAHEDRON:
                if (c->getNumberOfStructures() != 4) {
                    cout << "cell #" << c->getIndex() << " of type TETRAHEDRON does not contains 4 atoms (found only " <<  c->getNumberOfStructures() << "). Cell skipped." << endl;
                }
                else {
                    // tetrahedron are defined as follow:
                    //                    3                   triangular base: 0,1,2
                    //                  /| \                          -
                    //                 / |  \                 So to generate the neighborhodd,
                    //                2__|___\1               we just have to loop on all the
                    //                \  |   /                atoms and add them their corresponding neigh
                    //                 \ |  /                 This is easy as in a tetrahedron all atoms
                    //                  \|/                   are neighbors to all atoms...
                    //                  0
                    for (unsigned int j = 0; j < 4; j++) {
                        // get current atom
                        it = getIterator(c->getStructure(j)->getIndex());
                        // add all others to its neighborhood
                        (*it).second->addStructureIfNotIn(c->getStructure((j + 1) % 4));
                        (*it).second->addStructureIfNotIn(c->getStructure((j + 2) % 4));
                        (*it).second->addStructureIfNotIn(c->getStructure((j + 3) % 4));
                    }
                }
                break;

            case StructureProperties::HEXAHEDRON:
                if (c->getNumberOfStructures() != 8) {
                    cout << "cell #" << c->getIndex() << " of type HEXAHEDRON does not contains 8 atoms (found only " <<  c->getNumberOfStructures() << "). Cell skiped." << endl;
                }
                else {
                    // hexahedron are defined as follow:
                    //              2-------------6
                    //             / \           / \      So to generate the neighborhood,
                    //            /   \         /   \     we just have to loop on all the
                    //           1-----\-------5     \    atoms and add them their corresponding neigh
                    //           \     3-------------7
                    //            \   /         \   /
                    //             \ /           \ /
                    //              0-------------4

                    // atom 0 is neigbor of atoms : 1, 3, 4
                    it = getIterator(c->getStructure(0)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    (*it).second->addStructureIfNotIn(c->getStructure(3));
                    (*it).second->addStructureIfNotIn(c->getStructure(4));

                    // atom 1 is neigbor of atoms : 0, 2, 5
                    it = getIterator(c->getStructure(1)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    (*it).second->addStructureIfNotIn(c->getStructure(5));

                    // atom 2 is neigbor of atoms : 1, 3, 6
                    it = getIterator(c->getStructure(2)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    (*it).second->addStructureIfNotIn(c->getStructure(3));
                    (*it).second->addStructureIfNotIn(c->getStructure(6));

                    // atom 3 is neigbor of atoms : 0, 2, 7
                    it = getIterator(c->getStructure(3)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    (*it).second->addStructureIfNotIn(c->getStructure(7));

                    // atom 4 is neigbor of atoms : 0, 5, 7
                    it = getIterator(c->getStructure(4)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(0));
                    (*it).second->addStructureIfNotIn(c->getStructure(5));
                    (*it).second->addStructureIfNotIn(c->getStructure(7));

                    // atom 5 is neigbor of atoms : 1, 4, 6
                    it = getIterator(c->getStructure(5)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(1));
                    (*it).second->addStructureIfNotIn(c->getStructure(4));
                    (*it).second->addStructureIfNotIn(c->getStructure(6));

                    // atom 6 is neigbor of atoms : 2, 5, 7
                    it = getIterator(c->getStructure(6)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(2));
                    (*it).second->addStructureIfNotIn(c->getStructure(5));
                    (*it).second->addStructureIfNotIn(c->getStructure(7));

                    // atom 7 is neigbor of atoms : 3, 4, 6
                    it = getIterator(c->getStructure(7)->getIndex());
                    (*it).second->addStructureIfNotIn(c->getStructure(3));
                    (*it).second->addStructureIfNotIn(c->getStructure(4));
                    (*it).second->addStructureIfNotIn(c->getStructure(6));
                }
                break;
            default:
                cout << "Cannot generate neighborhood for cell #" << c->getIndex() << ": it is not of known type (TRIANGLE, QUAD, WEDGE, HEXAHEDRON or TETRAHEDRON)." << endl;
                break;
        }
    }

    // now that we have in neighMap all the neighborhoods, just add them in a new SC
    StructuralComponent* neigh = new StructuralComponent(NULL, "Neighborhoods");

    for (it = neighMap.begin(); it != neighMap.end(); it++) {
        neigh->addStructure((*it).second);
    }

    return neigh;
}

// -------------------- All border facets ------------------------
/// storing all the facets
vector <Facet*> allFacets;

// -------------------- equivalent ------------------------
// check if equivalent of already existing facet
void equivalent(int size, unsigned int id[]) {
    vector <Facet*>::iterator it;

    // look into allFacets for equivalence
    it = allFacets.begin();
    while (it != allFacets.end() && !(*it)->testEquivalence(size, id)) {
        it++;
    }

    // not found => insert
    if (it == allFacets.end()) {
        allFacets.push_back(new Facet(size, id));
    }
}


// -------------------- generateExternalSurface ------------------------
/// generate the outside surface
MultiComponent* generateExternalSurface(StructuralComponent* sc) {
    // outside/external facets are facets that are used in only one element

    // for each cells update the counter
    for (unsigned int i = 0; i < sc->getNumberOfCells(); i++) {
        // c is an hexahedron
        Cell* c = sc->getCell(i);

        // Facets have to be described in anticlockwise (trigonometrywise) when
        // looking at them from outside
        switch (c->getType()) {
            case StructureProperties::TRIANGLE: {
                // TRIANGLE (as viewed from outside)
                //       1      facet:   lines:
                //      / \     0,1,2    0,1
                //     /   \             1,2
                //    2-----0            2,0
                unsigned idT[3];
                idT[0] = c->getStructure(0)->getIndex();
                idT[1] = c->getStructure(1)->getIndex();
                idT[2] = c->getStructure(2)->getIndex();
                equivalent(3, idT);
                break;
            }
            case StructureProperties::QUAD: {
                // QUAD
                //    3--------2      lines:
                //    |        |      0,1
                //    |        |      1,2
                //    |        |      2,3
                //    0--------1      3,0

                unsigned idT[4];
                idT[0] = c->getStructure(0)->getIndex();
                idT[1] = c->getStructure(1)->getIndex();
                idT[2] = c->getStructure(2)->getIndex();
                idT[3] = c->getStructure(3)->getIndex();
                equivalent(4, idT);
                break;
            }
            case StructureProperties::WEDGE: {
                // WEDGE
                //     1-------------4       facets (quad):   facets (triangles):     lines:
                //     /\           . \       2,5,4,1          0,2,1                   0,1      2,5
                //    /  \         /   \      0,1,4,3          3,4,5                   0,2      3,4
                //   0- - \ - - - 3     \     2,0,3,5                                  1,2      4,5
                //     \   \         \   \                                             0,3      5,3
                //       \ 2-----------\--5                                            1,4
                unsigned int idQ[4];
                idQ[0] = c->getStructure(2)->getIndex();
                idQ[1] = c->getStructure(5)->getIndex();
                idQ[2] = c->getStructure(4)->getIndex();
                idQ[3] = c->getStructure(1)->getIndex();
                equivalent(4, idQ);

                idQ[0] = c->getStructure(0)->getIndex();
                idQ[1] = c->getStructure(1)->getIndex();
                idQ[2] = c->getStructure(4)->getIndex();
                idQ[3] = c->getStructure(3)->getIndex();
                equivalent(4, idQ);

                idQ[0] = c->getStructure(2)->getIndex();
                idQ[1] = c->getStructure(0)->getIndex();
                idQ[2] = c->getStructure(3)->getIndex();
                idQ[3] = c->getStructure(5)->getIndex();
                equivalent(4, idQ);

                unsigned int idT[3];
                idT[0] = c->getStructure(0)->getIndex();
                idT[1] = c->getStructure(2)->getIndex();
                idT[2] = c->getStructure(1)->getIndex();
                equivalent(3, idT);

                idT[0] = c->getStructure(3)->getIndex();
                idT[1] = c->getStructure(4)->getIndex();
                idT[2] = c->getStructure(5)->getIndex();
                equivalent(3, idT);
                break;
            }
            case StructureProperties::TETRAHEDRON: {
                // tetrahedron are defined as follow:
                //                    3
                //                  /| \                  So to generate the facet list,
                //                 / |  \                 we just have to loop on all the
                //                1__|___\ 2              tetrahedron and add the corresponding 4 facets :
                //                \  |   /                f0=0,1,2      f2=0,3,1
                //                 \ |  /                 f1=0,2,3      f3=2,1,3
                //                  \|/
                //                  0
                unsigned int id[3];
                id[0] = c->getStructure(0)->getIndex();
                id[1] = c->getStructure(1)->getIndex();
                id[2] = c->getStructure(2)->getIndex();
                equivalent(3, id);

                id[0] = c->getStructure(0)->getIndex();
                id[1] = c->getStructure(2)->getIndex();
                id[2] = c->getStructure(3)->getIndex();
                equivalent(3, id);

                id[0] = c->getStructure(0)->getIndex();
                id[1] = c->getStructure(3)->getIndex();
                id[2] = c->getStructure(1)->getIndex();
                equivalent(3, id);

                id[0] = c->getStructure(2)->getIndex();
                id[1] = c->getStructure(1)->getIndex();
                id[2] = c->getStructure(3)->getIndex();
                equivalent(3, id);
                break;
            }

            case StructureProperties::HEXAHEDRON: {
                // hexahedron are defined as follow:
                //              2-------------6
                //             / \           . \      So to generate the facet list,
                //            /   \         /   \     we just have to loop on all the
                //           1- - -\ - - - 5     \    hexahedron and add the corresponding 6 facets :
                //           \     3-------------7    f0=0,3,2,1     f3=3,7,6,2
                //            \   /         \   /     f1=0,4,7,3     f4=1,2,6,5
                //             \ /           . /      f2=0,1,5,4     f5=4,5,6,7
                //              0-------------4

                unsigned int id[4];
                id[0] = c->getStructure(0)->getIndex();
                id[1] = c->getStructure(3)->getIndex();
                id[2] = c->getStructure(2)->getIndex();
                id[3] = c->getStructure(1)->getIndex();
                equivalent(4, id);

                id[0] = c->getStructure(0)->getIndex();
                id[1] = c->getStructure(4)->getIndex();
                id[2] = c->getStructure(7)->getIndex();
                id[3] = c->getStructure(3)->getIndex();
                equivalent(4, id);

                id[0] = c->getStructure(0)->getIndex();
                id[1] = c->getStructure(1)->getIndex();
                id[2] = c->getStructure(5)->getIndex();
                id[3] = c->getStructure(4)->getIndex();
                equivalent(4, id);

                id[0] = c->getStructure(3)->getIndex();
                id[1] = c->getStructure(7)->getIndex();
                id[2] = c->getStructure(6)->getIndex();
                id[3] = c->getStructure(2)->getIndex();
                equivalent(4, id);

                id[0] = c->getStructure(1)->getIndex();
                id[1] = c->getStructure(2)->getIndex();
                id[2] = c->getStructure(6)->getIndex();
                id[3] = c->getStructure(5)->getIndex();
                equivalent(4, id);

                id[0] = c->getStructure(4)->getIndex();
                id[1] = c->getStructure(5)->getIndex();
                id[2] = c->getStructure(6)->getIndex();
                id[3] = c->getStructure(7)->getIndex();
                equivalent(4, id);
                break;
            }
            default:
                cout << "Cannot generate sub facets for cell #" << c->getIndex() << ": it is not of known type (TRIANGLE, QUAD, WEDGE, HEXAHEDRON or TETRAHEDRON)." << endl;
                break;
        }
    }

    // now that we have in facetMap all the facet and the number of times they have been used
    // just add in a new SC all the one that are used only once
    StructuralComponent* facet = new StructuralComponent(NULL, "extern");

    for (vector <Facet*>::iterator it = allFacets.begin(); it != allFacets.end(); it++) {
        //(*it)->debug();
        if ((*it)->getUsed() == 1) { // used only once
            facet->addStructure((*it)->getCell(sc->getPhysicalModel()));
        }
    }

    // delete all facets
    for (vector <Facet*>::iterator it = allFacets.begin(); it != allFacets.end(); it++) {
        delete (*it);
    }

    // create the enclosed volume
    MultiComponent* mc = new MultiComponent(NULL, "Enclosed Volumes");
    mc->addSubComponent(facet);

    // return the SC
    return mc;
}

// -------------------- main ------------------------
int main(int argc, char** argv) {

    if (argc != 2) {
        cout << "Usage:" << endl;
        cout << "\t" << argv[0] << " file.pml" << endl;
        cout << "generates neighborhood and external surface information, output to: file-neighborhood.pml" << endl;
        cout << "PML " << PhysicalModel::VERSION << endl;
        exit(-1);
    }

    try {
        // read the pml
        string filename(argv[1]);
        cout << "-> please wait while reading " << filename << " for adding neighborhood/enclosed volumes" << endl;
        PhysicalModel pm(filename.c_str());

        // find the structural component "Elements" in the exclusive components
        Component* c = pm.getComponentByName("Elements");
        if (!c) {
            throw PMLAbortException("Cannot find any component named \"Elements\"");
        }

        // check if it is a correct one
        if (!c->isExclusive() || !c->isInstanceOf("StructuralComponent")) {
            throw PMLAbortException("Cannot translate the \"Elements\" component: not exclusive or not a structural component.");
        }

        // check if c is made of hexahedron
        StructuralComponent* sc = (StructuralComponent*) c;
        if (sc->composedBy() != StructuralComponent::CELLS || sc->getNumberOfStructures() == 0 || (sc->getCell(0)->getType() != StructureProperties::HEXAHEDRON &&
                sc->getCell(0)->getType() != StructureProperties::TETRAHEDRON &&
                sc->getCell(0)->getType() != StructureProperties::WEDGE &&
                sc->getCell(0)->getType() != StructureProperties::TRIANGLE &&
                sc->getCell(0)->getType() != StructureProperties::QUAD)) {
            throw PMLAbortException("Cannot translate the \"Elements\" component: does not contains any cell geometric type HEXAHEDRON, WEDGE, TRIANGLE, QUAD or TETRAHEDRON. Exiting");
        }

        // now, we have the right component, let's do some work...
        cout << "-> please wait while generating neighborhood" << endl;
        StructuralComponent* neighborhoods = generateNeighborhood(sc);
        cout << "-> please wait while generating external surface" << endl;
        MultiComponent* polyhedrons = generateExternalSurface(sc);

        // add the new SC to the informative component
        if (neighborhoods) {
            pm.getExclusiveComponents()->addSubComponent(neighborhoods);
        }
        if (polyhedrons) {
            pm.getExclusiveComponents()->addSubComponent(polyhedrons);
        }

        // save the result
        unsigned int pLast = filename.rfind(".");
        if (pLast != string::npos) {
            filename.erase(pLast);
            filename += "-neighborhood.pml";
        }
        else {
            filename = "neighborhood.pml";
        }

        cout << "-> please wait while saving result in " << filename << endl;

        ofstream outputFile(filename.c_str());
        pm.setName(pm.getName() + " neighborhoods");
        // do not change cell and atom id
        pm.xmlPrint(outputFile, false);


    }
    catch (const PMLAbortException& ae) {
        cout << "AbortException: Physical model aborted:" << endl ;
        cout << ae.what() << endl;
    }
}
