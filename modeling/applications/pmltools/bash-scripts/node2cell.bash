#!/bin/bash
# if there is no problem with indexes:
cat $1 | cut -c1-8 | sed -f all-on-one-line.sed $nt1 | tail -1 | sed -e "s/  / /g" | sed -e "s/  / /g" | sed -e "s/  / /g"  > ${1/.node/.csv} 
gencells ${1/.node/.csv} >  ${1/.node/.pml}

# if there are some problems (re-indexing for example) :
# step1: extract node indexes
#cat $1 | cut -c1-8 | sed -e "s/^ *//g"   > ${1/.node/.csv} 
# - step2: openoffice to remove 1 to all indexes
# - step3: put everything on one line : 
# sed -nf all-on-one-line.sed {1/.node/.csv} > {1/.node/-2.csv}
# ou 
# for i in *.csv; do sed -nf ../all-on-one-line.sed $i > ${i/.csv/-2.csv} ;done
# - step4: generate one cell containing all atoms
# - ~/Projets/physicalModel/tools/gencells/gencells ${1/.node/-2.csv} >  ${1/.node/.pml}
# ou
# for i in *-2.csv; do ~/Projets/physicalModel/tools/gencells/gencells $i > ${i/-2.csv/.pml} ;done