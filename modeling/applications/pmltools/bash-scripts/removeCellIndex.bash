#/bin/bash
# remove all cell indexes
# usage:
# removeCellIndex.bash file.pml
file=$1
out=${1/.pml/-cleaned.pml}
echo $file processed to $out
sed -e "s/<cellProperties\(.*\)index=\"[^\"]*\"/<cellProperties \1/g" "$file" > "$out"