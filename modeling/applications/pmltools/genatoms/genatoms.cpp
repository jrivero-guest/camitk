/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
//
// C++ Implementation: genatoms
//
// Description: generate a simple pml containing only the atoms SC using a list of floats
// in a text file (floats are considered as 3d coordinates xi yi zi)
//
// Input: a text file containing
// x1 y1 z1
// ...
// xn yn zn...
//
// Output: the pml equivalent with n atoms
//
// Exemple of inputs:
//   212.949997  210.878006  49.9991989  211.138  218.839005  49.9991989
//   211.072998  214.925003  56.9520988  211.063995  206.959  56.9835014
//   211.104004  202.927002  49.9990997  211.065002  206.959  43.0144997
//   211.074005  214.925995  43.0461998  205.429993  226.548996  49.9990997
//   205.117996  224.679001  57.9211998  205.076996  218.942993  63.7588005
//
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

using namespace std;

int main(int argc, char** argv) {

    if (argc < 2) {
        cout << "Usage: " << argv[0] << " position.csv" << endl;
        exit(-1);
    }
    // open the noboite.txt file
    string filename(argv[1]);
    ifstream coordList(filename.c_str());

    // save the result in the pml file
    unsigned int pLast = filename.rfind(".");
    string basename = filename;
    if (pLast != string::npos) {
        basename.erase(pLast);
        filename = basename + "-out.pml";
    }
    else {
        filename += "-out.pml";
    }
    ofstream outputFile(filename.c_str());
    outputFile << "<!-- Created by genatoms (c) E Promayon, TIMC (c) 2005 -->" << endl;
    outputFile << "<physicalModel name=\"" << basename << "\" >" << endl;

    // vertices
    outputFile << "<atoms>" << endl;
    outputFile << "<structuralComponent name=\"vertices\">" << endl;

    double x, y, z;
    unsigned int i = 1;
    while (coordList.good()) {
        // read position
        coordList >> x;
        if (coordList.good()) {
            coordList >> y;
        }
        if (coordList.good()) {
            coordList >> z;
            // write atom
            outputFile << "<atom>" << endl;
            outputFile << "<atomProperties index=\"" << i << "\" x=\"" << x << "\"  y=\"" << y << "\" z=\"" << z << "\"  />" << endl;
            outputFile << "</atom>" << endl;
            i++;
        }
    }

    outputFile << "</structuralComponent>" << endl;
    outputFile << "</atoms>" << endl;

    // regions
    outputFile << "<exclusiveComponents>" << endl;
    outputFile << "<multiComponent name=\"Exclusive Components\">" << endl;
    /*    outputFile << "<structuralComponent name=\"Regions\" mass=\"1\" viscosityW=\"1\" >" << endl;
        outputFile << "<cell>" << endl;
        outputFile << "<cellProperties name=\"cell\" mode=\"NONE\" type=\"POLY_VERTEX\" materialType=\"elastic\" shapeW=\"300\" masterRegion=\"1\" incompressibility=\"0\" />" << endl;
        outputFile << "<color r=\"0.8\" g=\"0.8\" b=\"0.2\" a=\"1.0\" />" << endl;
        for (unsigned int i=1; i<=nVertices; i++)
            outputFile << "<atomRef index=\"" << i <<  "\"/>" << endl;
        outputFile << "</cell>" << endl;
        outputFile << "</structuralComponent>" << endl;

        // tetrahedra => elements (we can then convert the elements to neighborhood using the corresponding physicalmodel lib tool)
        unsigned int nTetra;
        nobox >> nTetra;

        outputFile << "<structuralComponent name=\"Elements\"  mode=\"WIREFRAME_AND_SURFACE\" >" << endl;
        outputFile << "<color r=\".95\" g=\"0.89\" b=\"0.63\" a=\"1.0\"/>" << endl;

        int id;
        for (unsigned int i=0;i<nTetra; i++) {
            outputFile << "<cell>" << endl;
            outputFile << "<cellProperties type=\"TETRAHEDRON\" />" << endl;
            for (unsigned int j=0; j<4; j++) {
                nobox >> id;
                outputFile << "<atomRef index=\"" << id << "\" />" << endl;
            }
            outputFile << "</cell>" << endl;
        }

        outputFile << "</structuralComponent>" << endl;*/
    outputFile << "</multiComponent>" << endl;
    outputFile << "</exclusiveComponents>" << endl;
    outputFile << "<informativeComponents>" << endl;
    outputFile << "</informativeComponents>" << endl;
    outputFile << "</physicalModel>" << endl;

    outputFile.close();

}
