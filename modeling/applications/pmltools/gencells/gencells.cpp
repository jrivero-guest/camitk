/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
//
// C++ Implementation: gencells
//
// Description:
// if one arguments given: generate on stdout a structural component containing cells using a simple text file where each
// line describes the atom indexes of a facet.
// If two arguments given: generate a POLY_VERTEX cell using all atoms indexes between 1st arg and 2nd arg
//
// Input: a text file containing
// ref0 ref1 ref2
// ref3 ref5 ref4 ref18
// ...
//
// Output: the pml structural component containing the cells
//
// Exemple of inputs:
// 0 9 6
// 1 0 6
// 9 0 7
// 0 3 5 4
//
// The output will be a structural component containing 4 cells, the first three cells are triangles, the
// last one is a quad.
//
// Input : two arguments
// Output : one pml cell containing all atoms between 1st and 2n arg
//
// Exemple of intputs:
// 14 545
// This will output a cell that uses atomRef 14, atomRef 15, atomRef 16... atomRef 545
//

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>

using namespace std;

void genFromFile(string filename) {
    // open the input.txt file
    ifstream facetList(filename.c_str());

    // output to stdout
    cout << "<structuralComponent name=\"" << filename << "\"  mode=\"WIREFRAME_AND_SURFACE\" >" << endl;
    cout << "<color r=\".95\" g=\"0.89\" b=\"0.63\" a=\"1.0\"/>" << endl;

    while (!facetList.eof()) {
        char l[200000];
        // read the next line
        if (facetList.getline(l, 199999)) {
            cout << "<cell>" << endl;
            // analyze
            std::stringstream line(l);
            std::vector<unsigned int> atomRefs;
            unsigned int id;
            while ((line >> id)) {
                atomRefs.push_back(id);
            }
            cout << "<cellProperties type=\"";
            switch (atomRefs.size()) {
                case 3:
                    cout << "TRIANGLE";
                    break;
                case 4:
                    cout << "QUAD";
                    break;
                default:
                    cout << "POLY_VERTEX";
                    break;
            }
            cout << "\"/>" << endl;
            for (unsigned int i = 0; i < atomRefs.size(); i++) {
                cout << "<atomRef index=\"" << atomRefs[i] << "\" />" << endl;
            }
            cout << "</cell>" << endl;
        }
    }

    cout << "</structuralComponent>" << endl;
}

void genFromIndex(unsigned int start, unsigned int end) {
    // output to stdout
    cout << "<structuralComponent name=\"GenCell\"  mode=\"WIREFRAME_AND_SURFACE\" >" << endl;
    cout << "<color r=\".95\" g=\"0.89\" b=\"0.63\" a=\"1.0\"/>" << endl;
    cout << "<cell>" << endl;
    cout << "<cellProperties type=\"POLY_VERTEX\"/>" << endl;
    for (unsigned int i = start; i <= end; i++) {
        cout << "<atomRef index=\"" << i << "\" />" << endl;
    }
    cout << "</cell>" << endl;
}

int main(int argc, char** argv) {
    if (argc == 3) {
        cout << "<!-- generating cell from indexes: " << argv[1] << " to " << argv[2] << " -->" << endl;
        genFromIndex(atoi(argv[1]), atoi(argv[2]));
    }
    else if (argc == 2) {
        cout << "<!-- generating cell using " << argv[1] << " -->" << endl;
        genFromFile(argv[1]);
    }
    else {
        cout << argv[0] << ", usage:" << endl;
        cout << argv[0] << " [fileName | id_start id_end]" << endl;
    }

}
