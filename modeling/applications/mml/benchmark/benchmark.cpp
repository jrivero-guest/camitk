/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <memory>   // std::auto_ptr
#include <iostream>

#include <MonitoringManager.h>
#include <MonitoringManagerFactory.h>

#ifdef MML_GENERATE_GUI
#include <QApplication>
#endif

using namespace std;

int
main(int argc, char* argv[]) {
    if (argc != 2) {
        cerr << "usage: " << argv[0] << " file.mml" << endl;
        return 1;
    }

    try {
#ifdef MML_GENERATE_GUI
        QApplication app(argc, argv);
#endif
        MonitoringManager* c = MonitoringManagerFactory::createManager(argv[1]);

        c->simulate();
        delete c;
    }
    catch (const xml_schema::exception& e) {
        cerr << e << endl;
        return 1;
    }
}

