/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include <iostream>
#include <sstream>

#include <pml/PhysicalModel.h>

#include <MonitorOut.hxx>

#ifdef MML_GENERATE_GUI
#include <QApplication>
#endif

using namespace std;

int
main(int argc, char* argv[]) {
    if (argc != 2) {
        cerr << "usage: " << argv[0] << " file.pml" << endl;
        return 1;
    }

    try {
#ifdef MML_GENERATE_GUI
        QApplication app(argc, argv);
#endif
        PhysicalModel pml = PhysicalModel(argv[1]);

        ostringstream indexes;
        ostringstream data;
        double pos[3];
        for (int i = 0; i < pml.getNumberOfAtoms(); i++) {
            Atom* a = (Atom*)(pml.getAtoms()->getStructure(i));
            indexes << a->getIndex() << " ";
            a->getPosition(pos);
            data << pos[0] << " " << pos[1] << " " << pos[2] << " ";
        }

        string filename = argv[1];
        string::size_type pLast = filename.rfind(".");
        if (pLast != string::npos) {
            filename.erase(pLast);
            filename += ".mml";
        }

        mml::MonitoringOut mml(argv[1]);
        mml::TimeStep t = mml::TimeStep(1000000, "s");
        mml::Monitor monit = mml::Monitor(mml::TimeParameter(0.0, "s"), mml::TimeParameter(1000000.0, "s"), mml::MonitorType::Position, 1, "");
        monit.data(data.str().c_str());
        monit.indexes(indexes.str().c_str());
        t.monitor().push_back(monit);
        mml::MonitoringOut::time_sequence& ts(mml.time());
        ts.push_back(t);

        xml_schema::namespace_infomap map;
        map[""].name = "";
        map[""].schema = "";

        ofstream ofs(filename.c_str());
        mml::monitoringOut(ofs, mml, map);

    }
    catch (const xml_schema::exception& e) {
        cerr << e << endl;
        return 1;
    }
}

