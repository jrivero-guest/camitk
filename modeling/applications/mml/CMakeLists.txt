# MML applications

# The mml applications are not defined as camitk applications
# (they do not use the camitk_application macro)
# because they are not publicly published yet...
# => includes, libraries and link path have to be defined manually
# and used in the subdirectories CMakeLists.txt
# respectively with:
# include_directories(${MML_INCLUDE_DIRECTORIES})
# set(applicationname_LIBRARIES ${MML_LIBRARIES} ...)
# link_directories(${MML_LINK_DIRECTORIES})

# To publish them as camitk application, the following would be needed:
# - a global naming (such as camitk-mml-appname)
# - a man page
# - for GUI app: an icon, a .desktop file etc...

set(MML_INCLUDE_DIRECTORIES
  ${CAMITK_INCLUDE_DIRECTORIES}
  ${CAMITK_INCLUDE_DIR}/libraries
  ${MMLSCHEMA_INCLUDE_DIR}
  ${CAMITK_INCLUDE_DIR}/libraries/mmlschema
  ${CAMITK_INCLUDE_DIR}/libraries/monitoring
  ${CAMITK_INCLUDE_DIR}/libraries/pml
  ${CAMITK_INCLUDE_DIR}/libraries/lml
  ${LIBXML2_INCLUDE_DIR}
  ${XERCESC_INCLUDE_DIR}
  ${XSD_INCLUDE_DIR}
  ${SOFA_INCLUDE_DIR}
)
set(MML_LIBRARIES
  ${LIBXML2_LIBRARIES}
  ${XERCESC_LIBRARIES}
  monitoring
)
set(MML_LINK_DIRECTORIES
  ${CAMITK_LINK_DIRECTORIES} 
  ${SOFA_LIB_DIR}
)

set(MMLSCHEMA_INCLUDE_DIR ${CAMITK_INCLUDE_DIR}/libraries/mmlschema)

# define names of the mml libraries globally
set(MMLSCHEMA_DEPENDENCY library${TARGET_NAME_SEPARATOR}mmlschema)
set(MONITORING_DEPENDENCY library${TARGET_NAME_SEPARATOR}monitoring)
set(MONITORING_GUI_DEPENDENCY library${TARGET_NAME_SEPARATOR}monitoringgui)
set(MML_DEPENDENCY ${MONITORING_DEPENDENCY} ${MMLSCHEMA_DEPENDENCY} ${PMLLML_DEPENDENCY})

option(MML_GENERATE_BENCHMARK "Generates benchmark binary" OFF)
if(MML_GENERATE_BENCHMARK)
  add_subdirectory(benchmark)
endif()

if(MML_GENERATE_GUI)
    option(MML_GENERATE_BENCHMARKGUI "Generates benchmarkgui binary" OFF)
    if(MML_GENERATE_BENCHMARKGUI)
        add_subdirectory(benchmarkgui)
    endif()
endif()

if(SOFA_SUPPORT)
    option(MML_GENERATE_SCN2PML "Generates scn2pml binary" OFF)
    if(MML_GENERATE_SCN2PML)    
        add_subdirectory(scn2pml)
    endif()
endif()

option(MML_GENERATE_PML2MMLOUT "Generates pml2mmlOut binary" OFF)
if(MML_GENERATE_PML2MMLOUT)
  add_subdirectory(pml2mmlOut)
endif()

option(MML_GENERATE_PARAMEXPLORER "Generates paramExplorer binary" OFF)
if(MML_GENERATE_PARAMEXPLORER)
  add_subdirectory(paramExplorer)
endif()

