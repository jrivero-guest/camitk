/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "SimulationDialog.h"

#include <Application.h>
#include <MonitoringGuiManager.h>
#include <MonitoringDialog.h>

#include <Log.h>

using namespace camitk;


// --------------- Constructor -------------------
SimulationDialog::SimulationDialog(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Simulation Dialog");
    setDescription("Show MML Simulation Dialog");
    setComponent("MMLComponent");

    // Setting classification family and tags
    setFamily("Modeling");
}

// --------------- getWidget -------------------
QWidget* SimulationDialog::getWidget() {
    MMLComponent* input = dynamic_cast<MMLComponent*>(getTargets().last());
    QWidget* myWidget = NULL;

    if (input) {
        myWidget = input->getMonitoringGuiManager()->getDialog();
        // show the widget
        myWidget->show();
    }
    else {
        CAMITK_ERROR(tr("Simulation Dialog action: MMLComponent not valid: no simulation tab can be shown"))
    }


    return myWidget;
}

// --------------- apply -------------------
Action::ApplyStatus SimulationDialog::apply() {
    return SUCCESS;
}


