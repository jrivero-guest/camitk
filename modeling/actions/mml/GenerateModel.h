/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef GENERATEMODEL_H
#define GENERATEMODEL_H

#include <Action.h>
#include <QVector3D>

#include <set>

namespace camitk {
class MeshComponent;
}

/**
 * @ingroup group_cepmodeling_actions_mml
 *
 * @brief
 * Action to generate MML/Sofa scn from a vtk mesh.
 *
 *
 **/
class GenerateModel : public camitk::Action {

public:
    /// the constructor
    GenerateModel(camitk::ActionExtension*);

    /// the destructor
    virtual ~GenerateModel() = default;

    /// default widget
    virtual QWidget* getWidget() {
        return Action::getWidget();
    }

public slots:
    /// method applied when the action is called
    virtual ApplyStatus apply();

private:
    /// save the .scn file, TODO add the choice to save MML with another simulator
    void saveSOFAFile(QString baseFilename);

    /// save all the MML, PML and LML file
    void saveMMLFiles(QString baseFilename);

    // list of index that are potentially fixed (null displacement constraint)
    std::set<int> fixedDOFIndex;

    /// The current managed mesh
    camitk::MeshComponent* targetMesh;
};

#endif // GENERATEMODEL_H
