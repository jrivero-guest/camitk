

#ifndef SIMULATIONTABS_H
#define SIMULATIONTABS_H

#include <Action.h>

#include <MMLComponent.h>

/**
 * @ingroup group_cepmodeling_actions_mml
 *
 * @brief
 * Action to show the MML Simulation Dialog
 *
 *
 **/
class SimulationDialog : public camitk::Action {

public:

    /// Default Constructor
    SimulationDialog(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~SimulationDialog() = default;

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of SimulationDialog (or a subclass).
      */
    virtual ApplyStatus apply();

    virtual QWidget* getWidget();

private:

};

#endif // SIMULATIONTABS_H

