<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>ui_AddNewPropertyWidget</name>
    <message>
        <location filename="../../AddNewPropertyWidget.ui" line="14"/>
        <source>Add New Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AddNewPropertyWidget.ui" line="35"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../AddNewPropertyWidget.ui" line="45"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ui_PmlExplorerWidget</name>
    <message>
        <location filename="../../PmlExplorerWidget.ui" line="20"/>
        <source>PML Explore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PmlExplorerWidget.ui" line="73"/>
        <location filename="../../PmlExplorerWidget.ui" line="76"/>
        <source>If checked, display a glyph on the atoms composing the current selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PmlExplorerWidget.ui" line="79"/>
        <source>Show Atom Glyph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PmlExplorerWidget.ui" line="117"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PmlExplorerWidget.ui" line="124"/>
        <source>Revert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PmlExplorerWidget.ui" line="131"/>
        <location filename="../../PmlExplorerWidget.ui" line="134"/>
        <location filename="../../PmlExplorerWidget.ui" line="137"/>
        <source>Click here to add a new property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../PmlExplorerWidget.ui" line="140"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
