/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef PMLEXPLOREACTION_H
#define PMLEXPLOREACTION_H

// Camitk includes
#include <Action.h>

class PMLExplorerWidget;
class PMLComponent;

class PMLExplorerAction : public camitk::Action {

public:

    /// Default Constructor
    PMLExplorerAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~PMLExplorerAction();

    /// @{
    /// @name Reimplemented from camitk::Action class
    /// Returns a QTreeWidget that displays in a tree view the PML component structure.
    QWidget* getWidget();
    /// @}

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of PMLExploreAction (or a subclass).
      */
    virtual ApplyStatus apply();

private:

    /// The widget of this action.
    /// Based on a Qt TreeView widget, allows to explore the PML structure.
    PMLExplorerWidget* pmlExplorerWidget;

};

#endif // PMLEXPLOREACTION_H

