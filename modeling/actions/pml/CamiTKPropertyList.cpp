/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "CamiTKPropertyList.h"

using namespace camitk;

// ---------------------- Destructor ----------------------------
CamiTKPropertyList::~CamiTKPropertyList() {
    // delete all properties
    foreach (Property* prop, propertyMap.values()) {
        delete prop;
    }
    propertyMap.clear();
}

// ---------------------- getProperty ----------------------------
Property* CamiTKPropertyList::getProperty(QString name) {
    return propertyMap.value(name);
}

Property* CamiTKPropertyList::getProperty(unsigned int index) {
    return getProperty(QString(dynamicPropertyNames().at(index)));
}

// ---------------------- addProperty ----------------------------
bool CamiTKPropertyList::addProperty(Property* prop) {
    // add a dynamic Qt Meta Object property with the same name
    bool returnStatus = setProperty(prop->getName().toStdString().c_str(), prop->getInitialValue());
    // add to the map
    propertyMap.insert(prop->getName(), prop);

    return returnStatus;
}

// ---------------------- getNumberOfProperties ----------------------------
unsigned int CamiTKPropertyList::getNumberOfProperties() const {
    return dynamicPropertyNames().size();
}

