/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef CREATESC_H
#define CREATESC_H

#include <Action.h>

class PMLComponent;

/**
 * @ingroup group_cepmodeling_actions_mml
 *
 * @brief
 * Action to generate MML/Sofa scn from a vtk mesh.
 *
 *
 **/
class CreateSC : public camitk::Action {

public:
    /// the constructor
    CreateSC(camitk::ActionExtension*);

    /// the destructor
    virtual ~CreateSC() = default;

    /// use the default widget (but update the values)
    virtual QWidget* getWidget();

    /// intercept signal for dynamic property changed
    virtual bool event(QEvent* e);

public slots:
    /// method applied when the action is called
    virtual ApplyStatus apply();

private:
    /// compute and update bound properties (bottom and top corner) considering selection parameters
    void updateCorners(PMLComponent*);

    /// compute the bounds of a named selection
    /// @return true if the selection exists and is not empty
    bool computeBoundsOfSelection(PMLComponent* comp, QString selectionName, double bounds[6]);

    /// compute bounds of a given point selection (list of Point Ids)
    void computeBoundsOfPointIds(PMLComponent* comp, vtkSmartPointer<vtkIdTypeArray> selectedIds, double bounds[6]);

    /// compute bounds of a given cell selection (list of cell Ids)
    void computeBoundsOfCellIds(PMLComponent* comp, vtkSmartPointer<vtkIdTypeArray> selectedIds, double bounds[6]);

    // merge 1st bounds to 2nd bounds (expand 2nd bounding box if needed)
    void mergeBounds(double bounds1[6], double bounds2[6]);

    /// if this is initialization time, do not consider any event
    bool checkEvents;
};

#endif // CREATESC_H
