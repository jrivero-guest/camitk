#undef QT_NO_STL
#undef QT_NO_STL_WCHAR

#ifndef NULL
#define NULL    0
#endif

// the PySide global.h file, containing common Qt Python declaration
#include "pyside_global.h"

// subdirectories header files
#include "./actions/global_sdk_actions.h"
#include "./applications/global_sdk_applications.h"
#include "./components/global_sdk_components.h"
#include "./libraries/global_sdk_libraries.h"

// include here the classes you want to be exposed to Python
