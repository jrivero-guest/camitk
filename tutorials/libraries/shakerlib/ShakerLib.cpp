/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "ShakerLib.h"

#include <cmath> // for sqrt
#include <cstdlib> // for rand

// --------------- randomDisplacement -------------------
void ShakerLib::randomDisplacement(float* pos, unsigned int nrOfPoints, double maxDisplacement) {
    // Move all the points
    for (unsigned int i = 0; i < nrOfPoints; i++) {
        // compute the random displacement
        float randomDisplacement[3];
        float factor = 0.0;

        for (float& value : randomDisplacement)  {
            value  =  -1. + 2.0 * ((double)  rand()) / ((double) RAND_MAX);
            factor  +=  value * value;
        }

        // scale up to max
        factor = maxDisplacement * ((double)  rand()) / ((double) RAND_MAX) / sqrt(factor);

        // move randomly
        for (unsigned  int  j = 0; j < 3; j++)  {
            randomDisplacement[j] *= factor;
            pos[i * 3 + j] += randomDisplacement[j];
        }

    }
}

