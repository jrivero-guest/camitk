/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "MixedComponent.h"

#include <VtkImageComponent.h>
#include <VtkMeshComponent.h>

#include <QFileInfo>

using namespace camitk;

// -------------------- default constructor  --------------------
MixedComponent::MixedComponent(const QString& file) : Component(file, QFileInfo(file).baseName()) {

    // read the file
    std::ifstream mixed(file.toStdString().c_str());
    std::string filename;
    getline(mixed, filename);
    QString mhaFile = QFileInfo(file).absolutePath() + "/" + filename.c_str();
    getline(mixed, filename);
    QString vtkFile = QFileInfo(file).absolutePath() + "/" + filename.c_str();

    // Create mha component and connect it as a sub-component
    VtkImageComponent* imageCpt = new VtkImageComponent(mhaFile);
    addChild(imageCpt);

    // Create vtk component and connect it as a sub-component
    VtkMeshComponent* meshCpt = new VtkMeshComponent(vtkFile);
    addChild(meshCpt);

}
