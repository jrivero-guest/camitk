/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK includes
#include "PickMe.h"

#include <vtkCell.h>

using namespace camitk;

// --------------- Constructor -------------------
PickMe::PickMe(const QString& file) : VtkMeshComponent(file) {
    // initially all the point data have a value of 0.0 (everything is blue)
    // when the user pick a cell or a point in 3D, the point data are modified
    // using the point index (for point picking) or average of cell's point index
    // (for cell picking)
    // -> the mesh starts to show colors!
    initPointData();
}

// --------------- initPointData -------------------
void PickMe::initPointData() {
    // create a new vtkDataArray to store the demo value
    demoPointData = vtkSmartPointer<vtkDoubleArray>::New();

    int numberOfPoints = getPointSet()->GetNumberOfPoints();
    demoPointData->SetNumberOfValues(numberOfPoints);
    demoPointData->SetName("Pick Me Data");
    demoPointData->FillComponent(0, 0.0);

    // add the data to the mesh
    addPointData(demoPointData->GetName(), demoPointData);
}


// -------------------- pointPicked --------------------
void PickMe::pointPicked(vtkIdType pointId, bool sel/* picking does not change the selection state, do not bother with the 2nd parameter*/) {
    MeshComponent::pointPicked(pointId, sel);

    // picking show demo point data
    demoPointData->SetValue(pointId, 1.0 - double (pointId) / double (getPointSet()->GetNumberOfPoints()));

    refreshPointData();
}

// -------------------- cellPicked --------------------
void PickMe::cellPicked(vtkIdType cellId, bool  sel /* picking does not change the selection state, do not bother with the 2nd parameter*/) {
    MeshComponent::cellPicked(cellId, sel);

    // change data for all cell points
    vtkSmartPointer<vtkCell> c = getPointSet()->GetCell(cellId);
    for (vtkIdType i = 0; i < c->GetNumberOfPoints(); i++) {
        vtkIdType pointId = c->GetPointId(i);
        demoPointData->SetValue(pointId, double (pointId) / double (getPointSet()->GetNumberOfPoints()));
    }

    refreshPointData();
}

// -------------------- refreshPointData --------------------
void PickMe::refreshPointData() {
    // force the modification flag of the vtkDoubleArray so that the range is going to be recomputed
    // This is needed so that VTK can "discover" that the data have changed (calls to SetValue do not modify the internal time stamp)
    demoPointData->Modified();

    // force the data visualization to be updated
    setDataRepresentationVisibility(MeshComponent::POINTS, demoPointData->GetName(), true);
}

