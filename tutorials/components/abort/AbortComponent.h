/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef Abort_COMPONENT_H
#define Abort_COMPONENT_H

#include <Component.h>

/** The manager of the abort testdata.
 *  A small extension that shows how CamiTK handle exception thrown during component instanciation
 *  This is just a test case.
 */
class AbortComponent : public camitk::Component {
public:
    /// default constructor
    /// This method may throw an AbortException if a problem occurs.
    AbortComponent(const QString& file);

    /// do nothing to init the representation
    virtual void initRepresentation() {};
};

#endif //Abort_COMPONENT_H
