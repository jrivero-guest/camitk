/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include <QCoreApplication>
#include <ExtensionManager.h>
#include <Application.h>
#include <Core.h>

using namespace camitk;

int main(int argc, char* argv[]) {
    // TODO FIXME allow an application to have no GUI
    // => Do everything below in a new CamiTK class ConsoleApplication that inherits from QCoreApplication
    // override setIcon in Action to have
    //     if (qApp->type() != QApplication::Tty)
    //  setIcon (QPixmap (":/fileOpen"));
    // add a new method in Action to set the shortcut (and NOT to do it if the qApp is Tty!)
    // Action::setShortcut() {
    //      if (qApp->type() != QApplication::Tty) {
    //         getQAction()->setShortcut( QKeySequence::Open );
    //         getQAction()->setShortcutContext(Qt::ApplicationShortcut);
    //      }
    // }
    // in ConsoleApplication, write an open() method that does not try to do anything on the cursor, main window, etc...
    // etc...
    QCoreApplication app(argc, argv);

    // get the executable path
    //QDir actionsDir(Core::getActionDirectories().first());
    //ExtensionManager::loadActionExtension(actionsDir.absoluteFilePath("libapplication.so"));

    // open an image volume
    ComponentExtension* cp = ExtensionManager::getComponentExtension("mha");
    cp->open(QFileInfo(Core::getTestDataDir() + "/brain.mha").absoluteFilePath());
    return app.exec();

}

