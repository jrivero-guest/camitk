/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef SIMPLEAPP_H
#define SIMPLEAPP_H

// -- Core stuff
#include <MainWindow.h>
using namespace camitk;

// -- QT stuff classes
class QTextEdit;
class QProgressBar;


/**
  * This class describes the simple application. There are no menubar, toolbar and statusbar.
  * This class demonstrates that an application can be a very classical 3D view and/or 4 viewers,
  * with just a MedicalViewer as the main widget.
  *
  * TODO add a button or something to be able to do something at least!!!
  */
class SimpleMainWindow : public MainWindow {
    Q_OBJECT

public:

    /// @name general
    ///@{
    /// construtor
    SimpleMainWindow();

    /// destructor
    virtual ~SimpleMainWindow() = default;

public slots:

    /// overidden to remove all but the main viewer
    virtual void aboutToShow();

};

#endif // SIMPLEAPP_H

