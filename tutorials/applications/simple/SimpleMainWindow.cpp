/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Application Simple stuff
#include "SimpleMainWindow.h"

// -- Core stuff
#include <MedicalImageViewer.h>
#include <InteractiveViewer.h>
#include <Log.h>

// -- QT stuff
#include <QToolBar>
#include <QKeyEvent>

// ------------- constructor -----------------
SimpleMainWindow::SimpleMainWindow() : MainWindow("Simple Demo") {

    // only add a main viewers
    setCentralViewer(MedicalImageViewer::getInstance());

    // all black like if they were old-school OpenGL
    InteractiveViewer::get3DViewer()->keyPressEvent(new QKeyEvent(QEvent::KeyPress, Qt::Key_A, Qt::NoModifier)); // simulate "a" stroke
    InteractiveViewer::get3DViewer()->setBackgroundColor(Qt::black);
    InteractiveViewer::get3DViewer()->setGradientBackground(false);
    InteractiveViewer::getAxialViewer()->setBackgroundColor(Qt::black);
    InteractiveViewer::getAxialViewer()->setGradientBackground(false);
    InteractiveViewer::getSagittalViewer()->setBackgroundColor(Qt::black);
    InteractiveViewer::getSagittalViewer()->setGradientBackground(false);
    InteractiveViewer::getCoronalViewer()->setBackgroundColor(Qt::black);
    InteractiveViewer::getCoronalViewer()->setGradientBackground(false);
}

// ------------- aboutToShow -----------------
void SimpleMainWindow::aboutToShow() {
    // remove all but the main viewer
    InteractiveViewer::get3DViewer()->getToolBar()->hide();
    camitk::MainWindow::aboutToShow();
}



