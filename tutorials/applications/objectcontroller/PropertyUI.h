/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef FORM_H
#define FORM_H

// -- QT stuff
#include <QWidget>
#include <QTextEdit>

// -- Core stuff classes
#include <ObjectController.h>

/** A simple user interface to interact with ObjectController.
  * The tricky part of this class is in the print() method (it prints
  * all static and dynamic property of any QObject in rich text and
  * take into account QVariantMap as property group)
  */
class PropertyUI : public QWidget {

    Q_OBJECT

public:
    PropertyUI(QWidget* parent = nullptr);

    /// set the property object to edit
    void setPropertyObject(QObject*);

private slots:
    /// called when the user wants to dynamically change the view mode of the object controller
    void changeViewMode(QString);

    /// print the currently edited object
    void print();

private :
    /// return a rich text QString that shows the name and property value
    QString propertyToString(QString name, QVariant value);

    /// the main object in the UI
    camitk::ObjectController* theController;

    /// to see the property values
    QTextEdit* console;
};

#endif
