/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Application ObjectController stuff
#include "TestClass.h"

// -- CamiTK stl stuff
#include <iostream>

// -- QT stuff
#include <QEvent>
#include <QLocale>
#include <QKeySequence>
#include <QRect>


TestClass::TestClass() : QObject() {

    // set a new dynamic property
    backgroundColor = QColor("wheat");
    setProperty("backgroundColor", backgroundColor);

    // install a filter to get the modification of the dynamic properties
    installEventFilter(this);

    // set initial values
    myBool = false;
    radius =  2.0 * 3.141592653589;
    position =  QVector3D(-1.0, 2.0, -4.5);
    name =  "testing!";
    color =  QColor("forestgreen");
    font = QFont("Times", 14, QFont::Bold);
    time =  QTime::currentTime();
    yourForm =  FEELING_GREAT;
    onWindows =  true;
    someNumber =  42; // the answer!

    propertyGroup.insert("subProperty1", "it works!");
    propertyGroup.insert("subProperty2", QVector3D(3.0, 4.0, 5.0));
    propertyGroup.insert("locale", QLocale::system());
    propertyGroup.insert("date", QDate::currentDate());
    propertyGroup.insert("keySeq", QKeySequence(Qt::CTRL + Qt::Key_C));
    propertyGroup.insert("char", QChar('s'));
    propertyGroup.insert("point", QPoint(28, 9));
    propertyGroup.insert("rect", QRect(20.0, 10.0, 19., 69.));
}

void TestClass::setBool(bool newValue) {
    myBool = newValue;
}

void TestClass::setPosition(QVector3D newValue) {
    position = newValue;
}


void TestClass::setLevel(StateLevel newValue) {
    yourForm = newValue;
}

void TestClass::setName(const QString& newValue) {
    name = newValue;
}

void TestClass::setRadius(double newValue) {
    radius = newValue;
}

void TestClass::setColor(const QColor& newValue) {
    color = newValue;
}

void TestClass::setFont(const QFont& newValue) {
    font = newValue;
}

void TestClass::setTime(const QTime& newValue) {
    time = newValue;
}

void TestClass::setGroup(const QVariantMap& newValue) {
    propertyGroup = newValue;
}


// ---------------------- event ----------------------------
bool TestClass::event(QEvent* e) {
    if (e->type() == QEvent::DynamicPropertyChange) {
        e->accept();
        auto* chev = dynamic_cast<QDynamicPropertyChangeEvent*>(e);

        if (! chev) {
            return false;
        }

        backgroundColor = property("backgroundColor").value<QColor>();
        return true;
    }

    return QObject::event(e);
}



