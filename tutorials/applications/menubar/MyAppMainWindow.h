/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef MYAPP_MAINWINDOW_H
#define MYAPP_MAINWINDOW_H


// -- CamiTK Core stuff
#include <MainWindow.h>
using namespace camitk;

/**
  * This Class describes a very simple application.
  * It is composed of a menu bar with three items:
  *  - file menu (open, close, quit)
  *  - action menu (present the available action for the currently selected component)
  *  - help menu (with a classic "about")
  *
  * The most difficult part is to manage the "Action" menu (as it depends on the
  * selection).
  */
class MyAppMainWindow : public MainWindow {
    Q_OBJECT

public:

    /// @name general
    ///@{
    /// construtor
    MyAppMainWindow();

    /// destructor
    ~MyAppMainWindow();
    ///@}

public slots:

    /** This slot is connected to all the viewers selectionChanged() signal,
     *  this will call the refresh method of all viewers but whoIsAsking
     */
    virtual void refresh();

    /// shows an "about" dialog
    void slotHelpAbout();

protected:
    /// update action menu
    void updateActionStates();

private:

    /** @name Menu and action */
    ///@{
    /// fileClose and fileSave are only enabled when a top-level component is selected
    QAction* fileClose;
    QAction* fileSave;

    /** the actionMenu.
     *  It contains the list of specific actions possible on the currently selected Component.
     *  It is enabled or not depending on the current selection.
     */
    QMenu* actionMenu;
    ///@}

};

#endif //MYAPP_MAINWINDOW_H

