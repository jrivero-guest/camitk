/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- CamiTK Application Imp stuff
#include "MyAppMainWindow.h"

// -- CamiTK Core stuff
#include <Application.h>
#include <Action.h>
#include <Explorer.h>
#include <InteractiveViewer.h>
#include <Core.h>
#include <Log.h>

// -- Qt Stuff
#include <QMenuBar>

// ------------- constructor -----------------
MyAppMainWindow::MyAppMainWindow() : MainWindow("MyApp - " + tr(Core::version)) {

    // init all GUI
    // -- file
    QMenu* fileMenu = new QMenu(tr("&File"));
    fileMenu->addAction(Application::getAction("Open")->getQAction());
    fileSave = Application::getAction("Save")->getQAction();
    fileMenu->addAction(fileSave);
    fileClose = Application::getAction("Close")->getQAction();
    fileMenu->addAction(fileClose);
    fileMenu->addSeparator();
    fileMenu->addAction(Application::getAction("Quit")->getQAction());

    // -- help
    QMenu* helpMenu = new QMenu(tr("&Help"));
    //--- actions of the Help menu
    helpMenu->addAction(Application::getAction("About...")->getQAction());

    // -- action
    actionMenu = new QMenu(tr("&Actions"));

    // -- add everything in the menu bar
    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(actionMenu);
    menuBar()->addSeparator();
    menuBar()->addMenu(helpMenu);

    // initialize architecture
    updateActionStates();

    // now add the different viewers
    addDockViewer(Qt::LeftDockWidgetArea, new Explorer());
    addDockViewer(Qt::RightDockWidgetArea, InteractiveViewer::get3DViewer());
    showStatusBar(true);
}


// ------------- destructor -----------------
MyAppMainWindow::~MyAppMainWindow() {
}

// ------------- refresh -----------------
void MyAppMainWindow::refresh() {
    MainWindow::refresh();

    // update all the action states
    updateActionStates();
}

// ------------------------ updateActionStates ----------------------------
void MyAppMainWindow::updateActionStates() {
    unsigned int nrOfSelectedItems = Application::getSelectedComponents().size();
    bool selectedIsTopLevel = (nrOfSelectedItems > 0 && Application::getSelectedComponents().last()->isTopLevel());

    //-- update file menu
    fileClose->setEnabled(selectedIsTopLevel);
    fileSave->setEnabled(selectedIsTopLevel);

    //-- update the action menu
    actionMenu->clear();
    actionMenu->setEnabled(false);
    if (nrOfSelectedItems > 0) {
        Component* comp = Application::getSelectedComponents().last();
        if (comp) {
            QMenu* compActionsMenu = comp->getActionMenu();
            if (compActionsMenu) {
                actionMenu->addActions(compActionsMenu->actions());
                actionMenu->setEnabled(true);
            }
        }
    }

    // update the application window title
    if (nrOfSelectedItems > 0) {
        setWindowSubtitle(Application::getSelectedComponents().last()->getFileName());
    }
}

// ------------- slotHelpAbout -----------------
void MyAppMainWindow::slotHelpAbout() {
    CAMITK_WARNING(tr("About: tutorial app for menu bar\nUsing %1\n(c) Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525").arg(QString(Core::version)))
}
