/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "MyViewer.h"

// -- Core stuff
#include <Application.h>
#include <SettingsDialog.h>

//-- Qt stuff
#include <QHBoxLayout>
#include <QTextEdit>


// -------------------- Constructor --------------------
MyViewer::MyViewer() : Viewer("My Simple Viewer") {
    inverse = nullptr;
    myMenu = nullptr;
    myWidget = nullptr;
    displayedTopLevelComponents = 0;
}


// -------------------- Destructor --------------------
MyViewer::~MyViewer() {
    delete myWidget;
    myWidget = nullptr;
    delete myMenu;
    myMenu = nullptr;
}


// -------------------- numberOfViewedComponentgetWidget --------------------
unsigned int MyViewer::numberOfViewedComponent() {
    return displayedTopLevelComponents;
}

// -------------------- refresh --------------------
void MyViewer::refresh(Viewer* whoIsAsking) {
    // view everything?
    if (displayedTopLevelComponents != Application::getTopLevelComponents().size()) {
        // if there the nr of Component changed since last refresh,
        // to something!
        output->append("There was " + QString::number(displayedTopLevelComponents) + " top-level components");
        output->append("Now there is " + QString::number(Application::getTopLevelComponents().size()));
        output->append("(" + QString::number(Application::getAllComponents().size()) + " components in total)");
        output->append("");

        // update the counter
        displayedTopLevelComponents = Application::getTopLevelComponents().size();
    }
}

// -------------------- getWidget --------------------
QWidget* MyViewer::getWidget(QWidget* parent) {
    if (!myWidget) {
        myWidget = new QWidget(parent);
        output = new QTextEdit;
        textColor = Qt::blue;
        bgColor = Qt::white;
        updateColors();

        //-- init layout
        auto* myWidgetLayout = new QHBoxLayout;
        myWidgetLayout->setSpacing(0);
        myWidgetLayout->setMargin(0);

        //-- add the output text edit
        myWidgetLayout->addWidget(output);

        myWidget->setLayout(myWidgetLayout);
    }

    return myWidget;
}

// ---------------------- getPropertyObject ----------------------------
QObject* MyViewer::getPropertyObject() {
    //  (change the text/background color)
    return this;
}

// -------------------- getMenu --------------------
QMenu* MyViewer::getMenu() {
    if (!myMenu) {
        //-- create the main menu
        myMenu = new QMenu(objectName());

        //-- add the action
        inverse = new QAction(tr("&Inverse display"), this);
        inverse->setCheckable(true);

        connect(inverse, SIGNAL(triggered(bool)), this, SLOT(inverseColors(bool)));

        myMenu->addAction(inverse);
    }
    return myMenu;
}

// -------------------- inverseColors --------------------
void MyViewer::inverseColors(bool state) {
    if (state) {
        QColor background = output->textBackgroundColor();
        output->setTextBackgroundColor(output->textColor());
        output->setTextColor(background);
        output->append("Inversed colors");
    }
    else  {
        QColor background = output->textBackgroundColor();
        output->setTextBackgroundColor(output->textColor());
        output->setTextColor(background);
        output->append("Normal colors");
    }
}

// -------------------- updateColors --------------------
void MyViewer::updateColors() {
    output->setTextColor(textColor);
    output->setTextBackgroundColor(bgColor);
    output->append("Colors updated");
}


// -------------------- getBgColor --------------------
QColor MyViewer::getBgColor() {
    return bgColor;
}

// -------------------- getTextColor --------------------
QColor MyViewer::getTextColor() {
    return textColor;
}

// -------------------- setBgColor --------------------
void MyViewer::setBgColor(QColor c) {
    bgColor = c;
    updateColors();
}

// -------------------- setTextColor --------------------
void MyViewer::setTextColor(QColor c) {
    textColor = c;
    updateColors();
}

// -------------------- slotEditPreference --------------------
void MyViewer::editPreference() {
    SettingsDialog settingsDialog;
    settingsDialog.editSettings(this);
    settingsDialog.exec();
}
