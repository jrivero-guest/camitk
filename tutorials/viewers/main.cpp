/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// -- Core stuff
#include <Application.h>
#include <MainWindow.h>
#include <Core.h>
#include <Action.h>

using namespace camitk;

#include "MyViewer.h"
#include <QMenuBar>

/** This program demonstrates how to build a CamiTK Application with only
 *  one viewer. This particular Viewer is the demo viewer MyViewer.
 *
 *  The program also show how you can add a simple menu with three items:
 *  - fileOpen to open any known component file
 *  - editPreference that automatically show all the default application preferences
 *    (i.e. the list of installed extensions) and all the viewers' preferences
 *    In this example the viewer preferences are just the choice of colors.
 *  - the viewer menu which is build by the viewer. In this example, it contains
 *    only one item (a checkbox to inverse the colors)
 */

int main(int argc, char* argv[]) {
    // create a camitk application
    Application a("viewer-tutorial", argc, argv);

    // create a myViewer instance
    auto* myViewer = new MyViewer();

    // add myViewer to the main window
    a.getMainWindow()->setCentralViewer(myViewer);

    // get the open action
    QAction* fileOpen = Application::getAction("Open")->getQAction();
    // get the edit preferences action
    QAction* editPreference = new QAction("&Edit Preferences...", a.getMainWindow());
    a.getMainWindow()->connect(editPreference, SIGNAL(triggered()), myViewer, SLOT(editPreference()));

    // create menu
    QMenu* simpleMenu = new QMenu("&Simple Menu");
    simpleMenu->addAction(fileOpen);
    simpleMenu->addAction(editPreference);
    simpleMenu->addMenu(myViewer->getMenu()); // add the viewer menu
    a.getMainWindow()->menuBar()->addMenu(simpleMenu);

    // open an image volume
    Application::open(Core::getTestDataDir() + "/brain.mha");

    // let's go!
    return a.exec();
}

