/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef MY_VIEWER_H
#define MY_VIEWER_H

// -- Core stuff
#include <Viewer.h>
using namespace camitk;

// -- QT stuff
#include <QTextEdit>
#include <QWidget>
#include <QAction>
#include <QMenu>

/**
  *    A simple text viewer...
  *    It just send a text message everytime a component is read/close and gives information
  *    about current number of components
  */
class MyViewer : public Viewer {
    Q_OBJECT

    /// the background color for the text output
    Q_PROPERTY(QColor bgColor READ getBgColor WRITE setBgColor)

    /// the forground color for the text ouput
    Q_PROPERTY(QColor textColor READ getTextColor WRITE setTextColor)

public:

    /** @name Constructors/Destructors
      */
    /// @{
    /** construtor. */
    MyViewer();

    /** destructor */
    virtual ~MyViewer();
    /// @}

    /** @name Viewer inherited
      */
    /// @{
    /// returns the number of Component that are displayed by this viewer
    virtual unsigned int numberOfViewedComponent();

    /// refresh the view (can be interesting to know which other viewer is calling this)
    virtual void refresh(Viewer* whoIsAsking = nullptr);

    /// get the viewer widget. @param parent the parent widget for the viewer widget
    virtual QWidget* getWidget(QWidget* parent = nullptr);

    /// get the propertyObject (only the 3D Scene one)
    virtual QObject* getPropertyObject();

    /// get the viewer menu
    virtual QMenu* getMenu();
    /// @}

    /** @name Property handling
      */
    /// @{
    QColor getTextColor();
    void setTextColor(QColor);
    QColor getBgColor();
    void setBgColor(QColor);
    /// @}

public slots:
    /// inverse background/forground colors
    void inverseColors(bool);

    /// this slot should be put in an MainWindow extension, but for simplicity sake for this example, this is here...
    void editPreference();

private:

    /// The viewer's widget
    QWidget* myWidget;

    /// the text output
    QTextEdit* output;

    /// the viewer's menu
    QMenu* myMenu;

    /// inverse rendering
    QAction* inverse;

    /// number of top level component that are currently displayed
    unsigned int displayedTopLevelComponents;

    /// colors
    QColor textColor;
    QColor bgColor;
    void updateColors();

};




#endif

