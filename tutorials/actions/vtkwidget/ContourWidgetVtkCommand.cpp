/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#include "ContourWidgetVtkCommand.h"
#include "ContourWidget.h"

#include <Log.h>

#include <vtkContourWidget.h>
#include <vtkOrientedGlyphContourRepresentation.h>


// -------------------- constructor --------------------
ContourWidgetVtkCommand::ContourWidgetVtkCommand(ContourWidget* a) {
    contourWidgetAction = a;
}

// --------------- Execute -------------------
void ContourWidgetVtkCommand::Execute(vtkObject* caller, long unsigned int, void*) {
    // Get the contour widget
    vtkSmartPointer <vtkContourWidget> contourWidget = reinterpret_cast<vtkContourWidget*>(caller);

    // set the action properties
    vtkSmartPointer<vtkOrientedGlyphContourRepresentation> contourRep = vtkOrientedGlyphContourRepresentation::SafeDownCast(contourWidget->GetRepresentation());

    if (contourRep != nullptr) {
        contourWidgetAction->setProperty("Number Of Points", QVariant(contourRep->GetNumberOfNodes()));
        CAMITK_INFO_ALT(tr("ContourWidget now has %1 nodes").arg(QString::number(contourRep->GetNumberOfNodes())))
    }
    else {
        CAMITK_INFO_ALT(tr("ContourWidget has no representation"))
    }

    // the setProperty line above modified the property value, the action's widget needs to be refreshed
    contourWidgetAction->updateWidget();
}


