/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef CONTOURWIDGET_H
#define CONTOURWIDGET_H

#include <Action.h>

#include <QObject>
#include <QFrame>
#include <QMap>

#include <vtkSmartPointer.h>
#include <vtkContourWidget.h>
#include <vtkTransformPolyDataFilter.h>

#include <MeshComponent.h>
#include <ImageComponent.h>

/** Adding a contour widget to the currently selected image component
 * To simplify this class inherits from both CamiTK Action and VTK Command, for real life code, please
 * add a specific/separated class inheriting from vtkCommand.
 */
class ContourWidget : public camitk::Action {
    Q_OBJECT

    /// the viewer to use
    Q_ENUMS(Viewer)
    Q_PROPERTY(Viewer viewer READ getViewer WRITE setWiewer)

public:
    /// Define the possible types of morphological operatiosn
    enum Viewer {Axial, Coronal, Sagittal};

    /// Default Constructor
    ContourWidget(camitk::ActionExtension* extension);

    /// Default Destructor
    virtual ~ContourWidget() = default;

    /// method called when the action when the action is triggered (i.e. started)
    virtual QWidget* getWidget();

    /// Setters and Getters for the viewer
    Viewer getViewer() const;
    void setWiewer(const Viewer);

    /// update the widget information
    virtual void updateWidget();

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of ImageComponent (or a subclass).
      */
    virtual ApplyStatus apply();

    /// initialize/reset the contour
    void initContour();

    /// close the current contour
    void closeContour();

private:
    /// this action widget (to simplify, it is just a label that gives information + a button)
    QFrame* informationFrame;

    /// currently selected image component
    camitk::ImageComponent* currentImage;

    /// the mesh component build by the spline
    camitk::MeshComponent* currentMesh;

    /// the interactive contour representation
    vtkSmartPointer<vtkContourWidget> contourWidget;

    /// Control the user interaction with the contour widget
    vtkSmartPointer<vtkCommand> contourWidgetCommand;

    /// the transformation filter
    vtkSmartPointer<vtkTransformPolyDataFilter> transformFromAxialToWorld;

    /// currently selected viewer
    Viewer viewer;

    /// automatic recording is already in place
    bool recording;

    /// update the contour visualization
    void updateContour();

};
#endif // CONTOURWIDGET_H
