/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

#ifndef BOX_WIDGET_H
#define BOX_WIDGET_H

#include <Action.h>

// VTK stuff
#include <vtkSmartPointer.h>
#include <vtkCommand.h>

class vtkBoxWidget;

/**
 * Demonstrates how to add a VTK Widget in the 3D Scene.
 * This is an embedded action (it is shown in the ActionViewer).
 * To simplify this class inherits from both CamiTK Action and VTK Command, for real life code, please
 * add a specific/separated class inheriting from vtkCommand.
 */
class BoxWidget : public camitk::Action {

public:
    /// the constructor
    BoxWidget(camitk::ActionExtension*);

    /// the destructor
    virtual ~BoxWidget();

    /// method called when the action when the action is triggered (i.e. started)
    virtual QWidget* getWidget();

    /// update the widget information
    virtual void updateWidget();

public slots:
    /// method called when the action is applied
    virtual ApplyStatus apply();

protected:

    /// The widget to see the real information => the high resolution region
    vtkSmartPointer<vtkBoxWidget> boxWidget;

    /// Control the user interaction with the box widget
    vtkSmartPointer<vtkCommand> boxWidgetCommand;
};

#endif // BOX_WIDGET_H
