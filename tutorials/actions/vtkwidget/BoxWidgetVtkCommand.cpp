/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "BoxWidgetVtkCommand.h"

#include "BoxWidget.h"

#include <vtkBoxWidget.h>
#include <vtkPolyData.h>

#include <QVector3D>

// -------------------- constructor --------------------
BoxWidgetVtkCommand::BoxWidgetVtkCommand(BoxWidget* a) {
    boxWidgetAction = a;
}

// --------------- Execute -------------------
void BoxWidgetVtkCommand::Execute(vtkObject* caller, long unsigned int, void*) {
    // Get the box widget
    vtkSmartPointer <vtkBoxWidget> boxWidget = reinterpret_cast<vtkBoxWidget*>(caller);

    // Get the box bounds
    vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
    boxWidget->GetPolyData(polyData);
    double boxBounds[6];
    polyData->GetBounds(boxBounds);

    // set the action properties
    boxWidgetAction->setProperty("Bottom Corner", QVector3D(boxBounds[0], boxBounds[2], boxBounds[4]));
    boxWidgetAction->setProperty("Top Corner", QVector3D(boxBounds[1], boxBounds[3], boxBounds[5]));

    // the two lines above modified the property values, the action's widget needs to be refreshed
    boxWidgetAction->updateWidget();
}
