/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef ADD_DYNAMIC_PROPERTY_H
#define ADD_DYNAMIC_PROPERTY_H

#include <Action.h>


class AddDynamicProperty : public camitk::Action {

    Q_OBJECT

    /// Enumerations
    Q_ENUMS(PropertyType)

    /// For QEnum, it is better/easier to use a static property
    Q_PROPERTY(PropertyType implementation READ getPropertyType WRITE setPropertyType)

public:
    /// Possible Property types
    enum PropertyType {INT, CHAR, BOOL, FLOAT, DOUBLE, QSTRING, QDATE, QTIME, QCOLOR, QPOINT, QPOINTF, QVECTOR3D  };

    /// Default Constructor
    AddDynamicProperty(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~AddDynamicProperty();

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., any instances of Component (or a subclass).
      */
    virtual ApplyStatus apply();

    /// get the currently selected property type
    PropertyType getPropertyType();

    /// method called when the user modify the widget
    void setPropertyType(PropertyType implementation);

private:
    PropertyType currentType;
};

#endif // ADD_DYNAMIC_PROPERTY_H

