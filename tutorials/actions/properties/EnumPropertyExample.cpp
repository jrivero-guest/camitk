/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "EnumPropertyExample.h"

#include <Property.h>
#include <Log.h>

// to manage icons
#include <QtGui/QIcon>
#include <QtVariantPropertyManager>

// to get the enum as a string
#include <QMetaEnum>

using namespace camitk;

// --------------- Constructor -------------------
EnumPropertyExample::EnumPropertyExample(ActionExtension* extension) : Action(extension) {
    //-- Setting name, description and input component
    setName("Enum Property Example");
    setDescription("Demonstrates how to add an enum parameters in an action. Show the information on the log console.");
    setComponent("");

    //-- Setting classification family and tags
    setFamily("Tutorial");
    addTag("Dynamic Property");
    addTag("Enum");

    //-- Enum demo #1
    // enum with automatic enum name. Names are taken directly from the enum literals (they all appear uppercase, not nice...)
    Property* autoEnum = new Property("Auto Enum", EnumPropertyExample::ACE, "An enum using different GUI strings", "");
    // To set the enum type, use the same name of the enum as declared in the class
    autoEnum->setEnumTypeName("EnumerationExample");
    // Add the property as an action parameter
    addParameter(autoEnum);

    //-- Enum demo #2
    // enum with custom list of enum name in the GUI, + icons for each value (taken from resources file cards.qrc in resources subdir)
    Property* customEnum = new Property("Custom Enum", EnumPropertyExample::ACE, "An enum using different GUI strings", "");
    // Set the enum type
    customEnum->setEnumTypeName("EnumerationExample");
    // Set a custom list of GUI names
    QStringList enumGuiText;
    enumGuiText << "A" << "K" << "Q" << "J" << "10" << "9" << "8" << "7";
    customEnum->setAttribute("enumNames", enumGuiText);
    // Add icon decoration
    Q_INIT_RESOURCE(cards);
    QtIconMap enumIcons;
    enumIcons[0] = QIcon(":/ace");
    enumIcons[1] = QIcon(":/king");
    enumIcons[2] = QIcon(":/queen");
    enumIcons[3] = QIcon(":/jack");
    enumIcons[4] = QIcon(":/ten");
    enumIcons[5] = QIcon(":/nine");
    enumIcons[6] = QIcon(":/eight");
    enumIcons[7] = QIcon(":/seven");
    customEnum->setEnumIcons(enumIcons);
    // Add the property as an action parameter
    addParameter(customEnum);

    //-- Enum demo #3
    // enum with automatic enum name but nicely presented with spaces and capitalized first letter + icons
    Property* cleanEnum = new Property("Clean Enum", EnumPropertyExample::ACE, "An enum using different GUI strings", "");
    // passing "this" as the second argument will enable the property to transform the enum literals to clearer GUI names
    cleanEnum->setEnumTypeName("EnumerationExample", this);
    // also add icons!
    cleanEnum->setEnumIcons(enumIcons);
    addParameter(cleanEnum);
}

// --------------- apply -------------------
Action::ApplyStatus EnumPropertyExample::apply() {
    // get the current selected values
    int autoValue = property("Auto Enum").toInt();
    QString autoValueString = "<Not Implemented>";

    int customValue = property("Custom Enum").toInt();
    QString customValueString = "<Not Implemented>";

    int cleanValue = property("Clean Enum").toInt();
    QString cleanValueString = "<Not Implemented>";

#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
    // With Q_ENUM corresponding string values can be retrieved easily
    QMetaEnum metaEnum = QMetaEnum::fromType<EnumerationExample>();

    autoValueString = metaEnum.valueToKey(autoValue);

    customValueString = metaEnum.valueToKey(customValue);

    cleanValueString = metaEnum.valueToKey(cleanValue);
#endif

    CAMITK_INFO(tr("Current value of the properties [int value, enum value]\n"
                   " - Auto Enum:   [%1,%2]\n"
                   " - Custom Enum: [%3,%4]\n"
                   " - Clean Enum:  [%5,%6]\n")
                .arg(QString::number(autoValue), autoValueString, QString::number(customValue), customValueString, QString::number(cleanValue), cleanValueString))

    return SUCCESS;
}


