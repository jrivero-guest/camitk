/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef PROPACTION_H
#define PROPACTION_H

#include <Action.h>

class PropAction : public camitk::Action {

    // DO NOT forget to add Q_OBJECT otherwise the enumeration cannot be registered in the Meta Object
    Q_OBJECT

public:
    /// Default Constructor
    PropAction(camitk::ActionExtension*);

    /// Default Destructor
    virtual ~PropAction() = default;

    /// Possible Enumeration
    enum Enumeration { ACE, KING, QUEEN, JACK, TEN, NINE, EIGHT, SEVEN };

#if QT_VERSION < QT_VERSION_CHECK(5, 5, 0)
    /// classic Q_ENUMS
    Q_ENUMS(Enumeration)
#else
    /// easier with Qt >= 5.5
    Q_ENUM(Enumeration)
#endif

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., any instances of Component (or a subclass).
      */
    virtual ApplyStatus apply();

};

#if QT_VERSION < QT_VERSION_CHECK(5, 5, 0)
/// with classic Q_ENUMS, meta-type needs to be declared
Q_DECLARE_METATYPE(PropAction::Enumeration)
#endif

#endif // PROPACTION_H

