/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef DIRECTSHAKER_H
#define DIRECTSHAKER_H

#include <Action.h>

#include <MeshComponent.h>

#include <QMutex>
#include <QTimer>
#include <vtkDoubleArray.h>

class DirectShaker : public camitk::Action {
    Q_OBJECT

public:

    /// Default Constructor
    DirectShaker(camitk::ActionExtension* extension);

    /// Default Destructor
    virtual ~DirectShaker();

public slots:
    /** this method is automatically called when the action is triggered.
      * Call getTargets() method to get the list of components to use.
      * \note getTargets() is automatically filtered so that it only contains compatible components,
      * i.e., instances of MeshComponent (or a subclass).
      */
    virtual ApplyStatus apply();

    /// move the points once
    void animate();

private:
    /// timer that call process method regularly
    QTimer* processTimer;

    /// Mutex used for the 3D update
    QMutex* mutex;

    /// current mesh component
    camitk::MeshComponent* currentTarget;

    /// initial position of the point (so that we can compute total displacements)
    vtkSmartPointer<vtkPoints> initialPosition;

    /// compute random displacements (pos should be of size 3*nrOfPoints)
    void randomDisplacement(float* pos, unsigned int nrOfPoints, double maxDisplacement);

};

#endif // DIRECTSHAKER_H

