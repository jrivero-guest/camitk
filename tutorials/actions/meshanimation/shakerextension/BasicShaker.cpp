/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#include "BasicShaker.h"

#include <Application.h>
#include <Property.h>

using namespace camitk;

// --------------- Constructor -------------------
BasicShaker::BasicShaker(ActionExtension* extension) : Action(extension) {
    // Setting name, description and input component
    setName("Basic Shaker");
    setDescription("The shaker motor. It shakes your mesh in all directions (once)!");
    setComponent("MeshComponent");

    // Setting classification family and tags
    setFamily("Tutorial");
    addTag("Demo");
    addTag("Animation");

    // Setting the action's parameters
    Property* intensity = new Property(tr("Shaking Intensity"), QVariant(1.0), tr("Max intensity for the sshhhhaakkiinggg..."), tr("%age of the bounding radius"));
    intensity->setAttribute("minimum", 0.0);
    intensity->setAttribute("maximum", 100.0);
    addParameter(intensity);
}

// --------------- apply -------------------
Action::ApplyStatus BasicShaker::apply() {
    // get the mesh component to use
    MeshComponent* currentTarget = dynamic_cast<MeshComponent*>(getTargets().last());
    // Get the parameters
    double shakingIntensity = currentTarget->getBoundingRadius() * property("Shaking Intensity").toDouble() / 100.0;

    // Move all the points randomly once
    for (vtkIdType id = 0; id < currentTarget->getPointSet()->GetNumberOfPoints(); id++) {
        // compute the random displacement
        double randomDisplacement[3];
        double factor = 0.0;
        for (unsigned  int  j = 0; j < 3; j++)  {
            randomDisplacement[j]  =  -1. + 2.0 * ((double)  rand()) / ((double) RAND_MAX);

            factor  +=  randomDisplacement[j] * randomDisplacement[j];
        }
        // scale up to max
        factor = shakingIntensity * ((double)  rand()) / ((double) RAND_MAX) / sqrt(factor);

        // get the initial point position
        double pos[3];
        currentTarget->getPointSet()->GetPoint(id, pos);

        // move randomly
        for (unsigned  int  j = 0; j < 3; j++)  {
            randomDisplacement[j] *= factor;
            pos[j] += randomDisplacement[j];
        }
        currentTarget->getPointSet()->GetPoints()->SetPoint(id, pos);
    }

    // refresh display
    currentTarget->getPointSet()->Modified();
    currentTarget->setModified(); // so as to warn the user when closing the shaked object

    // refresh the view
    currentTarget->refresh();

    return SUCCESS;
}

