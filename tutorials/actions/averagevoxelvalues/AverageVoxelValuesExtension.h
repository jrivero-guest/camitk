/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef AVERAGE_VOXEL_VALUES_EXTENSION_H
#define AVERAGE_VOXEL_VALUES_EXTENSION_H

#include <ActionExtension.h>

class AverageVoxelValuesExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension);
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.tutorials.action.averagevoxelvalues")

public:
    /// Constructor
    AverageVoxelValuesExtension() : ActionExtension() {};

    /// Destructor
    virtual ~AverageVoxelValuesExtension() = default;

    /// Method returning the action extension name
    virtual QString getName() {
        return "AverageVoxelValuesExtension";
    };

    /// Method returning the action extension descrption
    virtual QString getDescription() {
        return "Extensions for Biopolis developers";
    };

    /// initialize all the actions
    virtual void init();

};

#endif // AVERAGE_VOXEL_VALUES_EXTENSION_H


