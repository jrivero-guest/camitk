/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2018 Univ. Grenoble Alpes, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#ifndef BASIC_PICKING_EXTENSION_H
#define BASIC_PICKING_EXTENSION_H

#include <QObject>
#include <Action.h>
#include <ActionExtension.h>

/// a simple action extension containing two actions: one for mesh picking, one for image picking
class BasicPickingExtension : public camitk::ActionExtension {
    Q_OBJECT
    Q_INTERFACES(camitk::ActionExtension)
    Q_PLUGIN_METADATA(IID "fr.imag.camitk.tutorials.action.basicpicking")

public:
    /// the constructor
    BasicPickingExtension() : ActionExtension() {};

    /// the destructor
    virtual ~BasicPickingExtension() = default;

    /// initialize all the actions
    virtual void init();

    /// Method that return the action extension name
    virtual QString getName() {
        return "Basic Picking Tutorial";
    };

    /// Method that return the action extension descrption
    virtual QString getDescription() {
        return "This is a basic picking tutorial/demo extension.<br/>It contains two actions demonstrating one way to get information on mesh or images using picking (clicking on the object in the 2D/3D interactive viewers).";
    };

};

#endif // BASIC_PICKING_EXTENSION_H
