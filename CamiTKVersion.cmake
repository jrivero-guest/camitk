#Specific to SDK: control the version (major, minor, default patch and )
set(CAMITK_VERSION_MAJOR "4")
set(CAMITK_VERSION_MINOR "1")
set(CAMITK_VERSION_NICKNAME "burgundy") # Red Burgundy Amaranth Rose Auburn Cardinal Carmine Cerise Coquelicot Bordeaux
set(CAMITK_VERSION_PATCH "2") # patch version for packaging, change when appropriate
